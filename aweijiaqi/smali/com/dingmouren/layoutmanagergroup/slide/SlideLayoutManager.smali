.class public Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$LayoutManager;
.source "SlideLayoutManager.java"


# instance fields
.field private mItemTouchHelper:Landroid/support/v7/widget/helper/ItemTouchHelper;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/helper/ItemTouchHelper;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;-><init>()V

    .line 88
    new-instance v0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager$1;

    invoke-direct {v0, p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager$1;-><init>(Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;)V

    iput-object v0, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 23
    invoke-direct {p0, p1}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->checkIsNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 24
    invoke-direct {p0, p2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->checkIsNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/support/v7/widget/helper/ItemTouchHelper;

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mItemTouchHelper:Landroid/support/v7/widget/helper/ItemTouchHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;)Landroid/support/v7/widget/RecyclerView;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;)Landroid/support/v7/widget/helper/ItemTouchHelper;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mItemTouchHelper:Landroid/support/v7/widget/helper/ItemTouchHelper;

    return-object p0
.end method

.method private checkIsNull(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 29
    throw p1
.end method


# virtual methods
.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .line 36
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 11

    .line 41
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->detachAndScrapAttachedViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 42
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getItemCount()I

    move-result p2

    const v0, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x3

    const/4 v3, 0x0

    if-le p2, v2, :cond_2

    const/4 p2, 0x3

    :goto_0
    if-ltz p2, :cond_4

    .line 45
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v10

    .line 46
    invoke-virtual {p0, v10}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->addView(Landroid/view/View;)V

    .line 47
    invoke-virtual {p0, v10, v3, v3}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->measureChildWithMargins(Landroid/view/View;II)V

    .line 48
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getWidth()I

    move-result v4

    invoke-virtual {p0, v10}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v5

    sub-int/2addr v4, v5

    .line 49
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getHeight()I

    move-result v5

    invoke-virtual {p0, v10}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v6

    sub-int/2addr v5, v6

    .line 50
    div-int/lit8 v6, v4, 0x2

    div-int/lit8 v7, v5, 0x5

    .line 51
    invoke-virtual {p0, v10}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v4

    add-int v8, v6, v4

    .line 52
    invoke-virtual {p0, v10}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v4

    add-int v9, v7, v4

    move-object v4, p0

    move-object v5, v10

    .line 50
    invoke-virtual/range {v4 .. v9}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->layoutDecoratedWithMargins(Landroid/view/View;IIII)V

    if-ne p2, v2, :cond_0

    add-int/lit8 v4, p2, -0x1

    int-to-float v5, v4

    mul-float v5, v5, v0

    sub-float v5, v1, v5

    .line 55
    invoke-virtual {v10, v5}, Landroid/view/View;->setScaleX(F)V

    .line 56
    invoke-virtual {v10, v5}, Landroid/view/View;->setScaleY(F)V

    .line 57
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    mul-int v4, v4, v5

    div-int/lit8 v4, v4, 0xe

    int-to-float v4, v4

    invoke-virtual {v10, v4}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1

    :cond_0
    if-lez p2, :cond_1

    int-to-float v4, p2

    mul-float v4, v4, v0

    sub-float v4, v1, v4

    .line 59
    invoke-virtual {v10, v4}, Landroid/view/View;->setScaleX(F)V

    .line 60
    invoke-virtual {v10, v4}, Landroid/view/View;->setScaleY(F)V

    .line 61
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    mul-int v4, v4, p2

    div-int/lit8 v4, v4, 0xe

    int-to-float v4, v4

    invoke-virtual {v10, v4}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1

    .line 63
    :cond_1
    iget-object v4, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v10, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_1
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    add-int/lit8 p2, p2, -0x1

    :goto_2
    if-ltz p2, :cond_4

    .line 68
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v2

    .line 69
    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->addView(Landroid/view/View;)V

    .line 70
    invoke-virtual {p0, v2, v3, v3}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->measureChildWithMargins(Landroid/view/View;II)V

    .line 71
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getWidth()I

    move-result v4

    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v5

    sub-int/2addr v4, v5

    .line 72
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getHeight()I

    move-result v5

    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v6

    sub-int/2addr v5, v6

    .line 73
    div-int/lit8 v6, v4, 0x2

    div-int/lit8 v7, v5, 0x5

    .line 74
    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v4

    add-int v8, v6, v4

    .line 75
    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v4

    add-int v9, v7, v4

    move-object v4, p0

    move-object v5, v2

    .line 73
    invoke-virtual/range {v4 .. v9}, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->layoutDecoratedWithMargins(Landroid/view/View;IIII)V

    if-lez p2, :cond_3

    int-to-float v4, p2

    mul-float v4, v4, v0

    sub-float v4, v1, v4

    .line 78
    invoke-virtual {v2, v4}, Landroid/view/View;->setScaleX(F)V

    .line 79
    invoke-virtual {v2, v4}, Landroid/view/View;->setScaleY(F)V

    .line 80
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    mul-int v4, v4, p2

    div-int/lit8 v4, v4, 0xe

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_3

    .line 82
    :cond_3
    iget-object v4, p0, Lcom/dingmouren/layoutmanagergroup/slide/SlideLayoutManager;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_3
    add-int/lit8 p2, p2, -0x1

    goto :goto_2

    :cond_4
    return-void
.end method
