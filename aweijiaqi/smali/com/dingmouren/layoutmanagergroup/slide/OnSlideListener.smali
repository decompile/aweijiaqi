.class public interface abstract Lcom/dingmouren/layoutmanagergroup/slide/OnSlideListener;
.super Ljava/lang/Object;
.source "OnSlideListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onClear()V
.end method

.method public abstract onSlided(Landroid/support/v7/widget/RecyclerView$ViewHolder;Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            "TT;I)V"
        }
    .end annotation
.end method

.method public abstract onSliding(Landroid/support/v7/widget/RecyclerView$ViewHolder;FI)V
.end method
