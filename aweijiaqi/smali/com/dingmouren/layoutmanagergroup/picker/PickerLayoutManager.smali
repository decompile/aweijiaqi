.class public Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "PickerLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PickerLayoutManager"


# instance fields
.field private mIsAlpha:Z

.field private mItemCount:I

.field private mItemViewHeight:I

.field private mItemViewWidth:I

.field private mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

.field private mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;

.field private mOrientation:I

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    const/high16 p1, 0x3f000000    # 0.5f

    .line 20
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mScale:F

    const/4 p1, 0x1

    .line 21
    iput-boolean p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mIsAlpha:Z

    const/4 p1, -0x1

    .line 26
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    .line 32
    new-instance p1, Landroid/support/v7/widget/LinearSnapHelper;

    invoke-direct {p1}, Landroid/support/v7/widget/LinearSnapHelper;-><init>()V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    .line 33
    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOrientation:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;IZIFZ)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p3, p4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    const/high16 p1, 0x3f000000    # 0.5f

    .line 20
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mScale:F

    const/4 p1, 0x1

    .line 21
    iput-boolean p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mIsAlpha:Z

    const/4 p1, -0x1

    .line 26
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    .line 38
    new-instance p1, Landroid/support/v7/widget/LinearSnapHelper;

    invoke-direct {p1}, Landroid/support/v7/widget/LinearSnapHelper;-><init>()V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    .line 39
    iput p5, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    .line 40
    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOrientation:I

    .line 41
    iput-object p2, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 42
    iput-boolean p7, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mIsAlpha:Z

    .line 43
    iput p6, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mScale:F

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    .line 44
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->setAutoMeasureEnabled(Z)V

    :cond_0
    return-void
.end method

.method private scaleHorizontalChildView()V
    .locals 8

    .line 121
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/4 v2, 0x0

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 123
    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 124
    invoke-virtual {p0, v3}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getDecoratedLeft(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0, v3}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getDecoratedRight(Landroid/view/View;)I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v1

    const/high16 v5, -0x40800000    # -1.0f

    .line 125
    iget v6, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mScale:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v6, v7, v6

    mul-float v6, v6, v5

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float v6, v6, v4

    div-float/2addr v6, v0

    add-float/2addr v6, v7

    .line 126
    invoke-virtual {v3, v6}, Landroid/view/View;->setScaleX(F)V

    .line 127
    invoke-virtual {v3, v6}, Landroid/view/View;->setScaleY(F)V

    .line 128
    iget-boolean v4, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mIsAlpha:Z

    if-eqz v4, :cond_0

    .line 129
    invoke-virtual {v3, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private scaleVerticalChildView()V
    .locals 8

    .line 138
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/4 v2, 0x0

    .line 139
    :goto_0
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 140
    invoke-virtual {p0, v2}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 141
    invoke-virtual {p0, v3}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getDecoratedTop(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0, v3}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getDecoratedBottom(Landroid/view/View;)I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v1

    const/high16 v5, -0x40800000    # -1.0f

    .line 142
    iget v6, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mScale:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v6, v7, v6

    mul-float v6, v6, v5

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float v6, v6, v4

    div-float/2addr v6, v0

    add-float/2addr v6, v7

    .line 143
    invoke-virtual {v3, v6}, Landroid/view/View;->setScaleX(F)V

    .line 144
    invoke-virtual {v3, v6}, Landroid/view/View;->setScaleY(F)V

    .line 145
    iget-boolean v4, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mIsAlpha:Z

    if-eqz v4, :cond_0

    .line 146
    invoke-virtual {v3, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .line 53
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V

    .line 54
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/LinearSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 0

    .line 94
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 95
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getItemCount()I

    move-result p1

    if-ltz p1, :cond_2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->isPreLayout()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    iget p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOrientation:I

    if-nez p1, :cond_1

    .line 98
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->scaleHorizontalChildView()V

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    if-ne p1, p2, :cond_2

    .line 100
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->scaleVerticalChildView()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V
    .locals 1

    .line 67
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getItemCount()I

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    if-eqz v0, :cond_1

    const/4 p2, 0x0

    .line 69
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object p1

    .line 70
    invoke-virtual {p0, p1, p3, p4}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->measureChildWithMargins(Landroid/view/View;II)V

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewWidth:I

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewHeight:I

    .line 75
    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOrientation:I

    const/4 p4, 0x1

    if-nez p3, :cond_0

    .line 76
    iget p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    sub-int/2addr p1, p4

    div-int/lit8 p1, p1, 0x2

    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewWidth:I

    mul-int p1, p1, p3

    .line 77
    iget-object p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 78
    iget-object p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p3, p1, p2, p1, p2}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 79
    iget p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewWidth:I

    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    mul-int p1, p1, p2

    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewHeight:I

    invoke-virtual {p0, p1, p2}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_0
    if-ne p3, p4, :cond_2

    .line 81
    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    sub-int/2addr p3, p4

    div-int/lit8 p3, p3, 0x2

    mul-int p3, p3, p1

    .line 82
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 83
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1, p2, p3, p2, p3}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 84
    iget p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewWidth:I

    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemViewHeight:I

    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mItemCount:I

    mul-int p2, p2, p3

    invoke-virtual {p0, p1, p2}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->setMeasuredDimension(II)V

    goto :goto_0

    .line 87
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/LinearLayoutManager;->onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onScrollStateChanged(I)V
    .locals 2

    .line 158
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onScrollStateChanged(I)V

    if-nez p1, :cond_0

    .line 160
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    if-eqz p1, :cond_0

    .line 161
    invoke-virtual {p1, p0}, Landroid/support/v7/widget/LinearSnapHelper;->findSnapView(Landroid/support/v7/widget/RecyclerView$LayoutManager;)Landroid/view/View;

    move-result-object p1

    .line 162
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v0

    .line 163
    iget-object v1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;

    invoke-interface {v1, p1, v0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;->onSelectedView(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->scaleHorizontalChildView()V

    .line 108
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I

    move-result p1

    return p1
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->scaleVerticalChildView()V

    .line 114
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I

    move-result p1

    return p1
.end method

.method public setOnSelectedViewListener(Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager;->mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/picker/PickerLayoutManager$OnSelectedViewListener;

    return-void
.end method
