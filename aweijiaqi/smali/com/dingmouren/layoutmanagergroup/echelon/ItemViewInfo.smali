.class public Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;
.super Ljava/lang/Object;
.source "ItemViewInfo.java"


# instance fields
.field private mIsBottom:Z

.field private mLayoutPercent:F

.field private mPositionOffset:F

.field private mScaleXY:F

.field private mTop:I


# direct methods
.method public constructor <init>(IFFF)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mTop:I

    .line 17
    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mScaleXY:F

    .line 18
    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mPositionOffset:F

    .line 19
    iput p4, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mLayoutPercent:F

    return-void
.end method


# virtual methods
.method public getLayoutPercent()F
    .locals 1

    .line 36
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mLayoutPercent:F

    return v0
.end method

.method public getPositionOffset()F
    .locals 1

    .line 44
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mPositionOffset:F

    return v0
.end method

.method public getScaleXY()F
    .locals 1

    .line 28
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mScaleXY:F

    return v0
.end method

.method public getTop()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mTop:I

    return v0
.end method

.method public setIsBottom()Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;
    .locals 1

    const/4 v0, 0x1

    .line 23
    iput-boolean v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mIsBottom:Z

    return-object p0
.end method

.method public setLayoutPercent(F)V
    .locals 0

    .line 40
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mLayoutPercent:F

    return-void
.end method

.method public setPositionOffset(F)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mPositionOffset:F

    return-void
.end method

.method public setScaleXY(F)V
    .locals 0

    .line 32
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mScaleXY:F

    return-void
.end method

.method public setTop(I)V
    .locals 0

    .line 56
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->mTop:I

    return-void
.end method
