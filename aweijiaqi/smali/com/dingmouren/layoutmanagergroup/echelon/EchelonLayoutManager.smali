.class public Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$LayoutManager;
.source "EchelonLayoutManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EchelonLayoutManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemCount:I

.field private mItemViewHeight:I

.field private mItemViewWidth:I

.field private mScale:F

.field private mScrollOffset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;-><init>()V

    const v0, 0x7fffffff

    .line 23
    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    const v0, 0x3f666666    # 0.9f

    .line 24
    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScale:F

    .line 27
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mContext:Landroid/content/Context;

    .line 28
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getHorizontalSpace()I

    move-result p1

    int-to-float p1, p1

    const v0, 0x3f5eb852    # 0.87f

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewWidth:I

    int-to-float p1, p1

    const v0, 0x3fbae148    # 1.46f

    mul-float p1, p1, v0

    float-to-int p1, p1

    .line 29
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    return-void
.end method

.method private layoutChild(Landroid/support/v7/widget/RecyclerView$Recycler;)V
    .locals 22

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 68
    :cond_0
    iget v0, v6, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    iget v1, v6, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    div-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v1

    iget v2, v6, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    sub-int/2addr v1, v2

    .line 71
    iget v3, v6, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    rem-int/2addr v3, v2

    int-to-float v4, v3

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float v4, v4, v5

    int-to-float v2, v2

    div-float v2, v4, v2

    .line 74
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v9, v0, -0x1

    move v11, v9

    const/4 v12, 0x1

    :goto_0
    if-ltz v11, :cond_2

    .line 76
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v15

    iget v10, v6, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    sub-int/2addr v15, v10

    div-int/lit8 v15, v15, 0x2

    int-to-double v13, v15

    move v15, v11

    const-wide v10, 0x3fe999999999999aL    # 0.8

    int-to-double v5, v12

    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    mul-double v13, v13, v5

    int-to-double v5, v1

    float-to-double v10, v2

    mul-double v10, v10, v13

    sub-double v10, v5, v10

    double-to-int v1, v10

    move-object/from16 v11, p0

    .line 78
    iget v10, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScale:F

    move/from16 v17, v9

    float-to-double v9, v10

    add-int/lit8 v7, v12, -0x1

    move/from16 v18, v3

    move/from16 v19, v4

    int-to-double v3, v7

    invoke-static {v9, v10, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    iget v7, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScale:F

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v7, v16, v7

    mul-float v7, v7, v2

    sub-float v7, v16, v7

    move-wide/from16 v20, v3

    float-to-double v3, v7

    mul-double v9, v9, v3

    double-to-float v3, v9

    int-to-float v4, v1

    mul-float v4, v4, v16

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v4, v7

    .line 81
    new-instance v7, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    invoke-direct {v7, v1, v3, v2, v4}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;-><init>(IFFF)V

    const/4 v1, 0x0

    .line 82
    invoke-virtual {v8, v1, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    sub-double/2addr v5, v13

    double-to-int v3, v5

    if-gtz v3, :cond_1

    int-to-double v2, v3

    add-double/2addr v2, v13

    double-to-int v2, v2

    .line 85
    invoke-virtual {v7, v2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setTop(I)V

    const/4 v2, 0x0

    .line 86
    invoke-virtual {v7, v2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setPositionOffset(F)V

    .line 87
    invoke-virtual {v7}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v3

    div-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v7, v2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setLayoutPercent(F)V

    .line 88
    iget v2, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScale:F

    float-to-double v2, v2

    move-wide/from16 v4, v20

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v7, v2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setScaleXY(F)V

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v15, -0x1

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v7, p1

    move-object v6, v11

    move/from16 v9, v17

    move/from16 v4, v19

    const/high16 v5, 0x3f800000    # 1.0f

    move v11, v1

    move v1, v3

    move/from16 v3, v18

    goto/16 :goto_0

    :cond_2
    move/from16 v18, v3

    move/from16 v19, v4

    move-object v11, v6

    move/from16 v17, v9

    const/4 v1, 0x0

    .line 93
    :goto_1
    iget v2, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemCount:I

    if-ge v0, v2, :cond_3

    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v2

    sub-int v2, v2, v18

    .line 95
    new-instance v3, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    iget v4, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    int-to-float v4, v4

    div-float v4, v19, v4

    int-to-float v5, v2

    const/high16 v6, 0x3f800000    # 1.0f

    mul-float v5, v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getVerticalSpace()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-direct {v3, v2, v6, v4, v5}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;-><init>(IFFF)V

    .line 96
    invoke-virtual {v3}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setIsBottom()Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    move-result-object v2

    .line 95
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move/from16 v0, v17

    .line 101
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    sub-int v7, v0, v2

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    :goto_3
    if-ltz v2, :cond_6

    .line 106
    invoke-virtual {v11, v2}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 107
    invoke-virtual {v11, v3}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v4

    if-gt v4, v0, :cond_5

    if-ge v4, v7, :cond_4

    goto :goto_4

    :cond_4
    move-object/from16 v9, p1

    goto :goto_5

    :cond_5
    :goto_4
    move-object/from16 v9, p1

    .line 109
    invoke-virtual {v11, v3, v9}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->removeAndRecycleView(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    :goto_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    :cond_6
    move-object/from16 v9, p1

    .line 113
    invoke-virtual/range {p0 .. p1}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->detachAndScrapAttachedViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    const/4 v14, 0x0

    :goto_6
    if-ge v14, v6, :cond_7

    add-int v0, v7, v14

    .line 116
    invoke-virtual {v9, v0}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v12

    .line 117
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    .line 118
    invoke-virtual {v11, v12}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->addView(Landroid/view/View;)V

    .line 119
    invoke-direct {v11, v12}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->measureChildWithExactlySize(Landroid/view/View;)V

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getHorizontalSpace()I

    move-result v0

    iget v1, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewWidth:I

    sub-int/2addr v0, v1

    div-int/lit8 v2, v0, 0x2

    .line 121
    invoke-virtual {v13}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v3

    iget v0, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewWidth:I

    add-int v4, v2, v0

    invoke-virtual {v13}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v0

    iget v1, v11, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    add-int v5, v0, v1

    move-object/from16 v0, p0

    move-object v1, v12

    invoke-virtual/range {v0 .. v5}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->layoutDecoratedWithMargins(Landroid/view/View;IIII)V

    .line 122
    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {v12, v0}, Landroid/view/View;->setPivotX(F)V

    const/4 v0, 0x0

    .line 123
    invoke-virtual {v12, v0}, Landroid/view/View;->setPivotY(F)V

    .line 124
    invoke-virtual {v13}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getScaleXY()F

    move-result v1

    invoke-virtual {v12, v1}, Landroid/view/View;->setScaleX(F)V

    .line 125
    invoke-virtual {v13}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getScaleXY()F

    move-result v1

    invoke-virtual {v12, v1}, Landroid/view/View;->setScaleY(F)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    :cond_7
    return-void
.end method

.method private measureChildWithExactlySize(Landroid/view/View;)V
    .locals 3

    .line 133
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewWidth:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 134
    iget v2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 135
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    return-void
.end method


# virtual methods
.method public canScrollVertically()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .line 34
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public getHorizontalSpace()I
    .locals 2

    .line 149
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getVerticalSpace()I
    .locals 2

    .line 142
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 2

    .line 40
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->isPreLayout()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 43
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getHorizontalSpace()I

    move-result p2

    int-to-float p2, p2

    const v0, 0x3f5eb852    # 0.87f

    mul-float p2, p2, v0

    float-to-int p2, p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewWidth:I

    int-to-float p2, p2

    const v0, 0x3fbae148    # 1.46f

    mul-float p2, p2, v0

    float-to-int p2, p2

    .line 44
    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    .line 45
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->getItemCount()I

    move-result p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemCount:I

    .line 46
    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemCount:I

    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    mul-int v0, v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    .line 48
    invoke-direct {p0, p1}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->layoutChild(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 3

    .line 53
    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    add-int v0, p3, p1

    .line 54
    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    add-int/2addr p3, p1

    invoke-static {v1, p3}, Ljava/lang/Math;->max(II)I

    move-result p3

    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemCount:I

    iget v2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mItemViewHeight:I

    mul-int v1, v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    .line 55
    invoke-direct {p0, p2}, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->layoutChild(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 56
    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/echelon/EchelonLayoutManager;->mScrollOffset:I

    sub-int/2addr p2, v0

    add-int/2addr p2, p1

    return p2
.end method
