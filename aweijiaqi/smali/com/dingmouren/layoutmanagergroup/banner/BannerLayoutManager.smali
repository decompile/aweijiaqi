.class public Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "BannerLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;,
        Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BannerLayoutManager"


# instance fields
.field private mCurrentPosition:I

.field private mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

.field private mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

.field private mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;

.field private mOrientation:I

.field private mRealCount:I

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private mTimeDelayed:J

.field private mTimeSmooth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2

    .line 50
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 43
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    const-wide/16 v0, 0x3e8

    .line 45
    iput-wide v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeDelayed:J

    const/high16 v0, 0x43160000    # 150.0f

    .line 47
    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeSmooth:F

    .line 51
    new-instance v0, Landroid/support/v7/widget/LinearSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    .line 52
    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRealCount:I

    .line 53
    new-instance p3, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    invoke-direct {p3, p0}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;-><init>(Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;)V

    iput-object p3, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    .line 54
    iput-object p2, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 55
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->setOrientation(I)V

    .line 56
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mOrientation:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .line 59
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 43
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    const-wide/16 v0, 0x3e8

    .line 45
    iput-wide v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeDelayed:J

    const/high16 p1, 0x43160000    # 150.0f

    .line 47
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeSmooth:F

    .line 60
    new-instance p1, Landroid/support/v7/widget/LinearSnapHelper;

    invoke-direct {p1}, Landroid/support/v7/widget/LinearSnapHelper;-><init>()V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    .line 61
    iput p3, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRealCount:I

    .line 62
    new-instance p1, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    invoke-direct {p1, p0}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;-><init>(Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;)V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    .line 63
    iput-object p2, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 64
    invoke-virtual {p0, p4}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->setOrientation(I)V

    .line 65
    iput p4, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mOrientation:I

    return-void
.end method

.method static synthetic access$000(Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;)F
    .locals 0

    .line 37
    iget p0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeSmooth:F

    return p0
.end method


# virtual methods
.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .line 70
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V

    .line 77
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/LinearSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 2

    .line 130
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 131
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;->setSendMsg(Z)V

    .line 132
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    .line 133
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    add-int/2addr v0, p2

    iput v0, p1, Landroid/os/Message;->what:I

    .line 134
    iget-object p2, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    iget-wide v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeDelayed:J

    invoke-virtual {p2, p1, v0, v1}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public onScrollStateChanged(I)V
    .locals 4

    .line 98
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onScrollStateChanged(I)V

    const/4 v0, 0x1

    if-nez p1, :cond_1

    .line 100
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mLinearSnapHelper:Landroid/support/v7/widget/LinearSnapHelper;

    if-eqz p1, :cond_2

    .line 102
    invoke-virtual {p1, p0}, Landroid/support/v7/widget/LinearSnapHelper;->findSnapView(Landroid/support/v7/widget/RecyclerView$LayoutManager;)Landroid/view/View;

    move-result-object p1

    .line 103
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v1

    iput v1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    .line 105
    iget-object v2, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mRealCount:I

    rem-int/2addr v1, v3

    invoke-interface {v2, p1, v1}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;->onSelectedView(Landroid/view/View;I)V

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    invoke-virtual {p1, v0}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;->setSendMsg(Z)V

    .line 109
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    .line 110
    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mCurrentPosition:I

    .line 111
    iput v1, p1, Landroid/os/Message;->what:I

    .line 112
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    iget-wide v1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeDelayed:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    if-ne p1, v0, :cond_2

    .line 116
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mHandler:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$TaskHandler;->setSendMsg(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setOnSelectedViewListener(Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mOnSelectedViewListener:Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$OnSelectedViewListener;

    return-void
.end method

.method public setTimeDelayed(J)V
    .locals 0

    .line 121
    iput-wide p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeDelayed:J

    return-void
.end method

.method public setTimeSmooth(F)V
    .locals 0

    .line 125
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->mTimeSmooth:F

    return-void
.end method

.method public smoothScrollToPosition(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;I)V
    .locals 0

    .line 82
    new-instance p2, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$1;

    .line 83
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager$1;-><init>(Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;Landroid/content/Context;)V

    .line 91
    invoke-virtual {p2, p3}, Landroid/support/v7/widget/LinearSmoothScroller;->setTargetPosition(I)V

    .line 92
    invoke-virtual {p0, p2}, Lcom/dingmouren/layoutmanagergroup/banner/BannerLayoutManager;->startSmoothScroll(Landroid/support/v7/widget/RecyclerView$SmoothScroller;)V

    return-void
.end method
