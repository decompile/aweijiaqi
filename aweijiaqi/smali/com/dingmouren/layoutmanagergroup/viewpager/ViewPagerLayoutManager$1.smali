.class Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;
.super Ljava/lang/Object;
.source "ViewPagerLayoutManager.java"

# interfaces
.implements Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;


# direct methods
.method constructor <init>(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .line 124
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-virtual {p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getChildCount()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 125
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;->onInitComplete()V

    :cond_0
    return-void
.end method

.method public onChildViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .line 131
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$100(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-virtual {v2, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getPosition(Landroid/view/View;)I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;->onPageRelease(ZI)V

    goto :goto_0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-static {v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;->this$0:Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;

    invoke-virtual {v2, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getPosition(Landroid/view/View;)I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;->onPageRelease(ZI)V

    :cond_1
    :goto_0
    return-void
.end method
