.class public Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "ViewPagerLayoutManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ViewPagerLayoutManager"


# instance fields
.field private mChildAttachStateChangeListener:Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;

.field private mDrift:I

.field private mOnViewPagerListener:Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

.field private mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 121
    new-instance p1, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;

    invoke-direct {p1, p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;-><init>(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mChildAttachStateChangeListener:Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;

    .line 27
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 121
    new-instance p1, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;

    invoke-direct {p1, p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager$1;-><init>(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mChildAttachStateChangeListener:Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;

    .line 32
    invoke-direct {p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mOnViewPagerListener:Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    return-object p0
.end method

.method static synthetic access$100(Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;)I
    .locals 0

    .line 18
    iget p0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mDrift:I

    return p0
.end method

.method private init()V
    .locals 1

    .line 36
    new-instance v0, Landroid/support/v7/widget/PagerSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/PagerSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    return-void
.end method


# virtual methods
.method public onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .line 43
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V

    .line 44
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/PagerSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 45
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 46
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mChildAttachStateChangeListener:Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->addOnChildAttachStateChangeListener(Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;)V

    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 0

    .line 51
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    return-void
.end method

.method public onScrollStateChanged(I)V
    .locals 3

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_1

    .line 77
    :cond_0
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {p1, p0}, Landroid/support/v7/widget/PagerSnapHelper;->findSnapView(Landroid/support/v7/widget/RecyclerView$LayoutManager;)Landroid/view/View;

    move-result-object p1

    .line 78
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getPosition(Landroid/view/View;)I

    goto :goto_1

    .line 73
    :cond_1
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {p1, p0}, Landroid/support/v7/widget/PagerSnapHelper;->findSnapView(Landroid/support/v7/widget/RecyclerView$LayoutManager;)Landroid/view/View;

    move-result-object p1

    .line 74
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getPosition(Landroid/view/View;)I

    goto :goto_1

    .line 66
    :cond_2
    iget-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mPagerSnapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {p1, p0}, Landroid/support/v7/widget/PagerSnapHelper;->findSnapView(Landroid/support/v7/widget/RecyclerView$LayoutManager;)Landroid/view/View;

    move-result-object p1

    .line 67
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getPosition(Landroid/view/View;)I

    move-result p1

    .line 68
    iget-object v1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mOnViewPagerListener:Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getChildCount()I

    move-result v1

    if-ne v1, v0, :cond_4

    .line 69
    iget-object v1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mOnViewPagerListener:Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->getItemCount()I

    move-result v2

    sub-int/2addr v2, v0

    if-ne p1, v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, p1, v0}, Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;->onPageSelected(IZ)V

    :cond_4
    :goto_1
    return-void
.end method

.method public scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 0

    .line 109
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mDrift:I

    .line 110
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I

    move-result p1

    return p1
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 0

    .line 95
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mDrift:I

    .line 96
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I

    move-result p1

    return p1
.end method

.method public setOnViewPagerListener(Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/viewpager/ViewPagerLayoutManager;->mOnViewPagerListener:Lcom/dingmouren/layoutmanagergroup/viewpager/OnViewPagerListener;

    return-void
.end method
