.class public Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$LayoutManager;
.source "SkidRightLayoutManager.java"


# instance fields
.field private mHasChild:Z

.field private mItemCount:I

.field private mItemHeightWidthRatio:F

.field private mItemViewHeight:I

.field private mItemViewWidth:I

.field private mScale:F

.field private mScrollOffset:I

.field private mSkidRightSnapHelper:Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;


# direct methods
.method public constructor <init>(FF)V
    .locals 1

    .line 32
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mHasChild:Z

    const v0, 0x7fffffff

    .line 26
    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    .line 33
    iput p1, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemHeightWidthRatio:F

    .line 34
    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScale:F

    .line 35
    new-instance p1, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;

    invoke-direct {p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;-><init>()V

    iput-object p1, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mSkidRightSnapHelper:Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;

    return-void
.end method

.method private fillChild(Landroid/view/View;Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;)V
    .locals 7

    .line 133
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->addView(Landroid/view/View;)V

    .line 134
    invoke-direct {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->measureChildWithExactlySize(Landroid/view/View;)V

    .line 135
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getScaleXY()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v1

    mul-float v0, v0, v2

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 137
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPaddingTop()I

    move-result v4

    .line 138
    invoke-virtual {p2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v1

    sub-int v3, v1, v0

    .line 139
    invoke-virtual {p2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v1

    iget v2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    add-int/2addr v1, v2

    sub-int v5, v1, v0

    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewHeight:I

    add-int v6, v4, v0

    move-object v1, p0

    move-object v2, p1

    .line 138
    invoke-virtual/range {v1 .. v6}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->layoutDecoratedWithMargins(Landroid/view/View;IIII)V

    .line 140
    invoke-virtual {p2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getScaleXY()F

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setScaleX(Landroid/view/View;F)V

    .line 141
    invoke-virtual {p2}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getScaleXY()F

    move-result p2

    invoke-static {p1, p2}, Landroid/support/v4/view/ViewCompat;->setScaleY(Landroid/view/View;F)V

    return-void
.end method

.method private makeScrollOffsetWithinRange(I)I
    .locals 2

    .line 154
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    mul-int v0, v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method private measureChildWithExactlySize(Landroid/view/View;)V
    .locals 5

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 146
    iget v1, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 148
    iget v3, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewHeight:I

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int/2addr v3, v4

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    sub-int/2addr v3, v0

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 150
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method


# virtual methods
.method public calculateDistanceToPosition(I)I
    .locals 1

    .line 169
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->convert2LayoutPosition(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    mul-int v0, v0, p1

    .line 170
    iget p1, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    sub-int/2addr v0, p1

    return v0
.end method

.method public canScrollHorizontally()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public convert2AdapterPosition(I)I
    .locals 1

    .line 189
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method public convert2LayoutPosition(I)I
    .locals 1

    .line 193
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method public fill(Landroid/support/v7/widget/RecyclerView$Recycler;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 77
    iget v2, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    iget v3, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    div-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 78
    iget v3, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    iget v4, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    rem-int/2addr v3, v4

    int-to-float v5, v3

    const/high16 v6, 0x3f800000    # 1.0f

    mul-float v5, v5, v6

    int-to-float v4, v4

    div-float v4, v5, v4

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getHorizontalSpace()I

    move-result v7

    .line 82
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v9, v2, -0x1

    .line 83
    iget v10, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    sub-int v10, v7, v10

    const/4 v12, 0x1

    :goto_0
    if-ltz v9, :cond_1

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getHorizontalSpace()I

    move-result v14

    iget v15, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    sub-int/2addr v14, v15

    div-int/lit8 v14, v14, 0x2

    int-to-double v14, v14

    iget v11, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScale:F

    move/from16 v17, v7

    float-to-double v6, v11

    move v11, v2

    int-to-double v1, v12

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    mul-double v14, v14, v1

    int-to-double v1, v10

    float-to-double v6, v4

    mul-double v6, v6, v14

    sub-double v6, v1, v6

    double-to-int v6, v6

    .line 87
    new-instance v7, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    iget v10, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScale:F

    move-wide/from16 v18, v14

    float-to-double v13, v10

    add-int/lit8 v10, v12, -0x1

    move v15, v11

    int-to-double v10, v10

    .line 88
    invoke-static {v13, v14, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    move/from16 v20, v15

    iget v15, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScale:F

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v15, v16, v15

    mul-float v15, v15, v4

    sub-float v15, v16, v15

    move/from16 v21, v9

    move-wide/from16 v22, v10

    float-to-double v9, v15

    mul-double v13, v13, v9

    double-to-float v9, v13

    int-to-float v10, v6

    mul-float v10, v10, v16

    move/from16 v11, v17

    int-to-float v13, v11

    div-float/2addr v10, v13

    invoke-direct {v7, v6, v9, v4, v10}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;-><init>(IFFF)V

    const/4 v6, 0x0

    .line 92
    invoke-virtual {v8, v6, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    sub-double v1, v1, v18

    double-to-int v10, v1

    if-gtz v10, :cond_0

    int-to-double v1, v10

    add-double v1, v1, v18

    double-to-int v1, v1

    .line 96
    invoke-virtual {v7, v1}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setTop(I)V

    const/4 v1, 0x0

    .line 97
    invoke-virtual {v7, v1}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setPositionOffset(F)V

    .line 98
    invoke-virtual {v7}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->getTop()I

    move-result v1

    div-int/2addr v1, v11

    int-to-float v1, v1

    invoke-virtual {v7, v1}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setLayoutPercent(F)V

    .line 99
    iget v1, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScale:F

    float-to-double v1, v1

    move-wide/from16 v9, v22

    invoke-static {v1, v2, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v7, v1}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setScaleXY(F)V

    goto :goto_1

    :cond_0
    add-int/lit8 v9, v21, -0x1

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v1, p1

    move v7, v11

    move/from16 v2, v20

    const/high16 v6, 0x3f800000    # 1.0f

    goto/16 :goto_0

    :cond_1
    move/from16 v20, v2

    move v11, v7

    const/4 v6, 0x0

    .line 104
    :goto_1
    iget v1, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    move/from16 v2, v20

    if-ge v2, v1, :cond_2

    sub-int v7, v11, v3

    .line 106
    new-instance v1, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    iget v3, v0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    int-to-float v3, v3

    div-float/2addr v5, v3

    int-to-float v3, v7

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v3, v3, v4

    int-to-float v9, v11

    div-float/2addr v3, v9

    invoke-direct {v1, v7, v4, v5, v3}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;-><init>(IFFF)V

    .line 108
    invoke-virtual {v1}, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;->setIsBottom()Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    move-result-object v1

    .line 106
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, -0x1

    .line 113
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    sub-int v3, v2, v3

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getChildCount()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    :goto_3
    if-ltz v4, :cond_5

    .line 119
    invoke-virtual {v0, v4}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 120
    invoke-virtual {v0, v5}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->convert2LayoutPosition(I)I

    move-result v7

    if-gt v7, v2, :cond_4

    if-ge v7, v3, :cond_3

    goto :goto_4

    :cond_3
    move-object/from16 v7, p1

    goto :goto_5

    :cond_4
    :goto_4
    move-object/from16 v7, p1

    .line 122
    invoke-virtual {v0, v5, v7}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->removeAndRecycleView(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    :goto_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    :cond_5
    move-object/from16 v7, p1

    .line 125
    invoke-virtual/range {p0 .. p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->detachAndScrapAttachedViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    const/4 v13, 0x0

    :goto_6
    if-ge v13, v1, :cond_6

    add-int v2, v3, v13

    .line 128
    invoke-virtual {v0, v2}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->convert2AdapterPosition(I)I

    move-result v2

    invoke-virtual {v7, v2}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;

    invoke-direct {v0, v2, v4}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->fillChild(Landroid/view/View;Lcom/dingmouren/layoutmanagergroup/echelon/ItemViewInfo;)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    :cond_6
    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .line 40
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public getFixedScrollPosition(IF)I
    .locals 4

    .line 51
    iget-boolean v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mHasChild:Z

    const/4 v1, -0x1

    if-eqz v0, :cond_2

    .line 52
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    iget v2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    rem-int v3, v0, v2

    if-nez v3, :cond_0

    return v1

    :cond_0
    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v0, v0, v1

    int-to-float v2, v2

    div-float/2addr v0, v2

    if-lez p1, :cond_1

    add-float/2addr v0, p2

    goto :goto_0

    :cond_1
    sub-float/2addr v1, p2

    add-float/2addr v0, v1

    :goto_0
    float-to-int p1, v0

    add-int/lit8 p1, p1, -0x1

    .line 56
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->convert2AdapterPosition(I)I

    move-result p1

    return p1

    :cond_2
    return v1
.end method

.method public getHorizontalSpace()I
    .locals 2

    .line 201
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getVerticalSpace()I
    .locals 2

    .line 197
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .line 46
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onAttachedToWindow(Landroid/support/v7/widget/RecyclerView;)V

    .line 47
    iget-object v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mSkidRightSnapHelper:Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;

    invoke-virtual {v0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 1

    .line 64
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->isPreLayout()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 66
    iget-boolean p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mHasChild:Z

    if-nez p2, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getVerticalSpace()I

    move-result p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewHeight:I

    int-to-float p2, p2

    .line 68
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemHeightWidthRatio:F

    div-float/2addr p2, v0

    float-to-int p2, p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    const/4 p2, 0x1

    .line 69
    iput-boolean p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mHasChild:Z

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->getItemCount()I

    move-result p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    .line 72
    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    invoke-direct {p0, p2}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->makeScrollOffsetWithinRange(I)I

    move-result p2

    iput p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    .line 73
    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->fill(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 1

    .line 161
    iget p3, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    add-int/2addr p3, p1

    .line 162
    invoke-direct {p0, p3}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->makeScrollOffsetWithinRange(I)I

    move-result v0

    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    .line 163
    invoke-virtual {p0, p2}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->fill(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 164
    iget p2, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    sub-int/2addr p2, p3

    add-int/2addr p2, p1

    return p2
.end method

.method public scrollToPosition(I)V
    .locals 1

    if-lez p1, :cond_0

    .line 176
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemCount:I

    if-ge p1, v0, :cond_0

    .line 177
    iget v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mItemViewWidth:I

    invoke-virtual {p0, p1}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->convert2LayoutPosition(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    mul-int v0, v0, p1

    iput v0, p0, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->mScrollOffset:I

    .line 178
    invoke-virtual {p0}, Lcom/dingmouren/layoutmanagergroup/skidright/SkidRightLayoutManager;->requestLayout()V

    :cond_0
    return-void
.end method
