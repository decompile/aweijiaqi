.class public Lcom/iwodong/unityplugin/PhoneNetworkState;
.super Ljava/lang/Object;
.source "PhoneNetworkState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iwodong/unityplugin/PhoneNetworkState$WIFIReceiver;
    }
.end annotation


# static fields
.field private static final LTE_MAX:I = 0x0

.field private static final LTE_MIN:I = -0x5a

.field private static final LTE_SIGNAL_STRENGTH:Ljava/lang/String; = "getLteSignalStrength"

.field public static final NETWORK_CLASS_2_G:I = 0x1

.field public static final NETWORK_CLASS_3_G:I = 0x2

.field public static final NETWORK_CLASS_4_G:I = 0x3

.field public static final NETWORK_CLASS_UNKNOWN:I = 0x0

.field public static final NETWORK_CLASS_WIFI:I = 0x4


# instance fields
.field private _context:Landroid/content/Context;

.field private _networkClass:I

.field private _signalStrength:Landroid/telephony/SignalStrength;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 64
    iput-object v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_networkClass:I

    .line 76
    iput-object p1, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    .line 77
    invoke-direct {p0}, Lcom/iwodong/unityplugin/PhoneNetworkState;->getNetworkClass()I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_networkClass:I

    .line 84
    iget-object p1, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    .line 87
    new-instance v0, Lcom/iwodong/unityplugin/PhoneNetworkState$1;

    invoke-direct {v0, p0}, Lcom/iwodong/unityplugin/PhoneNetworkState$1;-><init>(Lcom/iwodong/unityplugin/PhoneNetworkState;)V

    const/16 v1, 0x100

    .line 97
    invoke-virtual {p1, v0, v1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method static synthetic access$0(Lcom/iwodong/unityplugin/PhoneNetworkState;Landroid/telephony/SignalStrength;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_signalStrength:Landroid/telephony/SignalStrength;

    return-void
.end method

.method private getNetworkClass()I
    .locals 4

    .line 161
    iget-object v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "connectivity"

    .line 165
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 164
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 166
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 168
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x4

    return v0

    :cond_1
    if-nez v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const-string v3, "phone"

    .line 178
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 177
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 180
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    const/4 v0, 0x3

    return v0

    :pswitch_1
    const/4 v0, 0x2

    return v0

    :pswitch_2
    return v2

    :cond_2
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getLTEsignalStrength(I)I
    .locals 7

    const/4 v0, 0x0

    .line 110
    :try_start_0
    const-class v1, Landroid/telephony/SignalStrength;

    invoke-virtual {v1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 111
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v2, :cond_0

    goto :goto_1

    :cond_0
    aget-object v4, v1, v3

    .line 113
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "getLteSignalStrength"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 115
    iget-object v1, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_signalStrength:Landroid/telephony/SignalStrength;

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, -0x5a

    if-gt v1, v2, :cond_1

    return v0

    :cond_1
    if-ltz v1, :cond_2

    add-int/lit8 p1, p1, -0x1

    return p1

    :cond_2
    sub-int/2addr v1, v2

    const/16 v2, 0x5a

    add-int/lit8 p1, p1, -0x1

    .line 124
    div-int/2addr v2, p1

    div-int/2addr v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 131
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return v0
.end method

.method public getWIFILevel(I)I
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/iwodong/unityplugin/PhoneNetworkState;->getNetworkClass()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    return v1

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/iwodong/unityplugin/PhoneNetworkState;->_context:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 153
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    invoke-static {v0, p1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result p1

    return p1
.end method
