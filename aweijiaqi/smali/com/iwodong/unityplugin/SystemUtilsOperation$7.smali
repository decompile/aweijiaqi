.class Lcom/iwodong/unityplugin/SystemUtilsOperation$7;
.super Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;
.source "SystemUtilsOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/iwodong/unityplugin/SystemUtilsOperation;->pickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$unityGameObjectName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 912
    iput-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;->val$unityGameObjectName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(ILjava/lang/String;)V
    .locals 2

    .line 942
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "error"

    .line 944
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "msg"

    .line 945
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 948
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 951
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 952
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "onFailure"

    invoke-static {p1, v0, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSuccess(Ljava/io/File;)V
    .locals 4

    .line 916
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "filePath"

    const-string v2, "result"

    if-eqz p1, :cond_0

    :try_start_0
    const-string v3, "success"

    .line 919
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 920
    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 923
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :cond_0
    :try_start_1
    const-string p1, "failure"

    .line 927
    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, ""

    .line 928
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception p1

    .line 931
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 933
    :goto_0
    sget-object p1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    const/4 v1, 0x0

    const-string v2, "\u4e34\u65f6\u6587\u4ef6\u751f\u6210\u9519\u8bef"

    invoke-static {p1, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 936
    :goto_1
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 937
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSuccess"

    invoke-static {p1, v1, v0}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
