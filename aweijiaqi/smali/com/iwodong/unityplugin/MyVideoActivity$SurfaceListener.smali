.class Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;
.super Ljava/lang/Object;
.source "MyVideoActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/iwodong/unityplugin/MyVideoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SurfaceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/iwodong/unityplugin/MyVideoActivity;


# direct methods
.method private constructor <init>(Lcom/iwodong/unityplugin/MyVideoActivity;)V
    .locals 0

    .line 344
    iput-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/iwodong/unityplugin/MyVideoActivity;Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;)V
    .locals 0

    .line 344
    invoke-direct {p0, p1}, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;-><init>(Lcom/iwodong/unityplugin/MyVideoActivity;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 371
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    iget p1, p1, Lcom/iwodong/unityplugin/MyVideoActivity;->_lastPosition:I

    if-lez p1, :cond_0

    .line 375
    :try_start_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    invoke-static {p1}, Lcom/iwodong/unityplugin/MyVideoActivity;->access$0(Lcom/iwodong/unityplugin/MyVideoActivity;)V

    .line 376
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    invoke-static {p1}, Lcom/iwodong/unityplugin/MyVideoActivity;->access$1(Lcom/iwodong/unityplugin/MyVideoActivity;)Landroid/media/MediaPlayer;

    move-result-object p1

    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    iget v0, v0, Lcom/iwodong/unityplugin/MyVideoActivity;->_lastPosition:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 377
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    const/4 v0, 0x0

    iput v0, p1, Lcom/iwodong/unityplugin/MyVideoActivity;->_lastPosition:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 382
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 388
    :cond_0
    :try_start_1
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;->this$0:Lcom/iwodong/unityplugin/MyVideoActivity;

    invoke-static {p1}, Lcom/iwodong/unityplugin/MyVideoActivity;->access$0(Lcom/iwodong/unityplugin/MyVideoActivity;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception p1

    .line 391
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method
