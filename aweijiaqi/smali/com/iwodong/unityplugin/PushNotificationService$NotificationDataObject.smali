.class Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;
.super Ljava/lang/Object;
.source "PushNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/iwodong/unityplugin/PushNotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotificationDataObject"
.end annotation


# instance fields
.field public dd:I

.field public hour:I

.field public id:I

.field public minute:I

.field public mm:I

.field public open:Z

.field public text:Ljava/lang/String;

.field final synthetic this$0:Lcom/iwodong/unityplugin/PushNotificationService;

.field public title:Ljava/lang/String;

.field public used:Z

.field public week:I

.field public yy:I


# direct methods
.method constructor <init>(Lcom/iwodong/unityplugin/PushNotificationService;)V
    .locals 1

    .line 59
    iput-object p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->this$0:Lcom/iwodong/unityplugin/PushNotificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 62
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->id:I

    .line 64
    iput-boolean p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->open:Z

    .line 66
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->yy:I

    .line 68
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->mm:I

    .line 70
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->dd:I

    .line 72
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->week:I

    .line 74
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->hour:I

    .line 76
    iput p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->minute:I

    const-string v0, ""

    .line 78
    iput-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->title:Ljava/lang/String;

    .line 80
    iput-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->text:Ljava/lang/String;

    .line 82
    iput-boolean p1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->used:Z

    return-void
.end method


# virtual methods
.method public fire()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .line 87
    iget-boolean v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->open:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->used:Z

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 89
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    .line 90
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    .line 91
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    .line 92
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xa

    .line 93
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x7

    .line 94
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0xc

    .line 95
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 97
    iget v6, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->yy:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    if-eq v6, v1, :cond_1

    return-void

    .line 98
    :cond_1
    iget v1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->mm:I

    if-eq v1, v7, :cond_2

    if-eq v1, v2, :cond_2

    return-void

    .line 99
    :cond_2
    iget v1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->dd:I

    if-eq v1, v7, :cond_3

    if-eq v1, v3, :cond_3

    return-void

    .line 100
    :cond_3
    iget v1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->week:I

    if-eq v1, v7, :cond_4

    if-eq v1, v5, :cond_4

    return-void

    .line 101
    :cond_4
    iget v1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->hour:I

    if-eq v1, v7, :cond_5

    if-eq v1, v4, :cond_5

    return-void

    .line 102
    :cond_5
    iget v1, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->minute:I

    if-eq v1, v7, :cond_6

    if-eq v1, v0, :cond_6

    return-void

    .line 106
    :cond_6
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_7

    return-void

    .line 108
    :cond_7
    iget-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->this$0:Lcom/iwodong/unityplugin/PushNotificationService;

    invoke-virtual {v0}, Lcom/iwodong/unityplugin/PushNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-nez v0, :cond_8

    return-void

    .line 112
    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    .line 113
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    sget-object v2, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    .line 115
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 116
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    iget v2, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->id:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_9

    :cond_9
    :goto_0
    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 144
    iput-boolean v0, p0, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->used:Z

    return-void
.end method
