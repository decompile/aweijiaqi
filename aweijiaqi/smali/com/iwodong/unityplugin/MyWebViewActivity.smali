.class public Lcom/iwodong/unityplugin/MyWebViewActivity;
.super Landroid/app/Activity;
.source "MyWebViewActivity.java"


# instance fields
.field _gameOjectCallback:Ljava/lang/String;

.field _pgDialog:Landroid/app/ProgressDialog;

.field private _webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    const-string v1, ""

    .line 38
    iput-object v1, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_gameOjectCallback:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_pgDialog:Landroid/app/ProgressDialog;

    return-void
.end method


# virtual methods
.method public loadUrl(Ljava/lang/String;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    const/4 p1, 0x0

    const-string v0, "\u9875\u9762\u52a0\u8f7d\u4e2d\uff0c\u8bf7\u7a0d\u540e.."

    .line 143
    invoke-static {p0, p1, v0}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_pgDialog:Landroid/app/ProgressDialog;

    .line 144
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->reload()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 52
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tbVisible"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 53
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "goName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_gameOjectCallback:Ljava/lang/String;

    const/4 v1, 0x1

    const-string v2, ""

    if-ne p1, v2, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "Error 101: URL\u9519\u8bef"

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 59
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->finish()V

    return-void

    :cond_0
    const-string v2, "wd_buildin_webview"

    const-string v3, "layout"

    .line 64
    invoke-static {v2, v3}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "Error 102: \u52a0\u8f7d\u6d4f\u89c8\u5668\u7a97\u53e3\u9519\u8bef"

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 68
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->finish()V

    return-void

    .line 71
    :cond_1
    invoke-virtual {p0, v2}, Lcom/iwodong/unityplugin/MyWebViewActivity;->setContentView(I)V

    .line 74
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    const-string v2, "id"

    const-string v3, "toolBar"

    .line 77
    invoke-static {v3, v2}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 78
    invoke-virtual {p0, v3}, Lcom/iwodong/unityplugin/MyWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    .line 82
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    const-string v0, "webView"

    .line 86
    invoke-static {v0, v2}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 87
    invoke-virtual {p0, v0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    if-nez v0, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "Error 102: \u6d4f\u89c8\u5668\u52a0\u8f7d\u5f02\u5e38"

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 91
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->finish()V

    return-void

    .line 95
    :cond_3
    new-instance v3, Lcom/iwodong/unityplugin/MyWebViewActivity$1;

    invoke-direct {v3, p0}, Lcom/iwodong/unityplugin/MyWebViewActivity$1;-><init>(Lcom/iwodong/unityplugin/MyWebViewActivity;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 114
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const/4 v1, 0x2

    .line 118
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    const-string v0, "webViewback"

    .line 121
    invoke-static {v0, v2}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 122
    invoke-virtual {p0, v0}, Lcom/iwodong/unityplugin/MyWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 123
    new-instance v1, Lcom/iwodong/unityplugin/MyWebViewActivity$2;

    invoke-direct {v1, p0}, Lcom/iwodong/unityplugin/MyWebViewActivity$2;-><init>(Lcom/iwodong/unityplugin/MyWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/MyWebViewActivity;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 152
    iget-object v1, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_gameOjectCallback:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v2, "onWebViewActivityDestroyed"

    invoke-static {v1, v2, v0}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    .line 154
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_webView:Landroid/webkit/WebView;

    .line 155
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyWebViewActivity;->_pgDialog:Landroid/app/ProgressDialog;

    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
