.class public abstract Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;
.super Ljava/lang/Object;
.source "AsynchronousHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/iwodong/unityplugin/AsynchronousHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ProgressHandler"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onFailure(ILjava/lang/String;)V
.end method

.method public abstract onFinish()V
.end method

.method public abstract onProgress(JJ)V
.end method

.method public abstract onRetry()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onSuccess(ILjava/io/File;Ljava/lang/String;)V
.end method
