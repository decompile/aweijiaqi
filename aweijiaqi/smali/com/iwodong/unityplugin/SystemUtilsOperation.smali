.class public Lcom/iwodong/unityplugin/SystemUtilsOperation;
.super Ljava/lang/Object;
.source "SystemUtilsOperation.java"


# static fields
.field public static PNS_ACTION:Ljava/lang/String; = "com.iwodong.unityplugin.pns"

.field public static _activityUnity:Landroid/app/Activity; = null

.field static _apkFileName:Ljava/lang/String; = ""

.field public static _assetManager:Landroid/content/res/AssetManager; = null

.field public static _batteryChangedReceiver:Lcom/iwodong/unityplugin/BatteryChangedReceiver; = null

.field private static _clipBoardMgr:Landroid/content/ClipboardManager; = null

.field public static _contextUnity:Landroid/content/Context; = null

.field public static _phoneNetworkState:Lcom/iwodong/unityplugin/PhoneNetworkState; = null

.field public static _soundEffectPlayer:Lcom/iwodong/unityplugin/SoundEffect; = null

.field public static _strLog:Ljava/lang/String; = ""

.field private static appIdsUpdater:Lcom/iwodong/unityplugin/MiitHelper$AppIdsUpdater; = null

.field static oaid:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 118
    new-instance v0, Lcom/iwodong/unityplugin/SystemUtilsOperation$1;

    invoke-direct {v0}, Lcom/iwodong/unityplugin/SystemUtilsOperation$1;-><init>()V

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->appIdsUpdater:Lcom/iwodong/unityplugin/MiitHelper$AppIdsUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetAllID()Ljava/lang/String;
    .locals 1

    .line 1132
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->oaid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->oaid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1133
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->oaid:Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public static GetAndroidID()Ljava/lang/String;
    .locals 2

    .line 1140
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static GetAndroidSDKInt()I
    .locals 1

    .line 1082
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public static GetAssetBundle(Ljava/lang/String;)[B
    .locals 1

    .line 263
    :try_start_0
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_assetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 265
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string v0, "unity"

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    .line 267
    :goto_0
    invoke-static {p0}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->readtextbytes(Ljava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static GetOAID()V
    .locals 3

    .line 1086
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    new-instance v1, Lcom/iwodong/unityplugin/SystemUtilsOperation$8;

    invoke-direct {v1}, Lcom/iwodong/unityplugin/SystemUtilsOperation$8;-><init>()V

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Lcom/bun/miitmdid/core/MdidSdkHelper;->InitSdk(Landroid/content/Context;ZLcom/bun/miitmdid/interfaces/IIdentifierListener;)I

    move-result v0

    const-string v1, "ids:"

    const v2, 0xf63e4

    if-ne v0, v2, :cond_0

    const-string v0, "\u83b7\u53d6OAID\uff1a\u4e0d\u652f\u6301\u7684\u8bbe\u5907"

    .line 1117
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const v2, 0xf63e5

    if-ne v0, v2, :cond_1

    const-string v0, "\u83b7\u53d6OAID\uff1a\u52a0\u8f7d\u914d\u7f6e\u6587\u4ef6\u51fa\u9519"

    .line 1119
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const v2, 0xf63e3

    if-ne v0, v2, :cond_2

    const-string v0, "\u83b7\u53d6OAID\uff1a\u4e0d\u652f\u6301\u7684\u8bbe\u5907\u5382\u5546"

    .line 1121
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const v2, 0xf63e6

    if-ne v0, v2, :cond_3

    const-string v0, "\u83b7\u53d6OAID\uff1a\u83b7\u53d6\u63a5\u53e3\u662f\u5f02\u6b65\u7684\uff0c\u7ed3\u679c\u4f1a\u5728\u56de\u8c03\u4e2d\u8fd4\u56de\uff0c\u56de\u8c03\u6267\u884c\u7684\u56de\u8c03\u53ef\u80fd\u5728\u5de5\u4f5c\u7ebf\u7a0b"

    .line 1123
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const v2, 0xf63e7

    if-ne v0, v2, :cond_4

    const-string v0, "\u83b7\u53d6OAID\uff1a\u53cd\u5c04\u8c03\u7528\u51fa\u9519"

    .line 1125
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1128
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "\u83b7\u53d6OAID\uff1a\u83b7\u53d6\u6210\u529f"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/iwodong/unityplugin/SystemUtilsOperation;->oaid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static GoToOtherApp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1065
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1069
    :cond_0
    invoke-static {p0}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1070
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 1071
    new-instance v0, Landroid/content/ComponentName;

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1073
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1075
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 1076
    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p1, v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1077
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method public static PlaySound(IF)V
    .locals 1

    .line 1029
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_soundEffectPlayer:Lcom/iwodong/unityplugin/SoundEffect;

    invoke-virtual {v0, p0, p1}, Lcom/iwodong/unityplugin/SoundEffect;->playSound(IF)V

    return-void
.end method

.method public static asyncDownloadFile(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    .line 630
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1 asyncDownloadFile begin url:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "asyncdownload"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, ".amr"

    .line 643
    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v4, v0, 0x1

    const-string v0, ".apk"

    .line 646
    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    sput-object p3, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_apkFileName:Ljava/lang/String;

    :cond_1
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    .line 649
    invoke-static/range {v1 .. v7}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->asyncDownloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static asyncDownloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 8

    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "asyncDownloadFile begin url:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "asyncdownload"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-long v0, p5

    .line 535
    new-instance v7, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;

    invoke-direct {v7, p6, v0, v1}, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;-><init>(Ljava/lang/String;J)V

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    .line 534
    invoke-static/range {v2 .. v7}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->downloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    return-void
.end method

.method public static asyncUploadFile(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 759
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 762
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 763
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p3

    if-nez p3, :cond_1

    return-void

    :cond_1
    const/4 p3, 0x0

    .line 769
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p3, v1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 772
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 776
    :goto_0
    new-instance v0, Lcom/iwodong/unityplugin/SystemUtilsOperation$6;

    invoke-direct {v0, p4}, Lcom/iwodong/unityplugin/SystemUtilsOperation$6;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p2, p1, p3, v0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->upload(Ljava/lang/String;Ljava/lang/String;ZLjava/io/InputStream;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    if-eqz p3, :cond_2

    .line 836
    :try_start_1
    invoke-virtual {p3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    .line 841
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    :cond_2
    :goto_1
    return-void
.end method

.method public static asyncUploadFile(Ljava/lang/String;ZLjava/lang/String;[BLjava/lang/String;)V
    .locals 1

    .line 668
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 672
    :cond_0
    new-instance v0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;

    invoke-direct {v0, p4}, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p2, p1, p3, v0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->upload(Ljava/lang/String;Ljava/lang/String;Z[BLcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    return-void
.end method

.method public static copyString2Clipboard(Ljava/lang/String;)V
    .locals 1

    const-string v0, "simple text"

    .line 160
    invoke-static {v0, p0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object p0

    .line 161
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_clipBoardMgr:Landroid/content/ClipboardManager;

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0, p0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    :cond_0
    return-void
.end method

.method public static get4GLevel(I)I
    .locals 1

    .line 1022
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_phoneNetworkState:Lcom/iwodong/unityplugin/PhoneNetworkState;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 1025
    :cond_0
    invoke-virtual {v0, p0}, Lcom/iwodong/unityplugin/PhoneNetworkState;->getLTEsignalStrength(I)I

    move-result p0

    return p0
.end method

.method private static getAppOpenIntentByPackageName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .line 322
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 328
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 329
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    invoke-virtual {v2, p1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x10200000

    .line 331
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 p1, 0x1

    .line 333
    invoke-virtual {v0, v2, p1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    .line 334
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_1

    move-object p1, v1

    goto :goto_1

    .line 335
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 336
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 337
    iget-object p1, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object p1, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 341
    :goto_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object v1

    .line 344
    :cond_2
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p0, p1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getBatteryRemainingPower()F
    .locals 1

    .line 992
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_batteryChangedReceiver:Lcom/iwodong/unityplugin/BatteryChangedReceiver;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 995
    :cond_0
    invoke-virtual {v0}, Lcom/iwodong/unityplugin/BatteryChangedReceiver;->getRemainingPower()F

    move-result v0

    return v0
.end method

.method public static getLog()Ljava/lang/String;
    .locals 1

    .line 191
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_strLog:Ljava/lang/String;

    return-object v0
.end method

.method public static getMAC()Ljava/lang/String;
    .locals 4

    .line 1297
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    const-string v2, "getMAC: "

    if-ge v0, v1, :cond_0

    .line 1298
    invoke-static {}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getMacDefault()Ljava/lang/String;

    move-result-object v0

    .line 1299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "getmac 0 --------------"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1300
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v3, :cond_1

    .line 1301
    invoke-static {}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 1302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "getmac 1 --------------"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1303
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_2

    .line 1304
    invoke-static {}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getMacFromHardware()Ljava/lang/String;

    move-result-object v0

    .line 1305
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "getmac 2 --------------"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "02:00:00:00:00:00"

    .line 1307
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "getmac 3 --------------"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private static getMacAddress()Ljava/lang/String;
    .locals 4

    .line 1250
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    new-instance v2, Ljava/io/File;

    const-string v3, "/sys/class/net/wlan0/address"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1252
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const-string v0, "02:00:00:00:00:00"

    :goto_0
    return-object v0
.end method

.method private static getMacDefault()Ljava/lang/String;
    .locals 3

    .line 1219
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    const-string v1, "02:00:00:00:00:00"

    if-nez v0, :cond_0

    return-object v1

    .line 1223
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "wifi"

    .line 1224
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1223
    check-cast v0, Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    const/4 v1, 0x0

    .line 1230
    :try_start_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    return-object v1

    .line 1236
    :cond_2
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 1237
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1238
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method private static getMacFromHardware()Ljava/lang/String;
    .locals 8

    .line 1264
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1265
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/NetworkInterface;

    .line 1266
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "wlan0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 1268
    :cond_1
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getHardwareAddress()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    const-string v0, ""

    return-object v0

    .line 1273
    :cond_2
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1274
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    const/4 v5, 0x1

    if-lt v4, v2, :cond_4

    .line 1278
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 1279
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sub-int/2addr v0, v5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1281
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1274
    :cond_4
    aget-byte v6, v0, v4

    const-string v7, "%02X:"

    new-array v5, v5, [Ljava/lang/Object;

    .line 1275
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v7, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1284
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    const-string v0, "02:00:00:00:00:00"

    return-object v0
.end method

.method public static getResId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .line 274
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, p1, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static getStringFromClipboard()Ljava/lang/String;
    .locals 6

    .line 169
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_clipBoardMgr:Landroid/content/ClipboardManager;

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 171
    :cond_0
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    .line 174
    :cond_1
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_clipBoardMgr:Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v2, :cond_2

    return-object v1

    .line 179
    :cond_2
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 180
    sget-object v5, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 181
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static getWifiLevel(I)I
    .locals 1

    .line 1008
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_phoneNetworkState:Lcom/iwodong/unityplugin/PhoneNetworkState;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 1011
    :cond_0
    invoke-virtual {v0, p0}, Lcom/iwodong/unityplugin/PhoneNetworkState;->getWIFILevel(I)I

    move-result p0

    return p0
.end method

.method private static gotoAppDetailAct()V
    .locals 3

    .line 488
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "\u4e3a\u4e86\u6b63\u5e38\u5347\u7ea7\u7248\u672c\uff0c\u8bf7\u70b9\u51fb\u8bbe\u7f6e\u6309\u94ae\uff0c\u5141\u8bb8\u5b89\u88c5\u672a\u77e5\u6765\u6e90\u5e94\u7528\uff0c\u672c\u529f\u80fd\u53ea\u9650\u7528\u4e8e\u7248\u672c\u5347\u7ea7"

    .line 489
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "\u63d0\u793a"

    .line 490
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 491
    new-instance v1, Lcom/iwodong/unityplugin/SystemUtilsOperation$2;

    invoke-direct {v1}, Lcom/iwodong/unityplugin/SystemUtilsOperation$2;-><init>()V

    const-string v2, "\u8bbe\u7f6e"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 504
    new-instance v1, Lcom/iwodong/unityplugin/SystemUtilsOperation$3;

    invoke-direct {v1}, Lcom/iwodong/unityplugin/SystemUtilsOperation$3;-><init>()V

    const-string v2, "\u53d6\u6d88"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 510
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static init()V
    .locals 3

    const-string v0, "asyncdownload"

    const-string v1, "SystemUtilsOperation init:"

    .line 128
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    sget-object v0, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    .line 132
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_assetManager:Landroid/content/res/AssetManager;

    .line 141
    new-instance v0, Lcom/iwodong/unityplugin/MiitHelper;

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->appIdsUpdater:Lcom/iwodong/unityplugin/MiitHelper$AppIdsUpdater;

    invoke-direct {v0, v1}, Lcom/iwodong/unityplugin/MiitHelper;-><init>(Lcom/iwodong/unityplugin/MiitHelper$AppIdsUpdater;)V

    .line 142
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/iwodong/unityplugin/MiitHelper;->getDeviceIds(Landroid/content/Context;)V

    .line 145
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-eqz v0, :cond_1

    const-string v1, "clipboard"

    .line 147
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_clipBoardMgr:Landroid/content/ClipboardManager;

    .line 148
    new-instance v0, Lcom/iwodong/unityplugin/PhoneNetworkState;

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/iwodong/unityplugin/PhoneNetworkState;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_phoneNetworkState:Lcom/iwodong/unityplugin/PhoneNetworkState;

    .line 151
    :cond_1
    new-instance v0, Lcom/iwodong/unityplugin/SoundEffect;

    const/16 v1, 0x64

    sget-object v2, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/iwodong/unityplugin/SoundEffect;-><init>(ILandroid/app/Activity;)V

    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_soundEffectPlayer:Lcom/iwodong/unityplugin/SoundEffect;

    return-void
.end method

.method public static initAsyncHttpClient(Ljava/lang/String;)V
    .locals 1

    .line 383
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-static {v0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->init(Landroid/content/Context;)Z

    .line 384
    sput-object p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->BASE_URL:Ljava/lang/String;

    return-void
.end method

.method private static installApk(Ljava/lang/String;)Z
    .locals 6

    .line 415
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/high16 v1, 0x10000000

    .line 416
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    .line 417
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v2, "\u3010\u9519\u8bef\u3011\u6587\u4ef6\u8def\u5f84\u4e0d\u5b58\u5728: "

    const/4 v3, 0x0

    const/16 v4, 0x18

    if-ge v1, v4, :cond_1

    .line 421
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 422
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 423
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return v3

    .line 426
    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    .line 430
    :cond_1
    new-instance v1, Ljava/io/File;

    sget-object v4, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    sget-object v5, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_apkFileName:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 436
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return v3

    .line 441
    :cond_2
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    .line 442
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p0, ".myfileprovider"

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 443
    sget-object v2, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-static {v2, p0, v1}, Lcom/iwodong/unityplugin/MyFileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    const/4 v1, 0x3

    .line 446
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :goto_0
    const-string v1, "application/vnd.android.package-archive"

    .line 451
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 p0, 0x1

    return p0
.end method

.method private static isAppInstalled(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    .line 1051
    :try_start_0
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const/4 v1, 0x0

    .line 1054
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object p0, v1

    :goto_0
    if-nez p0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public static loadSound(Ljava/lang/String;)I
    .locals 1

    .line 1033
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_soundEffectPlayer:Lcom/iwodong/unityplugin/SoundEffect;

    invoke-virtual {v0, p0}, Lcom/iwodong/unityplugin/SoundEffect;->loadSound(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static openPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 362
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 365
    :cond_0
    invoke-static {p0, p1}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getAppOpenIntentByPackageName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 367
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "extraInfo"

    .line 368
    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 370
    sget-object p1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 p0, 0x1

    return p0

    :cond_1
    return v1
.end method

.method public static openWebView(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .line 303
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 306
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const-class v2, Lcom/iwodong/unityplugin/MyWebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 307
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "url"

    .line 308
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "tbVisible"

    .line 309
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "goName"

    .line 310
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static pickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILjava/lang/String;)V
    .locals 7

    .line 912
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    new-instance v6, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;

    invoke-direct {v6, p5}, Lcom/iwodong/unityplugin/SystemUtilsOperation$7;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v6}, Lcom/iwodong/unityplugin/PhotoPicker;->pick(Landroid/app/Activity;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;)V

    return-void
.end method

.method public static pickPhotoFromAlbum(Ljava/lang/String;ZIILjava/lang/String;)V
    .locals 7

    .line 861
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    .line 862
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "jpg"

    .line 863
    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "jpeg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 864
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    :cond_1
    move-object v2, v0

    .line 866
    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->eAlbums:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-static/range {v1 .. v6}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->pickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILjava/lang/String;)V

    return-void
.end method

.method public static pickPhotoFromCamera(Ljava/lang/String;ZIILjava/lang/String;)V
    .locals 7

    .line 885
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    .line 886
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "jpg"

    .line 887
    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "jpeg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 888
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    :cond_1
    move-object v2, v0

    .line 890
    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->eCamera:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-static/range {v1 .. v6}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->pickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILjava/lang/String;)V

    return-void
.end method

.method private static readtextbytes(Ljava/io/InputStream;)[B
    .locals 4

    .line 243
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 245
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v1

    new-array v1, v1, [B

    .line 247
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 250
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 251
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    .line 248
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 254
    :catch_0
    :goto_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method public static registerBatteryChangeReceiver()V
    .locals 3

    .line 963
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 966
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    .line 967
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 969
    new-instance v1, Lcom/iwodong/unityplugin/BatteryChangedReceiver;

    invoke-direct {v1}, Lcom/iwodong/unityplugin/BatteryChangedReceiver;-><init>()V

    sput-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_batteryChangedReceiver:Lcom/iwodong/unityplugin/BatteryChangedReceiver;

    .line 970
    sget-object v2, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private static requestInstallPermission()V
    .locals 3

    .line 474
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    .line 476
    sget-object v0, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    const-string v1, "android.permission.REQUEST_INSTALL_PACKAGES"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    sget-object v0, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    .line 479
    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x536

    .line 478
    invoke-static {v0, v1, v2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0

    .line 481
    :cond_0
    invoke-static {}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->gotoAppDetailAct()V

    :cond_1
    :goto_0
    return-void
.end method

.method public static savePicToGallery(Ljava/lang/String;)V
    .locals 6

    .line 1170
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 1172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    .line 1175
    filled-new-array {v2, v1}, [Ljava/lang/String;

    move-result-object v2

    .line 1177
    sget-object v3, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    invoke-static {v3, v1}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1181
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 1184
    :cond_0
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v2, "Xiaomi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/DCIM/Camera/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1188
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/DCIM/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1190
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1191
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1192
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1196
    :cond_2
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1198
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x5a

    invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1200
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1201
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 1203
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v0, v3}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1214
    :catch_0
    :cond_3
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static startInstallAPK(Ljava/lang/String;)Z
    .locals 3

    .line 394
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_activityUnity:Landroid/app/Activity;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 397
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v0, v2, :cond_1

    .line 401
    :try_start_0
    invoke-static {}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->requestInstallPermission()V

    .line 402
    invoke-static {p0}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->installApk(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 404
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :goto_0
    return v1

    .line 410
    :cond_1
    invoke-static {p0}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->installApk(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static startPlayVideo(Ljava/lang/String;IIIIIILjava/lang/String;)V
    .locals 3

    .line 285
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 288
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const-class v2, Lcom/iwodong/unityplugin/MyVideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 289
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "video"

    .line 290
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "videoClipCount"

    .line 291
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "videoWidth"

    .line 292
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "videoHeight"

    .line 293
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "screenWidth"

    .line 294
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "screenHeight"

    .line 295
    invoke-virtual {v0, p0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "mode"

    .line 296
    invoke-virtual {v0, p0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p0, "goName"

    .line 297
    invoke-virtual {v0, p0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    sget-object p0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static unloadSound(I)V
    .locals 1

    .line 1037
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_soundEffectPlayer:Lcom/iwodong/unityplugin/SoundEffect;

    invoke-virtual {v0, p0}, Lcom/iwodong/unityplugin/SoundEffect;->unloadSound(I)V

    return-void
.end method

.method public static unregisterBatteryChangeReceiver()V
    .locals 2

    .line 977
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 980
    :cond_0
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_batteryChangedReceiver:Lcom/iwodong/unityplugin/BatteryChangedReceiver;

    if-eqz v1, :cond_1

    .line 981
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    const/4 v0, 0x0

    .line 983
    sput-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_batteryChangedReceiver:Lcom/iwodong/unityplugin/BatteryChangedReceiver;

    return-void
.end method


# virtual methods
.method public GetNetType()I
    .locals 5

    .line 1145
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1146
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    const-string v1, "networkInfo"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string v0, "networkInfo = null"

    .line 1148
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    .line 1151
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 1152
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "nType = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const/4 v2, 0x2

    :cond_2
    :goto_0
    return v2
.end method

.method public isPushServiceRunning()Z
    .locals 4

    .line 198
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "activity"

    .line 201
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const v2, 0x7fffffff

    .line 202
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 203
    iget-object v2, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.iwodong.unityplugin.PushNotificationService"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    return v0
.end method

.method public startPushNotificationService(Ljava/lang/String;)V
    .locals 2

    .line 215
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->isPushServiceRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 218
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 219
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 220
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->PNS_ACTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "jsText"

    .line 222
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    sget-object p1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    return-void
.end method

.method public stopPushNotificationService()V
    .locals 2

    .line 231
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 233
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->PNS_ACTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    sget-object v1, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_0
    return-void
.end method
