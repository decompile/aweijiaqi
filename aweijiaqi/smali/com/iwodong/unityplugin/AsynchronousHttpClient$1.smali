.class Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;
.super Lcom/loopj/android/http/FileAsyncHttpResponseHandler;
.source "AsynchronousHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/iwodong/unityplugin/AsynchronousHttpClient;->downloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$destDir:Ljava/io/File;

.field private final synthetic val$destFileName:Ljava/lang/String;

.field private final synthetic val$handler:Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;


# direct methods
.method constructor <init>(Ljava/io/File;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    .line 105
    iput-object p2, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$handler:Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;

    iput-object p3, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destDir:Ljava/io/File;

    iput-object p4, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destFileName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/loopj/android/http/FileAsyncHttpResponseHandler;-><init>(Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lcz/msebera/android/httpclient/Header;Ljava/lang/Throwable;Ljava/io/File;)V
    .locals 0

    .line 109
    iget-object p2, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$handler:Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;

    if-eqz p2, :cond_0

    const-string p3, ""

    .line 110
    invoke-virtual {p2, p1, p3}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;->onFailure(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onProgress(JJ)V
    .locals 1

    .line 155
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/FileAsyncHttpResponseHandler;->onProgress(JJ)V

    .line 157
    iget-object v0, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$handler:Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;->onProgress(JJ)V

    :cond_0
    return-void
.end method

.method public onSuccess(I[Lcz/msebera/android/httpclient/Header;Ljava/io/File;)V
    .locals 2

    .line 119
    invoke-static {}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->access$0()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 121
    new-instance p2, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destDir:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p3, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_0

    .line 129
    :cond_0
    :try_start_0
    new-instance p2, Ljava/io/FileInputStream;

    invoke-direct {p2, p3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 130
    invoke-virtual {p2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    .line 131
    new-array v0, v0, [B

    .line 132
    invoke-virtual {p2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 133
    invoke-virtual {p2}, Ljava/io/FileInputStream;->close()V

    .line 134
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 136
    sget-object p2, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->CONTEXT:Landroid/content/Context;

    iget-object p3, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destFileName:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-virtual {p2, p3, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object p2

    .line 137
    invoke-virtual {p2, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 138
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    .line 140
    new-instance p2, Ljava/io/File;

    new-instance p3, Ljava/lang/StringBuilder;

    sget-object v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$destFileName:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 145
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p2, 0x0

    .line 149
    :goto_0
    iget-object p3, p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;->val$handler:Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;

    if-eqz p3, :cond_1

    const-string v0, ""

    .line 150
    invoke-virtual {p3, p1, p2, v0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;->onSuccess(ILjava/io/File;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
