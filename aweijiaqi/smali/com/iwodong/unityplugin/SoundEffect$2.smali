.class Lcom/iwodong/unityplugin/SoundEffect$2;
.super Ljava/lang/Object;
.source "SoundEffect.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/iwodong/unityplugin/SoundEffect;->playSound(IF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/iwodong/unityplugin/SoundEffect;

.field private final synthetic val$soundId:I

.field private final synthetic val$volume:F


# direct methods
.method constructor <init>(Lcom/iwodong/unityplugin/SoundEffect;IF)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->this$0:Lcom/iwodong/unityplugin/SoundEffect;

    iput p2, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->val$soundId:I

    iput p3, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->val$volume:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->this$0:Lcom/iwodong/unityplugin/SoundEffect;

    iget v1, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->val$soundId:I

    iget v2, p0, Lcom/iwodong/unityplugin/SoundEffect$2;->val$volume:F

    invoke-virtual {v0, v1, v2}, Lcom/iwodong/unityplugin/SoundEffect;->play(IF)V

    return-void
.end method
