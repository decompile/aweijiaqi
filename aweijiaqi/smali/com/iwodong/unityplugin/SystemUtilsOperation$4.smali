.class Lcom/iwodong/unityplugin/SystemUtilsOperation$4;
.super Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;
.source "SystemUtilsOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/iwodong/unityplugin/SystemUtilsOperation;->asyncDownloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$unityGameObjectName:Ljava/lang/String;

.field private final synthetic val$vibratorTime:J


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .line 535
    iput-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    iput-wide p2, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$vibratorTime:J

    invoke-direct {p0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(ILjava/lang/String;)V
    .locals 2

    .line 554
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "statusCode"

    .line 556
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "msg"

    .line 557
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 560
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 563
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 564
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "onFailure"

    invoke-static {p1, v0, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onFinish()V
    .locals 3

    .line 599
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    const-string v1, "onFinish"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onProgress(JJ)V
    .locals 2

    .line 539
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "currentSize"

    .line 541
    invoke-virtual {v0, v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string p1, "totalSize"

    .line 542
    invoke-virtual {v0, p1, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 545
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 548
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 549
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "onProgress"

    invoke-static {p1, p3, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onRetry()V
    .locals 3

    .line 605
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    const-string v1, "onRetry"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    .line 593
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    const-string v1, "onStart"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSuccess(ILjava/io/File;Ljava/lang/String;)V
    .locals 5

    const-string v0, "asyncdownload"

    const-string v1, "********* onSuccess"

    .line 569
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    iget-wide v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$vibratorTime:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 572
    sget-object v0, Lcom/iwodong/unityplugin/SystemUtilsOperation;->_contextUnity:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 573
    iget-wide v1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$vibratorTime:J

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 576
    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p2

    .line 577
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "statusCode"

    .line 579
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "filePath"

    .line 580
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "msg"

    .line 581
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 584
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 587
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 588
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$4;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "onSuccess"

    invoke-static {p1, p3, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
