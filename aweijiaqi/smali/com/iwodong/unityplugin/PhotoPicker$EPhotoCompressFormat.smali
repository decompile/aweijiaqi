.class public final enum Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;
.super Ljava/lang/Enum;
.source "PhotoPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/iwodong/unityplugin/PhotoPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EPhotoCompressFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

.field public static final enum eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

.field public static final enum ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 43
    new-instance v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    const/4 v1, 0x0

    const-string v2, "ePNG"

    invoke-direct {v0, v2, v1}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    .line 44
    new-instance v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    const/4 v2, 0x1

    const-string v3, "eJPEG"

    invoke-direct {v0, v3, v2}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    .line 41
    sget-object v4, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    aput-object v4, v3, v1

    aput-object v0, v3, v2

    sput-object v3, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ENUM$VALUES:[Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;
    .locals 1

    .line 1
    const-class v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    return-object p0
.end method

.method public static values()[Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;
    .locals 4

    .line 1
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ENUM$VALUES:[Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    array-length v1, v0

    new-array v2, v1, [Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
