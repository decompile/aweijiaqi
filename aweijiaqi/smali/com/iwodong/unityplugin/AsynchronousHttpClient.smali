.class public Lcom/iwodong/unityplugin/AsynchronousHttpClient;
.super Ljava/lang/Object;
.source "AsynchronousHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;
    }
.end annotation


# static fields
.field public static BASE_URL:Ljava/lang/String; = "http://ksyx.update.iwodong.com/"

.field public static CONTEXT:Landroid/content/Context; = null

.field private static s_Client:Lcom/loopj/android/http/AsyncHttpClient; = null

.field public static s_downloadDirectory:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    .line 277
    invoke-static {}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->isExternalStorageExist()Z

    move-result v0

    return v0
.end method

.method public static downloadFile(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
    .locals 2

    .line 91
    sget-object v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_Client:Lcom/loopj/android/http/AsyncHttpClient;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 94
    invoke-static {p0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->getAbsoluteUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :cond_1
    if-eqz p3, :cond_2

    .line 95
    new-instance p1, Ljava/lang/StringBuilder;

    sget-object p3, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object p3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 97
    :cond_2
    new-instance p1, Ljava/io/File;

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_3

    .line 99
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 102
    :cond_3
    new-instance p2, Ljava/io/File;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    new-instance p3, Ljava/lang/StringBuilder;

    const-string v0, "destDir: "

    invoke-direct {p3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "destDir"

    invoke-static {v0, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object p3, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_Client:Lcom/loopj/android/http/AsyncHttpClient;

    new-instance v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;

    invoke-direct {v0, p2, p5, p1, p4}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$1;-><init>(Ljava/io/File;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p3, p0, v0}, Lcom/loopj/android/http/AsyncHttpClient;->get(Ljava/lang/String;Lcom/loopj/android/http/ResponseHandlerInterface;)Lcom/loopj/android/http/RequestHandle;

    return-void
.end method

.method private static getAbsoluteUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->BASE_URL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static init(Landroid/content/Context;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 61
    :cond_0
    sput-object p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->CONTEXT:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/loopj/android/http/AsyncHttpClient;

    invoke-direct {v0}, Lcom/loopj/android/http/AsyncHttpClient;-><init>()V

    sput-object v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_Client:Lcom/loopj/android/http/AsyncHttpClient;

    .line 65
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    .line 66
    invoke-static {}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->isExternalStorageExist()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 68
    sget-object p0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {p0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    .line 71
    :cond_1
    new-instance p0, Ljava/io/File;

    sget-object v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_downloadDirectory:Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    :cond_2
    const/4 p0, 0x1

    return p0
.end method

.method private static isExternalStorageExist()Z
    .locals 2

    .line 279
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 280
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static upload(Ljava/lang/String;Ljava/lang/String;ZLjava/io/File;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 177
    new-instance v0, Lcom/loopj/android/http/RequestParams;

    invoke-direct {v0}, Lcom/loopj/android/http/RequestParams;-><init>()V

    :try_start_0
    const-string v1, "file"

    .line 180
    invoke-virtual {v0, v1, p3, p1}, Lcom/loopj/android/http/RequestParams;->put(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 185
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 188
    :goto_0
    invoke-static {p0, p2, v0, p4}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->uploadImpl(Ljava/lang/String;ZLcom/loopj/android/http/RequestParams;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    return-void
.end method

.method public static upload(Ljava/lang/String;Ljava/lang/String;ZLjava/io/InputStream;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
    .locals 2

    if-nez p3, :cond_0

    return-void

    .line 205
    :cond_0
    new-instance v0, Lcom/loopj/android/http/RequestParams;

    invoke-direct {v0}, Lcom/loopj/android/http/RequestParams;-><init>()V

    const-string v1, "file"

    .line 206
    invoke-virtual {v0, v1, p3, p1}, Lcom/loopj/android/http/RequestParams;->put(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V

    const-string p3, "fileName"

    .line 207
    invoke-virtual {v0, p3, p1}, Lcom/loopj/android/http/RequestParams;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-static {p0, p2, v0, p4}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->uploadImpl(Ljava/lang/String;ZLcom/loopj/android/http/RequestParams;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    return-void
.end method

.method public static upload(Ljava/lang/String;Ljava/lang/String;Z[BLcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
    .locals 2

    .line 222
    new-instance v0, Lcom/loopj/android/http/RequestParams;

    invoke-direct {v0}, Lcom/loopj/android/http/RequestParams;-><init>()V

    .line 223
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string p3, "file"

    invoke-virtual {v0, p3, v1, p1}, Lcom/loopj/android/http/RequestParams;->put(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V

    .line 224
    invoke-static {p0, p2, v0, p4}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->uploadImpl(Ljava/lang/String;ZLcom/loopj/android/http/RequestParams;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    return-void
.end method

.method private static uploadImpl(Ljava/lang/String;ZLcom/loopj/android/http/RequestParams;Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V
    .locals 1

    .line 237
    sget-object v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_Client:Lcom/loopj/android/http/AsyncHttpClient;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 240
    invoke-static {p0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->getAbsoluteUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 241
    :cond_1
    sget-object p1, Lcom/iwodong/unityplugin/AsynchronousHttpClient;->s_Client:Lcom/loopj/android/http/AsyncHttpClient;

    new-instance v0, Lcom/iwodong/unityplugin/AsynchronousHttpClient$2;

    invoke-direct {v0, p3}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$2;-><init>(Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;)V

    invoke-virtual {p1, p0, p2, v0}, Lcom/loopj/android/http/AsyncHttpClient;->post(Ljava/lang/String;Lcom/loopj/android/http/RequestParams;Lcom/loopj/android/http/ResponseHandlerInterface;)Lcom/loopj/android/http/RequestHandle;

    return-void
.end method
