.class public Lcom/iwodong/unityplugin/PhotoPicker;
.super Ljava/lang/Object;
.source "PhotoPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;,
        Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;,
        Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$iwodong$unityplugin$PhotoPicker$EPhotoResource:[I = null

.field public static final ACTIVITY_REQUEST_CODE_ALBUM:I = 0x1

.field public static final ACTIVITY_REQUEST_CODE_CAMERA:I = 0x2

.field public static final ACTIVITY_REQUEST_CODE_CROP:I = 0x3

.field private static START_ACTIVITY:Landroid/app/Activity; = null

.field private static s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat; = null

.field private static s_Crop:Z = false

.field private static s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler; = null

.field private static s_OutputX:I = 0x0

.field private static s_OutputY:I = 0x0

.field private static s_PhotoUri:Landroid/net/Uri; = null

.field private static s_TempFileNameWithoutExt:Ljava/lang/String; = "phototemp"


# direct methods
.method static synthetic $SWITCH_TABLE$com$iwodong$unityplugin$PhotoPicker$EPhotoResource()[I
    .locals 3

    .line 27
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->$SWITCH_TABLE$com$iwodong$unityplugin$PhotoPicker$EPhotoResource:[I

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->values()[Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->eAlbums:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;

    invoke-virtual {v1}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->eCamera:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;

    invoke-virtual {v1}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    sput-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->$SWITCH_TABLE$com$iwodong$unityplugin$PhotoPicker$EPhotoResource:[I

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static albumRequestBack(Landroid/content/Intent;)V
    .locals 2

    if-nez p0, :cond_1

    .line 310
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz p0, :cond_0

    const/4 v0, 0x2

    const-string v1, "Data picked from album is null"

    .line 311
    invoke-virtual {p0, v0, v1}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onFailure(ILjava/lang/String;)V

    :cond_0
    return-void

    .line 316
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p0

    .line 317
    sget-boolean v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Crop:Z

    if-eqz v0, :cond_2

    .line 318
    invoke-static {p0}, Lcom/iwodong/unityplugin/PhotoPicker;->crop(Landroid/net/Uri;)V

    goto :goto_1

    .line 321
    :cond_2
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 324
    :try_start_0
    invoke-static {v0, p0}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 327
    invoke-static {p0}, Lcom/iwodong/unityplugin/PhotoPicker;->saveBitmap(Landroid/graphics/Bitmap;)V

    .line 328
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 332
    :cond_3
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz p0, :cond_4

    .line 333
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    const/4 v0, 0x3

    const-string v1, "Data picked from camera is null"

    invoke-virtual {p0, v0, v1}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onFailure(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-void

    :catch_0
    move-exception p0

    .line 342
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 339
    invoke-virtual {p0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 346
    :goto_0
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz p0, :cond_5

    .line 347
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onSuccess(Ljava/io/File;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private static albumsPickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)V
    .locals 2

    .line 179
    new-instance p0, Landroid/content/Intent;

    const-string v0, "android.intent.action.GET_CONTENT"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "image/*"

    .line 180
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private static cameraPickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)V
    .locals 2

    .line 166
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    sput-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_PhotoUri:Landroid/net/Uri;

    .line 167
    new-instance p0, Landroid/content/Intent;

    const-string v0, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {p0, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_PhotoUri:Landroid/net/Uri;

    const-string v1, "output"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 169
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private static cameraRequestBack()V
    .locals 3

    .line 356
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 357
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 359
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz v0, :cond_0

    const/4 v1, 0x6

    const-string v2, "Data picked from camera is null"

    .line 360
    invoke-virtual {v0, v1, v2}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onFailure(ILjava/lang/String;)V

    :cond_0
    return-void

    .line 365
    :cond_1
    sget-boolean v1, Lcom/iwodong/unityplugin/PhotoPicker;->s_Crop:Z

    if-eqz v1, :cond_2

    .line 367
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/iwodong/unityplugin/PhotoPicker;->crop(Landroid/net/Uri;)V

    goto :goto_0

    .line 371
    :cond_2
    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz v1, :cond_3

    .line 372
    invoke-virtual {v1, v0}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onSuccess(Ljava/io/File;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private static crop(Landroid/net/Uri;)V
    .locals 3

    if-nez p0, :cond_1

    .line 193
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz p0, :cond_0

    const/4 v0, 0x4

    const-string v1, "Data ready for crop picked from uri is null"

    .line 194
    invoke-virtual {p0, v0, v1}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onFailure(ILjava/lang/String;)V

    :cond_0
    return-void

    .line 199
    :cond_1
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    if-ne v0, v1, :cond_2

    .line 200
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 201
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.camera.action.CROP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "image/*"

    .line 204
    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "crop"

    const-string v2, "true"

    .line 205
    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p0, 0x1

    const-string v2, "aspectX"

    .line 207
    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "aspectY"

    .line 208
    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 210
    sget p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_OutputX:I

    const-string v2, "outputX"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    sget p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_OutputY:I

    const-string v2, "outputY"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p0, 0x0

    const-string v2, "return-data"

    .line 212
    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    new-instance p0, Ljava/io/File;

    sget-object v2, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v2}, Lcom/iwodong/unityplugin/PhotoPicker;->getCropPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    const-string v2, "output"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p0, "outputFormat"

    .line 214
    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private static cropRequestBack(Landroid/content/Intent;)V
    .locals 2

    .line 383
    sget-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    if-eqz p0, :cond_0

    .line 384
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getCropPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onSuccess(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method private static deleteTempFile()V
    .locals 2

    .line 420
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    .line 423
    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 424
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 427
    :cond_1
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 428
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 429
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 431
    :cond_2
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getCropPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 433
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 435
    :cond_3
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->ePNG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v1}, Lcom/iwodong/unityplugin/PhotoPicker;->getCropPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 436
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 437
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_4
    return-void
.end method

.method private static getCropPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;
    .locals 2

    .line 285
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 289
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    if-ne p0, v0, :cond_1

    const-string p0, "jpg"

    goto :goto_0

    :cond_1
    const-string p0, "png"

    .line 290
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 291
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 292
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 294
    :cond_2
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 296
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_TempFileNameWithoutExt:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_crop."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;
    .locals 2

    .line 262
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 266
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    if-ne p0, v0, :cond_1

    const-string p0, "jpg"

    goto :goto_0

    :cond_1
    const-string p0, "png"

    .line 267
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 268
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 271
    :cond_2
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_TempFileNameWithoutExt:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static onPickedResult(IILandroid/content/Intent;)V
    .locals 0

    const/4 p1, 0x1

    if-eq p0, p1, :cond_2

    const/4 p1, 0x2

    if-eq p0, p1, :cond_1

    const/4 p1, 0x3

    if-eq p0, p1, :cond_0

    return-void

    .line 147
    :cond_0
    invoke-static {p2}, Lcom/iwodong/unityplugin/PhotoPicker;->cropRequestBack(Landroid/content/Intent;)V

    goto :goto_0

    .line 144
    :cond_1
    invoke-static {}, Lcom/iwodong/unityplugin/PhotoPicker;->cameraRequestBack()V

    goto :goto_0

    .line 141
    :cond_2
    invoke-static {p2}, Lcom/iwodong/unityplugin/PhotoPicker;->albumRequestBack(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method public static pick(Landroid/app/Activity;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;ZIILcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;)V
    .locals 0

    if-nez p0, :cond_1

    if-eqz p6, :cond_0

    const/4 p0, 0x1

    const-string p1, "Start activity is not initialized."

    .line 103
    invoke-virtual {p6, p0, p1}, Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;->onFailure(ILjava/lang/String;)V

    :cond_0
    return-void

    .line 110
    :cond_1
    sput-object p0, Lcom/iwodong/unityplugin/PhotoPicker;->START_ACTIVITY:Landroid/app/Activity;

    .line 111
    sput-object p2, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    .line 112
    sput-boolean p3, Lcom/iwodong/unityplugin/PhotoPicker;->s_Crop:Z

    .line 113
    sput p4, Lcom/iwodong/unityplugin/PhotoPicker;->s_OutputX:I

    .line 114
    sput p5, Lcom/iwodong/unityplugin/PhotoPicker;->s_OutputY:I

    .line 115
    sput-object p6, Lcom/iwodong/unityplugin/PhotoPicker;->s_Handler:Lcom/iwodong/unityplugin/PhotoPicker$PickResultHandler;

    .line 118
    invoke-static {}, Lcom/iwodong/unityplugin/PhotoPicker;->deleteTempFile()V

    .line 120
    invoke-static {}, Lcom/iwodong/unityplugin/PhotoPicker;->$SWITCH_TABLE$com$iwodong$unityplugin$PhotoPicker$EPhotoResource()[I

    move-result-object p0

    invoke-virtual {p1}, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoResource;->ordinal()I

    move-result p1

    aget p0, p0, p1

    const/4 p1, 0x2

    if-eq p0, p1, :cond_2

    .line 127
    invoke-static {p2}, Lcom/iwodong/unityplugin/PhotoPicker;->albumsPickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)V

    goto :goto_0

    .line 123
    :cond_2
    invoke-static {p2}, Lcom/iwodong/unityplugin/PhotoPicker;->cameraPickImpl(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)V

    :goto_0
    return-void
.end method

.method private static saveBitmap(Landroid/graphics/Bitmap;)V
    .locals 4

    if-nez p0, :cond_0

    return-void

    .line 229
    :cond_0
    sget-object v0, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    invoke-static {v0}, Lcom/iwodong/unityplugin/PhotoPicker;->getPhotoTempPath(Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;)Ljava/lang/String;

    move-result-object v0

    .line 230
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 233
    sget-object v2, Lcom/iwodong/unityplugin/PhotoPicker;->s_CompressFormat:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    sget-object v3, Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;->eJPEG:Lcom/iwodong/unityplugin/PhotoPicker$EPhotoCompressFormat;

    if-ne v2, v3, :cond_1

    .line 234
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0

    .line 235
    :cond_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    const/16 v3, 0x64

    .line 237
    invoke-virtual {p0, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 238
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 240
    :cond_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 249
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 250
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception p0

    .line 244
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 245
    invoke-virtual {p0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_1
    return-void
.end method
