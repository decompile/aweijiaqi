.class Lcom/iwodong/unityplugin/SystemUtilsOperation$5;
.super Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;
.source "SystemUtilsOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/iwodong/unityplugin/SystemUtilsOperation;->asyncUploadFile(Ljava/lang/String;ZLjava/lang/String;[BLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$unityGameObjectName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 672
    iput-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/iwodong/unityplugin/AsynchronousHttpClient$ProgressHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(ILjava/lang/String;)V
    .locals 2

    .line 682
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "statusCode"

    .line 684
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "msg"

    .line 685
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 688
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 691
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 692
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "onFailure"

    invoke-static {p1, v0, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onFinish()V
    .locals 3

    .line 730
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    const-string v1, "onFinish"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onProgress(JJ)V
    .locals 2

    .line 698
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "currentSize"

    .line 700
    invoke-virtual {v0, v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string p1, "totalSize"

    .line 701
    invoke-virtual {v0, p1, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 704
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 707
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 708
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "onProgress"

    invoke-static {p1, p3, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onRetry()V
    .locals 3

    .line 737
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    const-string v1, "onRetry"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onSuccess(ILjava/io/File;Ljava/lang/String;)V
    .locals 1

    .line 713
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "statusCode"

    .line 715
    invoke-virtual {p2, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "filePath"

    const-string v0, ""

    .line 716
    invoke-virtual {p2, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "msg"

    .line 717
    invoke-virtual {p2, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 720
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 723
    :goto_0
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 724
    iget-object p1, p0, Lcom/iwodong/unityplugin/SystemUtilsOperation$5;->val$unityGameObjectName:Ljava/lang/String;

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "onSuccess"

    invoke-static {p1, p3, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
