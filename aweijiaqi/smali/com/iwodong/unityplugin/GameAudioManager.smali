.class public Lcom/iwodong/unityplugin/GameAudioManager;
.super Ljava/lang/Object;
.source "GameAudioManager.java"


# instance fields
.field _listAudios:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field _listMediaPlayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/media/MediaPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private _mediaRd:Landroid/media/MediaRecorder;

.field private _strAudioStorageDirectory:Ljava/lang/String;

.field _tempAudioFile:Ljava/io/File;

.field private countMediaPlayer:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 65
    iput-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listMediaPlayer:Ljava/util/ArrayList;

    const/4 v1, 0x0

    .line 75
    iput v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->countMediaPlayer:I

    .line 77
    iput-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_tempAudioFile:Ljava/io/File;

    return-void
.end method

.method private gotoAppDetailAct()V
    .locals 3

    .line 138
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "\u60a8\u9700\u8981\u5728\u8bbe\u7f6e\u4e2d\u624b\u52a8\u6253\u5f00\u5f55\u97f3\u6743\u9650\uff0c\u624d\u80fd\u4f7f\u7528\u8bed\u97f3\u804a\u5929\u529f\u80fd"

    .line 139
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "\u63d0\u793a"

    .line 140
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 141
    new-instance v1, Lcom/iwodong/unityplugin/GameAudioManager$1;

    invoke-direct {v1, p0}, Lcom/iwodong/unityplugin/GameAudioManager$1;-><init>(Lcom/iwodong/unityplugin/GameAudioManager;)V

    const-string v2, "\u6253\u5f00\u6743\u9650"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 153
    new-instance v1, Lcom/iwodong/unityplugin/GameAudioManager$2;

    invoke-direct {v1, p0}, Lcom/iwodong/unityplugin/GameAudioManager$2;-><init>(Lcom/iwodong/unityplugin/GameAudioManager;)V

    const-string v2, "\u53d6\u6d88"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 159
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private requestRecorderPermission()V
    .locals 3

    .line 127
    sget-object v0, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    .line 130
    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x4d2

    .line 129
    invoke-static {v0, v1, v2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->gotoAppDetailAct()V

    :goto_0
    return-void
.end method


# virtual methods
.method GetOneMediaPlayer()Landroid/media/MediaPlayer;
    .locals 2

    .line 277
    iget v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->countMediaPlayer:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 279
    iput v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->countMediaPlayer:I

    .line 281
    :cond_0
    iget v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->countMediaPlayer:I

    rem-int/lit8 v1, v0, 0xf

    add-int/lit8 v0, v0, 0x1

    .line 282
    iput v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->countMediaPlayer:I

    .line 283
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listMediaPlayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v1, :cond_1

    .line 285
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    .line 286
    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listMediaPlayer:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listMediaPlayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public addAudioName(Ljava/lang/String;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 95
    :goto_0
    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/iwodong/unityplugin/GameAudioManager;->deleteAudio(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public deleteAudio(Ljava/lang/String;)V
    .locals 2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 3

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    .line 86
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    return-void
.end method

.method public isHasPermission()Z
    .locals 9

    const v0, 0xac44

    const/16 v1, 0xc

    const/4 v2, 0x2

    .line 173
    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v8

    .line 175
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v4, 0x1

    const v5, 0xac44

    const/16 v6, 0xc

    const/4 v7, 0x2

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 180
    :try_start_0
    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 182
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 189
    :goto_0
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 190
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 191
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x0

    return v0

    .line 195
    :cond_0
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 196
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x1

    return v0
.end method

.method public playAudio(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 297
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_listAudios:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 300
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 301
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 302
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    .line 306
    :cond_1
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->GetOneMediaPlayer()Landroid/media/MediaPlayer;

    move-result-object p1

    .line 308
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 309
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 310
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->prepare()V

    .line 311
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method

.method public startRecord(Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_strAudioStorageDirectory:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 208
    :try_start_0
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->stopRecord()V

    .line 209
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->isHasPermission()Z

    move-result v2

    if-nez v2, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->requestRecorderPermission()V

    return v1

    .line 214
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_tempAudioFile:Ljava/io/File;

    .line 219
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 221
    iget-object v2, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_tempAudioFile:Ljava/io/File;

    if-eqz v2, :cond_1

    .line 224
    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/GameAudioManager;->addAudioName(Ljava/lang/String;)V

    .line 226
    new-instance p1, Landroid/media/MediaRecorder;

    invoke-direct {p1}, Landroid/media/MediaRecorder;-><init>()V

    iput-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    .line 228
    invoke-virtual {p1}, Landroid/media/MediaRecorder;->reset()V

    .line 230
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 232
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 234
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 236
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    invoke-virtual {p1, v0}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 239
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    invoke-virtual {p1}, Landroid/media/MediaRecorder;->prepare()V

    .line 240
    iget-object p1, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    invoke-virtual {p1}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :cond_1
    return v1

    .line 247
    :catch_0
    invoke-direct {p0}, Lcom/iwodong/unityplugin/GameAudioManager;->gotoAppDetailAct()V

    return v1
.end method

.method public stopRecord()V
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    return-void

    .line 263
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 264
    iget-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    const/4 v0, 0x0

    .line 265
    iput-object v0, p0, Lcom/iwodong/unityplugin/GameAudioManager;->_mediaRd:Landroid/media/MediaRecorder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
