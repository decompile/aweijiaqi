.class public Lcom/iwodong/unityplugin/PushNotificationService;
.super Landroid/app/Service;
.source "PushNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;
    }
.end annotation


# instance fields
.field _TAG:Ljava/lang/String;

.field _listNotificatons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;",
            ">;"
        }
    .end annotation
.end field

.field _timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    .line 152
    iput-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_timer:Ljava/util/Timer;

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_listNotificatons:Ljava/util/ArrayList;

    const-string v0, "WODONG"

    .line 158
    iput-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_TAG:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/iwodong/unityplugin/PushNotificationService;)V
    .locals 0

    .line 199
    invoke-direct {p0}, Lcom/iwodong/unityplugin/PushNotificationService;->resetNotification()V

    return-void
.end method

.method private parseJsonText(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 173
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "local"

    .line 174
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v0, 0x0

    .line 175
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    .line 177
    :cond_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 180
    new-instance v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;

    invoke-direct {v2, p0}, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;-><init>(Lcom/iwodong/unityplugin/PushNotificationService;)V

    const-string v3, "id"

    .line 181
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->id:I

    const-string v3, "open"

    .line 182
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->open:Z

    const-string v3, "yy"

    .line 183
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->yy:I

    const-string v3, "mm"

    .line 184
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->mm:I

    const-string v3, "dd"

    .line 185
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->dd:I

    const-string v3, "week"

    .line 186
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->week:I

    const-string v3, "hour"

    .line 187
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->hour:I

    const-string v3, "minute"

    .line 188
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->minute:I

    const-string v3, "title"

    .line 189
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->title:Ljava/lang/String;

    const-string v3, "text"

    .line 190
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->text:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_listNotificatons:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private resetNotification()V
    .locals 2

    const/4 v0, 0x0

    .line 201
    :goto_0
    iget-object v1, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_listNotificatons:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_listNotificatons:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;

    invoke-virtual {v1}, Lcom/iwodong/unityplugin/PushNotificationService$NotificationDataObject;->reset()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    .line 264
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 266
    iget-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_listNotificatons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    if-nez p1, :cond_0

    const/4 p2, 0x3

    .line 214
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1

    return p1

    :cond_0
    const-string v0, "jsText"

    .line 217
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :try_start_0
    invoke-direct {p0, v0}, Lcom/iwodong/unityplugin/PushNotificationService;->parseJsonText(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v5, 0x1388

    .line 228
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 230
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/iwodong/unityplugin/PushNotificationService;->_timer:Ljava/util/Timer;

    .line 231
    new-instance v2, Lcom/iwodong/unityplugin/PushNotificationService$1;

    invoke-direct {v2, p0, v0}, Lcom/iwodong/unityplugin/PushNotificationService$1;-><init>(Lcom/iwodong/unityplugin/PushNotificationService;Ljava/util/Calendar;)V

    const-wide/16 v3, 0x0

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 256
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1

    return p1

    .line 222
    :catch_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result p1

    return p1
.end method
