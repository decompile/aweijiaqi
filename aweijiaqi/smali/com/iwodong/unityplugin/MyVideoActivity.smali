.class public Lcom/iwodong/unityplugin/MyVideoActivity;
.super Landroid/app/Activity;
.source "MyVideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;
    }
.end annotation


# instance fields
.field private _currentPlayingVideoName:Ljava/lang/String;

.field private _currentVideoIndx:I

.field _gameOjectCallback:Ljava/lang/String;

.field _lastPosition:I

.field private _mediaPlayer:Landroid/media/MediaPlayer;

.field private _modeScale:I

.field private _screenHeight:I

.field private _screenWidth:I

.field private _surfaceView:Landroid/view/SurfaceView;

.field private _videoClipCount:I

.field private _videoHeight:I

.field private _videoName:Ljava/lang/String;

.field private _videoView:Landroid/widget/VideoView;

.field private _videoWidth:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoView:Landroid/widget/VideoView;

    .line 42
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_surfaceView:Landroid/view/SurfaceView;

    .line 45
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    const-string v0, ""

    .line 48
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentPlayingVideoName:Ljava/lang/String;

    const/4 v0, 0x1

    .line 52
    iput v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoClipCount:I

    .line 53
    iput v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentVideoIndx:I

    const/16 v1, 0x470

    .line 56
    iput v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoWidth:I

    const/16 v2, 0x280

    .line 57
    iput v2, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoHeight:I

    .line 60
    iput v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_screenWidth:I

    .line 61
    iput v2, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_screenHeight:I

    .line 64
    iput v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_modeScale:I

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_lastPosition:I

    const-string v0, "Singleton"

    .line 70
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_gameOjectCallback:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/iwodong/unityplugin/MyVideoActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 188
    invoke-direct {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->play()V

    return-void
.end method

.method static synthetic access$1(Lcom/iwodong/unityplugin/MyVideoActivity;)Landroid/media/MediaPlayer;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic access$2(Lcom/iwodong/unityplugin/MyVideoActivity;)Z
    .locals 0

    .line 255
    invoke-direct {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->nextVideo()Z

    move-result p0

    return p0
.end method

.method private nextVideo()Z
    .locals 4

    .line 257
    iget v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentVideoIndx:I

    iget v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoClipCount:I

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x1

    if-ne v1, v0, :cond_1

    .line 261
    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    iput-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentPlayingVideoName:Ljava/lang/String;

    goto :goto_0

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    const-string v3, "\\."

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 265
    new-instance v3, Ljava/lang/StringBuilder;

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentVideoIndx:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v1, v0

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentPlayingVideoName:Ljava/lang/String;

    .line 270
    :goto_0
    iget v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentVideoIndx:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentVideoIndx:I

    return v0
.end method

.method private play()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 191
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 192
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 195
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_currentPlayingVideoName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    .line 198
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v3

    .line 199
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v5

    .line 197
    invoke-virtual/range {v1 .. v6}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 203
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 205
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 208
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/iwodong/unityplugin/MyVideoActivity$2;

    invoke-direct {v1, p0}, Lcom/iwodong/unityplugin/MyVideoActivity$2;-><init>(Lcom/iwodong/unityplugin/MyVideoActivity;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 219
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/iwodong/unityplugin/MyVideoActivity$3;

    invoke-direct {v1, p0}, Lcom/iwodong/unityplugin/MyVideoActivity$3;-><init>(Lcom/iwodong/unityplugin/MyVideoActivity;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "video"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    .line 81
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x1

    const-string v1, "videoClipCount"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoClipCount:I

    .line 82
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/16 v1, 0x470

    const-string v2, "videoWidth"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoWidth:I

    .line 83
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/16 v2, 0x280

    const-string v3, "videoHeight"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoHeight:I

    .line 84
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "screenWidth"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_screenWidth:I

    .line 85
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "screenHeight"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_screenHeight:I

    .line 86
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "mode"

    invoke-virtual {p1, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_modeScale:I

    .line 87
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "goName"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_gameOjectCallback:Ljava/lang/String;

    .line 90
    iget-object p1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    const-string v3, ""

    if-ne p1, v3, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "Error 101: \u89c6\u9891\u53c2\u6570\u9519\u8bef"

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 93
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->finish()V

    return-void

    :cond_0
    const-string p1, "wd_playvideo"

    const-string v3, "layout"

    .line 98
    invoke-static {p1, v3}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "Error 102: \u52a0\u8f7d\u64ad\u653e\u7a97\u53e3\u9519\u8bef"

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 102
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->finish()V

    return-void

    .line 105
    :cond_1
    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/MyVideoActivity;->setContentView(I)V

    .line 108
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v3, 0x80

    invoke-virtual {p1, v3}, Landroid/view/Window;->addFlags(I)V

    const-string p1, "id"

    const-string v3, "wd_surfaceViewForVideo"

    .line 111
    invoke-static {v3, p1}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 113
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    .line 114
    invoke-virtual {p0, v3}, Lcom/iwodong/unityplugin/MyVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    iput-object v3, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_surfaceView:Landroid/view/SurfaceView;

    if-nez v3, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "Error 103: \u52a0\u8f7d\u64ad\u653e\u7ec4\u4ef6\u9519\u8bef"

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 118
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->finish()V

    return-void

    .line 123
    :cond_2
    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    .line 124
    iget-object v3, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    new-instance v4, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;-><init>(Lcom/iwodong/unityplugin/MyVideoActivity;Lcom/iwodong/unityplugin/MyVideoActivity$SurfaceListener;)V

    invoke-interface {v3, v4}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 125
    iget-object v3, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 128
    invoke-direct {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->nextVideo()Z

    move-result v1

    if-nez v1, :cond_3

    .line 130
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "Error 104: \u6ca1\u627e\u5230\u89c6\u9891\u6587\u4ef6"

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 131
    invoke-virtual {p0}, Lcom/iwodong/unityplugin/MyVideoActivity;->finish()V

    return-void

    :cond_3
    const-string v0, "wd_videoJump"

    .line 135
    invoke-static {v0, p1}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    .line 136
    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/MyVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 137
    new-instance v0, Lcom/iwodong/unityplugin/MyVideoActivity$1;

    invoke-direct {v0, p0}, Lcom/iwodong/unityplugin/MyVideoActivity$1;-><init>(Lcom/iwodong/unityplugin/MyVideoActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .line 314
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 317
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    .line 322
    iput-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_gameOjectCallback:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_gameOjectCallback:Ljava/lang/String;

    iget-object v1, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_videoName:Ljava/lang/String;

    const-string v2, "onPlayVideoActivityDestroyed"

    invoke-static {v0, v2, v1}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void

    .line 327
    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "gameObject is null, please set gameObject first"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_lastPosition:I

    .line 282
    iget-object v0, p0, Lcom/iwodong/unityplugin/MyVideoActivity;->_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 284
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
