.class public Lcom/iwodong/unityplugin/SoundEffect;
.super Landroid/media/SoundPool;
.source "SoundEffect.java"


# static fields
.field private static activity:Landroid/app/Activity;


# instance fields
.field private soundsSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILandroid/app/Activity;)V
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0, p1, v0, v1}, Landroid/media/SoundPool;-><init>(III)V

    .line 15
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    .line 19
    sput-object p2, Lcom/iwodong/unityplugin/SoundEffect;->activity:Landroid/app/Activity;

    .line 20
    new-instance p1, Lcom/iwodong/unityplugin/SoundEffect$1;

    invoke-direct {p1, p0}, Lcom/iwodong/unityplugin/SoundEffect$1;-><init>(Lcom/iwodong/unityplugin/SoundEffect;)V

    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/SoundEffect;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/iwodong/unityplugin/SoundEffect;)Ljava/util/Set;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public loadSound(Ljava/lang/String;)I
    .locals 2

    .line 53
    :try_start_0
    sget-object v0, Lcom/iwodong/unityplugin/SoundEffect;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    .line 58
    invoke-virtual {p0, p1, v0}, Lcom/iwodong/unityplugin/SoundEffect;->load(Landroid/content/res/AssetFileDescriptor;I)I

    move-result p1

    .line 59
    iget-object v0, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return p1

    :catch_0
    const-string p1, "SoundPluginUnity"

    const-string v0, "File does not exist!"

    .line 55
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, -0x1

    return p1
.end method

.method public play(IF)V
    .locals 8

    .line 31
    iget-object v0, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p2

    .line 32
    invoke-virtual/range {v1 .. v7}, Lcom/iwodong/unityplugin/SoundEffect;->play(IFFIIF)I

    :cond_0
    return-void
.end method

.method public playSound(IF)V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v0, "SoundPluginUnity"

    const-string v1, "File has not been loaded!"

    .line 38
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    :cond_1
    sget-object v0, Lcom/iwodong/unityplugin/SoundEffect;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/iwodong/unityplugin/SoundEffect$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/iwodong/unityplugin/SoundEffect$2;-><init>(Lcom/iwodong/unityplugin/SoundEffect;IF)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public unloadSound(I)V
    .locals 1

    .line 64
    invoke-virtual {p0, p1}, Lcom/iwodong/unityplugin/SoundEffect;->unload(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/iwodong/unityplugin/SoundEffect;->soundsSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string p1, "SoundPluginUnity"

    const-string v0, "File has not been loaded!"

    .line 68
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
