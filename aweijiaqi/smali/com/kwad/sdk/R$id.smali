.class public final Lcom/kwad/sdk/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f080006

.field public static final action_bar_activity_content:I = 0x7f080007

.field public static final action_bar_container:I = 0x7f080008

.field public static final action_bar_root:I = 0x7f080009

.field public static final action_bar_spinner:I = 0x7f08000a

.field public static final action_bar_subtitle:I = 0x7f08000b

.field public static final action_bar_title:I = 0x7f08000c

.field public static final action_container:I = 0x7f08000d

.field public static final action_context_bar:I = 0x7f08000e

.field public static final action_divider:I = 0x7f08000f

.field public static final action_image:I = 0x7f080010

.field public static final action_menu_divider:I = 0x7f080011

.field public static final action_menu_presenter:I = 0x7f080012

.field public static final action_mode_bar:I = 0x7f080013

.field public static final action_mode_bar_stub:I = 0x7f080014

.field public static final action_mode_close_button:I = 0x7f080015

.field public static final action_text:I = 0x7f080016

.field public static final actions:I = 0x7f080017

.field public static final activity_chooser_view_content:I = 0x7f080018

.field public static final add:I = 0x7f080019

.field public static final alertTitle:I = 0x7f08001a

.field public static final async:I = 0x7f080082

.field public static final blocking:I = 0x7f080086

.field public static final buttonPanel:I = 0x7f080089

.field public static final checkbox:I = 0x7f08008e

.field public static final chronometer:I = 0x7f08008f

.field public static final contentPanel:I = 0x7f080096

.field public static final custom:I = 0x7f08009b

.field public static final customPanel:I = 0x7f08009c

.field public static final decor_content_parent:I = 0x7f08009e

.field public static final default_activity_button:I = 0x7f08009f

.field public static final edit_query:I = 0x7f0800a6

.field public static final expand_activities_button:I = 0x7f0800ab

.field public static final expanded_menu:I = 0x7f0800ac

.field public static final forever:I = 0x7f0800b6

.field public static final home:I = 0x7f0800c0

.field public static final icon:I = 0x7f0800c2

.field public static final icon_group:I = 0x7f0800c3

.field public static final image:I = 0x7f0800c5

.field public static final info:I = 0x7f0800c6

.field public static final italic:I = 0x7f0800c7

.field public static final ksad_actionbar_black_style_h5:I = 0x7f0800cd

.field public static final ksad_actionbar_landscape_vertical:I = 0x7f0800ce

.field public static final ksad_actionbar_logo:I = 0x7f0800cf

.field public static final ksad_actionbar_portrait_horizontal:I = 0x7f0800d0

.field public static final ksad_ad_desc:I = 0x7f0800d1

.field public static final ksad_ad_dislike:I = 0x7f0800d2

.field public static final ksad_ad_dislike_logo:I = 0x7f0800d3

.field public static final ksad_ad_download_container:I = 0x7f0800d4

.field public static final ksad_ad_h5_container:I = 0x7f0800d5

.field public static final ksad_ad_image:I = 0x7f0800d6

.field public static final ksad_ad_image_left:I = 0x7f0800d7

.field public static final ksad_ad_image_mid:I = 0x7f0800d8

.field public static final ksad_ad_image_right:I = 0x7f0800d9

.field public static final ksad_ad_interstitial_logo:I = 0x7f0800da

.field public static final ksad_ad_label_play_bar:I = 0x7f0800db

.field public static final ksad_ad_light_convert_btn:I = 0x7f0800dc

.field public static final ksad_ad_normal_container:I = 0x7f0800dd

.field public static final ksad_ad_normal_convert_btn:I = 0x7f0800de

.field public static final ksad_ad_normal_des:I = 0x7f0800df

.field public static final ksad_ad_normal_logo:I = 0x7f0800e0

.field public static final ksad_ad_normal_title:I = 0x7f0800e1

.field public static final ksad_aggregate_web_ad_icon:I = 0x7f0800e2

.field public static final ksad_aggregate_web_navi_back:I = 0x7f0800e3

.field public static final ksad_aggregate_web_root:I = 0x7f0800e4

.field public static final ksad_aggregate_web_titlebar:I = 0x7f0800e5

.field public static final ksad_aggregate_webview:I = 0x7f0800e6

.field public static final ksad_app_ad_desc:I = 0x7f0800e7

.field public static final ksad_app_container:I = 0x7f0800e8

.field public static final ksad_app_desc:I = 0x7f0800e9

.field public static final ksad_app_download:I = 0x7f0800ea

.field public static final ksad_app_download_btn:I = 0x7f0800eb

.field public static final ksad_app_download_btn_cover:I = 0x7f0800ec

.field public static final ksad_app_download_count:I = 0x7f0800ed

.field public static final ksad_app_icon:I = 0x7f0800ee

.field public static final ksad_app_introduce:I = 0x7f0800ef

.field public static final ksad_app_name:I = 0x7f0800f0

.field public static final ksad_app_score:I = 0x7f0800f1

.field public static final ksad_app_title:I = 0x7f0800f2

.field public static final ksad_blur_video_cover:I = 0x7f0800f3

.field public static final ksad_bottom_container:I = 0x7f0800f4

.field public static final ksad_card_ad_desc:I = 0x7f0800f5

.field public static final ksad_card_app_close:I = 0x7f0800f6

.field public static final ksad_card_app_container:I = 0x7f0800f7

.field public static final ksad_card_app_desc:I = 0x7f0800f8

.field public static final ksad_card_app_download_btn:I = 0x7f0800f9

.field public static final ksad_card_app_download_count:I = 0x7f0800fa

.field public static final ksad_card_app_icon:I = 0x7f0800fb

.field public static final ksad_card_app_name:I = 0x7f0800fc

.field public static final ksad_card_app_score:I = 0x7f0800fd

.field public static final ksad_card_app_score_container:I = 0x7f0800fe

.field public static final ksad_card_close:I = 0x7f0800ff

.field public static final ksad_card_h5_container:I = 0x7f080100

.field public static final ksad_card_h5_open_btn:I = 0x7f080101

.field public static final ksad_card_logo:I = 0x7f080102

.field public static final ksad_click_mask:I = 0x7f080103

.field public static final ksad_close_btn:I = 0x7f080104

.field public static final ksad_container:I = 0x7f080105

.field public static final ksad_continue_btn:I = 0x7f080106

.field public static final ksad_data_flow_container:I = 0x7f080107

.field public static final ksad_data_flow_play_btn:I = 0x7f080108

.field public static final ksad_data_flow_play_tip:I = 0x7f080109

.field public static final ksad_detail_call_btn:I = 0x7f08010a

.field public static final ksad_detail_close_btn:I = 0x7f08010b

.field public static final ksad_detail_reward_icon:I = 0x7f08010c

.field public static final ksad_detail_reward_icon_new:I = 0x7f08010d

.field public static final ksad_detail_reward_tip_new:I = 0x7f08010e

.field public static final ksad_download_bar:I = 0x7f08010f

.field public static final ksad_download_bar_cover:I = 0x7f080110

.field public static final ksad_download_container:I = 0x7f080111

.field public static final ksad_download_icon:I = 0x7f080112

.field public static final ksad_download_install:I = 0x7f080113

.field public static final ksad_download_name:I = 0x7f080114

.field public static final ksad_download_percent_num:I = 0x7f080115

.field public static final ksad_download_progress:I = 0x7f080116

.field public static final ksad_download_progress_cover:I = 0x7f080117

.field public static final ksad_download_size:I = 0x7f080118

.field public static final ksad_download_status:I = 0x7f080119

.field public static final ksad_download_tips_web_card_webView:I = 0x7f08011a

.field public static final ksad_draw_h5_logo:I = 0x7f08011b

.field public static final ksad_draw_tailframe_logo:I = 0x7f08011c

.field public static final ksad_end_close_btn:I = 0x7f08011d

.field public static final ksad_end_left_call_btn:I = 0x7f08011e

.field public static final ksad_end_reward_icon:I = 0x7f08011f

.field public static final ksad_end_reward_icon_layout:I = 0x7f080120

.field public static final ksad_end_reward_icon_new_left:I = 0x7f080121

.field public static final ksad_end_reward_icon_new_right:I = 0x7f080122

.field public static final ksad_end_right_call_btn:I = 0x7f080123

.field public static final ksad_exit_intercept_content_layout:I = 0x7f080124

.field public static final ksad_exit_intercept_dialog_layout:I = 0x7f080125

.field public static final ksad_feed_logo:I = 0x7f080126

.field public static final ksad_feed_video_container:I = 0x7f080127

.field public static final ksad_foreground_cover:I = 0x7f080128

.field public static final ksad_h5_ad_desc:I = 0x7f080129

.field public static final ksad_h5_container:I = 0x7f08012a

.field public static final ksad_h5_desc:I = 0x7f08012b

.field public static final ksad_h5_open:I = 0x7f08012c

.field public static final ksad_h5_open_btn:I = 0x7f08012d

.field public static final ksad_h5_open_cover:I = 0x7f08012e

.field public static final ksad_image_container:I = 0x7f08012f

.field public static final ksad_install_tips_close:I = 0x7f080130

.field public static final ksad_install_tips_content:I = 0x7f080131

.field public static final ksad_install_tips_icon:I = 0x7f080132

.field public static final ksad_install_tips_install:I = 0x7f080133

.field public static final ksad_interactive_landing_page_container:I = 0x7f080134

.field public static final ksad_interstitial_close:I = 0x7f080135

.field public static final ksad_interstitial_count_down:I = 0x7f080136

.field public static final ksad_interstitial_desc:I = 0x7f080137

.field public static final ksad_interstitial_download_btn:I = 0x7f080138

.field public static final ksad_interstitial_logo:I = 0x7f080139

.field public static final ksad_interstitial_mute:I = 0x7f08013a

.field public static final ksad_interstitial_native:I = 0x7f08013b

.field public static final ksad_interstitial_play:I = 0x7f08013c

.field public static final ksad_interstitial_play_end:I = 0x7f08013d

.field public static final ksad_interstitial_playing:I = 0x7f08013e

.field public static final ksad_item_touch_helper_previous_elevation:I = 0x7f08013f

.field public static final ksad_js_bottom:I = 0x7f080140

.field public static final ksad_js_slide_black:I = 0x7f080141

.field public static final ksad_js_top:I = 0x7f080142

.field public static final ksad_kwad_titlebar_title:I = 0x7f080143

.field public static final ksad_kwad_web_navi_back:I = 0x7f080144

.field public static final ksad_kwad_web_navi_close:I = 0x7f080145

.field public static final ksad_kwad_web_title_bar:I = 0x7f080146

.field public static final ksad_kwad_web_titlebar:I = 0x7f080147

.field public static final ksad_landing_page_container:I = 0x7f080148

.field public static final ksad_landing_page_root:I = 0x7f080149

.field public static final ksad_logo_container:I = 0x7f08014a

.field public static final ksad_logo_icon:I = 0x7f08014b

.field public static final ksad_logo_text:I = 0x7f08014c

.field public static final ksad_message_toast_txt:I = 0x7f08014d

.field public static final ksad_middle_end_card:I = 0x7f08014e

.field public static final ksad_middle_end_card_native:I = 0x7f08014f

.field public static final ksad_middle_end_card_webview_container:I = 0x7f080150

.field public static final ksad_mini_web_card_container:I = 0x7f080151

.field public static final ksad_mini_web_card_webView:I = 0x7f080152

.field public static final ksad_normal_text:I = 0x7f080153

.field public static final ksad_play_detail_top_toolbar:I = 0x7f080154

.field public static final ksad_play_end_top_toolbar:I = 0x7f080155

.field public static final ksad_play_end_web_card_container:I = 0x7f080156

.field public static final ksad_play_web_card_webView:I = 0x7f080157

.field public static final ksad_playable_webview:I = 0x7f080158

.field public static final ksad_preload_container:I = 0x7f080159

.field public static final ksad_product_name:I = 0x7f08015a

.field public static final ksad_progress_bar:I = 0x7f08015b

.field public static final ksad_progress_bg:I = 0x7f08015c

.field public static final ksad_recycler_container:I = 0x7f08015d

.field public static final ksad_recycler_view:I = 0x7f08015e

.field public static final ksad_reward_container_new:I = 0x7f08015f

.field public static final ksad_reward_mini_card_close:I = 0x7f080160

.field public static final ksad_root_container:I = 0x7f080161

.field public static final ksad_score_fifth:I = 0x7f080162

.field public static final ksad_score_fourth:I = 0x7f080163

.field public static final ksad_skip_icon:I = 0x7f080164

.field public static final ksad_splash_background:I = 0x7f080165

.field public static final ksad_splash_close_btn:I = 0x7f080166

.field public static final ksad_splash_foreground:I = 0x7f080167

.field public static final ksad_splash_frame:I = 0x7f080168

.field public static final ksad_splash_logo_container:I = 0x7f080169

.field public static final ksad_splash_preload_tips:I = 0x7f08016a

.field public static final ksad_splash_root_container:I = 0x7f08016b

.field public static final ksad_splash_skip_time:I = 0x7f08016c

.field public static final ksad_splash_sound:I = 0x7f08016d

.field public static final ksad_splash_texture:I = 0x7f08016e

.field public static final ksad_splash_v_plus:I = 0x7f08016f

.field public static final ksad_splash_video_player:I = 0x7f080170

.field public static final ksad_splash_web_card_webView:I = 0x7f080171

.field public static final ksad_status_tv:I = 0x7f080172

.field public static final ksad_tf_h5_ad_desc:I = 0x7f080173

.field public static final ksad_tf_h5_open_btn:I = 0x7f080174

.field public static final ksad_title:I = 0x7f080175

.field public static final ksad_top_container:I = 0x7f080176

.field public static final ksad_top_container_product:I = 0x7f080177

.field public static final ksad_top_outer:I = 0x7f080178

.field public static final ksad_top_toolbar_close_tip:I = 0x7f080179

.field public static final ksad_video_app_tail_frame:I = 0x7f08017a

.field public static final ksad_video_complete_app_container:I = 0x7f08017b

.field public static final ksad_video_complete_app_icon:I = 0x7f08017c

.field public static final ksad_video_complete_h5_container:I = 0x7f08017d

.field public static final ksad_video_container:I = 0x7f08017e

.field public static final ksad_video_control_container:I = 0x7f08017f

.field public static final ksad_video_control_fullscreen:I = 0x7f080180

.field public static final ksad_video_control_fullscreen_container:I = 0x7f080181

.field public static final ksad_video_control_fullscreen_title:I = 0x7f080182

.field public static final ksad_video_control_play_button:I = 0x7f080183

.field public static final ksad_video_control_play_duration:I = 0x7f080184

.field public static final ksad_video_control_play_status:I = 0x7f080185

.field public static final ksad_video_control_play_total:I = 0x7f080186

.field public static final ksad_video_count_down:I = 0x7f080187

.field public static final ksad_video_count_down_new:I = 0x7f080188

.field public static final ksad_video_cover:I = 0x7f080189

.field public static final ksad_video_cover_image:I = 0x7f08018a

.field public static final ksad_video_error_container:I = 0x7f08018b

.field public static final ksad_video_fail_tip:I = 0x7f08018c

.field public static final ksad_video_first_frame:I = 0x7f08018d

.field public static final ksad_video_first_frame_container:I = 0x7f08018e

.field public static final ksad_video_h5_tail_frame:I = 0x7f08018f

.field public static final ksad_video_landscape_horizontal:I = 0x7f080190

.field public static final ksad_video_landscape_vertical:I = 0x7f080191

.field public static final ksad_video_network_unavailable:I = 0x7f080192

.field public static final ksad_video_place_holder:I = 0x7f080193

.field public static final ksad_video_play_bar_app_landscape:I = 0x7f080194

.field public static final ksad_video_play_bar_app_portrait:I = 0x7f080195

.field public static final ksad_video_play_bar_h5:I = 0x7f080196

.field public static final ksad_video_player:I = 0x7f080197

.field public static final ksad_video_portrait_horizontal:I = 0x7f080198

.field public static final ksad_video_portrait_vertical:I = 0x7f080199

.field public static final ksad_video_progress:I = 0x7f08019a

.field public static final ksad_video_sound_switch:I = 0x7f08019b

.field public static final ksad_video_tail_frame:I = 0x7f08019c

.field public static final ksad_video_tail_frame_container:I = 0x7f08019d

.field public static final ksad_video_text_below:I = 0x7f08019e

.field public static final ksad_video_tf_logo:I = 0x7f08019f

.field public static final ksad_video_thumb_container:I = 0x7f0801a0

.field public static final ksad_video_thumb_image:I = 0x7f0801a1

.field public static final ksad_video_thumb_img:I = 0x7f0801a2

.field public static final ksad_video_thumb_left:I = 0x7f0801a3

.field public static final ksad_video_thumb_mid:I = 0x7f0801a4

.field public static final ksad_video_thumb_right:I = 0x7f0801a5

.field public static final ksad_video_webView:I = 0x7f0801a6

.field public static final ksad_video_webview:I = 0x7f0801a7

.field public static final ksad_web_card_container:I = 0x7f0801a8

.field public static final ksad_web_card_frame:I = 0x7f0801a9

.field public static final ksad_web_card_webView:I = 0x7f0801aa

.field public static final ksad_web_download_container:I = 0x7f0801ab

.field public static final ksad_web_download_progress:I = 0x7f0801ac

.field public static final ksad_web_exit_intercept_negative_btn:I = 0x7f0801ad

.field public static final ksad_web_exit_intercept_positive_btn:I = 0x7f0801ae

.field public static final ksad_web_tip_bar:I = 0x7f0801af

.field public static final ksad_web_tip_bar_textview:I = 0x7f0801b0

.field public static final ksad_web_tip_close_btn:I = 0x7f0801b1

.field public static final ksad_web_video_seek_bar:I = 0x7f0801b2

.field public static final line1:I = 0x7f0801b8

.field public static final line3:I = 0x7f0801b9

.field public static final listMode:I = 0x7f0801ba

.field public static final list_item:I = 0x7f0801bb

.field public static final message:I = 0x7f0801c0

.field public static final multiply:I = 0x7f0801c6

.field public static final none:I = 0x7f0801c9

.field public static final normal:I = 0x7f0801ca

.field public static final notification_background:I = 0x7f0801cb

.field public static final notification_main_column:I = 0x7f0801cd

.field public static final notification_main_column_container:I = 0x7f0801ce

.field public static final parentPanel:I = 0x7f0801d2

.field public static final progress_circular:I = 0x7f0801d8

.field public static final progress_horizontal:I = 0x7f0801d9

.field public static final radio:I = 0x7f0801da

.field public static final right_icon:I = 0x7f0801dc

.field public static final right_side:I = 0x7f0801dd

.field public static final root:I = 0x7f0801de

.field public static final screen:I = 0x7f0801e2

.field public static final scrollIndicatorDown:I = 0x7f0801e4

.field public static final scrollIndicatorUp:I = 0x7f0801e5

.field public static final scrollView:I = 0x7f0801e6

.field public static final search_badge:I = 0x7f0801e8

.field public static final search_bar:I = 0x7f0801e9

.field public static final search_button:I = 0x7f0801ea

.field public static final search_close_btn:I = 0x7f0801eb

.field public static final search_edit_frame:I = 0x7f0801ec

.field public static final search_go_btn:I = 0x7f0801ed

.field public static final search_mag_icon:I = 0x7f0801ee

.field public static final search_plate:I = 0x7f0801ef

.field public static final search_src_text:I = 0x7f0801f0

.field public static final search_voice_btn:I = 0x7f0801f1

.field public static final select_dialog_listview:I = 0x7f0801f2

.field public static final shortcut:I = 0x7f0801f4

.field public static final spacer:I = 0x7f0801fd

.field public static final split_action_bar:I = 0x7f080201

.field public static final src_atop:I = 0x7f080202

.field public static final src_in:I = 0x7f080203

.field public static final src_over:I = 0x7f080204

.field public static final submenuarrow:I = 0x7f080207

.field public static final submit_area:I = 0x7f080208

.field public static final tabMode:I = 0x7f080209

.field public static final tag_transition_group:I = 0x7f08020a

.field public static final text:I = 0x7f08020d

.field public static final text2:I = 0x7f08020e

.field public static final textSpacerNoButtons:I = 0x7f08020f

.field public static final textSpacerNoTitle:I = 0x7f080210

.field public static final time:I = 0x7f080216

.field public static final title:I = 0x7f080217

.field public static final titleDividerNoCustom:I = 0x7f080218

.field public static final title_template:I = 0x7f08021a

.field public static final topPanel:I = 0x7f08021d

.field public static final uniform:I = 0x7f080317

.field public static final up:I = 0x7f080319

.field public static final wrap_content:I = 0x7f080324


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
