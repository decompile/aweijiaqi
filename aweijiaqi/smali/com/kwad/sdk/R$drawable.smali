.class public final Lcom/kwad/sdk/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f070006

.field public static final abc_action_bar_item_background_material:I = 0x7f070007

.field public static final abc_btn_borderless_material:I = 0x7f070008

.field public static final abc_btn_check_material:I = 0x7f070009

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f07000a

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f07000b

.field public static final abc_btn_colored_material:I = 0x7f07000c

.field public static final abc_btn_default_mtrl_shape:I = 0x7f07000d

.field public static final abc_btn_radio_material:I = 0x7f07000e

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f07000f

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f070010

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f070011

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f070012

.field public static final abc_cab_background_internal_bg:I = 0x7f070013

.field public static final abc_cab_background_top_material:I = 0x7f070014

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f070015

.field public static final abc_control_background_material:I = 0x7f070016

.field public static final abc_dialog_material_background:I = 0x7f070017

.field public static final abc_edit_text_material:I = 0x7f070018

.field public static final abc_ic_ab_back_material:I = 0x7f070019

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f07001a

.field public static final abc_ic_clear_material:I = 0x7f07001b

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f07001c

.field public static final abc_ic_go_search_api_material:I = 0x7f07001d

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f07001e

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f07001f

.field public static final abc_ic_menu_overflow_material:I = 0x7f070020

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f070021

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f070022

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f070023

.field public static final abc_ic_search_api_material:I = 0x7f070024

.field public static final abc_ic_star_black_16dp:I = 0x7f070025

.field public static final abc_ic_star_black_36dp:I = 0x7f070026

.field public static final abc_ic_star_black_48dp:I = 0x7f070027

.field public static final abc_ic_star_half_black_16dp:I = 0x7f070028

.field public static final abc_ic_star_half_black_36dp:I = 0x7f070029

.field public static final abc_ic_star_half_black_48dp:I = 0x7f07002a

.field public static final abc_ic_voice_search_api_material:I = 0x7f07002b

.field public static final abc_item_background_holo_dark:I = 0x7f07002c

.field public static final abc_item_background_holo_light:I = 0x7f07002d

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f07002f

.field public static final abc_list_focused_holo:I = 0x7f070030

.field public static final abc_list_longpressed_holo:I = 0x7f070031

.field public static final abc_list_pressed_holo_dark:I = 0x7f070032

.field public static final abc_list_pressed_holo_light:I = 0x7f070033

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f070034

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f070035

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f070036

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f070037

.field public static final abc_list_selector_holo_dark:I = 0x7f070038

.field public static final abc_list_selector_holo_light:I = 0x7f070039

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f07003a

.field public static final abc_popup_background_mtrl_mult:I = 0x7f07003b

.field public static final abc_ratingbar_indicator_material:I = 0x7f07003c

.field public static final abc_ratingbar_material:I = 0x7f07003d

.field public static final abc_ratingbar_small_material:I = 0x7f07003e

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f07003f

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f070040

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f070041

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f070042

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f070043

.field public static final abc_seekbar_thumb_material:I = 0x7f070044

.field public static final abc_seekbar_tick_mark_material:I = 0x7f070045

.field public static final abc_seekbar_track_material:I = 0x7f070046

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f070047

.field public static final abc_spinner_textfield_background_material:I = 0x7f070048

.field public static final abc_switch_thumb_material:I = 0x7f070049

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f07004a

.field public static final abc_tab_indicator_material:I = 0x7f07004b

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f07004c

.field public static final abc_text_cursor_material:I = 0x7f07004d

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f07004e

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f07004f

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f070050

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f070051

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f070052

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f070053

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f070054

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f070055

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f070056

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f070057

.field public static final abc_textfield_search_material:I = 0x7f070058

.field public static final abc_vector_test:I = 0x7f070059

.field public static final ksad_ad_dislike_bottom:I = 0x7f0700b9

.field public static final ksad_ad_dislike_gray:I = 0x7f0700ba

.field public static final ksad_ad_dislike_white:I = 0x7f0700bb

.field public static final ksad_app_score_gray:I = 0x7f0700bc

.field public static final ksad_app_score_half:I = 0x7f0700bd

.field public static final ksad_app_score_yellow:I = 0x7f0700be

.field public static final ksad_arrow_left:I = 0x7f0700bf

.field public static final ksad_author_live_tip:I = 0x7f0700c0

.field public static final ksad_content_logo_bg:I = 0x7f0700c1

.field public static final ksad_default_app_icon:I = 0x7f0700c2

.field public static final ksad_download_progress_mask_bg:I = 0x7f0700c3

.field public static final ksad_draw_bottom_bg:I = 0x7f0700c4

.field public static final ksad_draw_card_close:I = 0x7f0700c5

.field public static final ksad_draw_card_white_bg:I = 0x7f0700c6

.field public static final ksad_draw_concert_light_bg:I = 0x7f0700c7

.field public static final ksad_draw_convert_light_press:I = 0x7f0700c8

.field public static final ksad_draw_convert_light_unpress:I = 0x7f0700c9

.field public static final ksad_draw_convert_normal_bg:I = 0x7f0700ca

.field public static final ksad_draw_download_progress:I = 0x7f0700cb

.field public static final ksad_draw_float_white_bg:I = 0x7f0700cc

.field public static final ksad_emotion_loading:I = 0x7f0700cd

.field public static final ksad_feed_app_download_before_bg:I = 0x7f0700ce

.field public static final ksad_feed_download_progress:I = 0x7f0700cf

.field public static final ksad_feed_immerse_image_bg:I = 0x7f0700d0

.field public static final ksad_install_tips_bg:I = 0x7f0700d1

.field public static final ksad_install_tips_btn_install_bg:I = 0x7f0700d2

.field public static final ksad_install_tips_ic_close:I = 0x7f0700d3

.field public static final ksad_interstitial_mute:I = 0x7f0700d4

.field public static final ksad_interstitial_play:I = 0x7f0700d5

.field public static final ksad_interstitial_unmute:I = 0x7f0700d6

.field public static final ksad_loading_entry:I = 0x7f0700d7

.field public static final ksad_logo_gray:I = 0x7f0700d8

.field public static final ksad_logo_white:I = 0x7f0700d9

.field public static final ksad_message_toast_2_bg:I = 0x7f0700da

.field public static final ksad_message_toast_bg:I = 0x7f0700db

.field public static final ksad_native_video_duration_bg:I = 0x7f0700dc

.field public static final ksad_navi_back_selector:I = 0x7f0700dd

.field public static final ksad_navi_close_selector:I = 0x7f0700de

.field public static final ksad_navigation_back:I = 0x7f0700df

.field public static final ksad_navigation_back_pressed:I = 0x7f0700e0

.field public static final ksad_navigation_close:I = 0x7f0700e1

.field public static final ksad_navigation_close_pressed:I = 0x7f0700e2

.field public static final ksad_no_data_img:I = 0x7f0700e3

.field public static final ksad_no_video_img:I = 0x7f0700e4

.field public static final ksad_notification_default_icon:I = 0x7f0700e5

.field public static final ksad_notification_install_bg:I = 0x7f0700e6

.field public static final ksad_notification_progress:I = 0x7f0700e7

.field public static final ksad_notification_small_icon:I = 0x7f0700e8

.field public static final ksad_page_close:I = 0x7f0700e9

.field public static final ksad_page_loading_data_error:I = 0x7f0700ea

.field public static final ksad_page_loading_data_limit_error:I = 0x7f0700eb

.field public static final ksad_page_loading_network_error:I = 0x7f0700ec

.field public static final ksad_photo_default_author_icon:I = 0x7f0700ed

.field public static final ksad_realted_video_cover_bg:I = 0x7f0700ee

.field public static final ksad_reward_call_bg:I = 0x7f0700ef

.field public static final ksad_reward_icon_detail:I = 0x7f0700f0

.field public static final ksad_reward_icon_end:I = 0x7f0700f1

.field public static final ksad_round_light:I = 0x7f0700f2

.field public static final ksad_sdk_logo:I = 0x7f0700f3

.field public static final ksad_splash_logo:I = 0x7f0700f4

.field public static final ksad_splash_logo_bg:I = 0x7f0700f5

.field public static final ksad_splash_mute:I = 0x7f0700f6

.field public static final ksad_splash_mute_pressed:I = 0x7f0700f7

.field public static final ksad_splash_preload:I = 0x7f0700f8

.field public static final ksad_splash_sound_selector:I = 0x7f0700f9

.field public static final ksad_splash_unmute:I = 0x7f0700fa

.field public static final ksad_splash_unmute_pressed:I = 0x7f0700fb

.field public static final ksad_splash_vplus_close:I = 0x7f0700fc

.field public static final ksad_toast_text:I = 0x7f0700fd

.field public static final ksad_trend_panel_item_cover_bg:I = 0x7f0700fe

.field public static final ksad_tube_episode_cover_bg:I = 0x7f0700ff

.field public static final ksad_video_actionbar_app_progress:I = 0x7f070100

.field public static final ksad_video_actionbar_cover_bg:I = 0x7f070101

.field public static final ksad_video_actionbar_cover_normal:I = 0x7f070102

.field public static final ksad_video_actionbar_cover_pressed:I = 0x7f070103

.field public static final ksad_video_actionbar_h5_bg:I = 0x7f070104

.field public static final ksad_video_app_12_bg:I = 0x7f070105

.field public static final ksad_video_app_16_bg:I = 0x7f070106

.field public static final ksad_video_app_20_bg:I = 0x7f070107

.field public static final ksad_video_btn_bg:I = 0x7f070108

.field public static final ksad_video_closedialog_bg:I = 0x7f070109

.field public static final ksad_video_install_bg:I = 0x7f07010a

.field public static final ksad_video_play:I = 0x7f07010b

.field public static final ksad_video_player_back_btn:I = 0x7f07010c

.field public static final ksad_video_player_exit_fullscreen_btn:I = 0x7f07010d

.field public static final ksad_video_player_fullscreen_btn:I = 0x7f07010e

.field public static final ksad_video_player_pause_btn:I = 0x7f07010f

.field public static final ksad_video_player_pause_center:I = 0x7f070110

.field public static final ksad_video_player_play_btn:I = 0x7f070111

.field public static final ksad_video_player_play_center:I = 0x7f070112

.field public static final ksad_video_progress:I = 0x7f070113

.field public static final ksad_video_progress_normal:I = 0x7f070114

.field public static final ksad_video_reward_icon:I = 0x7f070115

.field public static final ksad_video_skip_icon:I = 0x7f070116

.field public static final ksad_video_sound_close:I = 0x7f070117

.field public static final ksad_video_sound_open:I = 0x7f070118

.field public static final ksad_video_sound_selector:I = 0x7f070119

.field public static final ksad_web_exit_intercept_dialog_bg:I = 0x7f07011a

.field public static final ksad_web_exit_intercept_negative_btn_bg:I = 0x7f07011b

.field public static final ksad_web_exit_intercept_positive_btn_bg:I = 0x7f07011c

.field public static final ksad_web_tip_bar_close_button:I = 0x7f07011d

.field public static final notification_action_background:I = 0x7f070121

.field public static final notification_bg:I = 0x7f070122

.field public static final notification_bg_low:I = 0x7f070123

.field public static final notification_bg_low_normal:I = 0x7f070124

.field public static final notification_bg_low_pressed:I = 0x7f070125

.field public static final notification_bg_normal:I = 0x7f070126

.field public static final notification_bg_normal_pressed:I = 0x7f070127

.field public static final notification_icon_background:I = 0x7f070128

.field public static final notification_template_icon_bg:I = 0x7f070129

.field public static final notification_template_icon_low_bg:I = 0x7f07012a

.field public static final notification_tile_bg:I = 0x7f07012b

.field public static final notify_panel_notification_icon_bg:I = 0x7f07012c

.field public static final tooltip_frame_dark:I = 0x7f07012e

.field public static final tooltip_frame_light:I = 0x7f07012f


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
