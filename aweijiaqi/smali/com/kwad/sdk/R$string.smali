.class public final Lcom/kwad/sdk/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0e0000

.field public static final abc_action_bar_up_description:I = 0x7f0e0001

.field public static final abc_action_menu_overflow_description:I = 0x7f0e0002

.field public static final abc_action_mode_done:I = 0x7f0e0003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e0004

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e0005

.field public static final abc_capital_off:I = 0x7f0e0006

.field public static final abc_capital_on:I = 0x7f0e0007

.field public static final abc_font_family_body_1_material:I = 0x7f0e0008

.field public static final abc_font_family_body_2_material:I = 0x7f0e0009

.field public static final abc_font_family_button_material:I = 0x7f0e000a

.field public static final abc_font_family_caption_material:I = 0x7f0e000b

.field public static final abc_font_family_display_1_material:I = 0x7f0e000c

.field public static final abc_font_family_display_2_material:I = 0x7f0e000d

.field public static final abc_font_family_display_3_material:I = 0x7f0e000e

.field public static final abc_font_family_display_4_material:I = 0x7f0e000f

.field public static final abc_font_family_headline_material:I = 0x7f0e0010

.field public static final abc_font_family_menu_material:I = 0x7f0e0011

.field public static final abc_font_family_subhead_material:I = 0x7f0e0012

.field public static final abc_font_family_title_material:I = 0x7f0e0013

.field public static final abc_search_hint:I = 0x7f0e001e

.field public static final abc_searchview_description_clear:I = 0x7f0e001f

.field public static final abc_searchview_description_query:I = 0x7f0e0020

.field public static final abc_searchview_description_search:I = 0x7f0e0021

.field public static final abc_searchview_description_submit:I = 0x7f0e0022

.field public static final abc_searchview_description_voice:I = 0x7f0e0023

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e0024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e0025

.field public static final abc_toolbar_collapse_description:I = 0x7f0e0026

.field public static final app_name:I = 0x7f0e003b

.field public static final ksad_ad_default_author:I = 0x7f0e0045

.field public static final ksad_ad_default_username:I = 0x7f0e0046

.field public static final ksad_ad_default_username_normal:I = 0x7f0e0047

.field public static final ksad_ad_function_disable:I = 0x7f0e0048

.field public static final ksad_click_to_next_video:I = 0x7f0e0049

.field public static final ksad_data_error_toast:I = 0x7f0e004a

.field public static final ksad_default_no_more_tip_or_toast_txt:I = 0x7f0e004b

.field public static final ksad_entry_tab_like_format:I = 0x7f0e004c

.field public static final ksad_half_page_loading_error_tip:I = 0x7f0e004d

.field public static final ksad_half_page_loading_no_comment_tip:I = 0x7f0e004e

.field public static final ksad_half_page_loading_no_related_tip:I = 0x7f0e004f

.field public static final ksad_install_tips:I = 0x7f0e0050

.field public static final ksad_look_related_button:I = 0x7f0e0051

.field public static final ksad_look_related_title:I = 0x7f0e0052

.field public static final ksad_network_dataFlow_tip:I = 0x7f0e0053

.field public static final ksad_network_error_toast:I = 0x7f0e0054

.field public static final ksad_page_load_more_tip:I = 0x7f0e0055

.field public static final ksad_page_load_no_more_tip:I = 0x7f0e0056

.field public static final ksad_page_loading_data_error_sub_title:I = 0x7f0e0057

.field public static final ksad_page_loading_data_error_title:I = 0x7f0e0058

.field public static final ksad_page_loading_data_limit_error_title:I = 0x7f0e0059

.field public static final ksad_page_loading_error_retry:I = 0x7f0e005a

.field public static final ksad_page_loading_network_error_sub_title:I = 0x7f0e005b

.field public static final ksad_page_loading_network_error_title:I = 0x7f0e005c

.field public static final ksad_photo_hot_enter_label_text:I = 0x7f0e005d

.field public static final ksad_photo_hot_enter_watch_count_format:I = 0x7f0e005e

.field public static final ksad_photo_hot_enter_watch_extra_button_format:I = 0x7f0e005f

.field public static final ksad_photo_hot_enter_watch_extra_button_format_v2:I = 0x7f0e0060

.field public static final ksad_photo_hot_scroll_more_hot_label:I = 0x7f0e0061

.field public static final ksad_reward_default_tip:I = 0x7f0e0062

.field public static final ksad_reward_success_tip:I = 0x7f0e0063

.field public static final ksad_slide_left_tips:I = 0x7f0e0064

.field public static final ksad_slide_up_tips:I = 0x7f0e0065

.field public static final ksad_text_placeholder:I = 0x7f0e0066

.field public static final ksad_trend_is_no_valid:I = 0x7f0e0067

.field public static final ksad_trend_list_item_photo_count_format:I = 0x7f0e0068

.field public static final ksad_trend_list_panel_title:I = 0x7f0e0069

.field public static final ksad_trend_title_info_format:I = 0x7f0e006a

.field public static final ksad_tube_author_name_label_text:I = 0x7f0e006b

.field public static final ksad_tube_enter_paly_count:I = 0x7f0e006c

.field public static final ksad_tube_episode_index:I = 0x7f0e006d

.field public static final ksad_tube_hot_list_label_string:I = 0x7f0e006e

.field public static final ksad_tube_more_episode:I = 0x7f0e006f

.field public static final ksad_tube_update_default:I = 0x7f0e0070

.field public static final ksad_tube_update_finished_format_text:I = 0x7f0e0071

.field public static final ksad_tube_update_unfinished_format_text:I = 0x7f0e0072

.field public static final ksad_video_no_found:I = 0x7f0e0073

.field public static final ksad_watch_next_video:I = 0x7f0e0074

.field public static final search_menu_title:I = 0x7f0e007b

.field public static final status_bar_notification_info_overflow:I = 0x7f0e007c


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
