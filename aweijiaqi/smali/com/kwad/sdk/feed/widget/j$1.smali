.class Lcom/kwad/sdk/feed/widget/j$1;
.super Lcom/kwad/sdk/core/download/b/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kwad/sdk/feed/widget/j;-><init>(Landroid/content/Context;Lcom/kwad/sdk/core/response/model/AdTemplate;Lcom/kwad/sdk/core/video/videoview/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/kwad/sdk/feed/widget/j;


# direct methods
.method constructor <init>(Lcom/kwad/sdk/feed/widget/j;)V
    .locals 0

    iput-object p1, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-direct {p0}, Lcom/kwad/sdk/core/download/b/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/a;->b(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object p1, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {p1}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadFailed()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->u(Lcom/kwad/sdk/core/response/model/AdInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadFinished()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->c(Lcom/kwad/sdk/feed/widget/j;)Lcom/kwad/sdk/core/response/model/AdTemplate;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->a(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadStarted()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onIdle()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->u(Lcom/kwad/sdk/core/response/model/AdInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onInstalled()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->j(Lcom/kwad/sdk/core/response/model/AdInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {v0}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onProgressUpdate(I)V
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/a;->a(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/kwad/sdk/feed/widget/j;->a(Lcom/kwad/sdk/feed/widget/j;Ljava/lang/String;)Ljava/lang/String;

    iget-object p1, p0, Lcom/kwad/sdk/feed/widget/j$1;->b:Lcom/kwad/sdk/feed/widget/j;

    invoke-static {p1}, Lcom/kwad/sdk/feed/widget/j;->b(Lcom/kwad/sdk/feed/widget/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/kwad/sdk/feed/widget/j;->a(Ljava/lang/String;)V

    return-void
.end method
