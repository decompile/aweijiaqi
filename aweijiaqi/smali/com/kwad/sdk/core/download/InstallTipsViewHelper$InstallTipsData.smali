.class Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/core/download/InstallTipsViewHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InstallTipsData"
.end annotation


# static fields
.field private static MAX_APP_NAME_LENGTH:I = 0x8

.field private static final serialVersionUID:J = 0x4e3c0fe56a7ab1efL


# instance fields
.field private displayContent:Ljava/lang/String;

.field private downloadId:Ljava/lang/String;

.field private iconUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/content/Context;Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;
    .locals 5

    new-instance v0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;

    invoke-direct {v0}, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;-><init>()V

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->k(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->iconUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->h(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/kwad/sdk/core/response/model/AdInfo;->downloadId:Ljava/lang/String;

    iput-object v1, v0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->downloadId:Ljava/lang/String;

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->h(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object p1

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/a;->o(Lcom/kwad/sdk/core/response/model/AdInfo;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sget v2, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->MAX_APP_NAME_LENGTH:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-lt v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->MAX_APP_NAME_LENGTH:I

    sub-int/2addr v2, v4

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "..."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    sget v1, Lcom/kwad/sdk/R$string;->ksad_install_tips:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->displayContent:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getDisplayContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->displayContent:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->downloadId:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/core/download/InstallTipsViewHelper$InstallTipsData;->iconUrl:Ljava/lang/String;

    return-object v0
.end method
