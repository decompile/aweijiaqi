.class public Lcom/kwad/sdk/core/i/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/core/i/b;
.implements Lcom/kwad/sdk/utils/au$a;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Lcom/kwad/sdk/utils/au;

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/kwad/sdk/core/i/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/kwad/sdk/api/core/fragment/KsFragment;

.field private e:Landroid/view/View;

.field private f:I

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/kwad/sdk/api/core/fragment/KsFragment;Landroid/view/View;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/kwad/sdk/core/i/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lcom/kwad/sdk/core/i/a;->d:Lcom/kwad/sdk/api/core/fragment/KsFragment;

    iput-object p2, p0, Lcom/kwad/sdk/core/i/a;->e:Landroid/view/View;

    iput p3, p0, Lcom/kwad/sdk/core/i/a;->f:I

    new-instance p1, Lcom/kwad/sdk/utils/au;

    invoke-direct {p1, p0}, Lcom/kwad/sdk/utils/au;-><init>(Lcom/kwad/sdk/utils/au$a;)V

    iput-object p1, p0, Lcom/kwad/sdk/core/i/a;->b:Lcom/kwad/sdk/utils/au;

    return-void
.end method

.method private a(Lcom/kwad/sdk/api/core/fragment/KsFragment;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/kwad/sdk/api/core/fragment/KsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/kwad/sdk/api/core/fragment/KsFragment;->isAllFragmentIsHidden()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/kwad/sdk/api/core/fragment/KsFragment;->isVisible()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/kwad/sdk/core/i/c;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_2

    invoke-interface {v1}, Lcom/kwad/sdk/core/i/c;->a_()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lcom/kwad/sdk/core/i/c;->e()V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private e()Z
    .locals 3

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->e:Landroid/view/View;

    iget v1, p0, Lcom/kwad/sdk/core/i/a;->f:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/at;->a(Landroid/view/View;IZ)Z

    move-result v0

    return v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start notifyPageVisible by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kwad/sdk/core/i/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FragmentPageVisibleHelper"

    invoke-static {v2, v0}, Lcom/kwad/sdk/core/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/kwad/sdk/core/i/a;->b(Z)V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start notifyPageInVisible by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/kwad/sdk/core/i/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FragmentPageVisibleHelper"

    invoke-static {v2, v0}, Lcom/kwad/sdk/core/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/kwad/sdk/core/i/a;->b(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->b:Lcom/kwad/sdk/utils/au;

    const/16 v1, 0x29a

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/utils/au;->sendEmptyMessage(I)Z

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 3

    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x29a

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/kwad/sdk/core/i/a;->d:Lcom/kwad/sdk/api/core/fragment/KsFragment;

    if-nez p1, :cond_0

    const-string p1, "FragmentPageVisibleHelper"

    const-string v0, "mFragment is null"

    invoke-static {p1, v0}, Lcom/kwad/sdk/core/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/kwad/sdk/core/i/a;->a(Lcom/kwad/sdk/api/core/fragment/KsFragment;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "message fragment"

    iput-object p1, p0, Lcom/kwad/sdk/core/i/a;->g:Ljava/lang/String;

    :cond_1
    invoke-direct {p0}, Lcom/kwad/sdk/core/i/a;->g()V

    goto :goto_0

    :cond_2
    const-string p1, "message view"

    iput-object p1, p0, Lcom/kwad/sdk/core/i/a;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/kwad/sdk/core/i/a;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/kwad/sdk/core/i/a;->f()V

    :goto_0
    iget-object p1, p0, Lcom/kwad/sdk/core/i/a;->b:Lcom/kwad/sdk/utils/au;

    const-wide/16 v1, 0x1f4

    invoke-virtual {p1, v0, v1, v2}, Lcom/kwad/sdk/utils/au;->sendEmptyMessageDelayed(IJ)Z

    :cond_3
    return-void
.end method

.method public a(Lcom/kwad/sdk/core/i/c;)V
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/utils/y;->a()V

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->c:Ljava/util/Set;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/kwad/sdk/core/i/a;->c:Ljava/util/Set;

    :cond_1
    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/kwad/sdk/core/i/c;->a_()V

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/kwad/sdk/core/i/c;->e()V

    :goto_0
    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->b:Lcom/kwad/sdk/utils/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/utils/au;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public b(Lcom/kwad/sdk/core/i/c;)V
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/utils/y;->a()V

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/kwad/sdk/core/i/a;->c:Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 2

    const-string v0, "onFragmentPause"

    const-string v1, "FragmentPageVisibleHelper"

    invoke-static {v1, v0}, Lcom/kwad/sdk/core/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/kwad/sdk/core/i/a;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/kwad/sdk/core/i/a;->g()V

    return-void
.end method
