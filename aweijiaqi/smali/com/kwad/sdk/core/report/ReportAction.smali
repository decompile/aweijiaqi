.class public Lcom/kwad/sdk/core/report/ReportAction;
.super Lcom/kwad/sdk/core/report/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;,
        Lcom/kwad/sdk/core/report/ReportAction$b;,
        Lcom/kwad/sdk/core/report/ReportAction$a;
    }
.end annotation


# instance fields
.field public A:J

.field public B:J

.field public C:Ljava/lang/String;

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:Ljava/lang/String;

.field public K:Lcom/kwad/sdk/core/scene/URLPackage;

.field public L:Lcom/kwad/sdk/core/scene/URLPackage;

.field public M:J

.field public N:I

.field public O:Ljava/lang/String;

.field public P:I

.field public Q:I

.field public R:J

.field public S:J

.field public T:I

.field public U:I

.field public V:I

.field public W:Lorg/json/JSONArray;

.field public X:Lorg/json/JSONArray;

.field public Y:Ljava/lang/String;

.field public Z:Ljava/lang/String;

.field public aA:J

.field public aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

.field public aC:Ljava/lang/String;

.field public aD:I

.field public aE:I

.field public aF:Ljava/lang/String;

.field public aG:Ljava/lang/String;

.field public aH:Lorg/json/JSONObject;

.field public aI:Lorg/json/JSONArray;

.field public aJ:J

.field public aK:I

.field public aL:I

.field public aM:I

.field public aN:I

.field public aO:I

.field public aP:J

.field public aQ:Lorg/json/JSONArray;

.field public aR:Z

.field public aS:Ljava/lang/String;

.field public aT:I

.field public aU:I

.field public aV:I

.field public aW:Ljava/lang/String;

.field public aX:J

.field public aY:J

.field public aZ:I

.field public aa:Ljava/lang/String;

.field public ab:Ljava/lang/String;

.field public ac:J

.field public ad:J

.field public ae:I

.field public af:Ljava/lang/String;

.field public ag:Lcom/kwad/sdk/core/report/ReportAction$a;

.field public ah:I

.field public ai:I

.field public aj:Ljava/lang/String;

.field public ak:J

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public an:J

.field public ao:J

.field public ap:J

.field public aq:I

.field public ar:I

.field public as:I

.field public at:I

.field public au:Lorg/json/JSONArray;

.field public av:I

.field public aw:I

.field public ax:Ljava/lang/String;

.field public ay:Ljava/lang/String;

.field public az:I

.field public transient b:Lcom/kwad/sdk/internal/api/SceneImpl;

.field public ba:Ljava/lang/String;

.field public bb:J

.field public bc:J

.field public bd:J

.field public be:J

.field public bf:Ljava/lang/String;

.field public bg:Ljava/lang/String;

.field public bh:J

.field public bi:J

.field public bj:Lorg/json/JSONArray;

.field public bk:J

.field public bl:J

.field public bm:J

.field public bn:Lorg/json/JSONArray;

.field public bo:I

.field public bp:Ljava/lang/String;

.field public bq:Ljava/lang/String;

.field public br:Ljava/lang/String;

.field public bs:Ljava/lang/String;

.field public bt:Ljava/lang/String;

.field public transient c:Lcom/kwad/sdk/core/response/model/AdTemplate;

.field public d:J

.field public e:Ljava/lang/String;

.field public f:J

.field public g:I

.field public h:J

.field public i:J

.field public j:J

.field public k:Lorg/json/JSONObject;

.field public l:Lorg/json/JSONObject;

.field public m:J

.field public n:I

.field public o:I

.field public p:J

.field public q:J

.field public r:J

.field public s:J

.field public t:J

.field public u:J

.field public v:J

.field public w:J

.field public x:J

.field public y:J

.field public z:J


# direct methods
.method public constructor <init>(J)V
    .locals 4

    invoke-direct {p0}, Lcom/kwad/sdk/core/report/c;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:I

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    new-instance v2, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-direct {v2}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;-><init>()V

    iput-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aN:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aO:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aT:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aU:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aV:I

    iput-wide p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:J

    return-void
.end method

.method public constructor <init>(JLcom/kwad/sdk/core/response/model/AdTemplate;)V
    .locals 4

    invoke-direct {p0}, Lcom/kwad/sdk/core/report/c;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:I

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    new-instance v2, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-direct {v2}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;-><init>()V

    iput-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aN:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aO:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aT:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aU:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aV:I

    iput-wide p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:J

    iput-object p3, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lcom/kwad/sdk/core/report/c;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:I

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    new-instance v2, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-direct {v2}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;-><init>()V

    iput-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aN:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aO:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aT:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aU:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aV:I

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/kwad/sdk/core/report/ReportAction;->parseJson(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private a(J)V
    .locals 2

    const-wide/16 v0, 0x1

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:J

    return-void
.end method

.method private a(Lcom/kwad/sdk/core/response/model/AdTemplate;)V
    .locals 3

    const/4 v0, 0x3

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:I

    const-class v0, Lcom/kwad/sdk/plugin/b;

    invoke-static {v0}, Lcom/kwad/sdk/plugin/f;->a(Ljava/lang/Class;)Lcom/kwad/sdk/plugin/d;

    move-result-object v0

    check-cast v0, Lcom/kwad/sdk/plugin/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/kwad/sdk/plugin/b;->e()I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aT:I

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->d:J

    invoke-static {}, Lcom/kwad/sdk/core/report/l;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->e:Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/kwad/sdk/core/report/l;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :goto_0
    invoke-static {}, Lcom/kwad/sdk/core/report/l;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->h:J

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/kwad/sdk/core/response/model/AdTemplate;->getShowPosition()I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/kwad/sdk/core/report/ReportAction;->a(J)V

    :cond_1
    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->getPosId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:J

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->getUrlPackage()Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->getAdStyle()I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    goto :goto_2

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    goto :goto_1

    :cond_3
    :goto_2
    if-eqz p1, :cond_b

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->e(Lcom/kwad/sdk/core/response/model/AdTemplate;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->j:J

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->f(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :cond_4
    :goto_3
    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->g(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    :try_start_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :cond_5
    :goto_4
    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->c(Lcom/kwad/sdk/core/response/model/AdTemplate;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:J

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->d(Lcom/kwad/sdk/core/response/model/AdTemplate;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    iget v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->realShowType:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->d(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->p:J

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->c(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:J

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->f(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->e(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->af:Ljava/lang/String;

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->i(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aj:Ljava/lang/String;

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->h(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:J

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->j(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->al:Ljava/lang/String;

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->k(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->an:J

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/d;->l(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->am:Ljava/lang/String;

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPreloadData:Lcom/kwad/sdk/core/response/model/PreloadData;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPreloadData:Lcom/kwad/sdk/core/response/model/PreloadData;

    iget-boolean v0, v0, Lcom/kwad/sdk/core/response/model/PreloadData;->isPreload:Z

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    :goto_5
    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:I

    goto :goto_7

    :cond_7
    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->h(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/a;->i(Lcom/kwad/sdk/core/response/model/AdInfo;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->p:J

    iget-object v1, v0, Lcom/kwad/sdk/core/response/model/AdInfo;->adBaseInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;

    iget-wide v1, v1, Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;->creativeId:J

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:J

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/a;->b(Lcom/kwad/sdk/core/response/model/AdInfo;)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:J

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo;->advertiserInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdvertiserInfo;

    iget-wide v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo$AdvertiserInfo;->userId:J

    :goto_6
    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    goto :goto_7

    :cond_8
    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->n(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/live/mode/LiveInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-static {v0}, Lcom/kwad/sdk/live/mode/a;->a(Lcom/kwad/sdk/live/mode/LiveInfo;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;->liveStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-static {v0}, Lcom/kwad/sdk/live/mode/a;->c(Lcom/kwad/sdk/live/mode/LiveInfo;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;->expTag:Ljava/lang/String;

    invoke-static {v0}, Lcom/kwad/sdk/live/mode/a;->b(Lcom/kwad/sdk/live/mode/LiveInfo;)J

    move-result-wide v0

    goto :goto_6

    :cond_9
    :goto_7
    iget v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mMediaPlayerType:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:I

    iget v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mIsLeftSlipStatus:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->as:I

    iget v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPhotoResponseType:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:I

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPageInfo:Lcom/kwad/sdk/core/response/model/PageInfo;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPageInfo:Lcom/kwad/sdk/core/response/model/PageInfo;

    iget v0, v0, Lcom/kwad/sdk/core/response/model/PageInfo;->pageType:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aK:I

    :cond_a
    invoke-static {p1}, Lcom/kwad/sdk/core/response/b/c;->i(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/PhotoInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/PhotoInfo;->baseInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo$BaseInfo;

    iget v0, v0, Lcom/kwad/sdk/core/response/model/PhotoInfo$BaseInfo;->contentSourceType:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    :cond_b
    invoke-static {}, Lcom/kwad/sdk/core/report/ReportAction$a;->a()Lcom/kwad/sdk/core/report/ReportAction$a;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:Lcom/kwad/sdk/core/report/ReportAction$a;

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-nez v0, :cond_c

    if-eqz p1, :cond_c

    iget-object p1, p1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    iput-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    :cond_c
    iget-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/kwad/sdk/internal/api/SceneImpl;->getPosId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:J

    iget-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    invoke-virtual {p1}, Lcom/kwad/sdk/internal/api/SceneImpl;->getUrlPackage()Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object p1

    iput-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    :cond_d
    iget-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz p1, :cond_e

    invoke-static {}, Lcom/kwad/sdk/core/scene/a;->a()Lcom/kwad/sdk/core/scene/a;

    move-result-object p1

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v0, v0, Lcom/kwad/sdk/core/scene/URLPackage;->identity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/kwad/sdk/core/scene/a;->b(Ljava/lang/String;)Lcom/kwad/sdk/core/scene/EntryPackage;

    move-result-object p1

    iget-object v0, p1, Lcom/kwad/sdk/core/scene/EntryPackage;->entryPageSource:Ljava/lang/String;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->J:Ljava/lang/String;

    iget-object p1, p1, Lcom/kwad/sdk/core/scene/EntryPackage;->entryId:Ljava/lang/String;

    iput-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:Ljava/lang/String;

    invoke-static {}, Lcom/kwad/sdk/core/scene/a;->a()Lcom/kwad/sdk/core/scene/a;

    move-result-object p1

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v0, v0, Lcom/kwad/sdk/core/scene/URLPackage;->identity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/kwad/sdk/core/scene/a;->a(Ljava/lang/String;)Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object p1

    iput-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:Lcom/kwad/sdk/core/scene/URLPackage;

    :cond_e
    return-void
.end method


# virtual methods
.method public a()Lcom/kwad/sdk/core/report/ReportAction;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    invoke-direct {p0, v0}, Lcom/kwad/sdk/core/report/ReportAction;->a(Lcom/kwad/sdk/core/response/model/AdTemplate;)V

    return-object p0
.end method

.method public afterParseJson(Lorg/json/JSONObject;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/kwad/sdk/core/report/c;->afterParseJson(Lorg/json/JSONObject;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:I

    const/4 v0, -0x1

    const-string v1, "adStyle"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    const-string v1, "reportType"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    const-string v1, "playerControlledType"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    const-string v1, "num"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aE:I

    const-string v1, "state"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aD:I

    const-wide/16 v1, -0x1

    const-string v3, "relatedFromPhotoId"

    invoke-virtual {p1, v3, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    const-string v3, "relatedContentSourceType"

    invoke-virtual {p1, v3, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    const-string v1, "hotCompType"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    const-string v1, "timeSpend"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    const-string v1, "loadingDuration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->bl:J

    const-string v1, "loadingDurationLimt"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->bm:J

    const-string v1, "playerTypeInfo"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aU:I

    const-string v0, "actionId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public afterToJson(Lorg/json/JSONObject;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/kwad/sdk/core/report/c;->afterToJson(Lorg/json/JSONObject;)V

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    const-string v1, "actionId"

    invoke-static {p1, v1, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:I

    if-lez v0, :cond_0

    const-string v1, "adStyle"

    invoke-static {p1, v1, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_0
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const-string v2, "reportType"

    invoke-static {p1, v2, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_1
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    if-eq v0, v1, :cond_2

    const-string v2, "playerControlledType"

    invoke-static {p1, v2, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_2
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aE:I

    if-lez v0, :cond_3

    const-string v2, "num"

    invoke-static {p1, v2, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_3
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aD:I

    if-eqz v0, :cond_4

    const-string v2, "state"

    invoke-static {p1, v2, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_4
    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    const-string v0, "relatedFromPhotoId"

    invoke-static {p1, v0, v2, v3}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_5
    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    const-string v0, "relatedContentSourceType"

    invoke-static {p1, v0, v2, v3}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_6
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    if-eq v0, v1, :cond_7

    const-string v1, "hotCompType"

    invoke-static {p1, v1, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_7
    iget-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_8

    const-string v4, "timeSpend"

    invoke-static {p1, v4, v0, v1}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_8
    iget-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->bl:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_9

    const-string v4, "loadingDuration"

    invoke-static {p1, v4, v0, v1}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_9
    iget-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->bm:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_a

    const-string v2, "loadingDurationLimt"

    invoke-static {p1, v2, v0, v1}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_a
    iget v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aU:I

    const-string v1, "playerTypeInfo"

    invoke-static {p1, v1, v0}, Lcom/kwad/sdk/utils/q;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    return-void
.end method

.method public b()V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/core/h/b;->a()Lcom/kwad/sdk/core/h/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/core/h/b;->b()Z

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aM:I

    invoke-virtual {v0}, Lcom/kwad/sdk/core/h/b;->c()I

    move-result v1

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aN:I

    invoke-virtual {v0}, Lcom/kwad/sdk/core/h/b;->d()I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aO:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "[actionType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",actionId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",viewModeType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",moduleName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",componentPosition:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",tubeId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",entryId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",blockDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",blockTimes:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->B:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",intervalDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",allIntervalDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->z:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",flowSdk:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",playEnd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",dragProgressType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",dragProgressPhotoDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",dragProgressVideoTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",contentSourceType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",trendId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->an:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",trendName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->am:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",tubeName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",episodeName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",seq:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, ",impAdExtra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, ",position:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",contentType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",playerType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",preloadType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",realShowType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",photoDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",startDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",playDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",stayDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",enterType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",entryPageSource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",stayLength:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->w:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const-string v1, ",hotCompType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_0
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    if-eq v1, v2, :cond_1

    const-string v1, ",playerControlledType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_1
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:I

    if-eqz v1, :cond_2

    const-string v1, ",adAggPageSource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_2
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    if-eq v1, v2, :cond_3

    const-string v1, ",reportType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_3
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_4

    const-string v1, ",urlPackage:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:Lcom/kwad/sdk/core/scene/URLPackage;

    iget v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_4
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_5

    const-string v1, ",referPage:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:Lcom/kwad/sdk/core/scene/URLPackage;

    iget v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_5
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aE:I

    if-le v1, v2, :cond_6

    const-string v1, ",num:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aE:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_6
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aD:I

    if-eqz v1, :cond_7

    const-string v1, ",state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aD:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_7
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aH:Lorg/json/JSONObject;

    if-eqz v1, :cond_8

    const-string v1, ",appExt:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aH:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    :cond_8
    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_9

    const-string v1, ",timeSpend:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aP:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    :cond_9
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/al;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ",videoCurrentUrl:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_a
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/al;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ",photoSize:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_b
    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    const-wide/16 v3, -0x1

    cmp-long v5, v1, v3

    if-eqz v5, :cond_c

    const-string v1, ",relatedFromPhotoId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    :cond_c
    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_d

    const-string v1, ",relatedContentSourceType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    :cond_d
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
