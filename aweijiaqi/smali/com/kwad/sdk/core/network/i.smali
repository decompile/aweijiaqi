.class public abstract Lcom/kwad/sdk/core/network/i;
.super Lcom/kwad/sdk/core/network/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcom/kwad/sdk/core/network/g;",
        "T:",
        "Lcom/kwad/sdk/core/network/BaseResultData;",
        ">",
        "Lcom/kwad/sdk/core/network/a<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/kwad/sdk/core/network/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/kwad/sdk/core/network/h<",
            "TR;TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/kwad/sdk/core/network/b/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/kwad/sdk/core/network/a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    new-instance v0, Lcom/kwad/sdk/core/network/b/b;

    invoke-direct {v0}, Lcom/kwad/sdk/core/network/b/b;-><init>()V

    iput-object v0, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    return-void
.end method


# virtual methods
.method protected a(Lcom/kwad/sdk/core/network/g;Lcom/kwad/sdk/core/network/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/kwad/sdk/core/network/c;",
            ")V"
        }
    .end annotation

    if-nez p2, :cond_1

    const-string p2, "Networking"

    const-string v0, "request responseBase is null"

    invoke-static {p2, v0}, Lcom/kwad/sdk/core/d/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz p2, :cond_0

    sget-object v0, Lcom/kwad/sdk/core/network/f;->a:Lcom/kwad/sdk/core/network/f;

    iget v0, v0, Lcom/kwad/sdk/core/network/f;->k:I

    sget-object v1, Lcom/kwad/sdk/core/network/f;->a:Lcom/kwad/sdk/core/network/f;

    iget-object v1, v1, Lcom/kwad/sdk/core/network/f;->l:Ljava/lang/String;

    invoke-interface {p2, p1, v0, v1}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p2, Lcom/kwad/sdk/core/network/c;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget v0, p2, Lcom/kwad/sdk/core/network/c;->a:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    goto :goto_2

    :cond_2
    :try_start_0
    iget-object p2, p2, Lcom/kwad/sdk/core/network/c;->b:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/kwad/sdk/core/network/i;->b(Ljava/lang/String;)Lcom/kwad/sdk/core/network/BaseResultData;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz p2, :cond_3

    sget-object v0, Lcom/kwad/sdk/core/network/f;->b:Lcom/kwad/sdk/core/network/f;

    iget v0, v0, Lcom/kwad/sdk/core/network/f;->k:I

    sget-object v1, Lcom/kwad/sdk/core/network/f;->b:Lcom/kwad/sdk/core/network/f;

    iget-object v1, v1, Lcom/kwad/sdk/core/network/f;->l:Ljava/lang/String;

    invoke-interface {p2, p1, v0, v1}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    invoke-virtual {v0}, Lcom/kwad/sdk/core/network/b/b;->c()V

    invoke-virtual {p2}, Lcom/kwad/sdk/core/network/BaseResultData;->isResultOk()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v0, :cond_5

    iget v1, p2, Lcom/kwad/sdk/core/network/BaseResultData;->result:I

    iget-object p2, p2, Lcom/kwad/sdk/core/network/BaseResultData;->errorMsg:Ljava/lang/String;

    invoke-interface {v0, p1, v1, p2}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_5
    return-void

    :cond_6
    invoke-virtual {p2}, Lcom/kwad/sdk/core/network/BaseResultData;->isDataEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object p2, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz p2, :cond_8

    sget-object v0, Lcom/kwad/sdk/core/network/f;->c:Lcom/kwad/sdk/core/network/f;

    iget v0, v0, Lcom/kwad/sdk/core/network/f;->k:I

    sget-object v1, Lcom/kwad/sdk/core/network/f;->c:Lcom/kwad/sdk/core/network/f;

    iget-object v1, v1, Lcom/kwad/sdk/core/network/f;->l:Ljava/lang/String;

    invoke-interface {p2, p1, v0, v1}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v0, :cond_8

    invoke-interface {v0, p1, p2}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;Lcom/kwad/sdk/core/network/BaseResultData;)V

    :cond_8
    :goto_0
    return-void

    :catch_0
    move-exception p2

    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v0, :cond_9

    sget-object v1, Lcom/kwad/sdk/core/network/f;->b:Lcom/kwad/sdk/core/network/f;

    iget v1, v1, Lcom/kwad/sdk/core/network/f;->k:I

    sget-object v2, Lcom/kwad/sdk/core/network/f;->b:Lcom/kwad/sdk/core/network/f;

    iget-object v2, v2, Lcom/kwad/sdk/core/network/f;->l:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_9
    invoke-static {p2}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    iget-object p1, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    const-string p2, "parseDataError"

    :goto_1
    invoke-virtual {p1, p2}, Lcom/kwad/sdk/core/network/b/b;->b(Ljava/lang/String;)V

    return-void

    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v0, :cond_b

    iget v1, p2, Lcom/kwad/sdk/core/network/c;->a:I

    const-string v2, "\u7f51\u7edc\u9519\u8bef"

    invoke-interface {v0, p1, v1, v2}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_b
    iget-object p1, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpCodeError:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lcom/kwad/sdk/core/network/c;->a:I

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method

.method public a(Lcom/kwad/sdk/core/network/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/kwad/sdk/core/network/h<",
            "TR;TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    invoke-virtual {v0}, Lcom/kwad/sdk/core/network/b/b;->a()V

    iput-object p1, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    invoke-virtual {p0}, Lcom/kwad/sdk/core/network/i;->d()V

    return-void
.end method

.method protected abstract b(Ljava/lang/String;)Lcom/kwad/sdk/core/network/BaseResultData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method protected c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 1

    invoke-super {p0}, Lcom/kwad/sdk/core/network/a;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    return-void
.end method

.method protected f()V
    .locals 6

    invoke-virtual {p0}, Lcom/kwad/sdk/core/network/i;->b()Lcom/kwad/sdk/core/network/g;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;)V

    :cond_0
    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kwad/sdk/KsAdSDKImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/ksad/download/d/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/kwad/sdk/core/network/i;->a:Lcom/kwad/sdk/core/network/h;

    if-eqz v1, :cond_1

    sget-object v2, Lcom/kwad/sdk/core/network/f;->a:Lcom/kwad/sdk/core/network/f;

    iget v2, v2, Lcom/kwad/sdk/core/network/f;->k:I

    sget-object v3, Lcom/kwad/sdk/core/network/f;->a:Lcom/kwad/sdk/core/network/f;

    iget-object v3, v3, Lcom/kwad/sdk/core/network/f;->l:Ljava/lang/String;

    invoke-interface {v1, v0, v2, v3}, Lcom/kwad/sdk/core/network/h;->a(Lcom/kwad/sdk/core/network/g;ILjava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0}, Lcom/kwad/sdk/core/network/g;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    invoke-virtual {v3, v2}, Lcom/kwad/sdk/core/network/b/b;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kwad/sdk/KsAdSDKImpl;->getProxyForHttp()Lcom/kwad/sdk/export/proxy/AdHttpProxy;

    move-result-object v3

    invoke-virtual {p0}, Lcom/kwad/sdk/core/network/i;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lcom/kwad/sdk/core/network/g;->b()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v0}, Lcom/kwad/sdk/core/network/g;->d()Lorg/json/JSONObject;

    move-result-object v5

    invoke-interface {v3, v2, v4, v5}, Lcom/kwad/sdk/export/proxy/AdHttpProxy;->doPost(Ljava/lang/String;Ljava/util/Map;Lorg/json/JSONObject;)Lcom/kwad/sdk/core/network/c;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Lcom/kwad/sdk/core/network/g;->b()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v0}, Lcom/kwad/sdk/core/network/g;->c()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v3, v2, v4, v5}, Lcom/kwad/sdk/export/proxy/AdHttpProxy;->doPost(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lcom/kwad/sdk/core/network/c;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {v2}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    const-string v3, "requestError"

    invoke-virtual {v2, v3}, Lcom/kwad/sdk/core/network/b/b;->b(Ljava/lang/String;)V

    :goto_0
    iget-object v2, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    invoke-virtual {v2}, Lcom/kwad/sdk/core/network/b/b;->b()V

    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/kwad/sdk/core/network/i;->a(Lcom/kwad/sdk/core/network/g;Lcom/kwad/sdk/core/network/c;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :goto_1
    invoke-virtual {p0}, Lcom/kwad/sdk/core/network/i;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/kwad/sdk/core/network/i;->b:Lcom/kwad/sdk/core/network/b/b;

    invoke-virtual {v0}, Lcom/kwad/sdk/core/network/b/b;->d()V

    :cond_4
    return-void
.end method

.method protected g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
