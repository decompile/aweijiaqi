.class Lcom/bytedance/sdk/component/video/a/a/b$1;
.super Ljava/lang/Object;
.source "VideoCacheImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/component/b/b/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/video/a/a/b;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/component/video/a/a/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/video/a/a/b;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V
    .locals 2

    .line 115
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;Z)Z

    .line 116
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;J)J

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/b/b/e;Lcom/bytedance/sdk/component/b/b/ab;)V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    const/4 v2, 0x0

    if-eqz p2, :cond_16

    const/4 v3, 0x0

    .line 125
    :try_start_0
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v4

    invoke-static {v0, v4}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;Z)Z

    .line 126
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 127
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 128
    :try_start_1
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    .line 129
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->b()J

    move-result-wide v5

    iget-object v7, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v7}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v7

    add-long/2addr v5, v7

    invoke-static {v0, v5, v6}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;J)J

    .line 130
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->c()Ljava/io/InputStream;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_0
    if-nez v3, :cond_5

    if-eqz v3, :cond_1

    .line 166
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 169
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_2
    if-eqz p2, :cond_3

    .line 173
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    .line 175
    :cond_3
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->h(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 176
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->i(Lcom/bytedance/sdk/component/video/a/a/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 180
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    :goto_2
    return-void

    :cond_5
    const/16 v0, 0x2000

    :try_start_3
    new-array v5, v0, [B

    .line 139
    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-wide v11, v8

    const/4 v10, 0x0

    :goto_3
    rsub-int v13, v10, 0x2000

    .line 140
    invoke-virtual {v3, v5, v10, v13}, Ljava/io/InputStream;->read([BII)I

    move-result v13

    const/4 v14, -0x1

    const/16 v15, 0x9

    const/16 v16, 0x8

    const/16 v17, 0x7

    const/16 v18, 0x6

    const/16 v19, 0x5

    const/16 v20, 0x4

    const/16 v21, 0x3

    const/16 v22, 0x2

    const/16 v0, 0xa

    const/16 v23, 0x1

    if-eq v13, v14, :cond_9

    add-int/2addr v10, v13

    int-to-long v13, v13

    add-long/2addr v11, v13

    const-wide/16 v13, 0x2000

    .line 143
    rem-long v13, v11, v13

    cmp-long v24, v13, v8

    if-eqz v24, :cond_7

    iget-object v13, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v13}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v13

    iget-object v8, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v8}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v8

    sub-long/2addr v13, v8

    cmp-long v8, v11, v13

    if-nez v8, :cond_6

    goto :goto_4

    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v8, 0x1

    :goto_5
    const-string v9, "VideoCacheImpl"

    const/16 v13, 0xe

    new-array v13, v13, [Ljava/lang/Object;

    const-string v14, "Write segment,execAppend ="

    aput-object v14, v13, v2

    .line 144
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v13, v23

    const-string v14, " offset="

    aput-object v14, v13, v22

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v13, v21

    const-string v14, " totalLength = "

    aput-object v14, v13, v20

    iget-object v14, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v14}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v13, v19

    const-string v14, " saveSize ="

    aput-object v14, v13, v18

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v13, v17

    const-string v14, " startSaved="

    aput-object v14, v13, v16

    iget-object v14, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v14}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v13, v15

    const-string v14, " fileHash="

    aput-object v14, v13, v0

    const/16 v0, 0xb

    iget-object v14, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v14}, Lcom/bytedance/sdk/component/video/a/a/b;->d(Lcom/bytedance/sdk/component/video/a/a/b;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v14

    invoke-virtual {v14}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v0

    const/16 v0, 0xc

    const-string v14, " url="

    aput-object v14, v13, v0

    const/16 v0, 0xd

    iget-object v14, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v14}, Lcom/bytedance/sdk/component/video/a/a/b;->d(Lcom/bytedance/sdk/component/video/a/a/b;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v14

    invoke-virtual {v14}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v0

    invoke-static {v9, v13}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v8, :cond_8

    .line 146
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->e(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 147
    :try_start_4
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->f(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/io/RandomAccessFile;

    move-result-object v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->intValue()I

    move-result v9

    iget-object v13, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v13}, Lcom/bytedance/sdk/component/video/a/a/b;->d(Lcom/bytedance/sdk/component/video/a/a/b;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v13

    invoke-virtual {v13}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object v13

    invoke-static {v0, v5, v9, v10, v13}, Lcom/bytedance/sdk/component/video/d/b;->a(Ljava/io/RandomAccessFile;[BIILjava/lang/String;)V

    .line 148
    monitor-exit v8

    int-to-long v8, v10

    add-long/2addr v6, v8

    const/4 v10, 0x0

    goto :goto_6

    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    :cond_8
    :goto_6
    const/16 v0, 0x2000

    const-wide/16 v8, 0x0

    goto/16 :goto_3

    :cond_9
    const-string v5, "VideoCacheImpl"

    new-array v0, v0, [Ljava/lang/Object;

    const-string v6, "Write segment,Write over, startIndex ="

    aput-object v6, v0, v2

    .line 153
    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v23

    const-string v6, " totalLength = "

    aput-object v6, v0, v22

    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v21

    const-string v6, " saveSize = "

    aput-object v6, v0, v20

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v0, v19

    const-string v6, " writeEndSegment ="

    aput-object v6, v0, v18

    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v6

    iget-object v8, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v8}, Lcom/bytedance/sdk/component/video/a/a/b;->b(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v8, v11, v6

    if-nez v8, :cond_a

    goto :goto_7

    :cond_a
    const/16 v23, 0x0

    :goto_7
    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v0, v17

    const-string v6, " url="

    aput-object v6, v0, v16

    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/a/b;->d(Lcom/bytedance/sdk/component/video/a/a/b;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v15

    invoke-static {v5, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_8

    :catchall_2
    move-exception v0

    goto :goto_9

    .line 155
    :cond_b
    :try_start_6
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;Z)Z

    .line 156
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    iget-object v4, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v4}, Lcom/bytedance/sdk/component/video/a/a/b;->g(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;J)J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-object v4, v3

    :goto_8
    if-eqz v3, :cond_c

    .line 166
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_c
    if-eqz v4, :cond_d

    .line 169
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_d
    if-eqz p2, :cond_e

    .line 173
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    .line 175
    :cond_e
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->h(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_17

    .line 176
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->i(Lcom/bytedance/sdk/component/video/a/a/b;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto/16 :goto_f

    :catchall_3
    move-exception v0

    move-object v4, v3

    .line 160
    :goto_9
    :try_start_8
    iget-object v5, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v5, v2}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;Z)Z

    .line 161
    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    iget-object v5, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v5}, Lcom/bytedance/sdk/component/video/a/a/b;->g(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;J)J

    .line 162
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    if-eqz v3, :cond_f

    .line 166
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_a

    :catchall_4
    move-exception v0

    goto :goto_b

    :cond_f
    :goto_a
    if-eqz v4, :cond_10

    .line 169
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_10
    if-eqz p2, :cond_11

    .line 173
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    .line 175
    :cond_11
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->h(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_17

    .line 176
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->i(Lcom/bytedance/sdk/component/video/a/a/b;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    goto :goto_f

    .line 180
    :goto_b
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_f

    :catchall_5
    move-exception v0

    move-object v2, v0

    if-eqz v3, :cond_12

    .line 166
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_c

    :catchall_6
    move-exception v0

    goto :goto_d

    :cond_12
    :goto_c
    if-eqz v4, :cond_13

    .line 169
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_13
    if-eqz p2, :cond_14

    .line 173
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    .line 175
    :cond_14
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->h(Lcom/bytedance/sdk/component/video/a/a/b;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->c(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_15

    .line 176
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->i(Lcom/bytedance/sdk/component/video/a/a/b;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    goto :goto_e

    .line 180
    :goto_d
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 182
    :cond_15
    :goto_e
    throw v2

    .line 184
    :cond_16
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;Z)Z

    .line 185
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/a/b$1;->a:Lcom/bytedance/sdk/component/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/a/b;->g(Lcom/bytedance/sdk/component/video/a/a/b;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/bytedance/sdk/component/video/a/a/b;->a(Lcom/bytedance/sdk/component/video/a/a/b;J)J

    :cond_17
    :goto_f
    return-void
.end method
