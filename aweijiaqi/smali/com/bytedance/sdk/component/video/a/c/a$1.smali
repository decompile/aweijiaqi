.class Lcom/bytedance/sdk/component/video/a/c/a$1;
.super Ljava/lang/Object;
.source "VideoPreload.java"

# interfaces
.implements Lcom/bytedance/sdk/component/b/b/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/video/a/c/a;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/bytedance/sdk/component/video/a/c/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/video/a/c/a;J)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iput-wide p2, p0, Lcom/bytedance/sdk/component/video/a/c/a$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V
    .locals 2

    .line 120
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {p1}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v0

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const/16 v1, 0x259

    invoke-static {p1, v0, v1, p2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V

    .line 121
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {p1}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/b/b/e;Lcom/bytedance/sdk/component/b/b/ab;)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    .line 128
    iget-wide v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->a:J

    const/4 v6, 0x3

    const-string v7, " Preload size="

    const/4 v8, 0x2

    const-string v9, "Pre finally "

    const/4 v10, 0x4

    const-string v11, "VideoPreload"

    const/4 v12, 0x1

    const/4 v13, 0x0

    if-eqz p2, :cond_10

    .line 131
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v14

    invoke-static {v0, v2, v3, v14}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-eqz p2, :cond_0

    .line 190
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_0
    :goto_0
    new-array v0, v10, [Ljava/lang/Object;

    aput-object v9, v0, v13

    .line 192
    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v12

    aput-object v7, v0, v8

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v6

    invoke-static {v11, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 194
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 196
    :goto_2
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V

    return-void

    .line 136
    :cond_1
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 137
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v14

    if-eqz v0, :cond_2

    if-eqz v14, :cond_2

    .line 140
    iget-wide v4, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->a:J

    invoke-virtual {v14}, Lcom/bytedance/sdk/component/b/b/ac;->b()J

    move-result-wide v16

    add-long v4, v4, v16

    .line 141
    invoke-virtual {v14}, Lcom/bytedance/sdk/component/b/b/ac;->c()Ljava/io/InputStream;

    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v15, v0

    goto :goto_3

    :cond_2
    const-wide/16 v4, 0x0

    const/4 v15, 0x0

    :goto_3
    if-nez v15, :cond_6

    .line 144
    :try_start_4
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v15, :cond_3

    .line 184
    :try_start_5
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    :goto_4
    if-eqz v14, :cond_4

    .line 187
    invoke-virtual {v14}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_4
    if-eqz p2, :cond_5

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    :cond_5
    new-array v0, v10, [Ljava/lang/Object;

    aput-object v9, v0, v13

    .line 192
    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v12

    aput-object v7, v0, v8

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v6

    invoke-static {v11, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 147
    :cond_6
    :try_start_6
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/c/a;->b(Lcom/bytedance/sdk/component/video/a/c/a;)Ljava/io/File;

    move-result-object v6

    const-string v8, "rw"

    invoke-direct {v0, v6, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/16 v6, 0x2000

    new-array v8, v6, [B

    const/4 v12, 0x0

    const-wide/16 v18, 0x0

    :goto_5
    rsub-int v13, v12, 0x2000

    .line 152
    invoke-virtual {v15, v8, v12, v13}, Ljava/io/InputStream;->read([BII)I

    move-result v13

    const/4 v6, -0x1

    if-eq v13, v6, :cond_e

    .line 153
    iget-object v6, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v6}, Lcom/bytedance/sdk/component/video/a/c/a;->c(Lcom/bytedance/sdk/component/video/a/c/a;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 155
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v15, :cond_7

    .line 184
    :try_start_7
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    goto :goto_6

    :catch_2
    move-exception v0

    goto/16 :goto_1

    :cond_7
    :goto_6
    if-eqz v14, :cond_8

    .line 187
    invoke-virtual {v14}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_8
    if-eqz p2, :cond_9

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    :cond_9
    new-array v0, v10, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v0, v2

    .line 192
    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const/4 v2, 0x2

    aput-object v7, v0, v2

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v0, v3

    invoke-static {v11, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    :cond_a
    add-int/2addr v12, v13

    move-object/from16 v20, v11

    int-to-long v10, v13

    add-long v18, v18, v10

    const-wide/16 v10, 0x2000

    .line 160
    :try_start_8
    rem-long v10, v18, v10

    const-wide/16 v16, 0x0

    cmp-long v13, v10, v16

    if-eqz v13, :cond_c

    iget-wide v10, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->a:J

    sub-long v10, v4, v10

    cmp-long v13, v18, v10

    if-nez v13, :cond_b

    goto :goto_7

    :cond_b
    const/4 v10, 0x0

    goto :goto_8

    :cond_c
    :goto_7
    const/4 v10, 0x1

    :goto_8
    if-eqz v10, :cond_d

    .line 162
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->intValue()I

    move-result v10

    iget-object v11, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v11}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v11

    invoke-virtual {v11}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v8, v10, v12, v11}, Lcom/bytedance/sdk/component/video/d/b;->a(Ljava/io/RandomAccessFile;[BIILjava/lang/String;)V

    int-to-long v10, v12

    add-long/2addr v2, v10

    const/4 v12, 0x0

    :cond_d
    move-object/from16 v11, v20

    const/16 v6, 0x2000

    const/4 v10, 0x4

    goto/16 :goto_5

    :cond_e
    move-object/from16 v20, v11

    .line 168
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/b/a;->j()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->b(Lcom/bytedance/sdk/component/video/a/c/a;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v0, v4, v2

    if-nez v0, :cond_f

    .line 169
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->d(Lcom/bytedance/sdk/component/video/a/c/a;)V

    .line 172
    :cond_f
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/bytedance/sdk/component/video/a/c/a;->b(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object v5, v15

    goto :goto_a

    :catchall_0
    move-exception v0

    move-object v5, v14

    move-object/from16 v2, v20

    goto :goto_9

    :catchall_1
    move-exception v0

    move-object v2, v11

    move-object v5, v14

    :goto_9
    const/16 v4, 0x259

    goto/16 :goto_11

    :catchall_2
    move-exception v0

    move-object v2, v11

    move-object v5, v14

    const/16 v4, 0x259

    goto/16 :goto_10

    :catchall_3
    move-exception v0

    move-object v2, v11

    goto :goto_e

    :cond_10
    move-object/from16 v20, v11

    .line 174
    :try_start_9
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    const-string v3, "Network link failed."
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    const/16 v4, 0x259

    :try_start_a
    invoke-static {v0, v2, v4, v3}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    const/4 v5, 0x0

    const/4 v14, 0x0

    :goto_a
    if-eqz v5, :cond_11

    .line 184
    :try_start_b
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_b

    :catch_3
    move-exception v0

    goto :goto_c

    :cond_11
    :goto_b
    if-eqz v14, :cond_12

    .line 187
    invoke-virtual {v14}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_12
    if-eqz p2, :cond_13

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    :cond_13
    const/4 v2, 0x4

    new-array v0, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v0, v2

    .line 192
    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const/4 v2, 0x2

    aput-object v7, v0, v2

    iget-object v2, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v0, v3

    move-object/from16 v2, v20

    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto :goto_d

    .line 194
    :goto_c
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 196
    :goto_d
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V

    goto :goto_13

    :catchall_4
    move-exception v0

    move-object/from16 v2, v20

    goto :goto_f

    :catchall_5
    move-exception v0

    move-object/from16 v2, v20

    :goto_e
    const/16 v4, 0x259

    :goto_f
    const/4 v5, 0x0

    :goto_10
    const/4 v15, 0x0

    .line 178
    :goto_11
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 179
    iget-object v3, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/component/video/a/c/a;->e(Lcom/bytedance/sdk/component/video/a/c/a;)V

    .line 180
    iget-object v3, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    iget-object v8, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v8}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v8

    if-eqz p2, :cond_14

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v4

    :cond_14
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v8, v4, v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    if-eqz v15, :cond_15

    .line 184
    :try_start_d
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    goto :goto_12

    :catch_4
    move-exception v0

    goto :goto_c

    :cond_15
    :goto_12
    if-eqz v5, :cond_16

    .line 187
    invoke-virtual {v5}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_16
    if-eqz p2, :cond_17

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    :cond_17
    const/4 v3, 0x4

    new-array v0, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v9, v0, v3

    .line 192
    iget-object v3, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v3, 0x2

    aput-object v7, v0, v3

    iget-object v3, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v0, v4

    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    goto :goto_d

    :goto_13
    return-void

    :catchall_6
    move-exception v0

    move-object v3, v0

    if-eqz v15, :cond_18

    .line 184
    :try_start_e
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    goto :goto_14

    :catch_5
    move-exception v0

    goto :goto_15

    :cond_18
    :goto_14
    if-eqz v5, :cond_19

    .line 187
    invoke-virtual {v5}, Lcom/bytedance/sdk/component/b/b/ac;->close()V

    :cond_19
    if-eqz p2, :cond_1a

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->close()V

    :cond_1a
    const/4 v4, 0x4

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v9, v0, v4

    .line 192
    iget-object v4, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v4}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v0, v5

    const/4 v4, 0x2

    aput-object v7, v0, v4

    iget-object v4, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v4}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x3

    aput-object v4, v0, v5

    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    goto :goto_16

    .line 194
    :goto_15
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 196
    :goto_16
    iget-object v0, v1, Lcom/bytedance/sdk/component/video/a/c/a$1;->b:Lcom/bytedance/sdk/component/video/a/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V

    .line 197
    throw v3
.end method
