.class public Lcom/bytedance/sdk/component/video/a/c/a;
.super Ljava/lang/Object;
.source "VideoPreload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/video/a/c/a$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/component/video/b/a;

.field private volatile c:Z

.field private d:Ljava/io/File;

.field private e:Ljava/io/File;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/component/video/a/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/component/video/b/a;)V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->c:Z

    const/4 v1, 0x0

    .line 40
    iput-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    .line 41
    iput-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    .line 47
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->g:Z

    .line 51
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    .line 53
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/video/b/a;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/video/d/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    .line 54
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/video/b/a;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/video/d/b;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/video/a/c/a;)Lcom/bytedance/sdk/component/video/b/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a;->b(Lcom/bytedance/sdk/component/video/b/a;I)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 3

    .line 218
    const-class v0, Lcom/bytedance/sdk/component/video/a/c/a$a;

    monitor-enter v0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v2, :cond_0

    .line 221
    invoke-interface {v2, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a$a;->a(Lcom/bytedance/sdk/component/video/b/a;I)V

    goto :goto_0

    .line 224
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    .locals 3

    .line 228
    const-class v0, Lcom/bytedance/sdk/component/video/a/c/a$a;

    monitor-enter v0

    .line 229
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v2, :cond_0

    .line 231
    invoke-interface {v2, p1, p2, p3}, Lcom/bytedance/sdk/component/video/a/c/a$a;->a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V

    goto :goto_0

    .line 234
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/component/video/a/c/a;)Ljava/io/File;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    return-object p0
.end method

.method private b()V
    .locals 8

    .line 90
    sget-object v0, Lcom/bytedance/sdk/component/video/a/b/a;->b:Lcom/bytedance/sdk/component/b/b/w;

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/bytedance/sdk/component/video/a/b/a;->b:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/w;->y()Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/component/b/b/w$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/w$a;-><init>()V

    .line 96
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/video/b/a;->g()I

    move-result v1

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/b/b/w$a;->a(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    .line 97
    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->h()I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/component/b/b/w$a;->b(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    .line 98
    invoke-virtual {v2}, Lcom/bytedance/sdk/component/video/b/a;->i()I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/component/b/b/w$a;->c(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    .line 99
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/w$a;->a()Lcom/bytedance/sdk/component/b/b/w;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 101
    iget-object v2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 103
    iget-object v4, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/video/b/a;->j()Z

    move-result v4

    const-string v5, "-"

    const-string v6, "bytes="

    const-string v7, "RANGE"

    if-eqz v4, :cond_1

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v7, v4}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    .line 105
    invoke-virtual {v5}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    .line 106
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    .line 107
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    goto :goto_1

    .line 109
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {v5}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v7, v4}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    .line 110
    invoke-virtual {v5}, Lcom/bytedance/sdk/component/video/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    .line 111
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v4

    .line 112
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    .line 115
    :goto_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/bytedance/sdk/component/video/a/c/a$1;

    invoke-direct {v1, p0, v2, v3}, Lcom/bytedance/sdk/component/video/a/c/a$1;-><init>(Lcom/bytedance/sdk/component/video/a/c/a;J)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/b/b/e;->a(Lcom/bytedance/sdk/component/b/b/f;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/component/video/a/c/a;Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/b/a;I)V

    return-void
.end method

.method private b(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 3

    .line 238
    const-class v0, Lcom/bytedance/sdk/component/video/a/c/a$a;

    monitor-enter v0

    .line 239
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v2, :cond_0

    .line 241
    invoke-interface {v2, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a$a;->b(Lcom/bytedance/sdk/component/video/b/a;I)V

    goto :goto_0

    .line 244
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private c()V
    .locals 1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 259
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 261
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/component/video/a/c/a;)Z
    .locals 0

    .line 32
    iget-boolean p0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->c:Z

    return p0
.end method

.method private d()V
    .locals 3

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 272
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error renaming file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " for completion!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    .line 275
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v1, "VideoPreload"

    .line 276
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    return-void

    :catchall_1
    move-exception v0

    .line 278
    throw v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/component/video/a/c/a;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/bytedance/sdk/component/video/a/c/a;->d()V

    return-void
.end method

.method static synthetic e(Lcom/bytedance/sdk/component/video/a/c/a;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/bytedance/sdk/component/video/a/c/a;->c()V

    return-void
.end method


# virtual methods
.method public a()Lcom/bytedance/sdk/component/video/b/a;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/video/a/c/a$a;)V
    .locals 5

    .line 65
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->g:Z

    if-eqz v0, :cond_0

    .line 66
    const-class v0, Lcom/bytedance/sdk/component/video/a/c/a$a;

    monitor-enter v0

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->e:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/video/b/a;->j()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->d:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v1

    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/video/b/a;->e()J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-ltz p1, :cond_1

    goto :goto_0

    .line 81
    :cond_1
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/video/a/c/a;->g:Z

    .line 82
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/video/b/a;->a(I)V

    .line 84
    invoke-direct {p0}, Lcom/bytedance/sdk/component/video/a/c/a;->b()V

    return-void

    :cond_2
    :goto_0
    const-string p1, "VideoPreload"

    const-string v1, "Cache file is exist"

    .line 75
    invoke-static {p1, v1}, Lcom/bytedance/sdk/component/video/d/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/video/b/a;->a(I)V

    .line 77
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    const/16 v0, 0xc8

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/component/video/a/c/a;->a(Lcom/bytedance/sdk/component/video/b/a;I)V

    .line 78
    iget-object p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->b:Lcom/bytedance/sdk/component/video/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 214
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/video/a/c/a;->c:Z

    return-void
.end method
