.class public Lcom/bytedance/sdk/component/video/b/a;
.super Ljava/lang/Object;
.source "VideoInfoModel.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:J

.field private d:J

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 10
    iput-wide v0, p0, Lcom/bytedance/sdk/component/video/b/a;->c:J

    const-wide/32 v0, 0x4b000

    .line 11
    iput-wide v0, p0, Lcom/bytedance/sdk/component/video/b/a;->d:J

    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/video/b/a;->e:Z

    const/4 v1, 0x0

    .line 13
    iput-object v1, p0, Lcom/bytedance/sdk/component/video/b/a;->f:Ljava/lang/String;

    .line 15
    iput v0, p0, Lcom/bytedance/sdk/component/video/b/a;->g:I

    const/16 v0, 0x2710

    .line 20
    iput v0, p0, Lcom/bytedance/sdk/component/video/b/a;->h:I

    .line 21
    iput v0, p0, Lcom/bytedance/sdk/component/video/b/a;->i:I

    .line 22
    iput v0, p0, Lcom/bytedance/sdk/component/video/b/a;->j:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .line 87
    iput p1, p0, Lcom/bytedance/sdk/component/video/b/a;->g:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 62
    iput-wide p1, p0, Lcom/bytedance/sdk/component/video/b/a;->c:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 123
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/video/b/a;->e:Z

    return-void
.end method

.method public a()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/b/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 95
    iput p1, p0, Lcom/bytedance/sdk/component/video/b/a;->h:I

    return-void
.end method

.method public b(J)V
    .locals 0

    .line 79
    iput-wide p1, p0, Lcom/bytedance/sdk/component/video/b/a;->d:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/b/a;->b:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->b:Ljava/lang/String;

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .line 103
    iput p1, p0, Lcom/bytedance/sdk/component/video/b/a;->i:I

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/b/a;->f:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/b/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .line 111
    iput p1, p0, Lcom/bytedance/sdk/component/video/b/a;->j:I

    return-void
.end method

.method public e()J
    .locals 2

    .line 75
    iget-wide v0, p0, Lcom/bytedance/sdk/component/video/b/a;->d:J

    return-wide v0
.end method

.method public f()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/bytedance/sdk/component/video/b/a;->g:I

    return v0
.end method

.method public g()I
    .locals 1

    .line 91
    iget v0, p0, Lcom/bytedance/sdk/component/video/b/a;->h:I

    return v0
.end method

.method public h()I
    .locals 1

    .line 99
    iget v0, p0, Lcom/bytedance/sdk/component/video/b/a;->i:I

    return v0
.end method

.method public i()I
    .locals 1

    .line 107
    iget v0, p0, Lcom/bytedance/sdk/component/video/b/a;->j:I

    return v0
.end method

.method public j()Z
    .locals 6

    .line 116
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/video/b/a;->e:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 119
    :cond_0
    iget-wide v2, p0, Lcom/bytedance/sdk/component/video/b/a;->c:J

    iget-wide v4, p0, Lcom/bytedance/sdk/component/video/b/a;->d:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
