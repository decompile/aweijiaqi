.class public Lcom/bytedance/sdk/component/video/view/PlayerLayout;
.super Landroid/widget/FrameLayout;
.source "PlayerLayout.java"

# interfaces
.implements Lcom/bytedance/sdk/component/video/c/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;
    }
.end annotation


# static fields
.field public static b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public static d:I


# instance fields
.field public a:I

.field public c:I

.field public e:Lcom/bytedance/sdk/component/video/c/a;

.field public f:Ljava/lang/Class;

.field public g:Landroid/view/TextureView;

.field public h:Landroid/view/SurfaceView;

.field private i:Z

.field private j:Ljava/util/Timer;

.field private k:Landroid/media/AudioManager;

.field private l:Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;

.field private m:J

.field private n:Lcom/bytedance/sdk/component/video/b/a;

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 66
    new-instance v0, Lcom/bytedance/sdk/component/video/view/PlayerLayout$1;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout$1;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v0, -0x1

    .line 83
    sput v0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->d:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->f:Ljava/lang/Class;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/video/c/a;

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    .line 131
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/video/c/a;->a(Landroid/content/Context;)V

    .line 132
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    iget-boolean v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->i:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/video/c/a;->b(Z)V

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    iget-boolean v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->o:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/video/c/a;->a(Z)V

    .line 134
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/component/video/c/a;->a(Lcom/bytedance/sdk/component/video/c/a$a;)V

    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    iget-object v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->n:Lcom/bytedance/sdk/component/video/b/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/video/c/a;->a(Lcom/bytedance/sdk/component/video/b/a;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 144
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 142
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    .line 138
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 147
    :goto_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->o:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->j()V

    goto :goto_1

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->k()V

    .line 152
    :goto_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->k:Landroid/media/AudioManager;

    .line 153
    sget-object v1, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 154
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/d/d;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 155
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c()V

    return-void
.end method

.method public a(IJJ)V
    .locals 3

    .line 405
    iput-wide p2, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m:J

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onProgress:  progress ="

    aput-object v2, v0, v1

    .line 406
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    const-string v1, "  position = "

    aput-object v1, v0, p1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v0, p2

    const/4 p1, 0x4

    const-string p2, "  duration="

    aput-object p2, v0, p1

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, v0, p2

    const-string p1, "PlayerLayout"

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public b()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "video_new onStateNormal "

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 188
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    iput v2, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 190
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    .line 191
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/c/a;->b()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new onStatePreparing "

    aput-object v2, v0, v1

    .line 197
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    iput v2, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 199
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->n()V

    return-void
.end method

.method public d()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new onStatePreparingPlaying "

    aput-object v2, v0, v1

    .line 204
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    .line 205
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    return-void
.end method

.method public e()V
    .locals 4

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "video_new onStatePreparingChangeUrl "

    aput-object v3, v1, v2

    .line 209
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "PlayerLayout"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 211
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->a()V

    return-void
.end method

.method public f()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "video_new onStatePlaying seekToInAdvance="

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 263
    iget v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x2

    aput-object v1, v0, v3

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    iget v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 265
    iget v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->a:I

    if-eqz v0, :cond_0

    .line 266
    iget-object v3, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    invoke-virtual {v3, v0}, Lcom/bytedance/sdk/component/video/c/a;->a(I)V

    const-string v0, "video_new onStatePlaying seekTo"

    .line 267
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iput v2, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->a:I

    goto :goto_0

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/video/c/a;->a(I)V

    :cond_1
    :goto_0
    const/4 v0, 0x5

    .line 274
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 275
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->l()V

    return-void
.end method

.method public g()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new onStatePause "

    aput-object v2, v0, v1

    .line 279
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x6

    .line 280
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 281
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    return-void
.end method

.method public getCurrentPositionWhenPlaying()J
    .locals 4

    .line 416
    iget v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    const-wide/16 v1, 0x0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    const/4 v3, 0x6

    if-eq v0, v3, :cond_0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 418
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/c/a;->c()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v1, v0

    :cond_1
    return-wide v1

    :catch_0
    move-exception v0

    .line 420
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    return-wide v1
.end method

.method public getDuration()J
    .locals 2

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/c/a;->d()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    move-exception v0

    .line 432
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public h()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new onStateError "

    aput-object v2, v0, v1

    .line 285
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v0, 0x8

    .line 286
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 287
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    return-void
.end method

.method public i()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new onStateAutoComplete "

    aput-object v2, v0, v1

    .line 291
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x7

    .line 292
    iput v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c:I

    .line 293
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    return-void
.end method

.method public j()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new addTextureView "

    aput-object v2, v0, v1

    .line 299
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->removeAllViews()V

    .line 301
    new-instance v0, Landroid/view/TextureView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->g:Landroid/view/TextureView;

    .line 303
    new-instance v1, Lcom/bytedance/sdk/component/video/view/PlayerLayout$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout$2;-><init>(Lcom/bytedance/sdk/component/video/view/PlayerLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 330
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x11

    invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 335
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->g:Landroid/view/TextureView;

    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public k()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new addSurfaceView "

    aput-object v2, v0, v1

    .line 340
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->removeAllViews()V

    .line 342
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->h:Landroid/view/SurfaceView;

    .line 343
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/video/view/PlayerLayout$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout$3;-><init>(Lcom/bytedance/sdk/component/video/view/PlayerLayout;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 373
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->h:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    .line 374
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->h:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 377
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x11

    invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 382
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->h:Landroid/view/SurfaceView;

    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public l()V
    .locals 7

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "startProgressTimer: "

    aput-object v2, v0, v1

    .line 387
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 388
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    .line 389
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->j:Ljava/util/Timer;

    .line 390
    new-instance v2, Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;-><init>(Lcom/bytedance/sdk/component/video/view/PlayerLayout;)V

    iput-object v2, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->l:Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;

    .line 391
    iget-object v1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->j:Ljava/util/Timer;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x12c

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method public m()V
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->j:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->l:Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;

    if-eqz v0, :cond_1

    .line 399
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout$a;->cancel()Z

    :cond_1
    return-void
.end method

.method public n()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 411
    iput-wide v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m:J

    return-void
.end method

.method public o()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "video_new reset "

    aput-object v2, v0, v1

    .line 517
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PlayerLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/video/d/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 518
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->m()V

    .line 519
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->b()V

    .line 520
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->removeAllViews()V

    .line 521
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 522
    sget-object v1, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->b:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 523
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/d/d;->a(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 524
    iget-object v0, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e:Lcom/bytedance/sdk/component/video/c/a;

    if-eqz v0, :cond_0

    .line 525
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/video/c/a;->b()V

    :cond_0
    return-void
.end method

.method public setMediaInterface(Ljava/lang/Class;)V
    .locals 0

    .line 182
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->o()V

    .line 183
    iput-object p1, p0, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->f:Ljava/lang/Class;

    return-void
.end method

.method public setState(I)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 555
    :pswitch_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->h()V

    goto :goto_0

    .line 558
    :pswitch_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->i()V

    goto :goto_0

    .line 552
    :pswitch_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->g()V

    goto :goto_0

    .line 549
    :pswitch_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->f()V

    goto :goto_0

    .line 543
    :pswitch_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->d()V

    goto :goto_0

    .line 546
    :pswitch_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->e()V

    goto :goto_0

    .line 540
    :pswitch_7
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->c()V

    goto :goto_0

    .line 537
    :pswitch_8
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/video/view/PlayerLayout;->b()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
