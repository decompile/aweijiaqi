.class public Lcom/bytedance/sdk/component/net/utils/NetLog;
.super Ljava/lang/Object;
.source "NetLog.java"


# static fields
.field private static DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "NetLog"

.field private static sLevel:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    .line 75
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "NetLog"

    .line 78
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 82
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 88
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 89
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 93
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 99
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 100
    invoke-static {p0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method public static varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 104
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 110
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 111
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->formatMsgs([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static debug()Z
    .locals 2

    .line 24
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    .line 209
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "NetLog"

    .line 212
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 216
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 222
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_2

    .line 223
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 227
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 233
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_2

    .line 234
    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method public static varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 239
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 245
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_2

    .line 246
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->formatMsgs([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private static varargs formatMsgs([Ljava/lang/Object;)Ljava/lang/String;
    .locals 4

    if-eqz p0, :cond_3

    .line 278
    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_2

    .line 281
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    if-eqz v3, :cond_1

    .line 284
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v3, " null "

    .line 286
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v3, " "

    .line 288
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    :goto_2
    const-string p0, ""

    return-object p0
.end method

.method public static getLogLevel()I
    .locals 1

    .line 20
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    return v0
.end method

.method private static getSimpleClassName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x2e

    .line 270
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 274
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static i(Ljava/lang/String;)V
    .locals 1

    .line 116
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "NetLog"

    .line 119
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 123
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 129
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_2

    .line 130
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 134
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 140
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_2

    .line 141
    invoke-static {p0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method public static varargs i(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 145
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 151
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_2

    .line 152
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->formatMsgs([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static isDebug()Z
    .locals 1

    .line 33
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    return v0
.end method

.method public static logDirect(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-nez p0, :cond_1

    const-string p0, "NetLog"

    .line 205
    :cond_1
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static openDebugMode()V
    .locals 1

    const/4 v0, 0x1

    .line 28
    sput-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    const/4 v0, 0x3

    .line 29
    invoke-static {v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->setLogLevel(I)V

    return-void
.end method

.method public static setLogLevel(I)V
    .locals 0

    .line 16
    sput p0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    return-void
.end method

.method public static st(Ljava/lang/String;I)V
    .locals 5

    .line 253
    :try_start_0
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 255
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 257
    :goto_0
    array-length v4, v0

    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-ge v3, v4, :cond_1

    if-le v3, v2, :cond_0

    const-string v4, "\n"

    .line 259
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_0
    aget-object v4, v0, v3

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/bytedance/sdk/component/net/utils/NetLog;->getSimpleClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "."

    .line 262
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    aget-object v4, v0, v3

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 265
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 1

    const-string v0, "NetLog"

    .line 37
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 41
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 47
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_2

    .line 48
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 52
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 58
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_2

    .line 59
    invoke-static {p0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method public static varargs v(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 63
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 69
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_2

    .line 70
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->formatMsgs([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static w(Ljava/lang/String;)V
    .locals 1

    .line 157
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "NetLog"

    .line 160
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 164
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 170
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 171
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 175
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 181
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 182
    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method public static varargs w(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 187
    sget-boolean v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->DEBUG:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 193
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/net/utils/NetLog;->sLevel:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 194
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->formatMsgs([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method
