.class public Lcom/bytedance/sdk/component/net/utils/MultiProcessFileUtils;
.super Ljava/lang/Object;
.source "MultiProcessFileUtils.java"


# static fields
.field public static final KEY_TNC_CONFIG:Ljava/lang/String; = "tnc_config"

.field private static final TAG:Ljava/lang/String; = "MultiProcessFileUtils"

.field public static final TYPE_TNC_CONFIG:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getData(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 44
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object p1

    const-string v1, "tnc_config"

    invoke-interface {p1, p0, v1, v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getProviderString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 55
    :cond_1
    :goto_0
    instance-of p0, v0, Ljava/lang/String;

    return-object v0
.end method

.method public static saveData(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3

    .line 20
    :try_start_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "tnc_config"

    .line 23
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string p1, "MultiProcessFileUtils"

    .line 28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveData = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 30
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object p1

    invoke-interface {p1, p0, v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->saveMapToProvider(Landroid/content/Context;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method
