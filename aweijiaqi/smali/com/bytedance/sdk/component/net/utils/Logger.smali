.class public Lcom/bytedance/sdk/component/net/utils/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;,
        Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;,
        Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;
    }
.end annotation


# instance fields
.field private mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

.field private mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->OFF:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    .line 30
    new-instance v0, Lcom/bytedance/sdk/component/net/utils/DefaultLoggerDelegate;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/net/utils/DefaultLoggerDelegate;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/component/net/utils/Logger$1;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/utils/Logger;-><init>()V

    return-void
.end method

.method public static debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 78
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    sget-object v1, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->DEBUG:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    invoke-interface {v0, p0, p1}, Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;->debug(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static error(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 60
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    sget-object v1, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->ERROR:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 61
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    invoke-interface {v0, p0, p1}, Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;->error(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 66
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    sget-object v1, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->ERROR:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 67
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    invoke-interface {v0, p0, p1, p2}, Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;->error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static info(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 72
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    sget-object v1, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->INFO:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 73
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    invoke-interface {v0, p0, p1}, Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;->info(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static resetLoggerDelegate()V
    .locals 3

    .line 39
    const-class v0, Lcom/bytedance/sdk/component/net/utils/Logger;

    monitor-enter v0

    .line 40
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/component/net/utils/DefaultLoggerDelegate;

    invoke-direct {v2}, Lcom/bytedance/sdk/component/net/utils/DefaultLoggerDelegate;-><init>()V

    iput-object v2, v1, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    .line 41
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static setLogLevel(Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;)V
    .locals 2

    .line 54
    const-class v0, Lcom/bytedance/sdk/component/net/utils/Logger;

    monitor-enter v0

    .line 55
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v1

    iput-object p0, v1, Lcom/bytedance/sdk/component/net/utils/Logger;->mLogLevel:Lcom/bytedance/sdk/component/net/utils/Logger$LogLevel;

    .line 56
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static setLoggerDelegate(Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;)V
    .locals 2

    if-eqz p0, :cond_0

    .line 48
    const-class v0, Lcom/bytedance/sdk/component/net/utils/Logger;

    monitor-enter v0

    .line 49
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/Logger$SingletonHolder;->access$100()Lcom/bytedance/sdk/component/net/utils/Logger;

    move-result-object v1

    iput-object p0, v1, Lcom/bytedance/sdk/component/net/utils/Logger;->mDelegate:Lcom/bytedance/sdk/component/net/utils/Logger$LoggerDelegate;

    .line 50
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 46
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "delegate MUST not be null!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
