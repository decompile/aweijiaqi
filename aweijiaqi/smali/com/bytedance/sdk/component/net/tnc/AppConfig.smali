.class public Lcom/bytedance/sdk/component/net/tnc/AppConfig;
.super Ljava/lang/Object;
.source "AppConfig.java"

# interfaces
.implements Lcom/bytedance/sdk/component/net/utils/WeakHandler$IHandler;


# static fields
.field static final KEY_LAST_REFRESH_TIME:Ljava/lang/String; = "last_refresh_time"

.field static final MSG_CONFIG_ERROR:I = 0x66

.field static final MSG_CONFIG_OK:I = 0x65

.field static final SP_SS_APP_CONFIG:Ljava/lang/String; = "ss_app_config"

.field static final TAG:Ljava/lang/String; = "AppConfig"

.field private static mInstance:Lcom/bytedance/sdk/component/net/tnc/AppConfig;


# instance fields
.field private mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mContext:Landroid/content/Context;

.field private mForceChanged:Z

.field private volatile mForceSwitch:Z

.field final mHandler:Lcom/bytedance/sdk/component/net/utils/WeakHandler;

.field private final mIsMainProcess:Z

.field private mLastRefreshTime:J

.field private mLastTryRefreshTime:J

.field private mLoading:Z

.field private volatile mLocalLoaded:Z

.field private mNetClient:Lcom/bytedance/sdk/component/net/NetClient;

.field private threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 50
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceSwitch:Z

    const/4 v1, 0x1

    .line 51
    iput-boolean v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    .line 52
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLoading:Z

    const-wide/16 v1, 0x0

    .line 53
    iput-wide v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    .line 54
    iput-wide v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastTryRefreshTime:J

    const/4 v1, 0x0

    .line 56
    iput-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 58
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 61
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLocalLoaded:Z

    .line 65
    new-instance v0, Lcom/bytedance/sdk/component/net/utils/WeakHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/component/net/utils/WeakHandler;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/net/utils/WeakHandler$IHandler;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mHandler:Lcom/bytedance/sdk/component/net/utils/WeakHandler;

    .line 86
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mContext:Landroid/content/Context;

    .line 87
    iput-boolean p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mIsMainProcess:Z

    return-void
.end method

.method static synthetic access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getDomainInternalNext(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/bytedance/sdk/component/net/tnc/AppConfig;Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->handleResponse(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->sendConfigUpdateMsg(I)V

    return-void
.end method

.method private addTncExecutorParams(Lcom/bytedance/sdk/component/net/executor/GetExecutor;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 400
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 401
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getLocationAdress(Landroid/content/Context;)Landroid/location/Address;

    move-result-object v0

    :cond_1
    const-string v1, ""

    if-eqz v0, :cond_2

    .line 403
    invoke-virtual {v0}, Landroid/location/Address;->hasLatitude()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/location/Address;->hasLongitude()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 404
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "latitude"

    invoke-virtual {p1, v3, v2}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "longitude"

    invoke-virtual {p1, v3, v2}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 408
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "city"

    invoke-virtual {p1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_2
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceSwitch:Z

    if-eqz v0, :cond_3

    const-string v0, "force"

    const-string v2, "1"

    .line 412
    invoke-virtual {p1, v0, v2}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_3
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_4

    .line 417
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    goto :goto_0

    .line 419
    :cond_4
    sget-object v0, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    :goto_0
    const-string v2, "abi"

    .line 421
    invoke-virtual {p1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    .line 424
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 426
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v2

    invoke-interface {v2}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getAid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "aid"

    invoke-virtual {p1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getPlatform()Ljava/lang/String;

    move-result-object v0

    const-string v2, "device_platform"

    invoke-virtual {p1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getChannel()Ljava/lang/String;

    move-result-object v0

    const-string v2, "channel"

    invoke-virtual {p1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v2

    invoke-interface {v2}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getVersionCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "version_code"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getDid()Ljava/lang/String;

    move-result-object v0

    const-string v1, "custom_info_1"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method private buildUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 387
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 390
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    .line 391
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/get_domains/v4/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getDomainInternal()Z
    .locals 2

    .line 295
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getConfigServers()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 296
    array-length v0, v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 299
    :cond_0
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getDomainInternalNext(I)V

    :cond_1
    :goto_0
    return v1
.end method

.method private getDomainInternalNext(I)V
    .locals 3

    .line 304
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getConfigServers()[Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x66

    if-eqz v0, :cond_3

    .line 305
    array-length v2, v0

    if-gt v2, p1, :cond_0

    goto :goto_1

    .line 309
    :cond_0
    aget-object v0, v0, p1

    .line 310
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 311
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->sendConfigUpdateMsg(I)V

    return-void

    .line 315
    :cond_1
    :try_start_0
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->buildUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 317
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->sendConfigUpdateMsg(I)V

    return-void

    .line 320
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getNetClient()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/NetClient;->getGetExecutor()Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    move-result-object v1

    .line 321
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->setUrl(Ljava/lang/String;)V

    .line 322
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->addTncExecutorParams(Lcom/bytedance/sdk/component/net/executor/GetExecutor;)V

    .line 323
    new-instance v0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;-><init>(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "try app config exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AppConfig"

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    .line 306
    :cond_3
    :goto_1
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->sendConfigUpdateMsg(I)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;
    .locals 3

    .line 68
    const-class v0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    monitor-enter v0

    .line 69
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mInstance:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    if-nez v1, :cond_0

    .line 70
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result v1

    .line 71
    new-instance v2, Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0, v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;-><init>(Landroid/content/Context;Z)V

    sput-object v2, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mInstance:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    .line 73
    :cond_0
    sget-object p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mInstance:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    .line 74
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method private getNetClient()Lcom/bytedance/sdk/component/net/NetClient;
    .locals 4

    .line 370
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mNetClient:Lcom/bytedance/sdk/component/net/NetClient;

    if-nez v0, :cond_0

    .line 371
    new-instance v0, Lcom/bytedance/sdk/component/net/NetClient$Builder;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    .line 372
    invoke-virtual {v0, v2, v3, v1}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 373
    invoke-virtual {v0, v2, v3, v1}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 374
    invoke-virtual {v0, v2, v3, v1}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object v0

    .line 375
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->build()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mNetClient:Lcom/bytedance/sdk/component/net/NetClient;

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mNetClient:Lcom/bytedance/sdk/component/net/NetClient;

    return-object v0
.end method

.method private handleResponse(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 259
    instance-of v0, p1, Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 260
    check-cast p1, Ljava/lang/String;

    .line 261
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    .line 264
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "message"

    .line 265
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "success"

    .line 266
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    .line 269
    :cond_1
    instance-of v0, p1, Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 270
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_0
    if-nez v0, :cond_4

    return v1

    :cond_4
    const-string p1, "data"

    .line 275
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 276
    monitor-enter p0

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mContext:Landroid/content/Context;

    const-string v2, "ss_app_config"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 279
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 280
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "last_refresh_time"

    .line 281
    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 283
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 284
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 288
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;->handleConfigChanged(Lorg/json/JSONObject;)V

    :cond_5
    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    .line 284
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public static onActivityResume(Landroid/content/Context;)V
    .locals 1

    .line 115
    sget-object v0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mInstance:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    if-eqz v0, :cond_1

    .line 117
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    .line 118
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig(Z)V

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    :cond_1
    :goto_0
    return-void
.end method

.method private sendConfigUpdateMsg(I)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mHandler:Lcom/bytedance/sdk/component/net/utils/WeakHandler;

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/net/utils/WeakHandler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private tryRefreshDomainConfig(Z)V
    .locals 6

    .line 139
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLoading:Z

    if-eqz v0, :cond_0

    return-void

    .line 141
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 142
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    const-wide/16 v0, 0x0

    .line 143
    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    .line 144
    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastTryRefreshTime:J

    :cond_1
    if-eqz p1, :cond_2

    const-wide/32 v0, 0xa4cb80

    goto :goto_0

    :cond_2
    const-wide/32 v0, 0x2932e00

    .line 147
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 148
    iget-wide v4, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    sub-long v4, v2, v4

    cmp-long p1, v4, v0

    if-lez p1, :cond_4

    .line 149
    iget-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastTryRefreshTime:J

    sub-long/2addr v2, v0

    const-wide/32 v0, 0x1d4c0

    cmp-long p1, v2, v0

    if-lez p1, :cond_4

    .line 150
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/NetworkUtils;->checkWifiAndGPRS(Landroid/content/Context;)Z

    move-result p1

    .line 151
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLocalLoaded:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_4

    .line 152
    :cond_3
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->doRefresh(Z)Z

    :cond_4
    return-void
.end method


# virtual methods
.method public doRefresh(Z)Z
    .locals 4

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doRefresh: updating state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TNCManager"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "doRefresh, already running"

    .line 161
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_0
    if-eqz p1, :cond_1

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastTryRefreshTime:J

    .line 167
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getThreadPoolExecutor()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/net/tnc/AppConfig$2;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig$2;-><init>(Lcom/bytedance/sdk/component/net/tnc/AppConfig;Z)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return v2
.end method

.method public getConfigServers()[Ljava/lang/String;
    .locals 2

    .line 248
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;->getConfigServers()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 251
    array-length v1, v0

    if-gtz v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :cond_2
    return-object v0
.end method

.method public getThreadPoolExecutor()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 8

    .line 438
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-wide/16 v4, 0x14

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x1

    .line 442
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object v0
.end method

.method public handleMsg(Landroid/os/Message;)V
    .locals 5

    .line 178
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x65

    const-string v1, "TNCManager"

    const/4 v2, 0x0

    if-eq p1, v0, :cond_2

    const/16 v0, 0x66

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 188
    :cond_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLoading:Z

    .line 189
    iget-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    if-eqz p1, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    :cond_1
    const-string p1, "doRefresh, error"

    .line 192
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 180
    :cond_2
    iput-boolean v2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLoading:Z

    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    const-string p1, "doRefresh, succ"

    .line 182
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    if-eqz p1, :cond_3

    .line 184
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    .line 185
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method

.method public setForceSwitch(Z)V
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceSwitch:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 80
    :cond_0
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceSwitch:Z

    const/4 p1, 0x1

    .line 81
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mForceChanged:Z

    .line 82
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    return-void
.end method

.method public setThreadPoolExecutor(Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 0

    .line 448
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->threadPoolExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-void
.end method

.method declared-synchronized tryLoadDomainConfig4OtherProcess()V
    .locals 5

    monitor-enter p0

    .line 126
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 127
    iget-wide v2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 131
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;->loadLocalConfigForOtherProcess()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :catch_0
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized tryLoadLocalConfig()V
    .locals 5

    monitor-enter p0

    .line 199
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLocalLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 200
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 201
    :try_start_1
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLocalLoaded:Z

    .line 202
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mContext:Landroid/content/Context;

    const-string v1, "ss_app_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "last_refresh_time"

    const-wide/16 v2, 0x0

    .line 204
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 205
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    move-wide v0, v2

    .line 209
    :cond_1
    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J

    .line 211
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 212
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;->loadLocalConfig()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public tryLodeConfigInSubThread()V
    .locals 2

    .line 217
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    return-void

    .line 221
    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mIsMainProcess:Z

    if-eqz v0, :cond_1

    .line 222
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLoadLocalConfig()V

    goto :goto_0

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLoadDomainConfig4OtherProcess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method public tryRefreshConfig()V
    .locals 1

    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig(Z)V

    return-void
.end method

.method public declared-synchronized tryRefreshConfig(Z)V
    .locals 4

    monitor-enter p0

    .line 95
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mIsMainProcess:Z

    if-eqz v0, :cond_0

    .line 96
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshDomainConfig(Z)V

    goto :goto_0

    .line 98
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLastRefreshTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-gtz p1, :cond_1

    .line 100
    :try_start_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getThreadPoolExecutor()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig$1;-><init>(Lcom/bytedance/sdk/component/net/tnc/AppConfig;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :catchall_0
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method updateConfig(Z)V
    .locals 2

    const-string v0, "TNCManager"

    const-string v1, "doRefresh, actual request"

    .line 231
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLoadLocalConfig()V

    const/4 v0, 0x1

    .line 234
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mLoading:Z

    if-nez p1, :cond_0

    .line 236
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mHandler:Lcom/bytedance/sdk/component/net/utils/WeakHandler;

    const/16 v0, 0x66

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/net/utils/WeakHandler;->sendEmptyMessage(I)Z

    return-void

    .line 240
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getDomainInternal()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 242
    :catch_0
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->mConfigUpdating:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method
