.class public interface abstract Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;
.super Ljava/lang/Object;
.source "ITTAdNetDepend.java"


# virtual methods
.method public abstract getAid()I
.end method

.method public abstract getChannel()Ljava/lang/String;
.end method

.method public abstract getConfigServers()[Ljava/lang/String;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getDid()Ljava/lang/String;
.end method

.method public abstract getLocationAdress(Landroid/content/Context;)Landroid/location/Address;
.end method

.method public abstract getPlatform()Ljava/lang/String;
.end method

.method public abstract getProviderInt(Landroid/content/Context;Ljava/lang/String;I)I
.end method

.method public abstract getProviderString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getVersionCode()I
.end method

.method public abstract saveMapToProvider(Landroid/content/Context;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation
.end method
