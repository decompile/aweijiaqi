.class Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;
.super Lcom/bytedance/sdk/component/net/callback/NetCallback;
.source "AppConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getDomainInternalNext(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iput p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V
    .locals 0

    .line 360
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    add-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V
    .locals 1

    if-eqz p2, :cond_4

    .line 326
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_3

    :cond_0
    const/4 p1, 0x0

    .line 332
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object v0, p1

    :goto_0
    if-nez v0, :cond_1

    .line 336
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    add-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    return-void

    :cond_1
    :try_start_1
    const-string p2, "message"

    .line 341
    invoke-virtual {v0, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    nop

    :goto_1
    const-string p2, "success"

    .line 344
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 345
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    add-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    return-void

    .line 349
    :cond_2
    :try_start_2
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$100(Lcom/bytedance/sdk/component/net/tnc/AppConfig;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 350
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    const/16 p2, 0x65

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$200(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    goto :goto_2

    .line 352
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    add-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :goto_2
    return-void

    .line 327
    :cond_4
    :goto_3
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->this$0:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/AppConfig$3;->val$index:I

    add-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->access$000(Lcom/bytedance/sdk/component/net/tnc/AppConfig;I)V

    return-void
.end method
