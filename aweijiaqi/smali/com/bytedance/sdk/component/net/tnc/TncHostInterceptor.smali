.class public Lcom/bytedance/sdk/component/net/tnc/TncHostInterceptor;
.super Ljava/lang/Object;
.source "TncHostInterceptor.java"

# interfaces
.implements Lcom/bytedance/sdk/component/b/b/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lcom/bytedance/sdk/component/b/b/u$a;)Lcom/bytedance/sdk/component/b/b/ab;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 13
    invoke-interface {p1}, Lcom/bytedance/sdk/component/b/b/u$a;->a()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 15
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getAppConfig()Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 16
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getAppConfig()Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLodeConfigInSubThread()V

    .line 18
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/t;->toString()Ljava/lang/String;

    move-result-object v1

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->handleHostMapping(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 21
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z;->f()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 24
    :cond_1
    :try_start_0
    invoke-interface {p1, v0}, Lcom/bytedance/sdk/component/b/b/u$a;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 26
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->onError(Lcom/bytedance/sdk/component/b/b/z;Ljava/lang/Exception;)V

    const/4 v1, 0x0

    .line 28
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->onResponse(Lcom/bytedance/sdk/component/b/b/z;Lcom/bytedance/sdk/component/b/b/ab;)V

    if-nez v1, :cond_2

    .line 29
    invoke-interface {p1, v0}, Lcom/bytedance/sdk/component/b/b/u$a;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v1

    :cond_2
    return-object v1
.end method
