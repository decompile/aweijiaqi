.class public Lcom/bytedance/sdk/component/net/tnc/TNCManager;
.super Ljava/lang/Object;
.source "TNCManager.java"

# interfaces
.implements Lcom/bytedance/sdk/component/net/tnc/TNCBridge;


# static fields
.field private static final MSG_UPDATE_REMOTE:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "TNCManager"

.field private static final TNC_DATA_VERSION_DEFAULT:J = 0x49637af88L

.field private static final TNC_HOST_REEPLACE_FAIILD_NUM:I = 0x3

.field private static final TNC_PROBE_CMD_GET_DOMAIN:I = 0x2710

.field private static final TNC_PROBE_CMD_TEST:I = 0x3e8

.field private static final TNC_PROBE_HEADER:Ljava/lang/String; = "tnc-cmd"

.field private static final TNC_PROBE_HEADER_SECEPTOR:Ljava/lang/String; = "@"

.field public static final TNC_SP_NAME:Ljava/lang/String; = "ttnet_tnc_config"

.field private static sInstance:Lcom/bytedance/sdk/component/net/tnc/TNCManager;


# instance fields
.field private hostReplaceMapFailed:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAppConfig:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mITTAdNetDepend:Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

.field private mInited:Z

.field private mIsMainProcess:Z

.field private mLastDoUpdateTime:J

.field private mReqErrApiMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReqErrIpMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReqErrorCnt:I

.field private mReqToApiMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReqToCnt:I

.field private mReqToIpMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTNCConfigHandler:Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

.field private mTncProbeCmd:I

.field private mTncProbeVersion:J

.field private mURLDispatchEnabled:Z


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 43
    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mLastDoUpdateTime:J

    const/4 v0, 0x0

    .line 63
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mInited:Z

    .line 68
    iput v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    const-wide v1, 0x49637af88L

    .line 69
    iput-wide v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    .line 72
    iput v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    .line 73
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    .line 74
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    .line 75
    iput v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    const/4 v0, 0x1

    .line 79
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mIsMainProcess:Z

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    .line 484
    new-instance v0, Lcom/bytedance/sdk/component/net/tnc/TNCManager$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager$1;-><init>(Lcom/bytedance/sdk/component/net/tnc/TNCManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/bytedance/sdk/component/net/tnc/TNCManager;Z)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->doUpdateRemote(Z)V

    return-void
.end method

.method private addTNCHostReplaceNum(Ljava/lang/String;)V
    .locals 3

    .line 300
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCHostReplaceMap()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 304
    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_2

    .line 308
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 311
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    return-void
.end method

.method private doUpdateRemote(Z)V
    .locals 9

    .line 499
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 503
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doUpdateRemote, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TNCManager"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    if-nez p1, :cond_1

    .line 506
    iget-wide v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mLastDoUpdateTime:J

    iget p1, v0, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->updateInterval:I

    int-to-long v0, p1

    const-wide/16 v7, 0x3e8

    mul-long v0, v0, v7

    add-long/2addr v5, v0

    cmp-long p1, v5, v3

    if-lez p1, :cond_1

    const-string p1, "doUpdateRemote, time limit"

    .line 507
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 510
    :cond_1
    iput-wide v3, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mLastDoUpdateTime:J

    .line 511
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/net/utils/NetworkUtils;->checkWifiAndGPRS(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->doRefresh(Z)Z

    return-void
.end method

.method private getConnectionIpStr(Lcom/bytedance/sdk/component/b/b/z;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p1, :cond_1

    .line 284
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/t;->a()Ljava/net/URL;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 289
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/t;->a()Ljava/net/URL;

    move-result-object p1

    .line 290
    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object p1

    .line 291
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-object v0
.end method

.method private getExceptionStr(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 2

    .line 526
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 527
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p1, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 528
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    .line 529
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static declared-synchronized getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;
    .locals 2

    const-class v0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    monitor-enter v0

    .line 57
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sInstance:Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    if-nez v1, :cond_0

    .line 58
    new-instance v1, Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sInstance:Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    .line 60
    :cond_0
    sget-object v1, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sInstance:Lcom/bytedance/sdk/component/net/tnc/TNCManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private handleTncProbe(Lcom/bytedance/sdk/component/b/b/ab;Ljava/lang/String;)V
    .locals 11

    if-nez p1, :cond_0

    return-void

    .line 412
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mIsMainProcess:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    const-string v1, "tnc-cmd"

    .line 416
    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/component/b/b/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 417
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "TNCManager"

    if-eqz v0, :cond_2

    .line 418
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "handleTncProbe, no probeProto, "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v0, "@"

    .line 421
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 422
    array-length v2, p1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    goto/16 :goto_2

    :cond_3
    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    .line 430
    :try_start_0
    aget-object v6, p1, v5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 431
    :try_start_1
    aget-object p1, p1, v2

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catchall_1
    move-exception p1

    const/4 v6, 0x0

    .line 433
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 434
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleTncProbe, probeProto except, "

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v7, v3

    .line 437
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleTncProbe, local: "

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v9, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v9, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    invoke-virtual {p1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v9, " svr: "

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-wide v9, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    cmp-long p1, v7, v9

    if-gtz p1, :cond_4

    return-void

    .line 444
    :cond_4
    iput v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    .line 445
    iput-wide v7, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    .line 446
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    const-string v9, "ttnet_tnc_config"

    invoke-virtual {p1, v9, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    .line 448
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v5, "tnc_probe_cmd"

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v5, "tnc_probe_version"

    .line 449
    invoke-interface {p1, v5, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 450
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 452
    iget p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    const/16 v5, 0x2710

    if-ne p1, v5, :cond_7

    .line 453
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object p1

    if-nez p1, :cond_5

    return-void

    .line 457
    :cond_5
    new-instance v5, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Random;-><init>(J)V

    .line 459
    iget v6, p1, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->updateRandomRange:I

    if-lez v6, :cond_6

    .line 460
    iget p1, p1, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->updateRandomRange:I

    invoke-virtual {v5, p1}, Ljava/util/Random;->nextInt(I)I

    move-result p1

    int-to-long v3, p1

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    .line 462
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleTncProbe, updateConfig delay: "

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-direct {p0, v2, v3, v4}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sendUpdateMsg(ZJ)V

    :cond_7
    return-void

    .line 423
    :cond_8
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "handleTncProbe, probeProto err, "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private isHttpRespCodeInBlack(I)Z
    .locals 4

    const/4 v0, 0x1

    const/16 v1, 0x64

    if-lt p1, v1, :cond_2

    const/16 v1, 0x3e8

    if-lt p1, v1, :cond_0

    goto :goto_0

    .line 545
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 546
    iget-object v2, v1, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->httpCodeBlack:Ljava/lang/String;

    .line 547
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, v1, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->httpCodeBlack:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 548
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x0

    return p1

    :cond_2
    :goto_0
    return v0
.end method

.method private isHttpRespCodeOk(I)Z
    .locals 1

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x190

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private isTNCHostFailedOverLimit(Ljava/lang/String;)Z
    .locals 3

    .line 326
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCHostReplaceMap()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 330
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 331
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 334
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_2

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleHostMapping, TNC host faild num over limit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TNCManager"

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v1
.end method

.method private loadProbeInfo()V
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "ttnet_tnc_config"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "tnc_probe_cmd"

    .line 129
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    const-string v1, "tnc_probe_version"

    const-wide v2, 0x49637af88L

    .line 130
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    return-void
.end method

.method private resetTNCControlState()V
    .locals 2

    const-string v0, "TNCManager"

    const-string v1, "resetTNCControlState"

    .line 515
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 517
    iput v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    .line 518
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 519
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 520
    iput v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    .line 521
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 522
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method private resetTNCHostReplaceNum(Ljava/lang/String;)V
    .locals 2

    .line 316
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private sendUpdateMsg(ZJ)V
    .locals 3

    .line 470
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 474
    iput v1, v0, Landroid/os/Message;->what:I

    .line 475
    iput p1, v0, Landroid/os/Message;->arg1:I

    const-wide/16 v1, 0x0

    cmp-long p1, p2, v1

    if-lez p1, :cond_1

    .line 477
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 479
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void
.end method


# virtual methods
.method public getAppConfig()Lcom/bytedance/sdk/component/net/tnc/AppConfig;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mAppConfig:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    return-object v0
.end method

.method public getITTAdNetDepend()Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mITTAdNetDepend:Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    return-object v0
.end method

.method public getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTNCConfigHandler:Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTNCConfigHandler()Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTNCConfigHandler:Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    return-object v0
.end method

.method public getTNCHostReplaceMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 141
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, v0, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->hostReplaceMap:Ljava/util/Map;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getURLDispatchEnabled()Z
    .locals 1

    .line 100
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mURLDispatchEnabled:Z

    return v0
.end method

.method public handleHostMapping(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 153
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "/network/get_network"

    .line 154
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "/get_domains/v4"

    .line 155
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "/ies/speed"

    .line 156
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    :cond_0
    const/4 v0, 0x0

    .line 163
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 165
    :try_start_1
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v2, v0

    .line 167
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 170
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "http"

    .line 171
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 172
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto/16 :goto_3

    .line 176
    :cond_2
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->isTNCHostFailedOverLimit(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "TNCManager"

    if-eqz v1, :cond_3

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleHostMapping, TNC host faild num over limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 181
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCHostReplaceMap()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 182
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_2

    .line 186
    :cond_4
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 187
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    return-object p1

    .line 191
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleHostMapping, match, origin: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 195
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 197
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleHostMapping, target: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 183
    :cond_7
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleHostMapping, nomatch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_3
    return-object p1
.end method

.method public handleRequestResult()V
    .locals 0

    return-void
.end method

.method public declared-synchronized initTnc(Landroid/content/Context;Z)V
    .locals 3

    monitor-enter p0

    .line 112
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mInited:Z

    if-nez v0, :cond_1

    .line 113
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    .line 114
    iput-boolean p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mIsMainProcess:Z

    .line 115
    new-instance v0, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    invoke-direct {v0, p1, p2}, Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTNCConfigHandler:Lcom/bytedance/sdk/component/net/tnc/TNCConfigHandler;

    if-eqz p2, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->loadProbeInfo()V

    :cond_0
    const-string p1, "TNCManager"

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initTnc, isMainProc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p2, " probeCmd: "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeCmd:I

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " probeVersion: "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mTncProbeVersion:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mAppConfig:Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    const/4 p1, 0x1

    .line 122
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mInited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized onError(Lcom/bytedance/sdk/component/b/b/z;Ljava/lang/Exception;)V
    .locals 6

    monitor-enter p0

    if-eqz p1, :cond_7

    .line 343
    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v0

    if-eqz v0, :cond_7

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 347
    :cond_0
    iget-boolean p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mIsMainProcess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_1

    .line 348
    monitor-exit p0

    return-void

    .line 351
    :cond_1
    :try_start_1
    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/bytedance/sdk/component/net/utils/NetworkUtils;->checkWifiAndGPRS(Landroid/content/Context;)Z

    move-result p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p2, :cond_2

    .line 352
    monitor-exit p0

    return-void

    :cond_2
    const/4 p2, 0x0

    .line 357
    :try_start_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/t;->a()Ljava/net/URL;

    move-result-object p2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-nez p2, :cond_3

    .line 362
    monitor-exit p0

    return-void

    .line 365
    :cond_3
    :try_start_3
    invoke-virtual {p2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 366
    invoke-virtual {p2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 367
    invoke-virtual {p2}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object p2

    .line 368
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getConnectionIpStr(Lcom/bytedance/sdk/component/b/b/z;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "http"

    .line 371
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "https"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v2, :cond_4

    .line 372
    monitor-exit p0

    return-void

    .line 380
    :cond_4
    :try_start_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v2, :cond_5

    .line 382
    monitor-exit p0

    return-void

    :cond_5
    :try_start_5
    const-string v3, "TNCManager"

    .line 391
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onError, url matched: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "# "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    .line 392
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    .line 393
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 391
    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget v3, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    .line 395
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    iget v3, v2, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqToCnt:I

    if-lt p2, v3, :cond_6

    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    .line 398
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result p2

    iget v3, v2, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqToApiCnt:I

    if-lt p2, v3, :cond_6

    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    .line 399
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result p2

    iget v2, v2, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqToIpCnt:I

    if-lt p2, v2, :cond_6

    const-string p2, "TNCManager"

    .line 400
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onError, url doUpate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "://"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "#"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 p1, 0x0

    .line 401
    invoke-direct {p0, v4, p1, p2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sendUpdateMsg(ZJ)V

    .line 402
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->resetTNCControlState()V

    .line 404
    :cond_6
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->addTNCHostReplaceNum(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 405
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 344
    :cond_7
    :goto_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onResponse(Lcom/bytedance/sdk/component/b/b/z;Lcom/bytedance/sdk/component/b/b/ab;)V
    .locals 7

    monitor-enter p0

    if-eqz p1, :cond_d

    if-nez p2, :cond_0

    goto/16 :goto_2

    .line 211
    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mIsMainProcess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 212
    monitor-exit p0

    return-void

    .line 215
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/net/utils/NetworkUtils;->checkWifiAndGPRS(Landroid/content/Context;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    .line 216
    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x0

    .line 220
    :try_start_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/t;->a()Ljava/net/URL;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-nez v0, :cond_3

    .line 224
    monitor-exit p0

    return-void

    .line 227
    :cond_3
    :try_start_3
    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-virtual {v0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 230
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getConnectionIpStr(Lcom/bytedance/sdk/component/b/b/z;)Ljava/lang/String;

    move-result-object p1

    .line 231
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v3

    const-string v4, "http"

    .line 233
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "https"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v4, :cond_4

    .line 234
    monitor-exit p0

    return-void

    .line 237
    :cond_4
    :try_start_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v4, :cond_5

    .line 238
    monitor-exit p0

    return-void

    :cond_5
    :try_start_5
    const-string v4, "TNCManager"

    .line 241
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onResponse, url: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getTNCConfig()Lcom/bytedance/sdk/component/net/tnc/TNCConfig;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 243
    iget-boolean v5, v4, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->probeEnable:Z

    if-eqz v5, :cond_6

    .line 244
    invoke-direct {p0, p2, v2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->handleTncProbe(Lcom/bytedance/sdk/component/b/b/ab;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_6
    if-nez v4, :cond_7

    .line 248
    monitor-exit p0

    return-void

    :cond_7
    :try_start_6
    const-string p2, "TNCManager"

    .line 257
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onResponse, url matched: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToApiMap:Ljava/util/HashMap;

    .line 258
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToIpMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    .line 259
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 257
    invoke-static {p2, v5}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v3, :cond_c

    .line 261
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->isHttpRespCodeOk(I)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 262
    iget p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqToCnt:I

    if-gtz p1, :cond_8

    iget p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    if-lez p1, :cond_9

    .line 263
    :cond_8
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->resetTNCControlState()V

    .line 265
    :cond_9
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->resetTNCHostReplaceNum(Ljava/lang/String;)V

    goto :goto_1

    .line 266
    :cond_a
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->isHttpRespCodeInBlack(I)Z

    move-result p2

    if-nez p2, :cond_c

    .line 267
    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    .line 268
    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p2, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrorCnt:I

    iget v0, v4, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqErrCnt:I

    if-lt p2, v0, :cond_b

    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrApiMap:Ljava/util/HashMap;

    .line 271
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result p2

    iget v0, v4, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqErrApiCnt:I

    if-lt p2, v0, :cond_b

    iget-object p2, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mReqErrIpMap:Ljava/util/HashMap;

    .line 272
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result p2

    iget v0, v4, Lcom/bytedance/sdk/component/net/tnc/TNCConfig;->reqErrIpCnt:I

    if-lt p2, v0, :cond_b

    const-string p2, "TNCManager"

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResponse, url doUpdate: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "#"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/component/net/utils/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 p1, 0x0

    .line 274
    invoke-direct {p0, v5, p1, p2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->sendUpdateMsg(ZJ)V

    .line 275
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->resetTNCControlState()V

    .line 277
    :cond_b
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->addTNCHostReplaceNum(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 281
    :cond_c
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 208
    :cond_d
    :goto_2
    monitor-exit p0

    return-void
.end method

.method public resetHostReplaceMapFailed()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->hostReplaceMapFailed:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public setITTAdNetDepend(Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mITTAdNetDepend:Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;

    return-void
.end method

.method public setURLDispatchEnabled(Z)V
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->mURLDispatchEnabled:Z

    return-void
.end method
