.class public Lcom/bytedance/sdk/component/net/NetClient;
.super Ljava/lang/Object;
.source "NetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/net/NetClient$Builder;
    }
.end annotation


# instance fields
.field private okHttpClient:Lcom/bytedance/sdk/component/b/b/w;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/component/net/NetClient$Builder;)V
    .locals 4

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/bytedance/sdk/component/b/b/w$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/w$a;-><init>()V

    iget v1, p1, Lcom/bytedance/sdk/component/net/NetClient$Builder;->connectTimeout:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 31
    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/b/b/w$a;->a(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v0

    iget v1, p1, Lcom/bytedance/sdk/component/net/NetClient$Builder;->writeTimeout:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 32
    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/b/b/w$a;->c(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v0

    iget v1, p1, Lcom/bytedance/sdk/component/net/NetClient$Builder;->readTimeout:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 33
    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/b/b/w$a;->b(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/b/b/w$a;

    move-result-object v0

    .line 34
    iget-boolean p1, p1, Lcom/bytedance/sdk/component/net/NetClient$Builder;->enableTNC:Z

    if-eqz p1, :cond_0

    .line 35
    new-instance p1, Lcom/bytedance/sdk/component/net/tnc/TncHostInterceptor;

    invoke-direct {p1}, Lcom/bytedance/sdk/component/net/tnc/TncHostInterceptor;-><init>()V

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/b/b/w$a;->a(Lcom/bytedance/sdk/component/b/b/u;)Lcom/bytedance/sdk/component/b/b/w$a;

    .line 37
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/w$a;->a()Lcom/bytedance/sdk/component/b/b/w;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/NetClient;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/component/net/NetClient$Builder;Lcom/bytedance/sdk/component/net/NetClient$1;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/NetClient;-><init>(Lcom/bytedance/sdk/component/net/NetClient$Builder;)V

    return-void
.end method

.method public static openDeubg()V
    .locals 1

    .line 68
    sget-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->a:Lcom/bytedance/sdk/component/adnet/d/d$a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/adnet/d/d;->a(Lcom/bytedance/sdk/component/adnet/d/d$a;)V

    return-void
.end method


# virtual methods
.method public getDownloadExecutor()Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;
    .locals 2

    .line 80
    new-instance v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/NetClient;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    return-object v0
.end method

.method public getGetExecutor()Lcom/bytedance/sdk/component/net/executor/GetExecutor;
    .locals 2

    .line 76
    new-instance v0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/NetClient;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    return-object v0
.end method

.method public getPostExecutor()Lcom/bytedance/sdk/component/net/executor/PostExecutor;
    .locals 2

    .line 72
    new-instance v0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/NetClient;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    return-object v0
.end method

.method public tryInitTTAdNet(Landroid/content/Context;ZZLcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;)V
    .locals 1

    if-eqz p1, :cond_3

    .line 53
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->setURLDispatchEnabled(Z)V

    .line 54
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p3

    invoke-virtual {p3, p4}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->setITTAdNetDepend(Lcom/bytedance/sdk/component/net/tnc/ITTAdNetDepend;)V

    .line 55
    invoke-static {}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->getInstance()Lcom/bytedance/sdk/component/net/tnc/TNCManager;

    move-result-object p3

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result p4

    invoke-virtual {p3, p1, p4}, Lcom/bytedance/sdk/component/net/tnc/TNCManager;->initTnc(Landroid/content/Context;Z)V

    .line 56
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMessageProcess(Landroid/content/Context;)Z

    move-result p3

    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result p3

    if-nez p3, :cond_1

    if-eqz p2, :cond_1

    .line 57
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLoadLocalConfig()V

    .line 58
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    .line 60
    :cond_1
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/utils/ProcessUtils;->isMainProcess(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_2

    return-void

    .line 63
    :cond_2
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryLoadLocalConfig()V

    .line 64
    invoke-static {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->tryRefreshConfig()V

    return-void

    .line 51
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "tryInitAdTTNet context is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
