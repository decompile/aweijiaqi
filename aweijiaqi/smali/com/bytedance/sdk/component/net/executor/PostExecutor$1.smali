.class Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;
.super Ljava/lang/Object;
.source "PostExecutor.java"

# interfaces
.implements Lcom/bytedance/sdk/component/b/b/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/net/executor/PostExecutor;->enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/bytedance/sdk/component/net/executor/PostExecutor;

.field final synthetic val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/net/executor/PostExecutor;Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    iput-object p2, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V
    .locals 1

    .line 111
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    if-eqz p1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    invoke-virtual {p1, v0, p2}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    :cond_0
    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/b/b/e;Lcom/bytedance/sdk/component/b/b/ab;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    if-eqz p1, :cond_1

    .line 119
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    if-eqz p2, :cond_1

    .line 121
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 123
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 124
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    new-instance p1, Lcom/bytedance/sdk/component/net/NetResponse;

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v1

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v2

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ac;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->l()J

    move-result-wide v6

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/b/b/ab;->m()J

    move-result-wide v8

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V

    .line 128
    iget-object p2, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V

    :cond_1
    return-void
.end method
