.class public Lcom/bytedance/sdk/component/net/executor/GetExecutor;
.super Lcom/bytedance/sdk/component/net/executor/NetExecutor;
.source "GetExecutor.java"


# static fields
.field public static final FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

.field public static final Not_FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

.field private static final TAG:Ljava/lang/String; = "GetExecutor"


# instance fields
.field private cacheControl:Lcom/bytedance/sdk/component/b/b/d;

.field private paramsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/bytedance/sdk/component/b/b/d$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/d$a;-><init>()V

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/d$a;->a()Lcom/bytedance/sdk/component/b/b/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/d$a;->c()Lcom/bytedance/sdk/component/b/b/d;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

    .line 32
    new-instance v0, Lcom/bytedance/sdk/component/b/b/d$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/d$a;-><init>()V

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/d$a;->c()Lcom/bytedance/sdk/component/b/b/d;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->Not_FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

    return-void
.end method

.method public constructor <init>(Lcom/bytedance/sdk/component/b/b/w;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    .line 34
    sget-object p1, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->cacheControl:Lcom/bytedance/sdk/component/b/b/d;

    .line 36
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addParams(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const-string p1, "GetExecutor"

    const-string p2, "name cannot be null !!!"

    .line 54
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    .locals 7

    .line 70
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 72
    new-instance v1, Lcom/bytedance/sdk/component/b/b/t$a;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/b/b/t$a;-><init>()V

    .line 74
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 75
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/component/b/b/t$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 76
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/component/b/b/t$a;->d(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 77
    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v3

    .line 78
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "/"

    .line 79
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 80
    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 82
    :cond_0
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/component/b/b/t$a;->e(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 85
    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 86
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 87
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 88
    iget-object v5, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-virtual {v2, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 92
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lcom/bytedance/sdk/component/b/b/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 100
    :cond_3
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 101
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_2

    .line 104
    :cond_4
    iget-object v2, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->cacheControl:Lcom/bytedance/sdk/component/b/b/d;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/d;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 105
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 107
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/t$a;->c()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/t;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/net/executor/GetExecutor$1;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/component/net/executor/GetExecutor$1;-><init>(Lcom/bytedance/sdk/component/net/executor/GetExecutor;Lcom/bytedance/sdk/component/net/callback/NetCallback;)V

    .line 111
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/b/b/e;->a(Lcom/bytedance/sdk/component/b/b/f;)V

    return-void

    :catchall_0
    move-exception v0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 96
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Url is not a valid HTTP or HTTPS URL"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void
.end method

.method public execute()Lcom/bytedance/sdk/component/net/NetResponse;
    .locals 13

    .line 143
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 145
    new-instance v1, Lcom/bytedance/sdk/component/b/b/t$a;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/b/b/t$a;-><init>()V

    const/4 v2, 0x0

    .line 147
    :try_start_0
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 148
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/component/b/b/t$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 149
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/component/b/b/t$a;->d(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 150
    invoke-virtual {v3}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "/"

    .line 152
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    .line 153
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 155
    :cond_0
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/component/b/b/t$a;->e(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;

    .line 158
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 159
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 160
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 161
    iget-object v6, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 164
    :cond_2
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 165
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, Lcom/bytedance/sdk/component/b/b/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 172
    :cond_3
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 173
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_2

    .line 176
    :cond_4
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->cacheControl:Lcom/bytedance/sdk/component/b/b/d;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/d;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 177
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 179
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/t$a;->c()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/t;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 183
    :try_start_1
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    .line 184
    invoke-interface {v0}, Lcom/bytedance/sdk/component/b/b/e;->b()Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 186
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 188
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v3, 0x0

    .line 190
    :goto_3
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 191
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 194
    :cond_5
    new-instance v1, Lcom/bytedance/sdk/component/net/NetResponse;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v4

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v5

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/component/b/b/ac;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->l()J

    move-result-wide v9

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->m()J

    move-result-wide v11

    move-object v3, v1

    invoke-direct/range {v3 .. v12}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 199
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_6
    return-object v2

    :catchall_0
    move-exception v0

    .line 168
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v2
.end method

.method public removeParams(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->paramsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setShouldCache(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 45
    sget-object p1, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->Not_FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->cacheControl:Lcom/bytedance/sdk/component/b/b/d;

    goto :goto_0

    .line 47
    :cond_0
    sget-object p1, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->FORCE_NET:Lcom/bytedance/sdk/component/b/b/d;

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->cacheControl:Lcom/bytedance/sdk/component/b/b/d;

    :goto_0
    return-void
.end method
