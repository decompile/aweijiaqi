.class public Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;
.super Lcom/bytedance/sdk/component/net/executor/NetExecutor;
.source "DownloadExecutor.java"


# static fields
.field public static final SEGMENT_SIZE:I = 0x4000

.field private static final TAG:Ljava/lang/String; = "DownloadExecutor"


# instance fields
.field public file:Ljava/io/File;

.field public tempFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/component/b/b/w;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    return-void
.end method

.method static synthetic access$000(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->deleteFileWhenError()V

    return-void
.end method

.method static synthetic access$100(Ljava/util/Map;)J
    .locals 2

    .line 32
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->getContentLength(Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Ljava/util/Map;)Z
    .locals 0

    .line 32
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->isSupportRange(Ljava/util/Map;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Ljava/util/Map;)Z
    .locals 0

    .line 32
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->isGzipContent(Ljava/util/Map;)Z

    move-result p0

    return p0
.end method

.method private deleteFileWhenError()V
    .locals 1

    .line 518
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :catchall_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    return-void
.end method

.method private static getContentLength(Ljava/util/Map;)J
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    const-string v0, "content-length"

    .line 485
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 486
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, "Content-Length"

    .line 487
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 488
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    .line 490
    :goto_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_2

    return-wide v1

    :cond_2
    if-eqz p0, :cond_3

    .line 496
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_3
    return-wide v1
.end method

.method private static isGzipContent(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "Content-Encoding"

    .line 506
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string v0, "gzip"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method private static isSupportRange(Ljava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "Accept-Ranges"

    .line 469
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "bytes"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    return v2

    :cond_0
    const-string v0, "accept-ranges"

    .line 473
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    const-string v0, "Content-Range"

    .line 476
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 477
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "content-range"

    .line 478
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/String;

    :cond_2
    if-eqz v0, :cond_3

    .line 480
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    return v2
.end method


# virtual methods
.method public enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    .locals 13

    .line 63
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 70
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 73
    new-instance v0, Lcom/bytedance/sdk/component/net/NetResponse;

    const/4 v4, 0x1

    const/16 v5, 0xc8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v6, "Success"

    move-object v3, v0

    move-wide v9, v11

    invoke-direct/range {v3 .. v12}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V

    .line 74
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V

    .line 75
    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V

    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-gez v0, :cond_2

    goto :goto_0

    :cond_2
    move-wide v1, v3

    .line 86
    :goto_0
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 89
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 91
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Range"

    invoke-virtual {p0, v4, v3}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 95
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Url is Empty"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void

    .line 99
    :cond_3
    :try_start_0
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->url:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 106
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v5, v4}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_1

    .line 108
    :cond_4
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 110
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v3, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    new-instance v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;

    invoke-direct {v3, p0, p1, v1, v2}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;-><init>(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;Lcom/bytedance/sdk/component/net/callback/NetCallback;J)V

    .line 111
    invoke-interface {v0, v3}, Lcom/bytedance/sdk/component/b/b/e;->a(Lcom/bytedance/sdk/component/b/b/f;)V

    return-void

    .line 101
    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Url is not a valid HTTP or HTTPS URL"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void

    :cond_5
    :goto_2
    if-eqz p1, :cond_6

    .line 65
    new-instance v0, Ljava/io/IOException;

    const-string v1, "File info is null, please exec setFileInfo(String dir, String fileName)"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    :cond_6
    return-void
.end method

.method public execute()Lcom/bytedance/sdk/component/net/NetResponse;
    .locals 25

    move-object/from16 v1, p0

    const-string v2, "Error occured when calling tmpFile.close"

    const-string v3, "Error occured when calling consumingContent"

    const-string v4, "Error occured when calling InputStream.close"

    .line 280
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    const/4 v5, 0x0

    if-eqz v0, :cond_21

    iget-object v6, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    if-nez v6, :cond_0

    goto/16 :goto_20

    .line 284
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const-wide/16 v6, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-eqz v0, :cond_1

    .line 285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 286
    new-instance v0, Lcom/bytedance/sdk/component/net/NetResponse;

    const/4 v9, 0x1

    const/16 v10, 0xc8

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-string v11, "Success"

    move-object v8, v0

    move-wide/from16 v14, v16

    invoke-direct/range {v8 .. v17}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V

    .line 287
    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V

    return-object v0

    .line 291
    :cond_1
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-gez v0, :cond_2

    move-wide v8, v6

    .line 297
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->getTag()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 302
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bytes="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v12, "Range"

    invoke-virtual {v1, v12, v10}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->url:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    const-string v12, "DownloadExecutor"

    if-eqz v10, :cond_3

    const-string v0, "execute: Url is Empty"

    .line 306
    invoke-static {v12, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v5

    .line 310
    :cond_3
    :try_start_0
    iget-object v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->url:Ljava/lang/String;

    invoke-virtual {v0, v10}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 316
    iget-object v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 317
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v0, v14, v13}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_0

    .line 321
    :cond_4
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 324
    :try_start_1
    iget-object v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v10, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/b/b/e;->b()Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 325
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v10

    if-eqz v10, :cond_20

    .line 326
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 327
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object v13

    if-eqz v13, :cond_5

    const/4 v14, 0x0

    .line 329
    :goto_1
    invoke-virtual {v13}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v15

    if-ge v14, v15, :cond_5

    .line 330
    invoke-virtual {v13, v14}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v10, v15, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v14, v14, 0x1

    const/4 v5, 0x0

    goto :goto_1

    .line 333
    :cond_5
    new-instance v5, Lcom/bytedance/sdk/component/net/NetResponse;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v14

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v15

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->l()J

    move-result-wide v19

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->m()J

    move-result-wide v21

    move-object v13, v5

    move-object/from16 v16, v17

    move-object/from16 v17, v10

    invoke-direct/range {v13 .. v22}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V

    .line 336
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v13

    invoke-virtual {v13}, Lcom/bytedance/sdk/component/b/b/ac;->b()J

    move-result-wide v13

    cmp-long v15, v13, v6

    if-gtz v15, :cond_6

    .line 338
    invoke-static {v10}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->getContentLength(Ljava/util/Map;)J

    move-result-wide v13

    .line 340
    :cond_6
    iget-object v15, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 342
    invoke-static {v10}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->isSupportRange(Ljava/util/Map;)Z

    move-result v15

    move-object/from16 v18, v2

    if-eqz v15, :cond_7

    add-long/2addr v13, v6

    const-string v2, "Content-Range"

    .line 345
    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 346
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_7

    move-object/from16 v20, v3

    .line 347
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v21, v4

    const-string v4, "bytes "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v6, 0x1

    sub-long v6, v13, v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 348
    invoke-static {v2, v3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_8

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "execute: The Content-Range Header is invalid Assume["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "] vs Real["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "], please remove the temporary file ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "]."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->deleteFileWhenError()V

    const/4 v2, 0x0

    return-object v2

    :cond_7
    move-object/from16 v20, v3

    move-object/from16 v21, v4

    :cond_8
    const-wide/16 v2, 0x0

    cmp-long v4, v13, v2

    if-lez v4, :cond_a

    .line 358
    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v4, v2, v13

    if-nez v4, :cond_a

    .line 359
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 361
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v5, v0}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V

    return-object v5

    :cond_9
    const-string v0, "Rename fail"

    .line 364
    invoke-static {v12, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v2, 0x0

    return-object v2

    .line 372
    :cond_a
    :try_start_2
    new-instance v2, Ljava/io/RandomAccessFile;

    iget-object v3, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    const-string v4, "rw"

    invoke-direct {v2, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v15, :cond_b

    .line 375
    :try_start_3
    invoke-virtual {v2, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    move-wide v3, v8

    goto :goto_3

    :cond_b
    const-wide/16 v3, 0x0

    .line 378
    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    :goto_2
    const-wide/16 v3, 0x0

    goto :goto_3

    :catchall_1
    const/4 v2, 0x0

    goto :goto_2

    .line 386
    :goto_3
    :try_start_4
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ac;->c()Ljava/io/InputStream;

    move-result-object v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_10

    .line 387
    :try_start_5
    invoke-static {v10}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->isGzipContent(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_c

    instance-of v0, v6, Ljava/util/zip/GZIPInputStream;

    if-nez v0, :cond_c

    .line 388
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v6}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v6, v0

    :cond_c
    const/16 v0, 0x4000

    new-array v7, v0, [B

    const/4 v10, 0x0

    const-wide/16 v23, 0x0

    :goto_4
    rsub-int v11, v10, 0x4000

    .line 396
    invoke-virtual {v6, v7, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_f

    const/16 v22, 0x1

    const/4 v0, -0x1

    if-eq v11, v0, :cond_10

    add-int/2addr v10, v11

    int-to-long v0, v11

    add-long v23, v23, v0

    const-wide/16 v0, 0x4000

    .line 399
    :try_start_6
    rem-long v0, v23, v0

    const-wide/16 v16, 0x0

    cmp-long v11, v0, v16

    if-eqz v11, :cond_e

    sub-long v0, v13, v8

    cmp-long v11, v23, v0

    if-nez v11, :cond_d

    goto :goto_5

    :cond_d
    const/16 v22, 0x0

    :cond_e
    :goto_5
    if-eqz v22, :cond_f

    .line 401
    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    const/4 v1, 0x0

    .line 402
    :try_start_7
    invoke-virtual {v2, v7, v1, v10}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    int-to-long v10, v10

    add-long/2addr v3, v10

    const/4 v10, 0x0

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_7

    :cond_f
    const/4 v1, 0x0

    :goto_6
    const/16 v0, 0x4000

    move-object/from16 v1, p0

    goto :goto_4

    :catchall_3
    move-exception v0

    const/4 v1, 0x0

    :goto_7
    move-object/from16 v3, p0

    :goto_8
    move-object/from16 v7, v18

    move-object/from16 v4, v20

    goto/16 :goto_16

    :cond_10
    const/4 v1, 0x0

    if-nez v15, :cond_11

    move-object/from16 v3, p0

    .line 410
    :try_start_8
    iget-object v0, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v13

    goto :goto_9

    :catchall_4
    move-exception v0

    goto :goto_8

    :cond_11
    move-object/from16 v3, p0

    :goto_9
    const-wide/16 v7, 0x0

    cmp-long v0, v13, v7

    if-lez v0, :cond_17

    .line 413
    iget-object v0, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v7

    cmp-long v0, v7, v13

    if-nez v0, :cond_17

    .line 414
    iget-object v0, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    iget-object v4, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 416
    iget-object v0, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v5, v0}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    if-eqz v6, :cond_12

    .line 436
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto :goto_a

    .line 438
    :catchall_5
    :try_start_a
    invoke-static/range {v21 .. v21}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    :cond_12
    :goto_a
    if-eqz v6, :cond_13

    .line 444
    :try_start_b
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    goto :goto_b

    :catchall_6
    :try_start_c
    new-array v0, v1, [Ljava/lang/Object;

    move-object/from16 v4, v20

    .line 449
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0

    .line 452
    :cond_13
    :goto_b
    :try_start_d
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_7

    goto :goto_c

    :catchall_7
    :try_start_e
    new-array v0, v1, [Ljava/lang/Object;

    move-object/from16 v7, v18

    .line 454
    invoke-static {v7, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0

    :goto_c
    return-object v5

    :cond_14
    move-object/from16 v7, v18

    move-object/from16 v4, v20

    if-eqz v6, :cond_15

    .line 436
    :try_start_f
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    goto :goto_d

    .line 438
    :catchall_8
    :try_start_10
    invoke-static/range {v21 .. v21}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0

    :cond_15
    :goto_d
    if-eqz v6, :cond_16

    .line 444
    :try_start_11
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    goto :goto_e

    :catchall_9
    :try_start_12
    new-array v0, v1, [Ljava/lang/Object;

    .line 449
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_0

    .line 452
    :cond_16
    :goto_e
    :try_start_13
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    :goto_f
    const/4 v1, 0x0

    goto :goto_10

    :catchall_a
    :try_start_14
    new-array v0, v1, [Ljava/lang/Object;

    .line 454
    invoke-static {v7, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_0

    goto :goto_f

    :goto_10
    return-object v1

    :cond_17
    move-object/from16 v7, v18

    move-object/from16 v4, v20

    .line 422
    :try_start_15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " tempFile.length() == fileSize is"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v5, v8, v13

    if-nez v5, :cond_18

    const/4 v5, 0x1

    goto :goto_11

    :cond_18
    const/4 v5, 0x0

    :goto_11
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_e

    if-eqz v6, :cond_19

    .line 436
    :try_start_16
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_b

    goto :goto_12

    .line 438
    :catchall_b
    :try_start_17
    invoke-static/range {v21 .. v21}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_0

    :cond_19
    :goto_12
    if-eqz v6, :cond_1a

    .line 444
    :try_start_18
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_c

    goto :goto_13

    :catchall_c
    :try_start_19
    new-array v0, v1, [Ljava/lang/Object;

    .line 449
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_0

    .line 452
    :cond_1a
    :goto_13
    :try_start_1a
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_d

    :goto_14
    const/4 v1, 0x0

    goto :goto_15

    :catchall_d
    :try_start_1b
    new-array v0, v1, [Ljava/lang/Object;

    .line 454
    invoke-static {v7, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_0

    goto :goto_14

    :goto_15
    return-object v1

    :catchall_e
    move-exception v0

    goto :goto_16

    :catchall_f
    move-exception v0

    move-object v3, v1

    move-object/from16 v7, v18

    move-object/from16 v4, v20

    const/4 v1, 0x0

    goto :goto_16

    :catchall_10
    move-exception v0

    move-object v3, v1

    move-object/from16 v7, v18

    move-object/from16 v4, v20

    const/4 v1, 0x0

    const/4 v6, 0x0

    :goto_16
    :try_start_1c
    const-string v5, "Error occured when FileRequest.parseHttpResponse"

    .line 426
    invoke-static {v5}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    if-nez v15, :cond_1b

    .line 429
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->deleteFileWhenError()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_14

    :cond_1b
    if-eqz v6, :cond_1c

    .line 436
    :try_start_1d
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_11

    goto :goto_17

    .line 438
    :catchall_11
    :try_start_1e
    invoke-static/range {v21 .. v21}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_0

    :cond_1c
    :goto_17
    if-eqz v6, :cond_1d

    .line 444
    :try_start_1f
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_12

    goto :goto_18

    :catchall_12
    :try_start_20
    new-array v0, v1, [Ljava/lang/Object;

    .line 449
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_0

    .line 452
    :cond_1d
    :goto_18
    :try_start_21
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_13

    :goto_19
    const/4 v1, 0x0

    goto :goto_1a

    :catchall_13
    :try_start_22
    new-array v0, v1, [Ljava/lang/Object;

    .line 454
    invoke-static {v7, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_0

    goto :goto_19

    :goto_1a
    return-object v1

    :catchall_14
    move-exception v0

    if-eqz v6, :cond_1e

    .line 436
    :try_start_23
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_15

    goto :goto_1b

    .line 438
    :catchall_15
    :try_start_24
    invoke-static/range {v21 .. v21}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_0

    goto :goto_1b

    :catch_0
    move-exception v0

    goto :goto_1e

    :cond_1e
    :goto_1b
    if-eqz v6, :cond_1f

    .line 444
    :try_start_25
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_16

    goto :goto_1c

    :catchall_16
    :try_start_26
    new-array v5, v1, [Ljava/lang/Object;

    .line 449
    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_0

    .line 452
    :cond_1f
    :goto_1c
    :try_start_27
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_17

    goto :goto_1d

    :catchall_17
    :try_start_28
    new-array v1, v1, [Ljava/lang/Object;

    .line 454
    invoke-static {v7, v1}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    :goto_1d
    throw v0
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_0

    :cond_20
    move-object v3, v1

    goto :goto_1f

    :catch_1
    move-exception v0

    move-object v3, v1

    .line 460
    :goto_1e
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 461
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->deleteFileWhenError()V

    :goto_1f
    const/4 v1, 0x0

    return-object v1

    :catch_2
    move-object v3, v1

    move-object v1, v5

    const-string v0, "execute: Url is not a valid HTTP or HTTPS URL"

    .line 312
    invoke-static {v12, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_21
    :goto_20
    move-object v3, v1

    move-object v1, v5

    return-object v1
.end method

.method public setFileInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 48
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 52
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 56
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    .line 57
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".temp"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    return-void
.end method

.method protected updateProgress(JJLcom/bytedance/sdk/component/net/callback/NetCallback;)V
    .locals 6

    if-eqz p5, :cond_0

    move-object v0, p5

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 512
    invoke-virtual/range {v0 .. v5}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onDownloadProgress(Lcom/bytedance/sdk/component/net/executor/NetExecutor;JJ)V

    :cond_0
    return-void
.end method
