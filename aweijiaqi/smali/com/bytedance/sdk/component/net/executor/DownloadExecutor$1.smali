.class Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;
.super Ljava/lang/Object;
.source "DownloadExecutor.java"

# interfaces
.implements Lcom/bytedance/sdk/component/b/b/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

.field final synthetic val$cachedSize:J

.field final synthetic val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;Lcom/bytedance/sdk/component/net/callback/NetCallback;J)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iput-object p2, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iput-wide p3, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V
    .locals 1

    .line 114
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    if-eqz p1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-virtual {p1, v0, p2}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    .line 117
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-static {p1}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$000(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;)V

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/b/b/e;Lcom/bytedance/sdk/component/b/b/ab;)V
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v2, "Error occured when calling tmpFile.close"

    const-string v3, "Error occured when calling consumingContent"

    const-string v4, "Error occured when calling InputStream.close"

    .line 122
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    if-eqz v0, :cond_17

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    if-eqz p2, :cond_17

    .line 125
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v6, 0x0

    .line 127
    :goto_0
    invoke-virtual {v5}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v7

    if-ge v6, v7, :cond_0

    .line 128
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 131
    :cond_0
    new-instance v13, Lcom/bytedance/sdk/component/net/NetResponse;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v7

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->l()J

    move-result-wide v11

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->m()J

    move-result-wide v16

    move-object v5, v13

    move-object v9, v0

    move-object v15, v13

    move-wide/from16 v13, v16

    invoke-direct/range {v5 .. v14}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V

    .line 132
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 134
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/sdk/component/b/b/ac;->b()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v9, v5, v7

    if-gtz v9, :cond_1

    .line 136
    invoke-static {v0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$100(Ljava/util/Map;)J

    move-result-wide v5

    .line 139
    :cond_1
    invoke-static {v0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$200(Ljava/util/Map;)Z

    move-result v9

    const/4 v10, -0x1

    if-eqz v9, :cond_2

    .line 141
    iget-wide v11, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    add-long/2addr v5, v11

    const-string v11, "Content-Range"

    .line 142
    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 143
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 144
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "bytes "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v13, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v13, "-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v13, 0x1

    sub-long v13, v5, v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 145
    invoke-static {v11, v12}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v13

    if-ne v13, v10, :cond_2

    .line 146
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-static {v0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$000(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;)V

    .line 147
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The Content-Range Header is invalid Assume["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "] vs Real["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "], please remove the temporary file ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v5, v5, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, "]."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void

    :cond_2
    move-wide v11, v5

    const-string v5, "Rename fail"

    cmp-long v6, v11, v7

    if-lez v6, :cond_4

    .line 156
    iget-object v6, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v6, v6, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v6, v6, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v13

    cmp-long v6, v13, v11

    if-nez v6, :cond_4

    .line 157
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v2, v2, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    iget-object v5, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    move-wide v6, v11

    move-wide v8, v11

    invoke-virtual/range {v5 .. v10}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->updateProgress(JJLcom/bytedance/sdk/component/net/callback/NetCallback;)V

    .line 160
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v15, v0}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V

    .line 161
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-virtual {v0, v2, v15}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V

    goto :goto_1

    .line 163
    :cond_3
    iget-object v0, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v2, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    :goto_1
    return-void

    .line 171
    :cond_4
    :try_start_0
    new-instance v13, Ljava/io/RandomAccessFile;

    iget-object v14, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v14, v14, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    const-string v6, "rw"

    invoke-direct {v13, v14, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v9, :cond_5

    move-wide/from16 v22, v11

    .line 174
    :try_start_1
    iget-wide v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    invoke-virtual {v13, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 175
    iget-wide v10, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    goto :goto_3

    :cond_5
    move-wide/from16 v22, v11

    .line 177
    invoke-virtual {v13, v7, v8}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    :goto_2
    move-wide v10, v7

    goto :goto_3

    :catchall_1
    move-wide/from16 v22, v11

    const/4 v13, 0x0

    goto :goto_2

    .line 185
    :goto_3
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v12

    invoke-virtual {v12}, Lcom/bytedance/sdk/component/b/b/ac;->c()Ljava/io/InputStream;

    move-result-object v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_b

    .line 186
    :try_start_3
    invoke-static {v0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$300(Ljava/util/Map;)Z

    move-result v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_6

    :try_start_4
    instance-of v0, v12, Ljava/util/zip/GZIPInputStream;

    if-nez v0, :cond_6

    .line 187
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v12}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v12, v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v11, v1

    :goto_4
    move-object v6, v12

    move-object v8, v13

    goto/16 :goto_11

    :cond_6
    :goto_5
    const/16 v0, 0x4000

    :try_start_5
    new-array v14, v0, [B

    move-wide/from16 v16, v7

    move-wide/from16 v24, v10

    const/4 v10, 0x0

    :goto_6
    rsub-int v11, v10, 0x4000

    .line 194
    invoke-virtual {v12, v14, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_a

    const/16 v18, 0x1

    const/4 v6, -0x1

    if-eq v11, v6, :cond_a

    add-int/2addr v10, v11

    int-to-long v0, v11

    add-long v0, v16, v0

    const-wide/16 v16, 0x4000

    .line 197
    :try_start_6
    rem-long v16, v0, v16
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    cmp-long v11, v16, v7

    if-eqz v11, :cond_8

    move-object/from16 v11, p0

    :try_start_7
    iget-wide v6, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    sub-long v6, v22, v6

    cmp-long v16, v0, v6

    if-nez v16, :cond_7

    goto :goto_7

    :cond_7
    const/16 v18, 0x0

    goto :goto_7

    :catchall_3
    move-exception v0

    goto :goto_4

    :cond_8
    move-object/from16 v11, p0

    :goto_7
    if-eqz v18, :cond_9

    move-wide/from16 v6, v24

    .line 199
    :try_start_8
    invoke-virtual {v13, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 v8, 0x0

    .line 200
    invoke-virtual {v13, v14, v8, v10}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-object v8, v13

    move-object/from16 v25, v14

    int-to-long v13, v10

    add-long/2addr v6, v13

    const/4 v10, 0x0

    goto :goto_8

    :catchall_4
    move-exception v0

    goto/16 :goto_f

    :cond_9
    move-object v8, v13

    move-wide/from16 v6, v24

    move-object/from16 v25, v14

    .line 204
    :goto_8
    :try_start_9
    iget-object v13, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    move-wide/from16 v26, v6

    iget-wide v6, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$cachedSize:J

    add-long v17, v6, v0

    iget-object v6, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    move-object/from16 v16, v13

    move-wide/from16 v19, v22

    move-object/from16 v21, v6

    invoke-virtual/range {v16 .. v21}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->updateProgress(JJLcom/bytedance/sdk/component/net/callback/NetCallback;)V

    move-wide/from16 v16, v0

    move-object v13, v8

    move-object v1, v11

    move-object/from16 v14, v25

    move-wide/from16 v24, v26

    const/16 v0, 0x4000

    const-wide/16 v7, 0x0

    goto :goto_6

    :catchall_5
    move-exception v0

    move-object/from16 v11, p0

    goto/16 :goto_f

    :cond_a
    move-object v11, v1

    move-object v8, v13

    if-nez v9, :cond_b

    .line 210
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    move-wide/from16 v22, v0

    goto :goto_9

    :catchall_6
    move-exception v0

    goto/16 :goto_10

    :cond_b
    :goto_9
    const-wide/16 v0, 0x0

    cmp-long v6, v22, v0

    if-lez v6, :cond_d

    .line 213
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long v6, v0, v22

    if-nez v6, :cond_d

    .line 214
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v1, v1, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 216
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    move-object/from16 v19, v0

    move-wide/from16 v20, v22

    move-object/from16 v24, v1

    invoke-virtual/range {v19 .. v24}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->updateProgress(JJLcom/bytedance/sdk/component/net/callback/NetCallback;)V

    .line 217
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v0, v0, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->file:Ljava/io/File;

    invoke-virtual {v15, v0}, Lcom/bytedance/sdk/component/net/NetResponse;->setFile(Ljava/io/File;)V

    .line 218
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-virtual {v0, v1, v15}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V

    goto :goto_b

    .line 220
    :cond_c
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v6}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    goto :goto_b

    .line 223
    :cond_d
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " tempFile.length() == fileSize is"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    iget-object v7, v7, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->tempFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v13

    cmp-long v7, v13, v22

    if-nez v7, :cond_e

    const/4 v7, 0x1

    goto :goto_a

    :cond_e
    const/4 v7, 0x0

    :goto_a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v5}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    :goto_b
    if-eqz v12, :cond_f

    .line 236
    :try_start_a
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    goto :goto_c

    .line 238
    :catchall_7
    invoke-static {v4}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V

    :cond_f
    :goto_c
    if-eqz v12, :cond_10

    .line 244
    :try_start_b
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_8

    goto :goto_d

    :catchall_8
    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Object;

    .line 249
    invoke-static {v3, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_e

    :cond_10
    :goto_d
    const/4 v1, 0x0

    .line 252
    :goto_e
    :try_start_c
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_9

    goto/16 :goto_1a

    :catchall_9
    new-array v0, v1, [Ljava/lang/Object;

    .line 254
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1a

    :catchall_a
    move-exception v0

    move-object v11, v1

    :goto_f
    move-object v8, v13

    :goto_10
    move-object v6, v12

    goto :goto_11

    :catchall_b
    move-exception v0

    move-object v11, v1

    move-object v8, v13

    const/4 v6, 0x0

    :goto_11
    :try_start_d
    const-string v1, "Error occured when FileRequest.parseHttpResponse"

    .line 226
    invoke-static {v1}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 228
    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v5, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    new-instance v7, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5, v7}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    if-nez v9, :cond_11

    .line 230
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-static {v0}, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;->access$000(Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_f

    :cond_11
    if-eqz v6, :cond_12

    .line 236
    :try_start_e
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_c

    goto :goto_12

    .line 238
    :catchall_c
    invoke-static {v4}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V

    :cond_12
    :goto_12
    if-eqz v6, :cond_13

    .line 244
    :try_start_f
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_d

    goto :goto_13

    :catchall_d
    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Object;

    .line 249
    invoke-static {v3, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_14

    :cond_13
    :goto_13
    const/4 v1, 0x0

    .line 252
    :goto_14
    :try_start_10
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_e

    goto :goto_15

    :catchall_e
    new-array v0, v1, [Ljava/lang/Object;

    .line 254
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_15
    return-void

    :catchall_f
    move-exception v0

    if-eqz v6, :cond_14

    .line 236
    :try_start_11
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_10

    goto :goto_16

    .line 238
    :catchall_10
    invoke-static {v4}, Lcom/bytedance/sdk/component/net/utils/NetLog;->v(Ljava/lang/String;)V

    :cond_14
    :goto_16
    if-eqz v6, :cond_15

    .line 244
    :try_start_12
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_11

    goto :goto_17

    :catchall_11
    const/4 v1, 0x0

    new-array v4, v1, [Ljava/lang/Object;

    .line 249
    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_18

    :cond_15
    :goto_17
    const/4 v1, 0x0

    .line 252
    :goto_18
    :try_start_13
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_12

    goto :goto_19

    :catchall_12
    new-array v1, v1, [Ljava/lang/Object;

    .line 254
    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    :goto_19
    throw v0

    :cond_16
    move-object v11, v1

    .line 261
    iget-object v0, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->val$callback:Lcom/bytedance/sdk/component/net/callback/NetCallback;

    iget-object v1, v11, Lcom/bytedance/sdk/component/net/executor/DownloadExecutor$1;->this$0:Lcom/bytedance/sdk/component/net/executor/DownloadExecutor;

    invoke-virtual {v0, v1, v15}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V

    goto :goto_1a

    :cond_17
    move-object v11, v1

    :goto_1a
    return-void
.end method
