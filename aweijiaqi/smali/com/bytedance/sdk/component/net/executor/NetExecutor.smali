.class public abstract Lcom/bytedance/sdk/component/net/executor/NetExecutor;
.super Ljava/lang/Object;
.source "NetExecutor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NetExecutor"


# instance fields
.field private extra:Ljava/lang/Object;

.field private extraMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

.field protected final requestHeadsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected tag:Ljava/lang/String;

.field protected url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/component/b/b/w;)V
    .locals 2

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    .line 26
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->requestHeadsMap:Ljava/util/Map;

    .line 27
    iput-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->url:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    .line 33
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->setTag(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public cancel()V
    .locals 5

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    if-nez v0, :cond_0

    goto :goto_2

    .line 74
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/w;->s()Lcom/bytedance/sdk/component/b/b/n;

    move-result-object v0

    .line 75
    monitor-enter v0

    .line 76
    :try_start_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/n;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/component/b/b/e;

    .line 77
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    invoke-interface {v2}, Lcom/bytedance/sdk/component/b/b/e;->a()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z;->e()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 78
    invoke-interface {v2}, Lcom/bytedance/sdk/component/b/b/e;->c()V

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/n;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/component/b/b/e;

    .line 82
    iget-object v3, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    invoke-interface {v2}, Lcom/bytedance/sdk/component/b/b/e;->a()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/z;->e()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 83
    invoke-interface {v2}, Lcom/bytedance/sdk/component/b/b/e;->c()V

    goto :goto_1

    .line 86
    :cond_4
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_5
    :goto_2
    return-void
.end method

.method public abstract enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
.end method

.method public abstract execute()Lcom/bytedance/sdk/component/net/NetResponse;
.end method

.method public getExtra()Ljava/lang/Object;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->extra:Ljava/lang/Object;

    return-object v0
.end method

.method public getExtraMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->extraMap:Ljava/util/Map;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public removeHeader(Ljava/lang/String;)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setExtra(Ljava/lang/Object;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->extra:Ljava/lang/Object;

    return-void
.end method

.method public setExtraMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 97
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->extraMap:Ljava/util/Map;

    return-void
.end method

.method public setHeaders(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 48
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 49
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->tag:Ljava/lang/String;

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->url:Ljava/lang/String;

    return-void
.end method
