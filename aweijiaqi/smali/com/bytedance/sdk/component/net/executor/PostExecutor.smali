.class public Lcom/bytedance/sdk/component/net/executor/PostExecutor;
.super Lcom/bytedance/sdk/component/net/executor/NetExecutor;
.source "PostExecutor.java"


# static fields
.field public static final CONTENT_TYPE_APPLICATION_JSON:Ljava/lang/String; = "application/json; charset=utf-8"

.field public static final CONTENT_TYPE_FORM_URLENCODED:Ljava/lang/String; = "application/x-www-form-urlencoded"

.field private static final TAG:Ljava/lang/String; = "PostExecutor"


# instance fields
.field requestBody:Lcom/bytedance/sdk/component/b/b/aa;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/component/b/b/w;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;-><init>(Lcom/bytedance/sdk/component/b/b/w;)V

    const/4 p1, 0x0

    .line 37
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    return-void
.end method


# virtual methods
.method public enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    .locals 4

    .line 78
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Url is Empty"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void

    .line 85
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    if-nez v1, :cond_2

    if-eqz p1, :cond_1

    .line 93
    new-instance v0, Ljava/io/IOException;

    const-string v1, "RequestBody is null, content type is not support!!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    :cond_1
    return-void

    .line 98
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 99
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 104
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    .line 105
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/aa;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/component/net/executor/PostExecutor$1;-><init>(Lcom/bytedance/sdk/component/net/executor/PostExecutor;Lcom/bytedance/sdk/component/net/callback/NetCallback;)V

    .line 108
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/b/b/e;->a(Lcom/bytedance/sdk/component/b/b/f;)V

    return-void

    .line 87
    :catch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Url is not a valid HTTP or HTTPS URL"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;->onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V

    return-void
.end method

.method public execute()Lcom/bytedance/sdk/component/net/NetResponse;
    .locals 14

    .line 138
    new-instance v0, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "PostExecutor"

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const-string v0, "execute: Url is Empty"

    .line 141
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 145
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 151
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    if-nez v1, :cond_1

    const-string v0, "RequestBody is null, content type is not support!!"

    .line 152
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestHeadsMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 157
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/Object;)Lcom/bytedance/sdk/component/b/b/z$a;

    .line 162
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    .line 163
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Lcom/bytedance/sdk/component/b/b/aa;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v0

    .line 166
    :try_start_1
    iget-object v1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->okHttpClient:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    .line 167
    invoke-interface {v0}, Lcom/bytedance/sdk/component/b/b/e;->b()Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 169
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 170
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v2, 0x0

    .line 172
    :goto_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 173
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 175
    :cond_3
    new-instance v1, Lcom/bytedance/sdk/component/net/NetResponse;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->d()Z

    move-result v5

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v6

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/b/b/ac;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->l()J

    move-result-wide v10

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->m()J

    move-result-wide v12

    move-object v4, v1

    invoke-direct/range {v4 .. v13}, Lcom/bytedance/sdk/component/net/NetResponse;-><init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 180
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_4
    return-object v3

    :catch_1
    const-string v0, "execute: Url is not a valid HTTP or HTTPS URL"

    .line 147
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/net/utils/NetLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method public setJson(Ljava/lang/String;)V
    .locals 1

    .line 45
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "{}"

    :cond_0
    const-string v0, "application/json; charset=utf-8"

    .line 48
    invoke-static {v0}, Lcom/bytedance/sdk/component/b/b/v;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/v;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/b/b/aa;->a(Lcom/bytedance/sdk/component/b/b/v;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/aa;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    return-void
.end method

.method public setJson(Lorg/json/JSONObject;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "{}"

    :goto_0
    const-string v0, "application/json; charset=utf-8"

    .line 56
    invoke-static {v0}, Lcom/bytedance/sdk/component/b/b/v;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/v;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/b/b/aa;->a(Lcom/bytedance/sdk/component/b/b/v;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/aa;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    return-void
.end method

.method public setParams(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/bytedance/sdk/component/b/b/q$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/q$a;-><init>()V

    if-eqz p1, :cond_0

    .line 62
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 64
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/component/b/b/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/q$a;

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/q$a;->a()Lcom/bytedance/sdk/component/b/b/q;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    return-void
.end method

.method public setRequestBody(Ljava/lang/String;[B)V
    .locals 0

    .line 71
    invoke-static {p1}, Lcom/bytedance/sdk/component/b/b/v;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/v;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/b/b/aa;->a(Lcom/bytedance/sdk/component/b/b/v;[B)Lcom/bytedance/sdk/component/b/b/aa;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->requestBody:Lcom/bytedance/sdk/component/b/b/aa;

    return-void
.end method
