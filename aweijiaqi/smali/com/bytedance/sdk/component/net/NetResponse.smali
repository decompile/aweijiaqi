.class public Lcom/bytedance/sdk/component/net/NetResponse;
.super Ljava/lang/Object;
.source "NetResponse.java"


# static fields
.field public static final CACHE_DISPATCH_FAIL_CODE:I = 0x25f

.field public static final CONNECT_FAIL_CODE:I = 0x259

.field public static final CONNECT_TIMEOUT_CODE:I = 0x25a

.field public static final DOWNLOAD_FILE_CANCEL_FAIL_CODE:I = 0x263

.field public static final DOWNLOAD_FILE_INVALID_FAIL_CODE:I = 0x262

.field public static final IMAGE_OOM_FAIL_CODE:I = 0x264

.field public static final NETWORK_DISPATCH_FAIL_CODE:I = 0x260

.field public static final NETWORK_FAIL_CODE:I = 0x25b

.field public static final PARSE_RESPONSE_CONTENT_FAIL_CODE:I = 0x25e

.field public static final PARSE_RESPONSE_FAIL_CODE:I = 0x25d

.field public static final RENAME_DOWNLOAD_FILE_FAIL_CODE:I = 0x261

.field public static final UNSUPPORT_ENCODE_FAIL_CODE:I = 0x25c


# instance fields
.field final body:Ljava/lang/String;

.field final code:I

.field private file:Ljava/io/File;

.field final headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isSuccess:Z

.field final message:Ljava/lang/String;

.field final receivedResponseAtMillis:J

.field final sentRequestAtMillis:J


# direct methods
.method public constructor <init>(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "JJ)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->file:Ljava/io/File;

    .line 49
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess:Z

    .line 50
    iput p2, p0, Lcom/bytedance/sdk/component/net/NetResponse;->code:I

    .line 51
    iput-object p3, p0, Lcom/bytedance/sdk/component/net/NetResponse;->message:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/bytedance/sdk/component/net/NetResponse;->headers:Ljava/util/Map;

    .line 53
    iput-object p5, p0, Lcom/bytedance/sdk/component/net/NetResponse;->body:Ljava/lang/String;

    .line 54
    iput-wide p6, p0, Lcom/bytedance/sdk/component/net/NetResponse;->sentRequestAtMillis:J

    .line 55
    iput-wide p8, p0, Lcom/bytedance/sdk/component/net/NetResponse;->receivedResponseAtMillis:J

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getCode()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->code:I

    return v0
.end method

.method public getDuration()J
    .locals 4

    .line 95
    iget-wide v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->sentRequestAtMillis:J

    iget-wide v2, p0, Lcom/bytedance/sdk/component/net/NetResponse;->receivedResponseAtMillis:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->file:Ljava/io/File;

    return-object v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->headers:Ljava/util/Map;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getReceivedResponseAtMillis()J
    .locals 2

    .line 79
    iget-wide v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->receivedResponseAtMillis:J

    return-wide v0
.end method

.method public getSentRequestAtMillis()J
    .locals 2

    .line 75
    iget-wide v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->sentRequestAtMillis:J

    return-wide v0
.end method

.method public isSuccess()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess:Z

    return v0
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/bytedance/sdk/component/net/NetResponse;->file:Ljava/io/File;

    return-void
.end method
