.class public final enum Lcom/bytedance/sdk/component/adnet/d/d$a;
.super Ljava/lang/Enum;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/component/adnet/d/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/bytedance/sdk/component/adnet/d/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/bytedance/sdk/component/adnet/d/d$a;

.field public static final enum b:Lcom/bytedance/sdk/component/adnet/d/d$a;

.field public static final enum c:Lcom/bytedance/sdk/component/adnet/d/d$a;

.field public static final enum d:Lcom/bytedance/sdk/component/adnet/d/d$a;

.field private static final synthetic e:[Lcom/bytedance/sdk/component/adnet/d/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 13
    new-instance v0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    const/4 v1, 0x0

    const-string v2, "DEBUG"

    invoke-direct {v0, v2, v1}, Lcom/bytedance/sdk/component/adnet/d/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->a:Lcom/bytedance/sdk/component/adnet/d/d$a;

    .line 14
    new-instance v0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    const/4 v2, 0x1

    const-string v3, "INFO"

    invoke-direct {v0, v3, v2}, Lcom/bytedance/sdk/component/adnet/d/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->b:Lcom/bytedance/sdk/component/adnet/d/d$a;

    .line 15
    new-instance v0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3}, Lcom/bytedance/sdk/component/adnet/d/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->c:Lcom/bytedance/sdk/component/adnet/d/d$a;

    .line 16
    new-instance v0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    const/4 v4, 0x3

    const-string v5, "OFF"

    invoke-direct {v0, v5, v4}, Lcom/bytedance/sdk/component/adnet/d/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->d:Lcom/bytedance/sdk/component/adnet/d/d$a;

    const/4 v5, 0x4

    new-array v5, v5, [Lcom/bytedance/sdk/component/adnet/d/d$a;

    .line 12
    sget-object v6, Lcom/bytedance/sdk/component/adnet/d/d$a;->a:Lcom/bytedance/sdk/component/adnet/d/d$a;

    aput-object v6, v5, v1

    sget-object v1, Lcom/bytedance/sdk/component/adnet/d/d$a;->b:Lcom/bytedance/sdk/component/adnet/d/d$a;

    aput-object v1, v5, v2

    sget-object v1, Lcom/bytedance/sdk/component/adnet/d/d$a;->c:Lcom/bytedance/sdk/component/adnet/d/d$a;

    aput-object v1, v5, v3

    aput-object v0, v5, v4

    sput-object v5, Lcom/bytedance/sdk/component/adnet/d/d$a;->e:[Lcom/bytedance/sdk/component/adnet/d/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/bytedance/sdk/component/adnet/d/d$a;
    .locals 1

    .line 12
    const-class v0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/bytedance/sdk/component/adnet/d/d$a;

    return-object p0
.end method

.method public static values()[Lcom/bytedance/sdk/component/adnet/d/d$a;
    .locals 1

    .line 12
    sget-object v0, Lcom/bytedance/sdk/component/adnet/d/d$a;->e:[Lcom/bytedance/sdk/component/adnet/d/d$a;

    invoke-virtual {v0}, [Lcom/bytedance/sdk/component/adnet/d/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/bytedance/sdk/component/adnet/d/d$a;

    return-object v0
.end method
