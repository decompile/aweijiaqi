.class public interface abstract Lcom/bytedance/sdk/component/adnet/face/IHttpStack;
.super Ljava/lang/Object;
.source "IHttpStack.java"


# virtual methods
.method public abstract performRequest(Lcom/bytedance/sdk/component/adnet/core/Request;Ljava/util/Map;)Lcom/bytedance/sdk/component/adnet/core/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/adnet/core/Request<",
            "*>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/bytedance/sdk/component/adnet/core/HttpResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation
.end method
