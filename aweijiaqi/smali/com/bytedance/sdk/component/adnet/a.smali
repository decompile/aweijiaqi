.class public Lcom/bytedance/sdk/component/adnet/a;
.super Ljava/lang/Object;
.source "AdNetSdk.java"


# static fields
.field public static a:Lcom/bytedance/sdk/component/adnet/core/n; = null

.field private static b:Ljava/lang/String; = null

.field private static c:Lcom/bytedance/sdk/component/adnet/c/b; = null

.field private static d:Z = true

.field private static e:Lcom/bytedance/sdk/component/adnet/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static a()Lcom/bytedance/sdk/component/adnet/c/b;
    .locals 2

    .line 97
    sget-object v0, Lcom/bytedance/sdk/component/adnet/a;->c:Lcom/bytedance/sdk/component/adnet/c/b;

    if-eqz v0, :cond_0

    return-object v0

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sITTNetDepend is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/component/adnet/core/l;
    .locals 0

    .line 58
    invoke-static {p0}, Lcom/bytedance/sdk/component/adnet/core/k;->a(Landroid/content/Context;)Lcom/bytedance/sdk/component/adnet/core/l;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 189
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 190
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getThreadPoolExecutor()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p0

    new-instance v1, Lcom/bytedance/sdk/component/adnet/a$1;

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/component/adnet/a$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/component/adnet/c/a;)V
    .locals 0

    .line 116
    sput-object p0, Lcom/bytedance/sdk/component/adnet/a;->e:Lcom/bytedance/sdk/component/adnet/c/a;

    return-void
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 71
    :try_start_0
    sget-object v0, Lcom/bytedance/sdk/component/adnet/a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    const-string v1, "VAdNetSdk"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 74
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/bytedance/sdk/component/adnet/a;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "init adnetsdk default directory error "

    .line 77
    invoke-static {p0, v1, v0}, Lcom/bytedance/sdk/component/adnet/core/o;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    :cond_0
    :goto_0
    sget-object p0, Lcom/bytedance/sdk/component/adnet/a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static b()Z
    .locals 1

    .line 108
    sget-boolean v0, Lcom/bytedance/sdk/component/adnet/a;->d:Z

    return v0
.end method

.method public static c()Lcom/bytedance/sdk/component/adnet/c/a;
    .locals 1

    .line 120
    sget-object v0, Lcom/bytedance/sdk/component/adnet/a;->e:Lcom/bytedance/sdk/component/adnet/c/a;

    return-object v0
.end method
