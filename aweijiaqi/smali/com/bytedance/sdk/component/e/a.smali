.class Lcom/bytedance/sdk/component/e/a;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "ADThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/e/a$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/concurrent/ThreadFactory;",
            "Ljava/util/concurrent/RejectedExecutionHandler;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-wide v3, p4

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 45
    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    move-object v1, p1

    .line 46
    iput-object v1, v0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 11

    .line 109
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 110
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->e()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 111
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 114
    iget-object p2, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, 0x1a344

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_1

    const v2, 0x2daeb0

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "aidl"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "log"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    const/4 p2, 0x5

    const-string v1, "  maxSize="

    const/4 v2, 0x3

    const-string v5, " coreSize="

    const-string v6, "afterExecute: reduce "

    const/4 v7, 0x6

    const-string v8, "ADThreadPoolExecutor"

    const/4 v9, 0x2

    const/4 v10, 0x4

    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_3

    goto :goto_1

    .line 141
    :cond_3
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v10, :cond_5

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v9, :cond_5

    .line 142
    invoke-virtual {p0, v9}, Lcom/bytedance/sdk/component/e/a;->setCorePoolSize(I)V

    .line 143
    invoke-virtual {p0, v10}, Lcom/bytedance/sdk/component/e/a;->setMaximumPoolSize(I)V

    new-array p1, v7, [Ljava/lang/Object;

    aput-object v6, p1, v3

    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    aput-object v0, p1, v4

    aput-object v5, p1, v9

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    aput-object v1, p1, v10

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getMaximumPoolSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, p2

    invoke-static {v8, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 129
    :cond_4
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-ge p1, v10, :cond_5

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result p1

    if-eqz p1, :cond_5

    .line 131
    :try_start_0
    invoke-virtual {p0, v3}, Lcom/bytedance/sdk/component/e/a;->setCorePoolSize(I)V

    .line 132
    invoke-virtual {p0, v10}, Lcom/bytedance/sdk/component/e/a;->setMaximumPoolSize(I)V

    new-array p1, v7, [Ljava/lang/Object;

    aput-object v6, p1, v3

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    aput-object v0, p1, v4

    aput-object v5, p1, v9

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v2

    aput-object v1, p1, v10

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getMaximumPoolSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, p2

    invoke-static {v8, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 135
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v8, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 12

    .line 51
    instance-of v0, p1, Lcom/bytedance/sdk/component/e/g;

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Lcom/bytedance/sdk/component/e/b;

    check-cast p1, Lcom/bytedance/sdk/component/e/g;

    invoke-direct {v0, p1, p0}, Lcom/bytedance/sdk/component/e/b;-><init>(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/component/e/a;)V

    invoke-super {p0, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 54
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/component/e/a$1;

    const-string v1, "unknown"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/component/e/a$1;-><init>(Lcom/bytedance/sdk/component/e/a;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 60
    new-instance p1, Lcom/bytedance/sdk/component/e/b;

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/component/e/b;-><init>(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/component/e/a;)V

    invoke-super {p0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 63
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->e()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 64
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 67
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x1a344

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v2, v3, :cond_2

    const v3, 0x2daeb0

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    const-string v2, "aidl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v2, "log"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    :goto_1
    const/4 v0, 0x5

    const-string v2, "  maxSize="

    const/4 v3, 0x3

    const-string v6, " coreSize="

    const/4 v7, 0x2

    const-string v8, "execute: increase poolType =  "

    const/4 v9, 0x6

    const-string v10, "ADThreadPoolExecutor"

    const/4 v11, 0x4

    if-eqz v1, :cond_5

    if-eq v1, v5, :cond_4

    goto/16 :goto_2

    .line 94
    :cond_4
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v11, :cond_6

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v11, :cond_6

    .line 95
    sget p1, Lcom/bytedance/sdk/component/e/e;->a:I

    add-int/2addr p1, v11

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/component/e/a;->setMaximumPoolSize(I)V

    .line 96
    invoke-virtual {p0, v11}, Lcom/bytedance/sdk/component/e/a;->setCorePoolSize(I)V

    new-array p1, v9, [Ljava/lang/Object;

    aput-object v8, p1, v4

    .line 97
    iget-object v1, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    aput-object v1, p1, v5

    aput-object v6, p1, v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v3

    aput-object v2, p1, v11

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getMaximumPoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-static {v10, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 82
    :cond_5
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v11, :cond_6

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v11, :cond_6

    .line 84
    :try_start_0
    sget p1, Lcom/bytedance/sdk/component/e/e;->a:I

    add-int/2addr p1, v11

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/component/e/a;->setMaximumPoolSize(I)V

    .line 85
    invoke-virtual {p0, v11}, Lcom/bytedance/sdk/component/e/a;->setCorePoolSize(I)V

    new-array p1, v9, [Ljava/lang/Object;

    aput-object v8, p1, v4

    .line 86
    iget-object v1, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    aput-object v1, p1, v5

    aput-object v6, p1, v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getCorePoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v3

    aput-object v2, p1, v11

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/e/a;->getMaximumPoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-static {v10, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 88
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v10, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public shutdown()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const-string v1, "io"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const-string v1, "aidl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 168
    :cond_0
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    :cond_1
    :goto_0
    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const-string v1, "io"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/component/e/a;->a:Ljava/lang/String;

    const-string v1, "aidl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 158
    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
