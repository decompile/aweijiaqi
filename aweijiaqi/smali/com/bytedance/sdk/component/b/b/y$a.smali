.class final Lcom/bytedance/sdk/component/b/b/y$a;
.super Lcom/bytedance/sdk/component/b/b/a/b;
.source "RealCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/component/b/b/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/component/b/b/y;

.field private final c:Lcom/bytedance/sdk/component/b/b/f;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/b/b/y;Lcom/bytedance/sdk/component/b/b/f;)V
    .locals 2

    .line 136
    iput-object p1, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 137
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/y;->g()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "OkHttp %s"

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/component/b/b/a/b;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    iput-object p2, p0, Lcom/bytedance/sdk/component/b/b/y$a;->c:Lcom/bytedance/sdk/component/b/b/f;

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    iget-object v0, v0, Lcom/bytedance/sdk/component/b/b/y;->c:Lcom/bytedance/sdk/component/b/b/z;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/z;->a()Lcom/bytedance/sdk/component/b/b/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/t;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/bytedance/sdk/component/b/b/y;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    return-object v0
.end method

.method protected c()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/b/b/y;->h()Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v2

    .line 158
    iget-object v3, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    iget-object v3, v3, Lcom/bytedance/sdk/component/b/b/y;->b:Lcom/bytedance/sdk/component/b/b/a/c/j;

    invoke-virtual {v3}, Lcom/bytedance/sdk/component/b/b/a/c/j;->b()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 160
    :try_start_1
    iget-object v1, p0, Lcom/bytedance/sdk/component/b/b/y$a;->c:Lcom/bytedance/sdk/component/b/b/f;

    iget-object v2, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Canceled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2, v3}, Lcom/bytedance/sdk/component/b/b/f;->onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V

    goto :goto_0

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/component/b/b/y$a;->c:Lcom/bytedance/sdk/component/b/b/f;

    iget-object v3, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-interface {v1, v3, v2}, Lcom/bytedance/sdk/component/b/b/f;->onResponse(Lcom/bytedance/sdk/component/b/b/e;Lcom/bytedance/sdk/component/b/b/ab;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    iget-object v0, v0, Lcom/bytedance/sdk/component/b/b/y;->a:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/w;->s()Lcom/bytedance/sdk/component/b/b/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/component/b/b/n;->b(Lcom/bytedance/sdk/component/b/b/y$a;)V

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    .line 168
    :try_start_2
    invoke-static {}, Lcom/bytedance/sdk/component/b/b/a/g/e;->b()Lcom/bytedance/sdk/component/b/b/a/g/e;

    move-result-object v0

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Callback failure for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/y;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/bytedance/sdk/component/b/b/a/g/e;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-static {v0}, Lcom/bytedance/sdk/component/b/b/y;->a(Lcom/bytedance/sdk/component/b/b/y;)Lcom/bytedance/sdk/component/b/b/p;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/b/b/p;->a(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V

    .line 171
    iget-object v0, p0, Lcom/bytedance/sdk/component/b/b/y$a;->c:Lcom/bytedance/sdk/component/b/b/f;

    iget-object v2, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    invoke-interface {v0, v2, v1}, Lcom/bytedance/sdk/component/b/b/f;->onFailure(Lcom/bytedance/sdk/component/b/b/e;Ljava/io/IOException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :goto_2
    return-void

    .line 174
    :goto_3
    iget-object v1, p0, Lcom/bytedance/sdk/component/b/b/y$a;->a:Lcom/bytedance/sdk/component/b/b/y;

    iget-object v1, v1, Lcom/bytedance/sdk/component/b/b/y;->a:Lcom/bytedance/sdk/component/b/b/w;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/w;->s()Lcom/bytedance/sdk/component/b/b/n;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/bytedance/sdk/component/b/b/n;->b(Lcom/bytedance/sdk/component/b/b/y$a;)V

    .line 175
    throw v0
.end method
