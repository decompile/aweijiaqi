.class public Lcom/bytedance/sdk/component/d/m;
.super Ljava/lang/Object;
.source "ImageResponse.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/d/c/a;Ljava/lang/Object;)Lcom/bytedance/sdk/component/d/m;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/d/c/a;",
            "TT;)",
            "Lcom/bytedance/sdk/component/d/m;"
        }
    .end annotation

    .line 34
    iput-object p2, p0, Lcom/bytedance/sdk/component/d/m;->c:Ljava/lang/Object;

    .line 35
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->e()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/sdk/component/d/m;->a:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->a()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/sdk/component/d/m;->b:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->h()I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/component/d/m;->d:I

    .line 38
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->i()I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/component/d/m;->e:I

    .line 39
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->l()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/component/d/m;->h:Z

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;Ljava/lang/Object;Ljava/util/Map;Z)Lcom/bytedance/sdk/component/d/m;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/d/c/a;",
            "TT;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/bytedance/sdk/component/d/m;"
        }
    .end annotation

    .line 44
    iput-object p3, p0, Lcom/bytedance/sdk/component/d/m;->f:Ljava/util/Map;

    .line 45
    iput-boolean p4, p0, Lcom/bytedance/sdk/component/d/m;->g:Z

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/component/d/m;->a(Lcom/bytedance/sdk/component/d/c/a;Ljava/lang/Object;)Lcom/bytedance/sdk/component/d/m;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/m;->c:Ljava/lang/Object;

    return-object v0
.end method
