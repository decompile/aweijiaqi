.class public Lcom/bytedance/sdk/component/d/o;
.super Ljava/lang/Object;
.source "LoadConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/d/o$a;
    }
.end annotation


# instance fields
.field private a:Lcom/bytedance/sdk/component/d/f;

.field private b:Ljava/util/concurrent/ExecutorService;

.field private c:Lcom/bytedance/sdk/component/d/c;

.field private d:Lcom/bytedance/sdk/component/d/i;

.field private e:Lcom/bytedance/sdk/component/d/j;

.field private f:Lcom/bytedance/sdk/component/d/b;

.field private g:Lcom/bytedance/sdk/component/d/h;

.field private h:Lcom/bytedance/sdk/component/d/a;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/component/d/o$a;)V
    .locals 1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->a(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->a:Lcom/bytedance/sdk/component/d/f;

    .line 58
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->b(Lcom/bytedance/sdk/component/d/o$a;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->b:Ljava/util/concurrent/ExecutorService;

    .line 59
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->c(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->c:Lcom/bytedance/sdk/component/d/c;

    .line 60
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->d(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->d:Lcom/bytedance/sdk/component/d/i;

    .line 61
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->e(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->e:Lcom/bytedance/sdk/component/d/j;

    .line 62
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->f(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->f:Lcom/bytedance/sdk/component/d/b;

    .line 63
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->g(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/o;->h:Lcom/bytedance/sdk/component/d/a;

    .line 64
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/o$a;->h(Lcom/bytedance/sdk/component/d/o$a;)Lcom/bytedance/sdk/component/d/h;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/d/o;->g:Lcom/bytedance/sdk/component/d/h;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/component/d/o$a;Lcom/bytedance/sdk/component/d/o$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/d/o;-><init>(Lcom/bytedance/sdk/component/d/o$a;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/component/d/o;
    .locals 0

    .line 100
    new-instance p0, Lcom/bytedance/sdk/component/d/o$a;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/o$a;-><init>()V

    .line 101
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/d/o$a;->a()Lcom/bytedance/sdk/component/d/o;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()Lcom/bytedance/sdk/component/d/f;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->a:Lcom/bytedance/sdk/component/d/f;

    return-object v0
.end method

.method public b()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->b:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public c()Lcom/bytedance/sdk/component/d/c;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->c:Lcom/bytedance/sdk/component/d/c;

    return-object v0
.end method

.method public d()Lcom/bytedance/sdk/component/d/i;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->d:Lcom/bytedance/sdk/component/d/i;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/component/d/j;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->e:Lcom/bytedance/sdk/component/d/j;

    return-object v0
.end method

.method public f()Lcom/bytedance/sdk/component/d/b;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->f:Lcom/bytedance/sdk/component/d/b;

    return-object v0
.end method

.method public g()Lcom/bytedance/sdk/component/d/h;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->g:Lcom/bytedance/sdk/component/d/h;

    return-object v0
.end method

.method public h()Lcom/bytedance/sdk/component/d/a;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/o;->h:Lcom/bytedance/sdk/component/d/a;

    return-object v0
.end method
