.class public Lcom/bytedance/sdk/component/d/d/j;
.super Lcom/bytedance/sdk/component/d/d/a;
.source "NetVisitor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/Throwable;Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 1

    .line 88
    new-instance v0, Lcom/bytedance/sdk/component/d/d/g;

    invoke-direct {v0, p1, p2, p3}, Lcom/bytedance/sdk/component/d/d/g;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p4, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    return-void
.end method

.method public static a([B)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    .line 83
    array-length v2, p0

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    aget-byte v2, p0, v1

    const/16 v3, 0x47

    if-ne v2, v3, :cond_0

    aget-byte v2, p0, v0

    const/16 v3, 0x49

    if-ne v2, v3, :cond_0

    const/4 v2, 0x2

    aget-byte p0, p0, v2

    const/16 v2, 0x46

    if-ne p0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "net_request"

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 8

    .line 22
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v4

    .line 23
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/d/c/b;->e()Lcom/bytedance/sdk/component/d/c;

    move-result-object v0

    const/4 v1, 0x0

    .line 25
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/component/d/c/a;->a(Z)V

    .line 28
    :try_start_0
    new-instance v1, Lcom/bytedance/sdk/component/d/b/c;

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->k()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/bytedance/sdk/component/d/b/c;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/d/c;->a(Lcom/bytedance/sdk/component/d/b/c;)Lcom/bytedance/sdk/component/d/b/d;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->a()I

    move-result v1

    .line 32
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->a()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_2

    .line 34
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->b()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, [B

    .line 36
    invoke-static {v5}, Lcom/bytedance/sdk/component/d/d/j;->a([B)Z

    move-result v3

    .line 38
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->j()Lcom/bytedance/sdk/component/d/p;

    move-result-object v1

    sget-object v2, Lcom/bytedance/sdk/component/d/p;->b:Lcom/bytedance/sdk/component/d/p;

    if-eq v1, v2, :cond_1

    if-eqz v3, :cond_0

    goto :goto_0

    .line 42
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/component/d/d/d;

    invoke-direct {v1, v5, v0}, Lcom/bytedance/sdk/component/d/d/d;-><init>([BLcom/bytedance/sdk/component/d/b/d;)V

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    goto :goto_1

    .line 40
    :cond_1
    :goto_0
    new-instance v1, Lcom/bytedance/sdk/component/d/d/l;

    invoke-direct {v1, v5, v0, v3}, Lcom/bytedance/sdk/component/d/d/l;-><init>(Ljava/lang/Object;Lcom/bytedance/sdk/component/d/b/d;Z)V

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    .line 46
    :goto_1
    invoke-virtual {v4}, Lcom/bytedance/sdk/component/d/c/b;->g()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    new-instance v7, Lcom/bytedance/sdk/component/d/d/j$1;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/component/d/d/j$1;-><init>(Lcom/bytedance/sdk/component/d/d/j;Lcom/bytedance/sdk/component/d/c/a;ZLcom/bytedance/sdk/component/d/c/b;[B)V

    invoke-interface {v6, v7}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_2

    .line 63
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/component/d/c/c;->a(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 67
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->b()Ljava/lang/Object;

    move-result-object v3

    .line 68
    instance-of v4, v3, Ljava/lang/Throwable;

    if-eqz v4, :cond_3

    .line 69
    move-object v2, v3

    check-cast v2, Ljava/lang/Throwable;

    .line 72
    :cond_3
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2, p1}, Lcom/bytedance/sdk/component/d/d/j;->a(ILjava/lang/String;Ljava/lang/Throwable;Lcom/bytedance/sdk/component/d/c/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    const/16 v1, 0x3ec

    const-string v2, "net request failed!"

    .line 76
    invoke-direct {p0, v1, v2, v0, p1}, Lcom/bytedance/sdk/component/d/d/j;->a(ILjava/lang/String;Ljava/lang/Throwable;Lcom/bytedance/sdk/component/d/c/a;)V

    :goto_2
    return-void
.end method
