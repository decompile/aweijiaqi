.class public Lcom/bytedance/sdk/component/d/d/k;
.super Lcom/bytedance/sdk/component/d/d/a;
.source "RawCacheVisitor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "raw_cache"

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 4

    .line 17
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->j()Lcom/bytedance/sdk/component/d/p;

    move-result-object v0

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/d/c/b;->c()Lcom/bytedance/sdk/component/d/j;

    move-result-object v1

    .line 21
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/component/d/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-nez v1, :cond_0

    .line 24
    new-instance v0, Lcom/bytedance/sdk/component/d/d/e;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/d/e;-><init>()V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    return-void

    .line 28
    :cond_0
    invoke-static {v1}, Lcom/bytedance/sdk/component/d/d/j;->a([B)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 30
    new-instance v0, Lcom/bytedance/sdk/component/d/d/l;

    invoke-direct {v0, v1, v3}, Lcom/bytedance/sdk/component/d/d/l;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    goto :goto_0

    .line 31
    :cond_1
    sget-object v2, Lcom/bytedance/sdk/component/d/p;->a:Lcom/bytedance/sdk/component/d/p;

    if-ne v0, v2, :cond_2

    .line 33
    new-instance v0, Lcom/bytedance/sdk/component/d/d/d;

    invoke-direct {v0, v1, v3}, Lcom/bytedance/sdk/component/d/d/d;-><init>([BZ)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    goto :goto_0

    .line 36
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/component/d/d/l;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/component/d/d/l;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    :goto_0
    return-void
.end method
