.class public Lcom/bytedance/sdk/component/d/d/l;
.super Lcom/bytedance/sdk/component/d/d/a;
.source "SuccessVisitor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/bytedance/sdk/component/d/d/a;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private b:Lcom/bytedance/sdk/component/d/b/d;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/d/l;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/bytedance/sdk/component/d/b/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/bytedance/sdk/component/d/b/d;",
            ")V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/d/l;->a:Ljava/lang/Object;

    .line 31
    iput-object p2, p0, Lcom/bytedance/sdk/component/d/d/l;->b:Lcom/bytedance/sdk/component/d/b/d;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/bytedance/sdk/component/d/b/d;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/bytedance/sdk/component/d/b/d;",
            "Z)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/d/l;->a:Ljava/lang/Object;

    .line 46
    iput-object p2, p0, Lcom/bytedance/sdk/component/d/d/l;->b:Lcom/bytedance/sdk/component/d/b/d;

    .line 47
    iput-boolean p3, p0, Lcom/bytedance/sdk/component/d/d/l;->c:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/d/l;->a:Ljava/lang/Object;

    .line 36
    iput-boolean p2, p0, Lcom/bytedance/sdk/component/d/d/l;->c:Z

    return-void
.end method

.method private b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/d/l;->b:Lcom/bytedance/sdk/component/d/b/d;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/b/d;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private b(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 5

    .line 84
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->c()Lcom/bytedance/sdk/component/d/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    new-instance v1, Lcom/bytedance/sdk/component/d/m;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/d/m;-><init>()V

    iget-object v2, p0, Lcom/bytedance/sdk/component/d/d/l;->a:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/l;->b()Ljava/util/Map;

    move-result-object v3

    iget-boolean v4, p0, Lcom/bytedance/sdk/component/d/d/l;->c:Z

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/bytedance/sdk/component/d/m;->a(Lcom/bytedance/sdk/component/d/c/a;Ljava/lang/Object;Ljava/util/Map;Z)Lcom/bytedance/sdk/component/d/m;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/component/d/g;->a(Lcom/bytedance/sdk/component/d/m;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "success"

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 4

    .line 52
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->e()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/d/c/b;->h()Ljava/util/Map;

    move-result-object v1

    .line 56
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_0

    .line 59
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/d/d/l;->b(Lcom/bytedance/sdk/component/d/c/a;)V

    goto :goto_1

    .line 62
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/component/d/c/a;

    .line 63
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/component/d/d/l;->b(Lcom/bytedance/sdk/component/d/c/a;)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 67
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void
.end method
