.class public Lcom/bytedance/sdk/component/d/d/f;
.super Lcom/bytedance/sdk/component/d/d/a;
.source "DuplicateCheckVisitor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "check_duplicate"

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 3

    .line 20
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->e()Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/d/c/b;->h()Ljava/util/Map;

    move-result-object v1

    .line 24
    monitor-enter v1

    .line 25
    :try_start_0
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_0

    .line 28
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 29
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    goto :goto_0

    .line 38
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/component/d/d/c;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/d/c;-><init>()V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    .line 41
    :goto_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
