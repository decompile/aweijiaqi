.class public Lcom/bytedance/sdk/component/d/d/e;
.super Lcom/bytedance/sdk/component/d/d/a;
.source "DiskCacheVisitor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/d/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "disk_cache"

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 5

    .line 18
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->d()Ljava/lang/String;

    move-result-object v0

    .line 20
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/d/c/b;->d()Lcom/bytedance/sdk/component/d/b;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 24
    invoke-interface {v1, v0}, Lcom/bytedance/sdk/component/d/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-eqz v1, :cond_2

    .line 28
    invoke-static {v1}, Lcom/bytedance/sdk/component/d/d/j;->a([B)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 29
    new-instance v2, Lcom/bytedance/sdk/component/d/d/l;

    invoke-direct {v2, v1, v3}, Lcom/bytedance/sdk/component/d/d/l;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    .line 30
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/b;->c()Lcom/bytedance/sdk/component/d/j;

    move-result-object p1

    invoke-interface {p1, v0, v1}, Lcom/bytedance/sdk/component/d/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void

    .line 34
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->j()Lcom/bytedance/sdk/component/d/p;

    move-result-object v2

    sget-object v4, Lcom/bytedance/sdk/component/d/p;->a:Lcom/bytedance/sdk/component/d/p;

    if-eq v2, v4, :cond_1

    .line 36
    new-instance v2, Lcom/bytedance/sdk/component/d/d/l;

    invoke-direct {v2, v1}, Lcom/bytedance/sdk/component/d/d/l;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    .line 37
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/b;->c()Lcom/bytedance/sdk/component/d/j;

    move-result-object p1

    invoke-interface {p1, v0, v1}, Lcom/bytedance/sdk/component/d/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void

    .line 42
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/component/d/d/d;

    invoke-direct {v0, v1, v3}, Lcom/bytedance/sdk/component/d/d/d;-><init>([BZ)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    goto :goto_0

    .line 46
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/component/d/d/j;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/d/j;-><init>()V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    goto :goto_0

    .line 50
    :cond_3
    new-instance v0, Lcom/bytedance/sdk/component/d/d/j;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/d/j;-><init>()V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/d/h;)Z

    :goto_0
    return-void
.end method
