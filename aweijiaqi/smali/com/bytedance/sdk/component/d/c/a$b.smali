.class public Lcom/bytedance/sdk/component/d/c/a$b;
.super Ljava/lang/Object;
.source "ImageRequest.java"

# interfaces
.implements Lcom/bytedance/sdk/component/d/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/component/d/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/bytedance/sdk/component/d/g;

.field private b:Landroid/widget/ImageView;

.field private c:Lcom/bytedance/sdk/component/d/c/a/b;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/widget/ImageView$ScaleType;

.field private g:Landroid/graphics/Bitmap$Config;

.field private h:I

.field private i:I

.field private j:Lcom/bytedance/sdk/component/d/p;

.field private k:Lcom/bytedance/sdk/component/d/n;

.field private l:Lcom/bytedance/sdk/component/d/k;

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/g;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->a:Lcom/bytedance/sdk/component/d/g;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/widget/ImageView;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->b:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/c/a/b;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->c:Lcom/bytedance/sdk/component/d/c/a/b;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/widget/ImageView$ScaleType;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->f:Landroid/widget/ImageView$ScaleType;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/graphics/Bitmap$Config;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->g:Landroid/graphics/Bitmap$Config;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/component/d/c/a$b;)I
    .locals 0

    .line 277
    iget p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->h:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/component/d/c/a$b;)I
    .locals 0

    .line 277
    iget p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->i:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/p;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->j:Lcom/bytedance/sdk/component/d/p;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/n;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->k:Lcom/bytedance/sdk/component/d/n;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/k;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->l:Lcom/bytedance/sdk/component/d/k;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;
    .locals 0

    .line 277
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/sdk/component/d/c/a$b;)Z
    .locals 0

    .line 277
    iget-boolean p0, p0, Lcom/bytedance/sdk/component/d/c/a$b;->m:Z

    return p0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;)Lcom/bytedance/sdk/component/d/d;
    .locals 1

    .line 372
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->b:Landroid/widget/ImageView;

    .line 373
    new-instance p1, Lcom/bytedance/sdk/component/d/c/a;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/bytedance/sdk/component/d/c/a;-><init>(Lcom/bytedance/sdk/component/d/c/a$b;Lcom/bytedance/sdk/component/d/c/a$1;)V

    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a;->d(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/d;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/bytedance/sdk/component/d/g;)Lcom/bytedance/sdk/component/d/d;
    .locals 1

    .line 366
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->a:Lcom/bytedance/sdk/component/d/g;

    .line 367
    new-instance p1, Lcom/bytedance/sdk/component/d/c/a;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/bytedance/sdk/component/d/c/a;-><init>(Lcom/bytedance/sdk/component/d/c/a$b;Lcom/bytedance/sdk/component/d/c/a$1;)V

    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a;->d(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/d;

    move-result-object p1

    return-object p1
.end method

.method public a(I)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 318
    iput p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->h:I

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/component/d/k;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->l:Lcom/bytedance/sdk/component/d/k;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->d:Ljava/lang/String;

    return-object p0
.end method

.method public b(I)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 324
    iput p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->i:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$b;->e:Ljava/lang/String;

    return-object p0
.end method
