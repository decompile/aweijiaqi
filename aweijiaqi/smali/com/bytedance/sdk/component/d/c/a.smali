.class public Lcom/bytedance/sdk/component/d/c/a;
.super Ljava/lang/Object;
.source "ImageRequest.java"

# interfaces
.implements Lcom/bytedance/sdk/component/d/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/component/d/c/a$a;,
        Lcom/bytedance/sdk/component/d/c/a$b;
    }
.end annotation


# instance fields
.field a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Lcom/bytedance/sdk/component/d/c/a/b;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/bytedance/sdk/component/d/g;

.field private g:Landroid/widget/ImageView$ScaleType;

.field private h:Landroid/graphics/Bitmap$Config;

.field private i:I

.field private j:I

.field private k:Lcom/bytedance/sdk/component/d/p;

.field private l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private volatile m:Z

.field private n:Z

.field private o:Lcom/bytedance/sdk/component/d/k;

.field private p:Lcom/bytedance/sdk/component/d/n;

.field private q:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/bytedance/sdk/component/d/d/h;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Landroid/os/Handler;

.field private s:Z


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/component/d/c/a$b;)V
    .locals 2

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->q:Ljava/util/Queue;

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->r:Landroid/os/Handler;

    const/4 v0, 0x1

    .line 92
    iput-boolean v0, p0, Lcom/bytedance/sdk/component/d/c/a;->s:Z

    .line 95
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->a(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->b:Ljava/lang/String;

    .line 96
    new-instance v0, Lcom/bytedance/sdk/component/d/c/a$a;

    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->b(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/g;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/component/d/c/a$a;-><init>(Lcom/bytedance/sdk/component/d/c/a;Lcom/bytedance/sdk/component/d/g;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->f:Lcom/bytedance/sdk/component/d/g;

    .line 97
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->c(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->l:Ljava/lang/ref/WeakReference;

    .line 99
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->d(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/c/a/b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/a/b;->a()Lcom/bytedance/sdk/component/d/c/a/b;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->d(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/c/a/b;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->c:Lcom/bytedance/sdk/component/d/c/a/b;

    .line 103
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->e(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->g:Landroid/widget/ImageView$ScaleType;

    .line 104
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->f(Lcom/bytedance/sdk/component/d/c/a$b;)Landroid/graphics/Bitmap$Config;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->h:Landroid/graphics/Bitmap$Config;

    .line 105
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->g(Lcom/bytedance/sdk/component/d/c/a$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/component/d/c/a;->i:I

    .line 106
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->h(Lcom/bytedance/sdk/component/d/c/a$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/component/d/c/a;->j:I

    .line 108
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->i(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/p;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/bytedance/sdk/component/d/p;->a:Lcom/bytedance/sdk/component/d/p;

    goto :goto_1

    :cond_1
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->i(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/p;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->k:Lcom/bytedance/sdk/component/d/p;

    .line 110
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->j(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/n;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/bytedance/sdk/component/d/n;->b:Lcom/bytedance/sdk/component/d/n;

    goto :goto_2

    .line 112
    :cond_2
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->j(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/n;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->p:Lcom/bytedance/sdk/component/d/n;

    .line 114
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->k(Lcom/bytedance/sdk/component/d/c/a$b;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->o:Lcom/bytedance/sdk/component/d/k;

    .line 117
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->l(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 118
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->l(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/component/d/c/a;->b(Ljava/lang/String;)V

    .line 119
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->l(Lcom/bytedance/sdk/component/d/c/a$b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Ljava/lang/String;)V

    .line 122
    :cond_3
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/c/a$b;->m(Lcom/bytedance/sdk/component/d/c/a$b;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/component/d/c/a;->n:Z

    .line 124
    iget-object p1, p0, Lcom/bytedance/sdk/component/d/c/a;->q:Ljava/util/Queue;

    new-instance v0, Lcom/bytedance/sdk/component/d/d/b;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/d/b;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/component/d/c/a$b;Lcom/bytedance/sdk/component/d/c/a$1;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/d/c/a;-><init>(Lcom/bytedance/sdk/component/d/c/a$b;)V

    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 263
    new-instance v0, Lcom/bytedance/sdk/component/d/d/g;

    invoke-direct {v0, p1, p2, p3}, Lcom/bytedance/sdk/component/d/d/g;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/component/d/d/g;->a(Lcom/bytedance/sdk/component/d/c/a;)V

    .line 264
    iget-object p1, p0, Lcom/bytedance/sdk/component/d/c/a;->q:Ljava/util/Queue;

    invoke-interface {p1}, Ljava/util/Queue;->clear()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/d/c/a;ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/component/d/c/a;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/d/c/a;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/bytedance/sdk/component/d/c/a;->m:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/component/d/c/a;)Ljava/util/Queue;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->q:Ljava/util/Queue;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->o:Lcom/bytedance/sdk/component/d/k;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/d;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/a;->m()Lcom/bytedance/sdk/component/d/d;

    move-result-object p0

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/component/d/c/a;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->l:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/p;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->k:Lcom/bytedance/sdk/component/d/p;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/component/d/c/a;)Landroid/os/Handler;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->r:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/n;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->p:Lcom/bytedance/sdk/component/d/n;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/component/d/c/a;)Ljava/lang/String;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a;->d:Ljava/lang/String;

    return-object p0
.end method

.method private m()Lcom/bytedance/sdk/component/d/d;
    .locals 3

    .line 211
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/d/c/b;->a()Lcom/bytedance/sdk/component/d/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/c/b;->g()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    new-instance v1, Lcom/bytedance/sdk/component/d/c/a$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/component/d/c/a$1;-><init>(Lcom/bytedance/sdk/component/d/c/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->a:Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 256
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ImageRequest"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/c;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a;->e:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 188
    iput-boolean p1, p0, Lcom/bytedance/sdk/component/d/c/a;->s:Z

    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/d/d/h;)Z
    .locals 1

    .line 196
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/d/c/a;->m:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->q:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public b()Lcom/bytedance/sdk/component/d/c/a/b;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->c:Lcom/bytedance/sdk/component/d/c/a/b;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->l:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x413c0901

    invoke-virtual {v0, v1, p1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 160
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a;->d:Ljava/lang/String;

    return-void
.end method

.method public c()Lcom/bytedance/sdk/component/d/g;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->f:Lcom/bytedance/sdk/component/d/g;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Landroid/widget/ImageView$ScaleType;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->g:Landroid/widget/ImageView$ScaleType;

    return-object v0
.end method

.method public g()Landroid/graphics/Bitmap$Config;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->h:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public h()I
    .locals 1

    .line 172
    iget v0, p0, Lcom/bytedance/sdk/component/d/c/a;->i:I

    return v0
.end method

.method public i()I
    .locals 1

    .line 176
    iget v0, p0, Lcom/bytedance/sdk/component/d/c/a;->j:I

    return v0
.end method

.method public j()Lcom/bytedance/sdk/component/d/p;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a;->k:Lcom/bytedance/sdk/component/d/p;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .line 184
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/d/c/a;->n:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .line 192
    iget-boolean v0, p0, Lcom/bytedance/sdk/component/d/c/a;->s:Z

    return v0
.end method
