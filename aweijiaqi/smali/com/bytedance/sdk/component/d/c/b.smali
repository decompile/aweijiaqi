.class public Lcom/bytedance/sdk/component/d/c/b;
.super Ljava/lang/Object;
.source "LoadFactory.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/component/d/c/b;


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/component/d/c/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/bytedance/sdk/component/d/o;

.field private d:Lcom/bytedance/sdk/component/d/i;

.field private e:Lcom/bytedance/sdk/component/d/j;

.field private f:Lcom/bytedance/sdk/component/d/b;

.field private g:Lcom/bytedance/sdk/component/d/c;

.field private h:Lcom/bytedance/sdk/component/d/f;

.field private i:Ljava/util/concurrent/ExecutorService;

.field private j:Lcom/bytedance/sdk/component/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V
    .locals 1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->b:Ljava/util/Map;

    .line 64
    invoke-static {p2}, Lcom/bytedance/sdk/component/d/c/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/d/o;

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    .line 66
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/d/o;->h()Lcom/bytedance/sdk/component/d/a;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    if-nez p2, :cond_0

    .line 69
    invoke-static {p1}, Lcom/bytedance/sdk/component/d/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/component/d/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    :cond_0
    return-void
.end method

.method public static a()Lcom/bytedance/sdk/component/d/c/b;
    .locals 2

    .line 37
    sget-object v0, Lcom/bytedance/sdk/component/d/c/b;->a:Lcom/bytedance/sdk/component/d/c/b;

    const-string v1, "ImageFactory was not initialized!"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/d/c/d;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/d/c/b;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V
    .locals 2

    const-class v0, Lcom/bytedance/sdk/component/d/c/b;

    monitor-enter v0

    .line 41
    :try_start_0
    new-instance v1, Lcom/bytedance/sdk/component/d/c/b;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/component/d/c/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V

    sput-object v1, Lcom/bytedance/sdk/component/d/c/b;->a:Lcom/bytedance/sdk/component/d/c/b;

    .line 42
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/o;->g()Lcom/bytedance/sdk/component/d/h;

    move-result-object p0

    invoke-static {p0}, Lcom/bytedance/sdk/component/d/c/c;->a(Lcom/bytedance/sdk/component/d/h;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private i()Lcom/bytedance/sdk/component/d/i;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->d()Lcom/bytedance/sdk/component/d/i;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a/b/a;->a(Lcom/bytedance/sdk/component/d/i;)Lcom/bytedance/sdk/component/d/i;

    move-result-object v0

    return-object v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a/b/a;->a(I)Lcom/bytedance/sdk/component/d/i;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/bytedance/sdk/component/d/j;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->e()Lcom/bytedance/sdk/component/d/j;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a/b/e;->a(I)Lcom/bytedance/sdk/component/d/j;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/bytedance/sdk/component/d/b;
    .locals 5

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->f()Lcom/bytedance/sdk/component/d/b;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 120
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/component/d/c/a/a/b;

    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/d/a;->c()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/component/d/c/b;->j:Lcom/bytedance/sdk/component/d/a;

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/d/a;->a()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/bytedance/sdk/component/d/c/b;->g()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/component/d/c/a/a/b;-><init>(Ljava/io/File;JLjava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method private l()Lcom/bytedance/sdk/component/d/c;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->c()Lcom/bytedance/sdk/component/d/c;

    move-result-object v0

    if-nez v0, :cond_0

    .line 134
    invoke-static {}, Lcom/bytedance/sdk/component/d/b/b;->a()Lcom/bytedance/sdk/component/d/c;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private m()Lcom/bytedance/sdk/component/d/f;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->a()Lcom/bytedance/sdk/component/d/f;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 153
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/component/d/a/b;->a()Lcom/bytedance/sdk/component/d/f;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->c:Lcom/bytedance/sdk/component/d/o;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 171
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/component/d/a/c;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/c/b/a;
    .locals 4

    .line 180
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->f()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    if-nez v0, :cond_0

    .line 182
    sget-object v0, Lcom/bytedance/sdk/component/d/c/b/a;->a:Landroid/widget/ImageView$ScaleType;

    .line 185
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->g()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    if-nez v1, :cond_1

    .line 187
    sget-object v1, Lcom/bytedance/sdk/component/d/c/b/a;->b:Landroid/graphics/Bitmap$Config;

    .line 190
    :cond_1
    new-instance v2, Lcom/bytedance/sdk/component/d/c/b/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->h()I

    move-result v3

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/c/a;->i()I

    move-result p1

    invoke-direct {v2, v3, p1, v0, v1}, Lcom/bytedance/sdk/component/d/c/b/a;-><init>(IILandroid/widget/ImageView$ScaleType;Landroid/graphics/Bitmap$Config;)V

    return-object v2
.end method

.method public b()Lcom/bytedance/sdk/component/d/i;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->d:Lcom/bytedance/sdk/component/d/i;

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->i()Lcom/bytedance/sdk/component/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->d:Lcom/bytedance/sdk/component/d/i;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->d:Lcom/bytedance/sdk/component/d/i;

    return-object v0
.end method

.method public c()Lcom/bytedance/sdk/component/d/j;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->e:Lcom/bytedance/sdk/component/d/j;

    if-nez v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->j()Lcom/bytedance/sdk/component/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->e:Lcom/bytedance/sdk/component/d/j;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->e:Lcom/bytedance/sdk/component/d/j;

    return-object v0
.end method

.method public d()Lcom/bytedance/sdk/component/d/b;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->f:Lcom/bytedance/sdk/component/d/b;

    if-nez v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->k()Lcom/bytedance/sdk/component/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->f:Lcom/bytedance/sdk/component/d/b;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->f:Lcom/bytedance/sdk/component/d/b;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/component/d/c;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->g:Lcom/bytedance/sdk/component/d/c;

    if-nez v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->l()Lcom/bytedance/sdk/component/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->g:Lcom/bytedance/sdk/component/d/c;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->g:Lcom/bytedance/sdk/component/d/c;

    return-object v0
.end method

.method public f()Lcom/bytedance/sdk/component/d/f;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->h:Lcom/bytedance/sdk/component/d/f;

    if-nez v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->m()Lcom/bytedance/sdk/component/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->h:Lcom/bytedance/sdk/component/d/f;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->h:Lcom/bytedance/sdk/component/d/f;

    return-object v0
.end method

.method public g()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->i:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 158
    invoke-direct {p0}, Lcom/bytedance/sdk/component/d/c/b;->n()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->i:Ljava/util/concurrent/ExecutorService;

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->i:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/component/d/c/a;",
            ">;>;"
        }
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/b;->b:Ljava/util/Map;

    return-object v0
.end method
