.class Lcom/bytedance/sdk/component/d/c/a$1;
.super Ljava/lang/Object;
.source "ImageRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/component/d/c/a;->m()Lcom/bytedance/sdk/component/d/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/component/d/c/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/d/c/a;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 220
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/c/a;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 221
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->b(Lcom/bytedance/sdk/component/d/c/a;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/d/d/h;

    if-nez v0, :cond_1

    goto :goto_1

    .line 227
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 228
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v1

    invoke-interface {v0}, Lcom/bytedance/sdk/component/d/d/h;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-interface {v1, v2, v3}, Lcom/bytedance/sdk/component/d/k;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/d/c/a;)V

    .line 231
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/d/d/h;->a(Lcom/bytedance/sdk/component/d/c/a;)V

    .line 233
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v1

    invoke-interface {v0}, Lcom/bytedance/sdk/component/d/d/h;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-interface {v1, v0, v2}, Lcom/bytedance/sdk/component/d/k;->b(Ljava/lang/String;Lcom/bytedance/sdk/component/d/c/a;)V

    goto :goto_0

    .line 239
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/c/a;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    const/16 v1, 0x3eb

    const-string v2, "canceled"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/c/a;ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    .line 245
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    const/16 v2, 0x7d0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/bytedance/sdk/component/d/c/a;->a(Lcom/bytedance/sdk/component/d/c/a;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 248
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->c(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/k;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$1;->a:Lcom/bytedance/sdk/component/d/c/a;

    const-string v2, "exception"

    invoke-interface {v0, v2, v1}, Lcom/bytedance/sdk/component/d/k;->b(Ljava/lang/String;Lcom/bytedance/sdk/component/d/c/a;)V

    :cond_4
    :goto_2
    return-void
.end method
