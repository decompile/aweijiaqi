.class Lcom/bytedance/sdk/component/d/c/a$a;
.super Ljava/lang/Object;
.source "ImageRequest.java"

# interfaces
.implements Lcom/bytedance/sdk/component/d/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/component/d/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/component/d/c/a;

.field private b:Lcom/bytedance/sdk/component/d/g;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/component/d/c/a;Lcom/bytedance/sdk/component/d/g;)V
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    iput-object p2, p0, Lcom/bytedance/sdk/component/d/c/a$a;->b:Lcom/bytedance/sdk/component/d/g;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/component/d/c/a$a;)Lcom/bytedance/sdk/component/d/g;
    .locals 0

    .line 377
    iget-object p0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->b:Lcom/bytedance/sdk/component/d/g;

    return-object p0
.end method

.method private a(Landroid/widget/ImageView;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const v1, 0x413c0901

    .line 426
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->i(Lcom/bytedance/sdk/component/d/c/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 434
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->h(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/n;

    move-result-object v0

    sget-object v1, Lcom/bytedance/sdk/component/d/n;->b:Lcom/bytedance/sdk/component/d/n;

    if-ne v0, v1, :cond_0

    .line 435
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->g(Lcom/bytedance/sdk/component/d/c/a;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/d/c/a$a$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/bytedance/sdk/component/d/c/a$a$3;-><init>(Lcom/bytedance/sdk/component/d/c/a$a;ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->b:Lcom/bytedance/sdk/component/d/g;

    if-eqz v0, :cond_1

    .line 445
    invoke-interface {v0, p1, p2, p3}, Lcom/bytedance/sdk/component/d/g;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/d/m;)V
    .locals 4

    .line 388
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->e(Lcom/bytedance/sdk/component/d/c/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 390
    iget-object v1, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v1}, Lcom/bytedance/sdk/component/d/c/a;->f(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/p;

    move-result-object v1

    sget-object v2, Lcom/bytedance/sdk/component/d/p;->a:Lcom/bytedance/sdk/component/d/p;

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/component/d/c/a$a;->a(Landroid/widget/ImageView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/m;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 394
    iget-object v2, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v2}, Lcom/bytedance/sdk/component/d/c/a;->g(Lcom/bytedance/sdk/component/d/c/a;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/component/d/c/a$a$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/bytedance/sdk/component/d/c/a$a$1;-><init>(Lcom/bytedance/sdk/component/d/c/a$a;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->h(Lcom/bytedance/sdk/component/d/c/a;)Lcom/bytedance/sdk/component/d/n;

    move-result-object v0

    sget-object v1, Lcom/bytedance/sdk/component/d/n;->b:Lcom/bytedance/sdk/component/d/n;

    if-ne v0, v1, :cond_1

    .line 403
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->a:Lcom/bytedance/sdk/component/d/c/a;

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/c/a;->g(Lcom/bytedance/sdk/component/d/c/a;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/component/d/c/a$a$2;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/component/d/c/a$a$2;-><init>(Lcom/bytedance/sdk/component/d/c/a$a;Lcom/bytedance/sdk/component/d/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/component/d/c/a$a;->b:Lcom/bytedance/sdk/component/d/g;

    if-eqz v0, :cond_2

    .line 413
    invoke-interface {v0, p1}, Lcom/bytedance/sdk/component/d/g;->a(Lcom/bytedance/sdk/component/d/m;)V

    :cond_2
    :goto_0
    return-void
.end method
