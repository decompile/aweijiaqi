.class public Lcom/bytedance/sdk/component/d/l;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# static fields
.field private static volatile a:Z


# direct methods
.method public static a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;
    .locals 1

    .line 48
    new-instance v0, Lcom/bytedance/sdk/component/d/c/a$b;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/c/a$b;-><init>()V

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/component/d/c/a$b;->b(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V
    .locals 2

    .line 31
    sget-boolean v0, Lcom/bytedance/sdk/component/d/l;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "ImageLoader"

    const-string v1, "already init!"

    .line 32
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/d/c/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    .line 34
    sput-boolean v0, Lcom/bytedance/sdk/component/d/l;->a:Z

    if-nez p1, :cond_1

    .line 36
    invoke-static {p0}, Lcom/bytedance/sdk/component/d/o;->a(Landroid/content/Context;)Lcom/bytedance/sdk/component/d/o;

    move-result-object p1

    .line 38
    :cond_1
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/d/c/b;->a(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V

    return-void
.end method
