.class public Lcom/bytedance/sdk/openadsdk/k/c/c;
.super Ljava/lang/Object;
.source "LogUploaderImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/k/c/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/k/c/c$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/k/c/b;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/c/c;->b:Ljava/util/List;

    .line 33
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/c/b;->a()Lcom/bytedance/sdk/openadsdk/k/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/c/c;->a:Lcom/bytedance/sdk/openadsdk/k/c/b;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/k/c/c;)Lcom/bytedance/sdk/openadsdk/k/c/b;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/k/c/c;->a:Lcom/bytedance/sdk/openadsdk/k/c/b;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/k/c/c;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/k/c/c;->b:Ljava/util/List;

    return-object p0
.end method

.method public static c()Lcom/bytedance/sdk/openadsdk/k/c/a;
    .locals 1

    .line 178
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/c/e;->c()Lcom/bytedance/sdk/openadsdk/k/c/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 38
    new-instance v0, Lcom/bytedance/sdk/openadsdk/k/c/c$1;

    const-string v1, "init"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/k/c/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/k/c/c;Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/k/a/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/k/c/c;->a(Lcom/bytedance/sdk/openadsdk/k/a/a;Z)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/k/a/a;Z)V
    .locals 2

    if-eqz p1, :cond_2

    .line 69
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 72
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/k/c/c$a;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/k/a/a;->a()Lorg/json/JSONObject;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/k/c/c$a;-><init>(Ljava/lang/String;Lorg/json/JSONObject;)V

    if-eqz p2, :cond_1

    .line 74
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->e()Lcom/bytedance/sdk/openadsdk/e/b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/e/b;->a(Lcom/bytedance/sdk/openadsdk/e/i;)V

    goto :goto_0

    .line 76
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/e/b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/e/b;->a(Lcom/bytedance/sdk/openadsdk/e/i;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
