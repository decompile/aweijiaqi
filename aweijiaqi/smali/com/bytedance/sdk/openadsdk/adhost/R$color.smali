.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$color;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final notification_action_color_filter:I = 0x7f050085

.field public static final notification_icon_bg_color:I = 0x7f050086

.field public static final primary_text_default_material_dark:I = 0x7f05008b

.field public static final ripple_material_light:I = 0x7f050090

.field public static final secondary_text_default_material_dark:I = 0x7f050091

.field public static final secondary_text_default_material_light:I = 0x7f050092

.field public static final tt_app_detail_bg:I = 0x7f05009d

.field public static final tt_app_detail_line_bg:I = 0x7f05009e

.field public static final tt_app_detail_privacy_text_bg:I = 0x7f05009f

.field public static final tt_app_detail_stroke_bg:I = 0x7f0500a0

.field public static final tt_appdownloader_notification_material_background_color:I = 0x7f0500a1

.field public static final tt_appdownloader_notification_title_color:I = 0x7f0500a2

.field public static final tt_appdownloader_s1:I = 0x7f0500a3

.field public static final tt_appdownloader_s13:I = 0x7f0500a4

.field public static final tt_appdownloader_s18:I = 0x7f0500a5

.field public static final tt_appdownloader_s4:I = 0x7f0500a6

.field public static final tt_appdownloader_s8:I = 0x7f0500a7

.field public static final tt_cancle_bg:I = 0x7f0500a8

.field public static final tt_common_download_bg:I = 0x7f0500a9

.field public static final tt_common_download_btn_bg:I = 0x7f0500aa

.field public static final tt_dislike_dialog_background:I = 0x7f0500ab

.field public static final tt_dislike_transparent:I = 0x7f0500ac

.field public static final tt_divider:I = 0x7f0500ad

.field public static final tt_download_app_name:I = 0x7f0500ae

.field public static final tt_download_bar_background:I = 0x7f0500af

.field public static final tt_download_bar_background_new:I = 0x7f0500b0

.field public static final tt_download_text_background:I = 0x7f0500b1

.field public static final tt_draw_btn_back:I = 0x7f0500b2

.field public static final tt_full_background:I = 0x7f0500b3

.field public static final tt_full_interaction_bar_background:I = 0x7f0500b4

.field public static final tt_full_interaction_dialog_background:I = 0x7f0500b5

.field public static final tt_full_screen_skip_bg:I = 0x7f0500b6

.field public static final tt_full_status_bar_color:I = 0x7f0500b7

.field public static final tt_header_font:I = 0x7f0500b8

.field public static final tt_heise3:I = 0x7f0500b9

.field public static final tt_listview:I = 0x7f0500ba

.field public static final tt_listview_press:I = 0x7f0500bb

.field public static final tt_rating_comment:I = 0x7f0500bc

.field public static final tt_rating_comment_vertical:I = 0x7f0500bd

.field public static final tt_rating_star:I = 0x7f0500be

.field public static final tt_skip_red:I = 0x7f0500bf

.field public static final tt_ssxinbaise4:I = 0x7f0500c0

.field public static final tt_ssxinbaise4_press:I = 0x7f0500c1

.field public static final tt_ssxinheihui3:I = 0x7f0500c2

.field public static final tt_ssxinhongse1:I = 0x7f0500c3

.field public static final tt_ssxinmian1:I = 0x7f0500c4

.field public static final tt_ssxinmian11:I = 0x7f0500c5

.field public static final tt_ssxinmian15:I = 0x7f0500c6

.field public static final tt_ssxinmian6:I = 0x7f0500c7

.field public static final tt_ssxinmian7:I = 0x7f0500c8

.field public static final tt_ssxinmian8:I = 0x7f0500c9

.field public static final tt_ssxinxian11:I = 0x7f0500ca

.field public static final tt_ssxinxian11_selected:I = 0x7f0500cb

.field public static final tt_ssxinxian3:I = 0x7f0500cc

.field public static final tt_ssxinxian3_press:I = 0x7f0500cd

.field public static final tt_ssxinzi12:I = 0x7f0500ce

.field public static final tt_ssxinzi15:I = 0x7f0500cf

.field public static final tt_ssxinzi4:I = 0x7f0500d0

.field public static final tt_ssxinzi9:I = 0x7f0500d1

.field public static final tt_text_font:I = 0x7f0500d2

.field public static final tt_titlebar_background_dark:I = 0x7f0500d3

.field public static final tt_titlebar_background_ffffff:I = 0x7f0500d4

.field public static final tt_titlebar_background_light:I = 0x7f0500d5

.field public static final tt_trans_black:I = 0x7f0500d6

.field public static final tt_trans_half_black:I = 0x7f0500d7

.field public static final tt_transparent:I = 0x7f0500d8

.field public static final tt_video_player_text:I = 0x7f0500d9

.field public static final tt_video_player_text_withoutnight:I = 0x7f0500da

.field public static final tt_video_playerbg_color:I = 0x7f0500db

.field public static final tt_video_shadow_color:I = 0x7f0500dc

.field public static final tt_video_shaoow_color_fullscreen:I = 0x7f0500dd

.field public static final tt_video_time_color:I = 0x7f0500de

.field public static final tt_video_traffic_tip_background_color:I = 0x7f0500df

.field public static final tt_video_transparent:I = 0x7f0500e0

.field public static final tt_white:I = 0x7f0500e1

.field public static final ttdownloader_transparent:I = 0x7f0500e2


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
