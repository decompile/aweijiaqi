.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final notification_action_background:I = 0x7f070121

.field public static final notification_bg:I = 0x7f070122

.field public static final notification_bg_low:I = 0x7f070123

.field public static final notification_bg_low_normal:I = 0x7f070124

.field public static final notification_bg_low_pressed:I = 0x7f070125

.field public static final notification_bg_normal:I = 0x7f070126

.field public static final notification_bg_normal_pressed:I = 0x7f070127

.field public static final notification_icon_background:I = 0x7f070128

.field public static final notification_template_icon_bg:I = 0x7f070129

.field public static final notification_template_icon_low_bg:I = 0x7f07012a

.field public static final notification_tile_bg:I = 0x7f07012b

.field public static final notify_panel_notification_icon_bg:I = 0x7f07012c

.field public static final tt_ad_backup_bk:I = 0x7f070130

.field public static final tt_ad_backup_bk2:I = 0x7f070131

.field public static final tt_ad_cover_btn_begin_bg:I = 0x7f070132

.field public static final tt_ad_cover_btn_draw_begin_bg:I = 0x7f070133

.field public static final tt_ad_download_progress_bar_horizontal:I = 0x7f070134

.field public static final tt_ad_logo:I = 0x7f070135

.field public static final tt_ad_logo_background:I = 0x7f070136

.field public static final tt_ad_logo_reward_full:I = 0x7f070137

.field public static final tt_ad_logo_small:I = 0x7f070138

.field public static final tt_ad_logo_small_rectangle:I = 0x7f070139

.field public static final tt_ad_skip_btn_bg:I = 0x7f07013a

.field public static final tt_adinfo_dialog_corner_bg:I = 0x7f07013b

.field public static final tt_app_detail_back_btn:I = 0x7f07013c

.field public static final tt_app_detail_bg:I = 0x7f07013d

.field public static final tt_app_detail_black:I = 0x7f07013e

.field public static final tt_app_detail_info:I = 0x7f07013f

.field public static final tt_appdownloader_action_bg:I = 0x7f070140

.field public static final tt_appdownloader_action_new_bg:I = 0x7f070141

.field public static final tt_appdownloader_ad_detail_download_progress:I = 0x7f070142

.field public static final tt_appdownloader_detail_download_success_bg:I = 0x7f070143

.field public static final tt_appdownloader_download_progress_bar_horizontal:I = 0x7f070144

.field public static final tt_appdownloader_download_progress_bar_horizontal_new:I = 0x7f070145

.field public static final tt_appdownloader_download_progress_bar_horizontal_night:I = 0x7f070146

.field public static final tt_back_video:I = 0x7f070147

.field public static final tt_backup_btn_1:I = 0x7f070148

.field public static final tt_backup_btn_2:I = 0x7f070149

.field public static final tt_browser_download_selector:I = 0x7f07014a

.field public static final tt_browser_progress_style:I = 0x7f07014b

.field public static final tt_circle_solid_mian:I = 0x7f07014c

.field public static final tt_close_move_detail:I = 0x7f07014d

.field public static final tt_close_move_details_normal:I = 0x7f07014e

.field public static final tt_close_move_details_pressed:I = 0x7f07014f

.field public static final tt_comment_tv:I = 0x7f070150

.field public static final tt_common_download_bg:I = 0x7f070151

.field public static final tt_common_download_btn_bg:I = 0x7f070152

.field public static final tt_custom_dialog_bg:I = 0x7f070153

.field public static final tt_detail_video_btn_bg:I = 0x7f070154

.field public static final tt_dislike_bottom_seletor:I = 0x7f070155

.field public static final tt_dislike_cancle_bg_selector:I = 0x7f070156

.field public static final tt_dislike_dialog_bg:I = 0x7f070157

.field public static final tt_dislike_flowlayout_tv_bg:I = 0x7f070158

.field public static final tt_dislike_icon:I = 0x7f070159

.field public static final tt_dislike_icon2:I = 0x7f07015a

.field public static final tt_dislike_icon_inter_night:I = 0x7f07015b

.field public static final tt_dislike_icon_night:I = 0x7f07015c

.field public static final tt_dislike_middle_seletor:I = 0x7f07015d

.field public static final tt_dislike_son_tag:I = 0x7f07015e

.field public static final tt_dislike_top_bg:I = 0x7f07015f

.field public static final tt_dislike_top_seletor:I = 0x7f070160

.field public static final tt_download_btn_bg:I = 0x7f070161

.field public static final tt_download_corner_bg:I = 0x7f070162

.field public static final tt_download_dialog_btn_bg:I = 0x7f070163

.field public static final tt_draw_back_bg:I = 0x7f070164

.field public static final tt_enlarge_video:I = 0x7f070165

.field public static final tt_forward_video:I = 0x7f070166

.field public static final tt_install_bk:I = 0x7f070167

.field public static final tt_install_btn_bk:I = 0x7f070168

.field public static final tt_leftbackbutton_titlebar_photo_preview:I = 0x7f070169

.field public static final tt_leftbackicon_selector:I = 0x7f07016a

.field public static final tt_leftbackicon_selector_for_dark:I = 0x7f07016b

.field public static final tt_lefterbackicon_titlebar:I = 0x7f07016c

.field public static final tt_lefterbackicon_titlebar_for_dark:I = 0x7f07016d

.field public static final tt_lefterbackicon_titlebar_press:I = 0x7f07016e

.field public static final tt_lefterbackicon_titlebar_press_for_dark:I = 0x7f07016f

.field public static final tt_mute:I = 0x7f070170

.field public static final tt_mute_btn_bg:I = 0x7f070171

.field public static final tt_new_pause_video:I = 0x7f070172

.field public static final tt_new_pause_video_press:I = 0x7f070173

.field public static final tt_new_play_video:I = 0x7f070174

.field public static final tt_normalscreen_loading:I = 0x7f070175

.field public static final tt_open_app_detail_download_btn_bg:I = 0x7f070176

.field public static final tt_open_app_detail_list_item:I = 0x7f070177

.field public static final tt_play_movebar_textpage:I = 0x7f070178

.field public static final tt_playable_btn_bk:I = 0x7f070179

.field public static final tt_playable_l_logo:I = 0x7f07017a

.field public static final tt_playable_progress_style:I = 0x7f07017b

.field public static final tt_refreshing_video_textpage:I = 0x7f07017c

.field public static final tt_refreshing_video_textpage_normal:I = 0x7f07017d

.field public static final tt_refreshing_video_textpage_pressed:I = 0x7f07017e

.field public static final tt_reward_countdown_bg:I = 0x7f07017f

.field public static final tt_reward_dislike_icon:I = 0x7f070180

.field public static final tt_reward_full_new_bar_bg:I = 0x7f070181

.field public static final tt_reward_full_new_bar_btn_bg:I = 0x7f070182

.field public static final tt_reward_full_video_backup_btn_bg:I = 0x7f070183

.field public static final tt_reward_video_download_btn_bg:I = 0x7f070184

.field public static final tt_right_arrow:I = 0x7f070185

.field public static final tt_seek_progress:I = 0x7f070186

.field public static final tt_seek_thumb:I = 0x7f070187

.field public static final tt_seek_thumb_fullscreen:I = 0x7f070188

.field public static final tt_seek_thumb_fullscreen_press:I = 0x7f070189

.field public static final tt_seek_thumb_fullscreen_selector:I = 0x7f07018a

.field public static final tt_seek_thumb_normal:I = 0x7f07018b

.field public static final tt_seek_thumb_press:I = 0x7f07018c

.field public static final tt_shadow_btn_back:I = 0x7f07018d

.field public static final tt_shadow_btn_back_withoutnight:I = 0x7f07018e

.field public static final tt_shadow_fullscreen_top:I = 0x7f07018f

.field public static final tt_shadow_lefterback_titlebar:I = 0x7f070190

.field public static final tt_shadow_lefterback_titlebar_press:I = 0x7f070191

.field public static final tt_shadow_lefterback_titlebar_press_withoutnight:I = 0x7f070192

.field public static final tt_shadow_lefterback_titlebar_withoutnight:I = 0x7f070193

.field public static final tt_shrink_fullscreen:I = 0x7f070194

.field public static final tt_shrink_video:I = 0x7f070195

.field public static final tt_skip_text_bg:I = 0x7f070196

.field public static final tt_splash_ad_backup_bg:I = 0x7f070197

.field public static final tt_splash_ad_backup_btn_bg:I = 0x7f070198

.field public static final tt_splash_ad_logo:I = 0x7f070199

.field public static final tt_splash_click_bar_go:I = 0x7f07019a

.field public static final tt_splash_click_bar_style:I = 0x7f07019b

.field public static final tt_splash_mute:I = 0x7f07019c

.field public static final tt_splash_unmute:I = 0x7f07019d

.field public static final tt_star_empty_bg:I = 0x7f07019e

.field public static final tt_star_full_bg:I = 0x7f07019f

.field public static final tt_stop_movebar_textpage:I = 0x7f0701a0

.field public static final tt_suggestion_logo:I = 0x7f0701a1

.field public static final tt_titlebar_close_drawable:I = 0x7f0701a2

.field public static final tt_titlebar_close_for_dark:I = 0x7f0701a3

.field public static final tt_titlebar_close_press:I = 0x7f0701a4

.field public static final tt_titlebar_close_press_for_dark:I = 0x7f0701a5

.field public static final tt_titlebar_close_seletor:I = 0x7f0701a6

.field public static final tt_titlebar_close_seletor_for_dark:I = 0x7f0701a7

.field public static final tt_unmute:I = 0x7f0701a8

.field public static final tt_video_black_desc_gradient:I = 0x7f0701a9

.field public static final tt_video_close_drawable:I = 0x7f0701aa

.field public static final tt_video_loading_progress_bar:I = 0x7f0701ab

.field public static final tt_video_progress_drawable:I = 0x7f0701ac

.field public static final tt_video_traffic_continue_play_bg:I = 0x7f0701ad

.field public static final tt_white_lefterbackicon_titlebar:I = 0x7f0701ae

.field public static final tt_white_lefterbackicon_titlebar_press:I = 0x7f0701af

.field public static final ttdownloader_bg_appinfo_btn:I = 0x7f0701b0

.field public static final ttdownloader_bg_appinfo_dialog:I = 0x7f0701b1

.field public static final ttdownloader_bg_button_blue_corner:I = 0x7f0701b2

.field public static final ttdownloader_bg_kllk_btn1:I = 0x7f0701b3

.field public static final ttdownloader_bg_kllk_btn2:I = 0x7f0701b4

.field public static final ttdownloader_bg_transparent:I = 0x7f0701b5

.field public static final ttdownloader_bg_white_corner:I = 0x7f0701b6

.field public static final ttdownloader_dash_line:I = 0x7f0701b7

.field public static final ttdownloader_icon_back_arrow:I = 0x7f0701b8

.field public static final ttdownloader_icon_download:I = 0x7f0701b9

.field public static final ttdownloader_icon_yes:I = 0x7f0701ba


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
