.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f0e003b

.field public static final status_bar_notification_info_overflow:I = 0x7f0e007c

.field public static final tt_00_00:I = 0x7f0e007d

.field public static final tt_ad:I = 0x7f0e007e

.field public static final tt_ad_logo_txt:I = 0x7f0e007f

.field public static final tt_app_name:I = 0x7f0e0080

.field public static final tt_app_privacy_dialog_title:I = 0x7f0e0081

.field public static final tt_appdownloader_button_cancel_download:I = 0x7f0e0082

.field public static final tt_appdownloader_button_queue_for_wifi:I = 0x7f0e0083

.field public static final tt_appdownloader_button_start_now:I = 0x7f0e0084

.field public static final tt_appdownloader_download_percent:I = 0x7f0e0085

.field public static final tt_appdownloader_download_remaining:I = 0x7f0e0086

.field public static final tt_appdownloader_download_unknown_title:I = 0x7f0e0087

.field public static final tt_appdownloader_duration_hours:I = 0x7f0e0088

.field public static final tt_appdownloader_duration_minutes:I = 0x7f0e0089

.field public static final tt_appdownloader_duration_seconds:I = 0x7f0e008a

.field public static final tt_appdownloader_jump_unknown_source:I = 0x7f0e008b

.field public static final tt_appdownloader_label_cancel:I = 0x7f0e008c

.field public static final tt_appdownloader_label_cancel_directly:I = 0x7f0e008d

.field public static final tt_appdownloader_label_ok:I = 0x7f0e008e

.field public static final tt_appdownloader_label_reserve_wifi:I = 0x7f0e008f

.field public static final tt_appdownloader_notification_download:I = 0x7f0e0090

.field public static final tt_appdownloader_notification_download_complete_open:I = 0x7f0e0091

.field public static final tt_appdownloader_notification_download_complete_with_install:I = 0x7f0e0092

.field public static final tt_appdownloader_notification_download_complete_without_install:I = 0x7f0e0093

.field public static final tt_appdownloader_notification_download_continue:I = 0x7f0e0094

.field public static final tt_appdownloader_notification_download_delete:I = 0x7f0e0095

.field public static final tt_appdownloader_notification_download_failed:I = 0x7f0e0096

.field public static final tt_appdownloader_notification_download_install:I = 0x7f0e0097

.field public static final tt_appdownloader_notification_download_open:I = 0x7f0e0098

.field public static final tt_appdownloader_notification_download_pause:I = 0x7f0e0099

.field public static final tt_appdownloader_notification_download_restart:I = 0x7f0e009a

.field public static final tt_appdownloader_notification_download_resume:I = 0x7f0e009b

.field public static final tt_appdownloader_notification_download_space_failed:I = 0x7f0e009c

.field public static final tt_appdownloader_notification_download_waiting_net:I = 0x7f0e009d

.field public static final tt_appdownloader_notification_download_waiting_wifi:I = 0x7f0e009e

.field public static final tt_appdownloader_notification_downloading:I = 0x7f0e009f

.field public static final tt_appdownloader_notification_install_finished_open:I = 0x7f0e00a0

.field public static final tt_appdownloader_notification_insufficient_space_error:I = 0x7f0e00a1

.field public static final tt_appdownloader_notification_need_wifi_for_size:I = 0x7f0e00a2

.field public static final tt_appdownloader_notification_no_internet_error:I = 0x7f0e00a3

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I = 0x7f0e00a4

.field public static final tt_appdownloader_notification_paused_in_background:I = 0x7f0e00a5

.field public static final tt_appdownloader_notification_pausing:I = 0x7f0e00a6

.field public static final tt_appdownloader_notification_prepare:I = 0x7f0e00a7

.field public static final tt_appdownloader_notification_request_btn_no:I = 0x7f0e00a8

.field public static final tt_appdownloader_notification_request_btn_yes:I = 0x7f0e00a9

.field public static final tt_appdownloader_notification_request_message:I = 0x7f0e00aa

.field public static final tt_appdownloader_notification_request_title:I = 0x7f0e00ab

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I = 0x7f0e00ac

.field public static final tt_appdownloader_resume_in_wifi:I = 0x7f0e00ad

.field public static final tt_appdownloader_tip:I = 0x7f0e00ae

.field public static final tt_appdownloader_wifi_recommended_body:I = 0x7f0e00af

.field public static final tt_appdownloader_wifi_recommended_title:I = 0x7f0e00b0

.field public static final tt_appdownloader_wifi_required_body:I = 0x7f0e00b1

.field public static final tt_appdownloader_wifi_required_title:I = 0x7f0e00b2

.field public static final tt_auto_play_cancel_text:I = 0x7f0e00b3

.field public static final tt_cancel:I = 0x7f0e00b4

.field public static final tt_click_replay:I = 0x7f0e00b5

.field public static final tt_comment_num:I = 0x7f0e00b6

.field public static final tt_comment_num_backup:I = 0x7f0e00b7

.field public static final tt_comment_score:I = 0x7f0e00b8

.field public static final tt_common_download_app_detail:I = 0x7f0e00b9

.field public static final tt_common_download_app_privacy:I = 0x7f0e00ba

.field public static final tt_common_download_cancel:I = 0x7f0e00bb

.field public static final tt_confirm_download:I = 0x7f0e00bc

.field public static final tt_confirm_download_have_app_name:I = 0x7f0e00bd

.field public static final tt_dislike_comment_hint:I = 0x7f0e00be

.field public static final tt_dislike_feedback_repeat:I = 0x7f0e00bf

.field public static final tt_dislike_feedback_success:I = 0x7f0e00c0

.field public static final tt_dislike_header_tv_back:I = 0x7f0e00c1

.field public static final tt_dislike_header_tv_title:I = 0x7f0e00c2

.field public static final tt_dislike_other_suggest:I = 0x7f0e00c3

.field public static final tt_dislike_other_suggest_out:I = 0x7f0e00c4

.field public static final tt_dislike_submit:I = 0x7f0e00c5

.field public static final tt_download:I = 0x7f0e00c6

.field public static final tt_download_finish:I = 0x7f0e00c7

.field public static final tt_feedback:I = 0x7f0e00c8

.field public static final tt_full_screen_skip_tx:I = 0x7f0e00c9

.field public static final tt_image_download_apk:I = 0x7f0e00ca

.field public static final tt_install:I = 0x7f0e00cb

.field public static final tt_label_cancel:I = 0x7f0e00cc

.field public static final tt_label_ok:I = 0x7f0e00cd

.field public static final tt_no_network:I = 0x7f0e00ce

.field public static final tt_open_app_detail_developer:I = 0x7f0e00cf

.field public static final tt_open_app_detail_privacy:I = 0x7f0e00d0

.field public static final tt_open_app_detail_privacy_list:I = 0x7f0e00d1

.field public static final tt_open_app_name:I = 0x7f0e00d2

.field public static final tt_open_app_version:I = 0x7f0e00d3

.field public static final tt_open_landing_page_app_name:I = 0x7f0e00d4

.field public static final tt_permission_denied:I = 0x7f0e00d5

.field public static final tt_playable_btn_play:I = 0x7f0e00d6

.field public static final tt_quit:I = 0x7f0e00d7

.field public static final tt_request_permission_descript_external_storage:I = 0x7f0e00d8

.field public static final tt_request_permission_descript_location:I = 0x7f0e00d9

.field public static final tt_request_permission_descript_read_phone_state:I = 0x7f0e00da

.field public static final tt_reward_feedback:I = 0x7f0e00db

.field public static final tt_reward_screen_skip_tx:I = 0x7f0e00dc

.field public static final tt_splash_backup_ad_btn:I = 0x7f0e00dd

.field public static final tt_splash_backup_ad_title:I = 0x7f0e00de

.field public static final tt_splash_click_bar_text:I = 0x7f0e00df

.field public static final tt_splash_skip_tv_text:I = 0x7f0e00e0

.field public static final tt_tip:I = 0x7f0e00e1

.field public static final tt_unlike:I = 0x7f0e00e2

.field public static final tt_video_bytesize:I = 0x7f0e00e3

.field public static final tt_video_bytesize_M:I = 0x7f0e00e4

.field public static final tt_video_bytesize_MB:I = 0x7f0e00e5

.field public static final tt_video_continue_play:I = 0x7f0e00e6

.field public static final tt_video_dial_phone:I = 0x7f0e00e7

.field public static final tt_video_dial_replay:I = 0x7f0e00e8

.field public static final tt_video_download_apk:I = 0x7f0e00e9

.field public static final tt_video_mobile_go_detail:I = 0x7f0e00ea

.field public static final tt_video_retry_des_txt:I = 0x7f0e00eb

.field public static final tt_video_without_wifi_tips:I = 0x7f0e00ec

.field public static final tt_web_title_default:I = 0x7f0e00ed

.field public static final tt_will_play:I = 0x7f0e00ee


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
