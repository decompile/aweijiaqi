.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_container:I = 0x7f08000d

.field public static final action_divider:I = 0x7f08000f

.field public static final action_image:I = 0x7f080010

.field public static final action_text:I = 0x7f080016

.field public static final actions:I = 0x7f080017

.field public static final async:I = 0x7f080082

.field public static final blocking:I = 0x7f080086

.field public static final bottom:I = 0x7f080087

.field public static final cancel_tv:I = 0x7f08008a

.field public static final chronometer:I = 0x7f08008f

.field public static final confirm_tv:I = 0x7f080093

.field public static final dash_line:I = 0x7f08009d

.field public static final end:I = 0x7f0800a7

.field public static final forever:I = 0x7f0800b6

.field public static final icon:I = 0x7f0800c2

.field public static final icon_group:I = 0x7f0800c3

.field public static final info:I = 0x7f0800c6

.field public static final italic:I = 0x7f0800c7

.field public static final iv_app_icon:I = 0x7f0800c9

.field public static final iv_detail_back:I = 0x7f0800ca

.field public static final iv_privacy_back:I = 0x7f0800cc

.field public static final left:I = 0x7f0801b5

.field public static final line:I = 0x7f0801b7

.field public static final line1:I = 0x7f0801b8

.field public static final line3:I = 0x7f0801b9

.field public static final ll_download:I = 0x7f0801bc

.field public static final message_tv:I = 0x7f0801c1

.field public static final none:I = 0x7f0801c9

.field public static final normal:I = 0x7f0801ca

.field public static final notification_background:I = 0x7f0801cb

.field public static final notification_main_column:I = 0x7f0801cd

.field public static final notification_main_column_container:I = 0x7f0801ce

.field public static final permission_list:I = 0x7f0801d4

.field public static final privacy_webview:I = 0x7f0801d6

.field public static final right:I = 0x7f0801db

.field public static final right_icon:I = 0x7f0801dc

.field public static final right_side:I = 0x7f0801dd

.field public static final start:I = 0x7f080205

.field public static final tag_transition_group:I = 0x7f08020a

.field public static final tag_unhandled_key_event_manager:I = 0x7f08020b

.field public static final tag_unhandled_key_listeners:I = 0x7f08020c

.field public static final text:I = 0x7f08020d

.field public static final text2:I = 0x7f08020e

.field public static final time:I = 0x7f080216

.field public static final title:I = 0x7f080217

.field public static final title_bar:I = 0x7f080219

.field public static final top:I = 0x7f08021c

.field public static final tt_ad_container:I = 0x7f080224

.field public static final tt_ad_content_layout:I = 0x7f080225

.field public static final tt_ad_logo:I = 0x7f080226

.field public static final tt_app_detail_back_tv:I = 0x7f080227

.field public static final tt_app_developer_tv:I = 0x7f080228

.field public static final tt_app_name_tv:I = 0x7f080229

.field public static final tt_app_privacy_back_tv:I = 0x7f08022a

.field public static final tt_app_privacy_title:I = 0x7f08022b

.field public static final tt_app_privacy_tv:I = 0x7f08022c

.field public static final tt_app_privacy_url_tv:I = 0x7f08022d

.field public static final tt_app_version_tv:I = 0x7f08022e

.field public static final tt_appdownloader_action:I = 0x7f08022f

.field public static final tt_appdownloader_desc:I = 0x7f080230

.field public static final tt_appdownloader_download_progress:I = 0x7f080231

.field public static final tt_appdownloader_download_progress_new:I = 0x7f080232

.field public static final tt_appdownloader_download_size:I = 0x7f080233

.field public static final tt_appdownloader_download_status:I = 0x7f080234

.field public static final tt_appdownloader_download_success:I = 0x7f080235

.field public static final tt_appdownloader_download_success_size:I = 0x7f080236

.field public static final tt_appdownloader_download_success_status:I = 0x7f080237

.field public static final tt_appdownloader_download_text:I = 0x7f080238

.field public static final tt_appdownloader_icon:I = 0x7f080239

.field public static final tt_appdownloader_root:I = 0x7f08023a

.field public static final tt_backup_draw_bg:I = 0x7f08023b

.field public static final tt_backup_logoLayout:I = 0x7f08023c

.field public static final tt_battery_time_layout:I = 0x7f08023d

.field public static final tt_browser_download_btn:I = 0x7f08023e

.field public static final tt_browser_download_btn_stub:I = 0x7f08023f

.field public static final tt_browser_progress:I = 0x7f080240

.field public static final tt_browser_titlebar_dark_view_stub:I = 0x7f080241

.field public static final tt_browser_titlebar_view_stub:I = 0x7f080242

.field public static final tt_browser_webview:I = 0x7f080243

.field public static final tt_browser_webview_loading:I = 0x7f080244

.field public static final tt_bu_close:I = 0x7f080245

.field public static final tt_bu_desc:I = 0x7f080246

.field public static final tt_bu_dislike:I = 0x7f080247

.field public static final tt_bu_download:I = 0x7f080248

.field public static final tt_bu_icon:I = 0x7f080249

.field public static final tt_bu_img:I = 0x7f08024a

.field public static final tt_bu_img_1:I = 0x7f08024b

.field public static final tt_bu_img_2:I = 0x7f08024c

.field public static final tt_bu_img_3:I = 0x7f08024d

.field public static final tt_bu_img_container:I = 0x7f08024e

.field public static final tt_bu_img_content:I = 0x7f08024f

.field public static final tt_bu_name:I = 0x7f080250

.field public static final tt_bu_score:I = 0x7f080251

.field public static final tt_bu_score_bar:I = 0x7f080252

.field public static final tt_bu_title:I = 0x7f080253

.field public static final tt_bu_total_title:I = 0x7f080254

.field public static final tt_bu_video_container:I = 0x7f080255

.field public static final tt_bu_video_container_inner:I = 0x7f080256

.field public static final tt_bu_video_icon:I = 0x7f080257

.field public static final tt_bu_video_name1:I = 0x7f080258

.field public static final tt_bu_video_name2:I = 0x7f080259

.field public static final tt_bu_video_score:I = 0x7f08025a

.field public static final tt_bu_video_score_bar:I = 0x7f08025b

.field public static final tt_button_ok:I = 0x7f08025c

.field public static final tt_click_lower_non_content_layout:I = 0x7f08025d

.field public static final tt_click_upper_non_content_layout:I = 0x7f08025e

.field public static final tt_column_line:I = 0x7f08025f

.field public static final tt_comment_backup:I = 0x7f080260

.field public static final tt_comment_close:I = 0x7f080261

.field public static final tt_comment_commit:I = 0x7f080262

.field public static final tt_comment_content:I = 0x7f080263

.field public static final tt_comment_number:I = 0x7f080264

.field public static final tt_comment_vertical:I = 0x7f080265

.field public static final tt_dialog_content:I = 0x7f080266

.field public static final tt_dislike_comment_layout:I = 0x7f080267

.field public static final tt_dislike_layout:I = 0x7f080268

.field public static final tt_dislike_line1:I = 0x7f080269

.field public static final tt_download_app_btn:I = 0x7f08026a

.field public static final tt_download_app_detail:I = 0x7f08026b

.field public static final tt_download_app_developer:I = 0x7f08026c

.field public static final tt_download_app_privacy:I = 0x7f08026d

.field public static final tt_download_app_version:I = 0x7f08026e

.field public static final tt_download_btn:I = 0x7f08026f

.field public static final tt_download_cancel:I = 0x7f080270

.field public static final tt_download_icon:I = 0x7f080271

.field public static final tt_download_layout:I = 0x7f080272

.field public static final tt_download_title:I = 0x7f080273

.field public static final tt_edit_suggestion:I = 0x7f080274

.field public static final tt_filer_words_lv:I = 0x7f080275

.field public static final tt_full_ad_app_name:I = 0x7f080276

.field public static final tt_full_ad_appname:I = 0x7f080277

.field public static final tt_full_ad_desc:I = 0x7f080278

.field public static final tt_full_ad_download:I = 0x7f080279

.field public static final tt_full_ad_icon:I = 0x7f08027a

.field public static final tt_full_comment:I = 0x7f08027b

.field public static final tt_full_desc:I = 0x7f08027c

.field public static final tt_full_image_full_bar:I = 0x7f08027d

.field public static final tt_full_image_layout:I = 0x7f08027e

.field public static final tt_full_image_reward_bar:I = 0x7f08027f

.field public static final tt_full_img:I = 0x7f080280

.field public static final tt_full_rb_score:I = 0x7f080281

.field public static final tt_full_root:I = 0x7f080282

.field public static final tt_full_splash_bar_layout:I = 0x7f080283

.field public static final tt_image:I = 0x7f080285

.field public static final tt_image_full_bar:I = 0x7f080286

.field public static final tt_image_group_layout:I = 0x7f080287

.field public static final tt_insert_ad_img:I = 0x7f080288

.field public static final tt_insert_ad_logo:I = 0x7f080289

.field public static final tt_insert_ad_text:I = 0x7f08028a

.field public static final tt_insert_dislike_icon_img:I = 0x7f08028b

.field public static final tt_insert_express_ad_fl:I = 0x7f08028c

.field public static final tt_install_btn_no:I = 0x7f08028d

.field public static final tt_install_btn_yes:I = 0x7f08028e

.field public static final tt_install_content:I = 0x7f08028f

.field public static final tt_install_title:I = 0x7f080290

.field public static final tt_item_desc_tv:I = 0x7f080291

.field public static final tt_item_select_img:I = 0x7f080292

.field public static final tt_item_title_tv:I = 0x7f080293

.field public static final tt_item_tv:I = 0x7f080294

.field public static final tt_item_tv_son:I = 0x7f080295

.field public static final tt_lite_web_back:I = 0x7f080296

.field public static final tt_lite_web_title:I = 0x7f080297

.field public static final tt_lite_web_view:I = 0x7f080298

.field public static final tt_message:I = 0x7f080299

.field public static final tt_middle_page_layout:I = 0x7f08029a

.field public static final tt_native_video_container:I = 0x7f08029b

.field public static final tt_native_video_frame:I = 0x7f08029c

.field public static final tt_native_video_img_cover:I = 0x7f08029d

.field public static final tt_native_video_img_cover_viewStub:I = 0x7f08029e

.field public static final tt_native_video_img_id:I = 0x7f08029f

.field public static final tt_native_video_layout:I = 0x7f0802a0

.field public static final tt_native_video_play:I = 0x7f0802a1

.field public static final tt_native_video_titlebar:I = 0x7f0802a2

.field public static final tt_negtive:I = 0x7f0802a3

.field public static final tt_open_app_detail_layout:I = 0x7f0802a4

.field public static final tt_personalization_layout:I = 0x7f0802a5

.field public static final tt_personalization_name:I = 0x7f0802a6

.field public static final tt_playable_ad_close:I = 0x7f0802a7

.field public static final tt_playable_ad_close_layout:I = 0x7f0802a8

.field public static final tt_playable_ad_dislike:I = 0x7f0802a9

.field public static final tt_playable_ad_mute:I = 0x7f0802aa

.field public static final tt_playable_loading:I = 0x7f0802ab

.field public static final tt_playable_pb_view:I = 0x7f0802ac

.field public static final tt_playable_play:I = 0x7f0802ad

.field public static final tt_playable_progress_tip:I = 0x7f0802ae

.field public static final tt_positive:I = 0x7f0802af

.field public static final tt_privacy_layout:I = 0x7f0802b0

.field public static final tt_privacy_list:I = 0x7f0802b1

.field public static final tt_privacy_webview:I = 0x7f0802b2

.field public static final tt_ratio_image_view:I = 0x7f0802b3

.field public static final tt_rb_score:I = 0x7f0802b4

.field public static final tt_rb_score_backup:I = 0x7f0802b5

.field public static final tt_reward_ad_appname:I = 0x7f0802b6

.field public static final tt_reward_ad_appname_backup:I = 0x7f0802b7

.field public static final tt_reward_ad_download:I = 0x7f0802b8

.field public static final tt_reward_ad_download_backup:I = 0x7f0802b9

.field public static final tt_reward_ad_download_layout:I = 0x7f0802ba

.field public static final tt_reward_ad_icon:I = 0x7f0802bb

.field public static final tt_reward_ad_icon_backup:I = 0x7f0802bc

.field public static final tt_reward_browser_webview:I = 0x7f0802bd

.field public static final tt_reward_browser_webview_playable:I = 0x7f0802be

.field public static final tt_reward_full_endcard_backup:I = 0x7f0802bf

.field public static final tt_reward_playable_loading:I = 0x7f0802c0

.field public static final tt_reward_root:I = 0x7f0802c1

.field public static final tt_rl_download:I = 0x7f0802c2

.field public static final tt_root_view:I = 0x7f0802c3

.field public static final tt_scroll_view:I = 0x7f0802c4

.field public static final tt_splash_ad_gif:I = 0x7f0802c5

.field public static final tt_splash_backup_desc:I = 0x7f0802c6

.field public static final tt_splash_backup_img:I = 0x7f0802c7

.field public static final tt_splash_backup_text:I = 0x7f0802c8

.field public static final tt_splash_backup_video_container:I = 0x7f0802c9

.field public static final tt_splash_bar_text:I = 0x7f0802ca

.field public static final tt_splash_close_btn:I = 0x7f0802cb

.field public static final tt_splash_express_container:I = 0x7f0802cc

.field public static final tt_splash_icon_close:I = 0x7f0802cd

.field public static final tt_splash_icon_image:I = 0x7f0802ce

.field public static final tt_splash_icon_video_container:I = 0x7f0802cf

.field public static final tt_splash_skip_btn:I = 0x7f0802d0

.field public static final tt_splash_video_ad_mute:I = 0x7f0802d1

.field public static final tt_splash_video_container:I = 0x7f0802d2

.field public static final tt_title:I = 0x7f0802d3

.field public static final tt_titlebar_app_detail:I = 0x7f0802d4

.field public static final tt_titlebar_app_name:I = 0x7f0802d5

.field public static final tt_titlebar_app_privacy:I = 0x7f0802d6

.field public static final tt_titlebar_back:I = 0x7f0802d7

.field public static final tt_titlebar_close:I = 0x7f0802d8

.field public static final tt_titlebar_detail_layout:I = 0x7f0802d9

.field public static final tt_titlebar_developer:I = 0x7f0802da

.field public static final tt_titlebar_dislike:I = 0x7f0802db

.field public static final tt_titlebar_title:I = 0x7f0802dc

.field public static final tt_top_dislike:I = 0x7f0802dd

.field public static final tt_top_layout_proxy:I = 0x7f0802de

.field public static final tt_top_mute:I = 0x7f0802df

.field public static final tt_top_skip:I = 0x7f0802e0

.field public static final tt_video_ad_bottom_layout:I = 0x7f0802e1

.field public static final tt_video_ad_button:I = 0x7f0802e2

.field public static final tt_video_ad_button_draw:I = 0x7f0802e3

.field public static final tt_video_ad_close_layout:I = 0x7f0802e4

.field public static final tt_video_ad_cover:I = 0x7f0802e5

.field public static final tt_video_ad_cover_center_layout:I = 0x7f0802e6

.field public static final tt_video_ad_cover_center_layout_draw:I = 0x7f0802e7

.field public static final tt_video_ad_covers:I = 0x7f0802e8

.field public static final tt_video_ad_finish_cover_image:I = 0x7f0802e9

.field public static final tt_video_ad_full_screen:I = 0x7f0802ea

.field public static final tt_video_ad_logo_image:I = 0x7f0802eb

.field public static final tt_video_ad_name:I = 0x7f0802ec

.field public static final tt_video_ad_replay:I = 0x7f0802ed

.field public static final tt_video_app_detail:I = 0x7f0802ee

.field public static final tt_video_app_detail_layout:I = 0x7f0802ef

.field public static final tt_video_app_name:I = 0x7f0802f0

.field public static final tt_video_app_privacy:I = 0x7f0802f1

.field public static final tt_video_back:I = 0x7f0802f2

.field public static final tt_video_btn_ad_image_tv:I = 0x7f0802f3

.field public static final tt_video_close:I = 0x7f0802f4

.field public static final tt_video_current_time:I = 0x7f0802f5

.field public static final tt_video_developer:I = 0x7f0802f6

.field public static final tt_video_draw_layout_viewStub:I = 0x7f0802f7

.field public static final tt_video_fullscreen_back:I = 0x7f0802f8

.field public static final tt_video_loading_cover_image:I = 0x7f0802f9

.field public static final tt_video_loading_progress:I = 0x7f0802fa

.field public static final tt_video_loading_retry:I = 0x7f0802fb

.field public static final tt_video_loading_retry_layout:I = 0x7f0802fc

.field public static final tt_video_play:I = 0x7f0802fd

.field public static final tt_video_progress:I = 0x7f0802fe

.field public static final tt_video_retry:I = 0x7f0802ff

.field public static final tt_video_retry_des:I = 0x7f080300

.field public static final tt_video_reward_bar:I = 0x7f080301

.field public static final tt_video_reward_container:I = 0x7f080302

.field public static final tt_video_seekbar:I = 0x7f080303

.field public static final tt_video_time_left_time:I = 0x7f080304

.field public static final tt_video_time_play:I = 0x7f080305

.field public static final tt_video_title:I = 0x7f080306

.field public static final tt_video_top_layout:I = 0x7f080307

.field public static final tt_video_top_title:I = 0x7f080308

.field public static final tt_video_traffic_continue_play_btn:I = 0x7f080309

.field public static final tt_video_traffic_continue_play_tv:I = 0x7f08030a

.field public static final tt_video_traffic_tip_layout:I = 0x7f08030b

.field public static final tt_video_traffic_tip_layout_viewStub:I = 0x7f08030c

.field public static final tt_video_traffic_tip_tv:I = 0x7f08030d

.field public static final tv_app_detail:I = 0x7f08030e

.field public static final tv_app_developer:I = 0x7f08030f

.field public static final tv_app_name:I = 0x7f080310

.field public static final tv_app_privacy:I = 0x7f080311

.field public static final tv_app_version:I = 0x7f080312

.field public static final tv_empty:I = 0x7f080313

.field public static final tv_give_up:I = 0x7f080314

.field public static final tv_permission_description:I = 0x7f080315

.field public static final tv_permission_title:I = 0x7f080316

.field public static final web_frame:I = 0x7f080322


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
