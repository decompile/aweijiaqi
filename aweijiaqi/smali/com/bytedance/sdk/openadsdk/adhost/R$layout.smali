.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$layout;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final notification_action:I = 0x7f0b0089

.field public static final notification_action_tombstone:I = 0x7f0b008a

.field public static final notification_template_custom_big:I = 0x7f0b008b

.field public static final notification_template_icon_group:I = 0x7f0b008c

.field public static final notification_template_part_chronometer:I = 0x7f0b008d

.field public static final notification_template_part_time:I = 0x7f0b008e

.field public static final tt_activity_full_image_mode_3_h:I = 0x7f0b0094

.field public static final tt_activity_full_image_mode_3_v:I = 0x7f0b0095

.field public static final tt_activity_full_image_model_173_h:I = 0x7f0b0096

.field public static final tt_activity_full_image_model_173_v:I = 0x7f0b0097

.field public static final tt_activity_full_image_model_33_h:I = 0x7f0b0098

.field public static final tt_activity_full_image_model_33_v:I = 0x7f0b0099

.field public static final tt_activity_full_image_model_3_178_h:I = 0x7f0b009a

.field public static final tt_activity_full_image_model_3_178_v:I = 0x7f0b009b

.field public static final tt_activity_full_image_model_3_191_h:I = 0x7f0b009c

.field public static final tt_activity_full_image_model_3_191_v:I = 0x7f0b009d

.field public static final tt_activity_full_video_default_style:I = 0x7f0b009e

.field public static final tt_activity_full_video_new_bar_style:I = 0x7f0b009f

.field public static final tt_activity_full_video_no_bar_style:I = 0x7f0b00a0

.field public static final tt_activity_lite_web_layout:I = 0x7f0b00a1

.field public static final tt_activity_middle_page:I = 0x7f0b00a2

.field public static final tt_activity_reward_and_full_endcard:I = 0x7f0b00a3

.field public static final tt_activity_reward_and_full_video_bar:I = 0x7f0b00a4

.field public static final tt_activity_reward_and_full_video_new_bar:I = 0x7f0b00a5

.field public static final tt_activity_ttlandingpage:I = 0x7f0b00a6

.field public static final tt_activity_ttlandingpage_playable:I = 0x7f0b00a7

.field public static final tt_activity_video_scroll_landingpage:I = 0x7f0b00a8

.field public static final tt_activity_videolandingpage:I = 0x7f0b00a9

.field public static final tt_adinfo_dialog_layout:I = 0x7f0b00aa

.field public static final tt_app_detail_dialog:I = 0x7f0b00ab

.field public static final tt_app_detail_full_dialog:I = 0x7f0b00ac

.field public static final tt_app_detail_full_dialog_list_head:I = 0x7f0b00ad

.field public static final tt_app_detail_listview_item:I = 0x7f0b00ae

.field public static final tt_app_privacy_dialog:I = 0x7f0b00af

.field public static final tt_appdownloader_notification_layout:I = 0x7f0b00b0

.field public static final tt_backup_ad:I = 0x7f0b00b1

.field public static final tt_backup_ad1:I = 0x7f0b00b2

.field public static final tt_backup_ad2:I = 0x7f0b00b3

.field public static final tt_backup_banner_layout1:I = 0x7f0b00b4

.field public static final tt_backup_banner_layout2:I = 0x7f0b00b5

.field public static final tt_backup_banner_layout3:I = 0x7f0b00b6

.field public static final tt_backup_draw:I = 0x7f0b00b7

.field public static final tt_backup_feed_horizontal:I = 0x7f0b00b8

.field public static final tt_backup_feed_img_group:I = 0x7f0b00b9

.field public static final tt_backup_feed_img_small:I = 0x7f0b00ba

.field public static final tt_backup_feed_vertical:I = 0x7f0b00bb

.field public static final tt_backup_feed_video:I = 0x7f0b00bc

.field public static final tt_backup_full_reward:I = 0x7f0b00bd

.field public static final tt_backup_insert_layout1:I = 0x7f0b00be

.field public static final tt_backup_insert_layout2:I = 0x7f0b00bf

.field public static final tt_backup_insert_layout3:I = 0x7f0b00c0

.field public static final tt_backup_splash:I = 0x7f0b00c1

.field public static final tt_browser_download_layout:I = 0x7f0b00c2

.field public static final tt_browser_titlebar:I = 0x7f0b00c3

.field public static final tt_browser_titlebar_for_dark:I = 0x7f0b00c4

.field public static final tt_common_download_dialog:I = 0x7f0b00c5

.field public static final tt_custom_dailog_layout:I = 0x7f0b00c6

.field public static final tt_dialog_listview_item:I = 0x7f0b00c7

.field public static final tt_dislike_comment_layout:I = 0x7f0b00c8

.field public static final tt_dislike_dialog_layout:I = 0x7f0b00c9

.field public static final tt_insert_ad_layout:I = 0x7f0b00ca

.field public static final tt_install_dialog_layout:I = 0x7f0b00cb

.field public static final tt_interaction_style_16_9_h:I = 0x7f0b00cc

.field public static final tt_interaction_style_16_9_v:I = 0x7f0b00cd

.field public static final tt_interaction_style_1_1:I = 0x7f0b00ce

.field public static final tt_interaction_style_2_3:I = 0x7f0b00cf

.field public static final tt_interaction_style_2_3_h:I = 0x7f0b00d0

.field public static final tt_interaction_style_3_2:I = 0x7f0b00d1

.field public static final tt_interaction_style_3_2_h:I = 0x7f0b00d2

.field public static final tt_interaction_style_9_16_h:I = 0x7f0b00d3

.field public static final tt_interaction_style_9_16_v:I = 0x7f0b00d4

.field public static final tt_native_video_ad_view:I = 0x7f0b00d5

.field public static final tt_native_video_img_cover_layout:I = 0x7f0b00d6

.field public static final tt_playable_loading_layout:I = 0x7f0b00d7

.field public static final tt_playable_view_layout:I = 0x7f0b00d8

.field public static final tt_splash_icon_view:I = 0x7f0b00d9

.field public static final tt_splash_view:I = 0x7f0b00da

.field public static final tt_top_reward_dislike_2:I = 0x7f0b00db

.field public static final tt_video_ad_cover_layout:I = 0x7f0b00dc

.field public static final tt_video_detail_layout:I = 0x7f0b00dd

.field public static final tt_video_draw_btn_layout:I = 0x7f0b00de

.field public static final tt_video_play_layout_for_live:I = 0x7f0b00df

.field public static final tt_video_traffic_tip:I = 0x7f0b00e0

.field public static final tt_video_traffic_tips_layout:I = 0x7f0b00e1

.field public static final ttdownloader_activity_app_detail_info:I = 0x7f0b00e2

.field public static final ttdownloader_activity_app_privacy_policy:I = 0x7f0b00e3

.field public static final ttdownloader_dialog_appinfo:I = 0x7f0b00e4

.field public static final ttdownloader_dialog_select_operation:I = 0x7f0b00e5

.field public static final ttdownloader_item_permission:I = 0x7f0b00e6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
