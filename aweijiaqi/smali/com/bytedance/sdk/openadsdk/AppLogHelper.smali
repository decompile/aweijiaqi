.class public Lcom/bytedance/sdk/openadsdk/AppLogHelper;
.super Ljava/lang/Object;
.source "AppLogHelper.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private volatile d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    const/4 v0, 0x0

    .line 32
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    return-void
.end method

.method private a()V
    .locals 2

    .line 69
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 70
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    const-string v1, "sdk_app_log_did"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 95
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getUserUniqueID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    const-string v1, "app_log_user_unique_id"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/bytedance/sdk/openadsdk/AppLogHelper;
    .locals 2

    .line 39
    sget-object v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    if-nez v0, :cond_1

    .line 40
    const-class v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    monitor-enter v0

    .line 41
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    if-nez v1, :cond_0

    .line 42
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    .line 44
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 46
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    return-object v0
.end method


# virtual methods
.method public getAppLogDid()Ljava/lang/String;
    .locals 3

    .line 54
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide v0, 0x9a7ec800L

    const-string v2, "sdk_app_log_did"

    .line 56
    invoke-static {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 60
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a()V

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getAppLogUserUniqueID()Ljava/lang/String;
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide v0, 0x9a7ec800L

    const-string v2, "app_log_user_unique_id"

    .line 82
    invoke-static {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    .line 84
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 86
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 88
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b()V

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 2

    .line 171
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const-string v0, "sdk_version_name"

    .line 176
    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized initAppLog(Landroid/content/Context;)V
    .locals 3

    monitor-enter p0

    .line 130
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_2

    .line 131
    new-instance v0, Lcom/bytedance/embedapplog/InitConfig;

    const v1, 0x2820a

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "unionser_slardar_applog"

    invoke-direct {v0, v1, v2}, Lcom/bytedance/embedapplog/InitConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    if-eqz v1, :cond_1

    .line 133
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUsePhoneState()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/InitConfig;->setImeiEnable(Z)V

    .line 134
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUsePhoneState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->getDevImei()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/InitConfig;->setAppImei(Ljava/lang/String;)V

    .line 138
    :cond_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWifiState()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/InitConfig;->setMacEnable(Z)V

    .line 140
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper$1;-><init>(Lcom/bytedance/sdk/openadsdk/AppLogHelper;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/InitConfig;->setSensitiveInfoProvider(Lcom/bytedance/embedapplog/ISensitiveInfoProvider;)V

    const/4 v1, 0x0

    .line 151
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/InitConfig;->setUriConfig(I)Lcom/bytedance/embedapplog/InitConfig;

    .line 153
    invoke-static {p1, v0}, Lcom/bytedance/embedapplog/AppLog;->init(Landroid/content/Context;Lcom/bytedance/embedapplog/InitConfig;)V

    .line 154
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/i;->a(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 155
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    .line 156
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a()V

    .line 157
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setHeaderInfo(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 164
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 165
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 167
    :cond_0
    invoke-static {p1}, Lcom/bytedance/embedapplog/AppLog;->setHeaderInfo(Ljava/util/HashMap;)V

    return-void
.end method
