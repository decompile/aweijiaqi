.class public Lcom/bytedance/sdk/openadsdk/j/g;
.super Ljava/lang/Object;
.source "VolumeChangeObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/j/g$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/bytedance/sdk/openadsdk/j/f;

.field private c:Lcom/bytedance/sdk/openadsdk/j/g$a;

.field private d:Landroid/content/Context;

.field private e:Landroid/media/AudioManager;

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 21
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->a:I

    const/4 v1, 0x0

    .line 26
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->f:Z

    .line 27
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->g:Z

    .line 28
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->h:I

    .line 31
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->d:Landroid/content/Context;

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "audio"

    .line 33
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->e:Landroid/media/AudioManager;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->h:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .line 41
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->h:I

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/j/f;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->b:Lcom/bytedance/sdk/openadsdk/j/f;

    return-void
.end method

.method public b()V
    .locals 3

    .line 125
    :try_start_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/g$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/j/g$a;-><init>(Lcom/bytedance/sdk/openadsdk/j/g;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->c:Lcom/bytedance/sdk/openadsdk/j/g$a;

    .line 126
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    .line 127
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/j/g;->c:Lcom/bytedance/sdk/openadsdk/j/g$a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    .line 129
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "VolumeChangeObserver"

    const-string v2, "registerReceiverError: "

    .line 131
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public c()V
    .locals 3

    .line 139
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->f:Z

    if-eqz v0, :cond_0

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->c:Lcom/bytedance/sdk/openadsdk/j/g$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 142
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->b:Lcom/bytedance/sdk/openadsdk/j/f;

    const/4 v0, 0x0

    .line 143
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "VolumeChangeObserver"

    const-string v2, "unregisterReceiverError: "

    .line 145
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public d()I
    .locals 4

    const/4 v0, -0x1

    .line 152
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->e:Landroid/media/AudioManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/g;->e:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return v0

    :catchall_0
    move-exception v1

    const-string v2, "VolumeChangeObserver"

    const-string v3, "getCurrentMusicVolumeError: "

    .line 154
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/j/f;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/g;->b:Lcom/bytedance/sdk/openadsdk/j/f;

    return-object v0
.end method
