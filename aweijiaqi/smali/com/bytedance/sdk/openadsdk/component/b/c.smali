.class Lcom/bytedance/sdk/openadsdk/component/b/c;
.super Lcom/bytedance/sdk/openadsdk/core/f/a;
.source "TTFeedAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTFeedAd;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$b;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;
.implements Lcom/bytedance/sdk/openadsdk/e/p;
.implements Lcom/bytedance/sdk/openadsdk/multipro/b/a$a;


# instance fields
.field a:Lcom/bytedance/sdk/openadsdk/multipro/b/a;

.field b:Z

.field c:Z

.field d:I

.field e:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field f:I

.field private n:[I

.field private o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

.field private p:Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;

.field private q:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/f/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    const/4 p1, 0x0

    .line 41
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    const/4 p1, 0x0

    .line 49
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    const/4 p2, 0x1

    .line 50
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    .line 55
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->q:Z

    .line 59
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->f:I

    .line 60
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->e:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 61
    new-instance p1, Lcom/bytedance/sdk/openadsdk/multipro/b/a;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/multipro/b/a;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->a:Lcom/bytedance/sdk/openadsdk/multipro/b/a;

    .line 62
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->d:I

    .line 63
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->a(I)V

    return-void
.end method

.method static synthetic A(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic B(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic C(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic D(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic E(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic F(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/n;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->g:Lcom/bytedance/sdk/openadsdk/core/n;

    return-object p0
.end method

.method private a(I)V
    .locals 3

    .line 136
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->d(I)I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x3

    if-ne v1, p1, :cond_0

    .line 139
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    .line 140
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne v1, p1, :cond_1

    .line 141
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    .line 143
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne v2, p1, :cond_3

    .line 145
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->e(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 146
    :cond_2
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    .line 147
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne v0, p1, :cond_4

    .line 150
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    if-ne v0, p1, :cond_6

    .line 152
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 153
    :cond_5
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    :cond_6
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/b/c;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/n;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->g:Lcom/bytedance/sdk/openadsdk/core/n;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/b/c;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->q:Z

    return p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method private i()Z
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->P()Lcom/bytedance/sdk/openadsdk/core/e/m$a;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->h()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 118
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic q(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic r(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic s(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic t(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic u(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic v(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic w(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic x(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic y(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic z(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .line 397
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 398
    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoError(II)V

    :cond_0
    return-void
.end method

.method public a(JJ)V
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 435
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onProgressUpdate(JJ)V

    :cond_0
    return-void
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/multipro/b/a;
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->a:Lcom/bytedance/sdk/openadsdk/multipro/b/a;

    return-object v0
.end method

.method public e_()V
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 408
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoAdStartPlay(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    :cond_0
    return-void
.end method

.method public f_()V
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 418
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoAdPaused(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    :cond_0
    return-void
.end method

.method public g()Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->q:Z

    return v0
.end method

.method public g_()V
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 428
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoAdContinuePlay(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    :cond_0
    return-void
.end method

.method public getAdView()Landroid/view/View;
    .locals 10

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    :try_start_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->i:Landroid/content/Context;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->f:I

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ZZLjava/lang/String;ZZ)V

    .line 78
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/b/c$1;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/b/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/b/c;)V

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setVideoAdClickListener(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$b;)V

    .line 88
    :cond_1
    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/b/c$2;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/b/c$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/b/c;)V

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setControllerStatusCallBack(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$a;)V

    .line 98
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setVideoAdLoadListener(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;)V

    .line 99
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setVideoAdInteractionListener(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$b;)V

    const/4 v2, 0x5

    .line 100
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->f:I

    if-ne v2, v3, :cond_3

    .line 101
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->b:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->e:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->isAutoPlay()Z

    move-result v2

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    :goto_0
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setIsAutoPlay(Z)V

    goto :goto_1

    .line 103
    :cond_3
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->c:Z

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setIsAutoPlay(Z)V

    .line 105
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v2

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->d:I

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setIsQuiet(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    :cond_4
    move-object v0, v1

    .line 110
    :goto_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->h()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->a(JZZ)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_3

    :cond_5
    return-object v0

    :cond_6
    :goto_3
    return-object v1
.end method

.method public getAdViewHeight()I
    .locals 4

    const/16 v0, 0x2d0

    .line 190
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    if-nez v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    array-length v1, v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    goto :goto_0

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    const/4 v2, 0x1

    aget v0, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    return v0

    :catchall_0
    move-exception v1

    const-string v2, "TTFeedAdImpl"

    const-string v3, "getAdViewHeight error"

    .line 198
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method public getAdViewWidth()I
    .locals 4

    const/16 v0, 0x500

    .line 174
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    if-nez v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    array-length v1, v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    goto :goto_0

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->n:[I

    const/4 v2, 0x0

    aget v0, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    return v0

    :catchall_0
    move-exception v1

    const-string v2, "TTFeedAdImpl"

    const-string v3, "getAdViewWidth error"

    .line 182
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method public getCustomVideo()Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->p:Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;

    if-nez v0, :cond_1

    .line 209
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/b/c$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/b/c;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->p:Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->p:Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;

    return-object v0
.end method

.method public getVideoDuration()D
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->e()D

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method h()Z
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    return v0
.end method

.method public h_()V
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 442
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoAdComplete(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    :cond_0
    return-void
.end method

.method public i_()V
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    if-eqz v0, :cond_0

    .line 391
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;->onVideoLoad(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    :cond_0
    return-void
.end method

.method public setVideoAdListener(Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c;->o:Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;

    return-void
.end method
