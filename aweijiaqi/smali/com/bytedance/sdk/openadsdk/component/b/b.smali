.class public Lcom/bytedance/sdk/openadsdk/component/b/b;
.super Lcom/bytedance/sdk/openadsdk/component/b/c;
.source "TTDrawFeedAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd;


# instance fields
.field private n:Z

.field private o:Landroid/graphics/Bitmap;

.field private p:I

.field private q:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/component/b/c;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method private a(I)Z
    .locals 3

    .line 91
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->d(I)I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x3

    if-ne v2, p1, :cond_0

    goto :goto_1

    :cond_0
    if-ne v1, p1, :cond_2

    .line 96
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x2

    if-ne v2, p1, :cond_3

    .line 99
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->e(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_3
    const/4 v2, 0x5

    if-ne v2, p1, :cond_4

    .line 103
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    :goto_1
    return v0
.end method

.method private i()V
    .locals 2

    .line 117
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->p:I

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    .line 118
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->p:I

    goto :goto_0

    :cond_0
    const/16 v1, 0x14

    if-gt v0, v1, :cond_1

    .line 120
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->p:I

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public bridge synthetic a(II)V
    .locals 0

    .line 22
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->a(II)V

    return-void
.end method

.method public bridge synthetic a(JJ)V
    .locals 0

    .line 22
    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/component/b/c;->a(JJ)V

    return-void
.end method

.method public bridge synthetic e()Lcom/bytedance/sdk/openadsdk/multipro/b/a;
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->e()Lcom/bytedance/sdk/openadsdk/multipro/b/a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic e_()V
    .locals 0

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->e_()V

    return-void
.end method

.method public bridge synthetic f_()V
    .locals 0

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->f_()V

    return-void
.end method

.method public bridge synthetic g()Z
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->g()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic g_()V
    .locals 0

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->g_()V

    return-void
.end method

.method public getAdView()Landroid/view/View;
    .locals 6

    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    if-nez v0, :cond_0

    goto :goto_1

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/b/b;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    :try_start_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->i:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 39
    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/b/b$1;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/b/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/b/b;)V

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setControllerStatusCallBack(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$a;)V

    .line 49
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setVideoAdLoadListener(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;)V

    .line 50
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setVideoAdInteractionListener(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$b;)V

    .line 51
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->h:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v2

    .line 52
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/component/b/b;->a(I)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setIsAutoPlay(Z)V

    .line 53
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setIsQuiet(Z)V

    .line 54
    move-object v2, v0

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;

    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->n:Z

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;->setCanInterruptVideoPlay(Z)V

    .line 55
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->o:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 56
    move-object v2, v0

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->o:Landroid/graphics/Bitmap;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->p:I

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeDrawVideoTsView;->a(Landroid/graphics/Bitmap;I)V

    .line 58
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->q:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->setDrawVideoListener(Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_2
    move-object v0, v1

    .line 64
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/b/b;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;->a(JZZ)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    return-object v0

    :cond_4
    :goto_1
    return-object v1
.end method

.method public bridge synthetic getAdViewHeight()I
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getAdViewHeight()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getAdViewWidth()I
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getAdViewWidth()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getCustomVideo()Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;
    .locals 1

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getCustomVideo()Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getVideoDuration()D
    .locals 2

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic h_()V
    .locals 0

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->h_()V

    return-void
.end method

.method public bridge synthetic i_()V
    .locals 0

    .line 22
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->i_()V

    return-void
.end method

.method public setCanInterruptVideoPlay(Z)V
    .locals 0

    .line 73
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->n:Z

    return-void
.end method

.method public setDrawVideoListener(Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->q:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;

    return-void
.end method

.method public setPauseIcon(Landroid/graphics/Bitmap;I)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->o:Landroid/graphics/Bitmap;

    .line 79
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/b;->p:I

    .line 80
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/b/b;->i()V

    return-void
.end method

.method public bridge synthetic setVideoAdListener(Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->setVideoAdListener(Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;)V

    return-void
.end method
