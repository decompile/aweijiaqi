.class Lcom/bytedance/sdk/openadsdk/component/b/c$3;
.super Ljava/lang/Object;
.source "TTFeedAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/b/c;->getCustomVideo()Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/b/c;

.field private b:J


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/b/c;)V
    .locals 2

    .line 209
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 210
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    return-void
.end method


# virtual methods
.method public getVideoUrl()Ljava/lang/String;
    .locals 2

    .line 214
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->c(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 215
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->d(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 216
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->e(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->f(Lcom/bytedance/sdk/openadsdk/component/b/c;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->a(Lcom/bytedance/sdk/openadsdk/component/b/c;Z)Z

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->g(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public reportVideoAutoStart()V
    .locals 9

    .line 327
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->m(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->n(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->o(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_auto_play"

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    .line 330
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->p(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->q(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;-><init>()V

    .line 332
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->r(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Ljava/lang/String;)V

    .line 333
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->s(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(I)V

    .line 334
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->t(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 335
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getFeedCacheDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->u(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->m()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 337
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->v(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 338
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->w(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(J)V

    .line 339
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->x(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const/4 v1, 0x1

    .line 340
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Z)V

    .line 341
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->y(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->z(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    :cond_0
    return-void
.end method

.method public reportVideoBreak(J)V
    .locals 8

    .line 302
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 303
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long v0, v0, v4

    .line 307
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    sub-long/2addr v4, v6

    sub-long/2addr v4, p1

    cmp-long v6, v4, v2

    if-gez v6, :cond_1

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 312
    :goto_0
    new-instance v4, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v4}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 313
    invoke-virtual {v4, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 314
    invoke-virtual {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 315
    invoke-virtual {v4, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    const/4 p1, 0x0

    .line 316
    invoke-virtual {v4, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e(I)V

    .line 317
    invoke-virtual {v4, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f(I)V

    const/4 p1, 0x1

    .line 318
    invoke-virtual {v4, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 321
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->l(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1, p2, v4}, Lcom/bytedance/sdk/openadsdk/e/a/a;->f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoContinue(J)V
    .locals 6

    .line 255
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 258
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    sub-long/2addr v0, v4

    sub-long/2addr v0, p1

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    goto :goto_0

    :cond_1
    move-wide v2, v0

    .line 263
    :goto_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 264
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 265
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide p1

    double-to-long p1, p1

    const-wide/16 v4, 0x3e8

    mul-long p1, p1, v4

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 266
    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    const/4 p1, 0x1

    .line 267
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 269
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->j(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoError(JII)V
    .locals 8

    .line 363
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 364
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long v0, v0, v4

    .line 368
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    sub-long/2addr v4, v6

    sub-long/2addr v4, p1

    cmp-long v6, v4, v2

    if-gez v6, :cond_1

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 373
    :goto_0
    new-instance v4, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v4}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 374
    invoke-virtual {v4, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 375
    invoke-virtual {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 376
    invoke-virtual {v4, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 377
    invoke-virtual {v4, p3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(I)V

    .line 378
    invoke-virtual {v4, p4}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(I)V

    const/4 p1, 0x1

    .line 379
    invoke-virtual {v4, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 380
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->F(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1, p2, v4}, Lcom/bytedance/sdk/openadsdk/e/a/a;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoFinish()V
    .locals 8

    .line 276
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 277
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long v0, v0, v4

    .line 281
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    sub-long/2addr v4, v6

    sub-long/2addr v4, v0

    cmp-long v6, v4, v2

    if-gez v6, :cond_1

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 286
    :goto_0
    new-instance v4, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v4}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 287
    invoke-virtual {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 288
    invoke-virtual {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 291
    invoke-virtual {v4, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    const/4 v0, 0x0

    .line 292
    invoke-virtual {v4, v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f(I)V

    const/4 v0, 0x1

    .line 293
    invoke-virtual {v4, v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 295
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->k(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0, v1, v4}, Lcom/bytedance/sdk/openadsdk/e/a/a;->g(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoPause(J)V
    .locals 6

    .line 236
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 237
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 239
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    sub-long/2addr v0, v4

    sub-long/2addr v0, p1

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    goto :goto_0

    :cond_1
    move-wide v2, v0

    .line 244
    :goto_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 245
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 246
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide p1

    double-to-long p1, p1

    const-wide/16 v4, 0x3e8

    mul-long p1, p1, v4

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 247
    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    const/4 p1, 0x1

    .line 248
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 249
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->i(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoStart()V
    .locals 3

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->b:J

    .line 228
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    const/4 v1, 0x1

    .line 229
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(Z)V

    .line 230
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(Z)V

    .line 231
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->h(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method public reportVideoStartError(II)V
    .locals 3

    .line 348
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 349
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->A(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    const-string v2, "creative_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "error_code"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "extra_error_code"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->B(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 354
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string v1, "video_size"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->f()Ljava/lang/String;

    move-result-object p1

    const-string p2, "video_resolution"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->C(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->D(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->E(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "play_start_error"

    invoke-static {p1, p2, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
