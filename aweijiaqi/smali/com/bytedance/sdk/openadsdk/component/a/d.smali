.class Lcom/bytedance/sdk/openadsdk/component/a/d;
.super Landroid/widget/FrameLayout;
.source "BannerView.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/openadsdk/component/a/c;

.field private c:Lcom/bytedance/sdk/openadsdk/component/a/c;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 44
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->i:Z

    .line 45
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->j:Z

    .line 49
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->g()V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/component/a/c;)Landroid/animation/ObjectAnimator;
    .locals 4

    .line 197
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->getWidth()I

    move-result v0

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v2, 0x1

    aput v0, v1, v2

    const-string v0, "translationX"

    .line 198
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/a/d;)Lcom/bytedance/sdk/openadsdk/dislike/ui/a;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->f:Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/a/d;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->f:Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 257
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aG()Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/a/d;Z)Z
    .locals 0

    .line 28
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->h:Z

    return p1
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/component/a/c;)Landroid/animation/ObjectAnimator;
    .locals 3

    .line 207
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->getWidth()I

    move-result v0

    const/4 v1, 0x2

    new-array v1, v1, [F

    int-to-float v0, v0

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v0, 0x1

    const/4 v2, 0x0

    aput v2, v1, v0

    const-string v0, "translationX"

    .line 208
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 209
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/a/d$2;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/a/d;Lcom/bytedance/sdk/openadsdk/component/a/c;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/a/d;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->k()V

    return-void
.end method

.method private g()V
    .locals 3

    .line 66
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->b:Lcom/bytedance/sdk/openadsdk/component/a/c;

    .line 67
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->i()V

    .line 69
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->h()V

    return-void
.end method

.method private h()V
    .locals 4

    .line 81
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->j:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 85
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->j:Z

    .line 86
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    .line 87
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_dislike_icon"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/a/d$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/a/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/a/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    .line 99
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    .line 100
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const v3, 0x800035

    .line 101
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 102
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 103
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 104
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/a/d;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    invoke-static {v1, v0, v0, v0, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;IIII)V

    return-void
.end method

.method private i()V
    .locals 3

    .line 111
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->i:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 114
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->i:Z

    .line 115
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->e:Landroid/widget/ImageView;

    .line 116
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_ad_logo_small"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const/16 v0, 0x14

    .line 121
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const v0, 0x800055

    .line 122
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 123
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private j()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->bringChildToFront(Landroid/view/View;)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 137
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->bringChildToFront(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private k()V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->b:Lcom/bytedance/sdk/openadsdk/component/a/c;

    .line 275
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->b:Lcom/bytedance/sdk/openadsdk/component/a/c;

    .line 276
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    .line 277
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/c;->b()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 127
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    const/16 v1, 0x8

    .line 128
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 188
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->g:I

    return-void
.end method

.method a(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->f:Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->e:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/p;->a(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 0

    .line 149
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 150
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->j()V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0

    .line 155
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 156
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->j()V

    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 0

    .line 161
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 162
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->j()V

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 167
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 168
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->j()V

    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 143
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->j()V

    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/component/a/c;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->b:Lcom/bytedance/sdk/openadsdk/component/a/c;

    return-object v0
.end method

.method public c()Lcom/bytedance/sdk/openadsdk/component/a/c;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method public e()V
    .locals 3

    .line 243
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->h:Z

    if-nez v0, :cond_0

    .line 244
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 245
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->b:Lcom/bytedance/sdk/openadsdk/component/a/c;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/a/c;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/component/a/d;->b(Lcom/bytedance/sdk/openadsdk/component/a/c;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 246
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->g:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->setVisibility(I)V

    const/4 v0, 0x1

    .line 248
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->h:Z

    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->c:Lcom/bytedance/sdk/openadsdk/component/a/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/c;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 74
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    const/4 v0, 0x0

    .line 76
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->i:Z

    .line 77
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/d;->j:Z

    return-void
.end method
