.class Lcom/bytedance/sdk/openadsdk/component/reward/c$a;
.super Lcom/bytedance/sdk/component/e/g;
.source "FullScreenVideoLoadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field b:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/component/reward/c;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/c;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->c:Lcom/bytedance/sdk/openadsdk/component/reward/c;

    const-string p1, "Fullscreen Task"

    .line 459
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 460
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 461
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 466
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 469
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 470
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 472
    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;-><init>()V

    .line 473
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 474
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Ljava/lang/String;)V

    .line 475
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->m()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(I)V

    .line 476
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(J)V

    .line 477
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->t()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Z)V

    .line 478
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getRewardFullCacheDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 480
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const-string v0, "FullScreenVideoLoadManager"

    const-string v2, "FullScreenLog: preload video "

    .line 481
    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c$a$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/c$a;)V

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/component/video/a/c/a$a;)V

    goto :goto_0

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->c:Lcom/bytedance/sdk/openadsdk/component/reward/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/c;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/a;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/reward/c$a$2;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c$a$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/c$a;)V

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/component/reward/a$a;)V

    :cond_2
    :goto_0
    return-void
.end method
