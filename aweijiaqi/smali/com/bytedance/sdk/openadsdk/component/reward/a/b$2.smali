.class Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;
.super Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;
.source "CommonEndCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->a(Landroid/webkit/DownloadListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/b;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->j()V

    .line 200
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->i()V

    .line 208
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iput p2, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->j:I

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iput-object p3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->k:Ljava/lang/String;

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 139
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 140
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 141
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 142
    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :catch_0
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 155
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 157
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 158
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 159
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 160
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :catch_0
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v1

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->j:I

    .line 167
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->k:Ljava/lang/String;

    .line 168
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 3

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 176
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 177
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 178
    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 179
    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 185
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/b;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz p3, :cond_2

    .line 188
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v1

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->j:I

    .line 189
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    const-string v1, "onReceivedHttpError"

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->k:Ljava/lang/String;

    .line 192
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V

    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 3

    .line 125
    :try_start_0
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b$2;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception v0

    const-string v1, "CommonEndCard"

    const-string v2, "shouldInterceptRequest error1"

    .line 127
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object p1

    return-object p1
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 0

    .line 118
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object p1

    return-object p1
.end method
