.class Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;
.super Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;
.source "PlayableLoadingEndCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Landroid/webkit/DownloadListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->c:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "loading_h5_success"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/bytedance/sdk/openadsdk/e/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 141
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 158
    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;Z)Z

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 0

    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 153
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;Z)Z

    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 0

    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V

    .line 147
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;Z)Z

    return-void
.end method
