.class public Lcom/bytedance/sdk/openadsdk/component/reward/a/c;
.super Lcom/bytedance/sdk/openadsdk/component/reward/a/a;
.source "PlayableEndCard.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;
    }
.end annotation


# instance fields
.field A:Lcom/bytedance/sdk/openadsdk/m/f;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Lcom/bytedance/sdk/openadsdk/j/d;

.field private E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

.field private final F:Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;

.field t:I

.field u:I

.field v:J

.field w:I

.field protected final x:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final y:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final z:Lcom/bytedance/sdk/component/utils/u;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V
    .locals 0

    .line 185
    invoke-direct/range {p0 .. p9}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    const/4 p1, 0x0

    .line 91
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->t:I

    .line 95
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->u:I

    const-wide/16 p2, 0x0

    .line 99
    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->v:J

    .line 103
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    .line 106
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 109
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 112
    new-instance p1, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->z:Lcom/bytedance/sdk/component/utils/u;

    .line 125
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$1;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->F:Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;

    .line 186
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a:Landroid/app/Activity;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a:Landroid/app/Activity;

    const-string p3, "tt_reward_browser_webview_playable"

    invoke-static {p2, p3}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 187
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a()V

    return-void
.end method

.method private E()V
    .locals 5

    .line 227
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->e(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    .line 228
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 230
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->n:I

    const/4 v2, 0x1

    const-string v3, "?"

    if-ne v1, v2, :cond_1

    .line 231
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&orientation=portrait"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    goto :goto_0

    .line 234
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "?orientation=portrait"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    .line 237
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "&aspect_ratio="

    const-string v3, "&width="

    if-eqz v1, :cond_2

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "&height="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->p:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->o:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    goto :goto_1

    .line 240
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "?height="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->p:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->o:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    :cond_3
    :goto_1
    return-void
.end method

.method private F()V
    .locals 1

    .line 424
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Lcom/bytedance/sdk/openadsdk/j/d;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->D:Lcom/bytedance/sdk/openadsdk/j/d;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Ljava/lang/String;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->F:Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;

    return-object p0
.end method


# virtual methods
.method public A()J
    .locals 4

    .line 493
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->v:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public B()V
    .locals 2

    .line 500
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->z:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    return-void
.end method

.method public C()V
    .locals 4

    .line 507
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    if-lez v0, :cond_0

    .line 508
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    .line 509
    iput v1, v0, Landroid/os/Message;->what:I

    .line 510
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 511
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->z:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method public D()Z
    .locals 1

    .line 520
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->f()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/os/Message;)V
    .locals 7

    .line 639
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    goto/16 :goto_2

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;->e()Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    move-result-object v0

    .line 644
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    .line 648
    :cond_1
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-lez p1, :cond_4

    const/4 v2, 0x1

    .line 650
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 652
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->u:I

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->t:I

    sub-int/2addr v4, p1

    sub-int/2addr v3, v4

    if-ne v3, p1, :cond_2

    .line 655
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-lez v3, :cond_3

    .line 659
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\u53ef\u5728("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "s)\u540e\u8df3\u8fc7"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 661
    :cond_3
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u8df3\u8fc7"

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 662
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    :goto_0
    sub-int/2addr p1, v2

    .line 665
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    .line 666
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    .line 667
    iput v1, p1, Landroid/os/Message;->what:I

    .line 668
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    iput v0, p1, Landroid/os/Message;->arg1:I

    .line 669
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->z:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p1, v1, v2}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    .line 671
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 673
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    if-eqz v0, :cond_5

    .line 674
    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;->a(I)V

    .line 675
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;->d()V

    .line 678
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    if-eqz p1, :cond_6

    .line 679
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;->c()V

    :cond_6
    :goto_2
    return-void
.end method

.method public a(Landroid/webkit/DownloadListener;)V
    .locals 8

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v7, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a:Landroid/app/Activity;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->i:Lcom/bytedance/sdk/openadsdk/e/j;

    move-object v1, v7

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    invoke-virtual {v0, v7}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 362
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)V

    .line 363
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setBackgroundColor(I)V

    .line 364
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 365
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$4;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->i:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-direct {v1, p0, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$4;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;Lcom/bytedance/sdk/openadsdk/core/w;Lcom/bytedance/sdk/openadsdk/e/j;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 373
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->F()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;)V
    .locals 0

    .line 485
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E:Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/j/d;)V
    .locals 1

    .line 480
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->D:Lcom/bytedance/sdk/openadsdk/j/d;

    .line 481
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->D:Lcom/bytedance/sdk/openadsdk/j/d;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/d;)Lcom/bytedance/sdk/openadsdk/core/w;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/j/e;)V
    .locals 4

    .line 529
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$5;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$5;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->a(Lcom/bytedance/sdk/openadsdk/m/e$a;)V

    .line 542
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$6;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$6;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;Lcom/bytedance/sdk/openadsdk/j/e;)V

    .line 586
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$7;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$7;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)V

    .line 593
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v1, v2, p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/m/b;Lcom/bytedance/sdk/openadsdk/m/a;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 594
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 595
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 596
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sdkEdition"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 597
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 598
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    const/4 v0, 0x0

    .line 599
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    .line 600
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    .line 601
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 602
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    .line 605
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/m/f;->j()Ljava/util/Set;

    move-result-object p1

    .line 607
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 609
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "subscribe_app_ad"

    .line 612
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "adInfo"

    .line 613
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "webview_time_track"

    .line 614
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "download_app_ad"

    .line 615
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 619
    :cond_3
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/w;->b()Lcom/bytedance/sdk/component/a/q;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$8;

    invoke-direct {v3, p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$8;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v2, v1, v3}, Lcom/bytedance/sdk/component/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/a/e;)Lcom/bytedance/sdk/component/a/q;

    goto :goto_0

    :cond_4
    return-void
.end method

.method public a(ZLjava/util/Map;Landroid/view/View;Lcom/bytedance/sdk/openadsdk/j/e;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/view/View;",
            "Lcom/bytedance/sdk/openadsdk/j/e;",
            ")V"
        }
    .end annotation

    .line 192
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/q;-><init>(ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    .line 193
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/j;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-direct {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/j;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/webkit/WebView;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->b(Z)Lcom/bytedance/sdk/openadsdk/e/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->i:Lcom/bytedance/sdk/openadsdk/e/j;

    .line 194
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->i:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Z)V

    .line 195
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->i:Lcom/bytedance/sdk/openadsdk/e/j;

    if-eqz p1, :cond_0

    const-string v1, "reward_endcard"

    goto :goto_0

    :cond_0
    const-string v1, "fullscreen_endcard"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Ljava/lang/String;)V

    .line 196
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    .line 198
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 199
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 200
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 201
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 p1, 0x7

    goto :goto_1

    :cond_1
    const/4 p1, 0x5

    .line 202
    :goto_1
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(I)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->r:Lcom/bytedance/sdk/openadsdk/j/a;

    .line 203
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/a;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 204
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->i(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 205
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    .line 206
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->c:Ljava/lang/String;

    .line 207
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    .line 208
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Ljava/util/Map;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->s:Lcom/bytedance/sdk/openadsdk/j/h;

    .line 209
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/h;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->C:Z

    .line 210
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Z)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    .line 211
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Landroid/view/View;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object p1

    .line 212
    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/e;)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 213
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g:Lcom/bytedance/sdk/openadsdk/core/w;

    new-instance p2, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$2;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)V

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/b;)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 222
    invoke-virtual {p0, p4}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Lcom/bytedance/sdk/openadsdk/j/e;)V

    .line 223
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->E()V

    return-void
.end method

.method public a(I)Z
    .locals 2

    .line 516
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w:I

    int-to-float v0, v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->t:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    const/high16 v0, 0x42c80000    # 100.0f

    mul-float v1, v1, v0

    int-to-float p1, p1

    cmpl-float p1, v1, p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Z)V
    .locals 1

    .line 525
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 405
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->d(Z)V

    .line 407
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->D:Lcom/bytedance/sdk/openadsdk/j/d;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/j/d;->c()Z

    move-result p1

    if-nez p1, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->w()V

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 489
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->C:Z

    return-void
.end method

.method public k()V
    .locals 1

    .line 389
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->k()V

    .line 390
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_0

    .line 391
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->p()V

    :cond_0
    return-void
.end method

.method public m()V
    .locals 1

    .line 397
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->m()V

    .line 398
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_0

    .line 399
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->r()V

    :cond_0
    return-void
.end method

.method public u()Z
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public v()V
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public w()V
    .locals 2

    const/4 v0, 0x1

    .line 413
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Z)V

    .line 414
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->b(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    .line 415
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->c(Z)V

    const/4 v1, 0x0

    .line 416
    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(ZZ)V

    return-void
.end method

.method public x()V
    .locals 4

    .line 439
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->j:I

    if-eqz v0, :cond_0

    .line 440
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->j:I

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    :catchall_0
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->C:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    :cond_1
    return-void
.end method

.method public y()V
    .locals 8

    .line 457
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-nez v0, :cond_0

    goto :goto_0

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->f:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/webkit/WebView;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-nez v5, :cond_2

    return-void

    .line 469
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->c:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-string v4, "playable_show_status"

    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;ZI)V

    :cond_3
    :goto_0
    return-void
.end method

.method public z()V
    .locals 1

    .line 474
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_0

    .line 475
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->q()V

    :cond_0
    return-void
.end method
