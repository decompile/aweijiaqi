.class Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;
.super Ljava/lang/Object;
.source "RewardVideoLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/component/reward/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/bytedance/sdk/openadsdk/component/reward/f$a<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/component/reward/k;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/h$1;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/component/reward/k;)V
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/Object;)V
    .locals 3

    .line 359
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "download video file: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", preload: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->a:Z

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "RewardVideoLoadManager"

    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 361
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/f;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object p2

    .line 362
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/k;

    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/k;->a(Ljava/lang/String;)V

    .line 364
    :cond_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-boolean p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->a:Z

    if-nez p2, :cond_1

    .line 365
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    if-eqz p1, :cond_2

    .line 366
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    if-eqz p1, :cond_2

    .line 367
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-wide v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->d:J

    invoke-static {p1, p2, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;J)V

    .line 368
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;->onRewardVideoCached()V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 373
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/f;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$3;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    :cond_2
    :goto_0
    return-void
.end method
