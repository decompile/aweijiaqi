.class Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;
.super Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;
.source "PlayableEndCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Landroid/webkit/DownloadListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->j()V

    .line 349
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->d(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->i()V

    .line 358
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 359
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->d(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$a;->a(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 283
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 284
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iput p2, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->j:I

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iput-object p3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->k:Ljava/lang/String;

    .line 286
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 288
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 289
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 290
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 291
    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :catch_0
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 304
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 306
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 307
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 308
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 309
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :catch_0
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v1

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->j:I

    .line 316
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->k:Ljava/lang/String;

    .line 317
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 3

    .line 323
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_1

    .line 325
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 326
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    const-string v1, "code"

    .line 327
    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "msg"

    .line 328
    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 334
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->c(Lcom/bytedance/sdk/openadsdk/component/reward/a/c;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz p3, :cond_2

    .line 337
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {p3}, Landroid/webkit/WebResourceResponse;->getStatusCode()I

    move-result v1

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->j:I

    .line 338
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    const-string v1, "onReceivedHttpError"

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->k:Ljava/lang/String;

    .line 341
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V

    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 3

    .line 274
    :try_start_0
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception v0

    const-string v1, "PlayableEndCard"

    const-string v2, "shouldInterceptRequest error1"

    .line 276
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object p1

    return-object p1
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 8

    .line 250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 252
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->j()Ljava/lang/String;

    move-result-object p1

    .line 253
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->k()Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a()Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    invoke-virtual {v1, v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object p1

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 256
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_2

    .line 257
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e$a;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    const/4 v7, 0x2

    .line 259
    :goto_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e$a;->a:Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e$a;

    if-ne v0, v1, :cond_1

    .line 260
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    move-object v2, p2

    invoke-virtual/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Ljava/lang/String;JJI)V

    goto :goto_1

    .line 261
    :cond_1
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e$a;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/e$a;

    if-ne v0, v1, :cond_2

    .line 262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->h:Lcom/bytedance/sdk/openadsdk/e/q;

    move-object v2, p2

    invoke-virtual/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/e/q;->b(Ljava/lang/String;JJI)V

    :cond_2
    :goto_1
    return-object p1

    .line 267
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object p1

    return-object p1
.end method
