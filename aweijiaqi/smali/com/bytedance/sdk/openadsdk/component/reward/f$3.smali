.class Lcom/bytedance/sdk/openadsdk/component/reward/f$3;
.super Ljava/lang/Object;
.source "RewardVideoCache.java"

# interfaces
.implements Lcom/bytedance/sdk/component/adnet/b/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/component/reward/f$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/component/reward/f$a;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/component/reward/f;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/f;Ljava/io/File;Lcom/bytedance/sdk/openadsdk/component/reward/f$a;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->d:Lcom/bytedance/sdk/openadsdk/component/reward/f;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/f$a;

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .line 326
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    .line 327
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(JJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/adnet/core/m;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/adnet/core/m<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 351
    iget-object v1, p1, Lcom/bytedance/sdk/component/adnet/core/m;->a:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/f$a;

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    .line 353
    invoke-interface {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/f$a;->a(ZLjava/lang/Object;)V

    .line 356
    :cond_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->d:Lcom/bytedance/sdk/openadsdk/component/reward/f;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-wide/16 v6, 0x0

    move-object v8, p1

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/component/reward/f;ZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    goto :goto_1

    .line 358
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/f$a;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 359
    invoke-interface {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/f$a;->a(ZLjava/lang/Object;)V

    .line 362
    :cond_2
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->d:Lcom/bytedance/sdk/openadsdk/component/reward/f;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez p1, :cond_3

    const-wide/16 v0, -0x3

    goto :goto_0

    :cond_3
    iget-wide v0, p1, Lcom/bytedance/sdk/component/adnet/core/m;->h:J

    :goto_0
    move-wide v6, v0

    move-object v8, p1

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/component/reward/f;ZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    :goto_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 335
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->d:Lcom/bytedance/sdk/openadsdk/component/reward/f;

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/component/reward/f;Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/io/File;
    .locals 0

    .line 341
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->a:Ljava/io/File;

    return-object p1
.end method

.method public b(Lcom/bytedance/sdk/component/adnet/core/m;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/adnet/core/m<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 368
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->b:Lcom/bytedance/sdk/openadsdk/component/reward/f$a;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 369
    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/f$a;->a(ZLjava/lang/Object;)V

    .line 372
    :cond_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->d:Lcom/bytedance/sdk/openadsdk/component/reward/f;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/f$3;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez p1, :cond_1

    const-wide/16 v0, -0x2

    goto :goto_0

    :cond_1
    iget-wide v0, p1, Lcom/bytedance/sdk/component/adnet/core/m;->h:J

    :goto_0
    move-wide v6, v0

    move-object v8, p1

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/component/reward/f;ZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    return-void
.end method
