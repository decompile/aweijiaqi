.class public Lcom/bytedance/sdk/openadsdk/component/reward/b/e;
.super Ljava/lang/Object;
.source "RewardFullWebViewManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/component/reward/b/e$a;,
        Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

.field private final g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

.field private h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

.field private i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

.field private j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

.field private k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

.field private l:Z

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 74
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    const/4 v0, 0x0

    .line 82
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->n:Z

    .line 83
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a:Landroid/app/Activity;

    .line 84
    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method private a(J)V
    .locals 4

    .line 561
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f()V

    .line 562
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    div-int/lit16 v0, v0, 0x3e8

    const/4 v1, 0x0

    if-lez v0, :cond_0

    .line 564
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 565
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    .line 566
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 567
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    int-to-long v0, v0

    sub-long/2addr v0, p1

    long-to-int v1, v0

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    .line 568
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/b/b;->a(J)V

    goto :goto_0

    .line 570
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    :goto_0
    return-void
.end method

.method private b(I)I
    .locals 2

    .line 493
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v1, :cond_0

    .line 494
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->o(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 495
    :cond_0
    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_1

    .line 496
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->p(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Z
    .locals 0

    .line 32
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l:Z

    return p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Lcom/bytedance/sdk/openadsdk/component/reward/a/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Lcom/bytedance/sdk/openadsdk/component/reward/a/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Lcom/bytedance/sdk/openadsdk/core/video/b/b;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)Lcom/bytedance/sdk/openadsdk/component/reward/b/c;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    return-object p0
.end method


# virtual methods
.method public A()V
    .locals 1

    .line 460
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->C()V

    return-void
.end method

.method public B()V
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->z()V

    return-void
.end method

.method public C()V
    .locals 2

    .line 472
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    .line 478
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 481
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/b/b;->a(I)V

    goto :goto_0

    :cond_1
    if-ltz v0, :cond_2

    .line 483
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/b;->a(I)V

    .line 484
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    const-wide/16 v0, 0x0

    .line 485
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(J)V

    :cond_2
    :goto_0
    return-void
.end method

.method public D()V
    .locals 2

    const/4 v0, 0x1

    .line 506
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l:Z

    .line 507
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_1

    .line 508
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->u()V

    goto :goto_0

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->d(Z)V

    .line 514
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v0, :cond_2

    .line 515
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    :cond_2
    return-void
.end method

.method public E()V
    .locals 2

    const/4 v0, 0x0

    .line 520
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l:Z

    .line 521
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    if-eqz v1, :cond_0

    .line 522
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->e(Z)V

    .line 523
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->x()V

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_1

    .line 526
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->e()V

    .line 527
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->b(Z)V

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v0, :cond_2

    .line 530
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    :cond_2
    return-void
.end method

.method public F()Z
    .locals 1

    .line 535
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    return v0
.end method

.method public G()V
    .locals 4

    .line 542
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 545
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    .line 546
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 547
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b()Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v2

    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x7

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    .line 548
    :goto_0
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/c;->a(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    .line 549
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    .line 550
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    .line 551
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 552
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 553
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/k/a;->l(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method public H()Z
    .locals 1

    .line 557
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l:Z

    return v0
.end method

.method public I()Z
    .locals 1

    .line 575
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 578
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->t()Z

    move-result v0

    return v0
.end method

.method public J()V
    .locals 2

    const-wide/16 v0, 0x3e8

    .line 582
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(J)V

    return-void
.end method

.method public a()V
    .locals 2

    const-string v0, "RewardFullWebViewManager"

    const-string v1, "tryLoadEndCard"

    .line 223
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->v()V

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->u()V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->v()V

    :cond_1
    return-void
.end method

.method public a(II)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->a(II)V

    :cond_0
    return-void
.end method

.method a(IIIF)V
    .locals 22

    move-object/from16 v0, p0

    .line 101
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a:Landroid/app/Activity;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c:Ljava/lang/String;

    iget-boolean v10, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    move-object v2, v1

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v2 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    .line 103
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v13, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a:Landroid/app/Activity;

    iget-object v14, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v15, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    move-object v12, v1

    move/from16 v16, p1

    move/from16 v17, p2

    move/from16 v18, p3

    move/from16 v19, p4

    move/from16 v20, v2

    move-object/from16 v21, v3

    invoke-direct/range {v12 .. v21}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    .line 110
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 111
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 112
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a:Landroid/app/Activity;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c:Ljava/lang/String;

    iget-boolean v10, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    move-object v2, v1

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v2 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    goto :goto_0

    .line 116
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    iget-object v13, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a:Landroid/app/Activity;

    iget-object v14, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v15, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    move-object v12, v1

    move/from16 v16, p1

    move/from16 v17, p2

    move/from16 v18, p3

    move/from16 v19, p4

    move/from16 v20, v2

    move-object/from16 v21, v3

    invoke-direct/range {v12 .. v21}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    .line 119
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    :goto_1
    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    return-void
.end method

.method public a(JJI)V
    .locals 6

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    move-wide v1, p1

    move-wide v3, p3

    move v5, p5

    .line 128
    invoke-virtual/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->a(JJI)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/component/reward/b/c;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->n:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 91
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->n:Z

    .line 92
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 93
    iput-boolean p8, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    .line 94
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c:Ljava/lang/String;

    .line 95
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    .line 96
    iput-object p9, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    .line 97
    invoke-virtual {p0, p4, p5, p6, p7}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(IIIF)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/b/e;)V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/b/e;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->b(Z)V

    :cond_0
    return-void
.end method

.method public a(ZILjava/lang/String;)V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {v0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->a(ZILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(ZLcom/bytedance/sdk/openadsdk/j/e;Landroid/webkit/DownloadListener;)V
    .locals 4

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v1, :cond_1

    .line 156
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(ZLjava/util/Map;Landroid/view/View;Lcom/bytedance/sdk/openadsdk/j/e;)V

    .line 157
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v3, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(ZLjava/util/Map;Landroid/view/View;Lcom/bytedance/sdk/openadsdk/j/e;)V

    .line 158
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Landroid/webkit/DownloadListener;)V

    .line 159
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Landroid/webkit/DownloadListener;)V

    .line 161
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->e(Z)V

    .line 162
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->w()Lcom/bytedance/sdk/openadsdk/j/d;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Lcom/bytedance/sdk/openadsdk/j/d;)V

    .line 163
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)V

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/e$b;)V

    .line 205
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)V

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/e$a;)V

    .line 212
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz p1, :cond_2

    .line 213
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d:Z

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v1, v0, v2, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->a(ZLjava/util/Map;Landroid/view/View;Lcom/bytedance/sdk/openadsdk/j/e;)V

    .line 214
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->a(Landroid/webkit/DownloadListener;)V

    :cond_2
    return-void
.end method

.method public a(I)Z
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->a(I)Z

    move-result p1

    return p1
.end method

.method public b()V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->j()V

    .line 238
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    if-ltz v0, :cond_1

    .line 239
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/b;->a(I)V

    const-wide/16 v0, 0x0

    .line 240
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(J)V

    :cond_1
    return-void
.end method

.method public c()V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->k()V

    .line 248
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m:I

    if-ltz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->g:Lcom/bytedance/sdk/openadsdk/core/video/b/b;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/b;->m()V

    :cond_1
    return-void
.end method

.method public d()V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->l()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->m()V

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_1

    .line 264
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->m()V

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->m()V

    :cond_2
    return-void
.end method

.method public f()V
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->n()V

    :cond_0
    return-void
.end method

.method public g()I
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->h()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public i()V
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->o()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 299
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->p()V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->q()V

    :cond_0
    return-void
.end method

.method public l()V
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->r()V

    :cond_0
    return-void
.end method

.method public m()V
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->s()V

    :cond_0
    return-void
.end method

.method public n()V
    .locals 3

    .line 331
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->C()V

    .line 332
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 333
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(F)V

    .line 334
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aM()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 335
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    goto :goto_0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 340
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->d()V

    .line 341
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->d(Z)V

    return-void
.end method

.method public o()V
    .locals 1

    .line 348
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->e()V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->f()V

    :cond_0
    return-void
.end method

.method public q()Z
    .locals 3

    .line 368
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    const-string v2, "RewardFullWebViewManager"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->a()I

    move-result v0

    if-ne v0, v1, :cond_0

    const-string v0, "can show end card follow js"

    .line 369
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->r()Z

    move-result v0

    goto :goto_0

    :cond_0
    const-string v0, "can show end card follow js WebViewClient"

    .line 372
    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->s()Z

    move-result v0

    :goto_0
    if-nez v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    return v1
.end method

.method public r()Z
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->b()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/a;->c()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->D()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public u()V
    .locals 2

    .line 412
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->e(Z)V

    .line 414
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->v()V

    .line 415
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->e()V

    :cond_0
    return-void
.end method

.method public v()V
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k:Lcom/bytedance/sdk/openadsdk/component/reward/a/a;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->x()V

    :cond_0
    return-void
.end method

.method public w()V
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->y()V

    return-void
.end method

.method public x()V
    .locals 1

    .line 440
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->h:Lcom/bytedance/sdk/openadsdk/component/reward/a/b;

    if-eqz v0, :cond_0

    .line 441
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/b;->g()V

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    if-eqz v0, :cond_1

    .line 444
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->g()V

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    if-eqz v0, :cond_2

    .line 447
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->g()V

    :cond_2
    return-void
.end method

.method public y()J
    .locals 2

    .line 452
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->A()J

    move-result-wide v0

    return-wide v0
.end method

.method public z()V
    .locals 1

    .line 456
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i:Lcom/bytedance/sdk/openadsdk/component/reward/a/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/c;->B()V

    return-void
.end method
