.class public Lcom/bytedance/sdk/openadsdk/component/reward/g;
.super Lcom/bytedance/sdk/openadsdk/core/video/b/a;
.source "RewardVideoController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const/4 p1, 0x0

    .line 23
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a(Z)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(II)V
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    return-void

    .line 36
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 38
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 39
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 40
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(I)V

    .line 41
    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(I)V

    .line 42
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->w()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    move-result-object p2

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected b()V
    .locals 3

    .line 51
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 52
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 53
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 54
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 55
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f(I)V

    .line 56
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->g(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected c()V
    .locals 3

    .line 61
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 62
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 63
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 64
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 65
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected d()V
    .locals 3

    .line 70
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 71
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 72
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 73
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 75
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/g;->w()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected e()V
    .locals 3

    .line 80
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    const/4 v1, 0x1

    .line 81
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(Z)V

    .line 82
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected f()V
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->j:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    return-void
.end method

.method protected g()V
    .locals 3

    .line 93
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    const/4 v1, 0x1

    .line 94
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(Z)V

    .line 95
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/g;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method
