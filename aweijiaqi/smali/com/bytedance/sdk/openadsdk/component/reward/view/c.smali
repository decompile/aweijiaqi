.class public Lcom/bytedance/sdk/openadsdk/component/reward/view/c;
.super Ljava/lang/Object;
.source "RewardFullVideoLayout.java"


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/widget/RelativeLayout;

.field c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/FrameLayout;

.field j:Landroid/widget/FrameLayout;

.field k:Landroid/widget/FrameLayout;

.field l:Z

.field protected m:I

.field protected final n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private o:Landroid/app/Activity;

.field private p:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Z

.field private t:Z

.field private u:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 95
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->u:I

    const/4 v0, 0x1

    .line 98
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->l:Z

    const/4 v0, 0x0

    .line 100
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->m:I

    .line 102
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 105
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->t:Z

    .line 106
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    return-void
.end method

.method private b(Z)Ljava/lang/String;
    .locals 2

    .line 425
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x4

    if-eqz p1, :cond_2

    .line 429
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result p1

    if-ne p1, v1, :cond_1

    const-string p1, "\u4e0b\u8f7d"

    goto :goto_0

    :cond_1
    const-string p1, "\u67e5\u770b"

    :goto_0
    return-object p1

    .line 431
    :cond_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result p1

    if-ne p1, v1, :cond_3

    const-string p1, "Install"

    goto :goto_1

    :cond_3
    const-string p1, "View"

    :goto_1
    return-object p1
.end method

.method private g()V
    .locals 3

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_reward_root"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a:Landroid/view/View;

    .line 128
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_video_reward_bar"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_reward_ad_icon"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    .line 130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_reward_ad_appname"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_comment_vertical"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    .line 132
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_reward_ad_download"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_rb_score"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 135
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setStarEmptyNum(I)V

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setStarFillNum(I)V

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setStarImageWidth(F)V

    .line 138
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setStarImageHeight(F)V

    .line 139
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setStarImagePadding(F)V

    .line 140
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->a()V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_ad_logo"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h:Landroid/widget/TextView;

    .line 143
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_video_reward_container"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_click_upper_non_content_layout"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_click_lower_non_content_layout"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    return-void
.end method

.method private h()V
    .locals 3

    .line 239
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->t()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->u:I

    const/16 v1, -0xc8

    if-ne v0, v1, :cond_0

    .line 241
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->k(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->u:I

    .line 244
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->u:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->l:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 245
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    :cond_1
    return-void
.end method

.method private i()V
    .locals 15

    .line 341
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 344
    invoke-static {v0, v0}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v0

    const v1, 0x3f266666    # 0.65f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 345
    invoke-static {v1, v2}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v1

    const v3, 0x3f43d70a    # 0.765f

    const v4, 0x3f666666    # 0.9f

    .line 346
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    const v4, 0x3f6147ae    # 0.88f

    .line 347
    invoke-static {v4, v2}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    const v5, 0x3f733333    # 0.95f

    .line 348
    invoke-static {v5, v5}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 349
    invoke-static {v2, v2}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    const/4 v6, 0x6

    new-array v7, v6, [Landroid/animation/Keyframe;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v9, 0x1

    aput-object v1, v7, v9

    const/4 v10, 0x2

    aput-object v3, v7, v10

    const/4 v11, 0x3

    aput-object v4, v7, v11

    const/4 v12, 0x4

    aput-object v5, v7, v12

    const/4 v13, 0x5

    aput-object v2, v7, v13

    const-string v14, "scaleX"

    .line 351
    invoke-static {v14, v7}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    new-array v6, v6, [Landroid/animation/Keyframe;

    aput-object v0, v6, v8

    aput-object v1, v6, v9

    aput-object v3, v6, v10

    aput-object v4, v6, v11

    aput-object v5, v6, v12

    aput-object v2, v6, v13

    const-string v0, "scaleY"

    .line 352
    invoke-static {v0, v6}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 353
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    new-array v2, v10, [Landroid/animation/PropertyValuesHolder;

    aput-object v7, v2, v8

    aput-object v0, v2, v9

    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    .line 354
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 355
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 7

    .line 367
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 372
    :try_start_0
    sget-object v3, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v3, 0x1

    .line 373
    :goto_1
    :try_start_1
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_0
    const/4 v3, 0x1

    :catchall_1
    const/4 v0, 0x0

    :goto_2
    const-string v4, "\u4e0b\u8f7d"

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_3
    if-eqz v0, :cond_4

    const-string v4, "Install"

    .line 383
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v5, :cond_5

    return-object v4

    .line 388
    :cond_5
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 389
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_b

    if-eqz v3, :cond_6

    const-string v4, "\u67e5\u770b"

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_b

    const-string v4, "View"

    goto :goto_4

    .line 397
    :cond_7
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 399
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->k(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-le v5, v6, :cond_9

    if-eqz v3, :cond_8

    .line 401
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_8
    if-eqz v0, :cond_b

    .line 403
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 405
    :cond_9
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->k(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x7

    if-le v5, v6, :cond_b

    if-eqz v3, :cond_a

    .line 407
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_a
    if-eqz v0, :cond_b

    .line 409
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Z)Ljava/lang/String;

    move-result-object v4

    :cond_b
    :goto_4
    if-eqz v0, :cond_c

    .line 415
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->k(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 416
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 417
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 418
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_c
    return-object v4
.end method

.method private k()V
    .locals 5

    .line 455
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 458
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->l:Z

    if-nez v0, :cond_1

    const/4 v0, 0x4

    .line 459
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x2

    .line 462
    :try_start_0
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->r:I

    if-ne v2, v1, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 463
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 464
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v4, 0x425c0000    # 55.0f

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 465
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 466
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 468
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 469
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v4, 0x41400000    # 12.0f

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 470
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    .line 474
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 475
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 476
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v0

    .line 477
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 478
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    mul-int/lit8 v0, v0, 0x9

    .line 479
    div-int/lit8 v0, v0, 0x10

    .line 480
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 481
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 483
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;)I

    move-result v2

    sub-int/2addr v2, v0

    div-int/2addr v2, v1

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->m:I

    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NonContentAreaHeight:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RewardFullVideoLayout"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/e/m;)I
    .locals 3

    .line 598
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v1, "tt_activity_full_video_default_style"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 599
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 611
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v0, "tt_activity_full_video_new_bar_style"

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 605
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v0, "tt_activity_full_video_no_bar_style"

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 606
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 607
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    invoke-static {p1, v1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 601
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    invoke-static {p1, v1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    :cond_3
    :goto_0
    return v0
.end method

.method public a()Landroid/widget/RelativeLayout;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method public a(IZ)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 195
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v1, 0x43190000    # 153.0f

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0

    .line 199
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    .line 200
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v1, 0x43ca0000    # 404.0f

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    :cond_1
    :goto_0
    if-nez p2, :cond_2

    const/4 p1, 0x0

    .line 204
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    :cond_2
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    const-string v1, "TTBaseVideoActivity#mRlDownloadBar"

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V
    .locals 3

    .line 523
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 525
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->e:Z

    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Landroid/view/View$OnClickListener;)V

    .line 527
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 529
    :cond_0
    invoke-virtual {p0, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Landroid/view/View$OnClickListener;)V

    .line 532
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 533
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->a:Z

    if-eqz v0, :cond_1

    .line 534
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Landroid/view/View$OnClickListener;)V

    .line 535
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 537
    :cond_1
    invoke-virtual {p0, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 541
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->c:Z

    if-eqz v0, :cond_3

    .line 542
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c(Landroid/view/View$OnClickListener;)V

    .line 543
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 546
    :cond_3
    invoke-virtual {p0, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c(Landroid/view/View$OnClickListener;)V

    .line 551
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 552
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->f:Z

    if-eqz v0, :cond_5

    .line 553
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 555
    :cond_5
    invoke-virtual {p0, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d(Landroid/view/View$OnClickListener;)V

    .line 559
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v0

    if-ne v0, v1, :cond_a

    .line 560
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_8

    .line 561
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 562
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 563
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->m:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 564
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 566
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->b:Z

    if-eqz v0, :cond_7

    .line 567
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_3

    .line 570
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 575
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_a

    .line 576
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 577
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 578
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->m:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 579
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 581
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->l()Lcom/bytedance/sdk/openadsdk/core/e/e;

    move-result-object v0

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/e;->d:Z

    if-eqz v0, :cond_9

    .line 582
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-virtual {p3, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 583
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_4

    .line 585
    :cond_9
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-virtual {p1, p3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    :goto_4
    return-void
.end method

.method public a(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    const-string v1, "TTBaseVideoActivity#mRlDownloadBar"

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View$OnTouchListener;Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 268
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 269
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 270
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->t:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 113
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->t:Z

    .line 114
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 115
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->q:Ljava/lang/String;

    .line 116
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->r:I

    .line 117
    iput-boolean p4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->s:Z

    .line 118
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g()V

    .line 119
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k()V

    .line 120
    invoke-virtual {p0, p5}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b()V

    .line 153
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    .line 154
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c()V

    .line 155
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(Ljava/lang/String;)V

    .line 156
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->r:I

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->s:Z

    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(IZ)V

    .line 157
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f()V

    .line 158
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->s:Z

    if-eqz p1, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h()V

    .line 161
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aH()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/r/p;->a(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 318
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->l:Z

    return-void
.end method

.method public b()V
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/g/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/d/e;->a(Landroid/widget/ImageView;)Lcom/bytedance/sdk/component/d/d;

    goto :goto_0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v2, "tt_ad_logo_small"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;->setImageResource(I)V

    .line 180
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 181
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->r:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ah()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public b(I)V
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public b(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 230
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public c()V
    .locals 4

    .line 212
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x1ad6

    .line 216
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 217
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->f()I

    move-result v0

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v2, "tt_comment_num"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2710

    if-le v0, v2, :cond_2

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    div-int/2addr v0, v2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\u4e07"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 221
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 222
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public c(I)V
    .locals 2

    .line 440
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-ne p1, v0, :cond_0

    .line 441
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_0

    .line 442
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 p1, 0x0

    .line 443
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    .line 444
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i()V

    :cond_0
    return-void
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    const-string v1, "TTBaseVideoActivity#mRlDownloadBar"

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    return-void
.end method

.method public c(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    const-string v1, "TTBaseVideoActivity#mRlDownloadBar"

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View$OnTouchListener;Ljava/lang/String;)V

    return-void
.end method

.method public d()V
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 326
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->j:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 327
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->k:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 328
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 329
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 330
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 331
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 332
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g:Lcom/bytedance/sdk/openadsdk/core/widget/TTRatingBar;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 333
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->f:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 334
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method public d(Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    const-string v1, "TTBaseVideoActivity#mVideoNativeFrame"

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    return-void
.end method

.method public e()Landroid/widget/FrameLayout;
    .locals 1

    .line 490
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->i:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public f()V
    .locals 7

    .line 497
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->p:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const-string v1, "#0070FF"

    .line 498
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 499
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v3, 0x41880000    # 17.0f

    invoke-static {v1, v3}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    const-string v3, "#80000000"

    .line 500
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 501
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v4

    .line 503
    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    invoke-direct {v5}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;-><init>()V

    aget v6, v0, v2

    .line 504
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v5

    .line 505
    invoke-virtual {v5, v3}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v3

    .line 506
    invoke-virtual {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->a([I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v0

    .line 507
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v0

    .line 508
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v0

    .line 509
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/widget/g$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/widget/g$a;

    move-result-object v0

    .line 510
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->o:Landroid/app/Activity;

    const-string v2, "tt_reward_ad_download_layout"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 511
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/g;->a(Landroid/view/View;Lcom/bytedance/sdk/openadsdk/core/widget/g$a;)V

    :cond_0
    return-void
.end method
