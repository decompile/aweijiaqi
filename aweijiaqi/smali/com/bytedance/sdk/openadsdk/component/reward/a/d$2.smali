.class Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;
.super Ljava/lang/Object;
.source "PlayableLoadingEndCard.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/j/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->w()Lcom/bytedance/sdk/openadsdk/j/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)V
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 211
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->f(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->h(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->t:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 220
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->t:Lcom/bytedance/sdk/component/utils/u;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    return-void
.end method

.method public a(I)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->f(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a:Landroid/app/Activity;

    .line 237
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->c(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->c(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 227
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 228
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->f(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 229
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->g(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->t:Lcom/bytedance/sdk/component/utils/u;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->a(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method

.method public c()Z
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->c(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/a/d$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/a/d;->c(Lcom/bytedance/sdk/openadsdk/component/reward/a/d;)Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
