.class Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;
.super Lcom/bytedance/sdk/component/video/a/c/b;
.source "RewardVideoLoadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/e/x;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/h$1;JLcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/x;)V
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->a:J

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->c:Lcom/bytedance/sdk/openadsdk/core/e/x;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/video/a/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 12

    const-string p1, "RewardVideoLoadManager"

    const-string v0, "RewardVideoLog: onVideoPreloadSuccess"

    .line 323
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->a:Z

    if-nez v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;->onRewardVideoCached()V

    const-string v0, "RewardVideoLog: onRewardVideoCached"

    .line 329
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->a:J

    sub-long v9, v0, v2

    .line 333
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    int-to-long v7, p2

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;ZLcom/bytedance/sdk/openadsdk/core/e/m;JJLjava/lang/String;)V

    goto :goto_0

    .line 335
    :cond_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/f;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const-string p2, "RewardVideoLog: ad json save"

    .line 336
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    .locals 12

    const-string p1, "RewardVideoLoadManager"

    const-string v0, "RewardVideoLog: onVideoPreloadFail"

    .line 344
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->a:J

    sub-long v9, v0, v2

    .line 346
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object v4

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    int-to-long v7, p2

    const/4 v5, 0x0

    move-object v11, p3

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;ZLcom/bytedance/sdk/openadsdk/core/e/m;JJLjava/lang/String;)V

    .line 347
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->c:Lcom/bytedance/sdk/openadsdk/core/e/x;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/x;->s()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 348
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/h$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/h$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {p2}, Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;->onRewardVideoCached()V

    const-string p2, "RewardVideoLog:  onVideoPreloadFail and exec onRewardVideoCached"

    .line 349
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
