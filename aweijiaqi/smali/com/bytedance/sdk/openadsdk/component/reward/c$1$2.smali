.class Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;
.super Lcom/bytedance/sdk/component/video/a/c/b;
.source "FullScreenVideoLoadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/e/x;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/c$1;JLcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/x;)V
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->a:J

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->c:Lcom/bytedance/sdk/openadsdk/core/e/x;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/video/a/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 12

    const-string p1, "FullScreenVideoLoadManager"

    const-string v0, "FullScreenLog:  onVideoPreloadSuccess"

    .line 309
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->a:Z

    if-nez v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;->onFullScreenVideoCached()V

    .line 314
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->a:J

    sub-long v9, v0, v2

    .line 315
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/c;)Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    int-to-long v7, p2

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Landroid/content/Context;ZLcom/bytedance/sdk/openadsdk/core/e/m;JJLjava/lang/String;)V

    const-string p2, "FullScreenLog: onFullScreenVideoCached"

    .line 316
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/c;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/c;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/a;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const-string p2, "FullScreenLog:  ad json save"

    .line 320
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    .locals 12

    .line 327
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->a:J

    sub-long v9, v0, v2

    .line 328
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->e:Lcom/bytedance/sdk/openadsdk/component/reward/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/c;)Landroid/content/Context;

    move-result-object v4

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    int-to-long v7, p2

    const/4 v5, 0x0

    move-object v11, p3

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/a;->a(Landroid/content/Context;ZLcom/bytedance/sdk/openadsdk/core/e/m;JJLjava/lang/String;)V

    const-string p1, "FullScreenVideoLoadManager"

    const-string p2, "FullScreenLog:  onVideoPreloadFail"

    .line 329
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->c:Lcom/bytedance/sdk/openadsdk/core/e/x;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/x;->s()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 332
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c$1$2;->d:Lcom/bytedance/sdk/openadsdk/component/reward/c$1;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/c$1;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    invoke-interface {p2}, Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;->onFullScreenVideoCached()V

    const-string p2, "FullScreenLog:  onVideoPreloadFail and exec onFullScreenVideoCached"

    .line 333
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
