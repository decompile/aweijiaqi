.class public Lcom/bytedance/sdk/openadsdk/component/reward/c/d;
.super Lcom/bytedance/sdk/openadsdk/component/reward/c/a;
.source "RewardFullTypeInteraction.java"


# instance fields
.field private l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;IIIF)V
    .locals 0

    .line 22
    invoke-direct/range {p0 .. p6}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;-><init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;IIIF)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/reward/c/d;)Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    return-object p0
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result p0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float p0, p0, v1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private f()Z
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ap()I

    move-result v0

    const/16 v2, 0xf

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method


# virtual methods
.method public a()Lcom/bytedance/sdk/openadsdk/component/reward/c/a$a;
    .locals 1

    .line 42
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/c/d;)V

    return-object v0
.end method

.method public a(Landroid/widget/FrameLayout;)V
    .locals 9

    .line 27
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    .line 28
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->h:Lcom/bytedance/sdk/openadsdk/core/b/e;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;->setDownloadListener(Lcom/bytedance/sdk/openadsdk/core/b/e;)V

    .line 29
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v5, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f:F

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->e:I

    iget v7, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->c:I

    iget v8, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->d:I

    const/4 v4, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;FIII)V

    .line 30
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;->getInteractionStyleRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/component/reward/view/c;)V
    .locals 3

    const/16 v0, 0x8

    .line 71
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    .line 72
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    .line 73
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 75
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 76
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 77
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 78
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    goto :goto_1

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aM()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 81
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 82
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 83
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->i:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f()Z

    move-result v1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    :goto_1
    return-void
.end method

.method public b()Z
    .locals 1

    .line 61
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .line 66
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->f()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->j:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->g:Ljava/lang/String;

    return-void
.end method

.method public e()Landroid/widget/FrameLayout;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->l:Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullInteractionStyleView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
