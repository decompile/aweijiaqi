.class Lcom/bytedance/sdk/openadsdk/component/reward/h$a;
.super Lcom/bytedance/sdk/component/e/g;
.source "RewardVideoLoadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field b:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/component/reward/h;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/h;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 475
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    const-string p1, "Reward Task"

    .line 476
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 477
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 478
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 483
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 486
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 487
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 491
    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;-><init>()V

    .line 492
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 493
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Ljava/lang/String;)V

    .line 494
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->m()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(I)V

    .line 495
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(J)V

    .line 496
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->t()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Z)V

    .line 497
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getRewardFullCacheDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 499
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 500
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/h$a$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/h$a;)V

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/component/video/a/c/a$a;)V

    goto :goto_0

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->c:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/reward/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/reward/h$a$2;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/h$a$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/reward/h$a;)V

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/component/reward/f$a;)V

    :cond_2
    :goto_0
    return-void
.end method
