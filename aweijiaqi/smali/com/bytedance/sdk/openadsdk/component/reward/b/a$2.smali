.class Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;
.super Ljava/lang/Object;
.source "RewardFullDownloadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;)V
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5

    const/4 p5, 0x1

    if-eq p1, p5, :cond_0

    return p5

    :cond_0
    if-eqz p2, :cond_e

    .line 262
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_e

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_2

    :cond_1
    const-string p1, "rewarded_video"

    .line 266
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    const-string v0, "click_start"

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 267
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->b:Landroid/view/View;

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;->a(Landroid/view/View;)V

    .line 268
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iput-object v1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->b:Landroid/view/View;

    return p5

    :cond_2
    const-string p2, "fullscreen_interstitial_ad"

    .line 270
    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->a:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->b:Landroid/view/View;

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;->a(Landroid/view/View;)V

    .line 273
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iput-object v1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->b:Landroid/view/View;

    return p5

    .line 275
    :cond_3
    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    const-string v0, "click_play_open"

    const-string v2, "click_open"

    if-nez p2, :cond_5

    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    const-string p1, "rewarded_video_landingpage"

    .line 290
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->h(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-eqz p1, :cond_a

    .line 291
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->g(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->h(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    invoke-static {p1, p2, p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return p5

    :cond_5
    :goto_0
    const/4 p1, -0x1

    .line 277
    invoke-virtual {p4}, Ljava/lang/String;->hashCode()I

    move-result p2

    const v3, -0x4d5dae82

    const/4 v4, 0x2

    if-eq p2, v3, :cond_8

    const v3, -0x2e50b15f

    if-eq p2, v3, :cond_7

    const v2, 0x6442087f

    if-eq p2, v2, :cond_6

    goto :goto_1

    :cond_6
    const-string p2, "click_pause"

    invoke-virtual {p4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p1, 0x0

    goto :goto_1

    :cond_7
    invoke-virtual {p4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p1, 0x2

    goto :goto_1

    :cond_8
    const-string p2, "click_continue"

    invoke-virtual {p4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p1, 0x1

    :cond_9
    :goto_1
    if-eqz p1, :cond_d

    if-eq p1, p5, :cond_c

    if-eq p1, v4, :cond_b

    :cond_a
    return p5

    .line 285
    :cond_b
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->g(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->h(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    invoke-static {p1, p2, p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return p5

    .line 282
    :cond_c
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->g(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->h(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    const-string p4, "click_play_continue"

    invoke-static {p1, p2, p3, p4, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return p5

    .line 279
    :cond_d
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->g(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a$2;->b:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->h(Lcom/bytedance/sdk/openadsdk/component/reward/b/a;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    const-string p4, "click_play_pause"

    invoke-static {p1, p2, p3, p4, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_e
    :goto_2
    return p5
.end method
