.class public Lcom/bytedance/sdk/openadsdk/component/splash/d;
.super Lcom/bytedance/sdk/openadsdk/core/video/b/a;
.source "SplashVideoController.java"


# instance fields
.field private l:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    const/4 p1, 0x0

    .line 49
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->l:Z

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected a(II)V
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x134

    if-ne p1, v0, :cond_1

    return-void

    .line 40
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 41
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 42
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 43
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 44
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(I)V

    .line 45
    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(I)V

    .line 46
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->w()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    move-result-object p2

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected b()V
    .locals 3

    .line 53
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->l:Z

    .line 57
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 58
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 59
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 60
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    .line 61
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f(I)V

    .line 63
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->g(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 3

    .line 81
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    const/4 v1, 0x1

    .line 82
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(Z)V

    .line 83
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method

.method protected f()V
    .locals 4

    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->j:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    return-void
.end method

.method protected g()V
    .locals 3

    .line 96
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    const/4 v1, 0x1

    .line 97
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(Z)V

    .line 98
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    return-void
.end method
