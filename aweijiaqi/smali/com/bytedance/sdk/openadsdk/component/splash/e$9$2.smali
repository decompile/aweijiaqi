.class Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;
.super Ljava/lang/Object;
.source "TTSplashAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/e$9;)V
    .locals 0

    .line 835
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 838
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 843
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    .line 845
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c(Lcom/bytedance/sdk/openadsdk/component/splash/e;Z)Z

    goto :goto_0

    .line 847
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 848
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->m()V

    .line 852
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 853
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;->onAdTimeOver()V

    .line 857
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 858
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->B()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 859
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b(Z)V

    .line 862
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_4

    .line 863
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b()V

    .line 865
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->t(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 866
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_5
    return-void
.end method

.method public c()V
    .locals 1

    .line 876
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9$2;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;->a:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->u(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    return-void
.end method
