.class public Lcom/bytedance/sdk/openadsdk/component/splash/TsView;
.super Landroid/widget/FrameLayout;
.source "TsView.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

.field private c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/FrameLayout;

.field private f:Landroid/widget/FrameLayout;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/RelativeLayout;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

.field private l:I

.field private m:I

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 63
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    .line 64
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a()V

    return-void
.end method

.method private a()V
    .locals 3

    .line 81
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v1, "tt_splash_view"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_ad_gif"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    .line 84
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_skip_btn"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    .line 85
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_video_ad_mute"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->d:Landroid/widget/ImageView;

    .line 86
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_video_container"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->e:Landroid/widget/FrameLayout;

    .line 87
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_express_container"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->f:Landroid/widget/FrameLayout;

    .line 88
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_ad_logo"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->g:Landroid/widget/ImageView;

    .line 90
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_full_splash_bar_layout"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    .line 91
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_bar_text"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->i:Landroid/widget/TextView;

    .line 92
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a:Landroid/content/Context;

    const-string v2, "tt_splash_close_btn"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->j:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method a(IIIII)V
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/16 p1, 0x8

    .line 203
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void

    :cond_1
    const/4 p1, 0x0

    .line 206
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 207
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->l:I

    .line 208
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->m:I

    .line 209
    iput p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->n:I

    .line 210
    iput p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->o:I

    return-void
.end method

.method getCountDownView()Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    return-object v0
.end method

.method getDislikeView()Landroid/view/View;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    return-object v0
.end method

.method getFullClickBarView()Landroid/view/View;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method getVideoContainer()Landroid/widget/FrameLayout;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->e:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 179
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 180
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 254
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 255
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;)I

    move-result p1

    .line 256
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getHeight()I

    move-result p2

    if-lt p2, p1, :cond_0

    .line 260
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->n:I

    goto :goto_0

    .line 263
    :cond_0
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->o:I

    .line 266
    :goto_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {p2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/widget/FrameLayout$LayoutParams;

    if-nez p2, :cond_1

    return-void

    .line 271
    :cond_1
    iget p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->m:I

    add-int/lit16 p3, p3, 0x96

    .line 273
    iget p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->l:I

    if-gt p4, p3, :cond_2

    .line 274
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->l:I

    :cond_2
    if-gtz p1, :cond_3

    const/4 p1, 0x0

    .line 279
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p3

    iget p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->m:I

    int-to-float p4, p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result p3

    iput p3, p2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 280
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p3

    iget p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->l:I

    int-to-float p4, p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result p3

    iput p3, p2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 281
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p3

    int-to-float p1, p1

    invoke-static {p3, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result p1

    iput p1, p2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    const/16 p1, 0x51

    .line 282
    iput p1, p2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 283
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, p2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method setADlogoLongClickContent(Ljava/lang/String;)V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->g:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/p;->a(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method setAdlogoViewVisibility(I)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->g:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method setClickBarDesc(Ljava/lang/String;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method setClickBarViewVisibility(I)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->h:Landroid/widget/RelativeLayout;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method setCloseViewListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method setCloseViewVisibility(I)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->j:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method setCountDownTime(I)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;->setCountDownTime(I)V

    :cond_0
    return-void
.end method

.method setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 161
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method setExpressView(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 104
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->k:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 105
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 106
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->k:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->k:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 108
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->f:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->k:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const/4 p1, 0x0

    .line 109
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setExpressViewVisibility(I)V

    const/16 p1, 0x8

    .line 110
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setVideoViewVisibility(I)V

    .line 111
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setImageViewVisibility(I)V

    return-void
.end method

.method setExpressViewVisibility(I)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->f:Landroid/widget/FrameLayout;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method setGifView([B)V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 166
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;->a([BZ)V

    return-void
.end method

.method setImageViewVisibility(I)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->b:Lcom/bytedance/sdk/openadsdk/core/widget/gif/GifView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    const-string p1, "\u4e0d\u5141\u8bb8\u5728Splash\u5e7f\u544a\u4e2d\u6ce8\u518cOnClickListener"

    .line 215
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/n;->a(Ljava/lang/String;)V

    return-void
.end method

.method final setOnClickListenerInternal(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 230
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    const-string p1, "\u4e0d\u5141\u8bb8\u5728Splash\u5e7f\u544a\u4e2d\u6ce8\u518cOnTouchListener"

    .line 220
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/n;->a(Ljava/lang/String;)V

    return-void
.end method

.method final setOnTouchListenerInternal(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .line 225
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method setSkipIconVisibility(I)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method final setSkipListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->c:Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/TTCountdownView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method setVideoViewVisibility(I)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->e:Landroid/widget/FrameLayout;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->d:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method setVideoVoiceVisibility(I)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->d:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method final setVoiceViewImageResource(I)V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method

.method final setVoiceViewListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
