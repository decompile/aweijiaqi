.class public Lcom/bytedance/sdk/openadsdk/component/splash/b;
.super Ljava/lang/Object;
.source "SplashAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/component/splash/b$a;
    }
.end annotation


# static fields
.field private static l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/bytedance/sdk/openadsdk/component/splash/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/core/e/t;

.field private B:Z

.field private C:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private D:I

.field private E:Lcom/bytedance/sdk/openadsdk/l/a/d;

.field private F:I

.field private G:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private H:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private I:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

.field private a:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private b:Lcom/bytedance/sdk/openadsdk/core/e/n;

.field private c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

.field private final d:Lcom/bytedance/sdk/openadsdk/core/p;

.field private e:Landroid/content/Context;

.field private final f:Lcom/bytedance/sdk/component/utils/u;

.field private final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private j:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private k:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

.field private n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private p:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private q:Z

.field private r:J

.field private s:J

.field private t:J

.field private final u:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final v:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private w:Lcom/bytedance/sdk/openadsdk/core/e/a;

.field private x:Lcom/bytedance/sdk/openadsdk/core/e/a;

.field private y:Lcom/bytedance/sdk/openadsdk/k/a/c;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 92
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 114
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 115
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 116
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v2, 0x0

    .line 124
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:J

    .line 127
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 145
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:I

    .line 150
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:Z

    const/4 v0, 0x4

    .line 160
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->F:I

    .line 1070
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1072
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz p1, :cond_0

    .line 167
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    .line 169
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 170
    new-instance p1, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    .line 171
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    .line 172
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->t()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->q:Z

    .line 173
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I
    .locals 0

    .line 72
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->F:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;J)J
    .locals 0

    .line 72
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->r:J

    return-wide p1
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/b;
    .locals 1

    .line 162
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 8

    .line 1404
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1406
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1407
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->c(Z)V

    .line 1410
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/e;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v4

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    const-string v7, "splash_ad"

    move-object v2, v0

    move-object v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    .line 1411
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1412
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    :cond_2
    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/a;)Lcom/bytedance/sdk/openadsdk/core/e/a;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->x:Lcom/bytedance/sdk/openadsdk/core/e/a;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/e/n;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    return-object p0
.end method

.method private a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 441
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 442
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p2

    .line 444
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 446
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "req_id"

    .line 447
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    goto :goto_0

    :cond_0
    move-object p2, v0

    move-object v1, p2

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 454
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->a:Ljava/lang/String;

    .line 457
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b()Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v2

    .line 458
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    .line 459
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->a(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 460
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    if-eqz v1, :cond_3

    .line 463
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_3
    if-eqz p2, :cond_4

    .line 467
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_4
    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p0

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getBidAdm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(I)V
    .locals 4

    .line 317
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->g(I)I

    move-result v0

    if-ge v0, p1, :cond_3

    if-gtz v0, :cond_0

    goto :goto_0

    .line 321
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    return-void

    .line 325
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    if-nez v1, :cond_2

    return-void

    :cond_2
    sub-int/2addr p1, v0

    const/4 v0, 0x4

    int-to-long v2, p1

    .line 329
    invoke-virtual {v1, v0, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    :cond_3
    :goto_0
    return-void
.end method

.method private declared-synchronized a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 8

    monitor-enter p0

    const/16 v0, 0x3a9c

    const/16 v1, 0x3a9b

    const/16 v2, 0x3a99

    const/4 v3, 0x1

    const/16 v4, 0x3a98

    if-ne p1, v4, :cond_0

    :try_start_0
    const-string v5, "splashLoadAd"

    .line 1088
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u5b9e\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto/16 :goto_5

    :cond_0
    if-ne p1, v2, :cond_1

    const-string v5, "splashLoadAd"

    .line 1091
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u7f13\u5b58\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_1
    if-ne p1, v0, :cond_2

    const-string v5, "splashLoadAd"

    .line 1094
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u8d85\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    .line 1095
    iput v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->F:I

    goto :goto_0

    :cond_2
    if-ne p1, v1, :cond_3

    const-string v5, "splashLoadAd"

    .line 1098
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u6e32\u67d3\u8d85\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    :cond_3
    :goto_0
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback mSplashAdHasLoad==true \u5df2\u6210\u529f\u56de\u8c03\uff0c\u4e0d\u518d\u6267\u884c\u56de\u8c03\u64cd\u4f5c\uff01\uff01\uff01"

    .line 1102
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1103
    monitor-exit p0

    return-void

    :cond_4
    if-nez p4, :cond_11

    .line 1108
    :try_start_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_6

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_5

    goto :goto_1

    :cond_5
    const-string p3, "splashLoadAd"

    const-string p4, "\u666e\u901a\u7c7b\u578b\u8d70\u8fd9\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1168
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    :goto_1
    if-ne p1, v4, :cond_a

    .line 1112
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_7

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u5b9e\u65f6\u5148\u56de\uff0c\u7f13\u5b58\u8fd8\u6ca1\u56de\uff09...\u7b49\u5f85\u7f13\u5b58"

    .line 1113
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114
    monitor-exit p0

    return-void

    :cond_7
    :try_start_2
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u7f13\u5b58\u5148\u56de\uff0c\u5b9e\u65f6\u540e\u56de\uff09...."

    .line 1116
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_9

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_8

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_9

    :cond_8
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->I:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    if-eqz p3, :cond_9

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u7f13\u5b58\u5148\u56de\uff0c\u5b9e\u65f6\u540e\u56de\uff09....\u5c1d\u8bd5\u56de\u8c03\u7f13\u5b58\u6210\u529f\uff01"

    .line 1119
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const-string p2, "cache_ad"

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Ljava/lang/String;)V

    .line 1122
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->I:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1123
    monitor-exit p0

    return-void

    .line 1125
    :cond_9
    :try_start_3
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_10

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_10

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5f53\u4e3a\u6a21\u7248\u5e7f\u544a\uff0c\u5b58\u5728\u7f13\u5b58\u7684\u60c5\u51b5\u4e0b\uff0c\u6a21\u7248\u6e32\u67d3\u8fd8\u6ca1\u6709\u56de\u6765\u65f6\u7b49\u5f85\u6a21\u7248\u5e7f\u544a\u6e32\u67d3\u56de\u6765\uff01"

    .line 1127
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1128
    monitor-exit p0

    return-void

    :cond_a
    if-ne p1, v2, :cond_c

    .line 1136
    :try_start_4
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_b

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u7f13\u5b58\u8bf7\u6c42\u5931\u8d25(\u7f13\u5b58\u5148\u56de\u6765)\uff0c\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u8c03....\u7b49\u5f85...\uff01"

    .line 1137
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1138
    monitor-exit p0

    return-void

    :cond_b
    :try_start_5
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u7f13\u5b58\u8bf7\u6c42\u5931\u8d25(\u7f13\u5b58\u5148\u56de\u6765)\uff0c\u5b9e\u65f6\u4e5f\u5931\u8d25....\u76f4\u63a5\u56de\u8c03\u51fa\u53bb\uff01"

    .line 1140
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1144
    :cond_c
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_10

    if-ne p1, v0, :cond_10

    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0cREQUEST_TYPE_TIMEOUT--->>>>"

    .line 1145
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_f

    .line 1148
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_10

    const-string p3, "splashLoadAd"

    .line 1149
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u5b9e\u65f6\u662f\u5426\u6e32\u67d3\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " \u7f13\u5b58\u6e32\u67d3\u662f\u5426\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_e

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_d

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_e

    :cond_d
    const-string p1, "splashLoadAd"

    const-string p2, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u7f13\u5b58\u6210\u529f\u76f4\u63a5\u56de\u8c03"

    .line 1151
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const-string p2, "cache_ad"

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Ljava/lang/String;)V

    .line 1153
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->I:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1154
    monitor-exit p0

    return-void

    :cond_e
    :try_start_6
    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1158
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_f
    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u7f13\u5b58\u4e5f\u5931\u8d25\u76f4\u63a5\u56de\u8c03\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1164
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    :cond_10
    :goto_2
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(I)V

    .line 1171
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1172
    monitor-exit p0

    return-void

    :cond_11
    if-nez p5, :cond_12

    if-eqz p3, :cond_12

    if-eqz p4, :cond_12

    .line 1179
    :try_start_7
    new-instance p5, Lcom/bytedance/sdk/openadsdk/l/a/d;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/q;->b()[B

    move-result-object p3

    invoke-direct {p5, p3}, Lcom/bytedance/sdk/openadsdk/l/a/d;-><init>([B)V

    .line 1180
    invoke-virtual {p4, p5}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    .line 1184
    :cond_12
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_14

    if-ne p1, v2, :cond_14

    .line 1185
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_13

    const-string p3, "splashLoadAd"

    .line 1187
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "\u5982\u679c\u7f13\u5b58\u5148\u56de\u6765,\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u6765\uff0c\u5b9e\u65f6\u662f\u5426\u6e32\u67d3\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " \u7f13\u5b58\u6e32\u67d3\u662f\u5426\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    const-string p1, "splashLoadAd"

    const-string p3, "\u7f13\u5b58\u8d4b\u503c\u7ed9resultTemp"

    .line 1189
    invoke-static {p1, p3}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->I:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const-string p1, "splashLoadAd"

    const-string p2, "\u5982\u679c\u7f13\u5b58\u5148\u56de\u6765,\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u6765\uff0c\u7b49\u5f85\u5b9e\u65f6\u56de\u6765"

    .line 1191
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1192
    monitor-exit p0

    return-void

    .line 1197
    :cond_13
    :try_start_8
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->G:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_14

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p3, :cond_14

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b()Z

    move-result p3

    if-eqz p3, :cond_14

    const-string p1, "splashLoadAd"

    const-string p2, "\u6b64\u65f6\u5b9e\u65f6\u53ef\u80fd\u5728\u6e32\u67d3\uff0c\u907f\u514d\u7f13\u5b58\u7684\u6e32\u67d3\u6210\u529f\u540e\u76f4\u63a5\u88ab\u56de\u8c03"

    .line 1198
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1199
    monitor-exit p0

    return-void

    :cond_14
    if-ne p1, v4, :cond_15

    :try_start_9
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u5b9e\u65f6\u6216\u6e32\u67d3\u6210\u529f\u56de\u8c03......\uff01"

    .line 1204
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    :cond_15
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_16

    if-ne p1, v4, :cond_16

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_16

    const-string p1, "splashLoadAd"

    const-string p2, "\u4f18\u5148\u5b9e\u65f6\uff0c\u4e14\u5b9e\u65f6\u7c7b\u578b\u5e7f\u544a\u56fe\u7247\u52a0\u8f7d\u6210\u529f\uff1b\u5982\u679c\u662f\u6a21\u677f\u5e7f\u544a\u4e14\u6e32\u67d3\u672a\u6210\u529f\uff0c\u5219\u7ee7\u7eed\u7b49\u5f85"

    .line 1208
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1209
    monitor-exit p0

    return-void

    :cond_16
    :try_start_a
    const-string p3, "splashLoadAd"

    .line 1212
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "splashAdTryCallback..\uff08 \u662f\u5426\u7f13\u5b58\u5df2check\u6210\u529f\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " || \u662f\u5426\u6e32\u67d3\u8d85\u65f6\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p5, 0x0

    if-ne p1, v1, :cond_17

    const/4 v0, 0x1

    goto :goto_3

    :cond_17
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " || \u662f\u5426\u4e3a\u5b9e\u65f6\u8bf7\u6c42"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne p1, v4, :cond_18

    goto :goto_4

    :cond_18
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, "\uff09 && \uff08\u662f\u5426\u5b9e\u65f6\u6a21\u7248\u6e32\u67d3\u5b8c\u6210\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1214
    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " || \u662f\u5426\u7f13\u5b58\u6a21\u7248\u6e32\u67d3\u5b8c\u6210\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, "\uff09"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 1212
    invoke-static {p3, p4}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_19

    if-eq p1, v4, :cond_19

    if-ne p1, v1, :cond_1b

    :cond_19
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_1a

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_1b

    .line 1217
    :cond_1a
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(I)V

    .line 1218
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1221
    :cond_1b
    monitor-exit p0

    return-void

    :goto_5
    monitor-exit p0

    throw p1
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tryDisplaySplashAdFromCache rit "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "splashLoadAd"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;I)V

    invoke-virtual {v0, p2, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/splash/a$c;)V

    goto :goto_0

    .line 425
    :cond_0
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 3

    if-nez p2, :cond_0

    return-void

    .line 756
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->O()Ljava/lang/String;

    move-result-object v0

    .line 757
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 760
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/b;

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/l/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->L()I

    move-result p2

    .line 762
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$9;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b$9;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V

    const/4 v2, 0x1

    invoke-static {p1, v1, p2, v0, v2}, Lcom/bytedance/sdk/openadsdk/r/f;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/l/b;ILcom/bytedance/sdk/openadsdk/r/f$a;Z)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 5

    const-string v0, "splashLoadAd"

    const-string v1, "try LoadSplashAdFromNetwork......"

    .line 533
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 535
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    .line 539
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/j/h;->i(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 541
    :cond_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    const/4 v3, 0x2

    iput v3, v2, Lcom/bytedance/sdk/openadsdk/core/e/n;->e:I

    .line 543
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    instance-of v3, v2, Lcom/bytedance/sdk/openadsdk/core/q;

    if-eqz v3, :cond_2

    .line 544
    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/e/t;)V

    .line 546
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    new-instance v4, Lcom/bytedance/sdk/openadsdk/component/splash/b$7;

    invoke-direct {v4, p0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$7;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    invoke-interface {v2, p1, v3, v0, v4}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;ILcom/bytedance/sdk/openadsdk/core/p$b;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v0, "splashLoadAd"

    const-string v1, "onCallback ......"

    .line 1465
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i()V

    if-nez p1, :cond_0

    const-string p1, "splashAdListener is null, then return"

    .line 1468
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    .line 1469
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1470
    monitor-exit p0

    return-void

    .line 1473
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    const/4 v1, 0x3

    if-nez v0, :cond_2

    const-string v0, "splashAdListener is null, then return"

    .line 1474
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    .line 1475
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    if-ne v0, v1, :cond_1

    .line 1476
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 1478
    :cond_1
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1479
    monitor-exit p0

    return-void

    .line 1482
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_7

    .line 1483
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1493
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    const/4 v4, 0x2

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_4

    if-eq v0, v1, :cond_3

    const/4 p1, -0x2

    .line 1517
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onError(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "splashLoadAd"

    const-string v1, "onCallback CALLBACK_RESULT_TIMEOUT"

    .line 1511
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1512
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 1513
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onTimeout()V

    goto :goto_0

    :cond_4
    const-string v0, "splashLoadAd"

    const-string v1, "onCallback CALLBACK_RESULT_FAILED"

    .line 1501
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$3;

    const-string v1, "onCallback"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    .line 1508
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    iget v1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->b:I

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->c:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onError(ILjava/lang/String;)V

    goto :goto_0

    .line 1496
    :cond_5
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 1497
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->d:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onSplashAdLoad(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V

    .line 1498
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 1521
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    if-eqz p1, :cond_6

    .line 1523
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 1524
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 1527
    :cond_6
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 1528
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1529
    monitor-exit p0

    return-void

    .line 1485
    :cond_7
    :try_start_3
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 1486
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    if-ne v0, v1, :cond_8

    .line 1487
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 1489
    :cond_8
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1490
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILjava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;I)V
    .locals 0

    .line 72
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;I)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 12

    const-string v0, "splashLoadAd"

    const-string v1, " SplashUtils preLoadImage"

    .line 625
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->b(Lcom/bytedance/sdk/openadsdk/core/e/a;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 631
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 632
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_1

    goto :goto_1

    .line 635
    :cond_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/e/l;

    .line 636
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v9

    .line 637
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u5f00\u5c4f\u52a0\u8f7d\u7684\u56fe\u7247\u94fe\u63a5 url "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/l;->b()I

    move-result v0

    .line 641
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v4

    const/4 v10, 0x1

    if-eqz v4, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    .line 642
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:J

    .line 643
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:J

    if-eqz v6, :cond_3

    const/4 v2, 0x2

    .line 646
    :cond_3
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;I)V

    .line 648
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/l/e;->e()Lcom/bytedance/sdk/openadsdk/l/a/b;

    move-result-object v2

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/t;)V

    .line 649
    new-instance v2, Lcom/bytedance/sdk/openadsdk/l/b;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/l;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v9, v3}, Lcom/bytedance/sdk/openadsdk/l/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    new-instance v11, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;

    move-object v3, v11

    move-object v4, p0

    move-object v5, p1

    move-object v7, v1

    move-object v8, p3

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/a;ZLcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/k/a/c;Ljava/lang/String;)V

    invoke-static {p2, v2, v0, v11, v10}, Lcom/bytedance/sdk/openadsdk/r/f;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/l/b;ILcom/bytedance/sdk/openadsdk/r/f$a;Z)V

    .line 748
    invoke-direct {p0, p2, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 10

    const-string v0, "splashLoadAd"

    const-string v1, "\u6267\u884c checkAdFromServer \u68c0\u6d4b\u7f13\u5b58...."

    .line 1021
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;)Z

    move-result v1

    const/4 v2, 0x4

    if-eqz v1, :cond_0

    .line 1026
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    .line 1027
    invoke-direct {p0, v2, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v6

    .line 1029
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    move-result-object v1

    .line 1030
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v2

    .line 1031
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    new-instance v9, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;

    move-object v3, v9

    move-object v4, p0

    move-object v5, p2

    move-object v7, p1

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/k/a/c;Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)V

    .line 1030
    invoke-interface {v1, v2, v0, v9}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/p$a;)V

    goto :goto_0

    :cond_0
    const-string p2, "checkAdFromServer check fail !!!!"

    .line 1055
    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e()Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p1, "checkAdFromServer check fail !!!! ---> tryLoadSplashAdFromNetwork !!!"

    .line 1057
    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 1061
    invoke-direct {p0, v2, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v9

    const-string p2, "checkAdFromServer check fail !!!! ---> return callback !!!"

    .line 1062
    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x3a99

    .line 1064
    new-instance p2, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 7

    monitor-enter p0

    if-nez p2, :cond_0

    .line 985
    monitor-exit p0

    return-void

    .line 987
    :cond_0
    :try_start_0
    new-instance v6, Lcom/bytedance/sdk/openadsdk/component/splash/b$12;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b$12;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/k/a/c;Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->renderExpressAd(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1016
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;I)V
    .locals 8

    monitor-enter p0

    if-nez p2, :cond_0

    .line 939
    monitor-exit p0

    return-void

    .line 941
    :cond_0
    :try_start_0
    new-instance v7, Lcom/bytedance/sdk/openadsdk/component/splash/b$11;

    move-object v0, v7

    move-object v1, p0

    move v2, p5

    move-object v3, p2

    move-object v4, p4

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$11;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/k/a/c;Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)V

    invoke-virtual {p2, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->renderExpressAd(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 979
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 10

    const-string v0, "SplashAdLoadManager"

    const-string v1, "\u7f51\u7edc\u8bf7\u6c42\u7684\u5e7f\u544a\u89e3\u6790\u5931\u8d25"

    .line 615
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x3

    .line 616
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    .line 617
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 618
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x2

    const/4 v6, -0x3

    const/4 v8, 0x0

    move-object v3, v1

    move-object v4, p0

    move-object v9, p1

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 p1, 0x1

    .line 619
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->F:I

    const/16 v4, 0x3a98

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move-object v5, v1

    .line 620
    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 6

    if-eqz p1, :cond_5

    .line 1286
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1289
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1290
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->i()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpl-double v5, v1, v3

    if-nez v5, :cond_1

    .line 1291
    array-length v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x44800000    # 1024.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 1292
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(D)V

    .line 1294
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1295
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "X"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1297
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->b(Ljava/lang/String;)V

    .line 1300
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->e()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1301
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->k()Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1302
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1303
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1304
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 1308
    :cond_3
    :try_start_0
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 1310
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SplashAdLoadManager"

    invoke-static {v3, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1313
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Lorg/json/JSONObject;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .line 475
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b$6;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/splash/a$c;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/l/a/d;)Lcom/bytedance/sdk/openadsdk/l/a/d;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/l/a/d;

    return-object p1
.end method

.method private b()V
    .locals 5

    .line 259
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    if-nez v0, :cond_0

    return-void

    .line 262
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(I)I

    move-result v0

    .line 263
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(I)V

    const-string v1, "splashLoadAd"

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    const/4 v3, 0x1

    if-eq v0, v3, :cond_3

    const/4 v4, 0x2

    if-eq v0, v4, :cond_2

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    goto :goto_0

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 295
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splash_type_concurrent_first_come_first_use=====\u5e76\u53d1\u8bf7\u6c42\u5e7f\u544a\u548ccheck\u7f13\u5b58\uff0c\u8c01\u5148\u5230\u7528\u8c01"

    .line 296
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 298
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    :goto_0
    return-void

    :cond_2
    const-string v0, "splash_type_concurrent_priority_real_time=====\u5e76\u53d1\u8bf7\u6c42\u5e7f\u544a\u548ccheck\u7f13\u5b58\uff0c\u4f18\u5148\u4f7f\u7528\u5b9e\u65f6"

    .line 286
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 288
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 289
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 290
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    return-void

    .line 273
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 274
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58"

    .line 275
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58--->\u6267\u884c\u7f13\u5b58\u5931\u8d25\uff0c\u8fdb\u884c\u7f51\u7edc\u8bf7\u6c42"

    .line 278
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    goto :goto_1

    :cond_4
    const-string v0, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58--->\u6267\u884c\u7f13\u5b58\u6210\u529f\uff01\uff01"

    .line 282
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 267
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splash_type_real_time=====\u53ea\u8d70\u5b9e\u65f6"

    .line 269
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method private b(I)V
    .locals 1

    .line 1250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x3a98

    if-ne p1, v0, :cond_1

    .line 1254
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1256
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const-string v0, "real_time_ad"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1259
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const-string v0, "cache_ad"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 15

    move-object v11, p0

    move-object/from16 v5, p1

    const-string v0, "splashLoadAd"

    const-string v1, " SplashUtils preLoadVideo"

    .line 786
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    invoke-static/range {p1 .. p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->b(Lcom/bytedance/sdk/openadsdk/core/e/a;)Z

    move-result v1

    if-eqz v1, :cond_8

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 791
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 792
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v7

    .line 793
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    const/4 v2, 0x2

    .line 799
    :cond_2
    invoke-static {v6, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;I)V

    if-eqz v1, :cond_7

    .line 801
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 802
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v12

    .line 803
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SplashUtils preLoadVideo videoUrl "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v12, :cond_3

    .line 806
    invoke-direct/range {p0 .. p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void

    .line 810
    :cond_3
    iput-object v5, v11, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/core/e/a;

    move-object/from16 v10, p3

    .line 811
    iput-object v10, v11, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 813
    invoke-static {v6, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;I)V

    .line 815
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v1

    .line 816
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v12}, Lcom/bytedance/sdk/component/utils/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    move-object v4, v1

    .line 818
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aN()Z

    move-result v2

    .line 820
    invoke-static {v4, v7, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Ljava/lang/String;IZ)Ljava/io/File;

    move-result-object v3

    .line 824
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/bytedance/sdk/openadsdk/core/j/h;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 825
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz v4, :cond_5

    .line 828
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "\u975ewifi\u73af\u5883\uff0c\u5df2\u7f13\u5b58\u76f8\u540curl\u7684\u89c6\u9891\u6587\u4ef6\u4e5f\u662f\u53ef\u4ee5\u64ad\u653e\u7684"

    .line 829
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/e/q;

    const/4 v2, 0x0

    invoke-direct {v1, v5, v6, v2}, Lcom/bytedance/sdk/openadsdk/core/e/q;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/a;Lcom/bytedance/sdk/openadsdk/core/e/m;[B)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/core/e/q;)V

    const/16 v0, 0x3a98

    .line 832
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    return-void

    :cond_5
    const-string v1, "\u975ewifi\u73af\u5883"

    .line 836
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 843
    :cond_6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object v13

    new-instance v14, Lcom/bytedance/sdk/openadsdk/component/splash/b$10;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v5, p1

    move-object/from16 v10, p3

    invoke-direct/range {v0 .. v10}, Lcom/bytedance/sdk/openadsdk/component/splash/b$10;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;ZLjava/io/File;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/a;Lcom/bytedance/sdk/openadsdk/core/e/m;IJLcom/bytedance/sdk/openadsdk/k/a/c;)V

    invoke-virtual {v13, v12, v14}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/adnet/b/b$a;)V

    goto :goto_1

    :cond_7
    move-object/from16 v10, p3

    const-string v1, "\u672a\u4e0b\u53d1\u89c6\u9891\u6587\u4ef6\uff0c\u5219\u52a0\u8f7d\u56fe\u7247\u5b8c\u6210\u540e\u7ed9\u4e88\u56de\u8c03"

    .line 927
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    invoke-direct/range {p0 .. p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    :cond_8
    :goto_1
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 1

    if-eqz p1, :cond_2

    .line 1237
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1240
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1242
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->c(I)V

    goto :goto_0

    .line 1244
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->c(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 2

    const-string v0, "splashLoadAd"

    const-string v1, "onLogTimeoutEvent"

    .line 1532
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$4;

    const-string v1, "splash_timeout"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$4;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1267
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p1

    if-eqz p1, :cond_1

    return-void

    .line 1271
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(I)I

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x1

    .line 1273
    :goto_1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:Z

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 5

    const-string v0, "splashLoadAd"

    const-string v1, "reportMarkAtRespFail outer_call_no_rsp"

    .line 1639
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 1644
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1646
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    .line 1649
    :cond_1
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1651
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 1654
    :cond_2
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportMarkAtRespFail cacheStatus "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "if_have_cache"

    .line 1656
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "if_have_rt_ads"

    .line 1657
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->F:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1660
    :catchall_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v0

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a;->d(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method private c()Z
    .locals 2

    .line 309
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3a99

    .line 310
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/component/splash/a;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    return-object p0
.end method

.method private d(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 14

    const-string v0, "splash_load_type"

    .line 1343
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz v1, :cond_6

    if-eqz p1, :cond_6

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 1346
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->v()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 1350
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->L()Z

    move-result v1

    .line 1351
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->w()Z

    move-result v2

    .line 1352
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    if-eqz v2, :cond_2

    .line 1355
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->t()J

    move-result-wide v5

    sub-long v5, v3, v5

    .line 1356
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/e/t;->c(J)V

    goto :goto_0

    .line 1359
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->t()J

    move-result-wide v5

    sub-long v5, v3, v5

    .line 1360
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->y()J

    move-result-wide v7

    sub-long v7, v3, v7

    invoke-virtual {v2, v7, v8}, Lcom/bytedance/sdk/openadsdk/core/e/t;->k(J)V

    .line 1361
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/e/t;->o(J)V

    .line 1364
    :goto_0
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    if-eqz v1, :cond_3

    :try_start_0
    const-string v2, "pre_connect_status"

    .line 1367
    sget v5, Lcom/bytedance/sdk/openadsdk/core/l;->d:I

    invoke-virtual {v13, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :cond_3
    const-string v2, "if_pre_connect"

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    .line 1369
    :goto_1
    invoke-virtual {v13, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1370
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a()I

    move-result v1

    invoke-virtual {v13, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "splash_final_type"

    .line 1371
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "active_type"

    .line 1372
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->c()I

    move-result v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "splash_creative_type"

    .line 1373
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->d()I

    move-result v2

    invoke-virtual {v13, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1374
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a()I

    move-result v1

    invoke-virtual {v13, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1377
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:Z

    if-eqz v0, :cond_5

    const-string v0, "load_duration"

    .line 1378
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->e()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_image_duration"

    .line 1379
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->x()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "cache_image_duration"

    .line 1380
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->g()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "image_cachetype"

    .line 1381
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->h()I

    move-result v1

    invoke-virtual {v13, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "image_size"

    .line 1382
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->i()D

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "image_resolution"

    .line 1383
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "image_response_header"

    .line 1384
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->k()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v13, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "client_start_time"

    .line 1385
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->l()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "network_time"

    .line 1386
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->m()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "sever_time"

    .line 1387
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->n()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "client_end_time"

    .line 1388
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->o()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_client_start_time"

    .line 1389
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->p()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_net_time"

    .line 1390
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->q()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_write_time"

    .line 1391
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->r()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_client_end_time"

    .line 1392
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->s()J

    move-result-wide v1

    invoke-virtual {v13, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 1395
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1397
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->u()J

    move-result-wide v0

    sub-long v11, v3, v0

    .line 1398
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    const-string v9, "splash_ad"

    const-string v10, "splash_ad_loadtime"

    move-object v8, p1

    invoke-static/range {v7 .. v13}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;JLorg/json/JSONObject;)V

    :cond_6
    :goto_3
    return-void
.end method

.method private d(Lcom/bytedance/sdk/openadsdk/k/a/c;)V
    .locals 4

    const-string v0, "splashLoadAd"

    const-string v1, "reportMarkAtRespSucc outer_call_send"

    .line 1664
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 1669
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1670
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportMarkAtRespSucc sSplashLoadImageSource "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/bytedance/sdk/openadsdk/l/a/b;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "image_CacheType"

    .line 1672
    sget v2, Lcom/bytedance/sdk/openadsdk/l/a/b;->a:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1675
    :catchall_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v0

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a;->c(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method

.method private d()Z
    .locals 11

    const-string v0, "splashLoadAd"

    const-string v1, "try checkSpashAdCacheIsValidAndTryShowAd......"

    .line 333
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x3a99

    const/4 v2, 0x0

    .line 334
    invoke-direct {p0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v9

    .line 336
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 337
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "\u6ca1\u6709\u7f13\u5b58\u6570\u636e.........."

    .line 338
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, -0xc

    const/16 v1, 0x3a99

    .line 341
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v5, 0x2

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v6, -0xc

    move-object v3, v10

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move v4, v1

    move-object v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 343
    :cond_1
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:I

    return v2

    .line 349
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "======== \u7f13\u5b58\u8fc7\u671f ========"

    .line 350
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/16 v0, -0xb

    const/16 v1, 0x3a99

    .line 353
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v5, 0x2

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v6, -0xb

    move-object v3, v10

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move v4, v1

    move-object v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x2

    .line 355
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:I

    return v2

    :cond_5
    return v4
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private e()Z
    .locals 2

    .line 363
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v1, v0, :cond_0

    const/4 v1, 0x3

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private f()V
    .locals 3

    .line 1225
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    .line 1226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->m(J)V

    .line 1227
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1228
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->b(I)V

    .line 1229
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->b(I)V

    :goto_0
    return-void
.end method

.method private g()V
    .locals 4

    .line 1278
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    return-void

    .line 1281
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->u()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1282
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(J)V

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Z
    .locals 0

    .line 72
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->q:Z

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private h()V
    .locals 8

    .line 1319
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    goto :goto_1

    .line 1322
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->v()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 1326
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "image_size"

    .line 1328
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->i()D

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "image_resolution"

    .line 1329
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "image_response_header"

    .line 1330
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->k()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "download_client_start_time"

    .line 1331
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->p()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_net_time"

    .line 1332
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->q()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_write_time"

    .line 1333
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->r()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1335
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1337
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->f()J

    move-result-wide v5

    .line 1338
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v3, "splash_ad"

    const-string v4, "download_image_duration"

    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;JLorg/json/JSONObject;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private i()V
    .locals 3

    .line 1454
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    if-eqz v0, :cond_0

    .line 1455
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;)V

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Z
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e()Z

    move-result p0

    return p0
.end method

.method private j()V
    .locals 12

    .line 1609
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/core/e/a;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 1612
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 1615
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_2

    return-void

    .line 1619
    :cond_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->u()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    return-void

    .line 1624
    :cond_3
    new-instance v7, Lcom/bytedance/sdk/openadsdk/core/e/q;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/core/e/a;

    const/4 v2, 0x0

    invoke-direct {v7, v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/e/q;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/a;Lcom/bytedance/sdk/openadsdk/core/e/m;[B)V

    .line 1625
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    if-nez v1, :cond_4

    return-void

    .line 1628
    :cond_4
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v8

    .line 1629
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    return-void

    .line 1632
    :cond_5
    invoke-direct {p0, v7, v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object v9

    const/16 v10, 0x3a9b

    .line 1633
    new-instance v11, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-object v0, v11

    move-object v1, p0

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    move-object v0, p0

    move v1, v10

    move-object v2, v11

    move-object v3, v7

    move-object v4, v9

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    :cond_6
    :goto_0
    return-void
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g()V

    return-void
.end method

.method private k()Lcom/bytedance/sdk/openadsdk/core/e/n;
    .locals 3

    .line 1679
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1680
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/e/n;-><init>()V

    .line 1681
    iput-wide v0, v2, Lcom/bytedance/sdk/openadsdk/core/e/n;->f:J

    return-object v2
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/e/t;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/e/t;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Landroid/content/Context;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J
    .locals 2

    .line 72
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:J

    return-wide v0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/l/a/d;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/l/a/d;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J
    .locals 2

    .line 72
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:J

    return-wide v0
.end method

.method static synthetic q(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h()V

    return-void
.end method

.method static synthetic r(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 12

    .line 1545
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-ne v0, v8, :cond_1

    .line 1546
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "SplashAdLoadManager"

    if-nez v0, :cond_0

    .line 1548
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1549
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b()V

    const-string v0, "\u5c1d\u8bd5\u4ece\u7f13\u5b58\u4e2d\u53d6"

    .line 1550
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1553
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i()V

    const-string v0, "\u5f00\u59cb\u9884\u52a0\u8f7d"

    .line 1554
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {v0, v7}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1559
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    const-string v9, "splashLoadAd"

    if-ne v0, v1, :cond_3

    .line 1560
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {v0, v7}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1561
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    const-string v0, "MSG_USER_TIME_OUT----7-"

    .line 1564
    invoke-static {v9, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1565
    invoke-direct {p0, v0, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v6

    const/16 v10, 0x3a9c

    .line 1566
    new-instance v11, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v11

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v3, 0x0

    move-object v0, p0

    move v1, v10

    move-object v2, v11

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 1569
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const-string v0, "handleMsg MSG_SPLASH_TIME_OUT "

    .line 1570
    invoke-static {v9, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 1572
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    .line 1576
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1577
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v0, 0x3

    .line 1580
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v1, :cond_5

    return-void

    .line 1583
    :cond_5
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->au()Z

    move-result v1

    .line 1584
    invoke-direct {p0, v0, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/e/q;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v6

    if-eqz v1, :cond_7

    .line 1587
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-nez v7, :cond_6

    return-void

    .line 1591
    :cond_6
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a()Ljava/lang/String;

    move-result-object v8

    .line 1592
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MSG_SPLASH_TIME_OUT videoCachePath "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v9, 0x3a9b

    .line 1593
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, v10

    move-object v1, p0

    move-object v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v3, 0x0

    move-object v0, p0

    move v1, v9

    move-object v2, v10

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    goto :goto_1

    .line 1595
    :cond_7
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-nez v7, :cond_8

    return-void

    :cond_8
    const/16 v8, 0x3a9b

    .line 1599
    new-instance v9, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, v8

    move-object v2, v9

    move-object v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 1603
    :cond_9
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_a

    .line 1604
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j()V

    :cond_a
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;I)V
    .locals 8

    .line 186
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k()Lcom/bytedance/sdk/openadsdk/core/e/n;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    .line 187
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 188
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 189
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 p2, 0x0

    .line 190
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 191
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 192
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 193
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 194
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u5f00\u53d1\u8005\u4f20\u5165\u7684\u8d85\u65f6\u65f6\u957f timeOut "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "splashLoadAd"

    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:I

    int-to-long v1, p3

    .line 197
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c(Ljava/lang/String;)I

    move-result p1

    .line 198
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4e91\u63a7\u7684\u8d85\u65f6\u65f6\u957f cloudTimeOut "

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p2, 0x1f4

    if-lez p1, :cond_0

    const-string p2, "\u4e91\u63a7\u63a7\u5236\u7684\u8d85\u65f6\u65f6\u957f\u5927\u4e8e0\u6bd4\u8f83 \u8f83\u670d\u52a1\u7aef\u4e0b\u53d1\u7684\u8d85\u65f6\u65f6\u957f\u548c\u5f00\u53d1\u8005\u914d\u7f6e\u7684\u8d85\u65f6\u65f6\u957f "

    .line 201
    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-lt p1, p3, :cond_1

    move p3, p1

    goto :goto_0

    :cond_0
    if-ge p3, p2, :cond_1

    const/16 p3, 0x1f4

    .line 210
    :cond_1
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSplashAd \u5b9e\u9645 timeOut "

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    const/4 v0, 0x2

    int-to-long v3, p3

    invoke-virtual {p2, v0, v3, v4}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    add-int/lit16 p2, p3, -0x12c

    if-lez p2, :cond_2

    .line 214
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/component/utils/u;

    const/4 v5, 0x5

    int-to-long v6, p2

    invoke-virtual {v0, v5, v6, v7}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    .line 216
    :cond_2
    invoke-direct {p0, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(I)V

    .line 218
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    iput-wide v1, p2, Lcom/bytedance/sdk/openadsdk/core/e/n;->g:J

    .line 219
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    int-to-long v5, p1

    iput-wide v5, p2, Lcom/bytedance/sdk/openadsdk/core/e/n;->h:J

    .line 220
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/e/n;

    iput-wide v3, p1, Lcom/bytedance/sdk/openadsdk/core/e/n;->i:J

    .line 222
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$1;

    const-string p2, "getSplashAd"

    invoke-direct {p1, p0, p2, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;J)V

    invoke-static {p1}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    .line 240
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b()V

    .line 241
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 431
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result p1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
