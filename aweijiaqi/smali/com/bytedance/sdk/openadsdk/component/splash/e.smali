.class public Lcom/bytedance/sdk/openadsdk/component/splash/e;
.super Ljava/lang/Object;
.source "TTSplashAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/TTSplashAd;


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/core/b/a;

.field private B:Lcom/bytedance/sdk/component/utils/u;

.field private C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private D:J

.field private E:Z

.field private F:Z

.field private G:Z

.field public a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:I

.field private final d:Landroid/content/Context;

.field private final e:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

.field private g:Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;

.field private h:Z

.field private i:J

.field private j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

.field private s:Ljava/lang/String;

.field private t:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private u:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

.field private v:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private w:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private x:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

.field private y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

.field private z:Lcom/bytedance/sdk/openadsdk/l/a/d;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V
    .locals 3

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 72
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c:I

    const-wide/16 v0, 0x0

    .line 79
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->i:J

    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    const/4 v0, 0x0

    .line 85
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->m:Z

    .line 86
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    const/4 v1, 0x1

    .line 88
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    const/4 v2, -0x1

    .line 89
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    .line 94
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 95
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 97
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 105
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    .line 107
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    .line 108
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->F:Z

    .line 109
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->G:Z

    .line 111
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 113
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->au()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    .line 114
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->t:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 115
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    .line 116
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V
    .locals 3

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 72
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c:I

    const-wide/16 v0, 0x0

    .line 79
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->i:J

    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    const/4 v0, 0x0

    .line 85
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->m:Z

    .line 86
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    const/4 v1, 0x1

    .line 88
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    const/4 v2, -0x1

    .line 89
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    .line 94
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 95
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 97
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 105
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    .line 107
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    .line 108
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->F:Z

    .line 109
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->G:Z

    .line 121
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    .line 122
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 123
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->au()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    .line 124
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    .line 125
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->t:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 126
    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    .line 127
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/e;I)I
    .locals 0

    .line 68
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/e;J)J
    .locals 0

    .line 68
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->i:J

    return-wide p1
.end method

.method private a(I)V
    .locals 1

    .line 1045
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v0, :cond_0

    .line 1046
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setCountDownTime(I)V

    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .line 1138
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "show_time"

    .line 1140
    invoke-virtual {v0, v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1141
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    const-string v2, "icon_splash_video_show_time"

    invoke-static {p1, p2, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1143
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 8

    if-eqz p1, :cond_4

    .line 191
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-nez v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->E()I

    move-result v0

    .line 195
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->F()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->H()I

    move-result v4

    .line 197
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->G()I

    move-result v5

    .line 198
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->I()I

    move-result v6

    .line 199
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->J()I

    move-result v7

    const/4 p1, 0x2

    if-ne v0, p1, :cond_2

    .line 201
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 202
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/4 v3, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a(IIIII)V

    .line 204
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setClickBarDesc(Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/4 v3, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a(IIIII)V

    .line 208
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const-string v0, "\u67e5\u770b\u8be6\u60c5"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setClickBarDesc(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 212
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/16 v3, 0x8

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a(IIIII)V

    goto :goto_0

    .line 215
    :cond_3
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/4 v3, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->a(IIIII)V

    .line 217
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setClickBarDesc(Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 5

    .line 499
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 501
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->b()V

    .line 502
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Landroid/app/Activity;)V

    .line 506
    :cond_0
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 507
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 508
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v1, :cond_1

    .line 509
    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Landroid/view/View;)V

    .line 511
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$7;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$7;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setCallback(Lcom/bytedance/sdk/openadsdk/core/EmptyView$a;)V

    .line 549
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "splash_show_type"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v1, v2, p2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 553
    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Landroid/view/View;)V

    .line 554
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 555
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Ljava/util/Map;)V

    .line 556
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setClickListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;)V

    .line 558
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v1, v2, p2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 559
    invoke-virtual {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V

    .line 560
    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Landroid/view/View;)V

    .line 561
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 562
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Ljava/util/Map;)V

    .line 563
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setClickCreativeListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;)V

    const/4 p1, 0x1

    .line 565
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setNeedCheckingShow(Z)V

    .line 566
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 306
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    if-eqz p1, :cond_0

    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 309
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->D:J

    sub-long/2addr v0, v2

    .line 310
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(J)V

    const/4 p1, 0x0

    .line 311
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    .line 313
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    if-eqz p1, :cond_1

    .line 315
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;->onSplashClickEyeAnimationFinish()V

    .line 317
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    if-eqz p1, :cond_2

    .line 318
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->m()V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/e;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Landroid/content/Context;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    return-object p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 2

    .line 1004
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/e;Z)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .line 638
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-nez v0, :cond_0

    return-void

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_1

    .line 643
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 646
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/splash/e$8;

    invoke-direct {v2, p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$8;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;ZLjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/TsView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    return-object p0
.end method

.method private c()V
    .locals 4

    .line 136
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    .line 137
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_0

    .line 138
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setADlogoLongClickContent(Ljava/lang/String;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 142
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_1

    .line 143
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setVideoViewVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setImageViewVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    new-instance v3, Lcom/bytedance/sdk/openadsdk/component/splash/e$1;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setVoiceViewListener(Landroid/view/View$OnClickListener;)V

    .line 161
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    if-nez v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setVideoViewVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setImageViewVisibility(I)V

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->j()I

    move-result v0

    if-nez v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v0, :cond_4

    .line 168
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setAdlogoViewVisibility(I)V

    goto :goto_0

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v0, :cond_4

    .line 172
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setAdlogoViewVisibility(I)V

    .line 177
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ar()I

    move-result v0

    if-gtz v0, :cond_5

    const/4 v0, 0x3

    .line 178
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(I)V

    goto :goto_1

    .line 180
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ar()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c:I

    .line 181
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(I)V

    .line 183
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 185
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->i()V

    .line 186
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f()V

    .line 187
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->B:Lcom/bytedance/sdk/component/utils/u;

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/e;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->F:Z

    return p1
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/d;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    return-object p0
.end method

.method private d()Z
    .locals 5

    .line 223
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/d;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/d;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mVideoCachePath:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wzj"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    .line 263
    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;-><init>()V

    .line 264
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Ljava/lang/String;)V

    .line 265
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d(Ljava/lang/String;)V

    .line 266
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(I)V

    .line 267
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(I)V

    .line 268
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->e(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    .line 269
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(J)V

    .line 270
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Z)V

    .line 272
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v2

    .line 273
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v3

    .line 274
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 275
    :cond_0
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 277
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aN()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 280
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getBrandCacheDir()Ljava/lang/String;

    move-result-object v0

    .line 279
    :cond_1
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/splash/e;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->G:Z

    return p1
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->g:Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;

    return-object p0
.end method

.method private e()V
    .locals 1

    .line 297
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j:Z

    if-nez v0, :cond_1

    .line 298
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 299
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Z)V

    :cond_0
    const/4 v0, 0x0

    .line 301
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->F:Z

    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method private f()V
    .locals 5

    .line 413
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-nez v0, :cond_1

    .line 417
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->t:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    goto :goto_0

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 420
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressVideoView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->t:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressVideoView;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 423
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-nez v0, :cond_3

    return-void

    .line 426
    :cond_3
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$5;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$5;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setBackupListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/c;)V

    .line 438
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 439
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$6;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    :cond_4
    :goto_1
    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Ljava/lang/String;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    return-object p0
.end method

.method private g()Z
    .locals 3

    .line 614
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 617
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->u:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    return-object p0
.end method

.method private h()V
    .locals 2

    .line 622
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    if-nez v0, :cond_0

    return-void

    .line 625
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 626
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;->isSupportSplashClickEye(Z)Z

    return-void

    .line 629
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->n:Z

    if-eqz v0, :cond_2

    .line 630
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k()V

    .line 632
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;->isSupportSplashClickEye(Z)Z

    .line 634
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;->onSplashClickEyeAnimationStart()V

    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private i()V
    .locals 6

    .line 742
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 743
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 745
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    goto :goto_0

    .line 748
    :cond_0
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    goto :goto_0

    .line 751
    :cond_1
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    .line 753
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    .line 754
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-direct {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const/4 v4, 0x3

    .line 755
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setAdType(I)V

    .line 756
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v4, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->addView(Landroid/view/View;)V

    .line 757
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v4, :cond_2

    .line 758
    invoke-interface {v4, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Landroid/view/View;)V

    .line 760
    :cond_2
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b(Z)V

    .line 761
    new-instance v3, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$9;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setCallback(Lcom/bytedance/sdk/openadsdk/core/EmptyView$a;)V

    .line 891
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setNeedCheckingShow(Z)V

    .line 893
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "splash_show_type"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    const/4 v5, 0x4

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    .line 895
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V

    .line 896
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Ljava/util/Map;)V

    .line 897
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v1, :cond_4

    .line 898
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->E()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 900
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getFullClickBarView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 902
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Landroid/view/View;)V

    .line 903
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 904
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 907
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Landroid/view/View;)V

    .line 908
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setOnClickListenerInternal(Landroid/view/View$OnClickListener;)V

    .line 909
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setOnTouchListenerInternal(Landroid/view/View$OnTouchListener;)V

    .line 912
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getDislikeView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->b(Landroid/view/View;)V

    .line 913
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 914
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$10;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$10;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/b/b$a;)V

    .line 924
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setSkipListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    return-object p0
.end method

.method private j()Z
    .locals 5

    .line 1063
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 1066
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/d;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {v1, v2, v0, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/d;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    .line 1067
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    .line 1068
    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/o/f/b;-><init>()V

    .line 1069
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Ljava/lang/String;)V

    .line 1070
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d(Ljava/lang/String;)V

    .line 1071
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(I)V

    .line 1072
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(I)V

    .line 1073
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->e(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    .line 1074
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(J)V

    .line 1075
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->p:Z

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a(Z)V

    .line 1077
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v2

    .line 1078
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v3

    .line 1079
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1080
    :cond_1
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 1081
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1082
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1084
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aN()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1085
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getBrandCacheDir()Ljava/lang/String;

    move-result-object v0

    .line 1084
    :cond_2
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 1087
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;)Z

    move-result v0

    return v0

    :cond_3
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private k()V
    .locals 1

    .line 1093
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    if-eqz v0, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    return-object p0
.end method

.method private l()V
    .locals 1

    .line 1102
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    if-eqz v0, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->x:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    return-object p0
.end method

.method private m()V
    .locals 3

    .line 1118
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-nez v0, :cond_0

    return-void

    .line 1122
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 1125
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1126
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_1

    .line 1130
    :cond_2
    instance-of v2, v1, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    if-eqz v2, :cond_3

    .line 1131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->removeView(Landroid/view/View;)V

    return-void

    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Ljava/lang/String;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->m()V

    return-void
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->h:Z

    return p0
.end method

.method static synthetic q(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l()V

    return-void
.end method

.method static synthetic r(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->F:Z

    return p0
.end method

.method static synthetic s(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->h()V

    return-void
.end method

.method static synthetic t(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->g()Z

    move-result p0

    return p0
.end method

.method static synthetic u(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k()V

    return-void
.end method

.method static synthetic v(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e()V

    return-void
.end method

.method static synthetic w(Lcom/bytedance/sdk/openadsdk/component/splash/e;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->o:Z

    return p0
.end method

.method static synthetic x(Lcom/bytedance/sdk/openadsdk/component/splash/e;)J
    .locals 2

    .line 68
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->i:J

    return-wide v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/os/Message;)V
    .locals 1

    .line 1111
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 1113
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Z)V

    :cond_0
    return-void
.end method

.method a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 2

    .line 574
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setGifView([B)V

    goto :goto_1

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 579
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 580
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 582
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/e/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->b()I

    move-result v0

    .line 583
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object p1

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/r/f;->a([BI)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 585
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_1
    return-void
.end method

.method b(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 594
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setGifView([B)V

    goto :goto_1

    .line 597
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/s;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 599
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 600
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 602
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/r/f;->a([BI)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 604
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public b()Z
    .locals 3

    .line 1038
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1039
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method c(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 0

    .line 610
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->z:Lcom/bytedance/sdk/openadsdk/l/a/d;

    return-void
.end method

.method public getInteractionType()I
    .locals 1

    .line 990
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getMediaExtraInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1020
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_2

    .line 1021
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/e/l;

    .line 1022
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->av()Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    if-nez v0, :cond_1

    .line 1027
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->av()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 1029
    :cond_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v0

    .line 1030
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->av()Ljava/util/Map;

    move-result-object v1

    const-string v2, "image_url"

    .line 1031
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSplashClickEyeSizeToDp()[I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 395
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 398
    :cond_0
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->au()Z

    move-result v1

    .line 399
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v3

    if-nez v3, :cond_1

    return-object v2

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 403
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/s;->d()I

    move-result v1

    aput v1, v0, v3

    .line 404
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/s;->e()I

    move-result v1

    aput v1, v0, v2

    goto :goto_0

    .line 406
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/s;->a()I

    move-result v1

    aput v1, v0, v3

    .line 407
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->S()Lcom/bytedance/sdk/openadsdk/core/e/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/s;->b()I

    move-result v1

    aput v1, v0, v2

    :goto_0
    return-object v0
.end method

.method public getSplashView()Landroid/view/View;
    .locals 1

    .line 978
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->getVideoContainer()Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 980
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 985
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    return-object v0
.end method

.method public renderExpressAd(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 288
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->r:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-nez v0, :cond_0

    goto :goto_0

    .line 291
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->u:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    .line 292
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->k()V

    :cond_1
    :goto_0
    return-void
.end method

.method public setDownloadListener(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 0

    .line 1000
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->x:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    return-void
.end method

.method public setNotAllowSdkCountdown()V
    .locals 2

    const/4 v0, 0x1

    .line 1012
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->h:Z

    .line 1013
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 1014
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setSkipIconVisibility(I)V

    :cond_0
    return-void
.end method

.method public setSplashClickEyeListener(Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;)V
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->y:Lcom/bytedance/sdk/openadsdk/ISplashClickEyeListener;

    return-void
.end method

.method public setSplashInteractionListener(Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;)V
    .locals 0

    .line 995
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->g:Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;

    return-void
.end method

.method public splashClickEyeAnimationFinish()V
    .locals 6

    .line 325
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 328
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 331
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->E:Z

    .line 332
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->D:J

    .line 333
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "splash_click_area"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    if-eqz v1, :cond_2

    .line 335
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->C:Ljava/util/Map;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Ljava/util/Map;)V

    .line 337
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->au()Z

    move-result v1

    .line 339
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setCloseViewVisibility(I)V

    .line 340
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setSkipIconVisibility(I)V

    .line 341
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setClickBarViewVisibility(I)V

    .line 342
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setVideoVoiceVisibility(I)V

    .line 343
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setAdlogoViewVisibility(I)V

    .line 345
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    if-eqz v2, :cond_3

    .line 346
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Landroid/view/View;)V

    .line 347
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setOnClickListenerInternal(Landroid/view/View$OnClickListener;)V

    .line 348
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->A:Lcom/bytedance/sdk/openadsdk/core/b/a;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setOnTouchListenerInternal(Landroid/view/View$OnTouchListener;)V

    .line 350
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->s:Ljava/lang/String;

    const-string v5, "show_splash_icon"

    invoke-static {v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    if-nez v1, :cond_4

    .line 353
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->z:Lcom/bytedance/sdk/openadsdk/l/a/d;

    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->b(Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    .line 354
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->A()I

    move-result v1

    if-lez v1, :cond_9

    .line 356
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->B:Lcom/bytedance/sdk/component/utils/u;

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v3, v1

    invoke-virtual {v2, v0, v3, v4}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 359
    :cond_4
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->G:Z

    if-eqz v0, :cond_6

    .line 360
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    if-eqz v0, :cond_5

    .line 361
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->m()V

    .line 363
    :cond_5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j()Z

    goto :goto_0

    .line 366
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_8

    .line 367
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->k:Lcom/bytedance/sdk/openadsdk/component/splash/d;

    if-eqz v0, :cond_7

    .line 368
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->m()V

    .line 370
    :cond_7
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->j()Z

    goto :goto_0

    .line 372
    :cond_8
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->l()V

    .line 377
    :cond_9
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/e;->f:Lcom/bytedance/sdk/openadsdk/component/splash/TsView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/e$4;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/e$4;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/e;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/TsView;->setCloseViewListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    :goto_1
    return-void
.end method
