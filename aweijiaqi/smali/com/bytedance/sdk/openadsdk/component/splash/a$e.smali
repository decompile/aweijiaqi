.class Lcom/bytedance/sdk/openadsdk/component/splash/a$e;
.super Lcom/bytedance/sdk/component/e/g;
.source "SplashAdCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/a;Ljava/lang/String;)V
    .locals 0

    .line 563
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    const-string p1, "ReadCacheTask"

    .line 564
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 565
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/a$e;Ljava/lang/String;)V
    .locals 0

    .line 560
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .line 569
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/io/File;)[B
    .locals 6

    .line 630
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    new-array v2, v1, [B

    const/4 v3, 0x0

    .line 635
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    const/4 p1, 0x0

    .line 636
    :try_start_1
    invoke-virtual {v4, v2, p1, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 637
    invoke-virtual {v0, v2, p1, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 639
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 645
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 648
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_3

    :catchall_0
    move-exception p1

    move-object v3, v4

    goto :goto_1

    :catch_0
    move-object v3, v4

    goto :goto_2

    :catchall_1
    move-exception p1

    .line 645
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    if-eqz v3, :cond_1

    .line 648
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 651
    :catch_1
    :cond_1
    throw p1

    .line 645
    :catch_2
    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    if-eqz v3, :cond_2

    .line 648
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 653
    :catch_3
    :cond_2
    :goto_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public run()V
    .locals 13

    const-string v0, "splashLoadAd"

    .line 574
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/utils/u;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x1

    .line 575
    iput v2, v1, Landroid/os/Message;->what:I

    .line 577
    :try_start_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/a;

    move-result-object v3

    .line 578
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/e/q;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5, v5}, Lcom/bytedance/sdk/openadsdk/core/e/q;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/a;Lcom/bytedance/sdk/openadsdk/core/e/m;[B)V

    if-eqz v3, :cond_4

    .line 579
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 580
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 581
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v6, :cond_4

    .line 582
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aK()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 585
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v7, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/e/a;)Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v7

    const-string v8, ""

    if-eqz v7, :cond_0

    .line 588
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/e/l;->g()Ljava/lang/String;

    move-result-object v8

    .line 590
    :cond_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 591
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a()Lcom/bytedance/sdk/openadsdk/l/a/a;

    move-result-object v7

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v8, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/e/a;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v9, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/e/a;)I

    move-result v9

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    .line 592
    invoke-static {v10, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/e/a;)I

    move-result v3

    sget-object v10, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    .line 591
    invoke-virtual {v7, v8, v9, v3, v10}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;)Ljava/lang/String;

    move-result-object v8

    .line 594
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a()Lcom/bytedance/sdk/openadsdk/l/a/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->b()Ljava/lang/String;

    move-result-object v3

    .line 595
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " readSplashAdFromCache \u5f00\u59cb\u83b7\u53d6\u7f13\u5b58\u6587\u4ef6 filePath "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " readSplashAdFromCache path "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v3, v9, v11

    if-gtz v3, :cond_2

    goto :goto_0

    .line 604
    :cond_2
    sput v2, Lcom/bytedance/sdk/openadsdk/l/a/b;->a:I

    .line 605
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " readSplashAdFromCache \u83b7\u53d6\u6587\u4ef6\u6210\u529f cacheKey "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    invoke-direct {p0, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a(Ljava/io/File;)[B

    move-result-object v2

    if-eqz v2, :cond_4

    .line 607
    array-length v3, v2

    if-eqz v3, :cond_4

    .line 608
    invoke-virtual {v4, v6}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 609
    invoke-virtual {v4, v2}, Lcom/bytedance/sdk/openadsdk/core/e/q;->a([B)V

    goto :goto_1

    .line 600
    :cond_3
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " readSplashAdFromCache \u83b7\u53d6\u6587\u4ef6\u5931\u8d25 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 614
    :cond_4
    :goto_1
    :try_start_1
    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618
    :catchall_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/component/utils/u;->sendMessage(Landroid/os/Message;)Z

    .line 623
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u83b7\u53d6\u7f13\u5b58\u5e7f\u544a\u4e4b\u540e\u5c06\u5176\u6e05\u7a7a clearCache "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    return-void
.end method
