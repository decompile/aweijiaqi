.class Lcom/bytedance/sdk/openadsdk/component/splash/b$8;
.super Ljava/lang/Object;
.source "SplashAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/r/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/k/a/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/e/a;

.field final synthetic b:Z

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/k/a/c;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/bytedance/sdk/openadsdk/component/splash/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/a;ZLcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/k/a/c;Ljava/lang/String;)V
    .locals 0

    .line 651
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    iput-boolean p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->b:Z

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .line 722
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V

    .line 723
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 725
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/k/a/c;Ljava/lang/String;)V

    .line 728
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    const/4 v1, -0x7

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    .line 729
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 730
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "splashLoad----5-LoadImageBytes-onFailed-code="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ",msg="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "splashLoadAd"

    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    const/4 v5, 0x2

    const/4 v8, 0x0

    const/4 v6, -0x7

    move-object v3, v2

    move-object v4, v0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const/16 v4, 0x3a98

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 734
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->b:Z

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-wide/16 v6, -0x7

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    :cond_0
    const-string v0, "SplashAdLoadManager"

    const-string v1, "\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    .line 738
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 11

    .line 657
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    const-string v0, "splashLoadAd"

    const-string v1, "\u56fe\u7247\u52a0\u8f7d\u6210\u529f"

    .line 659
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V

    .line 661
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/e/t;

    move-result-object v1

    sget v2, Lcom/bytedance/sdk/openadsdk/l/a/b;->a:I

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->d(I)V

    .line 663
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->b:Z

    if-nez v1, :cond_0

    .line 664
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$8$1;

    const-string v2, "preLoadImage"

    invoke-direct {v1, p0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b$8$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b$8;Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    .line 673
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 675
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->c(Z)V

    .line 676
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v4

    const-string v5, "splash_ad"

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/component/splash/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    .line 677
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 678
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/l/a/d;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 679
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/l/a/d;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->c(Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    .line 681
    :cond_1
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V

    .line 684
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 685
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-object v2, p1

    move-object v7, v0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 687
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/16 v3, 0x3a98

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v4, p1

    move-object v6, v0

    invoke-static/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 689
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 690
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    .line 694
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->b:Z

    if-eqz p1, :cond_6

    .line 695
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    goto :goto_0

    .line 699
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v1, 0x3

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 701
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->e:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/k/a/c;Ljava/lang/String;)V

    const/4 p1, -0x7

    .line 703
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-virtual {v2, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v2

    .line 704
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 705
    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v6, 0x2

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->d:Lcom/bytedance/sdk/openadsdk/k/a/c;

    const/4 v7, -0x7

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    const-string v3, "preLoadImage  data == null REQUEST_TYPE_REAL_NETWORK"

    .line 706
    invoke-static {v0, v3}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 708
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/16 v5, 0x3a98

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v6, v2

    invoke-static/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/e/q;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 710
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->b:Z

    if-eqz v0, :cond_5

    .line 711
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    int-to-long v6, p1

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    :cond_5
    const-string p1, "SplashAdLoadManager"

    const-string v0, "\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    .line 714
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 743
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->q(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V

    return-void
.end method
