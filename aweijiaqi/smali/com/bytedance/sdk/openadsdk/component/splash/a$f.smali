.class Lcom/bytedance/sdk/openadsdk/component/splash/a$f;
.super Lcom/bytedance/sdk/component/e/g;
.source "SplashAdCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

.field private b:Lcom/bytedance/sdk/openadsdk/core/e/q;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/e/q;)V
    .locals 0

    .line 662
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    const-string p1, "WriteCacheTask"

    .line 663
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 664
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->b:Lcom/bytedance/sdk/openadsdk/core/e/q;

    return-void
.end method

.method private c()V
    .locals 6

    .line 685
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 688
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/utils/u;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    .line 689
    iput v1, v0, Landroid/os/Message;->what:I

    .line 691
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->b:Lcom/bytedance/sdk/openadsdk/core/e/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->c(Lcom/bytedance/sdk/openadsdk/core/e/q;)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gtz v1, :cond_1

    .line 706
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/utils/u;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 695
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v3, "materialMeta"

    const-string v4, "tt_materialMeta"

    if-eqz v2, :cond_2

    .line 696
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->b:Lcom/bytedance/sdk/openadsdk/core/e/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/q;->c()Lcom/bytedance/sdk/openadsdk/core/e/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 699
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 700
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 702
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->b:Lcom/bytedance/sdk/openadsdk/core/e/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/q;->c()Lcom/bytedance/sdk/openadsdk/core/e/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 706
    :catchall_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/component/utils/u;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/utils/u;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/e/q;)V
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->b:Lcom/bytedance/sdk/openadsdk/core/e/q;

    return-void
.end method

.method public run()V
    .locals 0

    .line 678
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/a$f;->c()V

    return-void
.end method
