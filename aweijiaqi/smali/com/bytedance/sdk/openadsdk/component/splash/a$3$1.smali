.class Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;
.super Ljava/lang/Object;
.source "SplashAdCacheManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/r/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/e/a;

.field final synthetic b:Z

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/a$3;Lcom/bytedance/sdk/openadsdk/core/e/a;ZLcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 801
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    iput-boolean p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->b:Z

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .line 832
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V

    const-string v0, "SplashAdCacheManager"

    const-string v1, "\u56fe\u7247\u6570\u636e\u52a0\u8f7d\u5931\u8d25"

    .line 833
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "splashLoad"

    const-string v1, "\u56fe\u7247\u6570\u636e\u9884\u52a0\u8f7d\u5931\u8d25...."

    .line 834
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->b:Z

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->f(Lcom/bytedance/sdk/openadsdk/component/splash/a;)J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-wide/16 v6, -0x7

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 840
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/component/splash/a$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 841
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/component/splash/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a$a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a$a;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/l/a/d;)V
    .locals 8

    .line 806
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V

    .line 808
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->b:Z

    if-nez v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 810
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->e(Lcom/bytedance/sdk/openadsdk/component/splash/a;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-string v3, "splash_ad"

    .line 809
    invoke-static {v0, v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;J)V

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;J)J

    const-string v0, "SplashAdCacheManager"

    const-string v1, "\u56fe\u7247\u6570\u636e\u52a0\u8f7d\u7684\u5e7f\u544a\u7f13\u5b58\u5230\u672c\u5730"

    .line 815
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "splashLoad"

    const-string v1, "\u9884\u52a0\u8f7d\u6210\u529f\uff0c\u5e7f\u544a\u7f13\u5b58\u5230\u672c\u5730----10"

    .line 816
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->d(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/e/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/a/d;->b()[B

    move-result-object p1

    invoke-direct {v1, v2, v3, p1}, Lcom/bytedance/sdk/openadsdk/core/e/q;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/a;Lcom/bytedance/sdk/openadsdk/core/e/m;[B)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/q;)V

    .line 819
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->b:Z

    if-eqz p1, :cond_1

    .line 820
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->f(Lcom/bytedance/sdk/openadsdk/component/splash/a;)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/component/adnet/core/m;)V

    .line 822
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 823
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/component/splash/a$a;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 824
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->b:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->c(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/component/splash/a$a;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3$1;->d:Lcom/bytedance/sdk/openadsdk/component/splash/a$3;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/component/splash/a$3;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a$a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a$a;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    :cond_2
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method
