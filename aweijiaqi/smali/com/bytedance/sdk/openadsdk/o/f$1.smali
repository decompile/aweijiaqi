.class Lcom/bytedance/sdk/openadsdk/o/f$1;
.super Ljava/lang/Object;
.source "ProxyServer.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/o/g$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/o/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/o/f;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/o/f;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/f$1;->a:Lcom/bytedance/sdk/openadsdk/o/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/o/g;)V
    .locals 3

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/f$1;->a:Lcom/bytedance/sdk/openadsdk/o/f;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Lcom/bytedance/sdk/openadsdk/o/f;)Landroid/util/SparseArray;

    move-result-object v0

    monitor-enter v0

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/f$1;->a:Lcom/bytedance/sdk/openadsdk/o/f;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Lcom/bytedance/sdk/openadsdk/o/f;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    if-eqz v1, :cond_0

    .line 75
    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/o/g;)V
    .locals 3

    .line 82
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "afterExecute, ProxyTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TAG_PROXY_ProxyServer"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g;->f()I

    move-result v0

    .line 86
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/f$1;->a:Lcom/bytedance/sdk/openadsdk/o/f;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Lcom/bytedance/sdk/openadsdk/o/f;)Landroid/util/SparseArray;

    move-result-object v1

    monitor-enter v1

    .line 87
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/f$1;->a:Lcom/bytedance/sdk/openadsdk/o/f;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Lcom/bytedance/sdk/openadsdk/o/f;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 89
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 91
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
