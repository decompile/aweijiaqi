.class Lcom/bytedance/sdk/openadsdk/o/g;
.super Lcom/bytedance/sdk/openadsdk/o/a;
.source "ProxyTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/o/g$a;,
        Lcom/bytedance/sdk/openadsdk/o/g$b;,
        Lcom/bytedance/sdk/openadsdk/o/g$c;
    }
.end annotation


# instance fields
.field private final m:Ljava/net/Socket;

.field private final n:Lcom/bytedance/sdk/openadsdk/o/g$c;

.field private final o:Lcom/bytedance/sdk/openadsdk/o/d;

.field private volatile p:Lcom/bytedance/sdk/openadsdk/o/b;

.field private volatile q:Z


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/o/g$a;)V
    .locals 2

    .line 42
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/g$a;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/o/g$a;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a;-><init>(Lcom/bytedance/sdk/openadsdk/o/a/a;Lcom/bytedance/sdk/openadsdk/o/b/c;)V

    const/4 v0, 0x1

    .line 142
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:Z

    .line 44
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/g$a;->c:Ljava/net/Socket;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    .line 45
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/o/g$a;->d:Lcom/bytedance/sdk/openadsdk/o/g$c;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->n:Lcom/bytedance/sdk/openadsdk/o/g$c;

    .line 46
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/d;->c()Lcom/bytedance/sdk/openadsdk/o/d;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:Lcom/bytedance/sdk/openadsdk/o/d;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/o/g;)Lcom/bytedance/sdk/openadsdk/o/d;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:Lcom/bytedance/sdk/openadsdk/o/d;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/o/b/a;Ljava/io/File;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/c/d;,
            Lcom/bytedance/sdk/openadsdk/o/h$a;,
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/openadsdk/o/c/b;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 292
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/g$b;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 293
    invoke-direct {p0, p1, p3, p4}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)[B

    move-result-object v0

    .line 294
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    if-nez v0, :cond_0

    return-void

    .line 297
    :cond_0
    array-length v2, v0

    invoke-virtual {p3, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/g$b;->a([BII)V

    :cond_1
    const/4 v0, 0x0

    if-nez p1, :cond_4

    .line 301
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object p1

    if-nez p1, :cond_4

    .line 303
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_2

    const-string p1, "TAG_PROXY_ProxyTask"

    const-string v2, "failed to get video header info from db"

    .line 304
    invoke-static {p1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_2
    invoke-direct {p0, v0, p3, p4}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)[B

    .line 308
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 311
    :cond_3
    new-instance p1, Lcom/bytedance/sdk/openadsdk/o/c/c;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "failed to get header, rawKey: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", url: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/c/c;-><init>(Ljava/lang/String;)V

    throw p1

    .line 317
    :cond_4
    :goto_0
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    iget v4, p1, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    int-to-long v4, v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_6

    .line 319
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:Lcom/bytedance/sdk/openadsdk/o/b;

    if-eqz v2, :cond_5

    .line 320
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/o/b;->b()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/o/b;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 321
    :cond_5
    new-instance v2, Lcom/bytedance/sdk/openadsdk/o/b$a;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/o/b$a;-><init>()V

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    .line 323
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Lcom/bytedance/sdk/openadsdk/o/a/a;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Lcom/bytedance/sdk/openadsdk/o/b/c;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/o/l;

    iget-object v4, p4, Lcom/bytedance/sdk/openadsdk/o/l$a;->a:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/bytedance/sdk/openadsdk/o/l;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Lcom/bytedance/sdk/openadsdk/o/l;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Lcom/bytedance/sdk/openadsdk/o/i;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/o/g$1;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/o/g$1;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 325
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a(Lcom/bytedance/sdk/openadsdk/o/b$b;)Lcom/bytedance/sdk/openadsdk/o/b$a;

    move-result-object v2

    .line 340
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/o/b$a;->a()Lcom/bytedance/sdk/openadsdk/o/b;

    move-result-object v2

    .line 341
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:Lcom/bytedance/sdk/openadsdk/o/b;

    .line 343
    new-instance v3, Lcom/bytedance/sdk/component/e/f;

    const/16 v4, 0xa

    const/4 v5, 0x1

    invoke-direct {v3, v2, v0, v4, v5}, Lcom/bytedance/sdk/component/e/f;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;II)V

    .line 344
    new-instance v2, Lcom/bytedance/sdk/openadsdk/o/g$2;

    const-string v4, "processCacheNetWorkConcurrent"

    invoke-direct {v2, p0, v4, v3}, Lcom/bytedance/sdk/openadsdk/o/g$2;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;Ljava/lang/String;Lcom/bytedance/sdk/component/e/f;)V

    invoke-static {v2}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;)V

    .line 351
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_7

    const-string v2, "TAG_PROXY_ProxyTask"

    const-string v4, "fire download in process cache task"

    .line 352
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    move-object v3, v0

    :cond_7
    :goto_1
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 363
    :try_start_0
    new-instance v4, Lcom/bytedance/sdk/openadsdk/o/h;

    const-string v5, "r"

    invoke-direct {v4, p2, v5}, Lcom/bytedance/sdk/openadsdk/o/h;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 364
    :try_start_1
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    int-to-long v5, p2

    invoke-virtual {v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/o/h;->a(J)V

    .line 365
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget p2, p2, Lcom/bytedance/sdk/openadsdk/o/i$a;->e:I

    if-lez p2, :cond_8

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget p2, p2, Lcom/bytedance/sdk/openadsdk/o/i$a;->e:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_2

    :cond_8
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    .line 367
    :goto_2
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    if-ge p2, p1, :cond_10

    .line 368
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    .line 370
    invoke-virtual {v4, v2}, Lcom/bytedance/sdk/openadsdk/o/h;->a([B)I

    move-result p2

    if-gtz p2, :cond_f

    .line 373
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:Lcom/bytedance/sdk/openadsdk/o/b;

    if-eqz p2, :cond_b

    .line 375
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/b;->i()Lcom/bytedance/sdk/openadsdk/o/c/b;

    move-result-object v0

    if-nez v0, :cond_a

    .line 380
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/b;->h()Lcom/bytedance/sdk/openadsdk/o/h$a;

    move-result-object v0

    if-nez v0, :cond_9

    goto :goto_3

    .line 383
    :cond_9
    throw v0

    .line 377
    :cond_a
    throw v0

    :cond_b
    :goto_3
    if-eqz p2, :cond_d

    .line 387
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/b;->b()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/b;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_6

    .line 394
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    .line 396
    iget-object v0, p2, Lcom/bytedance/sdk/openadsdk/o/b;->m:Ljava/lang/Object;

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 398
    :try_start_2
    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/o/b;->m:Ljava/lang/Object;

    const-wide/16 v5, 0x3e8

    invoke-virtual {p2, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_0
    move-exception p2

    .line 400
    :try_start_3
    invoke-virtual {p2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 402
    :goto_4
    monitor-exit v0

    goto :goto_7

    :goto_5
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1

    .line 388
    :cond_d
    :goto_6
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_e

    const-string p1, "TAG_PROXY_ProxyTask"

    const-string p2, "download task has finished!!!"

    .line 389
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_e
    new-instance p1, Lcom/bytedance/sdk/openadsdk/o/c/c;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "illegal state download task has finished, rawKey: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", url: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/c/c;-><init>(Ljava/lang/String;)V

    throw p1

    .line 404
    :cond_f
    invoke-virtual {p3, v2, v1, p2}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b([BII)V

    .line 407
    :goto_7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    goto :goto_2

    .line 410
    :cond_10
    sget-boolean p2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p2, :cond_11

    const-string p2, "TAG_PROXY_ProxyTask"

    .line 411
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "read cache file complete: "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ", "

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_11
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 417
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    if-eqz v3, :cond_12

    .line 422
    :try_start_5
    invoke-virtual {v3}, Lcom/bytedance/sdk/component/e/f;->get()Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_8

    :catchall_1
    move-exception p1

    .line 424
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_12
    :goto_8
    return-void

    :catchall_2
    move-exception p1

    move-object v0, v4

    goto :goto_9

    :catchall_3
    move-exception p1

    :goto_9
    if-eqz v0, :cond_13

    .line 417
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    :cond_13
    if-eqz v3, :cond_14

    .line 422
    :try_start_6
    invoke-virtual {v3}, Lcom/bytedance/sdk/component/e/f;->get()Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_a

    :catchall_4
    move-exception p2

    .line 424
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 425
    :cond_14
    :goto_a
    throw p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/o/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/h$a;,
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/openadsdk/o/c/b;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/o/i;->a:Lcom/bytedance/sdk/openadsdk/o/i$c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/o/i$c;->a:Ljava/lang/String;

    const-string v1, "HEAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V

    goto :goto_0

    .line 218
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->c(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V

    :goto_0
    return-void
.end method

.method private a(ZIIII)V
    .locals 0

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/o/g$b;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    const-string v0, "TAG_PROXY_ProxyTask"

    .line 146
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_7

    .line 147
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    .line 149
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l;->b()Lcom/bytedance/sdk/openadsdk/o/l$a;

    move-result-object v1

    const/4 v3, 0x1

    .line 151
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/o/c/c; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/bytedance/sdk/openadsdk/o/c/d; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/bytedance/sdk/openadsdk/o/h$a; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/bytedance/sdk/openadsdk/o/c/b; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/bytedance/sdk/component/adnet/err/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v3

    :catch_0
    move-exception v1

    .line 196
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_0

    .line 197
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    .line 192
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_0

    .line 193
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception p1

    .line 185
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v1, :cond_1

    .line 186
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v2

    :catch_3
    move-exception v1

    .line 179
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_2

    .line 180
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_2
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:Z

    .line 183
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_4
    move-exception p1

    .line 173
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v1, :cond_3

    .line 174
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return v3

    :catch_5
    move-exception v2

    .line 157
    instance-of v3, v2, Ljava/net/SocketTimeoutException;

    if-eqz v3, :cond_4

    .line 158
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l$a;->b()V

    .line 161
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 162
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Canceled"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "okhttp call canceled"

    .line 164
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 166
    :cond_5
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 170
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_6
    move-exception v2

    .line 154
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l$a;->a()V

    .line 155
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    return v2
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/o/b/a;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    const-string v0, "TAG_PROXY_ProxyTask"

    if-eqz p1, :cond_1

    .line 266
    sget-boolean p3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p3, :cond_0

    const-string p3, "get header from db"

    .line 267
    invoke-static {v0, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;I)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lcom/bytedance/sdk/openadsdk/o/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, -0x1

    const/4 v1, 0x0

    const-string v2, "HEAD"

    .line 271
    invoke-virtual {p0, p3, v1, p1, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/l$a;IILjava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/e/a;

    move-result-object p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 274
    :cond_2
    :try_start_0
    invoke-static {p1, v1, v1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;ZZ)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 278
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-static {p1, p3, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;Lcom/bytedance/sdk/openadsdk/o/b/c;Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object p3

    .line 279
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v1, :cond_3

    const-string v1, "get header from network"

    .line 280
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_3
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    invoke-static {p3, p2}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;I)Ljava/lang/String;

    move-result-object p2

    sget-object p3, Lcom/bytedance/sdk/openadsdk/o/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    return-object p2

    .line 276
    :cond_4
    :try_start_1
    new-instance p2, Lcom/bytedance/sdk/openadsdk/o/c/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rawKey: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", url: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/bytedance/sdk/openadsdk/o/c/c;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p2

    .line 284
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    throw p2
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/c/d;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object v0

    .line 225
    invoke-direct {p0, v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)[B

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 227
    array-length v1, p2

    invoke-virtual {p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->a([BII)V

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/o/h$a;,
            Lcom/bytedance/sdk/openadsdk/o/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/openadsdk/o/c/b;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 232
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:Z

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a/a;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 235
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 236
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v4, v4, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object v7

    .line 237
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v5

    int-to-long v2, v5

    sub-long v2, v0, v2

    long-to-int v4, v2

    if-nez v7, :cond_0

    const/4 v8, -0x1

    goto :goto_0

    .line 239
    :cond_0
    iget v8, v7, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    .line 240
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v9

    int-to-long v9, v9

    cmp-long v11, v0, v9

    if-lez v11, :cond_2

    .line 242
    sget-boolean v9, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v9, :cond_1

    .line 243
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cache hit, remainSize: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TAG_PROXY_ProxyTask"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x1

    long-to-int v9, v0

    move-object v0, p0

    move v1, v2

    move v2, v4

    move v3, v8

    move v4, v9

    .line 246
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ZIIII)V

    .line 247
    invoke-direct {p0, v7, v6, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/b/a;Ljava/io/File;Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V

    return-void

    :cond_2
    const/4 v2, 0x0

    long-to-int v6, v0

    move-object v0, p0

    move v1, v2

    move v2, v4

    move v3, v8

    move v4, v6

    .line 251
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ZIIII)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 253
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ZIIII)V

    .line 256
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V

    return-void
.end method

.method private d(Lcom/bytedance/sdk/openadsdk/o/g$b;Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/o/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/openadsdk/o/c/b;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 431
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->i()V

    .line 432
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 433
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v2

    .line 434
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->e:I

    const-string v4, "GET"

    invoke-virtual {p0, p2, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/l$a;IILjava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/e/a;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 440
    :try_start_0
    invoke-static {v3, v6, v4}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;ZZ)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const-string v7, ", rawKey: "

    if-nez v4, :cond_e

    .line 445
    :try_start_1
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->f()I

    move-result v9

    invoke-virtual {v4, v8, v9}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object v4

    .line 446
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;)I

    move-result v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    const-string v9, "TAG_PROXY_ProxyTask"

    if-eqz v4, :cond_2

    .line 447
    :try_start_2
    iget v10, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    if-eq v10, v8, :cond_2

    .line 448
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_1

    .line 449
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Content-Length not match, old: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", key: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v9, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/openadsdk/o/c/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Content-Length not match, old length: "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v9, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ", new length: "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", currentUrl: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ", previousInfo: "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->e:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/c/b;-><init>(Ljava/lang/String;)V

    throw p1

    .line 456
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->a()Z

    move-result p2

    if-nez p2, :cond_3

    .line 457
    invoke-static {v3, v2}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;I)Ljava/lang/String;

    move-result-object p2

    .line 458
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    .line 459
    sget-object v2, Lcom/bytedance/sdk/openadsdk/o/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    .line 460
    array-length v2, p2

    invoke-virtual {p1, p2, v6, v2}, Lcom/bytedance/sdk/openadsdk/o/g$b;->a([BII)V

    .line 463
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    .line 465
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {p2, v2}, Lcom/bytedance/sdk/openadsdk/o/a/a;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object p2

    .line 466
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    const-string v4, ", from: "

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    :try_start_3
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v2

    int-to-long v10, v2

    cmp-long v2, v7, v10

    if-ltz v2, :cond_4

    .line 467
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v8, v8, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v8, v8, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-static {v3, v2, v7, v8}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;Lcom/bytedance/sdk/openadsdk/o/b/c;Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 471
    :try_start_4
    new-instance v2, Lcom/bytedance/sdk/openadsdk/o/h;

    const-string v7, "rwd"

    invoke-direct {v2, p2, v7}, Lcom/bytedance/sdk/openadsdk/o/h;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/bytedance/sdk/openadsdk/o/h$a; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 472
    :try_start_5
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v2, v7, v8}, Lcom/bytedance/sdk/openadsdk/o/h;->a(J)V
    :try_end_5
    .catch Lcom/bytedance/sdk/openadsdk/o/h$a; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_1

    :catch_0
    move-exception v7

    goto :goto_0

    :catch_1
    move-exception v7

    move-object v2, v5

    .line 474
    :goto_0
    :try_start_6
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/o/h$a;->printStackTrace()V

    move-object v2, v5

    .line 478
    :goto_1
    sget-boolean v7, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v7, :cond_6

    .line 479
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can write to cache file in network task, cache file size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v9, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_2

    .line 481
    :cond_4
    :try_start_7
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_5

    .line 482
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "can\'t write to cache file in network task, cache file size: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v9, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    :cond_5
    move-object v2, v5

    .line 485
    :cond_6
    :goto_2
    :try_start_8
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->f()I

    move-result v7

    invoke-virtual {p2, v4, v7}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object p2

    if-nez p2, :cond_7

    const/4 p2, 0x0

    goto :goto_3

    .line 486
    :cond_7
    iget p2, p2, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    :goto_3
    const/16 v4, 0x2000

    new-array v4, v4, [B

    .line 490
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    const/4 v8, 0x0

    .line 492
    :goto_4
    :try_start_9
    invoke-virtual {v7, v4}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-ltz v10, :cond_b

    .line 493
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    if-lez v10, :cond_a

    .line 495
    invoke-virtual {p1, v4, v6, v10}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b([BII)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    add-int/2addr v8, v10

    if-eqz v2, :cond_9

    .line 499
    :try_start_a
    invoke-virtual {v2, v4, v6, v10}, Lcom/bytedance/sdk/openadsdk/o/h;->a([BII)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v10

    .line 501
    :try_start_b
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 504
    :try_start_c
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_8

    .line 505
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "append to cache file error in network task!!! "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v10}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_8
    move-object v2, v5

    goto :goto_5

    :catchall_1
    move-exception p1

    goto :goto_6

    .line 511
    :cond_9
    :goto_5
    :try_start_d
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g$b;->b()I

    move-result v10

    invoke-virtual {p0, p2, v10}, Lcom/bytedance/sdk/openadsdk/o/g;->a(II)V

    .line 513
    :cond_a
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->e()V

    goto :goto_4

    .line 516
    :cond_b
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_c

    const-string p1, "read from net complete!"

    .line 517
    invoke-static {v9, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->c()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 522
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    if-eqz v2, :cond_d

    .line 525
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    .line 528
    :cond_d
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1, v8}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 529
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    return-void

    :catchall_2
    move-exception p1

    move-object v5, v2

    :goto_6
    move v6, v8

    goto :goto_7

    :catchall_3
    move-exception p1

    move-object v5, v2

    goto :goto_7

    .line 442
    :cond_e
    :try_start_e
    new-instance p1, Lcom/bytedance/sdk/openadsdk/o/c/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", url: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/c/c;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :catchall_4
    move-exception p1

    .line 522
    :goto_7
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    if-eqz v5, :cond_f

    .line 525
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    .line 528
    :cond_f
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p2, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 529
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    throw p1
.end method

.method private h()Lcom/bytedance/sdk/openadsdk/o/g$b;
    .locals 5

    const-string v0, "TAG_PROXY_ProxyTask"

    const/4 v1, 0x0

    .line 52
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/o/i;->a(Ljava/io/InputStream;)Lcom/bytedance/sdk/openadsdk/o/i;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    .line 53
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 55
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/bytedance/sdk/openadsdk/o/e;->a:Lcom/bytedance/sdk/openadsdk/o/a/b;

    goto :goto_0

    :cond_0
    sget-object v3, Lcom/bytedance/sdk/openadsdk/o/e;->b:Lcom/bytedance/sdk/openadsdk/o/a/c;

    :goto_0
    if-nez v3, :cond_2

    .line 57
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_1

    const-string v2, "cache is null"

    .line 58
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1

    .line 62
    :cond_2
    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    .line 64
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->b:Ljava/lang/String;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    .line 65
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i$a;->c:Ljava/lang/String;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    .line 66
    new-instance v3, Lcom/bytedance/sdk/openadsdk/o/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/o/i$a;->g:Ljava/util/List;

    invoke-direct {v3, v4}, Lcom/bytedance/sdk/openadsdk/o/l;-><init>(Ljava/util/List;)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    .line 67
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/o/i;->b:Ljava/util/List;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Ljava/util/List;

    .line 69
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_3

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request from MediaPlayer:    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/o/i;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_3
    new-instance v3, Lcom/bytedance/sdk/openadsdk/o/g$b;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v4, v4, Lcom/bytedance/sdk/openadsdk/o/i$a;->d:I

    invoke-direct {v3, v2, v4}, Lcom/bytedance/sdk/openadsdk/o/g$b;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/bytedance/sdk/openadsdk/o/i$d; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception v2

    .line 83
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/net/Socket;)V

    .line 84
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_4

    .line 85
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 88
    :goto_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_1
    move-exception v2

    .line 75
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/net/Socket;)V

    .line 76
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_6

    .line 77
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    if-nez v0, :cond_7

    move-object v0, v1

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 81
    :goto_2
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3
    return-object v1
.end method

.method private i()V
    .locals 2

    .line 540
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:Lcom/bytedance/sdk/openadsdk/o/b;

    const/4 v1, 0x0

    .line 541
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:Lcom/bytedance/sdk/openadsdk/o/b;

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/b;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 535
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/o/a;->a()V

    .line 536
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->i()V

    return-void
.end method

.method public run()V
    .locals 7

    const-string v0, "TAG_PROXY_ProxyTask"

    .line 95
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->h()Lcom/bytedance/sdk/openadsdk/o/g$b;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    .line 100
    :cond_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->n:Lcom/bytedance/sdk/openadsdk/o/g$c;

    if-eqz v2, :cond_1

    .line 101
    invoke-interface {v2, p0}, Lcom/bytedance/sdk/openadsdk/o/g$c;->a(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 104
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/o/a/a;->a(Ljava/lang/String;)V

    .line 106
    sget v2, Lcom/bytedance/sdk/openadsdk/o/e;->h:I

    if-eqz v2, :cond_3

    .line 108
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/o/i;->c:Lcom/bytedance/sdk/openadsdk/o/i$a;

    iget v4, v4, Lcom/bytedance/sdk/openadsdk/o/i$a;->a:I

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 110
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/o/a/a;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    int-to-long v5, v2

    cmp-long v2, v3, v5

    if-gez v2, :cond_3

    .line 111
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:Lcom/bytedance/sdk/openadsdk/o/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/o/d;->a(ZLjava/lang/String;)V

    .line 116
    :cond_3
    :try_start_0
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Lcom/bytedance/sdk/openadsdk/o/g$b;)Z
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/o/c/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/bytedance/sdk/component/adnet/err/VAdError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 125
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_4

    .line 126
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    .line 123
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/adnet/err/VAdError;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    .line 119
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v2, :cond_4

    .line 120
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a/a;->b(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:Lcom/bytedance/sdk/openadsdk/o/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->g()Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/d;->a(ZLjava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->a()V

    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:Ljava/net/Socket;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/net/Socket;)V

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->n:Lcom/bytedance/sdk/openadsdk/o/g$c;

    if-eqz v0, :cond_5

    .line 138
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$c;->b(Lcom/bytedance/sdk/openadsdk/o/g;)V

    :cond_5
    return-void
.end method
