.class final Lcom/bytedance/sdk/openadsdk/o/b$a;
.super Ljava/lang/Object;
.source "DownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/o/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Lcom/bytedance/sdk/openadsdk/o/l;

.field d:Lcom/bytedance/sdk/openadsdk/o/a/a;

.field e:Lcom/bytedance/sdk/openadsdk/o/b/c;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/o/i$b;",
            ">;"
        }
    .end annotation
.end field

.field g:I

.field h:Lcom/bytedance/sdk/openadsdk/o/i;

.field i:Lcom/bytedance/sdk/openadsdk/o/b$b;

.field j:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(I)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 0

    .line 309
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->g:I

    return-object p0
.end method

.method a(Lcom/bytedance/sdk/openadsdk/o/a/a;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 291
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->d:Lcom/bytedance/sdk/openadsdk/o/a/a;

    return-object p0

    .line 289
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "cache == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Lcom/bytedance/sdk/openadsdk/o/b$b;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->i:Lcom/bytedance/sdk/openadsdk/o/b$b;

    return-object p0
.end method

.method a(Lcom/bytedance/sdk/openadsdk/o/b/c;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 299
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->e:Lcom/bytedance/sdk/openadsdk/o/b/c;

    return-object p0

    .line 297
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "db == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Lcom/bytedance/sdk/openadsdk/o/i;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->h:Lcom/bytedance/sdk/openadsdk/o/i;

    return-object p0
.end method

.method a(Lcom/bytedance/sdk/openadsdk/o/l;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 1

    if-eqz p1, :cond_0

    .line 283
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->c:Lcom/bytedance/sdk/openadsdk/o/l;

    return-object p0

    .line 281
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "urls is empty"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Ljava/lang/Object;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->j:Ljava/lang/Object;

    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 1

    .line 264
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->a:Ljava/lang/String;

    return-object p0

    .line 265
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "rawKey == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/o/i$b;",
            ">;)",
            "Lcom/bytedance/sdk/openadsdk/o/b$a;"
        }
    .end annotation

    .line 304
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->f:Ljava/util/List;

    return-object p0
.end method

.method a()Lcom/bytedance/sdk/openadsdk/o/b;
    .locals 1

    .line 329
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->d:Lcom/bytedance/sdk/openadsdk/o/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->e:Lcom/bytedance/sdk/openadsdk/o/b/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->a:Ljava/lang/String;

    .line 330
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->c:Lcom/bytedance/sdk/openadsdk/o/l;

    if-eqz v0, :cond_0

    .line 333
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/b;-><init>(Lcom/bytedance/sdk/openadsdk/o/b$a;)V

    return-object v0

    .line 331
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/b$a;
    .locals 1

    .line 272
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b$a;->b:Ljava/lang/String;

    return-object p0

    .line 273
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "key == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
