.class Lcom/bytedance/sdk/openadsdk/o/b;
.super Lcom/bytedance/sdk/openadsdk/o/a;
.source "DownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/o/b$a;,
        Lcom/bytedance/sdk/openadsdk/o/b$b;
    }
.end annotation


# instance fields
.field final m:Ljava/lang/Object;

.field final n:Ljava/lang/Object;

.field private final o:I

.field private final p:Lcom/bytedance/sdk/openadsdk/o/b$b;

.field private volatile q:Lcom/bytedance/sdk/openadsdk/o/h$a;

.field private volatile r:Lcom/bytedance/sdk/openadsdk/o/c/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/o/b$a;)V
    .locals 2

    .line 43
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->d:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->e:Lcom/bytedance/sdk/openadsdk/o/b/c;

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a;-><init>(Lcom/bytedance/sdk/openadsdk/o/a/a;Lcom/bytedance/sdk/openadsdk/o/b/c;)V

    .line 45
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->g:I

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    .line 46
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->i:Lcom/bytedance/sdk/openadsdk/o/b$b;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->p:Lcom/bytedance/sdk/openadsdk/o/b$b;

    .line 47
    iput-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->m:Ljava/lang/Object;

    .line 49
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    .line 51
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->f:Ljava/util/List;

    .line 52
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->c:Lcom/bytedance/sdk/openadsdk/o/l;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    .line 53
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->h:Lcom/bytedance/sdk/openadsdk/o/i;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    .line 54
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/o/b$a;->j:Ljava/lang/Object;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->n:Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/o/h$a;,
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/openadsdk/o/c/b;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a/a;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 146
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    if-lez v3, :cond_1

    int-to-long v3, v3

    cmp-long v5, v1, v3

    if-ltz v5, :cond_1

    .line 147
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_0

    .line 148
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "no necessary to download for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", cache file size: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", max: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TAG_PROXY_DownloadTask"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    .line 152
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->f()I

    move-result v3

    .line 153
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/b;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 154
    iget v5, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    int-to-long v5, v5

    cmp-long v7, v1, v5

    if-ltz v7, :cond_3

    .line 155
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_2

    .line 156
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "file download complete, key: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TAG_PROXY_DownloadTask"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    .line 161
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->e()V

    long-to-int v5, v1

    .line 162
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    const-string v7, "GET"

    invoke-virtual {p0, p1, v5, v6, v7}, Lcom/bytedance/sdk/openadsdk/o/b;->a(Lcom/bytedance/sdk/openadsdk/o/l$a;IILjava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/e/a;

    move-result-object v6

    if-nez v6, :cond_4

    return-void

    :cond_4
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 169
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->e()V

    .line 171
    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/o/b;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    if-nez v10, :cond_5

    sget-boolean v10, Lcom/bytedance/sdk/openadsdk/o/e;->e:Z

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    :goto_0
    invoke-static {v6, v10, v8}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;ZZ)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_12

    .line 176
    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;)I

    move-result v10

    if-eqz v4, :cond_7

    .line 177
    iget v11, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    if-eq v11, v10, :cond_7

    .line 178
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v0, :cond_6

    const-string v0, "TAG_PROXY_DownloadTask"

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Content-Length not match, old: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_6
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/c/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Content-Length not match, old length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", new length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", rawKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", currentUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", previousInfo: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v4, Lcom/bytedance/sdk/openadsdk/o/b/a;->e:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/c/b;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-static {v6, p1, v4, v3}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Lcom/bytedance/sdk/openadsdk/o/e/a;Lcom/bytedance/sdk/openadsdk/o/b/c;Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    .line 186
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->b:Lcom/bytedance/sdk/openadsdk/o/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Lcom/bytedance/sdk/openadsdk/o/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/o/b/a;

    move-result-object p1

    if-nez p1, :cond_8

    const/4 p1, 0x0

    goto :goto_1

    .line 187
    :cond_8
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/o/b/a;->c:I

    .line 188
    :goto_1
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object v3

    .line 189
    new-instance v4, Lcom/bytedance/sdk/openadsdk/o/h;

    sget-boolean v10, Lcom/bytedance/sdk/openadsdk/o/e;->d:Z

    if-eqz v10, :cond_9

    const-string v10, "rwd"

    goto :goto_2

    :cond_9
    const-string v10, "rw"

    :goto_2
    invoke-direct {v4, v0, v10}, Lcom/bytedance/sdk/openadsdk/o/h;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 190
    :try_start_1
    invoke-virtual {v4, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/h;->a(J)V

    .line 191
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v0, :cond_a

    const-string v0, "TAG_PROXY_DownloadTask"

    .line 192
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "preload start from: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 198
    :goto_3
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_10

    .line 199
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->e()V

    if-lez v1, :cond_c

    .line 202
    invoke-virtual {v4, v0, v9, v1}, Lcom/bytedance/sdk/openadsdk/o/h;->a([BII)V

    add-int/2addr v5, v1

    .line 205
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->i:Lcom/bytedance/sdk/openadsdk/o/i;

    if-eqz v2, :cond_b

    .line 206
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->m:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 207
    :try_start_2
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/o/b;->m:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    .line 208
    monitor-exit v2

    goto :goto_4

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw p1

    .line 211
    :cond_b
    :goto_4
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 213
    invoke-virtual {p0, p1, v5}, Lcom/bytedance/sdk/openadsdk/o/b;->a(II)V

    .line 216
    :cond_c
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    if-lez v1, :cond_f

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    if-lt v5, v1, :cond_f

    .line 217
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_d

    const-string p1, "TAG_PROXY_DownloadTask"

    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "download, more data received, currentCacheFileSize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", max: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 231
    :cond_d
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    .line 234
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    .line 238
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->a()V

    .line 239
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_e

    const-string p1, "TAG_PROXY_DownloadTask"

    const-string v0, "cancel call"

    .line 240
    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    return-void

    .line 222
    :cond_f
    :try_start_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->e()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_3

    .line 226
    :cond_10
    :try_start_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->c()V

    .line 227
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz p1, :cond_11

    const-string p1, "TAG_PROXY_DownloadTask"

    const-string v0, "download succeed, no need to cancel call"

    .line 228
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 231
    :cond_11
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    .line 234
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    return-void

    :catchall_1
    move-exception p1

    move-object v7, v4

    const/4 v8, 0x0

    goto :goto_5

    :catchall_2
    move-exception p1

    move-object v7, v4

    goto :goto_5

    .line 173
    :cond_12
    :try_start_6
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/c/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", rawKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/c/c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception p1

    .line 231
    :goto_5
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/o/e/a;->d()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/o/g/d;->a(Ljava/io/Closeable;)V

    if-eqz v7, :cond_13

    .line 234
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/o/h;->a()V

    :cond_13
    if-eqz v8, :cond_14

    .line 238
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->a()V

    .line 239
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v0, :cond_14

    const-string v0, "TAG_PROXY_DownloadTask"

    const-string v1, "cancel call"

    .line 240
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    throw p1
.end method

.method private j()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/o/c/a;,
            Lcom/bytedance/sdk/component/adnet/err/VAdError;
        }
    .end annotation

    const-string v0, "TAG_PROXY_DownloadTask"

    .line 94
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l;->a()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 95
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->e()V

    .line 97
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->j:Lcom/bytedance/sdk/openadsdk/o/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l;->b()Lcom/bytedance/sdk/openadsdk/o/l$a;

    move-result-object v1

    .line 99
    :try_start_0
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/o/b;->a(Lcom/bytedance/sdk/openadsdk/o/l$a;)V
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/o/c/c; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/bytedance/sdk/openadsdk/o/h$a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/bytedance/sdk/openadsdk/o/c/b; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v1

    .line 134
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_1

    .line 135
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v2

    :catch_0
    move-exception v1

    .line 126
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->r:Lcom/bytedance/sdk/openadsdk/o/c/b;

    .line 127
    sget-boolean v3, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v3, :cond_2

    .line 128
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v2

    :catch_1
    move-exception v0

    .line 122
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->q:Lcom/bytedance/sdk/openadsdk/o/h$a;

    .line 123
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v0}, Lcom/bytedance/sdk/openadsdk/o/b;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v2

    :catch_2
    move-exception v2

    .line 106
    instance-of v3, v2, Ljava/net/SocketTimeoutException;

    if-eqz v3, :cond_3

    .line 107
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l$a;->b()V

    .line 110
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 111
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Canceled"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "okhttp call canceled"

    .line 113
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :cond_4
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/b;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_3
    move-exception v2

    .line 102
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/o/l$a;->a()V

    .line 103
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/b;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/o/b;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_6
    return v2
.end method


# virtual methods
.method h()Lcom/bytedance/sdk/openadsdk/o/h$a;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->q:Lcom/bytedance/sdk/openadsdk/o/h$a;

    return-object v0
.end method

.method i()Lcom/bytedance/sdk/openadsdk/o/c/b;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->r:Lcom/bytedance/sdk/openadsdk/o/c/b;

    return-object v0
.end method

.method public run()V
    .locals 5

    const-string v0, "TAG_PROXY_DownloadTask"

    .line 59
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/o/a/a;->a(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 62
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/b;->j()Z
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/o/c/a; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/bytedance/sdk/component/adnet/err/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/bytedance/sdk/component/adnet/err/VAdError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    .line 72
    sget-boolean v4, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v4, :cond_0

    .line 73
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    .line 70
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/adnet/err/VAdError;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 68
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/adnet/err/a;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v3

    .line 64
    sget-boolean v4, Lcom/bytedance/sdk/openadsdk/o/e;->c:Z

    if-eqz v4, :cond_0

    .line 65
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v1

    invoke-virtual {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 77
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->a:Lcom/bytedance/sdk/openadsdk/o/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/a/a;->b(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/b;->p:Lcom/bytedance/sdk/openadsdk/o/b$b;

    if-eqz v0, :cond_1

    .line 79
    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/b$b;->a(Lcom/bytedance/sdk/openadsdk/o/b;)V

    :cond_1
    return-void
.end method
