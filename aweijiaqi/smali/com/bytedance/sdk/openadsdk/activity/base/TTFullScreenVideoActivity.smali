.class public Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;
.super Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;
.source "TTFullScreenVideoActivity.java"


# static fields
.field private static E:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener; = null

.field private static F:Z = false


# instance fields
.field private D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;)Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;)Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 335
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->d:I

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->i(I)Z

    move-result p1

    return p1
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 424
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 425
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ax()Ljava/lang/String;

    move-result-object p1

    if-eqz v1, :cond_2

    .line 426
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    return v3

    :cond_2
    return v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .line 339
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$3;

    const-string v1, "executeMultiProcessCallback"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$3;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x5

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method private f(I)V
    .locals 2

    .line 254
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "s\u540e\u53ef\u8df3\u8fc7"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 255
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    const-string v0, "onAdShow"

    .line 269
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->i()V

    :cond_0
    return-void
.end method

.method public B()V
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 278
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->F:Z

    :cond_0
    const-string v0, "onAdVideoBarClick"

    .line 280
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public C()V
    .locals 1

    .line 292
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->y:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 295
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->y:Z

    const-string v0, "onAdClose"

    .line 297
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 2

    .line 64
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/content/Intent;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const-string v1, "is_verity_playable"

    .line 68
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->B:Z

    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 0

    .line 261
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result p1

    const/high16 p2, 0x42c80000    # 100.0f

    cmpl-float p1, p1, p2

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 262
    sput-boolean p1, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->F:Z

    :cond_0
    const-string p1, "onAdVideoBarClick"

    .line 264
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(JZ)Z
    .locals 5

    const-string v0, "TTFullScreenVideoActivity"

    const-string v1, "bindVideoAd execute"

    .line 121
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    check-cast v1, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/d;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->x()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Landroid/widget/FrameLayout;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V

    goto :goto_0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->x()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Landroid/widget/FrameLayout;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V

    :goto_0
    const/4 v0, 0x0

    .line 128
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 130
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Ljava/util/Map;)V

    .line 133
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$2;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$2;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;)V

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V

    .line 215
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->a(JZLjava/util/Map;)Z

    move-result p1

    return p1
.end method

.method protected a(Landroid/os/Bundle;)Z
    .locals 1

    .line 73
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->e()Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    :cond_0
    if-eqz p1, :cond_1

    .line 77
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    if-nez v0, :cond_1

    .line 78
    sget-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->E:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    const/4 v0, 0x0

    .line 79
    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->E:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    .line 82
    :cond_1
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/os/Bundle;)Z

    move-result p1

    return p1
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    .line 358
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$4;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$4;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected e(I)V
    .locals 6

    .line 224
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->h(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x5

    .line 228
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "tt_reward_screen_skip_tx"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v1, :cond_3

    if-lt p1, v0, :cond_2

    .line 230
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result p1

    if-nez p1, :cond_1

    .line 231
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 233
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 234
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    goto :goto_0

    .line 236
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    goto :goto_0

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 240
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    :cond_4
    if-gt p1, v0, :cond_5

    sub-int/2addr v0, p1

    .line 243
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->f(I)V

    .line 244
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    goto :goto_0

    .line 246
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 247
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V

    :goto_0
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 309
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    const/4 v0, 0x0

    .line 310
    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->E:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    return-void
.end method

.method public finish()V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->g()V

    .line 286
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->C()V

    .line 287
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->finish()V

    return-void
.end method

.method protected n()V
    .locals 1

    const-string v0, "onVideoComplete"

    .line 353
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 302
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->onDestroy()V

    .line 303
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->C()V

    const-string v0, "recycleRes"

    .line 304
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    .line 56
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->E:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    .line 59
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 315
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->onStop()V

    .line 316
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 319
    :cond_0
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k:Z

    .line 321
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Z

    .line 322
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->finish()V

    goto :goto_0

    .line 323
    :cond_1
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->F:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Z

    if-nez v0, :cond_2

    .line 324
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->F:Z

    .line 325
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->finish()V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->I()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Z

    if-nez v0, :cond_3

    .line 327
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->finish()V

    :cond_3
    :goto_0
    return-void
.end method

.method protected x()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected y()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected z()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity$1;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTFullScreenVideoActivity;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/top/b;)V

    return-void
.end method
