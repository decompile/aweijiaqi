.class public Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;
.super Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;
.source "TTRewardVideoActivity.java"


# static fields
.field private static K:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;


# instance fields
.field protected D:I

.field protected E:I

.field protected F:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

.field private G:Ljava/lang/String;

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private L:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private M:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 42
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;-><init>()V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, -0x1

    .line 60
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->M:I

    return-void
.end method

.method private E()Lorg/json/JSONObject;
    .locals 5

    .line 402
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "oversea_version_type"

    const/4 v2, 0x0

    .line 405
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "reward_name"

    .line 406
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "reward_amount"

    .line 407
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->H:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "network"

    .line 408
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "sdk_version"

    const-string v2, "3.6.1.4"

    .line 409
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "user_agent"

    .line 410
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "extra"

    .line 411
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "media_extra"

    .line 412
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "video_duration"

    .line 413
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->B()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v1, "play_start_ts"

    .line 414
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->D:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "play_end_ts"

    .line 415
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->E:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "duration"

    .line 416
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->u()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "user_id"

    .line 417
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->J:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "trans_id"

    .line 418
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 419
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Landroid/content/Context;Lorg/json/JSONObject;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;Ljava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;Ljava/lang/String;ZILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct/range {p0 .. p6}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(Ljava/lang/String;ZILjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;ZILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(ZILjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;ZZ)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;ZILjava/lang/String;ILjava/lang/String;)V
    .locals 10

    .line 510
    new-instance v9, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$5;

    const-string v2, "executeMultiProcessCallback"

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$5;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;ILjava/lang/String;)V

    const/4 v0, 0x5

    invoke-static {v9, v0}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method private a(ZILjava/lang/String;ILjava/lang/String;)V
    .locals 9

    .line 523
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    new-instance v8, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$6;

    move-object v1, v8

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$6;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;ZILjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v8}, Lcom/bytedance/sdk/component/utils/u;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .line 140
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 141
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 155
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->l()V

    if-eqz p1, :cond_1

    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->j()V

    .line 158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 160
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/widget/d;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/widget/d;-><init>(Landroid/content/Context;)V

    .line 161
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    if-eqz p1, :cond_2

    .line 163
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d:I

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(ILcom/bytedance/sdk/openadsdk/core/widget/d;)V

    goto :goto_0

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    const-string v2, "\u89c2\u770b\u5b8c\u6574\u89c6\u9891\u624d\u80fd\u83b7\u5f97\u5956\u52b1"

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/widget/d;

    move-result-object v1

    const-string v2, "\u7ee7\u7eed\u89c2\u770b"

    .line 166
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/widget/d;

    move-result-object v1

    const-string v2, "\u653e\u5f03\u5956\u52b1"

    .line 167
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/widget/d;

    .line 169
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$2;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$2;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;Lcom/bytedance/sdk/openadsdk/core/widget/d;ZZ)V

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->a(Lcom/bytedance/sdk/openadsdk/core/widget/d$a;)Lcom/bytedance/sdk/openadsdk/core/widget/d;

    move-result-object p1

    .line 197
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->show()V

    return-void

    :cond_3
    :goto_1
    if-eqz p1, :cond_5

    if-nez p2, :cond_4

    const-string p1, "onSkippedVideo"

    .line 145
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    .line 147
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->c()V

    return-void

    .line 150
    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, ""

    const/4 v5, 0x0

    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    .line 506
    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(Ljava/lang/String;ZILjava/lang/String;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    const-string v0, "onAdShow"

    .line 355
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->i()V

    :cond_0
    return-void
.end method

.method public B()V
    .locals 1

    const-string v0, "onAdVideoBarClick"

    .line 429
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected C()V
    .locals 7

    .line 365
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->L:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 372
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    .line 375
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->H:I

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->G:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, ""

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(ZILjava/lang/String;ILjava/lang/String;)V

    return-void

    .line 379
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->E()Lorg/json/JSONObject;

    move-result-object v0

    .line 380
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$4;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$4;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;)V

    invoke-interface {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/p$c;)V

    return-void
.end method

.method public D()V
    .locals 1

    .line 445
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->y:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 448
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->y:Z

    const-string v0, "onAdClose"

    .line 449
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected a(JJ)V
    .locals 3

    .line 213
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->M:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 214
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/j/a;->f:I

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->M:I

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v2, p3, v0

    if-gtz v2, :cond_1

    return-void

    :cond_1
    const-wide/16 v0, 0x7530

    cmp-long v2, p3, v0

    if-ltz v2, :cond_2

    const-wide/16 v0, 0x6978

    cmp-long v2, p1, v0

    if-ltz v2, :cond_2

    .line 224
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->C()V

    return-void

    :cond_2
    const-wide/16 v0, 0x64

    mul-long p1, p1, v0

    long-to-float p1, p1

    long-to-float p2, p3

    div-float/2addr p1, p2

    .line 229
    iget p2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->M:I

    int-to-float p2, p2

    cmpl-float p1, p1, p2

    if-ltz p1, :cond_3

    .line 230
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->C()V

    :cond_3
    return-void
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 2

    .line 202
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/content/Intent;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "reward_name"

    .line 206
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->G:Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "reward_amount"

    .line 207
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->H:I

    const-string v0, "media_extra"

    .line 208
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->I:Ljava/lang/String;

    const-string v0, "user_id"

    .line 209
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->J:Ljava/lang/String;

    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 0

    const-string p1, "onAdVideoBarClick"

    .line 350
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(JZ)Z
    .locals 5

    const-string v0, "TTRewardVideoActivity"

    const-string v1, "bindVideoAd execute"

    .line 238
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Landroid/widget/FrameLayout;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V

    .line 241
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 243
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 245
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Ljava/util/Map;)V

    .line 246
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$3;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$3;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;)V

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V

    .line 340
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->a(JZLjava/util/Map;)Z

    move-result p1

    if-eqz p1, :cond_1

    if-nez p3, :cond_1

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    const-wide/16 v0, 0x3e8

    div-long/2addr p2, v0

    long-to-int p3, p2

    iput p3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->D:I

    :cond_1
    return p1
.end method

.method protected a(Landroid/os/Bundle;)Z
    .locals 1

    .line 87
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->d()Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->F:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    :cond_0
    if-eqz p1, :cond_1

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->F:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    if-nez v0, :cond_1

    .line 92
    sget-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->K:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->F:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    const/4 v0, 0x0

    .line 93
    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->K:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    .line 96
    :cond_1
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/os/Bundle;)Z

    move-result p1

    return p1
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    .line 539
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$7;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$7;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 462
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    const/4 v0, 0x0

    .line 463
    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->K:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    return-void
.end method

.method public finish()V
    .locals 0

    .line 439
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->D()V

    .line 440
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->finish()V

    return-void
.end method

.method protected n()V
    .locals 1

    const-string v0, "onVideoComplete"

    .line 467
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public o()V
    .locals 9

    .line 483
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->x()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 488
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/j/a;->f:I

    .line 489
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->k(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 493
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->B()D

    move-result-wide v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 494
    iget v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->v:I

    int-to-double v7, v7

    div-double/2addr v7, v1

    sub-double/2addr v5, v7

    mul-double v5, v5, v3

    int-to-double v0, v0

    cmpl-double v2, v5, v0

    if-ltz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 498
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->b(I)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_3

    .line 501
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->p()V

    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 454
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->onDestroy()V

    .line 455
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->D()V

    const-string v0, "recycleRes"

    .line 456
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    .line 79
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->F:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->K:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    .line 82
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public p()V
    .locals 0

    .line 434
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->C()V

    return-void
.end method

.method protected x()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected y()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected z()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity$1;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTRewardVideoActivity;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Lcom/bytedance/sdk/openadsdk/component/reward/top/b;)V

    return-void
.end method
