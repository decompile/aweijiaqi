.class public Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;
.super Landroid/app/Activity;
.source "TTPlayableWebPageActivity.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/core/b/d;
.implements Lcom/bytedance/sdk/openadsdk/j/f;


# static fields
.field private static final K:Lcom/bytedance/sdk/openadsdk/m/e$a;


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private B:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:I

.field private F:Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;

.field private G:Lcom/bytedance/sdk/openadsdk/m/f;

.field private H:Z

.field private I:Lcom/bytedance/sdk/openadsdk/e/q;

.field private J:Lcom/bytedance/sdk/openadsdk/e/j;

.field a:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

.field protected b:Lcom/bytedance/sdk/openadsdk/j/g;

.field protected c:Lcom/bytedance/sdk/openadsdk/j/d;

.field private d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

.field private e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

.field private f:Z

.field private g:Z

.field private h:Landroid/widget/RelativeLayout;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/content/Context;

.field private l:I

.field private m:Landroid/widget/ProgressBar;

.field private n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Lcom/bytedance/sdk/openadsdk/core/w;

.field private r:Lcom/bytedance/sdk/openadsdk/core/w;

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private final v:Ljava/lang/String;

.field private w:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private x:Lcom/bytedance/sdk/component/utils/u;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 165
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$1;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$1;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->K:Lcom/bytedance/sdk/openadsdk/m/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 100
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    .line 110
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->f:Z

    .line 111
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->g:Z

    const-string v0, "embeded_ad"

    .line 132
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->v:Ljava/lang/String;

    .line 134
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    .line 138
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 140
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->D:I

    .line 141
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->E:I

    .line 154
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    .line 177
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$5;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$5;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->c:Lcom/bytedance/sdk/openadsdk/j/d;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;I)Landroid/os/Message;
    .locals 0

    .line 100
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b(I)Landroid/os/Message;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 13

    .line 410
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "web_title"

    const-string v2, "url"

    const/4 v3, 0x0

    const-string v4, "ad_pending_download"

    const/4 v5, -0x1

    const-string v6, "source"

    const-string v7, "log_extra"

    const-string v8, "adid"

    const/4 v9, 0x1

    const-string v10, "sdk_version"

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    iput v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->l:I

    .line 413
    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    .line 414
    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->p:Ljava/lang/String;

    .line 415
    invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    iput v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    .line 416
    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    iput-boolean v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    .line 417
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    const-string v11, "gecko_id"

    .line 418
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->C:Ljava/lang/String;

    .line 419
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->u:Ljava/lang/String;

    .line 423
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v11

    const-string v12, "TTPWPActivity"

    if-eqz v11, :cond_1

    const-string v11, "multi_process_materialmeta"

    .line 424
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 428
    :try_start_0
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Lcom/bytedance/sdk/openadsdk/core/b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v11, "TTPlayableWebPageActivity - onCreate MultiGlobalInfo : "

    .line 430
    invoke-static {v12, v11, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 434
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->c()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 435
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->g()V

    :cond_2
    :goto_0
    if-eqz p1, :cond_3

    .line 440
    :try_start_1
    invoke-virtual {p1, v10, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->l:I

    .line 441
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    .line 442
    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->p:Ljava/lang/String;

    .line 443
    invoke-virtual {p1, v6, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    .line 444
    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    .line 445
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    .line 446
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->u:Ljava/lang/String;

    const-string v0, "material_meta"

    const/4 v1, 0x0

    .line 448
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 449
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 450
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    nop

    .line 456
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez p1, :cond_4

    const-string p1, "material is null, no data to display"

    .line 457
    invoke-static {v12, p1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->finish()V

    .line 461
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->m()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    .line 463
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Ljava/lang/String;)V
    .locals 0

    .line 100
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;->a(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;->b(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a;->a(Landroid/webkit/WebView;)V

    .line 716
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->l:I

    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/r/h;->a(Landroid/webkit/WebView;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 718
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 719
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .line 724
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v1, "embeded_ad"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, p1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Z)Z
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->f:Z

    return p1
.end method

.method private b(I)Landroid/os/Message;
    .locals 2

    .line 218
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    .line 219
    iput v1, v0, Landroid/os/Message;->what:I

    .line 220
    iput p1, v0, Landroid/os/Message;->arg1:I

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/component/utils/u;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Z)Z
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    return p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Z)Z
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    return p1
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/openadsdk/core/w;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    return-object p0
.end method

.method private d()V
    .locals 5

    .line 282
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_0

    return-void

    .line 286
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    sget-object v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->K:Lcom/bytedance/sdk/openadsdk/m/e$a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->a(Lcom/bytedance/sdk/openadsdk/m/e$a;)V

    .line 290
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$6;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$6;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    .line 331
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$7;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$7;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    .line 338
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v2, v3, v1, v0}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/m/b;Lcom/bytedance/sdk/openadsdk/m/a;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    .line 340
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    .line 341
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    .line 342
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    .line 343
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sdkEdition"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    .line 344
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    const/4 v1, 0x0

    .line 345
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    .line 346
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 347
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->b(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    .line 349
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 350
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/m/f;

    .line 353
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->j()Ljava/util/Set;

    move-result-object v0

    .line 355
    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 357
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "subscribe_app_ad"

    .line 360
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "adInfo"

    .line 361
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "webview_time_track"

    .line 362
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "download_app_ad"

    .line 363
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_0

    .line 367
    :cond_4
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/w;->b()Lcom/bytedance/sdk/component/a/q;

    move-result-object v3

    new-instance v4, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$8;

    invoke-direct {v4, p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$8;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v3, v2, v4}, Lcom/bytedance/sdk/component/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/a/e;)Lcom/bytedance/sdk/component/a/q;

    goto :goto_0

    :cond_5
    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Z)Z
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->z:Z

    return p1
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Landroid/widget/ProgressBar;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->m:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method private e()V
    .locals 3

    .line 386
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 387
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v2, "embeded_ad"

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 389
    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    if-eqz v1, :cond_0

    .line 390
    check-cast v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f(Z)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Z)Z
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->g:Z

    return p1
.end method

.method private f()V
    .locals 5

    .line 400
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/j/a;->r:I

    if-ltz v0, :cond_0

    .line 403
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    const/4 v2, 0x1

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->h:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Z
    .locals 0

    .line 100
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->f:Z

    return p0
.end method

.method private g()V
    .locals 8

    .line 472
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-nez v0, :cond_0

    return-void

    .line 475
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/j;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {v1, p0, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/j;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/webkit/WebView;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/j;->b(Z)Lcom/bytedance/sdk/openadsdk/e/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    const-string v1, "embeded_ad"

    .line 476
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Lcom/bytedance/sdk/openadsdk/e/q;)V

    .line 478
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v7, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$9;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    move-object v1, v7

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$9;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    invoke-virtual {v0, v7}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 564
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)V

    .line 565
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)V

    .line 566
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->j()V

    .line 567
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->loadUrl(Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$10;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-direct {v1, p0, v2, v3}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$10;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Lcom/bytedance/sdk/openadsdk/core/w;Lcom/bytedance/sdk/openadsdk/e/j;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->i()V

    return-void
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Ljava/lang/String;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    return-object p0
.end method

.method private h()V
    .locals 2

    const-string v0, "tt_playable_loading"

    .line 621
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    const-string v0, "tt_browser_webview"

    .line 622
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const-string v0, "tt_browser_webview_loading"

    .line 623
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const-string v0, "tt_playable_ad_close_layout"

    .line 624
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->h:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 626
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$11;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$11;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const-string v0, "tt_browser_progress"

    .line 638
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->m:Landroid/widget/ProgressBar;

    const-string v0, "tt_playable_ad_dislike"

    .line 639
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->i:Landroid/view/View;

    .line 640
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$12;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$12;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "tt_playable_ad_mute"

    .line 646
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->j:Landroid/widget/ImageView;

    .line 647
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$2;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 656
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setBackgroundColor(I)V

    .line 657
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setBackgroundColor(I)V

    .line 658
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 659
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Ljava/lang/String;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->C:Ljava/lang/String;

    return-object p0
.end method

.method private i()V
    .locals 2

    .line 702
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 705
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 706
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 707
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    :cond_1
    return-void
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)I
    .locals 2

    .line 100
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->D:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->D:I

    return v0
.end method

.method private j()V
    .locals 9

    .line 728
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-nez v0, :cond_0

    return-void

    .line 731
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k()Ljava/lang/String;

    move-result-object v0

    .line 732
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 735
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v8, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$4;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->r:Lcom/bytedance/sdk/openadsdk/core/w;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    const/4 v7, 0x0

    move-object v2, v8

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$4;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/j;)V

    invoke-virtual {v1, v8}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 762
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->F:Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;

    return-object p0
.end method

.method private k()Ljava/lang/String;
    .locals 11

    .line 766
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->o()Ljava/lang/String;

    move-result-object v0

    .line 768
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 771
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 772
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/b;->e()I

    move-result v2

    .line 773
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/b;->f()I

    move-result v3

    .line 774
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 776
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v6

    .line 777
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/e/b;->d()Ljava/lang/String;

    move-result-object v7

    .line 778
    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v8

    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v8

    .line 779
    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v9

    .line 780
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v0, "?appname="

    .line 781
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "&stars="

    .line 782
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v0, "&comments="

    .line 783
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v0, "&icon="

    .line 784
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "&downloading="

    .line 785
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v0, "&id="

    .line 786
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "&pkg_name="

    .line 787
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "&download_url="

    .line 788
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "&name="

    .line 789
    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 790
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Lcom/bytedance/sdk/openadsdk/e/q;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    return-object p0
.end method

.method private l()V
    .locals 1

    .line 794
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 795
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->g()V

    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)I
    .locals 2

    .line 100
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->E:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->E:I

    return v0
.end method

.method private m()V
    .locals 5

    .line 800
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 801
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 803
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v3, 0x3

    const-string v4, "embeded_ad"

    invoke-direct {v1, v3, v4, v2}, Lcom/bytedance/sdk/openadsdk/e/q;-><init>(ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    .line 805
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/w;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    .line 806
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 807
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v1

    .line 808
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    .line 809
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->p:Ljava/lang/String;

    .line 810
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    .line 811
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(I)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    .line 812
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/b/d;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    .line 813
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->c:Lcom/bytedance/sdk/openadsdk/j/d;

    .line 814
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/j/d;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 815
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 816
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->i(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 817
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/w;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->r:Lcom/bytedance/sdk/openadsdk/core/w;

    .line 818
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 819
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    .line 820
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->p:Ljava/lang/String;

    .line 821
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    .line 822
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/b/d;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    .line 823
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(I)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    const/4 v1, 0x0

    .line 824
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->c(Z)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    .line 825
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 826
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 827
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->i(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 829
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d()V

    return-void
.end method

.method private n()V
    .locals 8

    .line 1000
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-nez v1, :cond_0

    goto :goto_0

    .line 1003
    :cond_0
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 1006
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/webkit/WebView;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-nez v5, :cond_2

    return-void

    .line 1010
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-string v3, "embeded_ad"

    const-string v4, "playable_show_status"

    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;ZI)V

    :cond_3
    :goto_0
    return-void
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n()V

    return-void
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Z
    .locals 0

    .line 100
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    return p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;)Z
    .locals 0

    .line 100
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->g:Z

    return p0
.end method


# virtual methods
.method protected a()V
    .locals 7

    .line 663
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    if-nez v0, :cond_0

    return-void

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->f(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 668
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->a()V

    return-void

    .line 672
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->b()V

    .line 675
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 676
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$3;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    const-string v5, "embeded_ad"

    move-object v1, v0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity$3;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 691
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 692
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 693
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 696
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->h(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 697
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    return-void
.end method

.method public a(I)V
    .locals 0

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 1047
    :goto_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b(Z)V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 4

    .line 965
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 970
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playable hidden loading , type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    .line 972
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 973
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v2, "remove_loading_page_type"

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    const-string v2, "playable_url"

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v2, "embeded_ad"

    const-string v3, "remove_loading_page"

    invoke-static {p0, p1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->j(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 977
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 978
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->n:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    if-eqz p1, :cond_2

    .line 979
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->a()V

    goto :goto_0

    .line 967
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->h:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    const/4 v0, 0x1

    .line 989
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    .line 990
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->z:Z

    if-nez p1, :cond_0

    .line 992
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    const/4 v0, 0x0

    const-string v1, "\u7a0d\u540e\u5f00\u59cb\u4e0b\u8f7d"

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 994
    :cond_0
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->z:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz p1, :cond_1

    .line 995
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->g()V

    :cond_1
    return-void
.end method

.method protected b()V
    .locals 1

    .line 1016
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1019
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    if-nez v0, :cond_1

    .line 1020
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->c()V

    .line 1022
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->showDislikeDialog()V

    return-void
.end method

.method protected b(Z)V
    .locals 2

    .line 1034
    :try_start_0
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    if-eqz p1, :cond_0

    .line 1036
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    const-string v1, "tt_mute"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    const-string v1, "tt_unmute"

    .line 1037
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1038
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1039
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Z)Lcom/bytedance/sdk/openadsdk/m/f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1041
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method c()V
    .locals 4

    .line 1026
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aG()Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object v1

    const-string v2, "embeded_ad"

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 905
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 906
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 907
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->m()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 616
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 226
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 228
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->requestWindowFeature(I)Z

    .line 229
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 230
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x1000000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 231
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    .line 235
    :goto_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a(Landroid/os/Bundle;)V

    .line 237
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez p1, :cond_0

    return-void

    .line 241
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->i(Lcom/bytedance/sdk/openadsdk/core/e/m;)I

    move-result p1

    if-eqz p1, :cond_3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 253
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 250
    :cond_2
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 243
    :cond_3
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt p1, v1, :cond_4

    const/16 p1, 0xe

    .line 244
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 246
    :cond_4
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->setRequestedOrientation(I)V

    .line 259
    :goto_1
    iput-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    const-string p1, "tt_activity_ttlandingpage_playable"

    .line 261
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->setContentView(I)V

    .line 262
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->h()V

    .line 263
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->e()V

    .line 264
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->a()V

    .line 265
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->m()V

    .line 266
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->f()V

    .line 267
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->g()V

    .line 268
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz p1, :cond_5

    .line 269
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/q;->l()V

    .line 271
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a;->a()Lcom/bytedance/sdk/openadsdk/f/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/f/a;->b()Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->F:Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;

    .line 272
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/app/Activity;)V

    .line 275
    new-instance p1, Lcom/bytedance/sdk/openadsdk/j/g;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/bytedance/sdk/openadsdk/j/g;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    .line 276
    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Lcom/bytedance/sdk/openadsdk/j/f;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .line 913
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 915
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 916
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Z)V

    .line 917
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->s()V

    .line 920
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->x:Lcom/bytedance/sdk/component/utils/u;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 921
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 925
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 926
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 928
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    .line 935
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 936
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->E:I

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->D:I

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/d$a;->a(IILcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 938
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a;->a()Lcom/bytedance/sdk/openadsdk/f/a;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->F:Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/f/a;->a(Lcom/bytedance/sdk/openadsdk/preload/falconx/a/a;)V

    .line 940
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->k:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/aa;->a(Landroid/content/Context;Landroid/webkit/WebView;)V

    .line 941
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/aa;->a(Landroid/webkit/WebView;)V

    .line 942
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-eqz v0, :cond_4

    .line 943
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->destroy()V

    .line 945
    :cond_4
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 947
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    if-eqz v0, :cond_5

    .line 948
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->s()V

    .line 950
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->r:Lcom/bytedance/sdk/openadsdk/core/w;

    if-eqz v0, :cond_6

    .line 951
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->s()V

    .line 953
    :cond_6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->l()V

    .line 954
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_7

    .line 955
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->r()V

    .line 957
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    if-eqz v0, :cond_8

    .line 958
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->e()V

    .line 960
    :cond_8
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    return-void
.end method

.method protected onPause()V
    .locals 3

    .line 872
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 873
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/t;->b(Z)V

    .line 874
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    if-eqz v0, :cond_0

    .line 875
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->r()V

    .line 876
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Z)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->r:Lcom/bytedance/sdk/openadsdk/core/w;

    if-eqz v0, :cond_1

    .line 879
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->r()V

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_2

    .line 882
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Z)Lcom/bytedance/sdk/openadsdk/m/f;

    .line 883
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->p()V

    .line 886
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    if-eqz v0, :cond_3

    .line 887
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/g;->c()V

    .line 888
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Lcom/bytedance/sdk/openadsdk/j/f;)V

    :cond_3
    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 842
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 843
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 844
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->q()V

    .line 845
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->d:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    if-eqz v0, :cond_1

    .line 846
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->q:Lcom/bytedance/sdk/openadsdk/core/w;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/core/w;->b(Z)Lcom/bytedance/sdk/openadsdk/core/w;

    .line 849
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->r:Lcom/bytedance/sdk/openadsdk/core/w;

    if-eqz v0, :cond_2

    .line 850
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/w;->q()V

    .line 852
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->G:Lcom/bytedance/sdk/openadsdk/m/f;

    if-eqz v0, :cond_3

    .line 853
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->q()V

    .line 855
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    if-eqz v0, :cond_4

    .line 856
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->c()V

    .line 860
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    if-eqz v0, :cond_6

    .line 861
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Lcom/bytedance/sdk/openadsdk/j/f;)V

    .line 862
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/g;->b()V

    .line 863
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b:Lcom/bytedance/sdk/openadsdk/j/g;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/g;->d()I

    move-result v0

    if-nez v0, :cond_5

    .line 864
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    .line 866
    :cond_5
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->H:Z

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->b(Z)V

    :cond_6
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    .line 595
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    :try_start_0
    const-string v0, "material_meta"

    .line 599
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->w:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sdk_version"

    .line 600
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "adid"

    .line 601
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "log_extra"

    .line 602
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "source"

    .line 603
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "ad_pending_download"

    .line 604
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "url"

    .line 605
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "web_title"

    .line 606
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_tag"

    const-string v1, "embeded_ad"

    .line 607
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    :catchall_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 834
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 835
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 836
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->o()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 894
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 895
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->I:Lcom/bytedance/sdk/openadsdk/e/q;

    if-eqz v0, :cond_0

    .line 896
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->n()V

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTPlayableWebPageActivity;->J:Lcom/bytedance/sdk/openadsdk/e/j;

    if-eqz v0, :cond_1

    .line 899
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->d()V

    :cond_1
    return-void
.end method
