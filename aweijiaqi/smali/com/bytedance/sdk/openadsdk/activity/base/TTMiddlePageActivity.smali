.class public Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;
.super Landroid/app/Activity;
.source "TTMiddlePageActivity.java"


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

.field private c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private d:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private e:Lcom/bytedance/sdk/openadsdk/core/b/b;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 39
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->f:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-nez p0, :cond_0

    goto :goto_1

    .line 163
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aD()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 164
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Q()Lcom/bytedance/sdk/openadsdk/core/e/m$a;

    move-result-object v3

    if-eqz v1, :cond_3

    if-nez v3, :cond_2

    goto :goto_1

    .line 168
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 169
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "middle_page_material_meta"

    .line 170
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, 0x0

    .line 171
    invoke-static {p0, v0, p1}, Lcom/bytedance/sdk/component/utils/b;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/bytedance/sdk/component/utils/b$a;)Z

    return v2

    :cond_3
    :goto_1
    return v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;)Z
    .locals 0

    .line 28
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->f:Z

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;Z)Z
    .locals 0

    .line 28
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->f:Z

    return p1
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aD()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 194
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Q()Lcom/bytedance/sdk/openadsdk/core/e/m$a;

    move-result-object p0

    if-eqz v1, :cond_3

    if-nez p0, :cond_2

    goto :goto_1

    :cond_2
    return v3

    :cond_3
    :goto_1
    return v0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 3

    if-eqz p1, :cond_1

    .line 141
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 145
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    .line 148
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/q;->j(Landroid/content/Context;)F

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :catchall_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v1

    invoke-static {p0, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    .line 152
    new-instance v2, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    .line 153
    invoke-virtual {v2, p1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p1

    .line 154
    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method public static b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 179
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 182
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 183
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "middle_page_material_meta"

    .line 184
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, 0x0

    .line 185
    invoke-static {p0, v0, p1}, Lcom/bytedance/sdk/component/utils/b;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/bytedance/sdk/component/utils/b$a;)Z

    const/4 p0, 0x1

    return p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;)Lcom/bytedance/sdk/openadsdk/core/b/b;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/b/b;

    return-object p0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "tt_activity_middle_page"

    .line 44
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "middle_page_material_meta"

    .line 47
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 49
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 51
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->d:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 52
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/n;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/n;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 54
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 57
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-nez p1, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->finish()V

    return-void

    .line 61
    :cond_1
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getExpressAdView()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_2

    .line 63
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->finish()V

    return-void

    :cond_2
    const-string v0, "tt_middle_page_layout"

    .line 66
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a:Landroid/widget/LinearLayout;

    .line 67
    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressVideoView;

    if-eqz v0, :cond_3

    .line 68
    move-object v0, p1

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressVideoView;

    .line 69
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressVideoView;->getClickListener()Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/b/b;

    goto :goto_1

    .line 70
    :cond_3
    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-eqz v0, :cond_4

    .line 71
    move-object v0, p1

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 72
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getClickListener()Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->e:Lcom/bytedance/sdk/openadsdk/core/b/b;

    .line 74
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_5

    .line 75
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aD()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-eqz v0, :cond_5

    .line 77
    move-object v0, p1

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 80
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getJsObject()Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 82
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->d:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 85
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setCanInterruptVideoPlay(Z)V

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 89
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity$1;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;)V

    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    .line 124
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->render()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 129
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 131
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_1

    .line 135
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    :cond_1
    return-void
.end method
