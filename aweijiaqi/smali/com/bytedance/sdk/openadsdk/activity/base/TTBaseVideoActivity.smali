.class public abstract Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;
.super Landroid/app/Activity;
.source "TTBaseVideoActivity.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/core/video/b/b;


# instance fields
.field protected A:Z

.field protected B:Z

.field protected C:Lcom/bytedance/sdk/openadsdk/j/e;

.field private D:Lcom/bytedance/sdk/openadsdk/core/b/e;

.field private E:Landroid/view/View$OnClickListener;

.field private F:F

.field private G:I

.field private H:I

.field private I:I

.field private final J:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

.field private K:Landroid/webkit/DownloadListener;

.field protected final a:Ljava/lang/String;

.field b:Landroid/content/Context;

.field c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field d:I

.field e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field g:Landroid/widget/ProgressBar;

.field h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

.field i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

.field j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

.field k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

.field l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

.field m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

.field n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

.field o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

.field p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

.field protected final q:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final r:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final s:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected t:Lcom/bytedance/sdk/openadsdk/IListenerManager;

.field u:Z

.field v:I

.field final w:Lcom/bytedance/sdk/component/utils/u;

.field protected x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

.field protected y:Z

.field protected z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 98
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 101
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "rewarded_video"

    goto :goto_0

    :cond_0
    const-string v0, "fullscreen_interstitial_ad"

    :goto_0
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    .line 136
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 138
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    .line 140
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    .line 145
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    .line 150
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    .line 155
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    .line 160
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    .line 165
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 168
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 170
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 172
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 178
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    .line 183
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    .line 189
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y:Z

    const/4 v0, 0x1

    .line 204
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    .line 216
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$1;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->J:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    .line 1194
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$13;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$13;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->C:Lcom/bytedance/sdk/openadsdk/j/e;

    .line 1204
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$14;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$14;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->K:Landroid/webkit/DownloadListener;

    return-void
.end method

.method private C()V
    .locals 8

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "initAdType start"

    .line 372
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 377
    :cond_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H:I

    iget v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I:I

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    iget v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/reward/c/b;->a(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/e/m;IIIF)Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v1, :cond_1

    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initAdType end, type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/c;Lcom/bytedance/sdk/openadsdk/component/reward/b/d;Lcom/bytedance/sdk/openadsdk/component/reward/b/e;)V

    .line 381
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a(Lcom/bytedance/sdk/openadsdk/component/reward/view/c;)V

    .line 382
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a(Lcom/bytedance/sdk/openadsdk/core/b/e;)V

    .line 383
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->d()V

    :cond_1
    return-void
.end method

.method private D()Z
    .locals 2

    .line 493
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ap()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ap()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private E()V
    .locals 4

    .line 625
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 626
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 627
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "dynamic_show_type"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 631
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/util/Map;)V

    .line 634
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->A()V

    return-void

    :cond_2
    const-string v0, "TTBaseVideoActivity"

    const-string v1, "bindVideoAd start"

    .line 637
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->e()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(JZ)Z

    move-result v0

    .line 639
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y()Z

    move-result v1

    if-nez v1, :cond_3

    .line 640
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->A()V

    :cond_3
    if-nez v0, :cond_4

    .line 643
    invoke-virtual {p0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Z)V

    .line 644
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    const/4 v1, 0x1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(II)V

    :cond_4
    return-void
.end method

.method private F()Z
    .locals 2

    .line 823
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 826
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->A()I

    move-result v0

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private G()V
    .locals 2

    .line 848
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/app/Activity;)V

    .line 849
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->d:I

    .line 850
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    .line 851
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    .line 852
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aI()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    return-void
.end method

.method private H()F
    .locals 2

    .line 1216
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->h(Landroid/content/Context;)I

    move-result v0

    .line 1217
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private I()F
    .locals 2

    .line 1224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->i(Landroid/content/Context;)I

    move-result v0

    .line 1225
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private J()[F
    .locals 7

    .line 1448
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->j(Landroid/content/Context;)F

    move-result v0

    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    .line 1449
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H()F

    move-result v1

    .line 1450
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I()F

    move-result v2

    .line 1451
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    cmpl-float v6, v1, v2

    if-lez v6, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    if-eq v3, v6, :cond_2

    add-float/2addr v1, v2

    sub-float v2, v1, v2

    sub-float/2addr v1, v2

    .line 1459
    :cond_2
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    if-ne v3, v5, :cond_3

    int-to-float v0, v0

    sub-float/2addr v1, v0

    goto :goto_2

    :cond_3
    int-to-float v0, v0

    sub-float/2addr v2, v0

    :goto_2
    const/4 v0, 0x2

    new-array v0, v0, [F

    aput v2, v0, v4

    aput v1, v0, v5

    return-object v0
.end method

.method private K()Ljava/lang/String;
    .locals 1

    .line 1588
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "video_player"

    return-object v0

    .line 1591
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1592
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "endcard"

    return-object v0
.end method

.method private L()V
    .locals 4

    .line 1602
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->b(Z)V

    .line 1604
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1605
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;->getJsObject()Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1606
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1610
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "state"

    const/4 v3, 0x1

    .line 1611
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "playableStateChange"

    .line 1612
    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1614
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    nop

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;
    .locals 0

    .line 98
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->J:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    return-object p0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 727
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_rb_score"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    const-string v0, "click_play_star_level"

    .line 728
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_1

    .line 729
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_comment_vertical"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_2

    const-string v0, "click_play_star_nums"

    .line 730
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_1

    .line 731
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_appname"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_3

    const-string v0, "click_play_source"

    .line 732
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_1

    .line 733
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_icon"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_4

    const-string v0, "click_play_logo"

    .line 734
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_1

    .line 735
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_video_reward_bar"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_8

    .line 736
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_click_lower_non_content_layout"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_8

    .line 737
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_click_upper_non_content_layout"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_5

    goto :goto_0

    .line 739
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_download"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 740
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_start_play"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1

    .line 741
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_video_reward_container"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 742
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_video"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1

    .line 743
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_download_backup"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 744
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "fallback_endcard_click"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1

    .line 738
    :cond_8
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_start_play_bar"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 746
    :cond_9
    :goto_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Landroid/view/View;)V
    .locals 0

    .line 98
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 98
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 1164
    new-instance v10, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$12;

    const-string v2, "executeMultiProcessAppDownloadCallBack"

    move-object v0, v10

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$12;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-static {v10, v0}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4

    .line 816
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {v0, v1, v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 10

    .line 753
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 757
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_rb_score"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 758
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_comment_vertical"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 759
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_appname"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 760
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_icon"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 761
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_video_reward_bar"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 762
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_click_lower_non_content_layout"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 763
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_click_upper_non_content_layout"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 764
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_reward_ad_download"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 765
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "tt_video_reward_container"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 766
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const-string v0, "tt_reward_ad_download_backup"

    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 768
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_3

    .line 769
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 770
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v9, p1

    goto :goto_0

    :cond_3
    move-object v9, v0

    .line 772
    :goto_0
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;-><init>()V

    const/4 v1, 0x0

    .line 773
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 774
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 775
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 776
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 777
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    const-wide/16 v2, 0x0

    .line 778
    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 779
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a()Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;)[I

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 780
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;)[I

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 781
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a()Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/view/View;)[I

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 782
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/view/View;)[I

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    const/4 v2, 0x1

    .line 783
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->f(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    const/4 v3, -0x1

    .line 784
    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->g(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 785
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->h(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 786
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(Landroid/util/SparseArray;)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 787
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_1

    :cond_4
    const/4 v2, 0x2

    :goto_1
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 788
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a()Lcom/bytedance/sdk/openadsdk/core/e/f;

    move-result-object v6

    .line 789
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    const/4 v8, 0x1

    const-string v4, "click_other"

    invoke-static/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->L()V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)Z
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D()Z

    move-result p0

    return p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->E()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 260
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a()Lcom/bytedance/sdk/openadsdk/component/reward/c/a$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a()Lcom/bytedance/sdk/openadsdk/component/reward/c/a$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a$a;->a(Z)V

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->b(Z)V

    goto :goto_1

    .line 258
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->m()V

    :goto_1
    return-void
.end method

.method public a(I)V
    .locals 4

    if-gtz p1, :cond_0

    .line 1110
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    goto :goto_0

    .line 1112
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x258

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void
.end method

.method public a(J)V
    .locals 2

    .line 1129
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x2bc

    .line 1130
    iput v1, v0, Landroid/os/Message;->what:I

    .line 1131
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {v1, v0, p1, p2}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 1673
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    const/4 v1, 0x1

    const-string v2, "show_download_bar"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Z)V

    const-string v0, "rit_scene"

    .line 1674
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    .line 1675
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    const-string v1, "video_cache_url"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Ljava/lang/String;)V

    const-string v0, "multi_process_meta_md5"

    .line 1676
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 2

    .line 1068
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleMsg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTBaseVideoActivity"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x12c

    const/4 v1, 0x0

    if-eq p1, v0, :cond_5

    const/16 v0, 0x190

    if-eq p1, v0, :cond_4

    const/16 v0, 0x1f4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x258

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2bc

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1100
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->J()V

    goto :goto_0

    .line 1083
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    goto :goto_0

    .line 1072
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 1074
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 1076
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->f()V

    .line 1077
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(F)V

    .line 1079
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->i()V

    goto :goto_0

    .line 1096
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->j()V

    .line 1097
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Z)V

    goto :goto_0

    .line 1087
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->r()V

    .line 1088
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->j()V

    .line 1089
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Z)V

    .line 1090
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 1091
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p()V

    :cond_6
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 711
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$10;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$10;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected a(Z)V
    .locals 4

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "showEndCard start"

    .line 520
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 526
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 530
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 533
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 534
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->b()V

    :cond_3
    const-string v1, "showEndCard execute"

    .line 537
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_4

    return-void

    .line 545
    :cond_4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    if-eqz v1, :cond_5

    .line 546
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->resetDislikeStatus()V

    .line 550
    :cond_5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 551
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->dismiss()V

    .line 555
    :cond_6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->j()V

    .line 557
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Z)V

    .line 559
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 560
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    if-eqz p1, :cond_7

    .line 562
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 566
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->q()Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_9

    .line 568
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-nez p1, :cond_8

    const-string p1, "TimeTrackLog report Success from Android"

    .line 569
    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    const/4 v0, 0x0

    invoke-virtual {p1, v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(ZILjava/lang/String;)V

    .line 592
    :cond_8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    .line 593
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->n()V

    .line 595
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v0, 0x1f4

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Lcom/bytedance/sdk/component/utils/u;->sendEmptyMessageDelayed(IJ)Z

    return-void

    .line 574
    :cond_9
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showEndCard isEndCardLoadSuc="

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->r()Z

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " so load back up end card"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    if-nez p1, :cond_a

    const-string p1, "TimeTrackLog report 408 from backup page"

    .line 576
    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    const/16 v0, 0x198

    const-string v3, "end_card_timeout"

    invoke-virtual {p1, v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(ZILjava/lang/String;)V

    .line 579
    :cond_a
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->m()V

    .line 580
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->o()V

    .line 581
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->G()V

    .line 582
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/a;->a()V

    .line 583
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    .line 584
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 585
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 587
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->i()V

    return-void
.end method

.method protected a(JZLjava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .line 652
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    if-eqz p3, :cond_1

    .line 655
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->x()Z

    move-result v0

    if-nez v0, :cond_2

    .line 657
    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x12c

    .line 658
    iput v1, v0, Landroid/os/Message;->what:I

    .line 659
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 661
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {v0, p1, p2, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(JZ)Z

    move-result p1

    if-eqz p1, :cond_3

    if-nez p3, :cond_3

    .line 663
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "pangolin ad show "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v0, 0x0

    invoke-static {p3, v0}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/view/View;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "AdEvent"

    invoke-static {p3, p2}, Lcom/bytedance/sdk/component/utils/j;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-static {p2, p3, v0, p4}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/util/Map;)V

    .line 665
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->A()V

    :cond_3
    return p1
.end method

.method protected a(Landroid/os/Bundle;)Z
    .locals 5

    .line 326
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "TTBaseVideoActivity"

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "multi_process_materialmeta"

    .line 329
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 332
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "initData MultiGlobalInfo throws "

    .line 335
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 339
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->c()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 341
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    .line 342
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/t;->a()Lcom/bytedance/sdk/openadsdk/core/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/t;->g()V

    :cond_2
    const/4 v0, 0x1

    if-eqz p1, :cond_4

    :try_start_1
    const-string v2, "material_meta"

    .line 347
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 348
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 349
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v3, "has_show_skip_btn"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-virtual {v2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 350
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 351
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 352
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    const-string v4, "tt_reward_screen_skip_tx"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 353
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->f(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357
    :catchall_0
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->a()V

    .line 359
    :cond_4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/d;->a()Lcom/bytedance/sdk/openadsdk/core/d;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 360
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez p1, :cond_5

    const-string p1, "mMaterialMeta is null , no data to display ,the TTBaseVideoActivity finished !!"

    .line 361
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->finish()V

    const/4 p1, 0x0

    return p1

    :cond_5
    return v0
.end method

.method protected b(I)Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 1

    .line 1177
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    .line 1178
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object p1

    .line 1179
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/a;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object p1

    return-object p1
.end method

.method protected b()V
    .locals 0

    .line 272
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->v()V

    return-void
.end method

.method protected b(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "multi_process_meta_md5"

    .line 1685
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->e:Ljava/lang/String;

    .line 1686
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    const-string v1, "video_cache_url"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Ljava/lang/String;)V

    const-string v0, "is_mute"

    .line 1687
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    const-string v0, "rit_scene"

    .line 1688
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected abstract b(Ljava/lang/String;)V
.end method

.method protected b(Z)V
    .locals 3

    .line 1412
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 1418
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 1419
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 1420
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 1421
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    goto :goto_1

    .line 1423
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aM()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 1424
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1425
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->c(Z)V

    .line 1427
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    instance-of v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/c/c;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    .line 1430
    :cond_4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->e(Z)V

    goto :goto_1

    .line 1428
    :cond_5
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    :goto_1
    const/16 v1, 0x8

    if-eqz p1, :cond_8

    .line 1434
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result p1

    if-nez p1, :cond_7

    iget p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    sget v2, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;->c:F

    cmpl-float p1, p1, v2

    if-nez p1, :cond_6

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D()Z

    move-result p1

    if-nez p1, :cond_7

    .line 1435
    :cond_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    .line 1436
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    goto :goto_2

    .line 1438
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    .line 1439
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    goto :goto_2

    .line 1442
    :cond_8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b(I)V

    .line 1443
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(I)V

    :goto_2
    return-void
.end method

.method protected c(I)Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 1

    .line 1183
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->t:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    if-nez v0, :cond_0

    .line 1184
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    .line 1185
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object p1

    .line 1186
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/a;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->t:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    .line 1188
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->t:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    return-object p1
.end method

.method protected c()V
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->i()V

    .line 280
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j()V

    .line 281
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a()V

    .line 282
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->finish()V

    return-void
.end method

.method protected c(Z)V
    .locals 2

    .line 1512
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startVideoPlayFinishPage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTBaseVideoActivity"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1513
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1515
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->finish()V

    goto :goto_0

    .line 1517
    :cond_2
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Z)V

    :goto_0
    return-void
.end method

.method protected d()V
    .locals 3

    .line 291
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;-><init>()V

    .line 292
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a(J)V

    .line 293
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->s()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c(J)V

    .line 294
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->p()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b(J)V

    const/4 v1, 0x3

    .line 295
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e(I)V

    .line 296
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f(I)V

    .line 298
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->D()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    .line 301
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 302
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "play_type"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->j()V

    .line 308
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    const/4 v1, 0x0

    const-string v2, "skip"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 309
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->d(Z)V

    .line 311
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v0

    const-string v1, "onSkippedVideo"

    if-eqz v0, :cond_1

    .line 312
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 314
    :cond_1
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b(Ljava/lang/String;)V

    .line 316
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j()V

    .line 317
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a()V

    const/4 v0, 0x1

    .line 318
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c(Z)V

    return-void
.end method

.method protected d(I)V
    .locals 2

    .line 1525
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 1526
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    .line 1527
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x78

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    .line 1528
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1529
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1530
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "tt_video_loading_progress_bar"

    invoke-static {p0, v1}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1531
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1532
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e()Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1534
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method protected e()V
    .locals 12

    .line 390
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    const-string v0, "tt_full_screen_interaction"

    .line 391
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/r;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->setTheme(I)V

    .line 392
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/app/Activity;)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->setContentView(I)V

    .line 397
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k()V

    .line 399
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h()V

    .line 402
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v5

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IZLjava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 406
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a()V

    .line 407
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aM()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 408
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->b(Z)V

    .line 409
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z()V

    .line 412
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 413
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/a;->a(Lcom/bytedance/sdk/openadsdk/core/b/e;)V

    .line 416
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    iget v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H:I

    iget v8, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I:I

    iget v9, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v10

    iget-object v11, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-virtual/range {v2 .. v11}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/c;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;IIIFZLjava/lang/String;)V

    .line 417
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->B:Z

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->C:Lcom/bytedance/sdk/openadsdk/j/e;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->K:Landroid/webkit/DownloadListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a(ZLcom/bytedance/sdk/openadsdk/j/e;Landroid/webkit/DownloadListener;)V

    .line 420
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/e;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/reward/b/c;)V

    .line 421
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/b/e;)V

    .line 423
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d()V

    .line 430
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->B()D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->v:I

    .line 433
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->b()Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/b/e;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 435
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->J:Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/a$a;)V

    .line 437
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$7;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$7;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->a(Lcom/bytedance/sdk/openadsdk/component/reward/b/a$b;)V

    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 3

    .line 503
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v1, "\u7acb\u5373\u4e0b\u8f7d"

    if-nez v0, :cond_0

    return-object v1

    .line 506
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const-string v1, "\u67e5\u770b\u8be6\u60c5"

    goto :goto_0

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v1

    :cond_2
    :goto_0
    return-object v1
.end method

.method protected g()V
    .locals 2

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "startBindAd"

    .line 602
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a()V

    .line 605
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Z)V

    .line 606
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->h()V

    return-void

    .line 609
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->d(I)V

    .line 611
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q()V

    return-void

    .line 614
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->a()V

    .line 615
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v0, :cond_2

    .line 616
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->a(Landroid/widget/FrameLayout;)V

    .line 618
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->E()V

    return-void
.end method

.method h()V
    .locals 7

    .line 671
    new-instance v6, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$8;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    const/4 v5, 0x7

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    const/4 v5, 0x5

    :goto_0
    move-object v0, v6

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$8;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    .line 686
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/core/b/e;->a(Landroid/view/View;)V

    .line 687
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 688
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 689
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    const-string v2, "rit_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->D:Lcom/bytedance/sdk/openadsdk/core/b/e;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/b/e;->a(Ljava/util/Map;)V

    .line 693
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$9;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$9;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->E:Landroid/view/View$OnClickListener;

    return-void
.end method

.method protected i()Lorg/json/JSONObject;
    .locals 5

    const/4 v0, 0x0

    .line 800
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->m()J

    move-result-wide v1

    .line 801
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->n()I

    move-result v3

    .line 802
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v0, "duration"

    .line 803
    invoke-virtual {v4, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 804
    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v4

    :catchall_0
    move-object v0, v4

    :catchall_1
    return-object v0
.end method

.method protected j()V
    .locals 5

    .line 835
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 837
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 838
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Ljava/util/Map;)V

    .line 840
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x0

    :cond_1
    const-string v4, "click_close"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method protected k()V
    .locals 9

    .line 859
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 860
    invoke-virtual {p0, v2}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 862
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->setRequestedOrientation(I)V

    .line 864
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I()F

    move-result v0

    .line 865
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H()F

    move-result v3

    .line 870
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    if-ne v4, v1, :cond_1

    .line 871
    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 872
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1

    .line 874
    :cond_1
    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 875
    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 879
    :goto_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/q;->j(Landroid/content/Context;)F

    move-result v5

    invoke-static {v3, v5}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v3

    .line 880
    iget v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    if-eq v5, v1, :cond_2

    .line 881
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_3

    int-to-float v3, v3

    sub-float/2addr v0, v3

    goto :goto_2

    .line 885
    :cond_2
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_3

    int-to-float v3, v3

    sub-float/2addr v4, v3

    .line 890
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v3

    if-eqz v3, :cond_4

    float-to-int v1, v4

    .line 891
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H:I

    float-to-int v0, v0

    .line 892
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I:I

    return-void

    .line 897
    :cond_4
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x42c80000    # 100.0f

    const/16 v7, 0x14

    const/4 v8, 0x0

    if-eq v3, v1, :cond_5

    .line 898
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    cmpl-float v3, v1, v8

    if-eqz v3, :cond_6

    cmpl-float v3, v1, v6

    if-eqz v3, :cond_6

    int-to-float v2, v7

    sub-float v3, v4, v2

    sub-float/2addr v3, v2

    div-float/2addr v3, v1

    sub-float v1, v0, v3

    div-float/2addr v1, v5

    .line 901
    invoke-static {v1, v8}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v2, v1

    move v1, v2

    move v3, v1

    const/16 v2, 0x14

    goto :goto_3

    .line 905
    :cond_5
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    cmpl-float v3, v1, v8

    if-eqz v3, :cond_6

    cmpl-float v3, v1, v6

    if-eqz v3, :cond_6

    int-to-float v2, v7

    sub-float v3, v0, v2

    sub-float/2addr v3, v2

    mul-float v3, v3, v1

    sub-float v1, v4, v3

    div-float/2addr v1, v5

    .line 908
    invoke-static {v1, v8}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v2, v1

    move v7, v2

    const/16 v1, 0x14

    const/16 v3, 0x14

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    :goto_3
    int-to-float v2, v2

    sub-float v5, v4, v2

    int-to-float v6, v7

    sub-float/2addr v5, v6

    float-to-int v5, v5

    .line 913
    iput v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H:I

    int-to-float v1, v1

    sub-float v5, v0, v1

    int-to-float v3, v3

    sub-float/2addr v5, v3

    float-to-int v5, v5

    .line 914
    iput v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I:I

    .line 915
    invoke-static {p0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v1

    .line 916
    invoke-static {p0, v3}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v3

    .line 917
    invoke-static {p0, v2}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v2

    .line 918
    invoke-static {p0, v6}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v5

    .line 919
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2, v1, v5, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 920
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initScreenOrientationAndSize , orientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "; aspectRatio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->F:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "; width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "; height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTBaseVideoActivity"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public l()V
    .locals 0

    .line 1118
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n()V

    return-void
.end method

.method public m()V
    .locals 2

    .line 1123
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x2bc

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 1124
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    return-void
.end method

.method protected n()V
    .locals 0

    return-void
.end method

.method public o()V
    .locals 0

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .line 1040
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 1042
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/app/Activity;)V

    .line 1044
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$11;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$11;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 1033
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->p()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 230
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 231
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/content/Intent;)V

    .line 232
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 234
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->requestWindowFeature(I)Z

    .line 235
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 236
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 237
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :goto_0
    if-eqz p1, :cond_0

    const-string v0, "video_current"

    .line 240
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    .line 241
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p1, v0, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->a(J)V

    .line 243
    :cond_0
    iput-object p0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    .line 244
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Landroid/os/Bundle;)Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    .line 247
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->G()V

    .line 248
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->e()V

    .line 249
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->C()V

    .line 250
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->g()V

    return-void
.end method

.method protected onDestroy()V
    .locals 10

    .line 986
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "onDestroy"

    .line 987
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    if-eqz v1, :cond_0

    .line 989
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->f()V

    .line 991
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    if-eqz v1, :cond_1

    .line 992
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->resetDislikeStatus()V

    .line 994
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 995
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->c(Z)V

    .line 996
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->f()V

    .line 997
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->z:Lcom/bytedance/sdk/openadsdk/component/reward/c/a;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/c/a;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_2

    .line 999
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->x()V

    .line 1001
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->e()V

    .line 1003
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    :try_start_0
    const-string v3, "recycleRes"

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const-string v9, ""

    move-object v2, p0

    .line 1005
    invoke-direct/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    const-string v2, "remove from ITTAppDownloadListener throw Exception : "

    .line 1007
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1010
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->e()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    .line 958
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "onPause"

    .line 959
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 961
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->g()V

    .line 963
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->e()V

    .line 964
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->d()V

    .line 965
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->c()V

    .line 966
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 967
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 931
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "onResume"

    .line 932
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->k:Lcom/bytedance/sdk/openadsdk/component/reward/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aM()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/c;->a(Z)V

    .line 936
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->b()V

    .line 938
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->b(ZLcom/bytedance/sdk/openadsdk/core/video/b/b;)V

    .line 939
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->m:Lcom/bytedance/sdk/openadsdk/component/reward/b/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/a;->d()V

    .line 941
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 942
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    const-string v1, "return_foreground"

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Ljava/lang/String;)V

    .line 943
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x:Lcom/bytedance/sdk/openadsdk/core/widget/d;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/d;->isShowing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 945
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->k()V

    .line 949
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->c()V

    .line 951
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    if-eqz v0, :cond_5

    .line 952
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->g()V

    :cond_5
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    .line 1016
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    :try_start_0
    const-string v0, "material_meta"

    .line 1019
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "multi_process_meta_md5"

    .line 1020
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "video_cache_url"

    .line 1021
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "video_current"

    .line 1022
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->n:Lcom/bytedance/sdk/openadsdk/component/reward/b/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/d;->o()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "is_mute"

    .line 1023
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "rit_scene"

    .line 1024
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "has_show_skip_btn"

    .line 1025
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1028
    :catchall_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 925
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 926
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->l()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    .line 973
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "onStop"

    .line 974
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->k()V

    .line 976
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->l:Lcom/bytedance/sdk/openadsdk/component/reward/b/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/e;->d()V

    .line 977
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->j()V

    .line 979
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    .line 980
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    const-string v1, "go_background"

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 0

    return-void
.end method

.method protected q()V
    .locals 11

    const-string v0, "TTBaseVideoActivity"

    const-string v1, "initExpressView"

    .line 1232
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [F

    .line 1234
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->H:I

    int-to-float v2, v2

    const/4 v3, 0x0

    aput v2, v1, v3

    .line 1235
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->I:I

    int-to-float v2, v2

    const/4 v4, 0x1

    aput v2, v1, v4

    .line 1236
    aget v2, v1, v3

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v2, v2, v5

    if-ltz v2, :cond_0

    aget v2, v1, v4

    cmpg-float v2, v2, v5

    if-gez v2, :cond_1

    :cond_0
    const-string v1, "get root view size error, so run backup"

    .line 1237
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->J()[F

    move-result-object v1

    .line 1240
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    .line 1241
    new-instance v2, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    .line 1242
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v0

    aget v2, v1, v3

    aget v1, v1, v4

    .line 1243
    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v0

    .line 1244
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    .line 1245
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$2;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;)V

    .line 1324
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$3;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    .line 1373
    new-instance v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$4;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-static {v8}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v9

    move-object v4, v0

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$4;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 1380
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "rit_scene"

    if-nez v1, :cond_2

    .line 1381
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1382
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Ljava/util/Map;)V

    .line 1386
    :cond_2
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$5;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b:Landroid/content/Context;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    invoke-static {v9}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v10

    move-object v5, v1

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$5;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    .line 1393
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1394
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1395
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->f:Ljava/lang/String;

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1396
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Ljava/util/Map;)V

    .line 1398
    :cond_3
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->j:Lcom/bytedance/sdk/openadsdk/component/reward/view/a;

    invoke-virtual {v2, v0, v1, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;Lcom/bytedance/sdk/openadsdk/component/reward/view/a;)V

    .line 1399
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    .line 1400
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1401
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->i:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1402
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->h()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1403
    invoke-virtual {p0, v3}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->b(Z)V

    .line 1405
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->j()V

    return-void
.end method

.method protected r()V
    .locals 4

    .line 1471
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x190

    .line 1472
    iput v1, v0, Landroid/os/Message;->what:I

    .line 1474
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1475
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p()V

    .line 1477
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method protected s()V
    .locals 2

    .line 1484
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->w:Lcom/bytedance/sdk/component/utils/u;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeMessages(I)V

    return-void
.end method

.method protected t()Z
    .locals 2

    .line 1495
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->j(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method protected u()Z
    .locals 1

    .line 1541
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    if-eqz v0, :cond_0

    .line 1542
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->isShow()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected v()V
    .locals 4

    .line 1551
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1554
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    if-nez v0, :cond_1

    .line 1555
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aG()Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->a:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    .line 1556
    new-instance v1, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity$6;-><init>(Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->setDislikeInteractionCallback(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    .line 1580
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->K()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->setDislikeSource(Ljava/lang/String;)V

    .line 1581
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->h:Lcom/bytedance/sdk/openadsdk/TTAdDislike;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdDislike;->showDislikeDialog()V

    return-void
.end method

.method protected w()Z
    .locals 5

    .line 1625
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1629
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->o:Lcom/bytedance/sdk/openadsdk/component/reward/b/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/b/b;->o()Z

    move-result v0

    .line 1631
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1632
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->p:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;->getJsObject()Lcom/bytedance/sdk/openadsdk/core/w;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1633
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTBaseVideoActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 1637
    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "state"

    .line 1638
    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "playableStateChange"

    .line 1639
    invoke-virtual {v2, v1, v3}, Lcom/bytedance/sdk/openadsdk/core/w;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1641
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    nop

    :cond_2
    :goto_0
    return v0
.end method

.method protected abstract x()Z
.end method

.method protected abstract y()Z
.end method

.method protected abstract z()V
.end method
