.class public Lcom/bytedance/sdk/openadsdk/m/d;
.super Ljava/lang/Object;
.source "PlayableJsBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/m/d$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/m/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/m/d$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/hardware/SensorEventListener;

.field private e:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/m/f;)V
    .locals 1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    .line 29
    new-instance v0, Lcom/bytedance/sdk/openadsdk/m/d$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/m/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->d:Landroid/hardware/SensorEventListener;

    .line 57
    new-instance v0, Lcom/bytedance/sdk/openadsdk/m/d$11;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/m/d$11;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->e:Landroid/hardware/SensorEventListener;

    .line 87
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/m/f;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->a:Landroid/content/Context;

    .line 88
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->b:Ljava/lang/ref/WeakReference;

    .line 89
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/m/d;->c()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/m/d;)Lcom/bytedance/sdk/openadsdk/m/f;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/m/d;->d()Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/m/d;)Lcom/bytedance/sdk/openadsdk/m/a;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/m/d;->e()Lcom/bytedance/sdk/openadsdk/m/a;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/m/d;)Landroid/content/Context;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->a:Landroid/content/Context;

    return-object p0
.end method

.method private c()V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$12;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$12;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "adInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$13;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$13;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "appInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$14;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$14;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "playableSDKInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$15;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$15;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "subscribe_app_ad"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$16;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$16;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "download_app_ad"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$17;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$17;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "isViewable"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$18;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$18;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "getVolume"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "getScreenSize"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$3;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "start_accelerometer_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$4;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$4;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "close_accelerometer_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$5;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$5;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "start_gyro_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$6;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "close_gyro_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$7;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$7;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "device_shake"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$8;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$8;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "playable_style"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$9;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$9;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "sendReward"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/m/d$10;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/m/d$10;-><init>(Lcom/bytedance/sdk/openadsdk/m/d;)V

    const-string v2, "webview_time_track"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/m/d;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->d:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method private d()Lcom/bytedance/sdk/openadsdk/m/f;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 346
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/m/f;

    return-object v0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/m/d;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->e:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method private e()Lcom/bytedance/sdk/openadsdk/m/a;
    .locals 1

    .line 350
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/m/d;->d()Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 354
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/f;->l()Lcom/bytedance/sdk/openadsdk/m/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2

    const/4 v0, 0x0

    .line 359
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/d;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/m/d$a;

    if-nez p1, :cond_0

    return-object v0

    .line 363
    :cond_0
    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/m/d$a;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    const-string p2, "PlayableJsBridge"

    const-string v1, "invoke error"

    .line 365
    invoke-static {p2, v1, p1}, Lcom/bytedance/sdk/openadsdk/m/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/d;->d:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/h;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    .line 372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/d;->e:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/h;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    return-void
.end method
