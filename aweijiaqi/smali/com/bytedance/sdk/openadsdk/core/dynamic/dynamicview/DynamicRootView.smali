.class public Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;
.super Landroid/widget/FrameLayout;
.source "DynamicRootView.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/theme/a;


# instance fields
.field protected a:Lcom/bytedance/sdk/openadsdk/core/e/p;

.field private b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

.field private c:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

.field private d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

.field private e:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

.field private f:Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;)V
    .locals 1

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/core/e/p;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    const/4 v0, 0x2

    .line 26
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/p;->a(I)V

    .line 27
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->e:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

    .line 28
    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;->a(Landroid/view/View;)V

    .line 29
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->f:Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;

    .line 30
    invoke-virtual {p2, p0}, Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;->a(Lcom/bytedance/sdk/openadsdk/theme/a;)V

    return-void
.end method

.method private c()Z
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->d:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 47
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/p;->a(Z)V

    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->c:F

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/p;->a(D)V

    .line 49
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->d:F

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/p;->b(D)V

    .line 50
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(Lcom/bytedance/sdk/openadsdk/core/e/p;)V

    return-void
.end method

.method public a(DDDDF)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/p;->c(D)V

    .line 55
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/e/p;->d(D)V

    .line 56
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p5, p6}, Lcom/bytedance/sdk/openadsdk/core/e/p;->e(D)V

    .line 57
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p7, p8}, Lcom/bytedance/sdk/openadsdk/core/e/p;->f(D)V

    .line 58
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p9}, Lcom/bytedance/sdk/openadsdk/core/e/p;->a(F)V

    .line 59
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p9}, Lcom/bytedance/sdk/openadsdk/core/e/p;->b(F)V

    .line 60
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p9}, Lcom/bytedance/sdk/openadsdk/core/e/p;->c(F)V

    .line 61
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-virtual {p1, p9}, Lcom/bytedance/sdk/openadsdk/core/e/p;->d(F)V

    return-void
.end method

.method public a_(I)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;->a(I)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/p;->a(Z)V

    .line 66
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->a:Lcom/bytedance/sdk/openadsdk/core/e/p;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(Lcom/bytedance/sdk/openadsdk/core/e/p;)V

    return-void
.end method

.method public getDynamicClickListener()Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->e:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

    return-object v0
.end method

.method public getExpressVideoListener()Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->c:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    return-object v0
.end method

.method public getRenderListener()Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    return-object v0
.end method

.method public setDislikeView(Landroid/view/View;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->e:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;->b(Landroid/view/View;)V

    return-void
.end method

.method public setDynamicBaseWidget(Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicBaseWidget;

    return-void
.end method

.method public setExpressVideoListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->c:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    return-void
.end method

.method public setRenderListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;)V
    .locals 1

    .line 34
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    .line 35
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/dynamicview/DynamicRootView;->e:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/a;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;)V

    return-void
.end method
