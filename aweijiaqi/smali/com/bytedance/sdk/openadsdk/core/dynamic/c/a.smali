.class public Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;
.super Ljava/lang/Object;
.source "TemplateToModelParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a$a;
    }
.end annotation


# static fields
.field private static e:Ljava/lang/String;

.field private static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

.field private c:Ljava/lang/String;

.field private d:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 33
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "dislike"

    const-string v2, "close"

    const-string v3, "close-fill"

    filled-new-array {v1, v2, v3}, [Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 33
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->f:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->a:Landroid/content/Context;

    .line 38
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->a:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 39
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x11

    if-lt p1, v0, :cond_0

    .line 40
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a$a;-><init>(Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a$1;)V

    const-string v1, "JS_DYNAMIC_LAYOUT_OBJ"

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    const-string v0, "about:blank"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->loadUrl(Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;)Landroid/content/Context;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a()Ljava/util/Set;
    .locals 1

    .line 25
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->f:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->c(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    .line 46
    sput-object p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e:Ljava/lang/String;

    return-void
.end method

.method private b()V
    .locals 2

    .line 50
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->d()Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/i;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:var res = getLayoutInfo("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ");window."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "JS_DYNAMIC_LAYOUT_OBJ"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".calculateResult(JSON.stringify(res));"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/i;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .line 195
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;-><init>()V

    .line 197
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 198
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 202
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;

    if-eqz p1, :cond_0

    .line 203
    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;)V

    .line 205
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e()V

    return-void
.end method

.method private static d()Ljava/lang/String;
    .locals 4

    .line 78
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 81
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    .line 82
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "var global = Function(\'return this\')();global.jsCoreGlobal = {width:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ",height:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;F)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ",os:\'Android\'};global.systemFontSizeRatioNative = "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide v0, 0x3ff3333333333333L    # 1.2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(function () {var JS_TTDYNAMIC_URL = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\';var xhrObj = new XMLHttpRequest();xhrObj.open(\'GET\', JS_TTDYNAMIC_URL, false);xhrObj.send(\'\');var se = document.createElement(\'script\');se.type = \'text/javascript\';se.text = xhrObj.responseText;document.getElementsByTagName(\'head\')[0].appendChild(se);})();"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "javascript:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/aa;->a(Landroid/content/Context;Landroid/webkit/WebView;)V

    .line 210
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/aa;->a(Landroid/webkit/WebView;)V

    const/4 v0, 0x0

    .line 211
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 60
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->c:Ljava/lang/String;

    .line 61
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->d:Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 62
    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/dynamic/b/f;)V

    .line 63
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->e()V

    .line 66
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/c/a;->c()V

    return-void
.end method
