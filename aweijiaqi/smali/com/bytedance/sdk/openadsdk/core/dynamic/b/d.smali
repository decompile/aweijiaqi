.class public Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;
.super Ljava/lang/Object;
.source "DynamicLayoutBrickValue.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;
    .locals 5

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 265
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;-><init>()V

    const-string v1, "adType"

    const-string v2, "embeded"

    .line 266
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a(Ljava/lang/String;)V

    const-string v1, "clickArea"

    const-string v2, "creative"

    .line 267
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->l(Ljava/lang/String;)V

    const-string v1, "clickTigger"

    const-string v2, "click"

    .line 268
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->m(Ljava/lang/String;)V

    const-string v1, "fontFamily"

    const-string v2, "PingFangSC"

    .line 269
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->b(Ljava/lang/String;)V

    const-string v1, "textAlign"

    const-string v2, "left"

    .line 270
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->c(Ljava/lang/String;)V

    const-string v1, "color"

    const-string v2, "#999999"

    .line 271
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->d(Ljava/lang/String;)V

    const-string v1, "bgColor"

    const-string v2, "transparent"

    .line 272
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->e(Ljava/lang/String;)V

    const-string v1, "borderColor"

    const-string v2, "#000000"

    .line 273
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->f(Ljava/lang/String;)V

    const-string v1, "borderStyle"

    const-string v2, "solid"

    .line 274
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->g(Ljava/lang/String;)V

    const-string v1, "heightMode"

    const-string v2, "auto"

    .line 275
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->h(Ljava/lang/String;)V

    const-string v1, "widthMode"

    const-string v2, "fixed"

    .line 276
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->i(Ljava/lang/String;)V

    const-string v1, ""

    const-string v2, "interactText"

    .line 277
    invoke-virtual {p0, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->j(Ljava/lang/String;)V

    const-string v2, "interactType"

    .line 278
    invoke-virtual {p0, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->k(Ljava/lang/String;)V

    const-string v1, "justifyHorizontal"

    const-string v2, "space-around"

    .line 279
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->n(Ljava/lang/String;)V

    const-string v1, "justifyVertical"

    const-string v2, "flex-start"

    .line 280
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->o(Ljava/lang/String;)V

    const-string v1, "timingStart"

    .line 281
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a(I)V

    const-string v1, "timingEnd"

    .line 282
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->b(I)V

    const-wide/16 v1, 0x0

    const-string v3, "width"

    .line 283
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->d(F)V

    const-string v3, "height"

    .line 284
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->c(F)V

    const-string v3, "borderRadius"

    .line 285
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a(F)V

    const-string v3, "borderSize"

    .line 286
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->b(F)V

    const-string v3, "interactValidate"

    .line 287
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->e(F)V

    const-string v3, "fontSize"

    .line 288
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->j(F)V

    const-string v3, "paddingBottom"

    .line 289
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->f(F)V

    const-string v3, "paddingLeft"

    .line 290
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->g(F)V

    const-string v3, "paddingRight"

    .line 291
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->h(F)V

    const-string v3, "paddingTop"

    .line 292
    invoke-virtual {p0, v3, v1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->i(F)V

    const/4 v1, 0x0

    const-string v2, "lineFeed"

    .line 293
    invoke-virtual {p0, v2, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a(Z)V

    return-object v0
.end method


# virtual methods
.method public a()F
    .locals 1

    .line 38
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a:F

    return v0
.end method

.method public a(F)V
    .locals 0

    .line 42
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->a:F

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 122
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->k:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->m:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 258
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->B:Z

    return-void
.end method

.method public b()F
    .locals 1

    .line 46
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->b:F

    return v0
.end method

.method public b(F)V
    .locals 0

    .line 50
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->b:F

    return-void
.end method

.method public b(I)V
    .locals 0

    .line 130
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->l:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->n:Ljava/lang/String;

    return-void
.end method

.method public c()F
    .locals 1

    .line 78
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->f:F

    return v0
.end method

.method public c(F)V
    .locals 0

    .line 58
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->c:F

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->o:Ljava/lang/String;

    return-void
.end method

.method public d()F
    .locals 1

    .line 86
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->g:F

    return v0
.end method

.method public d(F)V
    .locals 0

    .line 66
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->d:F

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->p:Ljava/lang/String;

    return-void
.end method

.method public e()F
    .locals 1

    .line 94
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->h:F

    return v0
.end method

.method public e(F)V
    .locals 0

    .line 74
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->e:F

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->q:Ljava/lang/String;

    return-void
.end method

.method public f()F
    .locals 1

    .line 102
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->i:F

    return v0
.end method

.method public f(F)V
    .locals 0

    .line 82
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->f:F

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->r:Ljava/lang/String;

    return-void
.end method

.method public g()F
    .locals 1

    .line 110
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->j:F

    return v0
.end method

.method public g(F)V
    .locals 0

    .line 90
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->g:F

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->s:Ljava/lang/String;

    return-void
.end method

.method public h()I
    .locals 1

    .line 118
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->k:I

    return v0
.end method

.method public h(F)V
    .locals 0

    .line 98
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->h:F

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->t:Ljava/lang/String;

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->o:Ljava/lang/String;

    return-object v0
.end method

.method public i(F)V
    .locals 0

    .line 106
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->i:F

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->u:Ljava/lang/String;

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->p:Ljava/lang/String;

    return-object v0
.end method

.method public j(F)V
    .locals 0

    .line 114
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->j:F

    return-void
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->v:Ljava/lang/String;

    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->q:Ljava/lang/String;

    return-object v0
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->w:Ljava/lang/String;

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->r:Ljava/lang/String;

    return-object v0
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->x:Ljava/lang/String;

    return-void
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->x:Ljava/lang/String;

    return-object v0
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->y:Ljava/lang/String;

    return-void
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->y:Ljava/lang/String;

    return-object v0
.end method

.method public n(Ljava/lang/String;)V
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->z:Ljava/lang/String;

    return-void
.end method

.method public o(Ljava/lang/String;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->A:Ljava/lang/String;

    return-void
.end method

.method public o()Z
    .locals 1

    .line 254
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/dynamic/b/d;->B:Z

    return v0
.end method
