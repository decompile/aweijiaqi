.class public Lcom/bytedance/sdk/openadsdk/core/i;
.super Ljava/lang/Object;
.source "H5AdInteractionManager.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private c:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private d:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

.field private e:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

.field private f:Landroid/view/View;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "rewarded_video"

    .line 28
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 36
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->a:Landroid/content/Context;

    .line 37
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->f:Landroid/view/View;

    .line 39
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 40
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/r/o;->c(Ljava/lang/String;)I

    move-result p3

    .line 41
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    goto :goto_0

    .line 43
    :cond_0
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    .line 46
    :goto_0
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result p3

    const/4 p4, 0x4

    if-ne p3, p4, :cond_1

    .line 47
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->a:Landroid/content/Context;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    invoke-static {p3, p4, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object p3

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->c:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    .line 50
    :cond_1
    new-instance p3, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p3, p1, p2, p4, v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->d:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    .line 51
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->f:Landroid/view/View;

    invoke-virtual {p3, p4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Landroid/view/View;)V

    .line 52
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->d:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->c:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {p3, p4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 54
    new-instance p3, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/i;->g:Ljava/lang/String;

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p3, p1, p2, p4, v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/i;->e:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    .line 55
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->f:Landroid/view/View;

    invoke-virtual {p3, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Landroid/view/View;)V

    .line 56
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->e:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->c:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    return-void
.end method


# virtual methods
.method public a(ILcom/bytedance/sdk/openadsdk/core/e/k;)V
    .locals 7

    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 70
    :cond_0
    iget v3, p2, Lcom/bytedance/sdk/openadsdk/core/e/k;->a:I

    .line 71
    iget v4, p2, Lcom/bytedance/sdk/openadsdk/core/e/k;->b:I

    .line 72
    iget v5, p2, Lcom/bytedance/sdk/openadsdk/core/e/k;->c:I

    .line 73
    iget v6, p2, Lcom/bytedance/sdk/openadsdk/core/e/k;->d:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 82
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->e:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    if-eqz p1, :cond_3

    .line 83
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/k;)V

    .line 84
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->e:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->f:Landroid/view/View;

    invoke-virtual/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;->a(Landroid/view/View;IIII)V

    goto :goto_0

    .line 76
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->d:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    if-eqz p1, :cond_3

    .line 77
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Lcom/bytedance/sdk/openadsdk/core/e/k;)V

    .line 78
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->d:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->f:Landroid/view/View;

    invoke-virtual/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/e;->a(Landroid/view/View;IIII)V

    :cond_3
    :goto_0
    return-void
.end method
