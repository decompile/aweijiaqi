.class public interface abstract Lcom/bytedance/sdk/openadsdk/core/p;
.super Ljava/lang/Object;
.source "NetApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/p$a;,
        Lcom/bytedance/sdk/openadsdk/core/p$b;,
        Lcom/bytedance/sdk/openadsdk/core/p$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()Lcom/bytedance/sdk/openadsdk/core/e/u;
.end method

.method public abstract a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/e/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)",
            "Lcom/bytedance/sdk/openadsdk/e/h;"
        }
    .end annotation
.end method

.method public abstract a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/e/h;
.end method

.method public abstract a(Lcom/bytedance/sdk/openadsdk/AdSlot;)Ljava/lang/String;
.end method

.method public abstract a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZI)Ljava/lang/String;
.end method

.method public abstract a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;ILcom/bytedance/sdk/openadsdk/core/p$b;)V
.end method

.method public abstract a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/dislike/c/b;",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/FilterWord;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/p$a;)V
.end method

.method public abstract a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/p$c;)V
.end method

.method public abstract a(Lorg/json/JSONObject;I)Z
.end method
