.class Lcom/bytedance/sdk/openadsdk/core/j/f$1;
.super Lcom/bytedance/sdk/component/net/callback/NetCallback;
.source "SdkSettingsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/j/f;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/j/f;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/j/f;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/f$1;->a:Lcom/bytedance/sdk/openadsdk/core/j/f;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V
    .locals 0

    .line 312
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/f$1;->a:Lcom/bytedance/sdk/openadsdk/core/j/f;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/j/f;->a(Lcom/bytedance/sdk/openadsdk/core/j/f;)Lcom/bytedance/sdk/openadsdk/core/j/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/j/c;->a()V

    .line 314
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V
    .locals 4

    if-eqz p2, :cond_3

    .line 252
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 256
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 258
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, p1

    :goto_0
    if-eqz v0, :cond_3

    const/4 v1, -0x1

    const-string v2, "cypher"

    .line 262
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const-string p1, "message"

    .line 267
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 268
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 269
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v1

    const-string v2, "SdkSettingsHelper"

    const-string v3, "setting data error2: "

    .line 273
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 279
    :cond_0
    :goto_1
    :try_start_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getHeaders()Ljava/util/Map;

    move-result-object p2

    .line 280
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/f$1;->a:Lcom/bytedance/sdk/openadsdk/core/j/f;

    invoke-static {v1, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/j/f;->a(Lcom/bytedance/sdk/openadsdk/core/j/f;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    :catchall_1
    :try_start_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/f$1;->a:Lcom/bytedance/sdk/openadsdk/core/j/f;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/j/f;->a(Lcom/bytedance/sdk/openadsdk/core/j/f;)Lcom/bytedance/sdk/openadsdk/core/j/c;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/c;->a(Lorg/json/JSONObject;)V

    .line 287
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/f;->h()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    .line 288
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/j/f;->a(Z)Z

    .line 289
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/n/a;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catchall_2
    nop

    .line 293
    :cond_1
    :goto_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/a;->b()V

    .line 294
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 295
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/f;->d()V

    :cond_2
    return-void

    .line 302
    :cond_3
    :try_start_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/f$1;->a:Lcom/bytedance/sdk/openadsdk/core/j/f;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/j/f;->a(Lcom/bytedance/sdk/openadsdk/core/j/f;)Lcom/bytedance/sdk/openadsdk/core/j/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/j/c;->a()V

    .line 304
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :catchall_3
    return-void
.end method
