.class public Lcom/bytedance/sdk/openadsdk/core/j/h;
.super Ljava/lang/Object;
.source "TTSdkSettings.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/j/c;


# instance fields
.field private A:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private B:I

.field private C:I

.field private D:J

.field private E:I

.field private F:I

.field private G:I

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:I

.field private K:I

.field private L:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/lang/String;

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private volatile R:Z

.field private S:F

.field private final T:Lcom/bytedance/sdk/component/utils/s;

.field private U:F

.field private V:I

.field private W:I

.field private X:I

.field private a:I

.field private b:I

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/core/j/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/core/j/i;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/j/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Lorg/json/JSONObject;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:I

.field private x:I

.field private y:J

.field private z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    .line 208
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    .line 212
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    .line 214
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    .line 215
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->d:Ljava/util/Map;

    .line 217
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->f:Ljava/util/Set;

    .line 219
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->h:Ljava/util/Set;

    .line 220
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->i:Ljava/util/List;

    .line 247
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    .line 248
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    .line 249
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    .line 250
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    const/4 v1, 0x0

    .line 251
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;

    const-string v2, ""

    .line 252
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    .line 256
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    .line 257
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    .line 260
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    .line 261
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    const-wide/16 v2, 0x0

    .line 262
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    .line 263
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    .line 264
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    .line 265
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    .line 266
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    const-wide/32 v2, 0x7fffffff

    .line 268
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    .line 269
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    .line 271
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    .line 272
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    .line 273
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    .line 274
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    .line 275
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    .line 276
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    .line 277
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    .line 282
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    .line 287
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    .line 289
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    .line 292
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    const/4 v1, 0x3

    .line 295
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    const/4 v1, 0x0

    .line 297
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->R:Z

    const/high16 v1, -0x40800000    # -1.0f

    .line 306
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    const/high16 v1, 0x4f000000

    .line 309
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    .line 313
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    .line 316
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    const/4 v0, 0x2

    .line 321
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    .line 324
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_sdk_settings"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    return-void
.end method

.method private O()V
    .locals 3

    const/4 v0, 0x0

    .line 829
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    .line 830
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "ab_test_param"

    const-string v2, "ab_test_version"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 831
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 834
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;)V

    .line 835
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Z)I
    .locals 0

    if-eqz p1, :cond_0

    const/16 p1, 0x14

    return p1

    :cond_0
    const/4 p1, 0x5

    return p1
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 1858
    :try_start_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0

    .line 1860
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1861
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1862
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1863
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_2
    return-object v0

    .line 1868
    :catchall_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 851
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v3

    const-string v4, "webview_render_concurrent_count"

    const-string v5, "webview_cache_count"

    const-string v6, "fetch_tpl_timeout_ctrl"

    const-string v7, "max_tpl_cnts"

    const-string v8, "app_list_control"

    const-string v9, "support_tnc"

    const-string v10, "if_both_open"

    const-string v11, "splash_check_type"

    const-string v12, "playableLoadH5Url"

    const-string v13, "pyload_h5"

    const-string v14, "web_info_page_count"

    const-string v15, "web_info_wifi_enable"

    const-string v1, "fetch_template"

    const-string v2, "download_config_storage_internal"

    move-object/from16 v16, v4

    const-string v4, "download_config_dl_size"

    move-object/from16 v17, v5

    const-string v5, "download_config_dl_network"

    move-object/from16 v18, v6

    const-string v6, "max"

    move-object/from16 v19, v7

    const-string v7, "duration"

    move-object/from16 v20, v8

    const-string v8, "xpath"

    move-object/from16 v21, v12

    const-string v12, "url_alog"

    move-object/from16 v22, v13

    const-string v13, "url_ads"

    if-eqz v3, :cond_6

    .line 853
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    move-object/from16 v23, v9

    const-string v9, "tt_sdk_settings"

    invoke-static {v9, v13, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    invoke-static {v9, v12, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    invoke-static {v9, v8, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    iget-wide v12, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v9, v7, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 857
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v9, v6, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 858
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v9, v5, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 859
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v9, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 860
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 862
    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "vbtt"

    invoke-static {v9, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 863
    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 864
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9, v15, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 865
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9, v14, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 866
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9, v11, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 867
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9, v10, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 868
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v23

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 869
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    move-object/from16 v3, v22

    invoke-static {v9, v3, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    move-object/from16 v2, v21

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v20

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 872
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v19

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 873
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v18

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 874
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v17

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 875
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, v16

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 876
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    const-string v2, "gecko_hosts"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 877
    iget-wide v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "hit_app_list_time"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 878
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    const-string v2, "hit_app_list_data"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 879
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    const-string v2, "scheme_list_data"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 880
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "circle_splash_switch"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 881
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "circle_load_splash_time"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 883
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "sp_key_if_sp_cache"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 884
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    const-string v2, "dyn_draw_engine_url"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    const-string v2, "download_sdk_config"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "enable_download_opt"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 889
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v2, "ab_test_version"

    .line 890
    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    :cond_0
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v2, "ab_test_param"

    .line 893
    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    :cond_1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "push_config"

    move-object/from16 v2, p2

    .line 896
    invoke-static {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    :cond_2
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "ad_slot_conf"

    move-object/from16 v2, p1

    .line 899
    invoke-static {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    :cond_3
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 902
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    const-string v2, "template_ids"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    :cond_4
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 905
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    const-string v2, "tpl_infos"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    :cond_5
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string v2, "call_stack_rate"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;)V

    .line 908
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "if_pre_connect"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 909
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string v2, "global_rate"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;)V

    .line 910
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "read_video_from_cache"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 911
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "icon_show_time"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 912
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "brand_video_cache_count"

    invoke-static {v9, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-void

    :cond_6
    move-object/from16 v23, v9

    move-object/from16 v9, v21

    move-object/from16 v3, v22

    move-object/from16 v21, v10

    .line 916
    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    move-object/from16 v22, v11

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    invoke-virtual {v10, v13, v11}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    invoke-virtual {v10, v12, v11}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    invoke-virtual {v10, v8, v11}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    iget-object v8, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-wide v10, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    invoke-virtual {v8, v7, v10, v11}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;J)V

    .line 920
    iget-object v7, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v8, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    invoke-virtual {v7, v6, v8}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 921
    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v7, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    invoke-virtual {v6, v5, v7}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 922
    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v6, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    invoke-virtual {v5, v4, v6}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 923
    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v5, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    invoke-virtual {v4, v2, v5}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 925
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v4, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    invoke-virtual {v2, v1, v4}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 926
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    invoke-virtual {v1, v15, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 927
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    invoke-virtual {v1, v14, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 928
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    invoke-virtual {v1, v9, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    move-object/from16 v3, v22

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 931
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    move-object/from16 v3, v21

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 932
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    move-object/from16 v3, v23

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 933
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    move-object/from16 v3, v20

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 934
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    move-object/from16 v3, v19

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 935
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    move-object/from16 v3, v18

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 936
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    move-object/from16 v3, v17

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 937
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    move-object/from16 v3, v16

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 938
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    const-string v3, "gecko_hosts"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 939
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-wide v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    const-string v4, "hit_app_list_time"

    invoke-virtual {v1, v4, v2, v3}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;J)V

    .line 940
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    const-string v3, "hit_app_list_data"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 941
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    const-string v3, "scheme_list_data"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 943
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    const-string v3, "circle_splash_switch"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 944
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    const-string v3, "circle_load_splash_time"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 945
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    const-string v3, "dyn_draw_engine_url"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    const-string v3, "sp_key_if_sp_cache"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 948
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    const-string v3, "download_sdk_config"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    const-string v3, "enable_download_opt"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 950
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 951
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "ab_test_version"

    invoke-virtual {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    :cond_7
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 954
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "ab_test_param"

    invoke-virtual {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    :cond_8
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    const-string v3, "vbtt"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 958
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 959
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v2, "push_config"

    move-object/from16 v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    :cond_9
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 963
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v2, "ad_slot_conf"

    move-object/from16 v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    :cond_a
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 967
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    const-string v3, "template_ids"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    :cond_b
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 971
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    const-string v3, "tpl_infos"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    :cond_c
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    const-string v3, "call_stack_rate"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;F)V

    .line 974
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    const-string v3, "if_pre_connect"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 975
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    const-string v3, "global_rate"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;F)V

    .line 977
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    const-string v3, "read_video_from_cache"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 978
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    const-string v3, "icon_show_time"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    .line 979
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    iget v2, v0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    const-string v3, "brand_video_cache_count"

    invoke-virtual {v1, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private static b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/j/a;
    .locals 30

    move-object/from16 v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const-string v1, "code_id"

    .line 573
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "auto_play"

    .line 574
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "voice_control"

    .line 575
    invoke-virtual {v0, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    const/4 v6, 0x2

    const-string v7, "rv_preload"

    .line 576
    invoke-virtual {v0, v7, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    const-string v8, "nv_preload"

    .line 577
    invoke-virtual {v0, v8, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    const/16 v9, 0x64

    const-string v10, "proportion_watching"

    .line 579
    invoke-virtual {v0, v10, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    const/4 v11, 0x0

    const-string v12, "skip_time_displayed"

    .line 580
    invoke-virtual {v0, v12, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v12

    const-string v13, "video_skip_result"

    .line 581
    invoke-virtual {v0, v13, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    const-string v14, "reg_creative_control"

    .line 582
    invoke-virtual {v0, v14, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v14

    const/4 v15, 0x3

    const-string v9, "play_bar_show_time"

    .line 583
    invoke-virtual {v0, v9, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    const/4 v15, -0x1

    const-string v11, "rv_skip_time"

    .line 584
    invoke-virtual {v0, v11, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v11

    const-string v2, "endcard_close_time"

    .line 585
    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    move/from16 v17, v2

    const-string v2, "playable_endcard_close_time"

    .line 586
    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 587
    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v15, "if_show_win"

    const/4 v6, 0x1

    .line 588
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    const-string v6, "sp_preload"

    move/from16 v20, v15

    const/4 v15, 0x0

    .line 589
    invoke-virtual {v0, v6, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const/16 v15, 0x5dc

    move/from16 v21, v6

    const-string v6, "stop_time"

    .line 590
    invoke-virtual {v0, v6, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v15, "native_playable_delay"

    move/from16 v22, v6

    const/4 v6, 0x2

    .line 591
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    const-string v6, "time_out_control"

    move/from16 v23, v15

    const/4 v15, -0x1

    .line 592
    invoke-virtual {v0, v6, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const/16 v15, 0x14

    move/from16 v24, v6

    const-string v6, "playable_duration_time"

    .line 593
    invoke-virtual {v0, v6, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v15, "playable_close_time"

    move/from16 v25, v6

    const/4 v6, -0x1

    .line 594
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v15, "playable_reward_type"

    move/from16 v18, v6

    const/4 v6, 0x0

    .line 595
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    move/from16 v16, v15

    const-string v15, "reward_is_callback"

    .line 596
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    const/4 v6, 0x5

    move/from16 v27, v15

    const-string v15, "iv_skip_time"

    .line 597
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v15, "close_on_click"

    move/from16 v28, v6

    const/4 v6, 0x0

    .line 598
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string v15, "parent_tpl_ids"

    .line 599
    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    move/from16 v26, v6

    const-string v6, "splash_load_type"

    move-object/from16 v29, v15

    const/4 v15, 0x2

    .line 600
    invoke-virtual {v0, v6, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v15, "splash_buffer_time"

    move/from16 v19, v6

    const/16 v6, 0x64

    .line 601
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 602
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/j/h;->j(I)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v5, 0x1

    .line 605
    :cond_1
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/j/h;->j(I)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v4, 0x1

    .line 608
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a()Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v6

    .line 609
    invoke-virtual {v6, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 610
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/j/a;->n(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 611
    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/openadsdk/core/j/a;->o(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 612
    invoke-virtual {v1, v7}, Lcom/bytedance/sdk/openadsdk/core/j/a;->p(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 613
    invoke-virtual {v1, v8}, Lcom/bytedance/sdk/openadsdk/core/j/a;->q(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 615
    invoke-virtual {v1, v10}, Lcom/bytedance/sdk/openadsdk/core/j/a;->r(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 616
    invoke-virtual {v1, v12}, Lcom/bytedance/sdk/openadsdk/core/j/a;->s(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 617
    invoke-virtual {v1, v13}, Lcom/bytedance/sdk/openadsdk/core/j/a;->t(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 618
    invoke-virtual {v1, v14}, Lcom/bytedance/sdk/openadsdk/core/j/a;->u(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 619
    invoke-virtual {v1, v9}, Lcom/bytedance/sdk/openadsdk/core/j/a;->m(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 620
    invoke-virtual {v1, v11}, Lcom/bytedance/sdk/openadsdk/core/j/a;->l(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v3, v17

    .line 621
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/j/a;->j(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 622
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->i(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 623
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->k(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v20

    .line 624
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->v(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v21

    .line 625
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->f(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v22

    .line 626
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->g(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v23

    .line 627
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->h(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v24

    .line 628
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->e(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v18

    .line 629
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v25

    .line 630
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->d(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v16

    .line 631
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->b(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v27

    .line 632
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->c(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    move/from16 v2, v19

    .line 633
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->w(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v1

    .line 634
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->x(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    move/from16 v1, v28

    .line 635
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->y(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    move-object/from16 v1, v29

    .line 636
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Lorg/json/JSONArray;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    move/from16 v1, v26

    .line 637
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Z)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    return-object v0
.end method

.method private c(Lorg/json/JSONObject;)I
    .locals 2

    const/4 v0, 0x1

    const-string v1, "splash_check_type"

    .line 840
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    return v0

    .line 845
    :cond_0
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    return p1
.end method

.method private static j(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private s(Ljava/lang/String;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 549
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 551
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    .line 553
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/j/e;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/j/e;-><init>()V

    .line 554
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "action"

    .line 555
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/j/e;->a:Ljava/lang/String;

    const-string v4, "service"

    .line 556
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/j/e;->b:Ljava/lang/String;

    const-string v4, "package"

    .line 557
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/j/e;->c:Ljava/lang/String;

    const-string v4, "wakeup_interval"

    .line 558
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/bytedance/sdk/openadsdk/core/j/e;->d:I

    .line 559
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 562
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/d;->c()Lcom/bytedance/sdk/openadsdk/core/j/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/j/d;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 564
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private t(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;
    .locals 5

    .line 1807
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 1811
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a()Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v2

    .line 1812
    invoke-virtual {v2, p1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1813
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->n(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1814
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->o(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v0, 0x2

    .line 1815
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->p(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1816
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->q(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/16 v2, 0x64

    .line 1818
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->r(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v3, 0x0

    .line 1819
    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/core/j/a;->s(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1820
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->u(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v4, 0x3

    .line 1821
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->m(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v4, -0x1

    .line 1822
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->l(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1823
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->j(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1824
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->i(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1825
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->k(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1826
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->v(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1827
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->e(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1828
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/16 v1, 0x14

    .line 1829
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/j/a;->d(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1830
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->w(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1831
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/j/a;->x(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v0, 0x5

    .line 1832
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->y(I)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1833
    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Z)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    const/4 v0, 0x0

    .line 1834
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/a;->a(Lorg/json/JSONArray;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public A()I
    .locals 3

    .line 1340
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1341
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x5

    const-string v2, "icon_show_time"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1342
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    goto :goto_0

    .line 1344
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    .line 1347
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    return v0
.end method

.method public B()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1590
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    .line 1591
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1595
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1596
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1597
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public C()I
    .locals 3

    .line 1603
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1604
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "sp_key_if_sp_cache"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1605
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    goto :goto_0

    .line 1607
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    .line 1610
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    return v0
.end method

.method public D()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1618
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    .line 1619
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1622
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1623
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1624
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public E()Z
    .locals 3

    .line 1637
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1638
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "app_list_control"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1639
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    goto :goto_0

    .line 1641
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    .line 1644
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public F()I
    .locals 3

    .line 1648
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1649
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x64

    const-string v2, "max_tpl_cnts"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1650
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    goto :goto_0

    .line 1652
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    .line 1655
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    return v0
.end method

.method public G()I
    .locals 3

    .line 1661
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    const/16 v1, 0xbb8

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1662
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "fetch_tpl_timeout_ctrl"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1663
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    goto :goto_0

    .line 1665
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    .line 1669
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    if-gtz v0, :cond_2

    .line 1670
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    .line 1674
    :cond_2
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    return v0
.end method

.method public H()I
    .locals 3

    .line 1678
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    const/16 v1, 0x14

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1679
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "webview_cache_count"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1680
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    goto :goto_0

    .line 1682
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    .line 1685
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    if-gez v0, :cond_2

    return v1

    :cond_2
    return v0
.end method

.method public I()[Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1700
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1702
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "gecko_hosts"

    if-eqz v1, :cond_1

    :try_start_1
    const-string v1, "tt_sdk_settings"

    .line 1703
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    goto :goto_0

    .line 1705
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    .line 1707
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    if-eqz v1, :cond_3

    .line 1708
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1

    .line 1712
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :catchall_0
    :cond_3
    :goto_1
    return-object v0
.end method

.method public J()I
    .locals 3

    .line 1766
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1767
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x5

    const-string v2, "vbtt"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1768
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    goto :goto_0

    .line 1770
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    .line 1773
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    return v0
.end method

.method public K()Z
    .locals 1

    .line 1874
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->R:Z

    return v0
.end method

.method public L()Z
    .locals 3

    .line 1879
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1880
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "if_pre_connect"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1881
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    goto :goto_0

    .line 1883
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    .line 1886
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public M()Z
    .locals 3

    .line 1892
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1893
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "read_video_from_cache"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1894
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    goto :goto_0

    .line 1896
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    .line 1899
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public N()I
    .locals 1

    .line 1903
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    return v0
.end method

.method public a(Ljava/lang/String;Z)I
    .locals 2

    if-nez p1, :cond_0

    .line 1542
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Z)I

    move-result p1

    return p1

    .line 1543
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1544
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->u:I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Z)I

    move-result p1

    :goto_0
    return p1
.end method

.method public declared-synchronized a()V
    .locals 16

    move-object/from16 v1, p0

    monitor-enter p0

    const/4 v2, 0x1

    .line 331
    :try_start_0
    iput-boolean v2, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->R:Z

    .line 332
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v10, 0xbb8

    const/16 v11, 0x64

    const/high16 v12, 0x3f800000    # 1.0f

    const/16 v13, 0xe10

    const/16 v14, 0x1e

    const/16 v15, 0x32

    const-wide/16 v3, 0x2710

    const/4 v5, 0x5

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v0, :cond_6

    const-string v0, "tt_sdk_settings"

    const-string v8, "url_ads"

    const-string v9, "pangolin.snssdk.com"

    .line 334
    invoke-static {v0, v8, v9}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v8, "url_alog"

    const-string v9, "extlog.snssdk.com/service/2/app_log/"

    .line 335
    invoke-static {v0, v8, v9}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v8, "xpath"

    const-string v9, ""

    .line 336
    invoke-static {v0, v8, v9}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v8, "duration"

    .line 337
    invoke-static {v0, v8, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    const-string v0, "tt_sdk_settings"

    const-string v3, "max"

    .line 338
    invoke-static {v0, v3, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "download_config_dl_network"

    .line 339
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "download_config_dl_size"

    .line 340
    invoke-static {v0, v3, v14}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "download_config_storage_internal"

    .line 341
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "vbtt"

    .line 343
    invoke-static {v0, v3, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "fetch_template"

    .line 344
    invoke-static {v0, v3, v13}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "template_ids"

    .line 345
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "ab_test_version"

    .line 346
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "ab_test_param"

    .line 347
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "web_info_wifi_enable"

    .line 348
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "web_info_page_count"

    .line 349
    invoke-static {v0, v3, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "pyload_h5"

    .line 350
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "playableLoadH5Url"

    .line 351
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "splash_check_type"

    .line 352
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "if_both_open"

    .line 353
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "support_tnc"

    .line 354
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "tpl_infos"

    .line 355
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "if_pre_connect"

    .line 357
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "global_rate"

    .line 358
    invoke-static {v0, v3, v12}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    const-string v0, "tt_sdk_settings"

    const-string v3, "app_list_control"

    .line 359
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "max_tpl_cnts"

    .line 360
    invoke-static {v0, v3, v11}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "fetch_tpl_timeout_ctrl"

    .line 362
    invoke-static {v0, v3, v10}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "webview_cache_count"

    const/16 v4, 0x14

    .line 363
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "webview_render_concurrent_count"

    const/4 v4, 0x3

    .line 364
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    .line 365
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    const-string v0, "tt_sdk_settings"

    const-string v3, "gecko_hosts"

    .line 368
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    .line 369
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    const-string v0, "tt_sdk_settings"

    const-string v3, "hit_app_list_time"

    const-wide/16 v8, 0x0

    .line 370
    invoke-static {v0, v3, v8, v9}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    .line 371
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "tt_sdk_settings"

    const-string v3, "circle_splash_switch"

    .line 373
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "circle_load_splash_time"

    const/4 v4, -0x1

    .line 374
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "sp_key_if_sp_cache"

    .line 376
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "icon_show_time"

    .line 378
    invoke-static {v0, v3, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "dyn_draw_engine_url"

    const-string v4, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    .line 379
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v3, "hit_app_list_data"

    .line 380
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 381
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 382
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 383
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 386
    :cond_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "tt_sdk_settings"

    const-string v3, "scheme_list_data"

    .line 387
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 388
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 389
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 390
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v0, "tt_sdk_settings"

    const-string v3, "push_config"

    .line 393
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->s(Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v3, "ad_slot_conf"

    .line 397
    invoke-static {v0, v3, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_4

    .line 400
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 401
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 403
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_4

    .line 405
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 406
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 408
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    iget-object v8, v5, Lcom/bytedance/sdk/openadsdk/core/j/a;->a:Ljava/lang/String;

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catch_0
    :cond_4
    :try_start_2
    const-string v0, "tt_sdk_settings"

    const-string v3, "download_sdk_config"

    const-string v4, ""

    .line 416
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    .line 417
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_5

    .line 419
    :try_start_3
    new-instance v0, Lorg/json/JSONObject;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v0

    .line 421
    :try_start_4
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_5
    :goto_3
    const-string v0, "tt_sdk_settings"

    const-string v3, "enable_download_opt"

    .line 424
    invoke-static {v0, v3, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "call_stack_rate"

    const/4 v4, 0x0

    .line 425
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    const-string v0, "tt_sdk_settings"

    const-string v3, "read_video_from_cache"

    .line 426
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    const-string v0, "tt_sdk_settings"

    const-string v2, "brand_video_cache_count"

    const/4 v3, 0x2

    .line 427
    invoke-static {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    .line 428
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 429
    monitor-exit p0

    return-void

    .line 432
    :cond_6
    :try_start_5
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v8, "url_ads"

    const-string v9, "pangolin.snssdk.com"

    invoke-virtual {v0, v8, v9}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    .line 433
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v8, "url_alog"

    const-string v9, "extlog.snssdk.com/service/2/app_log/"

    invoke-virtual {v0, v8, v9}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    .line 434
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v8, "xpath"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    .line 435
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v8, "duration"

    invoke-virtual {v0, v8, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    .line 436
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "max"

    invoke-virtual {v0, v3, v15}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    .line 437
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "download_config_dl_network"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    .line 438
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "download_config_dl_size"

    invoke-virtual {v0, v3, v14}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    .line 439
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "download_config_storage_internal"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    .line 441
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "fetch_template"

    invoke-virtual {v0, v3, v13}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    .line 442
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "ab_test_version"

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    .line 443
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "ab_test_param"

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    .line 445
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "vbtt"

    invoke-virtual {v0, v3, v5}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    .line 446
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "template_ids"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->g:Ljava/lang/String;

    .line 447
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "web_info_wifi_enable"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    .line 448
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "web_info_page_count"

    invoke-virtual {v0, v3, v5}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    .line 449
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "pyload_h5"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    .line 450
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "playableLoadH5Url"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    .line 451
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "splash_check_type"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    .line 452
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "if_both_open"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    .line 453
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "support_tnc"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    .line 454
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "tpl_infos"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->e:Ljava/lang/String;

    .line 456
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "if_pre_connect"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    .line 457
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "global_rate"

    invoke-virtual {v0, v3, v12}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;F)F

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    .line 459
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "app_list_control"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    .line 460
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "max_tpl_cnts"

    invoke-virtual {v0, v3, v11}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    .line 462
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "fetch_tpl_timeout_ctrl"

    invoke-virtual {v0, v3, v10}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    .line 463
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "webview_cache_count"

    const/16 v4, 0x14

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    .line 464
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "webview_render_concurrent_count"

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    .line 465
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    if-eqz v0, :cond_7

    .line 466
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 468
    :cond_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "gecko_hosts"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    .line 469
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    .line 470
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "hit_app_list_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v3, v8, v9}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    .line 471
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 473
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "circle_splash_switch"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    .line 474
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "circle_load_splash_time"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    .line 475
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "dyn_draw_engine_url"

    const-string v4, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    .line 476
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "sp_key_if_sp_cache"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    .line 477
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "icon_show_time"

    invoke-virtual {v0, v3, v5}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    .line 479
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "hit_app_list_data"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 480
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 481
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 482
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 485
    :cond_8
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 486
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "scheme_list_data"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 487
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 488
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 489
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 492
    :cond_9
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "push_config"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 493
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->s(Ljava/lang/String;)V

    .line 496
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "ad_slot_conf"

    invoke-virtual {v0, v3, v7}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 497
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v3, :cond_b

    .line 499
    :try_start_6
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 502
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v0, :cond_b

    .line 504
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 505
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 507
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    iget-object v8, v5, Lcom/bytedance/sdk/openadsdk/core/j/a;->a:Ljava/lang/String;

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 515
    :catch_2
    :cond_b
    :try_start_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "download_sdk_config"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    .line 516
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v0, :cond_c

    .line 518
    :try_start_8
    new-instance v0, Lorg/json/JSONObject;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_7

    :catch_3
    move-exception v0

    .line 520
    :try_start_9
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 523
    :cond_c
    :goto_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "enable_download_opt"

    invoke-virtual {v0, v3, v6}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    .line 524
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "call_stack_rate"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;F)F

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    .line 526
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v3, "read_video_from_cache"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    .line 527
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    const-string v2, "brand_video_cache_count"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    .line 528
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 529
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 9

    const-string v0, "dyn_draw_engine_url"

    const-string v1, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    .line 647
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    const-string v0, "ads_url"

    const-string v1, "pangolin.snssdk.com"

    .line 648
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    const-string v0, "app_log_url"

    const-string v1, "extlog.snssdk.com/service/2/app_log/"

    .line 649
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    const-string v0, "xpath"

    .line 651
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    const-string v0, "feq_policy"

    .line 653
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "duration"

    .line 655
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    const-string v1, "max"

    .line 656
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    :cond_0
    const/4 v0, 0x5

    const-string v1, "vbtt"

    .line 659
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->F:I

    const/16 v1, 0xe10

    const-string v2, "fetch_tpl_interval"

    .line 660
    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->G:I

    const-string v1, "abtest"

    .line 663
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "version"

    .line 667
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    const-string v2, "param"

    .line 668
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    goto :goto_0

    .line 671
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->O()V

    :goto_0
    const/4 v1, 0x1

    const-string v2, "read_video_from_cache"

    .line 674
    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->V:I

    const-string v2, "web_info"

    .line 678
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "web_info_wifi_enable"

    .line 680
    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    const-string v3, "web_info_page_count"

    .line 681
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    :cond_2
    const-string v2, "log_rate_conf"

    .line 684
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_4

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-string v5, "global_rate"

    .line 686
    invoke-virtual {v2, v5, v3, v4}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v3

    double-to-float v3, v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    const-string v3, "call_stack_rate"

    .line 687
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 688
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    goto :goto_1

    .line 690
    :cond_3
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    :cond_4
    :goto_1
    const-string v2, "pyload_h5"

    .line 694
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    const-string v2, "pure_pyload_h5"

    .line 695
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    .line 698
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->c(Lorg/json/JSONObject;)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    .line 699
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting-\u300bmSplashCheckType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "splashLoad"

    invoke-static {v3, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "if_both_open"

    .line 701
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->B:I

    const-string v3, "support_tnc"

    .line 702
    invoke-virtual {p1, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    const-string v3, "al"

    .line 704
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->x:I

    const/16 v3, 0x64

    const-string v4, "max_tpl_cnts"

    .line 705
    invoke-virtual {p1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->J:I

    const-string v3, "app_common_config"

    .line 707
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_6

    const/16 v4, 0xbb8

    const-string v5, "fetch_tpl_timeout_ctrl"

    .line 709
    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->K:I

    const-string v4, "circle_splash"

    .line 710
    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    const/4 v4, -0x1

    const-string v5, "circle_time"

    .line 711
    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    const-string v4, "if_sp_cache"

    .line 712
    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->N:I

    const-string v4, "if_pre_connect"

    .line 713
    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->O:I

    const/16 v4, 0x14

    const-string v5, "webview_cache_count"

    .line 714
    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->P:I

    const/4 v4, 0x3

    const-string v5, "webview_render_concurrent_count"

    .line 715
    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->Q:I

    const-string v4, "icon_show_time"

    .line 716
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->W:I

    .line 719
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "gecko_hosts"

    .line 720
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 721
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    .line 722
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 723
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 726
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->L:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    .line 728
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GeckoLog: settings json error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;)V

    .line 732
    :cond_6
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->y:J

    .line 733
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "spam_app_list"

    .line 734
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 736
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v3, :cond_8

    .line 738
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 739
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 740
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->z:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 745
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "scheme_check_list"

    .line 746
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 748
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v3, :cond_a

    .line 750
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 751
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 752
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->A:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_a
    const-string v0, "download_config"

    .line 757
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v3, "dl_network"

    .line 759
    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    const/16 v3, 0x1e

    const-string v4, "dl_size"

    .line 760
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    const-string v3, "if_storage_internal"

    .line 761
    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    const-string v1, "enable_download_opt"

    .line 762
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    :cond_b
    const-string v0, "download_sdk_config"

    .line 764
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;

    if-eqz v0, :cond_c

    .line 766
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    goto :goto_6

    :cond_c
    const-string v0, ""

    .line 768
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    :goto_6
    const/4 v0, 0x0

    :try_start_1
    const-string v1, "push_config"

    .line 774
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 776
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 778
    :try_start_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    const/4 v5, 0x0

    :goto_7
    if-ge v5, v4, :cond_f

    .line 780
    new-instance v6, Lcom/bytedance/sdk/openadsdk/core/j/e;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/core/j/e;-><init>()V

    .line 781
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    if-eqz v7, :cond_d

    const-string v8, "action"

    .line 783
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/j/e;->a:Ljava/lang/String;

    const-string v8, "service"

    .line 784
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/j/e;->b:Ljava/lang/String;

    const-string v8, "package"

    .line 785
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/j/e;->c:Ljava/lang/String;

    const-string v8, "wakeup_interval"

    .line 786
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v6, Lcom/bytedance/sdk/openadsdk/core/j/e;->d:I

    .line 787
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->i:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_e
    move-object v3, v0

    .line 792
    :cond_f
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/d;->c()Lcom/bytedance/sdk/openadsdk/core/j/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/j/d;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_9

    :catchall_1
    move-exception v1

    goto :goto_8

    :catchall_2
    move-exception v1

    move-object v3, v0

    .line 794
    :goto_8
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_9
    const-string v1, "ad_slot_conf_list"

    .line 800
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 802
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    .line 803
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_11

    .line 805
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    :goto_a
    if-ge v2, v4, :cond_11

    .line 807
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 808
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 810
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    iget-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/j/a;->a:Ljava/lang/String;

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/core/j/a;->a:Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/bytedance/sdk/openadsdk/p/a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 819
    :cond_11
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    const-string v2, "pre_cache_brand_count"

    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->X:I

    .line 821
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V

    .line 822
    invoke-direct {p0, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(I)Z
    .locals 0

    .line 1258
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(I)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .line 1368
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1370
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->e:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eq p1, v1, :cond_0

    return v2

    .line 1375
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1372
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .line 993
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 994
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "url_ads"

    const-string v2, "pangolin.snssdk.com"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 996
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    goto :goto_0

    .line 998
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    .line 1000
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1001
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    .line 1004
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    .line 1357
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1358
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->c:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .line 1397
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1398
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->g:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 0

    .line 1408
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1409
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->s:I

    return p1
.end method

.method public c()V
    .locals 1

    const-string v0, "api-access.pangolin-sdk-toutiao-b.com"

    .line 1008
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->H:Ljava/lang/String;

    return-void
.end method

.method public c(I)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1575
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1576
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->n:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public d(I)I
    .locals 0

    .line 1719
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1720
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->b:I

    return p1
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .line 1018
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1019
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "url_alog"

    const-string v2, "extlog.snssdk.com/service/2/app_log/"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1021
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    goto :goto_0

    .line 1023
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    .line 1025
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1026
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    .line 1029
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->I:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 1414
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1415
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->o:I

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e(I)I
    .locals 0

    .line 1724
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1725
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->i:I

    return p1
.end method

.method public e()Z
    .locals 3

    .line 1052
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1053
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "support_tnc"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1054
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    goto :goto_0

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    .line 1059
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->C:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public e(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 1428
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1429
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->p:I

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()I
    .locals 3

    .line 1063
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1064
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "download_config_dl_network"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1065
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    goto :goto_0

    .line 1067
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    .line 1070
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->k:I

    return v0
.end method

.method public f(I)I
    .locals 2

    .line 1737
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1738
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->A:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    return v0

    :cond_0
    return p1
.end method

.method public f(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/16 p1, 0x5dc

    return p1

    .line 1439
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1440
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->q:I

    return p1
.end method

.method public g()I
    .locals 3

    .line 1074
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1075
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x1e

    const-string v2, "download_config_dl_size"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1076
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    goto :goto_0

    .line 1078
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    .line 1082
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->l:I

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    return v0
.end method

.method public g(I)I
    .locals 0

    .line 1751
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1752
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->B:I

    return p1
.end method

.method public g(Ljava/lang/String;)I
    .locals 1

    .line 1450
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1451
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->k:I

    return p1
.end method

.method public h(I)I
    .locals 0

    .line 1756
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1757
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->x:I

    return p1
.end method

.method public h()Ljava/lang/String;
    .locals 3

    .line 1086
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1087
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "ab_test_version"

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const-string v2, "tt_sdk_settings"

    .line 1088
    invoke-static {v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    goto :goto_0

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    .line 1093
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->q:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 6

    const/4 v0, 0x0

    .line 1457
    :try_start_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 1458
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1461
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1462
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1463
    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/v;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1465
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "id"

    .line 1466
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/v;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "md5"

    .line 1467
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/v;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1468
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_2
    return-object v1

    :catch_0
    :cond_3
    :goto_1
    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 3

    .line 1097
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1098
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "ab_test_param"

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const-string v2, "tt_sdk_settings"

    .line 1099
    invoke-static {v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    goto :goto_0

    .line 1101
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    .line 1104
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->r:Ljava/lang/String;

    return-object v0
.end method

.method public i(I)Z
    .locals 0

    .line 1761
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1762
    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->y:Z

    return p1
.end method

.method public i(Ljava/lang/String;)Z
    .locals 1

    .line 1480
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1481
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->z:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :catch_0
    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public j(Ljava/lang/String;)I
    .locals 0

    .line 1497
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1498
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->h:I

    return p1
.end method

.method public j()Ljava/lang/String;
    .locals 3

    .line 1108
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1109
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, ""

    const-string v2, "xpath"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1110
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    goto :goto_0

    .line 1112
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    .line 1115
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k(Ljava/lang/String;)I
    .locals 0

    .line 1508
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1509
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->j:I

    return p1
.end method

.method public k()J
    .locals 5

    .line 1119
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 1120
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-wide/16 v1, 0x2710

    const-string v3, "duration"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1121
    invoke-static {v0, v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    goto :goto_0

    .line 1123
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v3, v1, v2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    .line 1126
    :cond_1
    :goto_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->D:J

    return-wide v0
.end method

.method public l()I
    .locals 3

    .line 1130
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1131
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x32

    const-string v2, "max"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1132
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    goto :goto_0

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    .line 1137
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->E:I

    return v0
.end method

.method public l(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 1516
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1517
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->v:I

    return p1
.end method

.method public m()F
    .locals 3

    .line 1152
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 1153
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "call_stack_rate"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1154
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    goto :goto_0

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    .line 1159
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->S:F

    return v0
.end method

.method public m(Ljava/lang/String;)Z
    .locals 0

    .line 1526
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->w:I

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public n()I
    .locals 3

    .line 1174
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1175
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x5

    const-string v2, "web_info_page_count"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1176
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    goto :goto_0

    .line 1178
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    .line 1181
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->t:I

    return v0
.end method

.method public n(Ljava/lang/String;)I
    .locals 2

    const/16 v0, 0x14

    if-nez p1, :cond_0

    return v0

    .line 1534
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1535
    iget v1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->t:I

    if-lez v1, :cond_1

    iget v0, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->t:I

    :cond_1
    return v0
.end method

.method public o(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 1552
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1553
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->l:I

    return p1
.end method

.method public o()Ljava/lang/String;
    .locals 3

    .line 1185
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1186
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "pyload_h5"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1187
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    goto :goto_0

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    .line 1192
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->u:Ljava/lang/String;

    return-object v0
.end method

.method public p(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 1561
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object p1

    .line 1562
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/j/a;->m:I

    return p1
.end method

.method public p()Ljava/lang/String;
    .locals 3

    .line 1196
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "playableLoadH5Url"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1198
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    goto :goto_0

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    .line 1203
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->v:Ljava/lang/String;

    return-object v0
.end method

.method public q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;
    .locals 7

    .line 1777
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    const-string v1, "tt_sdk_settings"

    const-string v2, "ad_slot_conf"

    .line 1778
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1779
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1781
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1782
    :try_start_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1783
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1784
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 1787
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 1788
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/j/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1790
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    iget-object v6, v4, Lcom/bytedance/sdk/openadsdk/core/j/a;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1795
    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    nop

    .line 1801
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j/a;

    if-nez v0, :cond_3

    .line 1802
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->t(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/j/a;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method public q()Lorg/json/JSONObject;
    .locals 3

    .line 1210
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_sdk_config"

    const-string v2, ""

    .line 1211
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    .line 1212
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->p:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1216
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 1221
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->o:Lorg/json/JSONObject;

    return-object v0
.end method

.method public r(Ljava/lang/String;)V
    .locals 1

    .line 1852
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public r()Z
    .locals 3

    .line 1225
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1226
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "enable_download_opt"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1227
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    goto :goto_0

    .line 1229
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    .line 1232
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->n:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public s()I
    .locals 3

    .line 1236
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1237
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "splash_check_type"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1238
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    goto :goto_0

    .line 1240
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    .line 1243
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->w:I

    return v0
.end method

.method public t()Z
    .locals 2

    .line 1268
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->s()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public u()Z
    .locals 3

    .line 1272
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1273
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "web_info_wifi_enable"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1274
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    goto :goto_0

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    .line 1280
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->s:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public v()Z
    .locals 3

    .line 1284
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x4f000000

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 1285
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "global_rate"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1286
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    goto :goto_0

    .line 1288
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    .line 1291
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->U:F

    float-to-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public w()Z
    .locals 3

    .line 1295
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1296
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v2, "download_config_storage_internal"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1297
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    goto :goto_0

    .line 1299
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    .line 1302
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->m:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public x()I
    .locals 3

    .line 1306
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1307
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "circle_splash_switch"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1308
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    goto :goto_0

    .line 1310
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    .line 1314
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->a:I

    return v0
.end method

.method public y()I
    .locals 3

    .line 1318
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1319
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, -0x1

    const-string v2, "circle_load_splash_time"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1320
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    goto :goto_0

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    .line 1325
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->b:I

    return v0
.end method

.method public z()Ljava/lang/String;
    .locals 3

    .line 1329
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1330
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-string v1, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    const-string v2, "dyn_draw_engine_url"

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    .line 1331
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    goto :goto_0

    .line 1333
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->T:Lcom/bytedance/sdk/component/utils/s;

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    .line 1336
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j/h;->M:Ljava/lang/String;

    return-object v0
.end method
