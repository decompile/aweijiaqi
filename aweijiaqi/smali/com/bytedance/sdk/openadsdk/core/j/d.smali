.class public Lcom/bytedance/sdk/openadsdk/core/j/d;
.super Lcom/bytedance/sdk/component/e/g;
.source "ProcessPushHelper.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/core/j/d;


# direct methods
.method private constructor <init>()V
    .locals 1

    const-string v0, "ProcessPushHelper"

    .line 49
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static c()Lcom/bytedance/sdk/openadsdk/core/j/d;
    .locals 2

    .line 38
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/j/d;->a:Lcom/bytedance/sdk/openadsdk/core/j/d;

    if-nez v0, :cond_1

    .line 39
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/j/f;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/j/d;->a:Lcom/bytedance/sdk/openadsdk/core/j/d;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/j/d;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/j/d;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/j/d;->a:Lcom/bytedance/sdk/openadsdk/core/j/d;

    .line 43
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 45
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/j/d;->a:Lcom/bytedance/sdk/openadsdk/core/j/d;

    return-object v0
.end method

.method private e()Z
    .locals 1

    .line 136
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public d()V
    .locals 1

    const/4 v0, 0x1

    .line 53
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method public run()V
    .locals 2

    .line 58
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/j/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-wide/16 v0, 0x3e8

    .line 63
    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
