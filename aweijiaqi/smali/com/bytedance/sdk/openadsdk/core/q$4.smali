.class Lcom/bytedance/sdk/openadsdk/core/q$4;
.super Lcom/bytedance/sdk/component/e/g;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/q;->a(JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:I

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/core/q$a;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:J

.field final synthetic g:Lcom/bytedance/sdk/openadsdk/core/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/q;Ljava/lang/String;ILjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;Ljava/lang/String;J)V
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->g:Lcom/bytedance/sdk/openadsdk/core/q;

    iput p3, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->a:I

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->b:Ljava/lang/String;

    iput p5, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->c:I

    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iput-object p7, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->e:Ljava/lang/String;

    iput-wide p8, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->f:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 619
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->a:I

    .line 620
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->b:Ljava/lang/String;

    .line 621
    :goto_0
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/a/b;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/k/a/b;-><init>()V

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->c:I

    .line 622
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/b;->a(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/k/a/b;

    .line 623
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/k/a/b;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/k/a/b;

    .line 624
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/b;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/k/a/b;

    const/4 v1, 0x0

    .line 630
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-string v3, ""

    if-eqz v2, :cond_2

    :try_start_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    .line 631
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    .line 632
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 633
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 634
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v2

    .line 635
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v4, :cond_1

    .line 637
    :try_start_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "req_id"

    .line 638
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    :cond_1
    move-object v4, v3

    goto :goto_1

    :cond_2
    move-object v2, v3

    move-object v4, v2

    .line 644
    :goto_1
    :try_start_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    if-eqz v5, :cond_3

    .line 645
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/a;->a()Ljava/lang/String;

    move-result-object v4

    .line 648
    :cond_3
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/k/a/b;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v4

    check-cast v4, Lcom/bytedance/sdk/openadsdk/k/a/b;

    if-eqz v1, :cond_4

    .line 649
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v3

    :cond_4
    invoke-virtual {v4, v3}, Lcom/bytedance/sdk/openadsdk/k/a/b;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/k/a/b;

    .line 650
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/k/a/b;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->e:Ljava/lang/String;

    .line 651
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/b;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/k/a/b;

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->f:J

    .line 652
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/b;->a(J)Lcom/bytedance/sdk/openadsdk/k/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$4;->d:Lcom/bytedance/sdk/openadsdk/core/q$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/core/q$a;->a:I

    int-to-long v2, v2

    goto :goto_2

    :cond_5
    const-wide/16 v2, 0x0

    .line 653
    :goto_2
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/b;->b(J)Lcom/bytedance/sdk/openadsdk/k/a/b;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v1

    const-string v2, "NetApiImpl"

    const-string v3, "uploadAdTypeTimeOutEvent throws exception "

    .line 655
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 657
    :goto_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/k/a;->e(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    return-void
.end method
