.class public Lcom/bytedance/sdk/openadsdk/core/i/c;
.super Lcom/bytedance/sdk/openadsdk/core/i/a;
.source "MSSdkImpl.java"


# instance fields
.field private a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

.field private volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/a;-><init>()V

    .line 40
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->b:Z

    return-void
.end method

.method private c()Z
    .locals 6

    .line 45
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    .line 46
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->h()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 48
    new-instance v3, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;

    const-string v4, "1371"

    const-string v5, "THYFfhd167Y/Etj/JFI+OYhGnAsIhCvIXKQbbKuslfRMO6XQmCuZImqOyljyF6dQ900Hy8ecQzUcHu72ks7Xvvncqt7BZjf4VSth/OzZbJlDJqtayy2lcb5mqCQUzE5fIvFXAZkyxl+SRzGnzUojBcyqITZ3bGRvteMi+qu/15oKM3BWY0IDJ9Ry5FUGfzt+FyCqvZI8PFQNAzvZXcWHlJoRXydZUjUbtEy/AFUvusIO1HDx"

    invoke-direct {v3, v4, v1, v5}, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v3, v2}, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;->setDeviceID(Ljava/lang/String;)Lms/bd/c/e0$b;

    move-result-object v1

    check-cast v1, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;

    const/4 v3, 0x1

    .line 50
    invoke-virtual {v1, v3}, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;->setClientType(I)Lms/bd/c/e0$b;

    move-result-object v1

    check-cast v1, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;

    .line 51
    invoke-virtual {v1}, Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;->build()Lcom/bytedance/mobsec/metasec/ml/MSConfig;

    move-result-object v1

    .line 52
    invoke-static {v0, v1}, Lcom/bytedance/mobsec/metasec/ml/MSManagerUtils;->init(Landroid/content/Context;Lcom/bytedance/mobsec/metasec/ml/MSConfig;)Z

    .line 53
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->e()V

    .line 54
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    invoke-virtual {v0, v2}, Lcom/bytedance/mobsec/metasec/ml/MSManager;->setDeviceID(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return v3

    :catchall_0
    move-exception v0

    const-string v1, "MSSdkImpl"

    const-string v2, "appid \u4e3a\u7a7a\uff0c\u521d\u59cb\u5316\u5931\u8d25\uff01"

    .line 59
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return v0
.end method

.method private declared-synchronized d()Z
    .locals 1

    monitor-enter p0

    .line 65
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->b:Z

    if-nez v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->b:Z

    .line 68
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-nez v0, :cond_0

    const-string v0, "1371"

    .line 74
    invoke-static {v0}, Lcom/bytedance/mobsec/metasec/ml/MSManagerUtils;->get(Ljava/lang/String;)Lcom/bytedance/mobsec/metasec/ml/MSManager;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;[B)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 124
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1

    .line 127
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->e()V

    .line 128
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/mobsec/metasec/ml/MSManager;->doHttpReqSignByUrl(Ljava/lang/String;[B)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 131
    :cond_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    return-object p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 80
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 83
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->e()V

    .line 84
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {v0, p1}, Lcom/bytedance/mobsec/metasec/ml/MSManager;->setDeviceID(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .line 102
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->d()Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 105
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->e()V

    .line 106
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {v0}, Lcom/bytedance/mobsec/metasec/ml/MSManager;->getSecDeviceToken()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v1
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 91
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/i/c;->e()V

    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/i/c;->a:Lcom/bytedance/mobsec/metasec/ml/MSManager;

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {v0, p1}, Lcom/bytedance/mobsec/metasec/ml/MSManager;->report(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
