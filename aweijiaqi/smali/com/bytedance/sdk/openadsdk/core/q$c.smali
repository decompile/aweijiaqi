.class public Lcom/bytedance/sdk/openadsdk/core/q$c;
.super Ljava/lang/Object;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Lcom/bytedance/sdk/openadsdk/core/e/w;


# direct methods
.method private constructor <init>(IZLcom/bytedance/sdk/openadsdk/core/e/w;)V
    .locals 0

    .line 1504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1505
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$c;->a:I

    .line 1506
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/q$c;->b:Z

    .line 1507
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/q$c;->c:Lcom/bytedance/sdk/openadsdk/core/e/w;

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/q$c;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "code"

    .line 1514
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "verify"

    .line 1515
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "data"

    .line 1516
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    .line 1517
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/e/w;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/e/w;-><init>()V

    if-eqz p0, :cond_1

    :try_start_0
    const-string v3, "reason"

    .line 1520
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/e/w;->a(I)V

    const-string v3, "corp_type"

    .line 1521
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/e/w;->b(I)V

    const-string v3, "reward_amount"

    .line 1522
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/e/w;->c(I)V

    const-string v3, "reward_name"

    .line 1523
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/bytedance/sdk/openadsdk/core/e/w;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 1526
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1528
    :cond_1
    :goto_0
    new-instance p0, Lcom/bytedance/sdk/openadsdk/core/q$c;

    invoke-direct {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/q$c;-><init>(IZLcom/bytedance/sdk/openadsdk/core/e/w;)V

    return-object p0
.end method
