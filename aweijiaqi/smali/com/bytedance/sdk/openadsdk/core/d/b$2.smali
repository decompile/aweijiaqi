.class Lcom/bytedance/sdk/openadsdk/core/d/b$2;
.super Ljava/lang/Object;
.source "TTInteractionExpressAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/EmptyView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/core/d/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/d/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a()V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    const-string v0, "TTInteractionExpressAd"

    const-string v1, "ExpressView SHOW"

    .line 271
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/b;J)J

    .line 273
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 274
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/core/d/b;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->p()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "dynamic_show_type"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pangolin ad show "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v2, p1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdEvent"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/core/d/b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b(Lcom/bytedance/sdk/openadsdk/core/d/b;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/util/Map;)V

    .line 277
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->d:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->d:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;->onAdShow(Landroid/view/View;I)V

    .line 280
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/d/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 283
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/d/b;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-eqz p1, :cond_1

    .line 284
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/d/b;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->l()V

    .line 285
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/d/b;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->j()V

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 5

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExpressView onWindowFocusChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTInteractionExpressAd"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->b()V

    goto :goto_0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->c()V

    .line 251
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/b;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b(Lcom/bytedance/sdk/openadsdk/core/d/b;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, p1, v3, v4}, Lcom/bytedance/sdk/openadsdk/e/d;->a(JZLcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/b;J)J

    return-void
.end method

.method public b()V
    .locals 5

    .line 263
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/b;->h:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->d()V

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/b;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/b$2;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b(Lcom/bytedance/sdk/openadsdk/core/d/b;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/e/d;->a(JLcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/b;J)J

    return-void
.end method
