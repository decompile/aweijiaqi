.class Lcom/bytedance/sdk/openadsdk/core/d/a;
.super Lcom/bytedance/sdk/openadsdk/core/nativeexpress/BackupView;
.source "InteractionExpressBackupView.java"


# static fields
.field private static l:[Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;


# instance fields
.field private m:Landroid/view/View;

.field private n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

.field private o:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private p:I

.field private q:Landroid/app/Dialog;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

.field private u:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    .line 39
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    const/4 v3, 0x1

    const/16 v4, 0x12c

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v5, v4, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;-><init>(IFII)V

    const/4 v5, 0x0

    aput-object v2, v1, v5

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    const/4 v5, 0x2

    const v6, 0x3f2aaaab

    const/16 v7, 0x1c2

    invoke-direct {v2, v5, v6, v4, v7}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;-><init>(IFII)V

    aput-object v2, v1, v3

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    const/high16 v3, 0x3fc00000    # 1.5f

    const/16 v6, 0xc8

    invoke-direct {v2, v0, v3, v4, v6}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;-><init>(IFII)V

    aput-object v2, v1, v5

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/d/a;->l:[Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 57
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/BackupView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 49
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->p:I

    .line 58
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/d/a;)Landroid/app/Dialog;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->q:Landroid/app/Dialog;

    return-object p0
.end method

.method private a(II)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;
    .locals 8

    int-to-float p1, p1

    const/4 v0, 0x0

    .line 314
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    int-to-float p2, p2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    div-float/2addr p1, p2

    .line 316
    sget-object p2, Lcom/bytedance/sdk/openadsdk/core/d/a;->l:[Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    aget-object p2, p2, v0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 318
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/d/a;->l:[Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    .line 319
    iget v6, v5, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->c:F

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v7, v6, v1

    if-gtz v7, :cond_0

    move-object p2, v5

    move v1, v6

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-object p2

    .line 328
    :catchall_0
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/d/a;->l:[Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    aget-object p1, p1, v0

    return-object p1
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ad()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/e/l;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/g/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/component/d/e;->a(Landroid/widget/ImageView;)Lcom/bytedance/sdk/component/d/d;

    return-void
.end method

.method private b()V
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressWidth()I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 96
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressHeight()I

    move-result v1

    .line 95
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(II)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    .line 98
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressWidth()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 99
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressWidth()I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressHeight()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->c:F

    mul-float v1, v1, v2

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    .line 102
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    .line 105
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getExpectExpressWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->c:F

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->d:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    .line 109
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->e:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    .line 113
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v1

    if-le v0, v1, :cond_2

    .line 114
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 115
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    .line 116
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    int-to-float v1, v1

    mul-float v1, v1, v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    .line 120
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_3

    .line 122
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 124
    :cond_3
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 125
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->g:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 126
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 129
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()V

    goto :goto_1

    .line 130
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 131
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->d()V

    goto :goto_1

    .line 132
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 133
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->e()V

    goto :goto_1

    .line 135
    :cond_6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()V

    :goto_1
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 200
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/d/a$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/d/a$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private c()V
    .locals 12

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v2, "tt_backup_insert_layout1"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    .line 147
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v3, "tt_ad_container"

    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 148
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v4, "tt_bu_img"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 149
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_close"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    .line 150
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_icon"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 151
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_title"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    .line 152
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_desc"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    .line 153
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_download"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 154
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v7, "tt_bu_dislike"

    invoke-static {v6, v7}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 156
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const/high16 v7, 0x41700000    # 15.0f

    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v6

    float-to-int v6, v6

    .line 157
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-static {v7, v6, v6, v6, v6}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;IIII)V

    .line 158
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-direct {p0, v6}, Lcom/bytedance/sdk/openadsdk/core/d/a;->b(Landroid/view/View;)V

    .line 159
    new-instance v6, Lcom/bytedance/sdk/openadsdk/core/d/a$1;

    invoke-direct {v6, p0}, Lcom/bytedance/sdk/openadsdk/core/d/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 166
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    :cond_0
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v6

    const/16 v7, 0x8

    const/4 v8, 0x0

    if-eqz v6, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getVideoView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 174
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 175
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    iget v10, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iget v11, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    mul-int/lit8 v11, v11, 0x9

    div-int/lit8 v11, v11, 0x10

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v10, 0x11

    .line 176
    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 177
    invoke-virtual {v0, v6, v8, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 179
    :cond_1
    invoke-static {v1, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 180
    invoke-static {v0, v8}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 182
    :cond_2
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/widget/ImageView;)V

    .line 183
    invoke-static {v1, v8}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 184
    invoke-static {v0, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 186
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/g/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/bytedance/sdk/component/d/e;->a(Landroid/widget/ImageView;)Lcom/bytedance/sdk/component/d/d;

    .line 187
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    invoke-virtual {p0, p0, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 191
    invoke-virtual {p0, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 192
    invoke-virtual {p0, v5, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 193
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;)V

    return-void
.end method

.method private c(I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 340
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->g()V

    .line 341
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    const-string v0, "#2c2c2c"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 343
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->f()V

    .line 344
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_0
    return-void
.end method

.method private d()V
    .locals 12

    .line 212
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v2, "tt_backup_insert_layout2"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    .line 214
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v3, "tt_ad_container"

    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 215
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v4, "tt_bu_img"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 216
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_close"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    .line 217
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_icon"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 218
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_title"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    .line 219
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_desc"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    .line 220
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v6, "tt_bu_download"

    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 221
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v7, "tt_bu_dislike"

    invoke-static {v6, v7}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 223
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const/high16 v7, 0x41700000    # 15.0f

    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v6

    float-to-int v6, v6

    .line 224
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-static {v7, v6, v6, v6, v6}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;IIII)V

    .line 225
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-direct {p0, v6}, Lcom/bytedance/sdk/openadsdk/core/d/a;->b(Landroid/view/View;)V

    .line 226
    new-instance v6, Lcom/bytedance/sdk/openadsdk/core/d/a$3;

    invoke-direct {v6, p0}, Lcom/bytedance/sdk/openadsdk/core/d/a$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 233
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aj()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :cond_0
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v6

    const/16 v7, 0x8

    const/4 v8, 0x0

    if-eqz v6, :cond_2

    .line 238
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getVideoView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 240
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 241
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    iget v10, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iget v11, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    mul-int/lit8 v11, v11, 0x9

    div-int/lit8 v11, v11, 0x10

    invoke-direct {v9, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v10, 0x11

    .line 242
    iput v10, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 243
    invoke-virtual {v0, v6, v8, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 245
    :cond_1
    invoke-static {v1, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 246
    invoke-static {v0, v8}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 249
    :cond_2
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/widget/ImageView;)V

    .line 250
    invoke-static {v1, v8}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 251
    invoke-static {v0, v7}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 255
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/g/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/bytedance/sdk/component/d/e;->a(Landroid/widget/ImageView;)Lcom/bytedance/sdk/component/d/d;

    .line 256
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    invoke-virtual {p0, p0, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 260
    invoke-virtual {p0, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 261
    invoke-virtual {p0, v5, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 262
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;)V

    return-void
.end method

.method private e()V
    .locals 10

    .line 266
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v2, "tt_backup_insert_layout3"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    .line 269
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v3, "tt_ad_container"

    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 270
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v4, "tt_bu_img"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 271
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_close"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    .line 272
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_desc"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    .line 273
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const-string v5, "tt_bu_dislike"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 275
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const/high16 v5, 0x41700000    # 15.0f

    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;F)F

    move-result v4

    float-to-int v4, v4

    .line 276
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-static {v5, v4, v4, v4, v4}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;IIII)V

    .line 277
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/bytedance/sdk/openadsdk/core/d/a;->b(Landroid/view/View;)V

    .line 278
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/d/a$4;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/d/a$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    .line 286
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getVideoView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 288
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 289
    iget v7, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->f:I

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->a:Landroid/content/Context;

    const/high16 v9, 0x41400000    # 12.0f

    invoke-static {v8, v9}, Lcom/bytedance/sdk/openadsdk/r/q;->d(Landroid/content/Context;F)I

    move-result v8

    sub-int/2addr v7, v8

    .line 290
    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    mul-int/lit8 v9, v7, 0x9

    div-int/lit8 v9, v9, 0x10

    invoke-direct {v8, v7, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v7, 0x11

    .line 291
    iput v7, v8, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 292
    invoke-virtual {v0, v4, v6, v8}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 294
    :cond_0
    invoke-static {v1, v5}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 295
    invoke-static {v0, v6}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 298
    :cond_1
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/widget/ImageView;)V

    .line 299
    invoke-static {v1, v6}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 300
    invoke-static {v0, v5}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;I)V

    .line 303
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    invoke-virtual {p0, p0, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 306
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->m:Landroid/view/View;

    invoke-virtual {p0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 307
    invoke-virtual {p0, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;Z)V

    .line 308
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Landroid/view/View;)V

    return-void
.end method

.method private f()V
    .locals 3

    .line 349
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 350
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    const-string v1, "#3E3E3E"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 351
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_titlebar_close_press_for_dark"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    const-string v1, "#FF333333"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 354
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    const-string v1, "#FF999999"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 355
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_dislike_icon"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void
.end method

.method private g()V
    .locals 3

    .line 360
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->t:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/i;->a:I

    const/4 v1, -0x1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 361
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 362
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 366
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 369
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 372
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->u:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_dislike_icon_inter_night"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method protected a(ILcom/bytedance/sdk/openadsdk/core/e/k;)V
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->a(ILcom/bytedance/sdk/openadsdk/core/e/k;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->q:Landroid/app/Dialog;

    return-void
.end method

.method a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V
    .locals 1

    const/4 v0, -0x1

    .line 80
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->setBackgroundColor(I)V

    .line 81
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 82
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 83
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->o:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    const-string p1, "interaction"

    .line 84
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->e:Ljava/lang/String;

    .line 85
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->h:I

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->b(I)V

    .line 86
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/a;->n:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 p3, -0x2

    invoke-direct {p2, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p0, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->b()V

    .line 89
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->A()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c(I)V

    return-void
.end method

.method public a_(I)V
    .locals 0

    .line 378
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/BackupView;->a_(I)V

    .line 379
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c(I)V

    return-void
.end method
