.class public Lcom/bytedance/sdk/openadsdk/core/video/c/d;
.super Ljava/lang/Object;
.source "SSMediaPlayerWrapper.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$a;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$b;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$d;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$e;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$f;
.implements Lcom/bytedance/sdk/openadsdk/core/video/c/c$g;


# static fields
.field private static s:Z = false

.field private static final u:Landroid/util/SparseIntArray;


# instance fields
.field private A:Z

.field private B:J

.field private C:J

.field private D:J

.field private E:J

.field private F:Z

.field private a:I

.field private b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:J

.field private final i:Landroid/os/Handler;

.field private j:Z

.field private k:J

.field private l:I

.field private m:J

.field private n:J

.field private o:Landroid/os/Handler;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:I

.field private t:Ljava/lang/String;

.field private v:Z

.field private w:Ljava/lang/Runnable;

.field private x:Z

.field private final y:Ljava/lang/Object;

.field private z:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 134
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    const/4 v0, -0x1

    .line 140
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;-><init>(Landroid/os/Handler;I)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;I)V
    .locals 5

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    .line 93
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a:I

    const/4 v0, 0x0

    .line 95
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    .line 96
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->c:Z

    .line 98
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    const/16 v1, 0xc9

    .line 102
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const-wide/16 v1, -0x1

    .line 103
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J

    .line 109
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    const-wide/16 v1, 0x0

    .line 110
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    .line 111
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    const-wide/high16 v3, -0x8000000000000000L

    .line 112
    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m:J

    .line 113
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    .line 128
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    const-string v3, "0"

    .line 132
    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->t:Ljava/lang/String;

    .line 236
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/video/c/d$1;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->w:Ljava/lang/Runnable;

    .line 1010
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->y:Ljava/lang/Object;

    .line 1011
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->z:Ljava/lang/StringBuilder;

    .line 1028
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->A:Z

    .line 1029
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    .line 1030
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    .line 1031
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D:J

    .line 1032
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->E:J

    .line 1035
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F:Z

    .line 146
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    .line 148
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    .line 149
    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "VideoManager"

    invoke-direct {p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 151
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    .line 152
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x11

    if-lt p1, v0, :cond_0

    const/4 p2, 0x1

    :cond_0
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F:Z

    .line 153
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->s()V

    return-void
.end method

.method private A()V
    .locals 1

    .line 942
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 946
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->y()V

    return-void

    .line 943
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->v()V

    return-void
.end method

.method private B()V
    .locals 1

    .line 950
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    :goto_0
    return-void
.end method

.method private C()I
    .locals 2

    .line 982
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    .line 983
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method private D()V
    .locals 2

    .line 992
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->s:Z

    if-eqz v0, :cond_0

    .line 993
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->r:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(IZ)V

    .line 994
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->s:Z

    :cond_0
    return-void
.end method

.method private E()V
    .locals 5

    .line 1038
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 1039
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    :cond_0
    return-void
.end method

.method private F()V
    .locals 8

    .line 1049
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1050
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    .line 1051
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/d;I)I
    .locals 0

    .line 39
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)Landroid/os/Handler;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    return-object p0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 1

    const/16 v0, 0x135

    if-ne p1, v0, :cond_0

    .line 742
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D()V

    .line 744
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 745
    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method private a(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    .line 970
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C()I

    move-result p2

    if-eq p2, p1, :cond_0

    const/4 v0, 0x1

    .line 972
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->s:Z

    .line 973
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->r:I

    .line 976
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p2

    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/media/AudioManager;

    if-eqz p2, :cond_1

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 978
    invoke-virtual {p2, v0, p1, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .line 915
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 916
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 703
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 704
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Ljava/io/FileDescriptor;)V

    .line 705
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    return-void
.end method

.method private a(II)Z
    .locals 2

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnError - Error code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " Extra code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SSMediaPlayeWrapper"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x3f2

    const/4 v1, 0x1

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ef

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-eq p1, v0, :cond_0

    const/16 v0, -0x6e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eq p2, v1, :cond_1

    const/16 v0, 0x2bc

    if-eq p2, v0, :cond_1

    const/16 v0, 0x320

    if-eq p2, v0, :cond_1

    move v1, p1

    :cond_1
    return v1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/d;Z)Z
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    return p1
.end method

.method private b(II)V
    .locals 6

    const/16 p2, 0x2bd

    if-ne p1, p2, :cond_0

    .line 852
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    .line 853
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D:J

    .line 854
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a:I

    goto :goto_0

    :cond_0
    const/16 p2, 0x2be

    const-wide/16 v0, 0x0

    if-ne p1, p2, :cond_2

    .line 856
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_1

    .line 857
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    .line 859
    :cond_1
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_3

    .line 860
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->E:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D:J

    sub-long/2addr v2, v4

    add-long/2addr p1, v2

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->E:J

    .line 861
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->D:J

    goto :goto_0

    .line 863
    :cond_2
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F:Z

    if-eqz p2, :cond_3

    const/4 p2, 0x3

    if-ne p1, p2, :cond_3

    .line 865
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_3

    .line 866
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    :cond_3
    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->s()V

    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 960
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->f:Z

    if-nez v0, :cond_1

    .line 961
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 963
    :cond_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .line 1014
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/16 v0, 0xc9

    .line 1015
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 1017
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->y:Ljava/lang/Object;

    monitor-enter p1

    .line 1018
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->z:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 1019
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->z:Ljava/lang/StringBuilder;

    .line 1021
    :cond_1
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)Lcom/bytedance/sdk/openadsdk/core/video/c/c;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    return-object p0
.end method

.method private r()V
    .locals 3

    const-wide/16 v0, 0x0

    .line 159
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    const/4 v2, 0x0

    .line 160
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    .line 161
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    .line 162
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    const-wide/high16 v0, -0x8000000000000000L

    .line 163
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m:J

    return-void
.end method

.method private s()V
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-nez v0, :cond_0

    const-string v0, "SSMediaPlayeWrapper"

    const-string v1, "SSMediaPlayerWrapper use System Mediaplayer"

    .line 173
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/video/c/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/b;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const-string v2, "0"

    .line 175
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->t:Ljava/lang/String;

    .line 176
    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$e;)V

    .line 177
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$b;)V

    .line 178
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$c;)V

    .line 179
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$a;)V

    .line 180
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$f;)V

    .line 181
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$d;)V

    .line 182
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$g;)V

    .line 184
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->c:Z

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    const-string v2, "setLooping error: "

    .line 186
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    const/4 v0, 0x0

    .line 188
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    :cond_0
    return-void
.end method

.method private t()V
    .locals 2

    const-string v0, "tag_video_play"

    const-string v1, "[video] MediaPlayerProxy#start first play prepare invoke !"

    .line 297
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private u()V
    .locals 3

    const-string v0, "SSMediaPlayeWrapper"

    .line 709
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-nez v1, :cond_0

    return-void

    .line 711
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    const-string v2, "releaseMediaplayer error1: "

    .line 713
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 715
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$b;)V

    .line 716
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$g;)V

    .line 717
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$a;)V

    .line 718
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$d;)V

    .line 719
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$c;)V

    .line 720
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$e;)V

    .line 721
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/c/c$f;)V

    .line 723
    :try_start_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    const-string v2, "releaseMediaplayer error2: "

    .line 725
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method private v()V
    .locals 3

    const-string v0, "SSMediaPlayeWrapper"

    .line 730
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "onDestory............"

    .line 732
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    const-string v2, "onDestroy error: "

    .line 735
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private w()V
    .locals 4

    .line 761
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 763
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0

    .line 765
    :cond_0
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseIntArray;->put(II)V

    :goto_0
    return-void
.end method

.method private x()V
    .locals 5

    .line 899
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F:Z

    if-nez v0, :cond_0

    .line 900
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 901
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    :cond_0
    return-void
.end method

.method private y()V
    .locals 2

    .line 922
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 925
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->e:Z

    .line 926
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 927
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 928
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 930
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 931
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->e:Z

    return-void
.end method

.method private z()V
    .locals 1

    .line 935
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 938
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->y()V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a()Landroid/media/MediaPlayer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 194
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eqz v0, :cond_0

    .line 195
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/c/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/b;->e()Landroid/media/MediaPlayer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 2

    .line 322
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    .line 323
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_0

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_1

    .line 325
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;J)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .line 337
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$5;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;Landroid/graphics/SurfaceTexture;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    .line 436
    iget v2, v0, Landroid/os/Message;->what:I

    .line 437
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[video]  execute , mCurrentState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " handlerMsg="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "tag_video_play"

    invoke-static {v4, v3}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const/4 v6, 0x1

    if-eqz v3, :cond_1a

    .line 439
    iget v3, v0, Landroid/os/Message;->what:I

    const/16 v8, 0xa

    const/4 v9, 0x2

    const-string v10, "[video] OP_RELEASE execute , releaseMediaplayer !"

    const/16 v7, 0x17

    const-string v15, "NativeVideoController"

    move-object/from16 v16, v15

    const-wide/16 v14, 0x0

    const/16 v11, 0xcf

    const/16 v5, 0xce

    const-string v12, "SSMediaPlayeWrapper"

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_8

    .line 605
    :pswitch_0
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    .line 606
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 607
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    new-instance v4, Landroid/view/Surface;

    invoke-direct {v4, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-interface {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Landroid/view/Surface;)V

    .line 609
    :cond_0
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0, v6}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Z)V

    .line 610
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v8}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Landroid/content/Context;I)V

    .line 611
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->z()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_8

    :catchall_0
    move-exception v0

    const-string v3, "OP_SET_SURFACE error: "

    .line 613
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 618
    :pswitch_1
    :try_start_1
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/SurfaceHolder;

    .line 619
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Landroid/view/SurfaceHolder;)V

    .line 620
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    if-ne v0, v9, :cond_1

    .line 621
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v8}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Landroid/content/Context;I)V

    .line 623
    :cond_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0, v6}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_8

    :catchall_1
    move-exception v0

    const-string v3, "OP_SET_DISPLAY error: "

    .line 626
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 655
    :pswitch_2
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    if-eq v0, v5, :cond_2

    if-ne v0, v11, :cond_3

    .line 657
    :cond_2
    :try_start_2
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->i()J

    move-result-wide v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v0

    const-string v3, "OP_REQUEST_CUR_POSITION error: "

    .line 659
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-wide v3, v14

    :goto_0
    cmp-long v0, v3, v14

    if-lez v0, :cond_1a

    const/16 v0, 0x6d

    .line 663
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v1, v0, v5}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ILjava/lang/Object;)V

    .line 664
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 665
    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m:J

    const-wide/high16 v10, -0x8000000000000000L

    cmp-long v0, v7, v10

    if-eqz v0, :cond_7

    cmp-long v0, v7, v3

    if-nez v0, :cond_5

    .line 667
    iget-boolean v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    if-nez v0, :cond_4

    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    const-wide/16 v9, 0x190

    cmp-long v0, v7, v9

    if-ltz v0, :cond_4

    .line 668
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    add-int/2addr v0, v6

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    .line 669
    iput-boolean v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    .line 672
    :cond_4
    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    const-wide/16 v9, 0xc8

    add-long/2addr v7, v9

    iput-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    goto :goto_1

    .line 674
    :cond_5
    iget-boolean v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    if-eqz v0, :cond_6

    .line 675
    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    iget-wide v10, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    add-long/2addr v7, v10

    iput-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const-string v5, "handleMsg:  bufferingDuration ="

    const/4 v10, 0x0

    aput-object v5, v0, v10

    .line 676
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v0, v6

    const-string v5, "  bufferingCount ="

    aput-object v5, v0, v9

    const/4 v5, 0x3

    iget v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v5

    invoke-static {v12, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    const/4 v5, 0x0

    .line 678
    iput-boolean v5, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    .line 679
    iput-wide v14, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    .line 682
    :cond_7
    :goto_1
    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m:J

    goto/16 :goto_8

    .line 644
    :pswitch_3
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    if-eq v0, v5, :cond_8

    if-ne v0, v11, :cond_9

    .line 646
    :cond_8
    :try_start_3
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->j()J

    move-result-wide v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_2

    :catchall_3
    move-exception v0

    const-string v3, "OP_REQUEST_DURATION error: "

    .line 648
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_9
    :goto_2
    const/16 v0, 0x6c

    .line 651
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ILjava/lang/Object;)V

    goto/16 :goto_8

    .line 546
    :pswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->r()V

    .line 547
    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v4, 0xc9

    if-eq v3, v4, :cond_a

    const/16 v4, 0xcb

    if-ne v3, v4, :cond_14

    .line 549
    :cond_a
    :try_start_4
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/o/f/b;

    .line 551
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 552
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/CacheDirConstants;->getFeedCacheDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c(Ljava/lang/String;)V

    .line 555
    :cond_b
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 556
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/bytedance/sdk/component/video/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 559
    :cond_c
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 562
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "try paly local:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 566
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Ljava/lang/String;)V

    :goto_3
    const/16 v0, 0xca

    goto/16 :goto_4

    .line 568
    :cond_d
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 571
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paly net:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/o/f/b;->b:I

    if-ne v3, v6, :cond_f

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v3, v7, :cond_f

    .line 574
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 576
    :cond_f
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v7, :cond_10

    .line 577
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    goto :goto_3

    .line 579
    :cond_10
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/f/a;->a()Lcom/bytedance/sdk/openadsdk/o/f/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/bytedance/sdk/openadsdk/o/f/a;->b(Lcom/bytedance/sdk/openadsdk/o/f/b;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "cache009"

    .line 580
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u4f7f\u7528Video\u7f13\u5b58-OP_SET_DATASOURCE-proxyurl="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_11

    .line 581
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v3, "file"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 582
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "cache010"

    .line 583
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u4f7f\u7528uri parse ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 587
    :cond_11
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 592
    :goto_4
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 593
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz v0, :cond_1a

    .line 594
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    const/16 v3, 0x13a

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto/16 :goto_8

    :catchall_4
    move-exception v0

    const-string v3, "OP_SET_DATASOURCE error: "

    .line 597
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 534
    :pswitch_5
    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    if-eq v3, v5, :cond_12

    if-eq v3, v11, :cond_12

    const/16 v4, 0xd1

    if-ne v3, v4, :cond_14

    .line 537
    :cond_12
    :try_start_5
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(J)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto/16 :goto_8

    :catchall_5
    move-exception v0

    const-string v3, "OP_SEEKTO error: "

    .line 539
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 630
    :pswitch_6
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v3, 0xcd

    if-eq v0, v3, :cond_13

    if-eq v0, v5, :cond_13

    const/16 v3, 0xd0

    if-eq v0, v3, :cond_13

    if-eq v0, v11, :cond_13

    const/16 v3, 0xd1

    if-ne v0, v3, :cond_14

    .line 633
    :cond_13
    :try_start_6
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->g()V

    const/16 v0, 0xd0

    .line 634
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    goto/16 :goto_8

    :catchall_6
    move-exception v0

    const-string v3, "OP_STOP error: "

    .line 636
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 491
    :pswitch_7
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v3, 0xca

    if-eq v0, v3, :cond_15

    const/16 v3, 0xd0

    if-ne v0, v3, :cond_14

    goto :goto_5

    :cond_14
    const/4 v5, 0x1

    goto/16 :goto_a

    .line 493
    :cond_15
    :goto_5
    :try_start_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/c/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/b;->e()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    const-string v0, "[video] OP_PREPARE_ASYNC execute , mMediaPlayer real prepareAsync !"

    .line 494
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz v0, :cond_1a

    .line 496
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    const/16 v5, 0x138

    invoke-virtual {v0, v5, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    goto/16 :goto_8

    :catchall_7
    move-exception v0

    const-string v3, "OP_PREPARE_ASYNC error: "

    move-object/from16 v7, v16

    .line 501
    invoke-static {v7, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    :pswitch_8
    move-object/from16 v7, v16

    .line 509
    :try_start_8
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u()V

    .line 510
    invoke-static {v4, v10}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    goto :goto_6

    :catchall_8
    move-exception v0

    const-string v3, "OP_RELEASE error: "

    .line 512
    invoke-static {v7, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_6
    const/4 v3, 0x0

    .line 514
    iput-boolean v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->f:Z

    const/16 v0, 0x135

    const/4 v3, 0x0

    .line 515
    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ILjava/lang/Object;)V

    const/16 v4, 0xcb

    .line 516
    iput v4, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 517
    iput-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    .line 521
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->A()V

    goto/16 :goto_8

    .line 526
    :pswitch_9
    :try_start_9
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->l()V

    .line 527
    invoke-static {v4, v10}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xc9

    .line 528
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    goto/16 :goto_8

    :catchall_9
    move-exception v0

    const-string v3, "OP_RESET error: "

    .line 530
    invoke-static {v12, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    :pswitch_a
    move-object/from16 v7, v16

    .line 469
    iget-boolean v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    if-eqz v0, :cond_16

    .line 470
    iget-wide v8, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    iget-wide v12, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    add-long/2addr v8, v12

    iput-wide v8, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    :cond_16
    const/4 v3, 0x0

    .line 472
    iput-boolean v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    .line 473
    iput-wide v14, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    const-wide/high16 v8, -0x8000000000000000L

    .line 474
    iput-wide v8, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m:J

    .line 476
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    if-eq v0, v5, :cond_17

    if-eq v0, v11, :cond_17

    const/16 v3, 0xd1

    if-ne v0, v3, :cond_14

    :cond_17
    :try_start_a
    const-string v0, "[video] OP_PAUSE execute , mMediaPlayer  OP_PAUSE !"

    .line 479
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->h()V

    .line 481
    iput v11, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_b

    const/4 v3, 0x0

    .line 482
    :try_start_b
    iput-boolean v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_a

    goto :goto_9

    :catchall_a
    move-exception v0

    goto :goto_7

    :catchall_b
    move-exception v0

    const/4 v3, 0x0

    :goto_7
    const-string v4, "OP_PAUSE error: "

    .line 484
    invoke-static {v7, v4, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    :pswitch_b
    const/4 v3, 0x0

    .line 441
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v7, 0xcd

    if-eq v0, v7, :cond_18

    if-eq v0, v5, :cond_18

    if-eq v0, v11, :cond_18

    const/16 v7, 0xd1

    if-ne v0, v7, :cond_14

    .line 444
    :cond_18
    :try_start_c
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->f()V

    const-string v0, "[video] OP_START execute , mMediaPlayer real start !"

    .line 445
    invoke-static {v4, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iput v5, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 448
    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J

    cmp-long v0, v7, v14

    if-ltz v0, :cond_19

    .line 449
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    iget-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J

    invoke-interface {v0, v7, v8}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(J)V

    const-wide/16 v7, -0x1

    .line 450
    iput-wide v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J

    .line 453
    :cond_19
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz v0, :cond_1b

    .line 454
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    const/16 v5, 0x138

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 455
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    const/16 v5, 0x139

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    goto :goto_9

    :catchall_c
    move-exception v0

    const-string v5, "OP_START error: "

    .line 459
    invoke-static {v4, v5, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    :cond_1a
    :goto_8
    const/4 v3, 0x0

    :cond_1b
    :goto_9
    const/4 v5, 0x0

    :goto_a
    if-eqz v5, :cond_1c

    const/16 v0, 0xc8

    .line 694
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 695
    iget-boolean v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    if-nez v0, :cond_1c

    const/16 v0, 0x134

    .line 696
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ILjava/lang/Object;)V

    .line 697
    iput-boolean v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    :cond_1c
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 357
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$6;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;Landroid/view/SurfaceHolder;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/c/c;)V
    .locals 1

    .line 771
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->c:Z

    if-nez p1, :cond_0

    const/16 p1, 0xd1

    goto :goto_0

    :cond_0
    const/16 p1, 0xce

    :goto_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 772
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 773
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x12e

    .line 774
    invoke-virtual {p1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    const-string p1, "completion"

    .line 777
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/String;)V

    .line 778
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/c/c;I)V
    .locals 1

    .line 751
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eq v0, p1, :cond_0

    return-void

    .line 754
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x12d

    .line 755
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/c/c;IIII)V
    .locals 0

    .line 1101
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/16 p4, 0x137

    .line 1102
    invoke-virtual {p1, p4, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/o/f/b;)V
    .locals 1

    .line 389
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$7;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 167
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->v:Z

    return-void
.end method

.method public a(ZJZ)V
    .locals 4

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[video] MediaPlayerProxy#start firstSeekToPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",firstPlay :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ",isPauseOtherMusicVolume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tag_video_play"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 202
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    if-nez p4, :cond_0

    .line 205
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eqz p4, :cond_1

    const/4 p4, 0x1

    .line 206
    invoke-virtual {p0, p4}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Z)V

    goto :goto_0

    .line 209
    :cond_0
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eqz p4, :cond_1

    .line 210
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Z)V

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    const-string p1, "[video] first start , SSMediaPlayer  start method !"

    .line 214
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->t()V

    .line 217
    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J

    goto :goto_3

    .line 219
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->E()V

    .line 220
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eqz p1, :cond_4

    .line 222
    :try_start_0
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->i()J

    move-result-wide v2

    cmp-long p1, p2, v2

    if-lez p1, :cond_3

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->i()J

    move-result-wide p2

    :goto_1
    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    .line 224
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "[video] MediaPlayerProxy#start  error: getCurrentPosition :"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_4
    :goto_2
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->v:Z

    if-eqz p1, :cond_5

    .line 228
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->w:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 230
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->w:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Ljava/lang/Runnable;)V

    :goto_3
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/c/c;II)Z
    .locals 2

    .line 783
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "what="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "extra="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SSMediaPlayeWrapper"

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->w()V

    const/16 p1, 0xc8

    .line 789
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 790
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/16 v0, 0x12f

    .line 791
    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 793
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x6c

    .line 794
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 795
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v0, 0x6d

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 797
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    const/4 v0, 0x1

    if-nez p1, :cond_2

    const/16 p1, 0x134

    .line 798
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ILjava/lang/Object;)V

    .line 799
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->d:Z

    .line 801
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(II)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 802
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->v()V

    :cond_3
    return v0
.end method

.method public b()V
    .locals 2

    .line 248
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    .line 249
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    .line 250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 251
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/c/c;)V
    .locals 2

    const/16 p1, 0xcd

    .line 873
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 875
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    if-eqz p1, :cond_0

    .line 876
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$8;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$8;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 889
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v0, 0x64

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 891
    :goto_0
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->u:Landroid/util/SparseIntArray;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q:I

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 892
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x131

    .line 893
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 895
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x()V

    return-void
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1001
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(FF)V

    goto :goto_0

    .line 1003
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/c;->a(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "SSMediaPlayeWrapper"

    const-string v1, "setQuietPlay error: "

    .line 1006
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/c/c;II)Z
    .locals 2

    .line 836
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "what,extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SSMediaPlayeWrapper"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_0

    return v1

    .line 840
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const/16 v0, 0x130

    .line 841
    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    const/16 p1, -0x3ec

    if-ne p3, p1, :cond_1

    .line 843
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    const/16 v0, 0x12f

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 846
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(II)V

    return v1
.end method

.method public c()V
    .locals 3

    const/16 v0, 0xcb

    .line 268
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    .line 269
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    .line 273
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B()V

    .line 274
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "release"

    .line 276
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 278
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b:Lcom/bytedance/sdk/openadsdk/core/video/c/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 279
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->f:Z

    .line 280
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 283
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->v()V

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "release error: "

    .line 284
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/video/c/c;)V
    .locals 1

    .line 909
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/16 v0, 0x132

    .line 910
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .line 311
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/d$3;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/d;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public e()V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/16 v1, 0x6c

    .line 370
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .line 375
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/16 v1, 0x6d

    .line 376
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public g()Z
    .locals 2

    .line 405
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h()Z
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public i()Z
    .locals 2

    .line 413
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->x:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j()Z
    .locals 2

    .line 417
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()Z
    .locals 2

    .line 421
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xcd

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Z
    .locals 2

    .line 425
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g:I

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 1044
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    .line 1045
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    return-void
.end method

.method public n()J
    .locals 5

    .line 1059
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 1060
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->j:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1061
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    add-long/2addr v2, v0

    return-wide v2

    .line 1064
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->k:J

    return-wide v0

    .line 1066
    :cond_1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->E:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .line 1070
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->F()V

    .line 1071
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    return-wide v0
.end method

.method public p()J
    .locals 6

    .line 1075
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1076
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    .line 1077
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->C:J

    .line 1079
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->B:J

    return-wide v0
.end method

.method public q()I
    .locals 2

    .line 1084
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 1085
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l:I

    return v0

    .line 1087
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a:I

    return v0
.end method
