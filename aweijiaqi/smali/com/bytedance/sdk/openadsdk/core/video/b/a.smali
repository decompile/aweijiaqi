.class public abstract Lcom/bytedance/sdk/openadsdk/core/video/b/a;
.super Ljava/lang/Object;
.source "BaseVideoController.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;


# instance fields
.field private A:J

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;",
            ">;"
        }
    .end annotation
.end field

.field private F:I

.field private G:Z

.field private H:Z

.field private final I:Ljava/lang/Runnable;

.field private final J:Ljava/lang/Runnable;

.field private final K:Ljava/lang/Runnable;

.field private L:Z

.field private M:J

.field private final N:Landroid/content/BroadcastReceiver;

.field private O:I

.field private P:Z

.field protected a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field public d:J

.field protected e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected f:J

.field protected g:J

.field protected h:Z

.field protected i:Z

.field protected j:Lcom/bytedance/sdk/openadsdk/o/f/b;

.field public k:J

.field private final l:Landroid/view/ViewGroup;

.field private final m:Lcom/bytedance/sdk/component/utils/u;

.field private n:J

.field private o:J

.field private p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

.field private q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

.field private r:J

.field private s:J

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private u:Z

.field private final v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 5

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v0, 0x0

    .line 83
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->n:J

    .line 84
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->o:J

    .line 87
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 88
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    const/4 v2, 0x0

    .line 92
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    .line 93
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    const/4 v3, 0x1

    .line 94
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->y:Z

    .line 95
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    .line 97
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->A:J

    .line 99
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B:Z

    .line 100
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->C:Z

    .line 101
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D:Z

    const/4 v4, 0x0

    .line 107
    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->e:Ljava/util/Map;

    .line 110
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f:J

    .line 111
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->g:J

    .line 113
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->h:Z

    .line 114
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i:Z

    .line 115
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G:Z

    .line 117
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->H:Z

    .line 356
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->I:Ljava/lang/Runnable;

    .line 365
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a$3;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    .line 396
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a$4;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->K:Ljava/lang/Runnable;

    .line 1279
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    .line 1346
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a$5;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N:Landroid/content/BroadcastReceiver;

    .line 1358
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->O:I

    .line 1377
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->P:Z

    .line 135
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->O:I

    .line 136
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    .line 137
    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    .line 138
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 139
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(Landroid/content/Context;)V

    .line 140
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F:I

    .line 141
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x11

    if-lt p1, p2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->v:Z

    return-void
.end method

.method private C()V
    .locals 5

    .line 377
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 381
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 379
    :cond_2
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->J()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 383
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 384
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/component/utils/u;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private D()V
    .locals 4

    .line 388
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->E()V

    .line 389
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->K:Ljava/lang/Runnable;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/component/utils/u;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private E()V
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private F()Z
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private G()V
    .locals 2

    .line 468
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 471
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 472
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 473
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_2
    :goto_1
    return-void
.end method

.method private H()Z
    .locals 3

    .line 703
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 706
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aJ()F

    move-result v0

    const/high16 v2, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private I()V
    .locals 7

    .line 715
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    goto :goto_1

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    .line 719
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    .line 720
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    .line 721
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v3

    int-to-float v3, v3

    .line 722
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v1

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v5, v1, v4

    div-float v5, v3, v5

    int-to-float v2, v2

    mul-float v6, v2, v4

    div-float v6, v0, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_1

    mul-float v0, v0, v4

    div-float v0, v2, v0

    mul-float v1, v3, v0

    goto :goto_0

    :cond_1
    mul-float v3, v3, v4

    div-float v2, v1, v3

    mul-float v2, v2, v0

    .line 733
    :goto_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v1, v1

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 734
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 736
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    instance-of v1, v1, Landroid/view/TextureView;

    if-eqz v1, :cond_2

    .line 737
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    check-cast v1, Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 738
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    instance-of v1, v1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_4

    .line 739
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_3
    :goto_1
    return-void

    :catchall_0
    move-exception v0

    const-string v1, "changeVideoSize"

    const-string v2, "changeVideoSizeSupportInteraction error"

    .line 742
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    :goto_2
    return-void
.end method

.method private J()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 747
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_1

    .line 748
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_1

    .line 749
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->P()Lcom/bytedance/sdk/openadsdk/core/e/m$a;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->k()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private K()V
    .locals 11

    .line 757
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    .line 758
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 761
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;)[I

    move-result-object v0

    .line 762
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v1

    .line 764
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aI()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 765
    :goto_0
    aget v3, v0, v3

    int-to-float v6, v3

    .line 766
    aget v0, v0, v4

    int-to-float v7, v0

    .line 767
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    int-to-float v8, v0

    .line 768
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    int-to-float v9, v0

    if-eqz v2, :cond_3

    cmpl-float v0, v8, v9

    if-lez v0, :cond_2

    const/4 v10, 0x1

    move-object v5, p0

    .line 772
    invoke-direct/range {v5 .. v10}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(FFFFZ)V

    return-void

    :cond_2
    mul-float v9, v9, v6

    div-float v7, v9, v8

    goto :goto_1

    :cond_3
    cmpg-float v0, v8, v9

    if-gez v0, :cond_4

    const/4 v10, 0x0

    move-object v5, p0

    .line 780
    invoke-direct/range {v5 .. v10}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(FFFFZ)V

    return-void

    :cond_4
    mul-float v8, v8, v7

    div-float v6, v8, v9

    .line 787
    :goto_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v1, v6

    float-to-int v2, v7

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 788
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 790
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    if-eqz v1, :cond_7

    .line 791
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    instance-of v1, v1, Landroid/view/TextureView;

    if-eqz v1, :cond_5

    .line 792
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    check-cast v1, Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 793
    :cond_5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    instance-of v1, v1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_7

    .line 794
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :cond_6
    :goto_2
    return-void

    :catchall_0
    move-exception v0

    const-string v1, "changeVideoSize"

    const-string v2, "changeVideoSizeByWidth error"

    .line 798
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    :goto_3
    return-void
.end method

.method private L()V
    .locals 15

    const-string v0, ",videoWidth="

    const-string v1, "changeVideoSize"

    .line 807
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeVideoSize start.......mMaterialMeta.getAdSlot()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->m()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J()Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    const-string v2, "changeVideoSize start check condition complete ... go .."

    .line 811
    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;)[I

    move-result-object v2

    .line 814
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v3

    .line 816
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aI()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v4, v6, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 818
    :goto_0
    aget v7, v2, v5

    int-to-float v9, v7

    .line 819
    aget v2, v2, v6

    int-to-float v10, v2

    .line 820
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    int-to-float v11, v2

    .line 821
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    int-to-float v12, v2

    if-eqz v4, :cond_2

    cmpl-float v2, v11, v12

    if-lez v2, :cond_3

    const-string v0, "\u6a2a\u8f6c\u7ad6\u5c4f\u5355\u72ec\u9002\u914d....."

    .line 826
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v13, 0x1

    move-object v8, p0

    .line 827
    invoke-direct/range {v8 .. v13}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(FFFFZ)V

    return-void

    :cond_2
    cmpg-float v2, v11, v12

    if-gez v2, :cond_3

    const-string v0, "\u7ad6\u5c4f\u8f6c\u6a2a\u5355\u72ec\u9002\u914d....."

    .line 833
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v13, 0x0

    move-object v8, p0

    .line 834
    invoke-direct/range {v8 .. v13}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(FFFFZ)V

    return-void

    :cond_3
    div-float v2, v11, v12

    div-float v3, v9, v10

    .line 842
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "screenHeight="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, ",screenWidth="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "videoHeight="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\u89c6\u9891\u5bbd\u9ad8\u6bd4,videoScale="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, ",\u5c4f\u5e55\u5bbd\u9ad8\u6bd4.screenScale="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, ",VERTICAL_SCALE(9:16)="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/high16 v8, 0x3f100000    # 0.5625f

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v13, ",HORIZONTAL_SCALE(16:9) ="

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v13, 0x3fe38e39

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v14, 0x41100000    # 9.0f

    if-eqz v4, :cond_4

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    cmpl-float v2, v2, v8

    if-nez v2, :cond_5

    mul-float v14, v14, v10

    div-float v11, v14, v7

    move v12, v10

    goto :goto_1

    :cond_4
    cmpl-float v3, v3, v13

    if-lez v3, :cond_5

    cmpl-float v2, v2, v13

    if-nez v2, :cond_5

    mul-float v14, v14, v9

    div-float v12, v14, v7

    move v11, v9

    :goto_1
    const/4 v5, 0x1

    .line 864
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u9002\u914d\u540e\u5bbd\u9ad8\uff1avideoHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v5, :cond_6

    .line 870
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " \u5c4f\u5e55\u6bd4\u4f8b\u548c\u89c6\u9891\u6bd4\u4f8b\u76f8\u540c\uff0c\u4ee5\u53ca\u5176\u4ed6\u60c5\u51b5\u90fd\u6309\u7167\u5c4f\u5e55\u5bbd\u9ad8\u64ad\u653e\uff0cvideoHeight="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "\uff0cvideoWidth="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v9, v11

    move v10, v12

    .line 874
    :goto_2
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v2, v9

    float-to-int v3, v10

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0xd

    .line 875
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 877
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 878
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v4

    instance-of v4, v4, Landroid/view/TextureView;

    if-eqz v4, :cond_7

    .line 879
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v4

    check-cast v4, Landroid/view/TextureView;

    invoke-virtual {v4, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 880
    :cond_7
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v4

    instance-of v4, v4, Landroid/view/SurfaceView;

    if-eqz v4, :cond_8

    .line 881
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    invoke-virtual {v4, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 886
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 888
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 889
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 890
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_9
    const-string v0, "changeVideoSize .... complete ... end !!!"

    .line 893
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    const-string v2, "changeSize error"

    .line 895
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_4
    return-void
.end method

.method private M()V
    .locals 10

    const-string v0, "changeVideoSize"

    .line 904
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    .line 905
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v1, :cond_0

    goto :goto_1

    .line 908
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aI()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const/4 v9, 0x1

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    .line 909
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/q;->b(Landroid/content/Context;)[I

    move-result-object v1

    .line 910
    aget v2, v1, v2

    int-to-float v5, v2

    .line 911
    aget v1, v1, v3

    int-to-float v6, v1

    .line 913
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a()Landroid/media/MediaPlayer;

    move-result-object v1

    .line 914
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    int-to-float v7, v2

    .line 915
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    int-to-float v8, v1

    move-object v4, p0

    .line 916
    invoke-direct/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(FFFFZ)V

    const-string v1, "changeSize=end"

    .line 917
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_2
    :goto_1
    return-void

    :catchall_0
    move-exception v1

    const-string v2, "changeSize error"

    .line 919
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-void
.end method

.method private N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;
    .locals 1

    .line 1200
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_0

    .line 1202
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->o()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private O()V
    .locals 2

    .line 1297
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 1298
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c(I)V

    .line 1299
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0, v1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(ZZ)V

    .line 1300
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(Z)V

    .line 1301
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b()V

    .line 1302
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->d()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;J)J
    .locals 0

    .line 60
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->n:J

    return-wide p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)Lcom/bytedance/sdk/openadsdk/core/video/c/d;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    return-object p0
.end method

.method private a(FFFFZ)V
    .locals 3

    const-string v0, "changeVideoSize"

    .line 934
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ",screenHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videoHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ",videoWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    cmpg-float v2, p3, v1

    if-lez v2, :cond_0

    cmpg-float v2, p4, v1

    if-gtz v2, :cond_1

    .line 939
    :cond_0
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/x;->c()I

    move-result p3

    int-to-float p3, p3

    .line 940
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p4

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/core/e/x;->b()I

    move-result p4

    int-to-float p4, p4

    :cond_1
    cmpg-float v2, p4, v1

    if-lez v2, :cond_7

    cmpg-float v1, p3, v1

    if-gtz v1, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0xd

    if-eqz p5, :cond_4

    cmpg-float p2, p3, p4

    if-gez p2, :cond_3

    return-void

    :cond_3
    const-string p2, "\u7ad6\u5c4f\u6a21\u5f0f\u4e0b\u6309\u89c6\u9891\u5bbd\u5ea6\u8ba1\u7b97\u653e\u5927\u500d\u6570\u503c"

    .line 953
    invoke-static {v0, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    mul-float p4, p4, p1

    div-float/2addr p4, p3

    .line 958
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int p1, p1

    float-to-int p3, p4

    invoke-direct {p2, p1, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 959
    invoke-virtual {p2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0

    :cond_4
    cmpl-float p1, p3, p4

    if-lez p1, :cond_5

    return-void

    :cond_5
    const-string p1, "\u6a2a\u5c4f\u6a21\u5f0f\u4e0b\u6309\u89c6\u9891\u9ad8\u5ea6\u8ba1\u7b97\u653e\u5927\u500d\u6570\u503c"

    .line 965
    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    mul-float p3, p3, p2

    div-float/2addr p3, p4

    .line 970
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int p3, p3

    float-to-int p2, p2

    invoke-direct {p1, p3, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 971
    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    move-object p2, p1

    .line 974
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 975
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p1

    instance-of p1, p1, Landroid/view/TextureView;

    if-eqz p1, :cond_6

    .line 976
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p1

    check-cast p1, Landroid/view/TextureView;

    invoke-virtual {p1, p2}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 977
    :cond_6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p1

    instance-of p1, p1, Landroid/view/SurfaceView;

    if-eqz p1, :cond_8

    .line 978
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->N()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p1

    check-cast p1, Landroid/view/SurfaceView;

    invoke-virtual {p1, p2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_7
    :goto_1
    return-void

    :catchall_0
    move-exception p1

    const-string p2, "changeSize error"

    .line 982
    invoke-static {v0, p2, p1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_8
    :goto_2
    return-void
.end method

.method private a(JJ)V
    .locals 2

    .line 1051
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 1052
    iput-wide p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    .line 1053
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(JJ)V

    .line 1054
    invoke-static {p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/d/a;->a(JJ)I

    move-result v0

    .line 1055
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(I)V

    .line 1057
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz v0, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string p2, "BaseVideoController"

    const-string p3, "onProgressUpdate error: "

    .line 1061
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private a(JZ)V
    .locals 1

    .line 1283
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 1287
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->O()V

    .line 1289
    :cond_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(J)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 8

    .line 126
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v5

    .line 127
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 128
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 129
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v3, "tt_video_play_layout_for_live"

    .line 130
    invoke-static {p1, v3}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v4, 0x1

    move-object v1, v0

    move-object v7, p0

    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;-><init>(Landroid/content/Context;Landroid/view/View;ZLjava/util/EnumSet;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    .line 131
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/a;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;Landroid/content/Context;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->u:Z

    if-eqz v0, :cond_1

    .line 454
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 456
    :cond_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)J
    .locals 2

    .line 60
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    return-wide v0
.end method

.method private b(I)V
    .locals 6

    .line 416
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 419
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-nez p1, :cond_1

    return-void

    .line 422
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 423
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->w()V

    .line 424
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->n:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->o:J

    .line 425
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz p1, :cond_2

    .line 426
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    invoke-static {v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/d/a;->a(JJ)I

    move-result v2

    invoke-interface {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JI)V

    .line 430
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    .line 431
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/ref/WeakReference;Z)V

    .line 434
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    if-nez p1, :cond_4

    .line 435
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b()V

    .line 436
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    .line 437
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    invoke-direct {p0, v1, v2, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(JJ)V

    .line 438
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    .line 440
    :cond_4
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D:Z

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    .line 1361
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1364
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result p1

    .line 1366
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->O:I

    if-ne v0, p1, :cond_1

    return-void

    .line 1370
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->C:Z

    if-nez v0, :cond_2

    const/4 v0, 0x2

    .line 1371
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d(I)Z

    .line 1374
    :cond_2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->O:I

    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/o/f/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 317
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->j:Lcom/bytedance/sdk/openadsdk/o/f/b;

    .line 318
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_3

    .line 320
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_2

    .line 321
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 323
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b(Ljava/lang/String;)V

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->f(Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x1

    .line 327
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d(I)V

    .line 328
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;)V

    .line 330
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->n:J

    .line 331
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 332
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->d(I)V

    .line 333
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->d(I)V

    .line 334
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/video/b/a$1;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(Ljava/lang/Runnable;)V

    :cond_4
    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    .line 461
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private c(I)Z
    .locals 1

    .line 1293
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(I)Z

    move-result p1

    return p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    return p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)Lcom/bytedance/sdk/component/utils/u;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    return-object p0
.end method

.method private d(I)Z
    .locals 4

    .line 1326
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-eq v0, v2, :cond_0

    if-eqz v0, :cond_0

    .line 1328
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i()V

    .line 1329
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B:Z

    .line 1330
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->C:Z

    .line 1331
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_1

    .line 1332
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(ILcom/bytedance/sdk/openadsdk/core/e/x;Z)Z

    move-result p1

    return p1

    :cond_0
    if-ne v0, v2, :cond_1

    .line 1335
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B:Z

    .line 1336
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_1

    .line 1337
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->q()V

    :cond_1
    return v3
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)Ljava/lang/Runnable;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->I:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D()V

    return-void
.end method

.method private f(Z)V
    .locals 0

    .line 1177
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-object p0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .line 1254
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->l()Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 1

    .line 1276
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected abstract a()I
.end method

.method public a(I)V
    .locals 3

    .line 1104
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 1109
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 1110
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_3

    return-void

    .line 1113
    :cond_3
    check-cast v1, Landroid/app/Activity;

    .line 1116
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    nop

    :goto_2
    const/16 p1, 0x400

    if-nez v0, :cond_4

    .line 1120
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1, p1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_3

    .line 1122
    :cond_4
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->clearFlags(I)V

    :goto_3
    return-void
.end method

.method protected abstract a(II)V
.end method

.method public a(J)V
    .locals 3

    .line 209
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 210
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 6

    .line 594
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_10

    if-eqz p1, :cond_10

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_10

    .line 595
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 598
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_f

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_d

    const/4 v1, 0x0

    const/16 v2, 0x134

    if-eq v0, v2, :cond_c

    const/16 v2, 0x137

    if-eq v0, v2, :cond_8

    const/16 v2, 0x13a

    if-eq v0, v2, :cond_7

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 677
    :pswitch_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 678
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_10

    .line 679
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->w()V

    goto/16 :goto_2

    .line 639
    :pswitch_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    if-eqz p1, :cond_1

    .line 640
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 642
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->v:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    if-nez p1, :cond_2

    .line 643
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f:J

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->g:J

    .line 644
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->g()V

    .line 645
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    .line 647
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_10

    .line 648
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->w()V

    goto/16 :goto_2

    .line 619
    :pswitch_2
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 620
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    const/4 v3, 0x3

    if-eqz v0, :cond_5

    if-eq p1, v3, :cond_4

    const/16 v4, 0x2be

    if-ne p1, v4, :cond_3

    goto :goto_0

    :cond_3
    const/16 v1, 0x2bd

    if-ne p1, v1, :cond_5

    .line 626
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->t()V

    .line 627
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->C()V

    .line 628
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G:Z

    goto :goto_1

    .line 622
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->w()V

    .line 623
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 624
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G:Z

    .line 631
    :cond_5
    :goto_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->v:Z

    if-eqz v0, :cond_10

    if-ne p1, v3, :cond_10

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    if-nez p1, :cond_10

    .line 632
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f:J

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->g:J

    .line 633
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->e()V

    .line 634
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    .line 635
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i:Z

    goto/16 :goto_2

    .line 664
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 665
    iget p1, p1, Landroid/os/Message;->arg2:I

    .line 666
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(II)V

    .line 668
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 669
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_6

    .line 670
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->w()V

    .line 672
    :cond_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz p1, :cond_10

    .line 673
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->o:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    invoke-static {v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/d/a;->a(JJ)I

    move-result v2

    invoke-interface {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->b(JI)V

    goto/16 :goto_2

    .line 609
    :pswitch_4
    iget p1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b(I)V

    goto/16 :goto_2

    .line 605
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f:J

    goto/16 :goto_2

    .line 686
    :cond_8
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->H()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 687
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->i()I

    move-result p1

    if-nez p1, :cond_9

    .line 688
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->M()V

    goto :goto_2

    .line 689
    :cond_9
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->i()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_a

    .line 690
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->K()V

    goto :goto_2

    .line 692
    :cond_a
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L()V

    goto :goto_2

    .line 695
    :cond_b
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->I()V

    goto :goto_2

    .line 660
    :cond_c
    invoke-virtual {p0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(II)V

    goto :goto_2

    .line 652
    :cond_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 653
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 654
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    cmp-long p1, v2, v0

    if-lez p1, :cond_e

    move-wide v0, v2

    :cond_e
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    .line 655
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(JJ)V

    goto :goto_2

    .line 612
    :cond_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 613
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_10

    .line 614
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    :cond_10
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x12e
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 2

    .line 1016
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez p1, :cond_0

    return-void

    .line 1019
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D()V

    .line 1020
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->M:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c(I)Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(JZ)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;IZ)V
    .locals 4

    .line 1035
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1038
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    int-to-long p2, p2

    .line 1039
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    mul-long p2, p2, v0

    long-to-float p2, p2

    const/high16 p3, 0x3f800000    # 1.0f

    mul-float p2, p2, p3

    const-string p3, "tt_video_progress_max"

    invoke-static {p1, p3}, Lcom/bytedance/sdk/component/utils/r;->l(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    float-to-long p1, p2

    .line 1040
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    const-wide/16 v2, 0x0

    cmp-long p3, v0, v2

    if-lez p3, :cond_1

    long-to-int p2, p1

    int-to-long p1, p2

    .line 1041
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->M:J

    goto :goto_0

    .line 1043
    :cond_1
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->M:J

    .line 1045
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_2

    .line 1046
    iget-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->M:J

    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(J)V

    :cond_2
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 1

    const/4 p1, 0x1

    .line 1224
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->u:Z

    .line 1226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    .line 1230
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Z)V

    .line 1232
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Landroid/graphics/SurfaceTexture;)V

    .line 1233
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 p1, 0x1

    .line 1188
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->u:Z

    .line 1189
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    .line 1193
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Z)V

    .line 1195
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Landroid/view/SurfaceHolder;)V

    .line 1196
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 2

    .line 989
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 992
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g()Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 993
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i()V

    .line 994
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(ZZ)V

    .line 995
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c()V

    goto :goto_0

    .line 997
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->i()Z

    move-result p1

    if-nez p1, :cond_3

    .line 998
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_2

    .line 999
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c(Landroid/view/ViewGroup;)V

    .line 1001
    :cond_2
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d(J)V

    .line 1002
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_4

    .line 1003
    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(ZZ)V

    goto :goto_0

    .line 1006
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->k()V

    .line 1007
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_4

    .line 1008
    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(ZZ)V

    :cond_4
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 1

    .line 1138
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->y:Z

    if-eqz p1, :cond_0

    .line 1139
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i()V

    :cond_0
    if-eqz p3, :cond_1

    .line 1141
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->y:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->A()Z

    move-result p1

    if-nez p1, :cond_1

    .line 1142
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B()Z

    move-result p2

    const/4 p3, 0x1

    xor-int/2addr p2, p3

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(ZZ)V

    .line 1143
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1, p4, p3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(ZZZ)V

    .line 1145
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->g()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1146
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c()V

    .line 1147
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b()V

    goto :goto_0

    .line 1149
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c()V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V
    .locals 0

    .line 480
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;)V
    .locals 1

    .line 411
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->E:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/widget/i$a;Ljava/lang/String;)V
    .locals 2

    .line 1309
    sget-object p2, Lcom/bytedance/sdk/openadsdk/core/video/b/a$6;->a:[I

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/i$a;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 v0, 0x2

    const/4 v1, 0x3

    if-eq p1, v0, :cond_1

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 1317
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->k()V

    const/4 p1, 0x0

    .line 1318
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B:Z

    .line 1319
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->C:Z

    goto :goto_0

    .line 1314
    :cond_1
    invoke-virtual {p0, p2, v1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(ZI)V

    goto :goto_0

    .line 1311
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->i()V

    :goto_0
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 309
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->e:Ljava/util/Map;

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 215
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->y:Z

    .line 216
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c(Z)V

    return-void
.end method

.method public a(ZI)V
    .locals 0

    .line 584
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/o/f/b;)Z
    .locals 9

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->j:Lcom/bytedance/sdk/openadsdk/o/f/b;

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "video local url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BaseVideoController"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "No video info"

    .line 152
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    .line 155
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f()V

    .line 156
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->h:Z

    .line 157
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->h()Z

    move-result v1

    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    .line 158
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->g()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-lez v1, :cond_3

    .line 159
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->g()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 160
    iget-wide v7, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    cmp-long v1, v7, v3

    if-lez v1, :cond_2

    move-wide v3, v7

    :cond_2
    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    .line 162
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v1, :cond_4

    .line 163
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->e()V

    .line 165
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->d()V

    .line 166
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->e()I

    move-result v3

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->f()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c(II)V

    .line 167
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c(Landroid/view/ViewGroup;)V

    .line 171
    :cond_4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v1, :cond_5

    .line 172
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    invoke-direct {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;-><init>(Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    .line 175
    :cond_5
    iput-wide v5, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->o:J

    .line 177
    :try_start_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b(Lcom/bytedance/sdk/openadsdk/o/f/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    return v0
.end method

.method protected abstract b()V
.end method

.method public b(J)V
    .locals 0

    .line 234
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->A:J

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 0

    .line 1025
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p1, :cond_0

    .line 1026
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->E()V

    .line 1028
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_1

    .line 1029
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->c()V

    :cond_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    const/4 p1, 0x0

    .line 1238
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->u:Z

    .line 1239
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p2, :cond_0

    .line 1240
    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 0

    const/4 p1, 0x0

    .line 1215
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->u:Z

    .line 1216
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p2, :cond_0

    .line 1217
    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1067
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 0

    .line 1072
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->F()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1075
    :cond_0
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    const/4 p2, 0x1

    xor-int/2addr p1, p2

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f(Z)V

    .line 1076
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_1

    const-string p1, "BaseVideoController"

    const-string p2, "context is not activity, not support this function."

    .line 1077
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1080
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    const/4 p4, 0x0

    if-eqz p1, :cond_3

    if-eqz p3, :cond_2

    const/16 p1, 0x8

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 1081
    :goto_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(I)V

    .line 1083
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_4

    .line 1084
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->a(Landroid/view/ViewGroup;)V

    .line 1085
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(Z)V

    goto :goto_1

    .line 1088
    :cond_3
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(I)V

    .line 1090
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_4

    .line 1091
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(Landroid/view/ViewGroup;)V

    .line 1092
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(Z)V

    .line 1096
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->E:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_6

    .line 1098
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(Z)V

    :cond_6
    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 260
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b(Z)V

    :cond_0
    return-void
.end method

.method protected abstract c()V
.end method

.method public c(J)V
    .locals 0

    .line 242
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    .line 1129
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_0

    .line 1130
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->g()V

    :cond_0
    const/4 p1, 0x1

    const/4 p2, 0x3

    .line 1132
    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(ZI)V

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 278
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D:Z

    return-void
.end method

.method protected abstract d()V
.end method

.method public d(J)V
    .locals 3

    .line 540
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    .line 541
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    .line 542
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_1

    .line 543
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->e()V

    .line 545
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz p1, :cond_2

    .line 546
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    const/4 v2, 0x1

    xor-int/2addr p2, v2

    invoke-virtual {p1, v2, v0, v1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ZJZ)V

    .line 547
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D()V

    :cond_2
    return-void
.end method

.method public d(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    .line 1155
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->L:Z

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 1156
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->f(Z)V

    .line 1157
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz p1, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b(Landroid/view/ViewGroup;)V

    .line 1160
    :cond_0
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(I)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x3

    .line 1162
    invoke-virtual {p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(ZI)V

    :goto_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    return-void
.end method

.method protected abstract e()V
.end method

.method public e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1168
    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 298
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->H:Z

    return-void
.end method

.method protected abstract f()V
.end method

.method public f(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected abstract g()V
.end method

.method public h()V
    .locals 2

    .line 1260
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_0

    .line 1261
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->b()V

    .line 1262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->e()V

    .line 1265
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_1

    .line 1266
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->u()V

    :cond_1
    const-wide/16 v0, -0x1

    .line 1268
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d(J)V

    .line 1269
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_2

    .line 1270
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->m()V

    :cond_2
    return-void
.end method

.method public i()V
    .locals 2

    .line 491
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->k:J

    .line 492
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b()V

    .line 495
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    if-eqz v0, :cond_1

    .line 496
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c()V

    :cond_1
    return-void
.end method

.method public j()V
    .locals 1

    .line 506
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->b()V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 5

    .line 513
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->e()V

    .line 515
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->q()V

    .line 516
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->u()V

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 520
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->z:Z

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->a(ZJZ)V

    .line 521
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D()V

    .line 524
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    if-eqz v0, :cond_2

    .line 525
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d()V

    :cond_2
    return-void
.end method

.method public l()V
    .locals 3

    .line 556
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 557
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->c()V

    .line 558
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    if-eqz v0, :cond_1

    .line 562
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->g()V

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    if-eqz v0, :cond_2

    .line 565
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 566
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 567
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->m:Lcom/bytedance/sdk/component/utils/u;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 568
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->E()V

    .line 573
    :cond_2
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->q:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-void
.end method

.method public m()V
    .locals 0

    .line 589
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->l()V

    return-void
.end method

.method public n()J
    .locals 2

    .line 204
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->r:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .line 247
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->v()Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->v()Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->n()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public p()I
    .locals 1

    .line 1450
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->q()I

    move-result v0

    :goto_0
    return v0
.end method

.method public q()J
    .locals 4

    .line 252
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->o()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->A:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public r()J
    .locals 4

    .line 268
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/d;->p()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->A:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public s()I
    .locals 4

    .line 304
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->s:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/a;->a(JJ)I

    move-result v0

    return v0
.end method

.method public t()J
    .locals 2

    .line 238
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:J

    return-wide v0
.end method

.method public u()Z
    .locals 1

    .line 230
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->B:Z

    return v0
.end method

.method public v()Lcom/bytedance/sdk/openadsdk/core/video/c/d;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/c/d;

    return-object v0
.end method

.method public w()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    return-object v0
.end method

.method public x()Z
    .locals 1

    .line 273
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->D:Z

    return v0
.end method

.method public y()Z
    .locals 1

    .line 293
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->G:Z

    return v0
.end method

.method public z()V
    .locals 1

    .line 533
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->w:Z

    if-eqz v0, :cond_0

    .line 534
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d()V

    :cond_0
    return-void
.end method
