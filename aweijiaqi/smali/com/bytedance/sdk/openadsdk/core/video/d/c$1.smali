.class final Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;
.super Ljava/lang/Object;
.source "VideoPreloadUtils.java"

# interfaces
.implements Lcom/bytedance/sdk/component/video/a/c/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/component/video/a/c/a$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/component/video/a/c/a$a;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/o/f/b;

.field final synthetic e:J


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/component/video/a/c/a$a;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/o/f/b;J)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->a:Lcom/bytedance/sdk/component/video/a/c/a$a;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    iput-wide p5, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->e:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 8

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->a:Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v0, :cond_0

    .line 74
    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a$a;->a(Lcom/bytedance/sdk/component/video/b/a;I)V

    .line 77
    :cond_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz p2, :cond_2

    .line 78
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 79
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v4

    .line 81
    new-instance v5, Lcom/bytedance/sdk/openadsdk/e/b/l;

    invoke-direct {v5}, Lcom/bytedance/sdk/openadsdk/e/b/l;-><init>()V

    .line 82
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Lcom/bytedance/sdk/openadsdk/e/b/l;->a(Ljava/lang/String;)V

    .line 83
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result p2

    int-to-long v0, p2

    invoke-virtual {v5, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/l;->a(J)V

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->e:J

    sub-long/2addr v0, v6

    invoke-virtual {v5, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/l;->b(J)V

    .line 85
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/video/b/a;->f()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_1

    const-wide/16 p1, 0x1

    .line 86
    invoke-virtual {v5, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/l;->c(J)V

    goto :goto_0

    :cond_1
    const-wide/16 p1, 0x0

    .line 88
    invoke-virtual {v5, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/b/l;->c(J)V

    .line 91
    :goto_0
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 92
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->b(Lcom/bytedance/sdk/openadsdk/e/b/a;)V

    :cond_2
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V
    .locals 8

    .line 98
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->a:Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v0, :cond_0

    .line 99
    invoke-interface {v0, p1, p2, p3}, Lcom/bytedance/sdk/component/video/a/c/a$a;->a(Lcom/bytedance/sdk/component/video/b/a;ILjava/lang/String;)V

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz p1, :cond_2

    .line 102
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 103
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v4

    .line 105
    new-instance v5, Lcom/bytedance/sdk/openadsdk/e/b/j;

    invoke-direct {v5}, Lcom/bytedance/sdk/openadsdk/e/b/j;-><init>()V

    .line 106
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Lcom/bytedance/sdk/openadsdk/e/b/j;->a(Ljava/lang/String;)V

    .line 107
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result p1

    int-to-long v0, p1

    invoke-virtual {v5, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/j;->a(J)V

    .line 108
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->e:J

    sub-long/2addr v0, v6

    invoke-virtual {v5, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/j;->b(J)V

    .line 109
    invoke-virtual {v5, p2}, Lcom/bytedance/sdk/openadsdk/e/b/j;->a(I)V

    .line 110
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const-string p2, ""

    if-eqz p1, :cond_1

    move-object p3, p2

    :cond_1
    invoke-virtual {v5, p3}, Lcom/bytedance/sdk/openadsdk/e/b/j;->b(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v5, p2}, Lcom/bytedance/sdk/openadsdk/e/b/j;->c(Ljava/lang/String;)V

    .line 113
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 114
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->c(Lcom/bytedance/sdk/openadsdk/e/b/a;)V

    :cond_2
    return-void
.end method

.method public b(Lcom/bytedance/sdk/component/video/b/a;I)V
    .locals 6

    .line 121
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->a:Lcom/bytedance/sdk/component/video/a/c/a$a;

    if-eqz v0, :cond_0

    .line 122
    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/component/video/a/c/a$a;->a(Lcom/bytedance/sdk/component/video/b/a;I)V

    .line 125
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz p1, :cond_1

    .line 126
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 127
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 p2, 0x0

    const/4 v0, -0x1

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v4

    .line 128
    new-instance v5, Lcom/bytedance/sdk/openadsdk/e/b/i;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result p2

    int-to-long v0, p2

    invoke-direct {v5, p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/i;-><init>(Ljava/lang/String;J)V

    .line 129
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 130
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->d(Lcom/bytedance/sdk/openadsdk/e/b/a;)V

    :cond_1
    return-void
.end method
