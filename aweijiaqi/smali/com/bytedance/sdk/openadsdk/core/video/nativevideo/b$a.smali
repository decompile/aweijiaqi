.class public final enum Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;
.super Ljava/lang/Enum;
.source "IMediaLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field public static final enum b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field public static final enum c:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field public static final enum d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field public static final enum e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field public static final enum f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

.field private static final synthetic g:[Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 99
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v1, 0x0

    const-string v2, "hideCloseBtn"

    invoke-direct {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 100
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v2, 0x1

    const-string v3, "alwayShowBackBtn"

    invoke-direct {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 101
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v3, 0x2

    const-string v4, "alwayShowMediaView"

    invoke-direct {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 102
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v4, 0x3

    const-string v5, "fixedSize"

    invoke-direct {v0, v5, v4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 103
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v5, 0x4

    const-string v6, "hideBackBtn"

    invoke-direct {v0, v6, v5}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 104
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v6, 0x5

    const-string v7, "hideTopMoreBtn"

    invoke-direct {v0, v7, v6}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    const/4 v7, 0x6

    new-array v7, v7, [Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    .line 98
    sget-object v8, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    aput-object v8, v7, v1

    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    aput-object v1, v7, v2

    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    aput-object v1, v7, v3

    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    aput-object v1, v7, v4

    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    aput-object v1, v7, v5

    aput-object v0, v7, v6

    sput-object v7, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->g:[Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;
    .locals 1

    .line 98
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    return-object p0
.end method

.method public static values()[Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;
    .locals 1

    .line 98
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->g:[Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v0}, [Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    return-object v0
.end method
