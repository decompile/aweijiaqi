.class Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;
.super Ljava/lang/Object;
.source "NativeVideoLayout.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->n()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    const/4 p5, 0x1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    return p5

    :cond_0
    if-eqz p2, :cond_5

    .line 177
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, "click_start_play"

    .line 181
    invoke-virtual {p4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    .line 182
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->C:Z

    if-eqz p1, :cond_2

    const-string p1, "click_start"

    goto :goto_0

    :cond_2
    const-string p1, "click_start_detail"

    .line 183
    :goto_0
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->z:Landroid/content/Context;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->y:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p4, v0, p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return p5

    :cond_3
    const-string p1, "click_open"

    .line 185
    invoke-virtual {p4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 186
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->C:Z

    if-eqz p1, :cond_4

    .line 187
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->z:Landroid/content/Context;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->y:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->y:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->h(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p4, p3, v0, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->i(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 189
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->z:Landroid/content/Context;

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i$1;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/i;->y:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v0, "click_open_detail"

    invoke-static {p1, p4, p3, v0, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_5
    :goto_1
    return p5
.end method
