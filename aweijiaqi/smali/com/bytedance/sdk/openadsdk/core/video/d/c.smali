.class public Lcom/bytedance/sdk/openadsdk/core/video/d/c;
.super Ljava/lang/Object;
.source "VideoPreloadUtils.java"


# direct methods
.method public static a(Lcom/bytedance/sdk/openadsdk/o/f/b;)V
    .locals 1

    const/4 v0, 0x0

    .line 37
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/component/video/a/c/a$a;)V

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/component/video/a/c/a$a;)V
    .locals 12

    .line 43
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->j()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "preloadVideo: preload size = "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 46
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "VideoPreloadUtils"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_3

    .line 48
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->k()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->l()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->i()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result v2

    int-to-long v2, v2

    .line 52
    :goto_0
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result v4

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, -0x1

    .line 53
    invoke-static {v0, v4, v6}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v6

    .line 54
    new-instance v7, Lcom/bytedance/sdk/openadsdk/e/b/k;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/b/k;-><init>(Ljava/lang/String;J)V

    .line 55
    new-instance v8, Lcom/bytedance/sdk/openadsdk/e/b/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v3

    move-object v2, v8

    move-object v4, v0

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 56
    invoke-static {v8}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;)V

    .line 58
    :cond_2
    new-instance v9, Lcom/bytedance/sdk/component/video/b/a;

    invoke-direct {v9}, Lcom/bytedance/sdk/component/video/b/a;-><init>()V

    .line 59
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->b(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->a(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->d()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v9, v2, v3}, Lcom/bytedance/sdk/component/video/b/a;->b(J)V

    .line 62
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->i()J

    move-result-wide v2

    invoke-virtual {v9, v2, v3}, Lcom/bytedance/sdk/component/video/b/a;->a(J)V

    .line 63
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->j()Z

    move-result v2

    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->a(Z)V

    .line 64
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->c(Ljava/lang/String;)V

    const/16 v2, 0x1770

    .line 65
    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->b(I)V

    .line 66
    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->c(I)V

    .line 67
    invoke-virtual {v9, v2}, Lcom/bytedance/sdk/component/video/b/a;->d(I)V

    .line 69
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    .line 70
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v10

    new-instance v11, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;

    move-object v2, v11

    move-object v3, p1

    move-object v4, v0

    move-object v5, v1

    move-object v6, p0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/core/video/d/c$1;-><init>(Lcom/bytedance/sdk/component/video/a/c/a$a;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/o/f/b;J)V

    invoke-static {v10, v9, v11}, Lcom/bytedance/sdk/component/video/a/b/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/component/video/b/a;Lcom/bytedance/sdk/component/video/a/c/a$a;)V

    goto :goto_1

    .line 137
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/f/a;->a()Lcom/bytedance/sdk/openadsdk/o/f/a;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/o/f/a;->a(Lcom/bytedance/sdk/openadsdk/o/f/b;)Z

    :goto_1
    return-void
.end method
