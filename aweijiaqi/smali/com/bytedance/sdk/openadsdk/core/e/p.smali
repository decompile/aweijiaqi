.class public Lcom/bytedance/sdk/openadsdk/core/e/p;
.super Ljava/lang/Object;
.source "RenderInfo.java"


# instance fields
.field private a:I

.field private b:Z

.field private c:D

.field private d:D

.field private e:D

.field private f:D

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:D

.field private l:D

.field private m:Ljava/lang/String;

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->a:I

    return v0
.end method

.method public a(D)V
    .locals 0

    .line 49
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->c:D

    return-void
.end method

.method public a(F)V
    .locals 0

    .line 133
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->g:F

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 33
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->a:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->m:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 41
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->b:Z

    return-void
.end method

.method public b(D)V
    .locals 0

    .line 57
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->d:D

    return-void
.end method

.method public b(F)V
    .locals 0

    .line 137
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->h:F

    return-void
.end method

.method public b(I)V
    .locals 0

    .line 105
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->n:I

    return-void
.end method

.method public b()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->b:Z

    return v0
.end method

.method public c()D
    .locals 2

    .line 45
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->c:D

    return-wide v0
.end method

.method public c(D)V
    .locals 0

    .line 65
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->e:D

    return-void
.end method

.method public c(F)V
    .locals 0

    .line 141
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->i:F

    return-void
.end method

.method public d()D
    .locals 2

    .line 53
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->d:D

    return-wide v0
.end method

.method public d(D)V
    .locals 0

    .line 73
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->f:D

    return-void
.end method

.method public d(F)V
    .locals 0

    .line 145
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->j:F

    return-void
.end method

.method public e()D
    .locals 2

    .line 61
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->e:D

    return-wide v0
.end method

.method public e(D)V
    .locals 0

    .line 81
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->k:D

    return-void
.end method

.method public f()D
    .locals 2

    .line 69
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->f:D

    return-wide v0
.end method

.method public f(D)V
    .locals 0

    .line 89
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->l:D

    return-void
.end method

.method public g()D
    .locals 2

    .line 77
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->k:D

    return-wide v0
.end method

.method public h()D
    .locals 2

    .line 85
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->l:D

    return-wide v0
.end method

.method public i()I
    .locals 1

    .line 101
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->n:I

    return v0
.end method

.method public j()F
    .locals 1

    .line 117
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->g:F

    return v0
.end method

.method public k()F
    .locals 1

    .line 121
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->h:F

    return v0
.end method

.method public l()F
    .locals 1

    .line 125
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->i:F

    return v0
.end method

.method public m()F
    .locals 1

    .line 129
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/p;->j:F

    return v0
.end method
