.class public Lcom/bytedance/sdk/openadsdk/core/e/f$a;
.super Ljava/lang/Object;
.source "ClickEventModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/e/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:[I

.field private h:[I

.field private i:[I

.field private j:[I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/b/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)[I
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->h:[I

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)[I
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->i:[I

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)[I
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->j:[I

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)[I
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->g:[I

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->f:I

    return p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->e:I

    return p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)J
    .locals 2

    .line 119
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b:J

    return-wide v0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)J
    .locals 2

    .line 119
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a:J

    return-wide v0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->k:I

    return p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->l:I

    return p0
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->m:I

    return p0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)I
    .locals 0

    .line 119
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->o:I

    return p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)Landroid/util/SparseArray;
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->n:Landroid/util/SparseArray;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/core/e/f$a;)Ljava/lang/String;
    .locals 0

    .line 119
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->p:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 138
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->o:I

    return-object p0
.end method

.method public a(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 148
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a:J

    return-object p0
.end method

.method public a(Landroid/util/SparseArray;)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/b/c$a;",
            ">;)",
            "Lcom/bytedance/sdk/openadsdk/core/e/f$a;"
        }
    .end annotation

    .line 143
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->n:Landroid/util/SparseArray;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->p:Ljava/lang/String;

    return-object p0
.end method

.method public a([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->g:[I

    return-object p0
.end method

.method public a()Lcom/bytedance/sdk/openadsdk/core/e/f;
    .locals 2

    .line 218
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/f;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/f$a;Lcom/bytedance/sdk/openadsdk/core/e/f$1;)V

    return-object v0
.end method

.method public b(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 158
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c:I

    return-object p0
.end method

.method public b(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 153
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b:J

    return-object p0
.end method

.method public b([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->h:[I

    return-object p0
.end method

.method public c(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 163
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d:I

    return-object p0
.end method

.method public c([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->i:[I

    return-object p0
.end method

.method public d(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 168
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->e:I

    return-object p0
.end method

.method public d([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->j:[I

    return-object p0
.end method

.method public e(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 173
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->f:I

    return-object p0
.end method

.method public f(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 198
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->k:I

    return-object p0
.end method

.method public g(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 203
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->l:I

    return-object p0
.end method

.method public h(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;
    .locals 0

    .line 208
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->m:I

    return-object p0
.end method
