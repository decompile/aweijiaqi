.class public Lcom/bytedance/sdk/openadsdk/core/e/o;
.super Ljava/lang/Object;
.source "PlayableModel.java"


# instance fields
.field private a:Z

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 4

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "playable"

    .line 72
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string v2, ""

    const-string v3, "playable_url"

    .line 74
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->d:Ljava/lang/String;

    const-string v3, "playable_download_url"

    .line 75
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->e:Ljava/lang/String;

    const-string v2, "if_playable_loading_show"

    .line 76
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->f:I

    const-string v2, "remove_loading_page_type"

    .line 77
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->g:I

    const-string v2, "playable_orientation"

    .line 78
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->h:I

    :cond_1
    const-string v0, "is_playable"

    .line 80
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->a:Z

    const-string v0, "playable_type"

    .line 81
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->b:I

    const-string v0, "playable_style"

    .line 82
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->c:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 127
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 131
    :cond_0
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/o;->a:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->e(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 140
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 144
    :cond_0
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/e/o;->a:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static c(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;
    .locals 0

    .line 162
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 166
    :cond_0
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;
    .locals 0

    .line 174
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 178
    :cond_0
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->d:Ljava/lang/String;

    return-object p0
.end method

.method public static e(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 190
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 194
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->j()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 206
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    .line 208
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->q()I

    move-result p0

    goto :goto_0

    .line 212
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 214
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->f:I

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public static g(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 224
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    .line 226
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 227
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->r()I

    move-result p0

    goto :goto_0

    .line 230
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 232
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->g:I

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public static h(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 243
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->d(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    if-eqz p0, :cond_1

    .line 245
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 248
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->r()I

    move-result p0

    goto :goto_1

    :cond_1
    :goto_0
    return v1

    .line 250
    :cond_2
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-nez p0, :cond_3

    return v1

    .line 254
    :cond_3
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->g:I

    :goto_1
    const/4 v0, 0x1

    if-ne p0, v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public static i(Lcom/bytedance/sdk/openadsdk/core/e/m;)I
    .locals 0

    .line 263
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 267
    :cond_0
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->h:I

    return p0
.end method

.method public static j(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    .line 274
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->m(Lcom/bytedance/sdk/openadsdk/core/e/m;)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static k(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 1

    .line 281
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->m(Lcom/bytedance/sdk/openadsdk/core/e/m;)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->g()Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    return-object p0
.end method

.method private static m(Lcom/bytedance/sdk/openadsdk/core/e/m;)I
    .locals 0

    .line 151
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->l(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/core/e/o;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 155
    :cond_0
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->b:I

    return p0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 3

    :try_start_0
    const-string v0, "is_playable"

    .line 87
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->a:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 91
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_1
    const-string v1, "playable_url"

    .line 94
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "playable_download_url"

    .line 95
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "if_playable_loading_show"

    .line 96
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->f:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "remove_loading_page_type"

    .line 97
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->g:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "playable_orientation"

    .line 98
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->h:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "playable"

    .line 99
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 101
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_1
    :try_start_2
    const-string v0, "playable_type"

    .line 105
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->b:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    .line 107
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_2
    :try_start_3
    const-string v0, "playable_style"

    .line 110
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/o;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    .line 112
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    :goto_3
    return-void
.end method
