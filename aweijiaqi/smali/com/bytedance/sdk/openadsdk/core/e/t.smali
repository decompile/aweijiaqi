.class public Lcom/bytedance/sdk/openadsdk/core/e/t;
.super Ljava/lang/Object;
.source "SplashReportModel.java"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Z

.field private e:J

.field private f:Z

.field private g:I

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:I

.field private p:D

.field private q:Ljava/lang/String;

.field private r:Lorg/json/JSONObject;

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:J

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->d:Z

    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->f:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->g:I

    return v0
.end method

.method public a(D)V
    .locals 0

    .line 137
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->p:D

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 73
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->g:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 105
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->k:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->h:Ljava/lang/String;

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->r:Lorg/json/JSONObject;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 249
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->d:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->h:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 89
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->i:I

    return-void
.end method

.method public b(J)V
    .locals 0

    .line 113
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->l:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->q:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 273
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->f:Z

    return-void
.end method

.method public c()I
    .locals 1

    .line 85
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->i:I

    return v0
.end method

.method public c(I)V
    .locals 0

    .line 97
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->j:I

    return-void
.end method

.method public c(J)V
    .locals 0

    .line 121
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->n:J

    return-void
.end method

.method public d()I
    .locals 1

    .line 93
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->j:I

    return v0
.end method

.method public d(I)V
    .locals 0

    .line 129
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->o:I

    return-void
.end method

.method public d(J)V
    .locals 0

    .line 161
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->s:J

    return-void
.end method

.method public e()J
    .locals 2

    .line 101
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->k:J

    return-wide v0
.end method

.method public e(J)V
    .locals 0

    .line 169
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->t:J

    return-void
.end method

.method public f()J
    .locals 2

    .line 109
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->l:J

    return-wide v0
.end method

.method public f(J)V
    .locals 0

    .line 177
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->u:J

    return-void
.end method

.method public g()J
    .locals 2

    .line 117
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->n:J

    return-wide v0
.end method

.method public g(J)V
    .locals 0

    .line 185
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->v:J

    return-void
.end method

.method public h()I
    .locals 1

    .line 125
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->o:I

    return v0
.end method

.method public h(J)V
    .locals 0

    .line 193
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->w:J

    return-void
.end method

.method public i()D
    .locals 2

    .line 133
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->p:D

    return-wide v0
.end method

.method public i(J)V
    .locals 0

    .line 201
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->x:J

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->q:Ljava/lang/String;

    return-object v0
.end method

.method public j(J)V
    .locals 0

    .line 209
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->y:J

    return-void
.end method

.method public k()Lorg/json/JSONObject;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->r:Lorg/json/JSONObject;

    return-object v0
.end method

.method public k(J)V
    .locals 0

    .line 217
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->z:J

    return-void
.end method

.method public l()J
    .locals 2

    .line 157
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->s:J

    return-wide v0
.end method

.method public l(J)V
    .locals 0

    .line 225
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->a:J

    return-void
.end method

.method public m()J
    .locals 2

    .line 165
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->t:J

    return-wide v0
.end method

.method public m(J)V
    .locals 0

    .line 233
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->b:J

    return-void
.end method

.method public n()J
    .locals 2

    .line 173
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->u:J

    return-wide v0
.end method

.method public n(J)V
    .locals 0

    .line 241
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->c:J

    return-void
.end method

.method public o()J
    .locals 2

    .line 181
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->v:J

    return-wide v0
.end method

.method public o(J)V
    .locals 0

    .line 257
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->m:J

    return-void
.end method

.method public p()J
    .locals 2

    .line 189
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->w:J

    return-wide v0
.end method

.method public p(J)V
    .locals 0

    .line 265
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->e:J

    return-void
.end method

.method public q()J
    .locals 2

    .line 197
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->x:J

    return-wide v0
.end method

.method public r()J
    .locals 2

    .line 205
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->y:J

    return-wide v0
.end method

.method public s()J
    .locals 2

    .line 213
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->z:J

    return-wide v0
.end method

.method public t()J
    .locals 2

    .line 221
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->a:J

    return-wide v0
.end method

.method public u()J
    .locals 2

    .line 229
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->b:J

    return-wide v0
.end method

.method public v()J
    .locals 2

    .line 237
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->c:J

    return-wide v0
.end method

.method public w()Z
    .locals 1

    .line 245
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->d:Z

    return v0
.end method

.method public x()J
    .locals 2

    .line 253
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->m:J

    return-wide v0
.end method

.method public y()J
    .locals 2

    .line 261
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->e:J

    return-wide v0
.end method

.method public z()Z
    .locals 1

    .line 269
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/t;->f:Z

    return v0
.end method
