.class public Lcom/bytedance/sdk/openadsdk/core/e/n;
.super Ljava/lang/Object;
.source "NetExtParams.java"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Lorg/json/JSONArray;

.field public e:I

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/o;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->a:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->b:I

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->d:Lorg/json/JSONArray;

    const/4 v1, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->e:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->f:J

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->j:Lorg/json/JSONObject;

    return-void
.end method


# virtual methods
.method public a()Lcom/bytedance/sdk/openadsdk/core/e/n;
    .locals 3

    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/e/n;-><init>()V

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->b:I

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->b:I

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->c:I

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->c:I

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->d:Lorg/json/JSONArray;

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->d:Lorg/json/JSONArray;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->e:I

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->e:I

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->f:J

    iput-wide v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->f:J

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->g:J

    iput-wide v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->g:J

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->h:J

    iput-wide v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->h:J

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/n;->i:J

    iput-wide v1, v0, Lcom/bytedance/sdk/openadsdk/core/e/n;->i:J

    return-object v0
.end method
