.class public Lcom/bytedance/sdk/openadsdk/core/e/k;
.super Ljava/lang/Object;
.source "DynamicClickInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:J

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)V
    .locals 2

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->a:I

    .line 24
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->b(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->b:I

    .line 25
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->c(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->c:I

    .line 26
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->d(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->d:I

    .line 27
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->e(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->e:J

    .line 28
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->f(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->f:J

    .line 29
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->g(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->g:I

    .line 30
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->h(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->h:I

    .line 31
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->i(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->i:I

    .line 32
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->j(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->j:I

    .line 33
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->k(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k;->k:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/core/e/k$a;Lcom/bytedance/sdk/openadsdk/core/e/k$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/e/k;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)V

    return-void
.end method
