.class public Lcom/bytedance/sdk/openadsdk/core/e/x;
.super Ljava/lang/Object;
.source "VideoInfo.java"


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:D

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->l:I

    .line 36
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->m:I

    .line 40
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->n:I

    .line 41
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->o:I

    const v0, 0x4b000

    .line 42
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)[I
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object v1

    if-nez v1, :cond_1

    return-object v0

    .line 239
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->g()[I

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->k:I

    return v0
.end method

.method public a(D)V
    .locals 0

    .line 87
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->d:D

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->k:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 79
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->c:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->e:Ljava/lang/String;

    return-void
.end method

.method public b()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->a:I

    return v0
.end method

.method public b(I)V
    .locals 0

    .line 63
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->a:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->f:Ljava/lang/String;

    return-void
.end method

.method public c()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->b:I

    return v0
.end method

.method public c(I)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->b:I

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->g:Ljava/lang/String;

    return-void
.end method

.method public d()J
    .locals 2

    .line 75
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->c:J

    return-wide v0
.end method

.method public d(I)V
    .locals 0

    .line 168
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->h:Ljava/lang/String;

    return-void
.end method

.method public e()D
    .locals 2

    .line 83
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->d:D

    return-wide v0
.end method

.method public e(I)V
    .locals 0

    .line 177
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->n:I

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->i:Ljava/lang/String;

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)V
    .locals 0

    .line 185
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->o:I

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->j:Ljava/lang/String;

    return-void
.end method

.method public g(I)V
    .locals 0

    .line 221
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->l:I

    return-void
.end method

.method public g()[I
    .locals 5

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->e:Ljava/lang/String;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 103
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 104
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->e:Ljava/lang/String;

    const/4 v4, 0x1

    add-int/2addr v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 106
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v1, v3, v2

    aput v0, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v3

    :catchall_0
    move-exception v0

    const-string v1, "VideoInfo"

    const-string v2, "getWidthAndHeight error"

    .line 109
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h(I)V
    .locals 0

    .line 229
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->m:I

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->h:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->i:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/component/video/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->j:Ljava/lang/String;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->j:Ljava/lang/String;

    return-object v0
.end method

.method public m()I
    .locals 5

    .line 158
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    if-gez v0, :cond_0

    const v0, 0x4b000

    .line 159
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    .line 161
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->c:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    long-to-int v0, v2

    .line 162
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    .line 164
    :cond_1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->p:I

    return v0
.end method

.method public n()I
    .locals 1

    .line 173
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->n:I

    return v0
.end method

.method public o()I
    .locals 1

    .line 181
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->o:I

    return v0
.end method

.method public p()Lorg/json/JSONObject;
    .locals 4

    .line 189
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "cover_height"

    .line 191
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "cover_url"

    .line 192
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "cover_width"

    .line 193
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "endcard"

    .line 194
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "file_hash"

    .line 195
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "resolution"

    .line 196
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "size"

    .line 197
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "video_duration"

    .line 198
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->e()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v1, "video_url"

    .line 199
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "playable_download_url"

    .line 200
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "if_playable_loading_show"

    .line 201
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->q()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "remove_loading_page_type"

    .line 202
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->r()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "fallback_endcard_judge"

    .line 203
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "video_preload_size"

    .line 204
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->m()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "reward_video_cached_type"

    .line 205
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "execute_cached_type"

    .line 206
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->o()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method

.method public q()I
    .locals 1

    .line 217
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->l:I

    return v0
.end method

.method public r()I
    .locals 1

    .line 225
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->m:I

    return v0
.end method

.method public s()Z
    .locals 2

    .line 247
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public t()Z
    .locals 1

    .line 257
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/x;->n:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
