.class public Lcom/bytedance/sdk/openadsdk/core/e/k$a;
.super Ljava/lang/Object;
.source "DynamicClickInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/e/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->f:I

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->e:I

    return p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->d:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->c:I

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)J
    .locals 2

    .line 36
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->b:J

    return-wide v0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)J
    .locals 2

    .line 36
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a:J

    return-wide v0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->g:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->h:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->i:I

    return p0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)I
    .locals 0

    .line 36
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->j:I

    return p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/core/e/k$a;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->k:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 60
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->c:I

    return-object p0
.end method

.method public a(J)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 50
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a:J

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->k:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/bytedance/sdk/openadsdk/core/e/k;
    .locals 2

    .line 106
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/k;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/k$a;Lcom/bytedance/sdk/openadsdk/core/e/k$1;)V

    return-object v0
.end method

.method public b(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 65
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->d:I

    return-object p0
.end method

.method public b(J)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 55
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->b:J

    return-object p0
.end method

.method public c(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 70
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->e:I

    return-object p0
.end method

.method public d(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 75
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->f:I

    return-object p0
.end method

.method public e(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 80
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->g:I

    return-object p0
.end method

.method public f(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 85
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->h:I

    return-object p0
.end method

.method public g(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 90
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->i:I

    return-object p0
.end method

.method public h(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;
    .locals 0

    .line 95
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->j:I

    return-object p0
.end method
