.class final Lcom/bytedance/sdk/openadsdk/core/z$1;
.super Ljava/lang/Object;
.source "WebHelper.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/TTNativeAd;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;ZLjava/util/Map;ZZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:I

.field final synthetic e:Z

.field final synthetic f:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Landroid/content/Context;IZLjava/util/Map;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->c:Landroid/content/Context;

    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->d:I

    iput-boolean p5, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->e:Z

    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->f:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 93
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->b:Ljava/lang/String;

    const-string v3, "deeplink_success_realtime"

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 8

    .line 98
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->az()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->d:I

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->b:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->e:Z

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->f:Ljava/util/Map;

    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;ILjava/lang/String;ZLjava/util/Map;)Z

    const-string v0, "WebHelper"

    const-string v1, "openDetailPage() -> context.startActivity(intent) fail :"

    .line 100
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/z$1;->b:Ljava/lang/String;

    const-string v2, "deeplink_fail_realtime"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
