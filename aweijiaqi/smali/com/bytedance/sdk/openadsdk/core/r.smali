.class public Lcom/bytedance/sdk/openadsdk/core/r;
.super Ljava/lang/Object;
.source "ScreenShotObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/r$a;,
        Lcom/bytedance/sdk/openadsdk/core/r$b;
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static volatile d:Z

.field private static volatile e:Z

.field private static f:Lcom/bytedance/sdk/openadsdk/core/r$b;

.field private static g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/r;->a:Ljava/lang/String;

    .line 27
    sget-object v0, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/r;->b:Ljava/lang/String;

    const-string v0, "Screenshots"

    .line 28
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/r;->c:Ljava/lang/String;

    const/4 v0, 0x0

    .line 29
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->d:Z

    .line 30
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->e:Z

    const-wide/16 v0, 0x0

    .line 34
    sput-wide v0, Lcom/bytedance/sdk/openadsdk/core/r;->g:J

    return-void
.end method

.method static synthetic a(J)J
    .locals 0

    .line 23
    sput-wide p0, Lcom/bytedance/sdk/openadsdk/core/r;->g:J

    return-wide p0
.end method

.method public static a()V
    .locals 1

    .line 40
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->e:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->d:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/r;->b()V

    :cond_1
    :goto_0
    return-void
.end method

.method public static b()V
    .locals 2

    const/4 v0, 0x1

    .line 53
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->e:Z

    .line 54
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->d:Z

    if-eqz v0, :cond_0

    return-void

    .line 57
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 58
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 63
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/r$1;

    const-string v1, "sso"

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/r$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public static c()J
    .locals 2

    .line 95
    sget-wide v0, Lcom/bytedance/sdk/openadsdk/core/r;->g:J

    return-wide v0
.end method

.method static synthetic d()V
    .locals 0

    .line 23
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/r;->e()V

    return-void
.end method

.method private static e()V
    .locals 4

    .line 72
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/r;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "SSO start"

    .line 75
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/r;->f()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 80
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/r$2;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/r$2;-><init>()V

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/r$b;->a(Ljava/io/File;Lcom/bytedance/sdk/openadsdk/core/r$a;)Lcom/bytedance/sdk/openadsdk/core/r$b;

    move-result-object v1

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/r;->f:Lcom/bytedance/sdk/openadsdk/core/r$b;

    const/4 v1, 0x1

    .line 87
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/core/r;->d:Z

    .line 88
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SSO File exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, "false"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", has started: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/r;->f:Lcom/bytedance/sdk/openadsdk/core/r$b;

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/r;->f:Lcom/bytedance/sdk/openadsdk/core/r$b;

    if-eqz v0, :cond_4

    .line 90
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/r$b;->startWatching()V

    :cond_4
    return-void
.end method

.method private static f()Ljava/io/File;
    .locals 5

    .line 105
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1d

    if-lt v0, v2, :cond_0

    goto :goto_1

    .line 108
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    .line 112
    :cond_1
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/bytedance/sdk/openadsdk/core/r;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/bytedance/sdk/openadsdk/core/r;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "SSO use pic"

    .line 114
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    return-object v1

    .line 117
    :cond_2
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/bytedance/sdk/openadsdk/core/r;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/bytedance/sdk/openadsdk/core/r;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "SSO use dc"

    .line 119
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    return-object v2

    .line 122
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/j;->b()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/j;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    const-string v0, "SSO use rom dc"

    .line 126
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    return-object v2

    :cond_5
    :goto_0
    const-string v0, "SSO use rom pic"

    .line 123
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;)V

    :cond_6
    :goto_1
    return-object v1
.end method
