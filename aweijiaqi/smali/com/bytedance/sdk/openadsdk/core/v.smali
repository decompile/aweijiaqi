.class public Lcom/bytedance/sdk/openadsdk/core/v;
.super Ljava/lang/Object;
.source "TTAdNativeImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdNative;


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/core/p;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/v;->a:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 50
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/v;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/v;)Landroid/content/Context;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/v;->b:Landroid/content/Context;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V
    .locals 1

    .line 514
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/l;->c:Z

    if-nez v0, :cond_1

    const-string p1, "TTAdNativeImpl"

    const-string v0, "please exec TTAdSdk.init before load ad"

    .line 515
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const/16 p1, 0x2710

    const-string v0, "Please exec TTAdSdk.init before load ad"

    .line 517
    invoke-interface {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/c/b;->onError(ILjava/lang/String;)V

    :cond_0
    return-void

    .line 521
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->c()Landroid/os/Handler;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 4

    .line 471
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "\u5fc5\u987b\u8bbe\u7f6e\u56fe\u7247\u7d20\u6750\u5c3a\u5bf8"

    invoke-static {v0, v3}, Lcom/bytedance/sdk/component/utils/n;->a(ZLjava/lang/String;)V

    .line 472
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getImgAcceptedHeight()I

    move-result p1

    if-lez p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/n;->a(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/v;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->c(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;Z)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 506
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object p2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/bytedance/sdk/openadsdk/core/j/h;->i(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_1

    return v1

    .line 509
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result p1

    const/4 p2, 0x0

    cmpl-float p1, p1, p2

    if-lez p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/c/b;)Z
    .locals 2

    .line 493
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const/16 v0, 0x3e8

    const-string v1, "\u5e7f\u544a\u8bf7\u6c42\u5f00\u5173\u5df2\u5173\u95ed,\u8bf7\u8054\u7cfb\u7a7f\u5c71\u7532\u7ba1\u7406\u5458"

    .line 495
    invoke-interface {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/c/b;->onError(ILjava/lang/String;)V

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/v;Lcom/bytedance/sdk/openadsdk/AdSlot;Z)Z
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Z)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/v;Lcom/bytedance/sdk/openadsdk/c/b;)Z
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/openadsdk/c/b;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/v;)Lcom/bytedance/sdk/openadsdk/core/p;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/v;->a:Lcom/bytedance/sdk/openadsdk/core/p;

    return-object p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 1

    .line 476
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 477
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getNativeAdType()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v0, "\u5fc5\u987b\u8bbe\u7f6e\u8bf7\u6c42\u539f\u751f\u5e7f\u544a\u7684\u7c7b\u578b\uff0c "

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/n;->a(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/v;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 1

    .line 481
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 482
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getNativeAdType()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v0, "\u8bf7\u6c42\u975e\u539f\u751f\u5e7f\u544a\u7684\u7c7b\u578b\uff0c\u8bf7\u52ff\u8c03\u7528setNativeAdType()\u65b9\u6cd5"

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/n;->a(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/v;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/v;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method


# virtual methods
.method public loadBannerAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;)V
    .locals 2

    .line 195
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/a;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/a;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;)V

    .line 196
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$10;

    const-string v1, "loadBannerAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$10;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/a;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 218
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 219
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadBannerExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V
    .locals 2

    .line 422
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/g;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/g;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    .line 424
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$5;

    const-string v1, "loadBannerExpressAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/g;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 442
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 443
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadDrawFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$DrawFeedAdListener;)V
    .locals 2

    .line 116
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/b;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/b;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$DrawFeedAdListener;)V

    .line 118
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$8;

    const-string v1, "loadDrawFeedAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$8;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/b;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 139
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 140
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/16 v0, 0x9

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadExpressDrawFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V
    .locals 2

    .line 398
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/g;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/g;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    .line 399
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$4;

    const-string v1, "loadExpressDrawFeedAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/g;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 415
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 416
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/16 v0, 0x9

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;)V
    .locals 2

    .line 57
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/c;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/c;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;)V

    .line 58
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$1;

    const-string v1, "loadFeedAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/c;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 78
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 79
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x5

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadFullScreenVideoAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;)V
    .locals 2

    .line 346
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/d;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/d;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;)V

    .line 347
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$2;

    const-string v1, "loadFullScreenVideoAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/d;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 367
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 368
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/16 v0, 0x8

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadInteractionAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;)V
    .locals 2

    .line 225
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/e;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/e;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;)V

    .line 226
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$11;

    const-string v1, "loadInteractionAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$11;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/e;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 249
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 250
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x2

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadInteractionExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V
    .locals 2

    .line 449
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/g;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/g;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    .line 450
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$6;

    const-string v1, "loadInteractionExpressAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/g;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 465
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 466
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x2

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadNativeAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeAdListener;)V
    .locals 8

    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 147
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getNativeAdType()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->setDurationSlotType(I)V

    .line 148
    new-instance v7, Lcom/bytedance/sdk/openadsdk/core/a/f;

    invoke-direct {v7, p2}, Lcom/bytedance/sdk/openadsdk/core/a/f;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeAdListener;)V

    .line 149
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$9;

    const-string v2, "loadNativeAd"

    move-object v0, p2

    move-object v1, p0

    move-object v3, v7

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/core/v$9;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/f;Lcom/bytedance/sdk/openadsdk/AdSlot;J)V

    .line 188
    invoke-direct {p0, p2, v7}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 189
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadNativeExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V
    .locals 2

    .line 374
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/g;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/g;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    .line 375
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$3;

    const-string v1, "loadNativeExpressAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/g;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 391
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 392
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x5

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadRewardVideoAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;)V
    .locals 2

    .line 318
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/h;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/h;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;)V

    .line 319
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$14;

    const-string v1, "loadRewardVideoAd"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$14;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/h;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 339
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 340
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x7

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadSplashAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;)V
    .locals 2

    .line 285
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/i;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/i;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;)V

    .line 286
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$13;

    const-string v1, "loadSplashAd a"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$13;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/i;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 310
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 311
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x3

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadSplashAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;I)V
    .locals 7

    .line 257
    new-instance v6, Lcom/bytedance/sdk/openadsdk/core/a/i;

    invoke-direct {v6, p2}, Lcom/bytedance/sdk/openadsdk/core/a/i;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;)V

    .line 258
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$12;

    const-string v2, "loadSplashAd b"

    move-object v0, p2

    move-object v1, p0

    move-object v3, v6

    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/core/v$12;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/i;Lcom/bytedance/sdk/openadsdk/AdSlot;I)V

    .line 278
    invoke-direct {p0, p2, v6}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 279
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 p3, 0x3

    invoke-virtual {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public loadStream(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;)V
    .locals 2

    .line 87
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/c;

    invoke-direct {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/a/c;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;)V

    .line 89
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/v$7;

    const-string v1, "loadStream"

    invoke-direct {p2, p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/v$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/v;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/a/c;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 109
    invoke-direct {p0, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/v;->a(Lcom/bytedance/sdk/component/e/g;Lcom/bytedance/sdk/openadsdk/c/b;)V

    .line 110
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/b/a;->a()Lcom/bytedance/sdk/openadsdk/b/a;

    move-result-object p2

    const/4 v0, 0x6

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/a;->a(ILcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method
