.class final Lcom/bytedance/sdk/openadsdk/core/k/b$1;
.super Lcom/bytedance/sdk/component/e/g;
.source "AdLocationUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/k/b;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/k/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .line 98
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/k/b$1;->a:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 101
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/k/b$1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/k/b;->b(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/k/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "latitude"

    .line 105
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/k/a;->a:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "longitude"

    .line 106
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/k/a;->b:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "lbstime"

    .line 107
    iget-wide v3, v0, Lcom/bytedance/sdk/openadsdk/core/k/a;->c:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "sdk_ad_location"

    .line 108
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 110
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 112
    :goto_0
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/k/b;->a(Lcom/bytedance/sdk/openadsdk/core/k/a;)Lcom/bytedance/sdk/openadsdk/core/k/a;

    :cond_0
    return-void
.end method
