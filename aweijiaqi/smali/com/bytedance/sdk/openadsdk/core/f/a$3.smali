.class Lcom/bytedance/sdk/openadsdk/core/f/a$3;
.super Ljava/lang/Object;
.source "TTNativeAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/f/a;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/f/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f/a;)V
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked(Landroid/view/View;I)V
    .locals 1

    .line 531
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 532
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-interface {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;->onAdClicked(Landroid/view/View;Lcom/bytedance/sdk/openadsdk/TTNativeAd;)V

    :cond_0
    return-void
.end method

.method public onAdShow(Landroid/view/View;I)V
    .locals 0

    .line 538
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 539
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/TTNativeAd$AdInteractionListener;->onAdShow(Lcom/bytedance/sdk/openadsdk/TTNativeAd;)V

    :cond_0
    return-void
.end method

.method public onRenderFail(Landroid/view/View;Ljava/lang/String;I)V
    .locals 2

    .line 546
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->b(Lcom/bytedance/sdk/openadsdk/core/f/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 547
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->c(Lcom/bytedance/sdk/openadsdk/core/f/a;)Landroid/view/ViewGroup;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;Landroid/view/View;)Landroid/view/View;

    .line 548
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    iget-object p2, p1, Lcom/bytedance/sdk/openadsdk/core/f/a;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;F)F

    .line 549
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    iget-object p2, p1, Lcom/bytedance/sdk/openadsdk/core/f/a;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedHeight()F

    move-result p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->b(Lcom/bytedance/sdk/openadsdk/core/f/a;F)F

    .line 550
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->d(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 551
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->d(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->c(Lcom/bytedance/sdk/openadsdk/core/f/a;)Landroid/view/ViewGroup;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    iget-object p3, p3, Lcom/bytedance/sdk/openadsdk/core/f/a;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result p3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/f/a;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 552
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedHeight()F

    move-result v0

    const/4 v1, 0x0

    .line 551
    invoke-interface {p1, p2, p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;->onRenderSuccess(Landroid/view/View;FFZ)V

    :cond_0
    return-void
.end method

.method public onRenderSuccess(Landroid/view/View;FF)V
    .locals 2

    .line 558
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f/a;->b(Lcom/bytedance/sdk/openadsdk/core/f/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 559
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f/a;->e(Lcom/bytedance/sdk/openadsdk/core/f/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 560
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;Landroid/view/View;)Landroid/view/View;

    .line 561
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/f/a;->a(Lcom/bytedance/sdk/openadsdk/core/f/a;F)F

    .line 562
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0, p3}, Lcom/bytedance/sdk/openadsdk/core/f/a;->b(Lcom/bytedance/sdk/openadsdk/core/f/a;F)F

    .line 563
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f/a;->d(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f/a$3;->a:Lcom/bytedance/sdk/openadsdk/core/f/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f/a;->d(Lcom/bytedance/sdk/openadsdk/core/f/a;)Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeAd$ExpressRenderListener;->onRenderSuccess(Landroid/view/View;FFZ)V

    :cond_0
    return-void
.end method
