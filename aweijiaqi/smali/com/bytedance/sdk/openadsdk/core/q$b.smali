.class public Lcom/bytedance/sdk/openadsdk/core/q$b;
.super Ljava/lang/Object;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:I

.field public final b:Z


# direct methods
.method private constructor <init>(IZ)V
    .locals 0

    .line 1910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1911
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$b;->a:I

    .line 1912
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/q$b;->b:Z

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/q$b;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "error_code"

    .line 1919
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "result"

    .line 1920
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result p0

    .line 1921
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-direct {v1, v0, p0}, Lcom/bytedance/sdk/openadsdk/core/q$b;-><init>(IZ)V

    return-object v1
.end method
