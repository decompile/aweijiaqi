.class Lcom/bytedance/sdk/openadsdk/core/q$8;
.super Lcom/bytedance/sdk/component/net/callback/NetCallback;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/p$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/p$a;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/core/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/p$a;)V
    .locals 0

    .line 1734
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$8;->b:Lcom/bytedance/sdk/openadsdk/core/q;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/q$8;->a:Lcom/bytedance/sdk/openadsdk/core/p$a;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V
    .locals 6

    .line 1772
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/q$8;->a:Lcom/bytedance/sdk/openadsdk/core/p$a;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/core/p$a;->a(ZJJ)V

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V
    .locals 10

    if-eqz p2, :cond_3

    .line 1741
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result p1

    const-wide/16 v0, -0x1

    if-eqz p1, :cond_1

    const-wide/16 v2, 0x0

    const/4 p1, 0x0

    .line 1745
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1747
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1748
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/q$b;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/q$b;

    move-result-object v4

    .line 1749
    iget v0, v4, Lcom/bytedance/sdk/openadsdk/core/q$b;->a:I

    int-to-long v0, v0

    .line 1750
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v2

    .line 1751
    iget-boolean p1, v4, Lcom/bytedance/sdk/openadsdk/core/q$b;->b:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move v5, p1

    move-wide v6, v0

    move-wide v8, v2

    goto :goto_0

    :catch_0
    move-exception p2

    .line 1753
    invoke-virtual {p2}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    move-wide v6, v0

    move-wide v8, v2

    const/4 v5, 0x0

    .line 1758
    :goto_0
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/q$8;->a:Lcom/bytedance/sdk/openadsdk/core/p$a;

    invoke-interface/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/core/p$a;->a(ZJJ)V

    goto :goto_1

    .line 1761
    :cond_1
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getCode()I

    move-result p1

    if-eqz p1, :cond_2

    .line 1762
    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getCode()I

    move-result p1

    int-to-long v0, p1

    :cond_2
    move-wide v4, v0

    .line 1764
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/q$8;->a:Lcom/bytedance/sdk/openadsdk/core/p$a;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v6

    invoke-interface/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/core/p$a;->a(ZJJ)V

    :cond_3
    :goto_1
    return-void
.end method
