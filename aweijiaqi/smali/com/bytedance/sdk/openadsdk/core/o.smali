.class public Lcom/bytedance/sdk/openadsdk/core/o;
.super Ljava/lang/Object;
.source "InternalContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/o$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/e/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:Lcom/bytedance/sdk/openadsdk/e/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:Lcom/bytedance/sdk/openadsdk/e/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:Lcom/bytedance/sdk/openadsdk/core/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "Lcom/bytedance/sdk/openadsdk/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:Lcom/bytedance/sdk/openadsdk/q/a;

.field private static volatile f:Lcom/bytedance/sdk/openadsdk/k/c/a;

.field private static volatile g:Landroid/content/Context;

.field private static volatile h:Lcom/bytedance/sdk/openadsdk/core/j/h;

.field private static final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Landroid/content/Context;
    .locals 1

    .line 54
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 55
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V

    .line 57
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/e/b;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 180
    new-instance p2, Lcom/bytedance/sdk/openadsdk/e/n;

    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/bytedance/sdk/openadsdk/e/n;-><init>(Landroid/content/Context;)V

    .line 181
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/g$b;->a()Lcom/bytedance/sdk/openadsdk/e/g$b;

    move-result-object v0

    goto :goto_0

    .line 183
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/g$b;->b()Lcom/bytedance/sdk/openadsdk/e/g$b;

    move-result-object v0

    .line 184
    new-instance p2, Lcom/bytedance/sdk/openadsdk/e/l;

    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-direct {p2, v1}, Lcom/bytedance/sdk/openadsdk/e/l;-><init>(Landroid/content/Context;)V

    .line 186
    :goto_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/o;->b(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/e/g$a;

    move-result-object v1

    .line 187
    new-instance v9, Lcom/bytedance/sdk/openadsdk/e/b;

    const/4 v10, 0x0

    new-instance v11, Lcom/bytedance/sdk/openadsdk/e/o;

    const/4 v6, 0x0

    move-object v2, v11

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v7, v0

    move-object v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/e/o;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/e;Lcom/bytedance/sdk/openadsdk/core/p;Lcom/bytedance/sdk/openadsdk/e/g$b;Lcom/bytedance/sdk/openadsdk/e/g$a;)V

    move-object v2, v9

    move-object v3, p2

    move-object v4, v10

    move-object v5, v0

    move-object v6, v1

    move-object v7, v11

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b;-><init>(Lcom/bytedance/sdk/openadsdk/e/e;Lcom/bytedance/sdk/openadsdk/core/p;Lcom/bytedance/sdk/openadsdk/e/g$b;Lcom/bytedance/sdk/openadsdk/e/g$a;Lcom/bytedance/sdk/openadsdk/e/g;)V

    return-object v9
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/bytedance/sdk/openadsdk/core/o;

    monitor-enter v0

    .line 61
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    if-nez v1, :cond_1

    if-eqz p0, :cond_0

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 64
    monitor-exit v0

    return-void

    .line 67
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o$a;->a()Landroid/app/Application;

    move-result-object p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p0, :cond_1

    .line 69
    :try_start_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o$a;->a()Landroid/app/Application;

    move-result-object p0

    sput-object p0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    .line 70
    sget-object p0, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p0, :cond_1

    .line 71
    monitor-exit v0

    return-void

    .line 77
    :catchall_0
    :cond_1
    monitor-exit v0

    return-void

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static b(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/e/g$a;
    .locals 1

    .line 193
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/o$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/o$1;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    .line 80
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    .line 81
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    .line 82
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    return-void
.end method

.method public static c()Lcom/bytedance/sdk/openadsdk/e/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/e/a;",
            ">;"
        }
    .end annotation

    .line 86
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/b;->c()Lcom/bytedance/sdk/openadsdk/e/b$a;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v0, :cond_3

    .line 90
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/o;

    monitor-enter v0

    .line 91
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v1, :cond_2

    .line 92
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/c;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/e/c;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    goto :goto_0

    .line 95
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/b;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/e/f;

    sget-object v3, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/e/f;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    move-result-object v3

    .line 98
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->l()Lcom/bytedance/sdk/openadsdk/e/g$b;

    move-result-object v4

    sget-object v5, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    .line 99
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/o;->b(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/e/g$a;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b;-><init>(Lcom/bytedance/sdk/openadsdk/e/e;Lcom/bytedance/sdk/openadsdk/core/p;Lcom/bytedance/sdk/openadsdk/e/g$b;Lcom/bytedance/sdk/openadsdk/e/g$a;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    .line 102
    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 104
    :cond_3
    :goto_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->a:Lcom/bytedance/sdk/openadsdk/e/b;

    return-object v0
.end method

.method public static d()Lcom/bytedance/sdk/openadsdk/e/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation

    .line 108
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/b;->d()Lcom/bytedance/sdk/openadsdk/e/b$b;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->c:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v0, :cond_3

    .line 112
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/o;

    monitor-enter v0

    .line 113
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->c:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v1, :cond_2

    .line 114
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 115
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/m;

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/e/m;-><init>(Z)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->c:Lcom/bytedance/sdk/openadsdk/e/b;

    goto :goto_0

    :cond_1
    const-string v1, "ttad_bk_batch_stats"

    const-string v3, "AdStatsEventBatchThread"

    .line 117
    invoke-static {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/e/b;

    move-result-object v1

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->c:Lcom/bytedance/sdk/openadsdk/e/b;

    .line 120
    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 122
    :cond_3
    :goto_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->c:Lcom/bytedance/sdk/openadsdk/e/b;

    return-object v0
.end method

.method public static e()Lcom/bytedance/sdk/openadsdk/e/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/bytedance/sdk/openadsdk/e/b<",
            "Lcom/bytedance/sdk/openadsdk/k/c/c$a;",
            ">;"
        }
    .end annotation

    .line 126
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/b;->d()Lcom/bytedance/sdk/openadsdk/e/b$b;

    move-result-object v0

    return-object v0

    .line 129
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->b:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v0, :cond_3

    .line 130
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/o;

    monitor-enter v0

    .line 131
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->b:Lcom/bytedance/sdk/openadsdk/e/b;

    if-nez v1, :cond_2

    .line 132
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 133
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/m;

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/e/m;-><init>(Z)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->b:Lcom/bytedance/sdk/openadsdk/e/b;

    goto :goto_0

    :cond_1
    const-string v1, "ttad_bk_stats"

    const-string v3, "AdStatsEventThread"

    .line 135
    invoke-static {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/e/b;

    move-result-object v1

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->b:Lcom/bytedance/sdk/openadsdk/e/b;

    .line 138
    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 140
    :cond_3
    :goto_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->b:Lcom/bytedance/sdk/openadsdk/e/b;

    return-object v0
.end method

.method public static f()Lcom/bytedance/sdk/openadsdk/core/p;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "Lcom/bytedance/sdk/openadsdk/e/a;",
            ">;"
        }
    .end annotation

    .line 144
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    if-nez v0, :cond_1

    .line 145
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/o;

    monitor-enter v0

    .line 146
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    if-nez v1, :cond_0

    .line 147
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/q;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/q;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 149
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 151
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    return-object v0
.end method

.method public static g()Lcom/bytedance/sdk/openadsdk/q/a;
    .locals 5

    .line 155
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/q/b;->c()Lcom/bytedance/sdk/openadsdk/q/a;

    move-result-object v0

    return-object v0

    .line 158
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    if-nez v0, :cond_3

    .line 159
    const-class v0, Lcom/bytedance/sdk/openadsdk/q/a;

    monitor-enter v0

    .line 160
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    if-nez v1, :cond_2

    .line 161
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    new-instance v1, Lcom/bytedance/sdk/openadsdk/q/c;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/q/c;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    goto :goto_0

    .line 164
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/q/b;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    new-instance v3, Lcom/bytedance/sdk/openadsdk/q/g;

    sget-object v4, Lcom/bytedance/sdk/openadsdk/core/o;->g:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/bytedance/sdk/openadsdk/q/g;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/q/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/q/f;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    .line 167
    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 169
    :cond_3
    :goto_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->e:Lcom/bytedance/sdk/openadsdk/q/a;

    return-object v0
.end method

.method public static h()Lcom/bytedance/sdk/openadsdk/core/j/h;
    .locals 2

    .line 202
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->h:Lcom/bytedance/sdk/openadsdk/core/j/h;

    if-nez v0, :cond_1

    .line 203
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/j/h;

    monitor-enter v0

    .line 204
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->h:Lcom/bytedance/sdk/openadsdk/core/j/h;

    if-nez v1, :cond_0

    .line 205
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/j/h;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/j/h;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->h:Lcom/bytedance/sdk/openadsdk/core/j/h;

    .line 207
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 209
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->h:Lcom/bytedance/sdk/openadsdk/core/j/h;

    return-object v0
.end method

.method public static i()Lcom/bytedance/sdk/openadsdk/k/c/a;
    .locals 2

    .line 225
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/c/c;->c()Lcom/bytedance/sdk/openadsdk/k/c/a;

    move-result-object v0

    return-object v0

    .line 228
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    if-nez v0, :cond_3

    .line 229
    const-class v0, Lcom/bytedance/sdk/openadsdk/k/c/c;

    monitor-enter v0

    .line 230
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    if-nez v1, :cond_2

    .line 231
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 232
    new-instance v1, Lcom/bytedance/sdk/openadsdk/k/c/d;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/k/c/d;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    goto :goto_0

    .line 234
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/k/c/c;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/k/c/c;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    .line 237
    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 239
    :cond_3
    :goto_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->f:Lcom/bytedance/sdk/openadsdk/k/c/a;

    return-object v0
.end method

.method public static j()V
    .locals 2

    .line 243
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 244
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_0
    return-void
.end method

.method public static k()Z
    .locals 1

    .line 249
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/o;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private static l()Lcom/bytedance/sdk/openadsdk/e/g$b;
    .locals 1

    .line 173
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/g$b;->a()Lcom/bytedance/sdk/openadsdk/e/g$b;

    move-result-object v0

    return-object v0
.end method
