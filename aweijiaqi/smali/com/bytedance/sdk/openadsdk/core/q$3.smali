.class Lcom/bytedance/sdk/openadsdk/core/q$3;
.super Lcom/bytedance/sdk/component/net/callback/NetCallback;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/q;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;ILcom/bytedance/sdk/openadsdk/core/p$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/p$b;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field final synthetic c:I

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/core/e/n;

.field final synthetic e:Lcom/bytedance/sdk/openadsdk/core/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/p$b;Lcom/bytedance/sdk/openadsdk/AdSlot;ILcom/bytedance/sdk/openadsdk/core/e/n;)V
    .locals 0

    .line 428
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->d:Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-direct {p0}, Lcom/bytedance/sdk/component/net/callback/NetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Ljava/io/IOException;)V
    .locals 13

    .line 522
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->getExtraMap()Ljava/util/Map;

    move-result-object p1

    const-string v0, "extra_time_start"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 523
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/16 p1, 0x25a

    if-eqz p2, :cond_0

    .line 526
    const-class v4, Ljava/net/SocketTimeoutException;

    invoke-virtual {p2}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 527
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Lcom/bytedance/sdk/openadsdk/core/p$b;->a(ILjava/lang/String;)V

    .line 529
    :cond_0
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Lcom/bytedance/sdk/openadsdk/core/p$b;->a(ILjava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "onFailure: "

    aput-object v5, p1, v4

    const/4 v4, 0x1

    const/16 v5, 0x259

    .line 530
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const-string v4, "NetApiImpl"

    invoke-static {v4, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    sub-long v6, v2, v0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v8

    iget v9, p0, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    const/4 v10, 0x0

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    const/16 v11, 0x259

    invoke-static/range {v5 .. v12}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    return-void
.end method

.method public onResponse(Lcom/bytedance/sdk/component/net/executor/NetExecutor;Lcom/bytedance/sdk/component/net/NetResponse;)V
    .locals 30

    move-object/from16 v1, p0

    if-eqz p2, :cond_6

    .line 433
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 438
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->getExtraMap()Ljava/util/Map;

    move-result-object v5

    const-string v6, "extra_time_start"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    .line 442
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 444
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 445
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-static {v6, v5}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v15

    if-nez v15, :cond_0

    .line 447
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/p$b;)V

    .line 449
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v4

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v6

    iget v7, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    const/4 v8, 0x0

    const/4 v9, -0x1

    const-string v10, "mate parse_fail"

    invoke-static/range {v3 .. v10}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    return-void

    .line 454
    :cond_0
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->d:Lcom/bytedance/sdk/openadsdk/core/e/n;

    invoke-static {v15, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/q$a;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/e/n;)Lcom/bytedance/sdk/openadsdk/core/q$a;

    move-result-object v14

    .line 456
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->i:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 457
    iget v5, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->d:I

    const/16 v6, 0x4e20

    if-eq v5, v6, :cond_1

    .line 459
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    iget v2, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->d:I

    iget-object v3, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->e:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/p$b;->a(ILjava/lang/String;)V

    .line 462
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v8

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v10

    iget v11, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    iget v13, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->d:I

    iget v0, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->f:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    move-object v12, v14

    move-object v14, v0

    invoke-static/range {v7 .. v14}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    return-void

    .line 466
    :cond_1
    iget-object v5, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    if-nez v5, :cond_2

    .line 467
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/p$b;)V

    .line 469
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v8

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v10

    iget v11, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    const/4 v13, -0x1

    const-string v0, "parse_fail"

    move-object v12, v14

    move-object v14, v0

    invoke-static/range {v7 .. v14}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    return-void

    .line 473
    :cond_2
    iget-object v5, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v15}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c(Ljava/lang/String;)V

    .line 474
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 475
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    iget-object v6, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-interface {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/p$b;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)V

    .line 478
    iget-object v5, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/e/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/a;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 480
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a;->a()Lcom/bytedance/sdk/openadsdk/f/a;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/bytedance/sdk/openadsdk/f/a;->a(Ljava/util/Map;)V

    :cond_3
    if-eqz v0, :cond_4

    .line 485
    iget-object v0, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    .line 486
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 487
    iget-object v0, v14, Lcom/bytedance/sdk/openadsdk/core/q$a;->h:Lcom/bytedance/sdk/openadsdk/core/e/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 488
    iget v4, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 489
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->d:Lcom/bytedance/sdk/openadsdk/core/e/n;

    move-wide v7, v2

    move-wide/from16 v9, v16

    move-object v11, v14

    move-wide/from16 v12, v18

    move-object/from16 v20, v14

    move-object v14, v0

    move-object v0, v15

    move-object v15, v4

    invoke-static/range {v5 .. v15}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/e/n;JJLcom/bytedance/sdk/openadsdk/core/q$a;JLcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    .line 490
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->d:Lcom/bytedance/sdk/openadsdk/core/e/n;

    iget-wide v5, v5, Lcom/bytedance/sdk/openadsdk/core/e/n;->f:J

    sub-long v22, v2, v5

    move-object/from16 v5, v20

    iget v5, v5, Lcom/bytedance/sdk/openadsdk/core/q$a;->a:I

    int-to-long v5, v5

    sub-long v26, v16, v2

    sub-long v28, v18, v16

    move-object/from16 v21, v4

    move-wide/from16 v24, v5

    invoke-static/range {v21 .. v29}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JJJJ)V

    goto :goto_1

    :cond_4
    move-object v0, v15

    .line 495
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/b;->a()Lcom/bytedance/sdk/openadsdk/a/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/a/b;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    const-string v2, "NetApiImpl"

    const-string v3, "get ad error: "

    .line 498
    invoke-static {v2, v3, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 499
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;Lcom/bytedance/sdk/openadsdk/core/p$b;)V

    .line 502
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v4

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v6

    iget v7, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    const/4 v8, 0x0

    const/4 v9, -0x1

    const-string v10, "parse_fail"

    invoke-static/range {v3 .. v10}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    goto :goto_2

    .line 507
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getCode()I

    move-result v0

    .line 508
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 510
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->a:Lcom/bytedance/sdk/openadsdk/core/p$b;

    invoke-interface {v3, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/p$b;->a(ILjava/lang/String;)V

    .line 513
    iget-object v11, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->e:Lcom/bytedance/sdk/openadsdk/core/q;

    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/component/net/NetResponse;->getDuration()J

    move-result-wide v12

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v14

    iget v15, v1, Lcom/bytedance/sdk/openadsdk/core/q$3;->c:I

    const/16 v16, 0x0

    move/from16 v17, v0

    move-object/from16 v18, v2

    invoke-static/range {v11 .. v18}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/core/q;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/q$a;ILjava/lang/String;)V

    :cond_6
    :goto_2
    return-void
.end method
