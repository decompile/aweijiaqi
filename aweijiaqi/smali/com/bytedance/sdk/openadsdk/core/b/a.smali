.class public Lcom/bytedance/sdk/openadsdk/core/b/a;
.super Lcom/bytedance/sdk/openadsdk/core/b/b;
.source "ClickCreativeListener.java"


# instance fields
.field private D:Z

.field private E:Z

.field private F:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;

.field private G:I

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/b/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V

    const/4 p1, 0x1

    .line 36
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->a:Z

    const/4 p1, 0x0

    .line 37
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->D:Z

    .line 39
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->E:Z

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .line 148
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "feed_video_middle_page"

    .line 151
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 262
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const-string v1, "splash_ad"

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "slide_banner_ad"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_1
    const-string v0, "draw_ad"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "interaction"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_3
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    :sswitch_4
    const-string v0, "embeded_ad"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :sswitch_5
    const-string v0, "banner_ad"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_4

    if-eq p1, v6, :cond_4

    const-string v0, "banner_call"

    if-eq p1, v5, :cond_3

    if-eq p1, v4, :cond_3

    if-eq p1, v3, :cond_2

    if-eq p1, v2, :cond_1

    const-string p1, ""

    return-object p1

    :cond_1
    return-object v1

    :cond_2
    const-string p1, "interaction_call"

    return-object p1

    :cond_3
    return-object v0

    :cond_4
    const-string p1, "feed_call"

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x65146dea -> :sswitch_5
        -0x2a77c376 -> :sswitch_4
        0xa6dd8fb -> :sswitch_3
        0x6deace12 -> :sswitch_2
        0x72060cfe -> :sswitch_1
        0x7cab2108 -> :sswitch_0
    .end sparse-switch
.end method

.method private c(Landroid/view/View;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 191
    :cond_0
    instance-of v1, p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView;

    const-string v2, "ClickCreativeListener"

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    const-string p1, "NativeVideoTsView...."

    .line 192
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    .line 196
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_ad_cover_center_layout"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eq v1, v4, :cond_7

    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_ad_logo_image"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eq v1, v4, :cond_7

    .line 198
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_btn_ad_image_tv"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eq v1, v4, :cond_7

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_ad_name"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eq v1, v4, :cond_7

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_ad_button"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-ne v1, v4, :cond_2

    goto :goto_3

    .line 206
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_root_view"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eq v1, v4, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    const-string v5, "tt_video_play"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-ne v1, v4, :cond_3

    goto :goto_2

    .line 210
    :cond_3
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    .line 211
    :goto_0
    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 212
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/core/b/a;->c(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    :goto_1
    return v0

    :cond_6
    :goto_2
    const-string p1, "tt_root_view...."

    .line 207
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v3

    :cond_7
    :goto_3
    const-string p1, "tt_video_ad_cover_center_layout...."

    .line 202
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method

.method private g()Z
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->h()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private h()Z
    .locals 1

    .line 223
    instance-of v0, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private i()Z
    .locals 5

    .line 233
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 236
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 242
    :cond_2
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->G:I

    if-nez v0, :cond_3

    .line 243
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->c(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->G:I

    .line 247
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "!isViewVisibility()="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->b()Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ",isAutoPlay()="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ",!isCoverPageVisibility()="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->c()Z

    move-result v2

    xor-int/2addr v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ClickCreativeListener"

    invoke-static {v2, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->G:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->c()Z

    move-result v0

    if-nez v0, :cond_4

    return v1

    .line 252
    :cond_4
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->G:I

    if-eq v0, v3, :cond_5

    const/4 v4, 0x2

    if-eq v0, v4, :cond_5

    if-eq v0, v2, :cond_5

    return v1

    :cond_5
    return v3
.end method

.method private j()V
    .locals 2

    .line 301
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->E:Z

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;IIII)V
    .locals 32

    move-object/from16 v12, p0

    const/4 v13, 0x2

    .line 59
    invoke-virtual {v12, v13}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 63
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->i()Z

    move-result v0

    const-string v1, "ClickCreativeListener"

    if-eqz v0, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->E:Z

    if-nez v0, :cond_1

    const-string v0, "\u62e6\u622a\u539f\u751f\u89c6\u9891view\u8d70\u666e\u901a\u70b9\u51fb\u4e8b\u4ef6....."

    .line 64
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/core/b/b;->a(Landroid/view/View;IIII)V

    return-void

    .line 68
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->j()V

    const-string v0, "\u8d70\u521b\u610f\u533a\u57df\u70b9\u51fb\u4e8b\u4ef6....."

    .line 69
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 72
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    .line 74
    :cond_2
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    if-nez v0, :cond_3

    return-void

    .line 77
    :cond_3
    iget-wide v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->w:J

    iget-wide v7, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->x:J

    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->f:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-nez v0, :cond_4

    move-object v9, v1

    goto :goto_0

    :cond_4
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->f:Ljava/lang/ref/WeakReference;

    .line 78
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v9, v0

    :goto_0
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->g:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_5

    move-object v10, v1

    goto :goto_1

    :cond_5
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->g:Ljava/lang/ref/WeakReference;

    .line 79
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v10, v0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/b/a;->f()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    .line 77
    invoke-virtual/range {v0 .. v11}, Lcom/bytedance/sdk/openadsdk/core/b/a;->a(IIIIJJLandroid/view/View;Landroid/view/View;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/f;

    move-result-object v0

    iput-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    .line 80
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    if-eq v0, v13, :cond_b

    const/4 v1, 0x3

    if-eq v0, v1, :cond_b

    const/4 v1, 0x4

    if-eq v0, v1, :cond_8

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v0, -0x1

    goto/16 :goto_2

    .line 109
    :cond_6
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    invoke-direct {v12, v1}, Lcom/bytedance/sdk/openadsdk/core/b/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 110
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 111
    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    const/4 v7, 0x1

    iget-object v8, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v3, "click_call"

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    .line 114
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ae()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    .line 115
    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v7, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    iget-object v9, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v4, "click"

    invoke-static/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    goto/16 :goto_2

    .line 91
    :cond_8
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    if-nez v1, :cond_9

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-nez v1, :cond_9

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    if-eqz v1, :cond_a

    :cond_9
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    const-string v2, "feed_video_middle_page"

    .line 92
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 93
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 94
    iget-object v13, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v14, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v15, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->e:I

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    const/16 v21, 0x1

    iget-object v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    iget-boolean v7, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->r:Z

    iget-object v8, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    .line 95
    invoke-direct {v12, v8}, Lcom/bytedance/sdk/openadsdk/core/b/a;->b(Ljava/lang/String;)Z

    move-result v24

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v19, v4

    move-object/from16 v20, v5

    move-object/from16 v22, v6

    move/from16 v23, v7

    .line 94
    invoke-static/range {v13 .. v24}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/TTNativeAd;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;ZLjava/util/Map;ZZ)Z

    move-result v30

    .line 96
    iget-boolean v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->a:Z

    if-eqz v1, :cond_e

    .line 97
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v26, "click"

    move-object/from16 v25, v1

    move-object/from16 v27, v2

    move-object/from16 v28, v3

    move-object/from16 v29, v4

    move-object/from16 v31, v5

    invoke-static/range {v25 .. v31}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    goto/16 :goto_2

    .line 100
    :cond_a
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v1, :cond_e

    .line 101
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->g()V

    .line 102
    iget-boolean v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->a:Z

    if-eqz v1, :cond_e

    .line 103
    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    const/4 v7, 0x1

    iget-object v8, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v3, "click"

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    goto :goto_2

    .line 121
    :cond_b
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    if-nez v1, :cond_c

    iget-boolean v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->D:Z

    if-eqz v1, :cond_d

    .line 122
    :cond_c
    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    const/4 v7, 0x1

    iget-object v8, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v3, "click_button"

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    :cond_d
    const/4 v1, 0x1

    .line 125
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Z)V

    .line 126
    iget-object v13, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v14, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v15, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->e:I

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    iget v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->e:I

    .line 127
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->a(I)Ljava/lang/String;

    move-result-object v19

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    const/16 v21, 0x1

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    iget-boolean v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->r:Z

    iget-object v7, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    invoke-direct {v12, v7}, Lcom/bytedance/sdk/openadsdk/core/b/a;->b(Ljava/lang/String;)Z

    move-result v24

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v20, v4

    move-object/from16 v22, v5

    move/from16 v23, v6

    .line 126
    invoke-static/range {v13 .. v24}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/TTNativeAd;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;ZLjava/util/Map;ZZ)Z

    move-result v30

    .line 128
    iget-boolean v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->a:Z

    if-eqz v1, :cond_e

    .line 129
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->d:Ljava/lang/String;

    iget-object v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->n:Ljava/util/Map;

    const-string v26, "click"

    move-object/from16 v25, v1

    move-object/from16 v27, v2

    move-object/from16 v28, v3

    move-object/from16 v29, v4

    move-object/from16 v31, v5

    invoke-static/range {v25 .. v31}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    .line 136
    :cond_e
    :goto_2
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->i:Lcom/bytedance/sdk/openadsdk/core/b/b$a;

    if-eqz v1, :cond_f

    .line 137
    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->i:Lcom/bytedance/sdk/openadsdk/core/b/b$a;

    move-object/from16 v2, p1

    invoke-interface {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/core/b/b$a;->a(Landroid/view/View;I)V

    .line 140
    :cond_f
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 141
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/a;->F:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;

    if-eqz v0, :cond_10

    .line 142
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;->onClick()V

    :cond_10
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->F:Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->a:Z

    return-void
.end method

.method protected a()Z
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->d(Ljava/lang/String;)I

    move-result v0

    .line 161
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->d(I)I

    move-result v0

    if-eq v0, v1, :cond_8

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    return v1

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1

    :cond_4
    return v3

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    :cond_7
    :goto_1
    return v1

    .line 167
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->d(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 0

    .line 55
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->D:Z

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 0

    .line 293
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/a;->E:Z

    return-void
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
