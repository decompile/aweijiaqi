.class public Lcom/bytedance/sdk/openadsdk/core/b/b;
.super Lcom/bytedance/sdk/openadsdk/core/b/c;
.source "ClickListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/b/b$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field protected b:Landroid/content/Context;

.field protected final c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field protected final d:Ljava/lang/String;

.field protected final e:I

.field protected f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected h:Lcom/bytedance/sdk/openadsdk/core/e/f;

.field protected i:Lcom/bytedance/sdk/openadsdk/core/b/b$a;

.field protected j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

.field protected k:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;

.field protected l:Z

.field protected m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field protected n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

.field protected p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

.field protected q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;

.field protected r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)V
    .locals 1

    .line 103
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/b/c;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->l:Z

    .line 64
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->r:Z

    .line 105
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    .line 106
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 107
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    .line 108
    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->e:I

    .line 109
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .line 179
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "feed_video_middle_page"

    .line 182
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method


# virtual methods
.method protected a(IIIIJJLandroid/view/View;Landroid/view/View;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/f;
    .locals 1

    .line 194
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;-><init>()V

    .line 195
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 196
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 197
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 198
    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 199
    invoke-virtual {p1, p5, p6}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 200
    invoke-virtual {p1, p7, p8}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(J)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 201
    invoke-static {p9}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;)[I

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->b([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 202
    invoke-static {p10}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;)[I

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 203
    invoke-static {p9}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/view/View;)[I

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->c([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 204
    invoke-static {p10}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/view/View;)[I

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->d([I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget p2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->y:I

    .line 205
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->f(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget p2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->z:I

    .line 206
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->g(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget p2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->A:I

    .line 207
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->h(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->C:Landroid/util/SparseArray;

    .line 208
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(Landroid/util/SparseArray;)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 209
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/h;->b()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 210
    invoke-virtual {p1, p11}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/f$a;

    move-result-object p1

    .line 211
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/f$a;->a()Lcom/bytedance/sdk/openadsdk/core/e/f;

    move-result-object p1

    return-object p1
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .line 117
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->f:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 32

    move-object/from16 v12, p0

    const/4 v0, 0x1

    .line 134
    invoke-virtual {v12, v0}, Lcom/bytedance/sdk/openadsdk/core/b/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 138
    :cond_0
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 139
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    .line 141
    :cond_1
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    return-void

    .line 145
    :cond_2
    iget-wide v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->w:J

    iget-wide v7, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->x:J

    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->f:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-nez v0, :cond_3

    move-object v9, v1

    goto :goto_0

    .line 146
    :cond_3
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v9, v0

    :goto_0
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->g:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_4

    move-object v10, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v10, v0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/b/b;->f()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    .line 145
    invoke-virtual/range {v0 .. v11}, Lcom/bytedance/sdk/openadsdk/core/b/b;->a(IIIIJJLandroid/view/View;Landroid/view/View;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/e/f;

    move-result-object v0

    iput-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    .line 148
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->i:Lcom/bytedance/sdk/openadsdk/core/b/b$a;

    if-eqz v0, :cond_5

    const/4 v1, -0x1

    move-object/from16 v2, p1

    .line 149
    invoke-interface {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/b/b$a;->a(Landroid/view/View;I)V

    .line 152
    :cond_5
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 153
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->e:I

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object/from16 v19, v0

    .line 154
    iget-object v13, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    iget-object v14, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v15, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->e:I

    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    iget-boolean v5, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->r:Z

    iget-object v6, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    .line 155
    invoke-direct {v12, v6}, Lcom/bytedance/sdk/openadsdk/core/b/b;->b(Ljava/lang/String;)Z

    move-result v24

    move-object/from16 v16, v0

    move-object/from16 v17, v1

    move-object/from16 v18, v2

    move-object/from16 v20, v3

    move-object/from16 v22, v4

    move/from16 v23, v5

    .line 154
    invoke-static/range {v13 .. v24}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/TTNativeAd;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;ZLjava/util/Map;ZZ)Z

    move-result v30

    if-nez v30, :cond_7

    .line 156
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_7

    .line 157
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 158
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/g;->c()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    return-void

    .line 163
    :cond_7
    iget-object v0, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    iget-object v1, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->h:Lcom/bytedance/sdk/openadsdk/core/e/f;

    iget-object v3, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    iget-object v4, v12, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    const-string v26, "click"

    move-object/from16 v25, v0

    move-object/from16 v27, v1

    move-object/from16 v28, v2

    move-object/from16 v29, v3

    move-object/from16 v31, v4

    invoke-static/range {v25 .. v31}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/core/e/f;Ljava/lang/String;ZLjava/util/Map;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTNativeAd;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/b/b$a;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->i:Lcom/bytedance/sdk/openadsdk/core/b/b$a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->k:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 126
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    goto :goto_0

    .line 128
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :goto_0
    return-void
.end method

.method protected a(I)Z
    .locals 6

    .line 221
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    new-array v2, v0, [I

    new-array v0, v0, [I

    .line 224
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->g:Ljava/lang/ref/WeakReference;

    if-eqz v3, :cond_0

    .line 225
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->a(Landroid/view/View;)[I

    move-result-object v2

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/q;->c(Landroid/view/View;)[I

    move-result-object v0

    .line 228
    :cond_0
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    invoke-direct {v3}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;-><init>()V

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->s:I

    .line 229
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->t:I

    .line 230
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->u:I

    .line 231
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->v:I

    .line 232
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->w:J

    .line 233
    invoke-virtual {v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->b(J)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->x:J

    .line 234
    invoke-virtual {v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a(J)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    aget v4, v2, v1

    .line 235
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v3

    const/4 v4, 0x1

    aget v2, v2, v4

    .line 236
    invoke-virtual {v3, v2}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->f(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v2

    aget v1, v0, v1

    .line 237
    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->g(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v1

    aget v0, v0, v4

    .line 238
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->h(I)Lcom/bytedance/sdk/openadsdk/core/e/k$a;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/k$a;->a()Lcom/bytedance/sdk/openadsdk/core/e/k;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;

    invoke-interface {v1, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/a;->a(ILcom/bytedance/sdk/openadsdk/core/e/k;)V

    return v4

    :cond_1
    return v1
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .line 121
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->g:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public d()Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    return-object v0
.end method

.method public d(Z)V
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->r:Z

    return-void
.end method

.method public e()V
    .locals 13

    .line 169
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 172
    :cond_0
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/e/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 173
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->e:I

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v7, v0

    .line 174
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->e:I

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->j:Lcom/bytedance/sdk/openadsdk/TTNativeAd;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->o:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->p:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->m:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->n:Ljava/util/Map;

    iget-boolean v11, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->r:Z

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->d:Ljava/lang/String;

    .line 175
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/b/b;->b(Ljava/lang/String;)Z

    move-result v12

    .line 174
    invoke-static/range {v1 .. v12}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;ILcom/bytedance/sdk/openadsdk/TTNativeAd;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;ZLjava/util/Map;ZZ)Z

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->l:Z

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/b/b;->a:Ljava/lang/String;

    return-object v0
.end method
