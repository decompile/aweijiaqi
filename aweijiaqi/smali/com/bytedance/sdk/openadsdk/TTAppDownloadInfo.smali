.class public Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;
.super Ljava/lang/Object;
.source "TTAppDownloadInfo.java"


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrBytes()J
    .locals 2

    .line 40
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->d:J

    return-wide v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .line 16
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->a:J

    return-wide v0
.end method

.method public getInternalStatusKey()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->b:I

    return v0
.end method

.method public getTotalBytes()J
    .locals 2

    .line 32
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->c:J

    return-wide v0
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->f:Ljava/lang/String;

    return-void
.end method

.method public setCurrBytes(J)V
    .locals 0

    .line 44
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->d:J

    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->e:Ljava/lang/String;

    return-void
.end method

.method public setId(J)V
    .locals 0

    .line 20
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->a:J

    return-void
.end method

.method public setInternalStatusKey(I)V
    .locals 0

    .line 28
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->b:I

    return-void
.end method

.method public setTotalBytes(J)V
    .locals 0

    .line 36
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/TTAppDownloadInfo;->c:J

    return-void
.end method
