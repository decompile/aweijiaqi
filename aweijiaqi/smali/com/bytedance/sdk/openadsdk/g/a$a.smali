.class final Lcom/bytedance/sdk/openadsdk/g/a$a;
.super Ljava/lang/Object;
.source "ImageLoaderWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/g/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/g/a$a;->a(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Lcom/bytedance/sdk/component/d/e;)Lcom/bytedance/sdk/component/d/e;
    .locals 1

    .line 123
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Lcom/bytedance/sdk/openadsdk/g/a$b;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/g/a$b;-><init>()V

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/component/d/e;->a(Lcom/bytedance/sdk/component/d/k;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/g/a$a;->b(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/g/a$a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    return-object p0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 2

    .line 55
    new-instance v0, Lcom/bytedance/sdk/component/d/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/d/o$a;-><init>()V

    .line 56
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/d/o$a;->a(Ljava/util/concurrent/ExecutorService;)Lcom/bytedance/sdk/component/d/o$a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/g/a$a$1;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/g/a$a$1;-><init>()V

    .line 57
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/d/o$a;->a(Lcom/bytedance/sdk/component/d/c;)Lcom/bytedance/sdk/component/d/o$a;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/d/o$a;->a()Lcom/bytedance/sdk/component/d/o;

    move-result-object v0

    .line 55
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/d/l;->a(Landroid/content/Context;Lcom/bytedance/sdk/component/d/o;)V

    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/core/e/l;)Lcom/bytedance/sdk/component/d/e;
    .locals 2

    .line 110
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/d/l;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->b()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/d/e;->a(I)Lcom/bytedance/sdk/component/d/e;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/component/d/e;->b(I)Lcom/bytedance/sdk/component/d/e;

    move-result-object v0

    .line 113
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->g()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/component/d/e;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    .line 114
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/g/a$a;->a(Lcom/bytedance/sdk/component/d/e;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    return-object p0
.end method

.method private static b(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;
    .locals 0

    .line 105
    invoke-static {p0}, Lcom/bytedance/sdk/component/d/l;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    .line 106
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/g/a$a;->a(Lcom/bytedance/sdk/component/d/e;)Lcom/bytedance/sdk/component/d/e;

    move-result-object p0

    return-object p0
.end method
