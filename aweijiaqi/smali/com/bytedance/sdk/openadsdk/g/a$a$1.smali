.class final Lcom/bytedance/sdk/openadsdk/g/a$a$1;
.super Ljava/lang/Object;
.source "ImageLoaderWrapper.java"

# interfaces
.implements Lcom/bytedance/sdk/component/d/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/g/a$a;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/d/b/c;)Lcom/bytedance/sdk/component/d/b/d;
    .locals 7

    .line 60
    new-instance v0, Lcom/bytedance/sdk/component/b/b/w;

    invoke-direct {v0}, Lcom/bytedance/sdk/component/b/b/w;-><init>()V

    .line 61
    new-instance v1, Lcom/bytedance/sdk/component/b/b/z$a;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/b/b/z$a;-><init>()V

    .line 62
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/b/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/b/b/z$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/z$a;->a()Lcom/bytedance/sdk/component/b/b/z$a;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/b/b/z$a;->d()Lcom/bytedance/sdk/component/b/b/z;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 67
    :try_start_0
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/b/b/w;->a(Lcom/bytedance/sdk/component/b/b/z;)Lcom/bytedance/sdk/component/b/b/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/component/b/b/e;->b()Lcom/bytedance/sdk/component/b/b/ab;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 71
    :try_start_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/b/c;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 73
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->g()Lcom/bytedance/sdk/component/b/b/s;

    move-result-object p1

    .line 74
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 76
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/b/b/s;->a()I

    move-result v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_1

    .line 78
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/component/b/b/s;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 79
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/component/b/b/s;->b(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_0

    .line 82
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 87
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/component/d/b/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->c()I

    move-result v1

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->h()Lcom/bytedance/sdk/component/b/b/ac;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/b/b/ac;->e()[B

    move-result-object v4

    const-string v5, ""

    invoke-direct {p1, v1, v4, v5, v3}, Lcom/bytedance/sdk/component/d/b/d;-><init>(ILjava/lang/Object;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 94
    :try_start_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/b/b/ab;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_2
    return-object p1

    :catchall_0
    move-exception p1

    move-object v3, v0

    goto :goto_1

    :catchall_1
    move-exception p1

    :goto_1
    :try_start_3
    const-string v0, "ImageLoaderWrapper"

    .line 89
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/bytedance/sdk/component/d/b/d;

    const-string v1, "net failed"

    invoke-direct {v0, v2, p1, v1}, Lcom/bytedance/sdk/component/d/b/d;-><init>(ILjava/lang/Object;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v3, :cond_3

    .line 94
    :try_start_4
    invoke-virtual {v3}, Lcom/bytedance/sdk/component/b/b/ab;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    :cond_3
    return-object v0

    :catchall_2
    move-exception p1

    if-eqz v3, :cond_4

    :try_start_5
    invoke-virtual {v3}, Lcom/bytedance/sdk/component/b/b/ab;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 96
    :catch_2
    :cond_4
    throw p1
.end method
