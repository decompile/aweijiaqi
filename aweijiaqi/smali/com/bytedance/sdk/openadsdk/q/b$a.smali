.class Lcom/bytedance/sdk/openadsdk/q/b$a;
.super Lcom/bytedance/sdk/component/e/g;
.source "TrackAdUrlImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/q/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/q/b;

.field private final b:Lcom/bytedance/sdk/openadsdk/q/e;

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/q/b;Lcom/bytedance/sdk/openadsdk/q/e;Ljava/lang/String;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    const-string p1, "AdsStats"

    .line 113
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 114
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    .line 115
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/q/b;Lcom/bytedance/sdk/openadsdk/q/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/q/b$1;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/q/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/q/b;Lcom/bytedance/sdk/openadsdk/q/e;Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 121
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "{TS}"

    .line 122
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "__TS__"

    if-nez v1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 124
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 125
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    const-string v0, "{UID}"

    .line 127
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "__UID__"

    if-nez v1, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 128
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 132
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/i;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "{OAID}"

    .line 133
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, "__OAID__"

    if-nez v2, :cond_4

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 134
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 135
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_5
    return-object p1
.end method


# virtual methods
.method a(Ljava/lang/String;)Z
    .locals 1

    .line 142
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 147
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :try_start_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const-string v1, "[ss_random]"

    .line 150
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "[ss_timestamp]"

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 153
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-object p1
.end method

.method public run()V
    .locals 3

    .line 162
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/q/b$a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->d()I

    move-result v0

    if-nez v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->a(Lcom/bytedance/sdk/openadsdk/q/b;)Lcom/bytedance/sdk/openadsdk/q/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/q/f;->c(Lcom/bytedance/sdk/openadsdk/q/e;)V

    return-void

    .line 173
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->d()I

    move-result v0

    if-lez v0, :cond_9

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 176
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->a(Lcom/bytedance/sdk/openadsdk/q/b;)Lcom/bytedance/sdk/openadsdk/q/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/q/f;->a(Lcom/bytedance/sdk/openadsdk/q/e;)V

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_2

    .line 182
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/q/b$a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/q/e;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 184
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/q/b$a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/NetClient;->getGetExecutor()Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    move-result-object v1

    .line 188
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->setUrl(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    .line 191
    :try_start_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->execute()Lcom/bytedance/sdk/component/net/NetResponse;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    nop

    :goto_1
    const-string v1, "trackurl"

    if-eqz v0, :cond_6

    .line 196
    :try_start_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 198
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->a(Lcom/bytedance/sdk/openadsdk/q/b;)Lcom/bytedance/sdk/openadsdk/q/f;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-interface {v0, v2}, Lcom/bytedance/sdk/openadsdk/q/f;->c(Lcom/bytedance/sdk/openadsdk/q/e;)V

    .line 199
    invoke-static {}, Lcom/bytedance/sdk/component/utils/j;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track success : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/q/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 204
    :cond_6
    invoke-static {}, Lcom/bytedance/sdk/component/utils/j;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track fail : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/q/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/q/e;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/q/e;->a(I)V

    .line 209
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/q/e;->d()I

    move-result v0

    if-nez v0, :cond_8

    .line 210
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->a(Lcom/bytedance/sdk/openadsdk/q/b;)Lcom/bytedance/sdk/openadsdk/q/f;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-interface {v0, v2}, Lcom/bytedance/sdk/openadsdk/q/f;->c(Lcom/bytedance/sdk/openadsdk/q/e;)V

    .line 211
    invoke-static {}, Lcom/bytedance/sdk/component/utils/j;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track fail and delete : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/q/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 216
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->a:Lcom/bytedance/sdk/openadsdk/q/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/q/b;->a(Lcom/bytedance/sdk/openadsdk/q/b;)Lcom/bytedance/sdk/openadsdk/q/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/q/b$a;->b:Lcom/bytedance/sdk/openadsdk/q/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/q/f;->b(Lcom/bytedance/sdk/openadsdk/q/e;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    :catchall_1
    nop

    goto/16 :goto_0

    :cond_9
    :goto_2
    return-void
.end method
