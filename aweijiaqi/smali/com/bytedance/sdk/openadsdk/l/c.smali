.class public Lcom/bytedance/sdk/openadsdk/l/c;
.super Ljava/lang/Object;
.source "StatsImageListener.java"

# interfaces
.implements Lcom/bytedance/sdk/component/d/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/bytedance/sdk/component/d/g<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Lcom/bytedance/sdk/openadsdk/k/a/c;


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz p1, :cond_0

    .line 61
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b()Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->a(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_1
    :goto_0
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 79
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p2, 0xc9

    .line 82
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    .line 83
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 84
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/a;->k(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/d/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/component/d/m<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .line 67
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 70
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/d/m;->a()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    .line 71
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    const/16 v0, 0xca

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object p1

    .line 72
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/k/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    .line 73
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/k/a;->k(Lcom/bytedance/sdk/openadsdk/k/a/c;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_1
    :goto_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_1
    :goto_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/c;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    if-nez v0, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/a/c;

    :cond_1
    :goto_0
    return-void
.end method
