.class public Lcom/bytedance/sdk/openadsdk/l/e;
.super Ljava/lang/Object;
.source "TTNetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/l/e$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/l/e;

.field private static c:Lcom/bytedance/sdk/component/adnet/face/IHttpStack;


# instance fields
.field private b:Landroid/content/Context;

.field private volatile d:Lcom/bytedance/sdk/component/adnet/core/l;

.field private e:Lcom/bytedance/sdk/component/adnet/b/b;

.field private f:Lcom/bytedance/sdk/component/adnet/b/d;

.field private g:Lcom/bytedance/sdk/openadsdk/l/a/b;

.field private final h:Lcom/bytedance/sdk/component/net/NetClient;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 74
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->b:Landroid/content/Context;

    .line 75
    new-instance p1, Lcom/bytedance/sdk/component/net/NetClient$Builder;

    invoke-direct {p1}, Lcom/bytedance/sdk/component/net/NetClient$Builder;-><init>()V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x2710

    .line 76
    invoke-virtual {p1, v1, v2, v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object p1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 77
    invoke-virtual {p1, v1, v2, v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object p1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 78
    invoke-virtual {p1, v1, v2, v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 79
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->enableTNC(Z)Lcom/bytedance/sdk/component/net/NetClient$Builder;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetClient$Builder;->build()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->h:Lcom/bytedance/sdk/component/net/NetClient;

    return-void
.end method

.method public static a()Lcom/bytedance/sdk/component/adnet/face/IHttpStack;
    .locals 1

    .line 46
    sget-object v0, Lcom/bytedance/sdk/openadsdk/l/e;->c:Lcom/bytedance/sdk/component/adnet/face/IHttpStack;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/widget/ImageView;II)Lcom/bytedance/sdk/openadsdk/l/e$a;
    .locals 1

    .line 162
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/e$a;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/l/e$a;-><init>(Landroid/widget/ImageView;Ljava/lang/String;II)V

    return-object v0
.end method

.method public static a(Lcom/bytedance/sdk/component/adnet/face/IHttpStack;)V
    .locals 0

    .line 50
    sput-object p0, Lcom/bytedance/sdk/openadsdk/l/e;->c:Lcom/bytedance/sdk/component/adnet/face/IHttpStack;

    return-void
.end method

.method public static b()Lcom/bytedance/sdk/openadsdk/l/e;
    .locals 3

    .line 63
    sget-object v0, Lcom/bytedance/sdk/openadsdk/l/e;->a:Lcom/bytedance/sdk/openadsdk/l/e;

    if-nez v0, :cond_1

    .line 64
    const-class v0, Lcom/bytedance/sdk/openadsdk/l/e;

    monitor-enter v0

    .line 65
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/l/e;->a:Lcom/bytedance/sdk/openadsdk/l/e;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/e;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/l/e;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/l/e;->a:Lcom/bytedance/sdk/openadsdk/l/e;

    .line 68
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 70
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/l/e;->a:Lcom/bytedance/sdk/openadsdk/l/e;

    return-object v0
.end method

.method private f()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->g:Lcom/bytedance/sdk/openadsdk/l/a/b;

    if-nez v0, :cond_0

    .line 243
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/a/b;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->d()Lcom/bytedance/sdk/component/adnet/core/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/a/b;-><init>(Lcom/bytedance/sdk/component/adnet/core/l;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->g:Lcom/bytedance/sdk/openadsdk/l/a/b;

    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .line 248
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->f:Lcom/bytedance/sdk/component/adnet/b/d;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lcom/bytedance/sdk/component/adnet/b/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->d()Lcom/bytedance/sdk/component/adnet/core/l;

    move-result-object v1

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a;->a()Lcom/bytedance/sdk/openadsdk/l/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/component/adnet/b/d;-><init>(Lcom/bytedance/sdk/component/adnet/core/l;Lcom/bytedance/sdk/component/adnet/b/d$b;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->f:Lcom/bytedance/sdk/component/adnet/b/d;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1

    const/4 v0, 0x0

    .line 139
    invoke-static {p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Ljava/lang/String;Landroid/widget/ImageView;II)Lcom/bytedance/sdk/openadsdk/l/e$a;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Ljava/lang/String;Landroid/widget/ImageView;Lcom/bytedance/sdk/component/adnet/b/d$e;)V

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/widget/ImageView;Lcom/bytedance/sdk/component/adnet/b/d$e;)V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->g()V

    .line 144
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/l/e;->f:Lcom/bytedance/sdk/component/adnet/b/d;

    invoke-virtual {p2, p1, p3}, Lcom/bytedance/sdk/component/adnet/b/d;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/adnet/b/d$e;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/sdk/component/adnet/b/b$a;)V
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->e:Lcom/bytedance/sdk/component/adnet/b/b;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/bytedance/sdk/component/adnet/b/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->d()Lcom/bytedance/sdk/component/adnet/core/l;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/component/adnet/b/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/component/adnet/core/l;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->e:Lcom/bytedance/sdk/component/adnet/b/b;

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->e:Lcom/bytedance/sdk/component/adnet/b/b;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/component/adnet/b/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/component/adnet/b/b$a;)V

    return-void
.end method

.method public c()Lcom/bytedance/sdk/component/net/NetClient;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->h:Lcom/bytedance/sdk/component/net/NetClient;

    return-object v0
.end method

.method public d()Lcom/bytedance/sdk/component/adnet/core/l;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->d:Lcom/bytedance/sdk/component/adnet/core/l;

    if-nez v0, :cond_1

    .line 97
    const-class v0, Lcom/bytedance/sdk/openadsdk/l/e;

    monitor-enter v0

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->d:Lcom/bytedance/sdk/component/adnet/core/l;

    if-nez v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/component/adnet/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/component/adnet/core/l;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/e;->d:Lcom/bytedance/sdk/component/adnet/core/l;

    .line 101
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 103
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->d:Lcom/bytedance/sdk/component/adnet/core/l;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/l/a/b;
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->f()V

    .line 130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/e;->g:Lcom/bytedance/sdk/openadsdk/l/a/b;

    return-object v0
.end method
