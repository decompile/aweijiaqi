.class public Lcom/bytedance/sdk/openadsdk/l/a/b;
.super Ljava/lang/Object;
.source "GifLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/l/a/b$a;,
        Lcom/bytedance/sdk/openadsdk/l/a/b$c;,
        Lcom/bytedance/sdk/openadsdk/l/a/b$d;,
        Lcom/bytedance/sdk/openadsdk/l/a/b$b;
    }
.end annotation


# static fields
.field public static volatile a:I

.field public static b:Z


# instance fields
.field private final c:Lcom/bytedance/sdk/component/adnet/core/l;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/l/a/b$d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/bytedance/sdk/openadsdk/core/e/t;

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/bytedance/sdk/component/adnet/core/l;)V
    .locals 2

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->d:Landroid/os/Handler;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->e:Ljava/util/Map;

    .line 53
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->c:Lcom/bytedance/sdk/component/adnet/core/l;

    return-void
.end method

.method public static a()Lcom/bytedance/sdk/openadsdk/l/a/b$a;
    .locals 1

    .line 290
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/a/b$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/l/a/b$a;-><init>()V

    return-object v0
.end method

.method private a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/a/c;
    .locals 9

    .line 172
    new-instance v8, Lcom/bytedance/sdk/openadsdk/l/a/c;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/l/a/b$4;

    invoke-direct {v2, p0, p5, p1}, Lcom/bytedance/sdk/openadsdk/l/a/b$4;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/b;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v0, v8

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/l/a/c;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/a/c$a;IILandroid/widget/ImageView$ScaleType;Landroid/graphics/Bitmap$Config;Ljava/lang/String;)V

    .line 211
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v8, p1}, Lcom/bytedance/sdk/openadsdk/l/a/c;->a(Lcom/bytedance/sdk/openadsdk/core/e/t;)V

    return-object v8
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/l/a/b;)Ljava/util/Map;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->e:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/l/a/b;Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V
    .locals 0

    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/l/a/b;->b(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/l/a/b;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/a/b$d;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/a/b$d;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/l/a/b;Z)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/a/b$d;)V
    .locals 5

    if-nez p3, :cond_0

    return-void

    .line 219
    :cond_0
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->a()Z

    move-result v0

    .line 220
    iget-object v1, p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->c:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 221
    iget-object v1, p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/l/a/b$b;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_2

    .line 224
    new-instance v3, Lcom/bytedance/sdk/openadsdk/l/a/b$c;

    iget-object v4, p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->e:Lcom/bytedance/sdk/openadsdk/l/a/d;

    invoke-direct {v3, v4, v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/l/a/b$c;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/d;Lcom/bytedance/sdk/openadsdk/l/a/b$b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/bytedance/sdk/openadsdk/l/a/b$b;->a(Lcom/bytedance/sdk/openadsdk/l/a/b$c;)V

    goto :goto_1

    .line 226
    :cond_2
    new-instance v3, Lcom/bytedance/sdk/openadsdk/l/a/b$c;

    iget-object v4, p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->d:Lcom/bytedance/sdk/component/adnet/err/VAdError;

    invoke-direct {v3, v4, v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/l/a/b$c;-><init>(Lcom/bytedance/sdk/component/adnet/err/VAdError;Lcom/bytedance/sdk/openadsdk/l/a/b$b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/bytedance/sdk/openadsdk/l/a/b$b;->b(Lcom/bytedance/sdk/openadsdk/l/a/b$c;)V

    .line 228
    :goto_1
    invoke-interface {v2}, Lcom/bytedance/sdk/openadsdk/l/a/b$b;->b()V

    goto :goto_0

    .line 232
    :cond_3
    iget-object p1, p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    :cond_4
    return-void
.end method

.method private a(Z)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    return-void

    .line 151
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->z()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->a(Z)V

    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V
    .locals 7

    if-nez p1, :cond_0

    return-void

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Ljava/lang/String;

    move-result-object p1

    .line 97
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a()Lcom/bytedance/sdk/openadsdk/l/a/a;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p4, p5}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;)Ljava/lang/String;

    move-result-object v0

    .line 100
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " GiftLoader doTask cacheKey "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "splashLoadAd"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/l/a/b;->b:Z

    if-eqz v1, :cond_2

    .line 104
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a()Lcom/bytedance/sdk/openadsdk/l/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/l/a/a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/a/a$a;

    move-result-object v1

    goto :goto_0

    .line 106
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a()Lcom/bytedance/sdk/openadsdk/l/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/l/a/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/a/a$a;

    move-result-object v1

    :goto_0
    move-object v5, v1

    if-eqz v5, :cond_3

    .line 109
    iget-object v1, v5, Lcom/bytedance/sdk/openadsdk/l/a/a$a;->a:[B

    if-eqz v1, :cond_3

    .line 111
    new-instance v6, Lcom/bytedance/sdk/openadsdk/l/a/b$c;

    new-instance p3, Lcom/bytedance/sdk/openadsdk/l/a/d;

    iget-object p4, v5, Lcom/bytedance/sdk/openadsdk/l/a/a$a;->a:[B

    invoke-direct {p3, p4}, Lcom/bytedance/sdk/openadsdk/l/a/d;-><init>([B)V

    invoke-direct {v6, p3, p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/l/a/b$c;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/d;Lcom/bytedance/sdk/openadsdk/l/a/b$b;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->d:Landroid/os/Handler;

    new-instance p4, Lcom/bytedance/sdk/openadsdk/l/a/b$3;

    move-object v1, p4

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/l/a/b$3;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/a/a$a;Lcom/bytedance/sdk/openadsdk/l/a/b$c;)V

    invoke-virtual {p3, p4}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    return-void

    .line 132
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/l/a/b$d;

    if-eqz v1, :cond_4

    .line 134
    invoke-virtual {v1, p2}, Lcom/bytedance/sdk/openadsdk/l/a/b$d;->a(Lcom/bytedance/sdk/openadsdk/l/a/b$b;)V

    return-void

    :cond_4
    const/4 v1, 0x0

    .line 137
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Z)V

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " GiftLoader doTask \u7f13\u5b58\u4e0d\u5b58\u5728 \u7f51\u7edc\u8bf7\u6c42\u56fe\u7247 requestUrl "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, v0

    .line 140
    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/a/c;

    move-result-object p1

    .line 141
    new-instance p3, Lcom/bytedance/sdk/openadsdk/l/a/b$d;

    invoke-direct {p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/l/a/b$d;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/c;Lcom/bytedance/sdk/openadsdk/l/a/b$b;)V

    .line 142
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/l/a/b;->c()V

    .line 143
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->c:Lcom/bytedance/sdk/component/adnet/core/l;

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/component/adnet/core/l;->a(Lcom/bytedance/sdk/component/adnet/core/Request;)Lcom/bytedance/sdk/component/adnet/core/Request;

    .line 144
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->e:Ljava/util/Map;

    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private c()V
    .locals 5

    .line 158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    if-nez v0, :cond_0

    return-void

    .line 161
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/t;->z()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 164
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->g:J

    .line 165
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->t()J

    move-result-wide v3

    sub-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/e/t;->h(J)V

    .line 166
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->g:J

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/e/t;->n(J)V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/e/t;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V
    .locals 10

    if-eqz p2, :cond_0

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->d:Landroid/os/Handler;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/a/b$1;

    invoke-direct {v1, p0, p2}, Lcom/bytedance/sdk/openadsdk/l/a/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 81
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/a/b$2;

    const-string v4, "GifLoader get"

    move-object v2, v0

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move v7, p3

    move v8, p4

    move-object v9, p5

    invoke-direct/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/l/a/b$2;-><init>(Lcom/bytedance/sdk/openadsdk/l/a/b;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V

    const/4 p1, 0x5

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IIZ)V
    .locals 6

    .line 62
    sput-boolean p5, Lcom/bytedance/sdk/openadsdk/l/a/b;->b:Z

    .line 63
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/l/a/b;->a(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/a/b$b;IILandroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/core/e/t;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/b;->f:Lcom/bytedance/sdk/openadsdk/core/e/t;

    return-object v0
.end method
