.class public Lcom/bytedance/sdk/openadsdk/r/d;
.super Ljava/lang/Object;
.source "ExternalSpUtils.java"


# direct methods
.method public static a()Ljava/lang/String;
    .locals 2

    const-string v0, "any_door_id"

    const/4 v1, 0x0

    .line 36
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)V
    .locals 1

    const-string v0, "splash_storage_from"

    .line 52
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(J)V
    .locals 1

    const-string v0, "save_dpl_success_time"

    .line 57
    invoke-static {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;J)V

    return-void
.end method

.method public static a(JLjava/lang/String;)V
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "last_load_splash_ad_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 41
    invoke-static {p2, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;J)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "any_door_id"

    .line 32
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/String;I)V
    .locals 2

    .line 141
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 145
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 148
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method private static a(Ljava/lang/String;J)V
    .locals 2

    .line 199
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 203
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 206
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 229
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 233
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 236
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method private static a(Ljava/lang/String;Z)V
    .locals 2

    .line 170
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 174
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 177
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method public static a(Z)V
    .locals 1

    const-string v0, "is_landing_page_open_market"

    .line 98
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public static b()J
    .locals 3

    const-wide/16 v0, 0x0

    const-string v2, "save_dpl_success_time"

    .line 62
    invoke-static {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Ljava/lang/String;)J
    .locals 2

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "last_load_splash_ad_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-wide/16 v0, 0x0

    .line 47
    invoke-static {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static b(Ljava/lang/String;J)J
    .locals 2

    .line 215
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-static {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;J)J

    move-result-wide p0

    return-wide p0

    :cond_0
    const-string v0, ""

    .line 219
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;J)J

    move-result-wide p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide p0

    :catchall_0
    return-wide p1
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 243
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 248
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 252
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p0

    :catchall_0
    return-object p1
.end method

.method public static b(J)V
    .locals 1

    const-string v0, "sdk_first_init_timestamp"

    .line 113
    invoke-static {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;J)V

    return-void
.end method

.method private static b(Ljava/lang/String;Z)Z
    .locals 2

    .line 186
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Z)Z

    move-result p0

    return p0

    :cond_0
    const-string v0, ""

    .line 190
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/s;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/component/utils/s;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/bytedance/sdk/component/utils/s;->b(Ljava/lang/String;Z)Z

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return p0

    :catchall_0
    return p1
.end method

.method public static c()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const-string v1, "save_dpl_success_ad_tag"

    .line 72
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(J)V
    .locals 1

    const-string v0, "sdk_first_init_timestamp"

    .line 123
    invoke-static {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;J)V

    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    const-string v0, "save_dpl_success_ad_tag"

    .line 67
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const-string v1, "save_dpl_success_materialmeta"

    .line 83
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    const-string v0, "save_dpl_success_materialmeta"

    .line 78
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const-string v1, "save_playable_screen_shot_materialmeta"

    .line 93
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    const-string v0, "save_playable_screen_shot_materialmeta"

    .line 88
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static f(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sdk_brand_video_cahce"

    .line 131
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static f()Z
    .locals 2

    const-string v0, "is_landing_page_open_market"

    const/4 v1, 0x0

    .line 102
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static g()J
    .locals 3

    const-wide/16 v0, 0x0

    const-string v2, "sdk_first_init_timestamp"

    .line 108
    invoke-static {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static h()J
    .locals 3

    const-wide/16 v0, 0x0

    const-string v2, "sdk_first_init_timestamp"

    .line 118
    invoke-static {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static i()Ljava/lang/String;
    .locals 2

    const-string v0, "sdk_brand_video_cahce"

    const-string v1, ""

    .line 127
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
