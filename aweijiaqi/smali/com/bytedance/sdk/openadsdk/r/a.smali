.class public Lcom/bytedance/sdk/openadsdk/r/a;
.super Ljava/lang/Object;
.source "ActivityLifecycleListener.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/r/a$b;,
        Lcom/bytedance/sdk/openadsdk/r/a$a;
    }
.end annotation


# static fields
.field public static a:Z = false

.field public static b:J

.field public static c:J


# instance fields
.field private volatile d:Lcom/bytedance/sdk/openadsdk/r/a$a;

.field private volatile e:Lcom/bytedance/sdk/openadsdk/r/a$b;

.field private f:Ljava/util/concurrent/atomic/AtomicInteger;

.field private g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->h:Ljava/util/HashSet;

    return-void
.end method

.method private c()V
    .locals 2

    .line 127
    new-instance v0, Lcom/bytedance/sdk/openadsdk/r/a$1;

    const-string v1, "reportSdkUseTime"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/r/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method private d()V
    .locals 7

    .line 148
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/d;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    return-void

    .line 152
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/32 v0, 0x5265c00

    cmp-long v6, v4, v0

    if-gez v6, :cond_3

    cmp-long v0, v4, v2

    if-gtz v0, :cond_1

    goto :goto_0

    .line 157
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/d;->c()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/d;->d()Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 162
    :cond_2
    invoke-static {v4, v5, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->a(JLjava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/r/a$a;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/r/a$b;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->e:Lcom/bytedance/sdk/openadsdk/r/a$b;

    return-void
.end method

.method public a()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/app/Activity;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->h:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method public b()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 175
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 40
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz p1, :cond_0

    .line 41
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$a;->d()V

    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->h:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 106
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz p1, :cond_1

    .line 107
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$a;->f()V

    :cond_1
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .line 76
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz p1, :cond_0

    .line 77
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$a;->c()V

    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->h:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/r/a$a;->a()V

    .line 65
    :cond_1
    invoke-static {p1}, Lcom/bytedance/sdk/component/adnet/a;->a(Landroid/app/Activity;)V

    .line 66
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/r/a;->a:Z

    if-nez p1, :cond_2

    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/bytedance/sdk/openadsdk/r/a;->b:J

    const/4 p1, 0x1

    .line 68
    sput-boolean p1, Lcom/bytedance/sdk/openadsdk/r/a;->a:Z

    .line 71
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/r;->a()V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .line 47
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p1

    if-lez p1, :cond_0

    .line 48
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/r/a;->d()V

    .line 51
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz p1, :cond_1

    .line 52
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$a;->b()V

    :cond_1
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    .line 83
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result p1

    if-nez p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 85
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->e:Lcom/bytedance/sdk/openadsdk/r/a$b;

    if-eqz p1, :cond_0

    .line 86
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->e:Lcom/bytedance/sdk/openadsdk/r/a$b;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$b;->b()V

    .line 90
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    if-eqz p1, :cond_1

    .line 91
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/r/a;->d:Lcom/bytedance/sdk/openadsdk/r/a$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/r/a$a;->e()V

    .line 93
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/r/a;->c()V

    return-void
.end method
