.class public Lcom/bytedance/sdk/openadsdk/r/b;
.super Ljava/lang/Object;
.source "CommonDialogHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/r/b$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/r/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/bytedance/sdk/openadsdk/IListenerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->a:Ljava/util/Map;

    return-void
.end method

.method static synthetic a()Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 1

    .line 34
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/b;->b()Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 80
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->b(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .line 62
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    invoke-static {p1, p3}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    .line 66
    invoke-static/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;Ljava/lang/String;Z)V
    .locals 1

    .line 72
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 75
    :cond_0
    invoke-static {p1, p3}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    .line 76
    invoke-static {p0, p1, p2, p4, p5}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V
    .locals 1

    .line 51
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p4, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    invoke-static {p1, p4}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    .line 57
    invoke-static {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V
    .locals 1

    .line 138
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p6, :cond_0

    goto :goto_0

    .line 142
    :cond_0
    invoke-static {p1, p6}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    .line 144
    invoke-static/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 84
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 88
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v5

    .line 89
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->U()Ljava/lang/String;

    move-result-object v3

    .line 90
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 91
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/bytedance/sdk/openadsdk/r/b$1;

    invoke-direct {v4, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/r/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;Ljava/lang/String;Z)V

    :cond_2
    :goto_1
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 148
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/lang/String;I)V
    .locals 2

    .line 189
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 193
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    new-instance v0, Lcom/bytedance/sdk/openadsdk/r/b$3;

    const-string v1, "doHandler"

    invoke-direct {v0, v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/b$3;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    const/4 p0, 0x5

    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;I)V

    return-void

    .line 206
    :cond_1
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/b;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/r/b$a;

    move-result-object p0

    if-nez p0, :cond_2

    return-void

    :cond_2
    const/4 v0, 0x1

    if-eq p1, v0, :cond_5

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    .line 222
    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/r/b$a;->c()V

    goto :goto_0

    .line 219
    :cond_3
    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/r/b$a;->c()V

    goto :goto_0

    .line 216
    :cond_4
    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/r/b$a;->b()V

    goto :goto_0

    .line 213
    :cond_5
    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/r/b$a;->a()V

    :goto_0
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V
    .locals 2

    .line 160
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 164
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    new-instance v0, Lcom/bytedance/sdk/openadsdk/r/b$2;

    const-string v1, "addDialogListener"

    invoke-direct {v0, v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/r/b$2;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    const/4 p0, 0x5

    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/e/e;->c(Lcom/bytedance/sdk/component/e/g;I)V

    goto :goto_0

    .line 175
    :cond_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->a:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_0
    return-void
.end method

.method private static b()Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 2

    .line 229
    sget-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->b:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    if-nez v0, :cond_0

    .line 230
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    const/4 v1, 0x2

    .line 231
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object v0

    .line 232
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/c;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->b:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    .line 235
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->b:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 109
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTDelegateActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/a;->a()Lcom/bytedance/sdk/openadsdk/k/a;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/k/a;->c(Ljava/lang/String;)V

    return-void
.end method

.method public static b(Lcom/bytedance/sdk/openadsdk/core/e/m;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 115
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {p1, p0, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object p0

    .line 119
    instance-of p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    if-eqz p1, :cond_1

    .line 120
    move-object p1, p0

    check-cast p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g(Z)V

    .line 122
    :cond_1
    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->g()V

    :cond_2
    :goto_0
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x2

    .line 152
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x3

    .line 156
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public static d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/r/b$a;
    .locals 1

    .line 180
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 184
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/r/b;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/bytedance/sdk/openadsdk/r/b$a;

    return-object p0
.end method
