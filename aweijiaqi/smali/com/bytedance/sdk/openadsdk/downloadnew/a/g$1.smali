.class final Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$1;
.super Ljava/lang/Object;
.source "LibHolder.java"

# interfaces
.implements Lcom/ss/android/a/a/c/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/ss/android/a/a/c/c;Lcom/ss/android/a/a/c/a;Lcom/ss/android/a/a/c/b;)V
    .locals 0

    const-string p1, "LibHolder"

    const-string p2, "completeListener: onDownloadStart"

    .line 94
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V
    .locals 1

    const-string p1, "LibHolder"

    const-string v0, "completeListener: onCanceled"

    .line 117
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;Ljava/lang/String;)V
    .locals 0

    const-string p1, "LibHolder"

    const-string p2, "completeListener: onDownloadFailed"

    .line 111
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;)V
    .locals 0

    const-string p1, "LibHolder"

    const-string p2, "completeListener: onDownloadFinished"

    .line 100
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;)V
    .locals 1

    const-string p1, "LibHolder"

    const-string v0, "completeListener: onInstalled"

    .line 105
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;)V

    return-void
.end method
