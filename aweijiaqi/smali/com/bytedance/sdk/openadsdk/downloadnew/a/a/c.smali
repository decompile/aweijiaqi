.class public Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;
.super Ljava/lang/Object;
.source "LibNetwork.java"

# interfaces
.implements Lcom/ss/android/a/a/b/g;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Lcom/bytedance/sdk/component/net/NetResponse;Lcom/ss/android/a/a/b/r;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_5

    .line 82
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/ss/android/a/a/b/r;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_0
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 85
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 86
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz p2, :cond_5

    if-eqz v0, :cond_3

    .line 88
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->getMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetResponse;->getCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_4
    const-string p1, ""

    .line 89
    :goto_2
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/ss/android/a/a/b/r;->a(Ljava/lang/Throwable;)V

    :cond_5
    :goto_3
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/ss/android/a/a/b/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/ss/android/a/a/b/r;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0x11336

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x2590a0

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "GET"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_5

    if-eq p1, v2, :cond_3

    const/4 p1, 0x0

    goto :goto_4

    .line 52
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetClient;->getPostExecutor()Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    move-result-object p1

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 54
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 55
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 57
    :cond_4
    move-object p3, p1

    check-cast p3, Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    invoke-virtual {p3, v0}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->setParams(Ljava/util/Map;)V

    goto :goto_4

    .line 44
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/NetClient;->getGetExecutor()Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    move-result-object p1

    if-eqz p3, :cond_6

    .line 46
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 47
    move-object v1, p1

    check-cast v1, Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->addParams(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    :goto_4
    if-eqz p1, :cond_7

    .line 63
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->setUrl(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/net/executor/NetExecutor;->execute()Lcom/bytedance/sdk/component/net/NetResponse;

    move-result-object p1

    .line 65
    invoke-direct {p0, p1, p4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;->a(Lcom/bytedance/sdk/component/net/NetResponse;Lcom/ss/android/a/a/b/r;)V

    :cond_7
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ILcom/ss/android/a/a/b/r;)V
    .locals 0

    .line 72
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object p4

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object p4

    invoke-virtual {p4}, Lcom/bytedance/sdk/component/net/NetClient;->getPostExecutor()Lcom/bytedance/sdk/component/net/executor/PostExecutor;

    move-result-object p4

    .line 73
    invoke-virtual {p4, p1}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->setUrl(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p4, p3, p2}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->setRequestBody(Ljava/lang/String;[B)V

    .line 75
    invoke-virtual {p4}, Lcom/bytedance/sdk/component/net/executor/PostExecutor;->execute()Lcom/bytedance/sdk/component/net/NetResponse;

    move-result-object p1

    .line 76
    invoke-direct {p0, p1, p5}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;->a(Lcom/bytedance/sdk/component/net/NetResponse;Lcom/ss/android/a/a/b/r;)V

    return-void
.end method
