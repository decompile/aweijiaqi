.class Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;
.super Ljava/lang/Object;
.source "DMLibManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 0

    .line 1164
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->a:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1167
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    .line 1169
    new-instance v1, Lcom/bytedance/sdk/openadsdk/multipro/aidl/b/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->a:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/b/e;-><init>(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    const/4 v2, 0x3

    .line 1170
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object v0

    .line 1171
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/b;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1174
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/IListenerManager;->registerTTAppDownloadListener(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/ITTAppDownloadListener;)V

    .line 1175
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/util/List;

    move-result-object v0

    monitor-enter v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1176
    :try_start_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "DMLibManager"

    .line 1177
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerMultiProcessListener, mTTAppDownloadListenerList size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 1181
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method
