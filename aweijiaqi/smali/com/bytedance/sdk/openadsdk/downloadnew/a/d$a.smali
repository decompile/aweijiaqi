.class Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;
.super Lcom/bytedance/sdk/component/e/g;
.source "DMLibManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:J

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field final synthetic f:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V
    .locals 0

    .line 1318
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->f:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const-string p1, "DownloadCallbackRunnable"

    .line 1319
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1323
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->f:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const-string p1, "DownloadCallbackRunnable"

    .line 1324
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 1326
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a:Ljava/lang/String;

    .line 1327
    iput-wide p3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b:J

    .line 1328
    iput-wide p5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->c:J

    .line 1329
    iput-object p7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->d:Ljava/lang/String;

    .line 1330
    iput-object p8, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    .line 1338
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 1334
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a:Ljava/lang/String;

    return-void
.end method

.method public b(J)V
    .locals 0

    .line 1342
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->c:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 1346
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->d:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 1350
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->e:Ljava/lang/String;

    return-void
.end method

.method public run()V
    .locals 10

    .line 1356
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->f:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l()Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->f:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a:Ljava/lang/String;

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b:J

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->c:J

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->d:Ljava/lang/String;

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->e:Ljava/lang/String;

    invoke-interface/range {v1 .. v9}, Lcom/bytedance/sdk/openadsdk/IListenerManager;->executeAppDownloadCallback(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "DMLibManager"

    const-string v2, "executeRewardVideoCallback execute throw Exception : "

    .line 1358
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
