.class Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;
.super Ljava/lang/Object;
.source "DMLibManager.java"

# interfaces
.implements Lcom/ss/android/a/a/c/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    const-string v0, "onIdle"

    .line 134
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v2, "onIdle"

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onIdle()V

    :cond_1
    return-void
.end method

.method public a(Lcom/ss/android/a/a/c/c;Lcom/ss/android/a/a/c/a;)V
    .locals 8

    .line 148
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 149
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onDownloadStart: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 151
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-interface {p1}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 153
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v1, "onIdle"

    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 158
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 159
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onIdle()V

    :cond_1
    return-void
.end method

.method public a(Lcom/ss/android/a/a/e/e;)V
    .locals 9

    .line 203
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 204
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDownloadFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v3, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v5, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v7, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 210
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v8

    const-string v2, "onDownloadFailed"

    .line 208
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v1

    iget-wide v2, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v4, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v6, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onDownloadFailed(JJLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/ss/android/a/a/e/e;I)V
    .locals 8

    .line 165
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 166
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 167
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->a:J

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 168
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onDownloadActive: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->c:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->d:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 170
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 171
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v2, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v4, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v6, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 173
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v7

    const-string v1, "onDownloadActive"

    .line 171
    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 177
    :cond_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 178
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v3, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v5, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onDownloadActive(JJLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b(Lcom/ss/android/a/a/e/e;)V
    .locals 9

    .line 221
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 222
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 223
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onInstalled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 226
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v3, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v5, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v7, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 229
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v8

    const-string v2, "onInstalled"

    .line 227
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    iget-object p1, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onInstalled(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b(Lcom/ss/android/a/a/e/e;I)V
    .locals 8

    .line 184
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 185
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 186
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->a:J

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 187
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onDownloadPaused: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->c:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/ss/android/a/a/e/e;->d:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 190
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v2, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v4, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v6, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 192
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v7

    const-string v1, "onDownloadPaused"

    .line 190
    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 196
    :cond_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 197
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v3, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v5, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onDownloadPaused(JJLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public c(Lcom/ss/android/a/a/e/e;)V
    .locals 9

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 241
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(J)V

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDownloadFinished: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-wide v3, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-wide v5, p1, Lcom/ss/android/a/a/e/e;->d:J

    iget-object v7, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 247
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v8

    const-string v2, "onDownloadFinished"

    .line 245
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    move-result-object v0

    iget-wide v1, p1, Lcom/ss/android/a/a/e/e;->c:J

    iget-object p1, p1, Lcom/ss/android/a/a/e/e;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;->a:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->onDownloadFinished(JLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
