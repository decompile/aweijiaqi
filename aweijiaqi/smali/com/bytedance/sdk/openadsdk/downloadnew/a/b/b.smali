.class public Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;
.super Ljava/lang/Object;
.source "LibModelFactory.java"


# direct methods
.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/ss/android/b/a/a/a$a;
    .locals 5

    const/4 v0, 0x1

    if-nez p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->y()I

    move-result v1

    :goto_0
    const/4 v2, 0x0

    if-nez p0, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    .line 157
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->z()I

    move-result v3

    :goto_1
    if-eqz p0, :cond_2

    .line 158
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ax()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v3, 0x2

    .line 162
    :cond_2
    new-instance v4, Lcom/ss/android/b/a/a/a$a;

    invoke-direct {v4}, Lcom/ss/android/b/a/a/a$a;-><init>()V

    .line 163
    invoke-virtual {v4, v1}, Lcom/ss/android/b/a/a/a$a;->a(I)Lcom/ss/android/b/a/a/a$a;

    move-result-object v1

    .line 164
    invoke-virtual {v1, v3}, Lcom/ss/android/b/a/a/a$a;->b(I)Lcom/ss/android/b/a/a/a$a;

    move-result-object v1

    .line 165
    invoke-virtual {v1, v0}, Lcom/ss/android/b/a/a/a$a;->a(Z)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    .line 166
    invoke-virtual {v0, v2}, Lcom/ss/android/b/a/a/a$a;->b(Z)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    .line 167
    invoke-virtual {v0, p0}, Lcom/ss/android/b/a/a/a$a;->a(Ljava/lang/Object;)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    if-eqz p0, :cond_3

    .line 168
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->am()Lcom/bytedance/sdk/openadsdk/core/e/j;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 169
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->am()Lcom/bytedance/sdk/openadsdk/core/e/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/j;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/a$a;->f(Z)Lcom/ss/android/b/a/a/a$a;

    .line 170
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->am()Lcom/bytedance/sdk/openadsdk/core/e/j;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/j;->b()Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/ss/android/b/a/a/a$a;->g(Z)Lcom/ss/android/b/a/a/a$a;

    :cond_3
    return-object v0
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 176
    new-instance p0, Lcom/ss/android/b/a/a/b$a;

    invoke-direct {p0}, Lcom/ss/android/b/a/a/b$a;-><init>()V

    .line 177
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->a(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    .line 178
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->b(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const-string p1, "click_start"

    .line 179
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->c(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const-string p1, "click_continue"

    .line 180
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->e(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const-string p1, "click_pause"

    .line 181
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->d(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const-string p1, "download_failed"

    .line 182
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->h(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const-string p1, "click_install"

    .line 183
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->f(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const/4 p1, 0x1

    .line 184
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->a(Z)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    const/4 p1, 0x0

    .line 185
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/b$a;->c(Z)Lcom/ss/android/b/a/a/b$a;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;
    .locals 4

    if-nez p1, :cond_0

    .line 94
    new-instance p0, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {p0}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    return-object p0

    .line 98
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 100
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a()Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p0

    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p0

    const-string p2, "open_ad_sdk_download_extra"

    .line 101
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b()Lorg/json/JSONObject;

    move-result-object p0

    invoke-virtual {v0, p2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 105
    :goto_0
    new-instance p0, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {p0}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    .line 106
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Double;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/ss/android/b/a/a/c$a;->a(J)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 107
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object p2

    if-nez p2, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object p2

    :goto_1
    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->c(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 108
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/h;->o()Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->b(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 109
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/h;->o()Z

    move-result p2

    const/4 v1, 0x1

    xor-int/2addr p2, v1

    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->d(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 110
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->a(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 111
    invoke-virtual {p0, v0}, Lcom/ss/android/b/a/a/c$a;->a(Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 112
    invoke-virtual {p0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    new-instance p2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b$2;

    invoke-direct {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b$2;-><init>()V

    .line 113
    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->a(Lcom/ss/android/socialbase/downloader/depend/t;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 119
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Lcom/ss/android/b/a/a/c$a;)V

    .line 121
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 123
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ss/android/b/a/a/c$a;->d(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    .line 124
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ss/android/b/a/a/c$a;->e(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    .line 125
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/b;->d()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/ss/android/b/a/a/c$a;->b(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    .line 128
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 129
    invoke-virtual {p0, v1}, Lcom/ss/android/b/a/a/c$a;->c(Z)Lcom/ss/android/b/a/a/c$a;

    .line 132
    :cond_3
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 133
    new-instance p2, Lcom/ss/android/a/a/e/b;

    invoke-direct {p2}, Lcom/ss/android/a/a/e/b;-><init>()V

    .line 134
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lcom/ss/android/a/a/e/b;->a(J)V

    .line 136
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/ss/android/a/a/e/b;->b(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ah()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/ss/android/a/a/e/b;->c(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/g;->c()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 140
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_2

    .line 144
    :cond_4
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/g;->c()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 145
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/g;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/ss/android/a/a/e/b;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 147
    :cond_5
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/ss/android/a/a/e/b;->a(Ljava/lang/String;)V

    .line 149
    :goto_2
    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/a/c$a;->a(Lcom/ss/android/a/a/e/b;)Lcom/ss/android/b/a/a/c$a;

    :cond_6
    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;
    .locals 4

    if-nez p2, :cond_0

    .line 40
    new-instance p0, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {p0}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    return-object p0

    .line 42
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    new-instance p0, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {p0}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    return-object p0

    .line 47
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 49
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a()Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p1

    const-string p3, "open_ad_sdk_download_extra"

    .line 50
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b()Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {v0, p3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 53
    :goto_0
    new-instance p1, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {p1}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    .line 54
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/ss/android/b/a/a/c$a;->a(J)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 55
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object p3

    if-nez p3, :cond_2

    const/4 p3, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object p3

    :goto_1
    invoke-virtual {p1, p3}, Lcom/ss/android/b/a/a/c$a;->c(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 56
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/h;->o()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/ss/android/b/a/a/c$a;->b(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 57
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/h;->o()Z

    move-result p3

    const/4 v1, 0x1

    xor-int/2addr p3, v1

    invoke-virtual {p1, p3}, Lcom/ss/android/b/a/a/c$a;->d(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 58
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/ss/android/b/a/a/c$a;->a(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 59
    invoke-virtual {p1, v0}, Lcom/ss/android/b/a/a/c$a;->a(Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    .line 60
    invoke-virtual {p1, p0}, Lcom/ss/android/b/a/a/c$a;->d(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 61
    invoke-virtual {p0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    new-instance p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b$1;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b$1;-><init>()V

    .line 62
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/c$a;->a(Lcom/ss/android/socialbase/downloader/depend/t;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p0

    .line 68
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Lcom/ss/android/b/a/a/c$a;)V

    .line 70
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 71
    invoke-virtual {p0, v1}, Lcom/ss/android/b/a/a/c$a;->c(Z)Lcom/ss/android/b/a/a/c$a;

    .line 74
    :cond_3
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 75
    new-instance p1, Lcom/ss/android/a/a/e/b;

    invoke-direct {p1}, Lcom/ss/android/a/a/e/b;-><init>()V

    .line 76
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/ss/android/a/a/e/b;->a(J)V

    .line 77
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/g;->a()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/ss/android/a/a/e/b;->b(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ah()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/ss/android/a/a/e/b;->c(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/g;->c()I

    move-result p3

    const/4 v0, 0x2

    if-ne p3, v0, :cond_4

    .line 80
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p3

    if-nez p3, :cond_4

    goto :goto_2

    .line 82
    :cond_4
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/e/g;->c()I

    move-result p3

    if-ne p3, v1, :cond_5

    .line 83
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/g;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/e/b;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 85
    :cond_5
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/e/b;->a(Ljava/lang/String;)V

    .line 87
    :goto_2
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/a/c$a;->a(Lcom/ss/android/a/a/e/b;)Lcom/ss/android/b/a/a/c$a;

    :cond_6
    return-object p0
.end method
