.class public final Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;
.super Ljava/lang/Object;
.source "LibHolder.java"


# static fields
.field public static volatile a:Ljava/lang/String;

.field private static final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static c:Landroid/content/Context;

.field private static d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Z

.field private static f:Lcom/ss/android/a/a/c/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    .line 79
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->e:Z

    .line 91
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$1;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$1;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->f:Lcom/ss/android/a/a/c/a/a;

    .line 417
    :try_start_0
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lorg/json/JSONObject;)Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;
    .locals 2

    const v0, 0x3dffffff    # 0.12499999f

    const-string v1, "download_exp_switch_temp"

    .line 686
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    .line 687
    new-instance v0, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;

    invoke-direct {v0, p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$8;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$8;-><init>()V

    .line 688
    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->downloadSetting(Lcom/ss/android/socialbase/downloader/depend/ac;)Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;

    move-result-object v0

    .line 694
    invoke-virtual {v0, p1}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->downloadExpSwitch(I)Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/f;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/f;-><init>(Landroid/content/Context;)V

    .line 695
    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->httpService(Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;)Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;

    move-result-object p0

    return-object p0
.end method

.method public static a()V
    .locals 3

    const/4 v0, 0x0

    .line 447
    :try_start_0
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->e:Z

    if-eqz v1, :cond_0

    .line 449
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 452
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 453
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 464
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 465
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    .line 467
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 468
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 470
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 475
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 476
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 477
    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 478
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 479
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 482
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 483
    sput-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    .line 486
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/e;->a()V

    :cond_3
    const-string v0, "LibHolder"

    .line 489
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(I)V
    .locals 1

    .line 536
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 537
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 543
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 544
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d:Ljava/util/Map;

    .line 546
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    if-nez p0, :cond_0

    .line 427
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p0

    :cond_0
    if-nez p0, :cond_1

    return-void

    .line 432
    :cond_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    .line 433
    const-class v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;

    monitor-enter v0

    .line 434
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_2

    .line 435
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c:Landroid/content/Context;

    .line 436
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a()V

    .line 437
    sget-object p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c:Landroid/content/Context;

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Landroid/content/Context;)Z

    move-result p0

    .line 438
    sget-object v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 440
    :cond_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_3
    :goto_0
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 7

    if-eqz p0, :cond_5

    if-nez p1, :cond_0

    goto :goto_2

    .line 307
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p0

    if-eqz p0, :cond_1

    return-void

    .line 311
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 312
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-string p0, ""

    :goto_0
    move-object v4, p0

    .line 316
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    const-string p0, "\u5e94\u7528\u5b89\u88c5\u5b8c\u6210\uff0c\u662f\u5426\u7acb\u5373\u6253\u5f00 \uff1f"

    goto :goto_1

    :cond_3
    const-string p0, "\u5b89\u88c5\u5b8c\u6210\uff0c\u662f\u5426\u7acb\u5373\u6253\u5f00 \uff1f"

    :goto_1
    move-object v5, p0

    .line 320
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/h;->c()Lcom/bytedance/sdk/openadsdk/r/a;

    move-result-object p0

    if-nez p0, :cond_4

    return-void

    .line 324
    :cond_4
    new-instance v6, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$3;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$3;-><init>(Lcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/bytedance/sdk/openadsdk/r/a;->a(Lcom/bytedance/sdk/openadsdk/r/a$a;)V

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;)V
    .locals 0

    .line 74
    invoke-static {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 0

    .line 74
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/r/a;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 74
    invoke-static {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Lcom/bytedance/sdk/openadsdk/r/a;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    .line 74
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 2

    .line 163
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_0
    if-eqz p1, :cond_5

    .line 167
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->n()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 171
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "LibHolder"

    if-eqz v0, :cond_2

    .line 172
    :try_start_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "\u542b\u6709deepLink"

    .line 173
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->y()I

    move-result v0

    if-eqz v0, :cond_2

    .line 175
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "deepLink\u8fc7\u6ee4 mMeta.getDownConfigAutoOpen() "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->y()I

    move-result p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 179
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 181
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u8be5app\u5df2\u88ab\u6fc0\u6d3b pkgName "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 186
    :cond_3
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->p()Z

    move-result v0

    if-nez v0, :cond_4

    .line 187
    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    nop

    :catchall_0
    :cond_5
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 599
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 603
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/d;->j()Lcom/ss/android/socialbase/appdownloader/d;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/ss/android/socialbase/appdownloader/d;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object p0

    .line 604
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 605
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v1, :cond_1

    .line 606
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_2
    :goto_0
    return v0
.end method

.method private static a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 290
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->i()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Lorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 293
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v1

    .line 294
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ax()Ljava/lang/String;

    move-result-object p0

    .line 295
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    const/4 p0, 0x4

    if-eq v1, p0, :cond_3

    .line 296
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/d;->f()Z

    move-result p0

    if-eqz p0, :cond_4

    .line 297
    :cond_3
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/d;->a(Z)V

    const/4 p0, 0x1

    return p0

    :cond_4
    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/Object;)Z
    .locals 10

    .line 556
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_0

    goto :goto_1

    .line 561
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->h()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 563
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 564
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 565
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;

    if-eqz v4, :cond_1

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    move-object v9, p3

    .line 567
    invoke-interface/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;->a(ILcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v2

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method private static a(Lorg/json/JSONObject;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    :try_start_0
    const-string v1, "enable_open_app_dialog"

    .line 716
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    return v1

    :catchall_0
    :cond_1
    return v0
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/core/e/m;JLcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;)V
    .locals 4

    if-nez p0, :cond_0

    return-void

    .line 257
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 258
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->r()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    sub-long/2addr v0, p1

    cmp-long p1, v0, v2

    if-ltz p1, :cond_3

    .line 262
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->c()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    .line 266
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_2

    const-string p2, "\u5e94\u7528\u5b89\u88c5\u5b8c\u6210\uff0c\u662f\u5426\u7acb\u5373\u6253\u5f00 \uff1f"

    goto :goto_1

    :cond_2
    const-string p2, "\u5b89\u88c5\u5b8c\u6210\uff0c\u662f\u5426\u7acb\u5373\u6253\u5f00 \uff1f"

    .line 269
    :goto_1
    invoke-static {p3, p0, p1, p2, p4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->b(Lcom/bytedance/sdk/openadsdk/r/a;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 272
    :cond_3
    invoke-static {p0, p4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 8

    .line 200
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 203
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->c()Lcom/bytedance/sdk/openadsdk/r/a;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 207
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 208
    new-instance v7, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$2;

    move-object v1, v7

    move-object v2, v0

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$2;-><init>(Lcom/bytedance/sdk/openadsdk/r/a;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;J)V

    invoke-virtual {v0, v7}, Lcom/bytedance/sdk/openadsdk/r/a;->a(Lcom/bytedance/sdk/openadsdk/r/a$a;)V

    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/r/a;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    if-nez p0, :cond_0

    return-void

    .line 380
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/bytedance/sdk/openadsdk/r/o;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "\u8be5app\u672a\u5b89\u88c5 packageName "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "LibHolder"

    invoke-static {p1, p0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 384
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p2, ""

    :cond_2
    move-object v2, p2

    .line 387
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    new-instance v6, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$4;

    invoke-direct {v6, p1, p4, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/a;)V

    const-string v4, "\u7acb\u5373\u6253\u5f00"

    const-string v5, "\u9000\u51fa"

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)V
    .locals 2

    .line 123
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/q;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string p0, "LibHolder"

    const-string v0, "\u9501\u5c4f\u4e0d\u6267\u884c\u81ea\u542f\u52a8\u8c03\u8d77"

    .line 125
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 128
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 131
    :cond_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/ss/android/downloadlib/addownload/b/f;->a(Ljava/lang/String;)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    .line 135
    :cond_2
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->g()Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    const-string v1, "open_ad_sdk_download_extra"

    .line 139
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_4

    return-void

    .line 143
    :cond_4
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object v0

    if-nez v0, :cond_5

    return-void

    .line 147
    :cond_5
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_6

    return-void

    .line 151
    :cond_6
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    return-void
.end method

.method public static b()Z
    .locals 3

    .line 501
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 502
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/g/d;->a()Lcom/bytedance/sdk/openadsdk/core/g/d;

    move-result-object v0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/g/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 623
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    .line 625
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 626
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, ""

    .line 630
    :cond_1
    invoke-static {p0}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ss/android/downloadlib/g;->a()Lcom/ss/android/a/a/a;

    move-result-object v2

    if-nez v2, :cond_2

    return v0

    .line 636
    :cond_2
    new-instance v0, Lcom/ss/android/a/a/e/a$a;

    invoke-direct {v0}, Lcom/ss/android/a/a/e/a$a;-><init>()V

    const-string v3, "143"

    .line 637
    invoke-virtual {v0, v3}, Lcom/ss/android/a/a/e/a$a;->b(Ljava/lang/String;)Lcom/ss/android/a/a/e/a$a;

    move-result-object v0

    const-string v3, "open_news"

    .line 638
    invoke-virtual {v0, v3}, Lcom/ss/android/a/a/e/a$a;->a(Ljava/lang/String;)Lcom/ss/android/a/a/e/a$a;

    move-result-object v0

    const-string v3, "3.6.1.4"

    .line 639
    invoke-virtual {v0, v3}, Lcom/ss/android/a/a/e/a$a;->c(Ljava/lang/String;)Lcom/ss/android/a/a/e/a$a;

    move-result-object v0

    const/16 v3, 0xe1e

    .line 640
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/ss/android/a/a/e/a$a;->d(Ljava/lang/String;)Lcom/ss/android/a/a/e/a$a;

    move-result-object v0

    .line 641
    invoke-virtual {v0}, Lcom/ss/android/a/a/e/a$a;->a()Lcom/ss/android/a/a/e/a;

    move-result-object v0

    .line 643
    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/d;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/d;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/h;)Lcom/ss/android/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;-><init>(Landroid/content/Context;)V

    .line 644
    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/f;)Lcom/ss/android/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/e;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/e;-><init>(Landroid/content/Context;)V

    .line 645
    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/k;)Lcom/ss/android/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/c;-><init>(Landroid/content/Context;)V

    .line 646
    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/g;)Lcom/ss/android/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$6;

    invoke-direct {v3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$6;-><init>()V

    .line 647
    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/i;)Lcom/ss/android/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/a;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/a;-><init>(Landroid/content/Context;)V

    .line 653
    invoke-interface {v2, v3}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/b;)Lcom/ss/android/a/a/a;

    move-result-object v2

    .line 654
    invoke-interface {v2, v0}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/e/a;)Lcom/ss/android/a/a/a;

    move-result-object v0

    new-instance v2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$5;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$5;-><init>()V

    .line 655
    invoke-interface {v0, v2}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/a/a/b/q;)Lcom/ss/android/a/a/a;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".TTFileProvider"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 661
    invoke-interface {v0, v1}, Lcom/ss/android/a/a/a;->a(Ljava/lang/String;)Lcom/ss/android/a/a/a;

    move-result-object v0

    .line 662
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->i()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;Lorg/json/JSONObject;)Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/ss/android/a/a/a;->a(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)Lcom/ss/android/a/a/a;

    move-result-object v0

    .line 663
    invoke-interface {v0}, Lcom/ss/android/a/a/a;->a()V

    .line 666
    invoke-static {}, Lcom/ss/android/downloadlib/g/a;->a()V

    .line 669
    invoke-static {p0}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/g;->d()Lcom/ss/android/b/a/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/ss/android/b/a/a;->a(I)V

    .line 670
    invoke-static {p0}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object p0

    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->f:Lcom/ss/android/a/a/c/a/a;

    invoke-virtual {p0, v0}, Lcom/ss/android/downloadlib/g;->a(Lcom/ss/android/a/a/c/a/a;)V

    .line 672
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/d;->j()Lcom/ss/android/socialbase/appdownloader/d;

    move-result-object p0

    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$7;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g$7;-><init>()V

    invoke-virtual {p0, v0}, Lcom/ss/android/socialbase/appdownloader/d;->a(Lcom/ss/android/socialbase/downloader/depend/af;)V

    return v1
.end method

.method public static c()V
    .locals 2

    .line 511
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 513
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->w()Z

    move-result v0

    .line 514
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->e:Z

    if-eq v1, v0, :cond_0

    .line 516
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->e:Z

    .line 517
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a()V

    :cond_0
    return-void
.end method

.method private static c(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    .line 280
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 281
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u901a\u8fc7\u5305\u540d\u8c03\u8d77 "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "LibHolder"

    invoke-static {p1, p0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static d()Lcom/ss/android/downloadlib/g;
    .locals 1

    .line 523
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;)V

    .line 524
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object v0

    return-object v0
.end method

.method public static e()V
    .locals 2

    .line 528
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/g;->g()V

    .line 529
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 530
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 531
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/f;->c(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method static synthetic f()Lorg/json/JSONObject;
    .locals 1

    .line 74
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->i()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method private static g()Landroid/content/Context;
    .locals 1

    .line 423
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;",
            ">;"
        }
    .end annotation

    .line 552
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d:Ljava/util/Map;

    return-object v0
.end method

.method private static i()Lorg/json/JSONObject;
    .locals 2

    .line 702
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 704
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->q()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0

    .line 706
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    return-object v0
.end method
