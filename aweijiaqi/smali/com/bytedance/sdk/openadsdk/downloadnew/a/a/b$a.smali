.class Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;
.super Lcom/bytedance/sdk/component/e/g;
.source "LibEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/ss/android/a/a/e/d;

.field private b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

.field private c:Lcom/bytedance/sdk/openadsdk/core/e/m;


# direct methods
.method private constructor <init>(Lcom/ss/android/a/a/e/d;)V
    .locals 1

    const-string v0, "LogTask"

    .line 76
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->d()Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->d()Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "ad_extra_data"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 82
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "open_ad_sdk_download_extra"

    .line 85
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 86
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v0}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    .line 89
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz p1, :cond_0

    .line 90
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public static a(Lcom/ss/android/a/a/e/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;
    .locals 1

    .line 72
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;-><init>(Lcom/ss/android/a/a/e/d;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .line 108
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 109
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "embeded_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 110
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "draw_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 111
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "draw_ad_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 112
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "banner_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 113
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "banner_call"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 114
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "banner_ad_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 115
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "feed_call"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 116
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "embeded_ad_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 117
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "interaction"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 118
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "interaction_call"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 119
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "interaction_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 120
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "slide_banner_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 121
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "splash_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 122
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "fullscreen_interstitial_ad"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 123
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "splash_ad_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 124
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "rewarded_video"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 125
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "rewarded_video_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 126
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "openad_sdk_download_complete_tag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 127
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "download_notification"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 128
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "landing_h5_download_ad_button"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 129
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "fullscreen_interstitial_ad_landingpage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 130
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "feed_video_middle_page"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 131
    invoke-virtual {p1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "stream"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private c()Landroid/content/Context;
    .locals 1

    .line 101
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    const-string v0, "LibEventLogger"

    .line 138
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    if-nez v1, :cond_0

    return-void

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object v1

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "label "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v3}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 145
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    .line 149
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 152
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v3}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v1, v3, v4, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 157
    invoke-virtual {v1}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    .line 158
    invoke-virtual {v1}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;->c(Lcom/ss/android/a/a/e/d;)Lorg/json/JSONObject;

    move-result-object v1

    .line 163
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    .line 164
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v3}, Lcom/ss/android/a/a/e/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 165
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v3}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "click"

    .line 167
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    return-void

    .line 170
    :cond_4
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/e/d;

    invoke-virtual {v5}, Lcom/ss/android/a/a/e/d;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v2, v5, v1}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_5
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    const-string v2, "upload event log error"

    .line 174
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/component/utils/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_6
    :goto_1
    return-void
.end method
