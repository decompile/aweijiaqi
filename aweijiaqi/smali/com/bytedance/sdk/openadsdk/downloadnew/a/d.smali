.class public Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;
.super Ljava/lang/Object;
.source "DMLibManager.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/u$a;
.implements Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;,
        Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;
    }
.end annotation


# static fields
.field public static j:Z = false

.field public static k:Z = false

.field public static l:Z = false


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private final C:Lcom/ss/android/a/a/c/d;

.field private D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

.field private E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/ITTAppDownloadListener;",
            ">;"
        }
    .end annotation
.end field

.field private F:Z

.field private G:Z

.field protected a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field protected final b:Lcom/bytedance/sdk/openadsdk/core/e/b;

.field protected final c:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field protected d:Ljava/lang/String;

.field protected final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected g:Z

.field protected h:Z

.field protected i:Z

.field protected m:Lcom/bytedance/sdk/openadsdk/IListenerManager;

.field private n:I

.field private o:Lcom/ss/android/a/a/c/a;

.field private p:Lcom/ss/android/a/a/c/b;

.field private q:Lcom/ss/android/a/a/c/c;

.field private r:Z

.field private final s:Ljava/util/concurrent/atomic/AtomicLong;

.field private final t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private u:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private v:Z

.field private w:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

.field private final y:Lcom/bytedance/sdk/component/utils/u;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 4

    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 97
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:I

    .line 102
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 105
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 106
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    .line 110
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s:Ljava/util/concurrent/atomic/AtomicLong;

    .line 111
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 113
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v:Z

    .line 117
    new-instance v0, Lcom/bytedance/sdk/component/utils/u;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3, p0}, Lcom/bytedance/sdk/component/utils/u;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/u$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:Lcom/bytedance/sdk/component/utils/u;

    .line 118
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Z

    .line 120
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    .line 121
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    .line 130
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->C:Lcom/ss/android/a/a/c/d;

    .line 258
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    .line 1157
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->E:Ljava/util/List;

    .line 1187
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->F:Z

    .line 1188
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->G:Z

    .line 288
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    .line 289
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 290
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    .line 291
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    .line 292
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ao()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/r/o;->c(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:I

    .line 293
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->hashCode()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 294
    invoke-static {p2}, Lcom/bytedance/sdk/component/utils/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Ljava/lang/String;

    .line 296
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "====tag==="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 298
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-nez p2, :cond_0

    const-string p1, "DMLibManager"

    const-string p2, "download create error: not a App type Ad!"

    .line 299
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 303
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p2

    if-nez p2, :cond_1

    .line 304
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V

    .line 307
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    .line 308
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/c$a;->a()Lcom/ss/android/b/a/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    .line 309
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/ss/android/b/a/a/a$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/a$a;->a()Lcom/ss/android/b/a/a/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Lcom/ss/android/a/a/c/a;

    .line 310
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/b$a;->a()Lcom/ss/android/b/a/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Lcom/ss/android/a/a/c/b;

    .line 311
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V
    .locals 9

    if-nez p1, :cond_0

    return-void

    .line 983
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->U()Ljava/lang/String;

    move-result-object v2

    .line 985
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 987
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 989
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    move-object v5, v0

    .line 991
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    const/4 v3, 0x1

    if-ne v0, v1, :cond_2

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 992
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v()Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "pop_up"

    invoke-static {v0, p1, v1, v8, v7}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 993
    sput-boolean v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j:Z

    .line 994
    sput-boolean v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Z

    .line 995
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$4;

    invoke-direct {v3, p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$4;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    invoke-static/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-direct/range {p0 .. p7}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 11

    move-object v9, p0

    .line 262
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    if-nez v0, :cond_0

    .line 263
    new-instance v10, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    iput-object v10, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    goto :goto_0

    :cond_0
    move-object v1, p1

    .line 265
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a(Ljava/lang/String;)V

    .line 266
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-wide v1, p2

    invoke-virtual {v0, p2, p3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a(J)V

    .line 267
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-wide v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b(J)V

    .line 268
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b(Ljava/lang/String;)V

    .line 269
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->c(Ljava/lang/String;)V

    .line 271
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iget-object v1, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(I)Z
    .locals 4

    .line 836
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->f()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 860
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h;->b(I)Z

    move-result p1

    if-nez p1, :cond_4

    const/high16 p1, 0x6400000

    .line 862
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->g()I

    move-result v0

    if-lez v0, :cond_0

    .line 863
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->g()I

    move-result p1

    .line 865
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->g()I

    move-result v0

    if-le p1, v0, :cond_4

    :cond_1
    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x4

    if-eq p1, v0, :cond_4

    goto :goto_0

    .line 845
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h;->b(I)Z

    move-result p1

    xor-int/lit8 v2, p1, 0x1

    :cond_4
    :goto_1
    return v2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 651
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->az()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p4, :cond_1

    .line 652
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Z

    .line 655
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 656
    new-instance p3, Landroid/content/Intent;

    const-string p4, "android.intent.action.VIEW"

    invoke-direct {p3, p4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 657
    invoke-virtual {p3, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 p2, 0x10000000

    .line 658
    invoke-virtual {p3, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string p2, "open_url"

    .line 659
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 660
    invoke-virtual {p0, p3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :catchall_0
    return v0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 9

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 925
    :cond_0
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 928
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    .line 931
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v4

    const-string v5, "tt_no_network"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 936
    :cond_2
    :goto_0
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(I)Z

    move-result v1

    .line 937
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->B()I

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    .line 938
    :goto_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->C()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    .line 939
    :goto_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->C()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    .line 940
    :goto_3
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->D()I

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x1

    goto :goto_4

    :cond_6
    const/4 v7, 0x0

    .line 942
    :goto_4
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 943
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_7

    return v2

    :cond_7
    if-eqz v5, :cond_8

    return v2

    .line 949
    :cond_8
    iput v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:I

    if-eqz v4, :cond_9

    return v1

    :cond_9
    return v0

    .line 954
    :cond_a
    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v4, :cond_c

    if-eqz v7, :cond_b

    return v2

    :cond_b
    const/4 p1, 0x3

    .line 958
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:I

    return v0

    .line 961
    :cond_c
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:I

    if-eqz v3, :cond_d

    return v1

    .line 964
    :cond_d
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->B()I

    move-result p1

    if-ne p1, v6, :cond_f

    .line 966
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->F:Z

    if-eqz p1, :cond_f

    .line 967
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->G:Z

    if-eqz p1, :cond_e

    return v0

    :cond_e
    return v1

    :cond_f
    return v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/c/c;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    return-object p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 2

    .line 1160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "registerMultiProcessListener, mMetaMd5:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DMLibManager"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 1164
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic b(Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/c/b;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Lcom/ss/android/a/a/c/b;

    return-object p0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 1

    const-string v0, "DMLibManager"

    .line 284
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/c/a;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Lcom/ss/android/a/a/c/a;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lorg/json/JSONObject;
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v()Lorg/json/JSONObject;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/lang/String;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/util/List;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->E:Ljava/util/List;

    return-object p0
.end method

.method private h(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 1284
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "quickapp_success"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1286
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "quickapp_fail"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private o()V
    .locals 0

    return-void
.end method

.method private p()V
    .locals 2

    const-string v0, "tryReleaseResource=="

    .line 392
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const-string v0, "tryReleaseResource==  mContext is null"

    .line 395
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 398
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    const-string v0, "tryReleaseResource==  activity is null"

    .line 402
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 405
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "tryReleaseResource==  isActivityAlive is true"

    .line 406
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 409
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y()V

    return-void
.end method

.method private declared-synchronized q()V
    .locals 3

    monitor-enter p0

    .line 422
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unbindDownload=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 424
    monitor-exit p0

    return-void

    .line 426
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 428
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    invoke-interface {v1}, Lcom/ss/android/a/a/c/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;I)V

    .line 430
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized r()V
    .locals 5

    monitor-enter p0

    .line 434
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bindDownload=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 437
    monitor-exit p0

    return-void

    .line 440
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 445
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 446
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->C:Lcom/ss/android/a/a/c/d;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;ILcom/ss/android/a/a/c/d;Lcom/ss/android/a/a/c/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private s()V
    .locals 10

    .line 470
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 475
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->f()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 476
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWriteExternal()Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    :try_start_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    .line 479
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 480
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    return-void

    :catchall_0
    nop

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->w()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "changeDownloadStatus, not support pause/continue function"

    .line 489
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 492
    :try_start_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    const-string v1, "\u5e94\u7528\u6b63\u5728\u4e0b\u8f7d..."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return-void

    .line 499
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 500
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V

    goto :goto_0

    .line 508
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v4

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Lcom/ss/android/a/a/c/b;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Lcom/ss/android/a/a/c/a;

    invoke-virtual/range {v2 .. v8}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;JILcom/ss/android/a/a/c/b;Lcom/ss/android/a/a/c/a;)V

    :goto_0
    return-void

    .line 512
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeDownloadStatus, the current status is1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 513
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v3

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Lcom/ss/android/a/a/c/b;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Lcom/ss/android/a/a/c/a;

    const/4 v8, 0x0

    new-instance v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$3;

    invoke-direct {v9, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$3;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-virtual/range {v1 .. v9}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;JILcom/ss/android/a/a/c/b;Lcom/ss/android/a/a/c/a;Lcom/ss/android/a/a/b/v;Lcom/ss/android/a/a/b/n;)V

    .line 528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeDownloadStatus, the current status is2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private t()Z
    .locals 4

    .line 534
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 537
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    .line 538
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ax()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 539
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private u()Z
    .locals 6

    .line 621
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 625
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-static {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 627
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x9

    .line 628
    iput v2, v1, Landroid/os/Message;->what:I

    .line 629
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:Lcom/bytedance/sdk/component/utils/u;

    const-wide/16 v3, 0xbb8

    invoke-virtual {v2, v1, v3, v4}, Lcom/bytedance/sdk/component/utils/u;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 631
    :cond_1
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    :goto_0
    return v0

    :cond_2
    :goto_1
    return v1
.end method

.method private v()Lorg/json/JSONObject;
    .locals 3

    .line 1019
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "download_type"

    .line 1021
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1023
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private w()V
    .locals 2

    .line 1033
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x()V

    .line 1034
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method private x()V
    .locals 0

    .line 1051
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e()V

    return-void
.end method

.method private y()V
    .locals 2

    .line 1198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "unregisterMultiProcessListener, mMetaMd5:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DMLibManager"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1202
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$6;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private z()V
    .locals 1

    .line 1291
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i()Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1297
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 1300
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v1

    .line 1304
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object v1

    .line 1307
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 0

    .line 316
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    .line 318
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o()V

    return-void
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V
    .locals 2

    .line 1237
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 1238
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Ljava/util/HashSet;

    .line 1241
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1243
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V

    return-void
.end method

.method public a(J)V
    .locals 1

    .line 456
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setActivity==activity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DMLibManager"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 380
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    .line 381
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 1

    .line 1248
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1250
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 1251
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    .line 1252
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Z

    if-eqz p1, :cond_2

    .line 1254
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 1257
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z()V

    :cond_2
    return-void

    :cond_3
    const/4 p1, 0x1

    .line 1261
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    :goto_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 387
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 1

    const/4 v0, 0x1

    .line 1137
    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;Z)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1145
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    if-eqz v0, :cond_1

    .line 1146
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 1149
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    .line 1154
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 11

    const/4 v0, 0x0

    if-eqz p1, :cond_8

    .line 1082
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 1085
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DMLibManager"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v1, :cond_1

    .line 1087
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c \u5f00\u59cb\u4e0a\u62a5 lp_open_dpl packageName "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "lp_open_dpl"

    invoke-static {v1, v3, v4, v5, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    :cond_1
    :try_start_0
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/r/o;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "lp_openurl_failed"

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    .line 1093
    :try_start_1
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/r/o;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    if-nez p2, :cond_2

    return v0

    .line 1098
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    const-string v4, "START_ONLY_FOR_ANDROID"

    .line 1099
    invoke-virtual {p2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1100
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1101
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_3

    const-string p1, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u5f00\u59cb\u8c03\u8d77\uff0c\u4e0a\u62a5 lp_openurl "

    .line 1102
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "lp_openurl"

    invoke-static {p1, p2, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_4

    .line 1106
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/k;->a()Lcom/bytedance/sdk/openadsdk/e/k;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-virtual {p1, p2, v4, v1}, Lcom/bytedance/sdk/openadsdk/e/k;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    return v1

    .line 1110
    :catch_0
    :try_start_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1111
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v4

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v7

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;ILjava/lang/String;ZLjava/util/Map;)Z

    .line 1113
    :cond_5
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_6

    const-string p1, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u5f00\u59cb\u8c03\u8d77\uff0c\u8c03\u8d77\u5f02\u5e38\uff0c\u4e0a\u62a5 lp_openurl_failed "

    .line 1114
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1, p2, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return v1

    .line 1120
    :cond_7
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_8

    const-string p1, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u8be5app\u672a\u5b89\u88c5 \uff0c\u4e0a\u62a5 lp_openurl_failed "

    .line 1121
    invoke-static {v2, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1, p2, v1, v3}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    :cond_8
    :goto_0
    return v0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 1

    const/4 v0, 0x1

    .line 1132
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public a(Z)Z
    .locals 0

    .line 782
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Z

    .line 783
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u()Z

    move-result p1

    return p1
.end method

.method public b()V
    .locals 1

    .line 336
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x1

    .line 339
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r:Z

    .line 341
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public b(J)V
    .locals 1

    .line 550
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-nez p1, :cond_0

    return-void

    .line 553
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 554
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Lcom/ss/android/a/a/c/c;

    invoke-interface {p2}, Lcom/ss/android/a/a/c/c;->a()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;Z)V

    .line 555
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public b(Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    .line 347
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r:Z

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 418
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v:Z

    return-void
.end method

.method public d()V
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->a()V

    .line 356
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q()V

    .line 358
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    .line 359
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 360
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 362
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(I)V

    .line 363
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 368
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 v0, 0x0

    .line 369
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    :cond_2
    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 1190
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->F:Z

    return-void
.end method

.method public e()V
    .locals 0

    .line 466
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s()V

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 1194
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->G:Z

    return-void
.end method

.method public f()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 545
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(J)V

    return-void
.end method

.method public f(Z)V
    .locals 0

    .line 1267
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    return-void
.end method

.method public g()V
    .locals 9

    .line 560
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v1, "feed_video_middle_page"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 566
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    .line 570
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 571
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->al()Lcom/bytedance/sdk/openadsdk/core/e/b;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 577
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;ILjava/lang/String;ZLjava/util/Map;)Z

    return-void

    .line 581
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    .line 586
    :cond_4
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 587
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 592
    :cond_5
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Z

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 593
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 597
    :cond_6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z()V

    return-void
.end method

.method public g(Z)V
    .locals 0

    .line 1279
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    return-void
.end method

.method public h()Z
    .locals 2

    .line 607
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->R()Lcom/bytedance/sdk/openadsdk/core/e/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->R()Lcom/bytedance/sdk/openadsdk/core/e/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/h;->b()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 5

    .line 789
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 791
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->c(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 794
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v3

    const-string v4, "tt_no_network"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/r;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 803
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 804
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 806
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w()V

    goto :goto_2

    .line 811
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 812
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 815
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e()V

    .line 817
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    goto :goto_0

    .line 819
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_6

    .line 820
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1

    .line 818
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_6
    :goto_1
    const/4 v1, 0x0

    :catch_0
    :goto_2
    return v1
.end method

.method public j()Z
    .locals 6

    .line 1056
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/e/b;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1059
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/b;->d()Ljava/lang/String;

    move-result-object v0

    .line 1060
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1062
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1065
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v3, "click_open"

    invoke-virtual {p0, v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1066
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/r/o;->h(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v0

    .line 1067
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v0, v5}, Lcom/bytedance/sdk/openadsdk/e/d;->i(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return v1
.end method

.method public k()Z
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "open_url_app"

    .line 704
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_a

    .line 705
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/m;->an()Lcom/bytedance/sdk/openadsdk/core/e/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/e/g;->a()Ljava/lang/String;

    move-result-object v2

    .line 706
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u542b\u6709deeplink\u94fe\u63a5 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DMLibManager"

    invoke-static {v5, v4}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DMLibManager \u542b\u6709deeplink\u94fe\u63a5\u5c1d\u8bd5deeplink\u8c03\u8d77 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "deepLink"

    invoke-static {v6, v4}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-nez v4, :cond_8

    .line 709
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 710
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 711
    invoke-virtual {v8, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 713
    iget-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v4, :cond_0

    .line 714
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\u542b\u6709deeplink\u94fe\u63a5\uff0c\u5f00\u59cb\u4e0a\u62a5 lp_open_dpl schema "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v4

    iget-object v9, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v11, "lp_open_dpl"

    invoke-static {v4, v9, v10, v11, v2}, Lcom/bytedance/sdk/openadsdk/e/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    const-string v4, "lp_deeplink_fail_realtime"

    const-string v9, "lp_openurl_failed"

    const-string v10, "deeplink_fail_realtime"

    if-eqz v2, :cond_6

    const-string v2, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5 "

    .line 719
    invoke-static {v5, v2}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-nez v2, :cond_1

    const/high16 v2, 0x10000000

    .line 721
    invoke-virtual {v8, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 725
    :cond_1
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 726
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v0, v2, v1, v11}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 727
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v12, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v2, v11, v12, v1, v6}, Lcom/bytedance/sdk/openadsdk/e/d;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 729
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 731
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/k;->a()Lcom/bytedance/sdk/openadsdk/e/k;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-boolean v8, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v1, v2, v6, v8}, Lcom/bytedance/sdk/openadsdk/e/k;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V

    .line 732
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v1, :cond_3

    const-string v1, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5\uff0c\u8fdb\u884c\u5f00\u59cb\u8c03\u8d77\u4e0a\u62a5 lp_openurl "

    .line 733
    invoke-static {v5, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v8, "lp_openurl"

    invoke-static {v1, v2, v6, v8}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v8, "lp_deeplink_success_realtime"

    invoke-static {v1, v2, v6, v8}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 737
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v8, "deeplink_success_realtime"

    invoke-static {v1, v2, v6, v8}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v7

    :catchall_0
    nop

    .line 741
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v1

    .line 742
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 743
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v11

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Z()Ljava/lang/String;

    move-result-object v12

    iget-object v13, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Ljava/lang/String;)I

    move-result v14

    iget-object v15, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/16 v16, 0x1

    const/16 v17, 0x0

    invoke-static/range {v11 .. v17}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;ILjava/lang/String;ZLjava/util/Map;)Z

    .line 745
    :cond_4
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v1, :cond_5

    const-string v1, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5\uff0c\u8c03\u8d77\u5931\u8d25 \u4e0a\u62a5lp_openurl_failed "

    .line 746
    invoke-static {v5, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v5, v9}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v5, v4}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 750
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v4, v10}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v3

    .line 756
    :cond_6
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v1, :cond_7

    const-string v1, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u672a\u5b89\u88c5\uff0c\u4e0a\u62a5lp_openurl_failed "

    .line 757
    invoke-static {v5, v1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v5, v9}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v5, v4}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 761
    :cond_7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v4, v10}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_8
    :goto_2
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_a

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_a

    .line 767
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 768
    :cond_9
    iput-boolean v7, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    .line 770
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    const-string v4, "open_fallback_url"

    invoke-virtual {v0, v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 771
    invoke-virtual/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2, v5, v4, v6}, Lcom/bytedance/sdk/openadsdk/e/d;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_a
    return v3
.end method

.method protected l()Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    if-nez v0, :cond_0

    .line 276
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    const/4 v1, 0x3

    .line 277
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object v0

    .line 278
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/b;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    return-object v0
.end method

.method protected m()Landroid/content/Context;
    .locals 1

    .line 698
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method protected n()V
    .locals 2

    .line 1040
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    if-nez v0, :cond_0

    return-void

    .line 1043
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1044
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 1045
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1046
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method
