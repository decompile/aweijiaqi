.class public Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;
.super Ljava/lang/Object;
.source "DMLibWebManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Lcom/ss/android/a/a/c/c;

.field private final d:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private e:Ljava/lang/String;

.field private f:Lcom/ss/android/a/a/c/a;

.field private g:Lcom/ss/android/a/a/c/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a:Ljava/lang/ref/WeakReference;

    .line 49
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->d:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 50
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->e:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->b:Ljava/lang/String;

    const/4 p1, 0x0

    .line 52
    invoke-static {p2, p4, p3, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/c$a;->a()Lcom/ss/android/b/a/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    .line 53
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->d:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Lcom/ss/android/b/a/a/a$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/a$a;->a()Lcom/ss/android/b/a/a/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->f:Lcom/ss/android/a/a/c/a;

    .line 54
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->d:Lcom/bytedance/sdk/openadsdk/core/e/m;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->e:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/b$a;->a()Lcom/ss/android/b/a/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->g:Lcom/ss/android/a/a/c/b;

    .line 55
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->p()V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 159
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->D()I

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    return v1

    :cond_2
    return v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->e:Ljava/lang/String;

    return-object p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 172
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->U()Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/f;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->Y()Lcom/bytedance/sdk/openadsdk/core/e/l;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 178
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/e/l;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    move-object v5, v0

    .line 180
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->X()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 181
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->o()Lorg/json/JSONObject;

    move-result-object v3

    const-string v7, "pop_up"

    invoke-static {v0, p1, v1, v7, v3}, Lcom/bytedance/sdk/openadsdk/e/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 182
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/e/m;->ak()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e$1;

    invoke-direct {v3, p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e$1;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    invoke-static/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/r/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/r/b$a;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;)Lorg/json/JSONObject;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->o()Lorg/json/JSONObject;

    move-result-object p0

    return-object p0
.end method

.method private k()Landroid/content/Context;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method private declared-synchronized l()V
    .locals 3

    monitor-enter p0

    .line 68
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->b:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 8

    monitor-enter p0

    .line 72
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/g;->e()Lcom/ss/android/b/a/b;

    move-result-object v1

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->k()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    .line 74
    invoke-interface {v0}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/c/c;->u()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v7

    .line 73
    invoke-interface/range {v1 .. v7}, Lcom/ss/android/b/a/b;->a(Landroid/content/Context;JLjava/lang/String;Lcom/ss/android/a/a/c/d;I)Z

    goto :goto_0

    .line 76
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->k()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;ILcom/ss/android/a/a/c/d;Lcom/ss/android/a/a/c/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()Z
    .locals 1

    .line 81
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->r()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private o()Lorg/json/JSONObject;
    .locals 3

    .line 203
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "download_type"

    const/4 v2, 0x3

    .line 205
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 207
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private p()V
    .locals 18

    move-object/from16 v0, p0

    .line 213
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->f()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWriteExternal()Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    .line 217
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_0

    .line 218
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    return-void

    :catchall_0
    nop

    .line 226
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ss/android/downloadlib/g;->e()Lcom/ss/android/b/a/b;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->k()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    iget-object v7, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->g:Lcom/ss/android/a/a/c/b;

    iget-object v8, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->f:Lcom/ss/android/a/a/c/a;

    const/4 v9, 0x0

    .line 228
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->hashCode()I

    move-result v10

    .line 227
    invoke-interface/range {v2 .. v10}, Lcom/ss/android/b/a/b;->a(Landroid/content/Context;Ljava/lang/String;ZLcom/ss/android/a/a/c/c;Lcom/ss/android/a/a/c/b;Lcom/ss/android/a/a/c/a;Lcom/ss/android/a/a/c/d;I)Landroid/app/Dialog;

    goto :goto_0

    .line 230
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v11

    iget-object v12, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->c:Lcom/ss/android/a/a/c/c;

    invoke-interface {v1}, Lcom/ss/android/a/a/c/c;->d()J

    move-result-wide v13

    const/4 v15, 0x2

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->g:Lcom/ss/android/a/a/c/b;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->f:Lcom/ss/android/a/a/c/a;

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    invoke-virtual/range {v11 .. v17}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;JILcom/ss/android/a/a/c/b;Lcom/ss/android/a/a/c/a;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->m()V

    return-void
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V
    .locals 0

    return-void
.end method

.method public a(J)V
    .locals 1

    .line 242
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->b:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;Z)V
    .locals 0

    return-void
.end method

.method public a(Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public b()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->m()V

    return-void
.end method

.method public b(Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 1

    .line 106
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->l()V

    .line 108
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 v0, 0x0

    .line 110
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->d:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->d:Lcom/bytedance/sdk/openadsdk/core/e/m;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    goto :goto_0

    .line 150
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->p()V

    :goto_0
    return-void
.end method

.method public f()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 237
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/e;->a(J)V

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
