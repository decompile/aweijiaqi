.class public Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ThemeStatusBroadcastReceiver.java"


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/theme/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/theme/a;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;->a:Lcom/bytedance/sdk/openadsdk/theme/a;

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string p1, "ThemeStatusBroadcastReceiver"

    const-string v0, "====\u4e3b\u9898\u72b6\u6001\u66f4\u65b0===="

    .line 27
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    const-string v0, "theme_status_change"

    .line 31
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    .line 33
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/theme/ThemeStatusBroadcastReceiver;->a:Lcom/bytedance/sdk/openadsdk/theme/a;

    if-nez p2, :cond_1

    return-void

    .line 36
    :cond_1
    invoke-interface {p2, p1}, Lcom/bytedance/sdk/openadsdk/theme/a;->a_(I)V

    return-void
.end method
