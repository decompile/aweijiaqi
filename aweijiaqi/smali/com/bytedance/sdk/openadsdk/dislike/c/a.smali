.class public Lcom/bytedance/sdk/openadsdk/dislike/c/a;
.super Ljava/lang/Object;
.source "DislikeInfoImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/DislikeInfo;


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/dislike/c/b;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/c/a;->a:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-void
.end method


# virtual methods
.method public getFilterWords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/FilterWord;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/c/a;->a:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPersonalizationPrompt()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/c/a;->a:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 34
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v0

    return-object v0
.end method
