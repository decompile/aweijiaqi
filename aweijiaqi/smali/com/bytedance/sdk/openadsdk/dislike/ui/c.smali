.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/c;
.super Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;
.source "TTDislikeDialog.java"


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/dislike/b/d;

.field private b:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;

.field private final c:Lcom/bytedance/sdk/openadsdk/dislike/c/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    const-string v0, "tt_dislikeDialog_new"

    .line 44
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;-><init>(Landroid/content/Context;I)V

    .line 45
    invoke-super {p0, p2}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;->setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    .line 46
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->c:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a:Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->c:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-object p0
.end method

.method private b()V
    .locals 2

    .line 81
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x11

    .line 83
    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    const v1, 0x3eae147b    # 0.34f

    .line 84
    invoke-virtual {v0, v1}, Landroid/view/Window;->setDimAmount(F)V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .line 89
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_dislike_layout"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;

    .line 90
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->c:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;Lcom/bytedance/sdk/openadsdk/dislike/b/c;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/dislike/b/d;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a:Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    return-void
.end method

.method public dismiss()V
    .locals 1

    .line 129
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;->dismiss()V

    .line 130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a:Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    if-eqz v0, :cond_0

    .line 131
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/dislike/b/d;->c()V

    :cond_0
    return-void
.end method

.method public getLayoutId()I
    .locals 2

    .line 64
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_dislike_dialog_layout"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .line 76
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->b()Lcom/bytedance/sdk/openadsdk/dislike/a/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x43ac8000    # 345.0f

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/a/c;->a(Landroid/content/Context;F)I

    move-result v0

    .line 77
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v1
.end method

.method public getTTDislikeListViewIds()[I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 70
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "tt_filer_words_lv"

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 55
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    .line 56
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->setCanceledOnTouchOutside(Z)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->setCancelable(Z)V

    .line 58
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b()V

    .line 59
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->c()V

    return-void
.end method

.method public setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 140
    :cond_0
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;->setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    .line 141
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;

    if-eqz v0, :cond_1

    .line 142
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->setDislikeInfo(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    :cond_1
    return-void
.end method

.method public show()V
    .locals 1

    .line 120
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/TTDislikeDialogAbstract;->show()V

    .line 121
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a:Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    if-eqz v0, :cond_0

    .line 122
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/dislike/b/d;->b()V

    :cond_0
    return-void
.end method
