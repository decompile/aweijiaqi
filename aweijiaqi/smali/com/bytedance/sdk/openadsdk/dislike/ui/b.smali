.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/b;
.super Landroid/app/Dialog;
.source "TTDislikeCommentDialog.java"


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

.field private b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

.field private c:Lcom/bytedance/sdk/openadsdk/dislike/b/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    const-string v0, "quick_option_dialog"

    .line 31
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 32
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/b;)Lcom/bytedance/sdk/openadsdk/dislike/b/b;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->c:Lcom/bytedance/sdk/openadsdk/dislike/b/b;

    return-object p0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x1

    .line 57
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->setCanceledOnTouchOutside(Z)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->setCancelable(Z)V

    .line 60
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 62
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, -0x1

    .line 63
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v2, -0x2

    .line 64
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 65
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->c()V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/dislike/b/b;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->c:Lcom/bytedance/sdk/openadsdk/dislike/b/b;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 96
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    .line 97
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    :cond_1
    return-void
.end method

.method public dismiss()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b()V

    .line 85
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 86
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->c:Lcom/bytedance/sdk/openadsdk/dislike/b/b;

    if-eqz v0, :cond_0

    .line 87
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/dislike/b/b;->b()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 38
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "tt_dislike_comment_layout"

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    .line 41
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/b$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/b;)V

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;Lcom/bytedance/sdk/openadsdk/dislike/b/a;)V

    .line 54
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->b()V

    return-void
.end method

.method public show()V
    .locals 3

    .line 70
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->a()V

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->c:Lcom/bytedance/sdk/openadsdk/dislike/b/b;

    if-eqz v0, :cond_0

    .line 73
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/dislike/b/b;->a()V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/16 v2, 0x50

    .line 77
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 78
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method
