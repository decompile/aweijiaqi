.class Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;
.super Ljava/lang/Object;
.source "TTDislikeDialog.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/dislike/b/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/dislike/b/d;->a()V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->dismiss()V

    return-void
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/FilterWord;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/dislike/b/d;->a(ILcom/bytedance/sdk/openadsdk/FilterWord;)V

    .line 104
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->dismiss()V

    return-void
.end method

.method public b()V
    .locals 4

    .line 109
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/b/d;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/b/d;->a(Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;)V

    .line 112
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->b()Lcom/bytedance/sdk/openadsdk/dislike/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/a/c;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Z)V

    .line 113
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->a()Lcom/bytedance/sdk/openadsdk/dislike/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c$1;->a:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->b(Lcom/bytedance/sdk/openadsdk/dislike/ui/c;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    move-result-object v2

    const-string v3, "ad_explation_click"

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/a/b;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/lang/String;)V

    return-void
.end method
