.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;
.super Landroid/widget/FrameLayout;
.source "TTDislikeToast.java"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a:Landroid/os/Handler;

    const/16 p2, 0x8

    .line 40
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->setVisibility(I)V

    const/4 p2, 0x0

    .line 41
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->setClickable(Z)V

    .line 42
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->setFocusable(Z)V

    .line 43
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;)Landroid/widget/TextView;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .line 48
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    const/4 p1, 0x0

    .line 49
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 50
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 51
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    .line 54
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 55
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->b()Lcom/bytedance/sdk/openadsdk/dislike/a/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-interface {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/dislike/a/c;->a(Landroid/content/Context;F)I

    move-result v2

    .line 56
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->b()Lcom/bytedance/sdk/openadsdk/dislike/a/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->getContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41400000    # 12.0f

    invoke-interface {v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/dislike/a/c;->a(Landroid/content/Context;F)I

    move-result v3

    .line 57
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v2, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 58
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 60
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 61
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 63
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 64
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    const-string p1, "#CC000000"

    .line 65
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 66
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a/a;->b()Lcom/bytedance/sdk/openadsdk/dislike/a/c;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-interface {p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/a/c;->a(Landroid/content/Context;F)I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 67
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 95
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_dislike_feedback_success"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .line 73
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a:Landroid/os/Handler;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast$1;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 86
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a:Landroid/os/Handler;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast$2;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;)V

    const-wide/16 v1, 0x7d0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b()V
    .locals 2

    .line 99
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_dislike_feedback_repeat"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a(Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 2

    const/16 v0, 0x8

    .line 103
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method
