.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;
.super Landroid/widget/LinearLayout;
.source "TTDislikeLayout.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

.field private e:Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

.field private f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

.field private g:Lcom/bytedance/sdk/openadsdk/dislike/b/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)Lcom/bytedance/sdk/openadsdk/dislike/b/c;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->g:Lcom/bytedance/sdk/openadsdk/dislike/b/c;

    return-object p0
.end method

.method private a()V
    .locals 2

    .line 65
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_personalization_layout"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->a:Landroid/view/View;

    .line 66
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_personalization_name"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->b:Landroid/widget/TextView;

    .line 68
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_edit_suggestion"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->c:Landroid/widget/TextView;

    .line 69
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_filer_words_lv"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->d:Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

    .line 79
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$2;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-object p0
.end method

.method private b()V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->a:Landroid/view/View;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$3;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->b()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/d;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

    .line 107
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$4;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout$4;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/d;->a(Lcom/bytedance/sdk/openadsdk/dislike/ui/d$a;)V

    .line 116
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->d:Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->d:Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;->setDislikeInfo(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;Lcom/bytedance/sdk/openadsdk/dislike/b/c;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    .line 59
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->g:Lcom/bytedance/sdk/openadsdk/dislike/b/c;

    .line 60
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->a()V

    .line 61
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->b()V

    return-void
.end method

.method public setDislikeInfo(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    .line 124
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    .line 125
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->d:Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeListView;->setDislikeInfo(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->b()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/d;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method
