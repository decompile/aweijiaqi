.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/a;
.super Ljava/lang/Object;
.source "TTAdDislikeImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdDislike;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

.field private c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

.field private d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

.field private e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Z

.field private h:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;

.field private j:Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/lang/String;Z)V
    .locals 2

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 73
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->b(Ljava/lang/String;)V

    const-string p3, "other"

    .line 74
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a(Ljava/lang/String;)V

    const-string p3, "Dislike \u521d\u59cb\u5316\u5fc5\u987b\u4f7f\u7528activity,\u8bf7\u5728TTAdManager.createAdNative(activity)\u4e2d\u4f20\u5165"

    .line 75
    invoke-static {p1, p3}, Lcom/bytedance/sdk/component/utils/n;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 76
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    .line 78
    iput-boolean p4, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->g:Z

    .line 79
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;Ljava/lang/String;Z)V

    return-void
.end method

.method private a()V
    .locals 3

    .line 88
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    .line 89
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->a(Lcom/bytedance/sdk/openadsdk/dislike/b/d;)V

    .line 143
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    .line 144
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$2;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a(Lcom/bytedance/sdk/openadsdk/dislike/b/b;)V

    .line 185
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->g:Z

    if-eqz v0, :cond_0

    .line 186
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    .line 187
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 188
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b()V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->j:Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;

    return-object p0
.end method

.method private b()V
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->show()V

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-object p0
.end method

.method private c()V
    .locals 5

    .line 275
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    if-nez v0, :cond_0

    return-void

    .line 278
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "slide_banner_ad"

    .line 279
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "banner_ad"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "embeded_ad"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->h:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->h:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 285
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->i:Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;

    if-nez v1, :cond_3

    .line 286
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->i:Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;

    .line 288
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->i:Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$3;

    invoke-direct {v2, p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$3;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;Ljava/lang/String;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a$a;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c()V

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Lcom/bytedance/sdk/openadsdk/dislike/ui/c;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Ljava/lang/ref/SoftReference;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->h:Ljava/lang/ref/SoftReference;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/dislike/ui/a;)Landroid/content/Context;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    .line 271
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->h:Ljava/lang/ref/SoftReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    .line 230
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V

    return-void
.end method

.method public isShow()Z
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->isShowing()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 264
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    if-eqz v1, :cond_1

    .line 265
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->isShowing()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public resetDislikeStatus()V
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->d:Lcom/bytedance/sdk/openadsdk/dislike/ui/b;

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/b;->a()V

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    if-eqz v0, :cond_1

    .line 253
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->c()V

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public setDislikeInteractionCallback(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->j:Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;

    return-void
.end method

.method public setDislikeSource(Ljava/lang/String;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public showDislikeDialog()V
    .locals 4

    .line 195
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    instance-of v1, v0, Landroid/app/Activity;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->g:Z

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->e:Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeToast;->b()V

    return-void

    .line 202
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->b:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/c/b;->i()Ljava/lang/String;

    move-result-object v1

    const-string v3, "interaction"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 204
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->a:Landroid/content/Context;

    const-string v1, "tt_dislike_feedback_repeat"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_2
    if-eqz v0, :cond_3

    .line 207
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->isShow()Z

    move-result v0

    if-nez v0, :cond_3

    .line 208
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/a;->c:Lcom/bytedance/sdk/openadsdk/dislike/ui/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/c;->show()V

    :cond_3
    return-void
.end method
