.class public Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;
.super Landroid/widget/LinearLayout;
.source "TTDislikeCommentLayout.java"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

.field private f:Lcom/bytedance/sdk/openadsdk/dislike/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)Landroid/widget/EditText;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)Lcom/bytedance/sdk/openadsdk/dislike/c/b;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)Lcom/bytedance/sdk/openadsdk/dislike/b/a;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/b/a;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)Landroid/widget/TextView;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->d:Landroid/widget/TextView;

    return-object p0
.end method

.method private d()V
    .locals 2

    .line 58
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_comment_content"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    .line 59
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->setEditTextInputSpace(Landroid/widget/EditText;)V

    .line 60
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_comment_commit"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->c:Landroid/widget/TextView;

    .line 62
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_comment_close"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->a:Landroid/widget/ImageView;

    .line 80
    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$2;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_comment_number"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->d:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$3;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;)Landroid/widget/TextView;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method public static setEditTextInputSpace(Landroid/widget/EditText;)V
    .locals 3

    .line 113
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$4;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout$4;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 125
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v2, 0x1f4

    invoke-direct {v0, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/dislike/c/b;Lcom/bytedance/sdk/openadsdk/dislike/b/a;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    .line 53
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->f:Lcom/bytedance/sdk/openadsdk/dislike/b/a;

    .line 54
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->d()V

    return-void
.end method

.method public b()V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 145
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setDislikeModel(Lcom/bytedance/sdk/openadsdk/dislike/c/b;)V
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/ui/TTDislikeCommentLayout;->e:Lcom/bytedance/sdk/openadsdk/dislike/c/b;

    return-void
.end method
