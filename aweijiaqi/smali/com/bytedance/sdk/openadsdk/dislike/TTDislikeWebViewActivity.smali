.class public Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;
.super Landroid/app/Activity;
.source "TTDislikeWebViewActivity.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 44
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->h:Z

    return-void
.end method

.method private a()V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 75
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity$2;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p0, v2, v2}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity$2;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/w;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)Z
    .locals 0

    .line 24
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->h:Z

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;Z)Z
    .locals 0

    .line 24
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->g:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string p1, "tt_activity_lite_web_layout"

    .line 49
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->setContentView(I)V

    const-string p1, "tt_lite_web_back"

    .line 50
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->a:Landroid/view/View;

    const-string p1, "tt_lite_web_title"

    .line 51
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->b:Landroid/widget/TextView;

    const-string p1, "tt_lite_web_view"

    .line 52
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/r;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    .line 53
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->a:Landroid/view/View;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity$1;-><init>(Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->a()V

    .line 61
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "ad_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->f:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "tag"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->e:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "log_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->d:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "label"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->g:Ljava/lang/String;

    .line 66
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->c:Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;->finish()V

    :goto_0
    return-void
.end method
