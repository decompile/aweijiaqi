.class public final Lcom/bytedance/sdk/openadsdk/TTAdSdk;
.super Ljava/lang/Object;
.source "TTAdSdk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/TTAdSdk$InitCallback;
    }
.end annotation


# static fields
.field private static volatile a:Z = false

.field private static final b:Lcom/bytedance/sdk/openadsdk/TTAdManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/u;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/u;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(JZLcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 7

    .line 373
    new-instance v6, Lcom/bytedance/sdk/openadsdk/TTAdSdk$5;

    const-string v1, "initMustBeCall"

    move-object v0, v6

    move-wide v2, p0

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$5;-><init>(Ljava/lang/String;JZLcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    invoke-static {v6}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 0

    .line 59
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 0

    .line 59
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .line 59
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    return v0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .line 59
    sput-boolean p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    return p0
.end method

.method static synthetic b()Lcom/bytedance/sdk/openadsdk/TTAdManager;
    .locals 1

    .line 59
    sget-object v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    return-object v0
.end method

.method static synthetic b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 0

    .line 59
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->e(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 4

    .line 111
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 114
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    const-string v2, "1"

    const-string v3, "0"

    if-eqz v1, :cond_1

    move-object v1, v2

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_0
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/p/a;->b(ILjava/lang/String;)V

    const/4 v0, 0x3

    .line 116
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/p/a;->a(ILjava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->e()V

    .line 118
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getAppId()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x2

    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/p/a;->a(ILjava/lang/String;)V

    .line 120
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    const-string v1, "com.bytedance.sdk.openadsdk.core.GlobalInfo"

    invoke-virtual {p0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    .line 121
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/p/a;->b(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 123
    :catch_0
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/p/a;->b(ILjava/lang/String;)V

    :goto_1
    const/4 p0, 0x0

    const-string v0, "3.6.1.4"

    .line 125
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/p/a;->a(ILjava/lang/String;)V

    return-void
.end method

.method private static c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 2

    .line 183
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 186
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 187
    invoke-static {p0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->getInstance(Landroid/content/Context;)Lcom/bytedance/sdk/component/net/tnc/AppConfig;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/net/tnc/AppConfig;->setThreadPoolExecutor(Ljava/util/concurrent/ThreadPoolExecutor;)V

    .line 188
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/preload/geckox/b;->a(Ljava/util/concurrent/ThreadPoolExecutor;)V

    .line 190
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/multipro/d;->a(Landroid/content/Context;)V

    .line 191
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isSupportMultiProcess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->a()V

    .line 195
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->b()Lcom/bytedance/sdk/openadsdk/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Lcom/bytedance/sdk/component/net/NetClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/net/NetClient;->getGetExecutor()Lcom/bytedance/sdk/component/net/executor/GetExecutor;

    move-result-object v0

    const-string v1, "/api/ad/union/ping"

    .line 198
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/r/o;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->setUrl(Ljava/lang/String;)V

    .line 200
    new-instance v1, Lcom/bytedance/sdk/openadsdk/TTAdSdk$2;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$2;-><init>()V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/net/executor/GetExecutor;->enqueue(Lcom/bytedance/sdk/component/net/callback/NetCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :catchall_0
    :cond_2
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->updateAdConfig(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 220
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 221
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->a()V

    return-void
.end method

.method private static d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 1

    const-string v0, "Context is null, please check."

    .line 226
    invoke-static {p0, v0}, Lcom/bytedance/sdk/component/utils/n;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "TTAdConfig is null, please check."

    .line 227
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/utils/n;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/o;->a(Landroid/content/Context;)V

    .line 229
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 230
    invoke-static {}, Lcom/bytedance/sdk/component/utils/j;->b()V

    :cond_0
    return-void
.end method

.method private static e(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 2

    .line 235
    new-instance v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;

    const-string v1, "init sync"

    invoke-direct {v0, v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    .line 282
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->c()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/TTAdSdk$4;

    invoke-direct {v1, p1, p0}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$4;-><init>(Lcom/bytedance/sdk/openadsdk/TTAdConfig;Landroid/content/Context;)V

    const-wide/16 p0, 0x2710

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private static f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 1

    .line 320
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getHttpStack()Lcom/bytedance/sdk/component/adnet/face/IHttpStack;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 321
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getHttpStack()Lcom/bytedance/sdk/component/adnet/face/IHttpStack;

    move-result-object p0

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/component/adnet/face/IHttpStack;)V

    .line 324
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isAsyncInit()Z

    move-result p0

    sput-boolean p0, Lcom/bytedance/sdk/openadsdk/core/l;->a:Z

    .line 325
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getCustomController()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object p0

    sput-object p0, Lcom/bytedance/sdk/openadsdk/core/l;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    .line 327
    sget-object p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setAppId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 328
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getAppName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setName(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 330
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setKeywords(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 331
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setData(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 332
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getTitleBarTheme()I

    move-result v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setTitleBarTheme(I)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 333
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isAllowShowNotify()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setAllowShowNotifiFromSDK(Z)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 334
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isAllowShowPageWhenScreenLock()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setAllowLandingPageShowWhenScreenLock(Z)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 335
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getDirectDownloadNetworkType()[I

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setDirectDownloadNetworkType([I)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 336
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isUseTextureView()Z

    move-result v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->isUseTextureView(Z)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 337
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getTTDownloadEventLogger()Lcom/bytedance/sdk/openadsdk/TTDownloadEventLogger;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setTTDownloadEventLogger(Lcom/bytedance/sdk/openadsdk/TTDownloadEventLogger;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 338
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getNeedClearTaskReset()[Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setNeedClearTaskReset([Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 339
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getTTSecAbs()Lcom/bytedance/sdk/openadsdk/TTSecAbs;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setTTSecAbs(Lcom/bytedance/sdk/openadsdk/TTSecAbs;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p0

    .line 340
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getCustomController()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->setCustomController(Lcom/bytedance/sdk/openadsdk/TTCustomController;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    .line 341
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 344
    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 345
    sget-object p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    invoke-interface {p0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->openDebugMode()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    .line 347
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/g;->a()V

    .line 348
    invoke-static {}, Lcom/bytedance/sdk/component/video/d/c;->a()V

    .line 349
    invoke-static {}, Lcom/bytedance/sdk/component/net/utils/NetLog;->openDebugMode()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_1
    return-void
.end method

.method public static getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;
    .locals 1

    .line 315
    sget-object v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    return-object v0
.end method

.method public static init(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)Lcom/bytedance/sdk/openadsdk/TTAdManager;
    .locals 6

    .line 82
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 83
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 87
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    if-eqz v2, :cond_0

    .line 88
    sget-object p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    return-object p0

    :cond_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 91
    :try_start_0
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 92
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 93
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->e(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 94
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    .line 95
    invoke-static {v4, v5, v3, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a(JZLcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 96
    sput-boolean v2, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 98
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 100
    sput-boolean v3, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    .line 102
    :goto_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 103
    sput-boolean v2, Lcom/bytedance/sdk/openadsdk/core/l;->c:Z

    .line 104
    sget-object p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b:Lcom/bytedance/sdk/openadsdk/TTAdManager;

    return-object p0

    .line 84
    :cond_1
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "Wrong Thread ! Please exec TTAdSdk.init in main thread."

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static init(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;Lcom/bytedance/sdk/openadsdk/TTAdSdk$InitCallback;)V
    .locals 4

    .line 136
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 137
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 141
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    if-eqz v2, :cond_1

    if-eqz p2, :cond_0

    .line 143
    invoke-interface {p2}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$InitCallback;->success()V

    :cond_0
    return-void

    .line 147
    :cond_1
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const/4 v0, 0x1

    .line 149
    invoke-static {v2, v3, v0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a(JZLcom/bytedance/sdk/openadsdk/TTAdConfig;)V

    .line 150
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->c()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/TTAdSdk$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/TTAdSdk$1;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;Lcom/bytedance/sdk/openadsdk/TTAdSdk$InitCallback;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 174
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/l;->c:Z

    return-void

    .line 138
    :cond_2
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "Wrong Thread ! Please exec TTAdSdk.init in main thread."

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static isInitSuccess()Z
    .locals 1

    .line 71
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a:Z

    return v0
.end method

.method public static updateAdConfig(Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 361
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 362
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/h;->d(Ljava/lang/String;)V

    .line 365
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 366
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->getKeywords()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/h;->c(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public static updatePaid(Z)V
    .locals 1

    .line 400
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/h;->b(Z)V

    return-void
.end method
