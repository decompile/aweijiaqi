.class Lcom/bytedance/sdk/openadsdk/e/j$a;
.super Ljava/lang/Object;
.source "LandingPageLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/e/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/e/j;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/e/j;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/e/j;Lcom/bytedance/sdk/openadsdk/e/j$1;)V
    .locals 0

    .line 376
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/j$a;-><init>(Lcom/bytedance/sdk/openadsdk/e/j;)V

    return-void
.end method


# virtual methods
.method public getUrl()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 398
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->c(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readHtml(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 403
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->d(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Lcom/bytedance/sdk/openadsdk/e/j;Ljava/util/concurrent/ConcurrentHashMap;)Ljava/util/concurrent/ConcurrentHashMap;

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->d(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/j;->d(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/e/d;

    goto :goto_0

    .line 413
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/e/d;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/e/d;-><init>()V

    .line 414
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/j;->d(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/e/d;->a(Ljava/lang/String;)V

    move-object p1, v0

    .line 417
    :goto_0
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/e/d;->b(Ljava/lang/String;)V

    .line 418
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/e/j;->e(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/List;

    move-result-object p2

    if-nez p2, :cond_3

    .line 419
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Lcom/bytedance/sdk/openadsdk/e/j;Ljava/util/List;)Ljava/util/List;

    .line 421
    :cond_3
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/e/j;->e(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_1
    return-void
.end method

.method public readPercent(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "measure height: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Lcom/bytedance/sdk/openadsdk/e/j;)Landroid/webkit/WebView;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/j;->a(Lcom/bytedance/sdk/openadsdk/e/j;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getMeasuredHeight()I

    move-result v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LandingPageLog"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read percent: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Float;->intValue()I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    const/16 v2, 0x64

    goto :goto_1

    :cond_1
    if-gez p1, :cond_2

    goto :goto_1

    :cond_2
    move v2, p1

    .line 393
    :catchall_0
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/j$a;->a:Lcom/bytedance/sdk/openadsdk/e/j;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/e/j;->b(Lcom/bytedance/sdk/openadsdk/e/j;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method
