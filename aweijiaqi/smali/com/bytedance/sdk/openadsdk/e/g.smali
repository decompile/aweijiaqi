.class public Lcom/bytedance/sdk/openadsdk/e/g;
.super Landroid/os/HandlerThread;
.source "AdEventThread.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/e/g$b;,
        Lcom/bytedance/sdk/openadsdk/e/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/bytedance/sdk/openadsdk/e/i;",
        ">",
        "Landroid/os/HandlerThread;",
        "Landroid/os/Handler$Callback;"
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String; = "AdEventThread"

.field public static b:Ljava/lang/String; = "ttad_bk"


# instance fields
.field public final c:Lcom/bytedance/sdk/openadsdk/e/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/e/e<",
            "TT;>;"
        }
    .end annotation
.end field

.field public d:Lcom/bytedance/sdk/openadsdk/core/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field public f:J

.field public g:Z

.field public h:I

.field public i:Landroid/os/Handler;

.field public final j:Lcom/bytedance/sdk/openadsdk/e/g$a;

.field public final k:Lcom/bytedance/sdk/openadsdk/e/g$b;

.field protected l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/e/e;Lcom/bytedance/sdk/openadsdk/core/p;Lcom/bytedance/sdk/openadsdk/e/g$b;Lcom/bytedance/sdk/openadsdk/e/g$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/e/e<",
            "TT;>;",
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "TT;>;",
            "Lcom/bytedance/sdk/openadsdk/e/g$b;",
            "Lcom/bytedance/sdk/openadsdk/e/g$a;",
            ")V"
        }
    .end annotation

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tt_pangle_thread__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/bytedance/sdk/openadsdk/e/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 61
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    .line 62
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/e/g;->j:Lcom/bytedance/sdk/openadsdk/e/g$a;

    .line 63
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    .line 64
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 65
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    const/4 p1, 0x0

    .line 66
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->l:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/e/e;Lcom/bytedance/sdk/openadsdk/core/p;Lcom/bytedance/sdk/openadsdk/e/g$b;Lcom/bytedance/sdk/openadsdk/e/g$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/e/e<",
            "TT;>;",
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "TT;>;",
            "Lcom/bytedance/sdk/openadsdk/e/g$b;",
            "Lcom/bytedance/sdk/openadsdk/e/g$a;",
            ")V"
        }
    .end annotation

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tt_pangle_thread__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 72
    sput-object p2, Lcom/bytedance/sdk/openadsdk/e/g;->a:Ljava/lang/String;

    .line 73
    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    .line 74
    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/e/g;->j:Lcom/bytedance/sdk/openadsdk/e/g$a;

    .line 75
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    .line 76
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/e/g;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 77
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    const/4 p1, 0x0

    .line 78
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->l:Z

    return-void
.end method

.method private a()V
    .locals 4

    .line 113
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/e/g$b;->d:I

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v2, v2, Lcom/bytedance/sdk/openadsdk/e/g$b;->e:J

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/e;->a(IJ)V

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/e;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    .line 116
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/e;->b()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    .line 117
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleInitEvent serverBusy, retryCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->h()V

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    const/16 v1, 0x64

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/e;->a(ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 122
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->b(Ljava/util/List;)V

    const-string v0, "onHandleInitEvent,mCloseSaveAndRetry is false, read db event data"

    .line 123
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleInitEvent cacheData count = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->e()V

    :goto_0
    return-void
.end method

.method private a(IJ)V
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 366
    iput p1, v0, Landroid/os/Message;->what:I

    .line 367
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/e/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->c(Ljava/util/List;)V

    .line 251
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Ljava/lang/Object;)V

    const-string v0, "onHandleReceivedAdEvent mCloseSaveAndRetry is false, save event into db"

    .line 252
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 253
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "onHandleReceivedAdEvent"

    .line 257
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->n()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "onHandleReceivedAdEvent upload"

    .line 260
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 261
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->e()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .line 444
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/g;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lcom/bytedance/sdk/openadsdk/e/h;)Z
    .locals 1

    .line 412
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/e/h;->b:I

    const/16 v0, 0x1fd

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private b()V
    .locals 5

    .line 155
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->j:Lcom/bytedance/sdk/openadsdk/e/g$a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/g$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    .line 157
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v1, v1, Lcom/bytedance/sdk/openadsdk/e/g$b;->c:J

    invoke-direct {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/g;->a(IJ)V

    const-string v0, "onHandleServerBusyRetryEvent, no net"

    .line 158
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    const/16 v1, 0x64

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/e;->a(ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 162
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->c(Ljava/util/List;)V

    .line 163
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/h;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "onHandleServerBusyRetryEvent, empty list start routine"

    .line 164
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->o()V

    .line 169
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->j()V

    return-void

    .line 172
    :cond_1
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/e/h;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 174
    iget-boolean v2, v1, Lcom/bytedance/sdk/openadsdk/e/h;->a:Z

    if-eqz v2, :cond_2

    const-string v0, "onHandleServerBusyRetryEvent, success"

    .line 175
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 177
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->g()V

    .line 179
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->f()V

    goto :goto_0

    .line 181
    :cond_2
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Lcom/bytedance/sdk/openadsdk/e/h;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 182
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    .line 183
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v2, v1}, Lcom/bytedance/sdk/openadsdk/e/e;->a(I)V

    .line 184
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/e/g$b;->d:I

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v3, v3, Lcom/bytedance/sdk/openadsdk/e/g$b;->e:J

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Ljava/util/List;IJ)V

    .line 187
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->h()V

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleServerBusyRetryEvent, serverbusy, count = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_3
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/g;->b(Lcom/bytedance/sdk/openadsdk/e/h;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 192
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->g()V

    .line 194
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->f()V

    goto :goto_0

    .line 196
    :cond_4
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->l:Z

    if-nez v1, :cond_5

    .line 198
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->i()V

    const-string v0, "onHandleServerBusyRetryEvent, net fail"

    .line 199
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_5
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    .line 203
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v2, v1}, Lcom/bytedance/sdk/openadsdk/e/e;->a(I)V

    .line 204
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/e/g$b;->d:I

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v3, v3, Lcom/bytedance/sdk/openadsdk/e/g$b;->e:J

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Ljava/util/List;IJ)V

    .line 207
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->l()V

    :cond_6
    :goto_0
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 136
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 140
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/e/i;

    .line 142
    invoke-interface {v2}, Lcom/bytedance/sdk/openadsdk/e/i;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/e/i;

    .line 145
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/e/i;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 146
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    :goto_2
    const-string p1, "reloadCacheList adEventList is empty======"

    .line 137
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/e/h;)Z
    .locals 0

    .line 422
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/e/h;->d:Z

    return p0
.end method

.method private c()V
    .locals 1

    .line 230
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "onHandleRoutineRetryEvent"

    .line 234
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 235
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->e()V

    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 267
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->l:Z

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 270
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_1

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start and return, checkAndDeleteEvent local size:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "\u5c0f\u4e8e:"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    return-void

    .line 275
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42960000    # 75.0f

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start checkAndDeleteEvent local size,deleteCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 279
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/e/i;

    .line 280
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 282
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 283
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Ljava/util/List;)V

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "end checkAndDeleteEvent local size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private d()V
    .locals 1

    .line 240
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "onHandleRoutineUploadEvent"

    .line 244
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 245
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 290
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 292
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/h;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->f:J

    .line 295
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->j()V

    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->j:Lcom/bytedance/sdk/openadsdk/e/g$a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/g$a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "doRoutineUpload no net, wait retry"

    .line 299
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 300
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->i()V

    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/e/h;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 305
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/e/h;->a:Z

    if-eqz v1, :cond_2

    const-string v0, "doRoutineUpload success"

    .line 306
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 308
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->g()V

    .line 310
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->f()V

    goto :goto_0

    .line 313
    :cond_2
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Lcom/bytedance/sdk/openadsdk/e/h;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "doRoutineUpload serverbusy"

    .line 314
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 315
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->k()V

    goto :goto_0

    .line 317
    :cond_3
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/g;->b(Lcom/bytedance/sdk/openadsdk/e/h;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 319
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->g()V

    .line 321
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->f()V

    goto :goto_0

    .line 322
    :cond_4
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-nez v0, :cond_6

    .line 323
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->l:Z

    if-nez v0, :cond_5

    .line 325
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->i()V

    const-string v0, "doRoutineUpload net fail retry"

    .line 326
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->l()V

    :cond_6
    :goto_0
    return-void
.end method

.method private f()V
    .locals 2

    .line 338
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->f:J

    .line 340
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->o()V

    .line 342
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->j()V

    return-void
.end method

.method private g()V
    .locals 2

    const-string v0, "clearCacheList, delete event from cache and db"

    .line 346
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Ljava/util/List;)V

    .line 348
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method private h()V
    .locals 3

    .line 353
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->m()J

    move-result-wide v0

    const/4 v2, 0x4

    invoke-direct {p0, v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(IJ)V

    return-void
.end method

.method private i()V
    .locals 3

    .line 357
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v0, v0, Lcom/bytedance/sdk/openadsdk/e/g$b;->c:J

    const/4 v2, 0x3

    invoke-direct {p0, v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(IJ)V

    return-void
.end method

.method private j()V
    .locals 3

    .line 361
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v0, v0, Lcom/bytedance/sdk/openadsdk/e/g$b;->b:J

    const/4 v2, 0x2

    invoke-direct {p0, v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(IJ)V

    return-void
.end method

.method private k()V
    .locals 2

    const/4 v0, 0x1

    .line 374
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    .line 375
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Z)V

    .line 376
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 377
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 378
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 379
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->h()V

    return-void
.end method

.method private l()V
    .locals 2

    const/4 v0, 0x1

    .line 388
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    .line 389
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Z)V

    .line 390
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 391
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 392
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 393
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->h()V

    return-void
.end method

.method private m()J
    .locals 4

    .line 402
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    rem-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v2, v2, Lcom/bytedance/sdk/openadsdk/e/g$b;->f:J

    mul-long v0, v0, v2

    return-wide v0
.end method

.method private n()Z
    .locals 5

    .line 431
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/e/g$b;->a:I

    if-ge v0, v1, :cond_0

    .line 432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->f:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/g;->k:Lcom/bytedance/sdk/openadsdk/e/g$b;

    iget-wide v2, v2, Lcom/bytedance/sdk/openadsdk/e/g$b;->b:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private o()V
    .locals 2

    const/4 v0, 0x0

    .line 436
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->g:Z

    .line 437
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/e;->a(Z)V

    .line 438
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->h:I

    .line 439
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/g;->c:Lcom/bytedance/sdk/openadsdk/e/e;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/e;->a(I)V

    .line 440
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/e/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)",
            "Lcom/bytedance/sdk/openadsdk/e/h;"
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    if-nez v0, :cond_0

    .line 219
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->f()Lcom/bytedance/sdk/openadsdk/core/p;

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->d:Lcom/bytedance/sdk/openadsdk/core/p;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 224
    :cond_1
    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/e/h;

    move-result-object p1

    return-object p1
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .line 91
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 p1, 0x2

    if-eq v0, p1, :cond_3

    const/4 p1, 0x3

    if-eq v0, p1, :cond_2

    const/4 p1, 0x4

    if-eq v0, p1, :cond_1

    const/4 p1, 0x5

    if-eq v0, p1, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->a()V

    goto :goto_0

    .line 103
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->b()V

    goto :goto_0

    .line 100
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->c()V

    goto :goto_0

    .line 97
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->d()V

    goto :goto_0

    .line 94
    :cond_4
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/i;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/g;->a(Lcom/bytedance/sdk/openadsdk/e/i;)V

    :goto_0
    return v1
.end method

.method protected onLooperPrepared()V
    .locals 2

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->f:J

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/g;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/g;->i:Landroid/os/Handler;

    return-void
.end method
