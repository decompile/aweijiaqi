.class Lcom/bytedance/sdk/openadsdk/e/q$8;
.super Lcom/bytedance/sdk/component/e/g;
.source "WebviewTimeTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/e/q;->n()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/e/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 296
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 297
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 298
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 299
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    const-string v5, "ts"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v4, v3, v5, v1}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 300
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    const-string v2, "type"

    const-string v4, "native_enterBackground"

    invoke-static {v1, v3, v2, v4}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 301
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/q$8;->a:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/e/q;->c(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-static {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONArray;Ljava/lang/Object;)V

    .line 302
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
