.class public Lcom/bytedance/sdk/openadsdk/e/q;
.super Ljava/lang/Object;
.source "WebviewTimeTrack.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Lorg/json/JSONObject;

.field private g:Lorg/json/JSONArray;

.field private h:Lorg/json/JSONArray;

.field private i:Z

.field private final j:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "embeded_ad"

    .line 29
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->c:Ljava/lang/Boolean;

    .line 32
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->d:Ljava/lang/Boolean;

    .line 33
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->e:Ljava/lang/Boolean;

    .line 39
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->i:Z

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "embeded_ad"

    .line 29
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->c:Ljava/lang/Boolean;

    .line 32
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->d:Ljava/lang/Boolean;

    .line 33
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->e:Ljava/lang/Boolean;

    .line 39
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->i:Z

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->j:Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/q;->a:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/q;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 47
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/q;->f:Lorg/json/JSONObject;

    .line 48
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/q;->g:Lorg/json/JSONArray;

    .line 49
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/q;->h:Lorg/json/JSONArray;

    .line 50
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/q;->f:Lorg/json/JSONObject;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p3, "webview_source"

    invoke-direct {p0, p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->d:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;)Ljava/lang/Object;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->j:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONArray;Ljava/lang/Object;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONArray;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;Z)V

    return-void
.end method

.method private a(Lorg/json/JSONArray;Ljava/lang/Object;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 534
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    goto :goto_0

    .line 538
    :cond_0
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x1

    .line 529
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;Z)V

    return-void
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    if-eqz p1, :cond_2

    .line 514
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    if-nez p4, :cond_1

    .line 518
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_1

    return-void

    .line 521
    :cond_1
    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/q;Z)Z
    .locals 0

    .line 20
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONObject;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->f:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONArray;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->g:Lorg/json/JSONArray;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONArray;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->h:Lorg/json/JSONArray;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/e/q;)Z
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/e/q;->t()Z

    move-result p0

    return p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/e/q;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->i:Z

    return p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/e/q;)Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/e/q;)Ljava/lang/String;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->a:Ljava/lang/String;

    return-object p0
.end method

.method private t()Z
    .locals 1

    .line 505
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 58
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$1;

    const-string v1, "onRenderStart"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$1;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(I)V
    .locals 2

    .line 89
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$19;

    const-string v1, "onRenderError"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/e/q$19;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    .line 101
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$20;

    const-string v1, "onRenderError"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/q$20;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 370
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$13;

    const-string v1, "onWebviewJsbStart"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/e/q$13;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;JJI)V
    .locals 10

    .line 325
    new-instance v9, Lcom/bytedance/sdk/openadsdk/e/q$10;

    const-string v2, "onInterceptHtml"

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p4

    move-wide v6, p2

    move/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/e/q$10;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Ljava/lang/String;JJI)V

    invoke-static {v9}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    .line 244
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$5;

    const-string v1, "onWebviewLoadError"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/e/q$5;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 462
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/q;->e:Ljava/lang/Boolean;

    return-void
.end method

.method public b()V
    .locals 2

    .line 75
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$12;

    const-string v1, "onRenderSuc"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$12;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .line 389
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$14;

    const-string v1, "onWebviewJsbEnd"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/e/q$14;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public b(Ljava/lang/String;JJI)V
    .locals 10

    .line 347
    new-instance v9, Lcom/bytedance/sdk/openadsdk/e/q$11;

    const-string v2, "onInterceptJs"

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p4

    move-wide v6, p2

    move/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/e/q$11;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Ljava/lang/String;JJI)V

    invoke-static {v9}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public b(Lorg/json/JSONObject;)V
    .locals 2

    .line 437
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$17;

    const-string v1, "addExtraH5JsonObject"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/e/q$17;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public c()V
    .locals 2

    .line 120
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$21;

    const-string v1, "onNativeRenderStart"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$21;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public d()V
    .locals 2

    .line 136
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$22;

    const-string v1, "onDynamicRenderStart"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$22;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public e()V
    .locals 2

    .line 151
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$23;

    const-string v1, "onDynamicRenderSuc"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$23;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public f()V
    .locals 2

    .line 166
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$24;

    const-string v1, "onDynamicRenderError"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$24;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public g()V
    .locals 2

    .line 181
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$25;

    const-string v1, "onBeforeWebViewRequest"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$25;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public h()V
    .locals 2

    .line 196
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$2;

    const-string v1, "onNativeRenderEnd"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$2;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public i()V
    .locals 2

    .line 211
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$3;

    const-string v1, "onWebviewLoadStart"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$3;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public j()V
    .locals 2

    .line 225
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$4;

    const-string v1, "onWebviewLoadSuc"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$4;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x0

    .line 240
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lorg/json/JSONObject;)V

    return-void
.end method

.method public l()V
    .locals 2

    .line 263
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$6;

    const-string v1, "onNativeEndCardShow"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$6;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public m()V
    .locals 2

    .line 278
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$7;

    const-string v1, "onNativeEndCardClose"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$7;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public n()V
    .locals 2

    .line 293
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$8;

    const-string v1, "onNativeEnterBackground"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$8;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public o()V
    .locals 2

    .line 309
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$9;

    const-string v1, "onNativeEnterForeground"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$9;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public p()V
    .locals 2

    .line 408
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$15;

    const-string v1, "onNoNativeRender"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$15;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public q()V
    .locals 2

    .line 422
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$16;

    const-string v1, "onRenderFailed"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$16;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->b(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method

.method public r()V
    .locals 1

    const/4 v0, 0x1

    .line 458
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q;->c:Ljava/lang/Boolean;

    return-void
.end method

.method public s()V
    .locals 2

    .line 466
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/q$18;

    const-string v1, "trySendTrackInfo"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/q$18;-><init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;)V

    return-void
.end method
