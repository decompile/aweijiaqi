.class public Lcom/bytedance/sdk/openadsdk/e/b/o$a;
.super Ljava/lang/Object;
.source "VideoLogHelperModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/e/b/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 67
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a:J

    .line 68
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b:J

    .line 69
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c:J

    const/4 v0, 0x0

    .line 71
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->d:Z

    .line 74
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e:I

    .line 75
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f:I

    .line 86
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l:Z

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 91
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a:J

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    .line 120
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 95
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a:J

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 184
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l:Z

    return-void
.end method

.method public b()J
    .locals 2

    .line 99
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b:J

    return-wide v0
.end method

.method public b(I)V
    .locals 0

    .line 128
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f:I

    return-void
.end method

.method public b(J)V
    .locals 0

    .line 103
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b:J

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 192
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->d:Z

    return-void
.end method

.method public c()J
    .locals 2

    .line 107
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c:J

    return-wide v0
.end method

.method public c(I)V
    .locals 0

    .line 136
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->g:I

    return-void
.end method

.method public c(J)V
    .locals 0

    .line 111
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c:J

    return-void
.end method

.method public d()I
    .locals 1

    .line 116
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e:I

    return v0
.end method

.method public d(I)V
    .locals 0

    .line 144
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h:I

    return-void
.end method

.method public e()I
    .locals 1

    .line 124
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f:I

    return v0
.end method

.method public e(I)V
    .locals 0

    .line 160
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->i:I

    return-void
.end method

.method public f()I
    .locals 1

    .line 132
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->g:I

    return v0
.end method

.method public f(I)V
    .locals 0

    .line 172
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->k:I

    return-void
.end method

.method public g()I
    .locals 1

    .line 140
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h:I

    return v0
.end method

.method public h()I
    .locals 6

    .line 148
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v0, 0x0

    return v0

    .line 151
    :cond_0
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a:J

    const-wide/16 v4, 0x64

    mul-long v2, v2, v4

    div-long/2addr v2, v0

    long-to-int v0, v2

    const/16 v1, 0x64

    .line 152
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    .line 156
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->i:I

    return v0
.end method

.method public j()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->j:I

    return v0
.end method

.method public k()I
    .locals 1

    .line 168
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->k:I

    return v0
.end method

.method public l()Z
    .locals 1

    .line 180
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .line 188
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->d:Z

    return v0
.end method
