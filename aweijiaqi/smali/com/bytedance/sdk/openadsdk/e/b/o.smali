.class public Lcom/bytedance/sdk/openadsdk/e/b/o;
.super Ljava/lang/Object;
.source "VideoLogHelperModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/e/b/o$a;
    }
.end annotation


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/bytedance/sdk/openadsdk/o/f/b;

.field private e:Lcom/bytedance/sdk/openadsdk/core/e/m;


# direct methods
.method public constructor <init>(JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->a:J

    .line 19
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->b:Ljava/lang/String;

    .line 20
    iput p4, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->c:I

    .line 21
    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    .line 22
    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 26
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->a:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->c:I

    return v0
.end method

.method public d()Lcom/bytedance/sdk/openadsdk/o/f/b;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->d:Lcom/bytedance/sdk/openadsdk/o/f/b;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/o;->e:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object v0
.end method
