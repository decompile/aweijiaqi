.class public Lcom/bytedance/sdk/openadsdk/e/b/a;
.super Ljava/lang/Object;
.source "BaseEventModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/bytedance/sdk/openadsdk/e/b/c;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/sdk/openadsdk/core/e/m;

.field private c:Ljava/lang/String;

.field private d:Lorg/json/JSONObject;

.field private e:Lcom/bytedance/sdk/openadsdk/e/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/bytedance/sdk/openadsdk/core/e/m;",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "TT;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->e:Lcom/bytedance/sdk/openadsdk/e/b/c;

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->f:Z

    .line 25
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->a:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    .line 27
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->c:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->d:Lorg/json/JSONObject;

    .line 29
    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->e:Lcom/bytedance/sdk/openadsdk/e/b/c;

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .line 80
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->f:Z

    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/core/e/m;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->b:Lcom/bytedance/sdk/openadsdk/core/e/m;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lorg/json/JSONObject;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->d:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->d:Lorg/json/JSONObject;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->d:Lorg/json/JSONObject;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/e/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->e:Lcom/bytedance/sdk/openadsdk/e/b/c;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/e/b/a;->f:Z

    return v0
.end method
