.class Lcom/bytedance/sdk/openadsdk/e/q$20;
.super Lcom/bytedance/sdk/component/e/g;
.source "WebviewTimeTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/e/q;->a(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/e/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    iput p3, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->a:I

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 104
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 105
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 106
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 107
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    const-string v5, "ts"

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v4, v3, v5, v1}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    const-string v2, "code"

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v3, v2, v4}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    const-string v2, "msg"

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->b:Ljava/lang/String;

    invoke-static {v1, v3, v2, v4}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/q$20;->c:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/e/q;->b(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v4, "render_error"

    invoke-static {v1, v2, v4, v3}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
