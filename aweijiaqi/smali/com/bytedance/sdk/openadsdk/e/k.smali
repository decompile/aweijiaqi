.class public Lcom/bytedance/sdk/openadsdk/e/k;
.super Ljava/lang/Object;
.source "OpenAppSuccEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/e/k$c;,
        Lcom/bytedance/sdk/openadsdk/e/k$b;,
        Lcom/bytedance/sdk/openadsdk/e/k$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/e/k;


# instance fields
.field private b:Lcom/bytedance/sdk/openadsdk/e/k$c;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/bytedance/sdk/openadsdk/e/k$b;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/e/k$c;->a()Lcom/bytedance/sdk/openadsdk/e/k$c;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/k;->b:Lcom/bytedance/sdk/openadsdk/e/k$c;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/k;)Lcom/bytedance/sdk/openadsdk/e/k$b;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/e/k;->d:Lcom/bytedance/sdk/openadsdk/e/k$b;

    return-object p0
.end method

.method public static a()Lcom/bytedance/sdk/openadsdk/e/k;
    .locals 2

    .line 38
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/k;->a:Lcom/bytedance/sdk/openadsdk/e/k;

    if-nez v0, :cond_1

    .line 39
    const-class v0, Lcom/bytedance/sdk/openadsdk/e/k;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/e/k;->a:Lcom/bytedance/sdk/openadsdk/e/k;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/bytedance/sdk/openadsdk/e/k;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/e/k;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/e/k;->a:Lcom/bytedance/sdk/openadsdk/e/k;

    .line 43
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 46
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/k;->a:Lcom/bytedance/sdk/openadsdk/e/k;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/k;->b(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/e/k$b;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 84
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/k$b;->d()V

    .line 86
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/k$b;->c()I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/k;->b:Lcom/bytedance/sdk/openadsdk/e/k$c;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/e/k$c;->a:I

    mul-int v0, v0, v1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/k;->b:Lcom/bytedance/sdk/openadsdk/e/k$c;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/e/k$c;->b:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    .line 88
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/e/k$b;->a(Z)Lcom/bytedance/sdk/openadsdk/e/k$b;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/k;->c(Lcom/bytedance/sdk/openadsdk/e/k$b;)V

    return-void

    .line 92
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->d()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/k$a;

    const/16 v1, 0x64

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/e/k$a;-><init>(Lcom/bytedance/sdk/openadsdk/e/k;I)V

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/k;->b:Lcom/bytedance/sdk/openadsdk/e/k$c;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/e/k$c;->a:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v0, v1, v2, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/e/k;Lcom/bytedance/sdk/openadsdk/e/k$b;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/k;->b(Lcom/bytedance/sdk/openadsdk/e/k$b;)V

    return-void
.end method

.method private static b(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;)V
    .locals 2

    if-eqz p0, :cond_2

    .line 289
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 292
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/r/d;->a(J)V

    .line 293
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->aO()Lorg/json/JSONObject;

    move-result-object p0

    if-nez p0, :cond_1

    return-void

    .line 297
    :cond_1
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    .line 298
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/r/d;->d(Ljava/lang/String;)V

    .line 299
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/d;->c(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/e/k$b;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 113
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/r/o;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/k;->a(Lcom/bytedance/sdk/openadsdk/e/k$b;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 118
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/e/k$b;->a(Z)Lcom/bytedance/sdk/openadsdk/e/k$b;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/k;->c(Lcom/bytedance/sdk/openadsdk/e/k$b;)V

    :goto_0
    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/e/k$b;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x5

    .line 166
    invoke-static {p1, v0}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/g;I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Z)V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/k;->c:Ljava/util/Map;

    invoke-static {p1, p2, v0, p3}, Lcom/bytedance/sdk/openadsdk/e/k$b;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/util/Map;Z)Lcom/bytedance/sdk/openadsdk/e/k$b;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/k;->d:Lcom/bytedance/sdk/openadsdk/e/k$b;

    .line 75
    invoke-static {}, Lcom/bytedance/sdk/component/e/e;->d()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    new-instance p2, Lcom/bytedance/sdk/openadsdk/e/k$a;

    const/16 p3, 0x64

    invoke-direct {p2, p0, p3}, Lcom/bytedance/sdk/openadsdk/e/k$a;-><init>(Lcom/bytedance/sdk/openadsdk/e/k;I)V

    sget-object p3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x0

    invoke-interface {p1, p2, v0, v1, p3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method
