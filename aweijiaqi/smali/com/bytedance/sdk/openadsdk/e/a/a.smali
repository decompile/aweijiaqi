.class public Lcom/bytedance/sdk/openadsdk/e/a/a;
.super Ljava/lang/Object;
.source "VideoEventManager.java"


# static fields
.field private static final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Lcom/bytedance/sdk/openadsdk/e/p;",
            "Lcom/bytedance/sdk/openadsdk/e/b/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 58
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;
    .locals 3

    .line 72
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 75
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "session_id"

    .line 76
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    if-lez p2, :cond_1

    const-string p1, "play_type"

    .line 79
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    if-eqz p0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/m;->V()Lcom/bytedance/sdk/openadsdk/core/e/x;

    move-result-object p0

    if-eqz p0, :cond_2

    const-string p1, "video_resolution"

    .line 84
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->f()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "video_size"

    .line 85
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "video_url"

    .line 86
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/e/x;->i()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/o/f/b;)V
    .locals 10

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    goto :goto_1

    .line 121
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    const/4 v8, 0x2

    .line 124
    :goto_0
    new-instance v9, Lcom/bytedance/sdk/openadsdk/e/b/o;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-object v1, v9

    move-object v4, v0

    move v5, v8

    move-object v6, p3

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b/o;-><init>(JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/o/f/b;Lcom/bytedance/sdk/openadsdk/core/e/m;)V

    .line 125
    sget-object v1, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2, v9}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {p1, v0, v8}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v6

    .line 128
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v5

    .line 129
    new-instance p2, Lcom/bytedance/sdk/openadsdk/e/b/a;

    const/4 v7, 0x0

    move-object v2, p2

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 130
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/o/f/b;->o()Z

    move-result p0

    invoke-virtual {p2, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    const-string p0, "play_start"

    .line 131
    invoke-static {p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 7

    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 138
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_1

    return-void

    .line 142
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 143
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_3

    if-nez v3, :cond_2

    goto :goto_0

    .line 148
    :cond_2
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/h;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/h;-><init>()V

    .line 149
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->m()Z

    move-result v1

    invoke-virtual {v6, v1}, Lcom/bytedance/sdk/openadsdk/e/b/h;->a(I)V

    .line 150
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/f/b;->n()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/h;->b(J)V

    .line 151
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->a()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/h;->a(J)V

    .line 152
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 154
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 155
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 157
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 158
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    const-string p0, "feed_play"

    .line 159
    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/e/b/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/e/b/a<",
            "Lcom/bytedance/sdk/openadsdk/e/b/k;",
            ">;)V"
        }
    .end annotation

    const-string v0, "load_video_start"

    .line 98
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 463
    invoke-static {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method private static a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 7

    if-nez p0, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    goto :goto_0

    .line 474
    :cond_1
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    .line 477
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->d()Lorg/json/JSONObject;

    move-result-object v0

    .line 478
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->e()Lcom/bytedance/sdk/openadsdk/e/b/c;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 479
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->e()Lcom/bytedance/sdk/openadsdk/e/b/c;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/e/b/c;->a(Lorg/json/JSONObject;)V

    :cond_2
    const-string v1, "ad_extra_data"

    .line 481
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 483
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 486
    :goto_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->f()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 487
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    const v6, -0x352ab080    # -6989760.0f

    if-eq v5, v6, :cond_5

    const v6, -0x2a77c376

    if-eq v5, v6, :cond_4

    const v6, 0x72060cfe

    if-eq v5, v6, :cond_3

    goto :goto_2

    :cond_3
    const-string v5, "draw_ad"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v4, 0x2

    goto :goto_2

    :cond_4
    const-string v5, "embeded_ad"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const-string v5, "stream"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v4, 0x1

    :cond_6
    :goto_2
    if-eqz v4, :cond_7

    if-eq v4, v3, :cond_7

    if-eq v4, v2, :cond_7

    goto :goto_3

    .line 491
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "customer_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_8
    :goto_3
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const-string v4, "reportLog:  tag="

    aput-object v4, v0, v1

    .line 495
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "  label="

    aput-object v1, v0, v2

    const/4 v1, 0x3

    aput-object p1, v0, v1

    const/4 v1, 0x4

    const-string v2, "  ad_extra_data="

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p2, v0, v1

    const-string v1, "VideoEventManager"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/j;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 496
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->b()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->c()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v1, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 9

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 167
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_1

    return-void

    .line 171
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 172
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_4

    if-nez v3, :cond_2

    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v7

    .line 177
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-gtz v2, :cond_3

    return-void

    .line 183
    :cond_3
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/g;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/g;-><init>()V

    .line 184
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b/g;->a(J)V

    .line 185
    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/g;->b(J)V

    .line 187
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 188
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 189
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 191
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 192
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 195
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "duration"

    .line 196
    invoke-virtual {p0, v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 197
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "feed_pause"

    .line 198
    invoke-static {p1, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 200
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_4
    :goto_0
    return-void
.end method

.method public static b(Lcom/bytedance/sdk/openadsdk/e/b/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/e/b/a<",
            "Lcom/bytedance/sdk/openadsdk/e/b/l;",
            ">;)V"
        }
    .end annotation

    const-string v0, "load_video_success"

    .line 103
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 9

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 208
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_1

    return-void

    .line 212
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 213
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_4

    if-nez v3, :cond_2

    goto :goto_0

    .line 217
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v7

    .line 218
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-gtz v2, :cond_3

    return-void

    .line 224
    :cond_3
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/e;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/e;-><init>()V

    .line 225
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b/e;->a(J)V

    .line 226
    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/e;->b(J)V

    .line 228
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 229
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 230
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 232
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 233
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 235
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "duration"

    .line 236
    invoke-virtual {p0, v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 237
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "feed_continue"

    .line 238
    invoke-static {p1, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 240
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_4
    :goto_0
    return-void
.end method

.method public static c(Lcom/bytedance/sdk/openadsdk/e/b/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/e/b/a<",
            "Lcom/bytedance/sdk/openadsdk/e/b/j;",
            ">;)V"
        }
    .end annotation

    const-string v0, "load_video_error"

    .line 108
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 9

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 250
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_1

    return-void

    .line 254
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 255
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_4

    if-nez v3, :cond_2

    goto :goto_0

    .line 259
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v7

    .line 260
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-gtz v2, :cond_3

    return-void

    .line 266
    :cond_3
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/n;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/n;-><init>()V

    .line 267
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b/n;->a(J)V

    .line 268
    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/n;->b(J)V

    .line 269
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->d()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/e/b/n;->a(I)V

    .line 270
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->e()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/e/b/n;->b(I)V

    .line 272
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 273
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 274
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 276
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 277
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 280
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "duration"

    .line 281
    invoke-virtual {p0, v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 282
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "play_error"

    .line 283
    invoke-static {p1, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 285
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_4
    :goto_0
    return-void
.end method

.method public static d(Lcom/bytedance/sdk/openadsdk/e/b/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/openadsdk/e/b/a<",
            "Lcom/bytedance/sdk/openadsdk/e/b/i;",
            ">;)V"
        }
    .end annotation

    const-string v0, "load_video_cancel"

    .line 113
    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 9

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 295
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_1

    return-void

    .line 299
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 300
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_4

    if-nez v3, :cond_2

    goto :goto_0

    .line 304
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v7

    .line 305
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-gtz v2, :cond_3

    return-void

    .line 311
    :cond_3
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/b;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/b;-><init>()V

    .line 312
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b/b;->a(J)V

    .line 313
    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/b;->b(J)V

    .line 314
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->f()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/e/b/b;->a(I)V

    .line 315
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->g()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/e/b/b;->b(I)V

    .line 317
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 318
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 319
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 321
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 322
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 324
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "duration"

    .line 325
    invoke-virtual {p0, v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 326
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "endcard_skip"

    .line 327
    invoke-static {p1, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 329
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_4
    :goto_0
    return-void
.end method

.method public static f(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 10

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 338
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez v0, :cond_1

    return-void

    .line 342
    :cond_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v1

    .line 343
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v4

    if-eqz v1, :cond_4

    if-nez v4, :cond_2

    goto :goto_1

    .line 347
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v8

    .line 348
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-gtz v3, :cond_3

    return-void

    .line 354
    :cond_3
    new-instance v7, Lcom/bytedance/sdk/openadsdk/e/b/d;

    invoke-direct {v7}, Lcom/bytedance/sdk/openadsdk/e/b/d;-><init>()V

    .line 355
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v5

    invoke-virtual {v7, v5, v6}, Lcom/bytedance/sdk/openadsdk/e/b/d;->b(J)V

    .line 356
    invoke-virtual {v7, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/d;->a(J)V

    .line 357
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->i()I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/bytedance/sdk/openadsdk/e/b/d;->a(I)V

    .line 358
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->j()I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/bytedance/sdk/openadsdk/e/b/d;->b(I)V

    .line 360
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v1

    .line 361
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v6

    .line 362
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v5

    .line 364
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v2, v0

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 365
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 367
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "duration"

    .line 368
    invoke-virtual {p0, v1, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "percent"

    .line 369
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "feed_break"

    .line 370
    invoke-static {v0, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 372
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    .line 374
    :goto_0
    sget-object p0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {p0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    :goto_1
    return-void
.end method

.method public static g(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 10

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 382
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/e/a/a;->h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V

    .line 384
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez v0, :cond_1

    return-void

    .line 388
    :cond_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v1

    .line 389
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v4

    if-eqz v1, :cond_4

    if-nez v4, :cond_2

    goto :goto_1

    .line 393
    :cond_2
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->a()J

    move-result-wide v8

    .line 394
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-gtz v3, :cond_3

    return-void

    .line 400
    :cond_3
    new-instance v7, Lcom/bytedance/sdk/openadsdk/e/b/f;

    invoke-direct {v7}, Lcom/bytedance/sdk/openadsdk/e/b/f;-><init>()V

    .line 401
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v5

    invoke-virtual {v7, v5, v6}, Lcom/bytedance/sdk/openadsdk/e/b/f;->b(J)V

    .line 402
    invoke-virtual {v7, v1, v2}, Lcom/bytedance/sdk/openadsdk/e/b/f;->a(J)V

    .line 403
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->j()I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/bytedance/sdk/openadsdk/e/b/f;->a(I)V

    .line 405
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v1

    .line 406
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v6

    .line 407
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v5

    .line 408
    new-instance v0, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v2, v0

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 409
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    .line 412
    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "duration"

    .line 413
    invoke-virtual {p0, v1, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "percent"

    .line 414
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->h()I

    move-result p2

    invoke-virtual {p0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "feed_over"

    .line 415
    invoke-static {v0, p2, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 417
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    .line 420
    :goto_0
    sget-object p0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {p0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    :goto_1
    return-void
.end method

.method public static h(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/e/p;Lcom/bytedance/sdk/openadsdk/e/b/o$a;)V
    .locals 7

    if-eqz p0, :cond_5

    if-eqz p1, :cond_5

    if-nez p2, :cond_0

    goto :goto_0

    .line 430
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->k()I

    move-result v0

    if-gtz v0, :cond_1

    const-string p0, "VideoEventManager"

    const-string p1, "Cancel log report when buffer count is 0"

    .line 431
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 434
    :cond_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/e/a/a;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/e/b/o;

    if-nez p1, :cond_2

    return-void

    .line 438
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->d()Lcom/bytedance/sdk/openadsdk/o/f/b;

    move-result-object v0

    .line 439
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->e()Lcom/bytedance/sdk/openadsdk/core/e/m;

    move-result-object v3

    if-eqz v0, :cond_5

    if-nez v3, :cond_3

    goto :goto_0

    .line 443
    :cond_3
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->c()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-gtz v2, :cond_4

    return-void

    .line 449
    :cond_4
    new-instance v6, Lcom/bytedance/sdk/openadsdk/e/b/m;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/e/b/m;-><init>()V

    .line 450
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->b()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/bytedance/sdk/openadsdk/e/b/m;->a(J)V

    .line 451
    invoke-virtual {v6, v0, v1}, Lcom/bytedance/sdk/openadsdk/e/b/m;->b(J)V

    .line 452
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->k()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/bytedance/sdk/openadsdk/e/b/m;->a(I)V

    .line 454
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->c()I

    move-result v0

    .line 455
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/e/b/o;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v5

    .line 456
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/r/o;->a(Lcom/bytedance/sdk/openadsdk/core/e/m;)Ljava/lang/String;

    move-result-object v4

    .line 457
    new-instance p1, Lcom/bytedance/sdk/openadsdk/e/b/a;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/e/b/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/e/m;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/e/b/c;)V

    .line 458
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/e/b/o$a;->l()Z

    move-result p0

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/b/a;->a(Z)V

    const-string p0, "play_buffer"

    .line 459
    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/e/a/a;->a(Lcom/bytedance/sdk/openadsdk/e/b/a;Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void
.end method
