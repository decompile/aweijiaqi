.class Lcom/bytedance/sdk/openadsdk/e/q$17;
.super Lcom/bytedance/sdk/component/e/g;
.source "WebviewTimeTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/e/q;->b(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lorg/json/JSONObject;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/e/q;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->a:Lorg/json/JSONObject;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 440
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 441
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/e/q;->b(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->a:Lorg/json/JSONObject;

    if-nez v1, :cond_0

    goto :goto_1

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->a:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 445
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 446
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 447
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/e/q;->b(Lcom/bytedance/sdk/openadsdk/e/q;)Lorg/json/JSONObject;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->a:Lorg/json/JSONObject;

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v3, v4, v2, v5}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 449
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/e/q;->a(Lcom/bytedance/sdk/openadsdk/e/q;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 450
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/e/q$17;->b:Lcom/bytedance/sdk/openadsdk/e/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/e/q;->s()V

    .line 451
    monitor-exit v0

    return-void

    .line 442
    :cond_2
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 451
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
