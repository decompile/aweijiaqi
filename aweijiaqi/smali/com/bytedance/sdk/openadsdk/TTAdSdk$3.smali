.class final Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;
.super Lcom/bytedance/sdk/component/e/g;
.source "TTAdSdk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/TTAdSdk;->e(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 0

    .line 235
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/component/e/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 239
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->h()Lcom/bytedance/sdk/openadsdk/core/j/h;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->K()Z

    move-result v1

    if-nez v1, :cond_1

    .line 241
    monitor-enter v0

    .line 242
    :try_start_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->K()Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j/h;->a()V

    .line 245
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 249
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;)V

    .line 251
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->getInstance()Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 252
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->d()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->i()V

    .line 255
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/k/c;->f()V

    .line 256
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isPaid()Z

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->updatePaid(Z)V

    .line 259
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/l;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 262
    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->a(Z)V

    .line 263
    new-instance v0, Lcom/bytedance/sdk/openadsdk/k/b/a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/k/b/a;-><init>()V

    invoke-static {v0}, Lcom/bytedance/sdk/component/e/e;->a(Lcom/bytedance/sdk/component/e/c;)V

    .line 265
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->b()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->openDebugMode()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    .line 269
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/dislike/a;->a()V

    .line 270
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/n/a;->a(Landroid/content/Context;)V

    .line 271
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/k/c;->a(Landroid/content/Context;)V

    .line 272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_3

    .line 274
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/component/utils/q;->a()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_3
    return-void
.end method
