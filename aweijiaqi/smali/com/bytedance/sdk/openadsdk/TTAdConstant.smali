.class public Lcom/bytedance/sdk/openadsdk/TTAdConstant;
.super Ljava/lang/Object;
.source "TTAdConstant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/TTAdConstant$NATIVE_AD_TYPE;,
        Lcom/bytedance/sdk/openadsdk/TTAdConstant$ORIENTATION_STATE;,
        Lcom/bytedance/sdk/openadsdk/TTAdConstant$NETWORK_STATE;,
        Lcom/bytedance/sdk/openadsdk/TTAdConstant$TITLE_BAR_THEME;,
        Lcom/bytedance/sdk/openadsdk/TTAdConstant$RitScenes;
    }
.end annotation


# static fields
.field public static final AD_MAX_EVENT_TIME:J = 0x927c0L

.field public static final AD_TYPE_COMMON_VIDEO:I = 0x0

.field public static final AD_TYPE_PLAYABLE:I = 0x2

.field public static final AD_TYPE_PLAYABLE_VIDEO:I = 0x1

.field public static final AD_TYPE_UNKNOWN:I = -0x1

.field public static final FALLBACK_TYPE_DOWNLOAD:I = 0x2

.field public static final FALLBACK_TYPE_LANDING_PAGE:I = 0x1

.field public static final FULL_INTERACTION_MARGIN:I = 0x14

.field public static final FULL_INTERACTION_TYPE_DEFAULT:F = 100.0f

.field public static final HORIZONTAL:I = 0x2

.field public static final IMAGE_MODE_GROUP_IMG:I = 0x4

.field public static final IMAGE_MODE_LARGE_IMG:I = 0x3

.field public static final IMAGE_MODE_SMALL_IMG:I = 0x2

.field public static final IMAGE_MODE_UNKNOWN:I = -0x1

.field public static final IMAGE_MODE_VERTICAL_IMG:I = 0x10

.field public static final IMAGE_MODE_VIDEO:I = 0x5

.field public static final IMAGE_MODE_VIDEO_VERTICAL:I = 0xf

.field public static final INTERACTION_TYPE_BROWSER:I = 0x2

.field public static final INTERACTION_TYPE_DIAL:I = 0x5

.field public static final INTERACTION_TYPE_DOWNLOAD:I = 0x4

.field public static final INTERACTION_TYPE_LANDING_PAGE:I = 0x3

.field public static final INTERACTION_TYPE_UNKNOWN:I = -0x1

.field public static final MULTI_PROCESS_DATA:Ljava/lang/String; = "multi_process_data"

.field public static final MULTI_PROCESS_MATERIALMETA:Ljava/lang/String; = "multi_process_materialmeta"

.field public static final NETWORK_STATE_2G:I = 0x2

.field public static final NETWORK_STATE_3G:I = 0x3

.field public static final NETWORK_STATE_4G:I = 0x5

.field public static final NETWORK_STATE_MOBILE:I = 0x1

.field public static final NETWORK_STATE_WIFI:I = 0x4

.field public static final ORIENTATION_LANDSCAPE:I = 0x2

.field public static final ORIENTATION_VERTICAL:I = 0x1

.field public static final STYLE_SIZE_RADIO_16_9:I = 0x6f1

.field public static final STYLE_SIZE_RADIO_1_1:I = 0x3e8

.field public static final STYLE_SIZE_RADIO_2_3:I = 0x29a

.field public static final STYLE_SIZE_RADIO_3_2:I = 0x5dc

.field public static final STYLE_SIZE_RADIO_9_16:I = 0x232

.field public static final STYLE_SIZE_RADIO_FULL:F = 100.0f

.field public static final TAG:Ljava/lang/String; = "TT_AD_SDK"

.field public static final TITLE_BAR_THEME_DARK:I = 0x1

.field public static final TITLE_BAR_THEME_LIGHT:I = 0x0

.field public static final TITLE_BAR_THEME_NO_TITLE_BAR:I = -0x1

.field public static final VERTICAL:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
