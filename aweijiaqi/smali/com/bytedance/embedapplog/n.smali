.class Lcom/bytedance/embedapplog/n;
.super Lcom/bytedance/embedapplog/i;
.source "SourceFile"


# static fields
.field private static final b:[J


# instance fields
.field private c:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/32 v2, 0xea60

    aput-wide v2, v0, v1

    .line 20
    sput-object v0, Lcom/bytedance/embedapplog/n;->b:[J

    return-void
.end method

.method constructor <init>(Lcom/bytedance/embedapplog/k;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/i;-><init>(Lcom/bytedance/embedapplog/k;)V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method b()J
    .locals 4

    .line 35
    iget-wide v0, p0, Lcom/bytedance/embedapplog/n;->c:J

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 40
    sget-object v0, Lcom/bytedance/embedapplog/n;->b:[J

    return-object v0
.end method

.method public d()Z
    .locals 5

    .line 45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 46
    iget-object v2, p0, Lcom/bytedance/embedapplog/n;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/k;->h()Lcom/bytedance/embedapplog/q;

    move-result-object v2

    if-eqz v2, :cond_0

    const-wide/32 v3, 0xc350

    .line 48
    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/bytedance/embedapplog/q;->a(JJ)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "play_session"

    .line 50
    invoke-static {v1, v0}, Lcom/bytedance/embedapplog/AppLog;->onEventV3(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 51
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->flush()V

    :cond_0
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lcom/bytedance/embedapplog/n;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/z;->o()I

    move-result v1

    if-eqz v1, :cond_2

    .line 57
    iget-object v1, p0, Lcom/bytedance/embedapplog/n;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/z;->b()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/bytedance/embedapplog/n;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->c()Lcom/bytedance/embedapplog/aq;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/aq;->a(Lorg/json/JSONObject;)Z

    move-result v0

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/embedapplog/n;->c:J

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 62
    invoke-static {v1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "p"

    return-object v0
.end method
