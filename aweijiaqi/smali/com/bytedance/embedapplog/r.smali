.class Lcom/bytedance/embedapplog/r;
.super Lcom/bytedance/embedapplog/t;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lcom/bytedance/embedapplog/y;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/y;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, v0, v1}, Lcom/bytedance/embedapplog/t;-><init>(ZZ)V

    .line 23
    iput-object p1, p0, Lcom/bytedance/embedapplog/r;->e:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->I()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ab_client"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    sget-boolean v0, Lcom/bytedance/embedapplog/bg;->b:Z

    if-eqz v0, :cond_1

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init config has abversion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/y;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->s()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ab_version"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    :cond_2
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 39
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->J()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ab_group"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    :cond_3
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 42
    iget-object v0, p0, Lcom/bytedance/embedapplog/r;->f:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->K()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ab_feature"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    const/4 p1, 0x1

    return p1
.end method
