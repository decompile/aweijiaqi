.class public Lcom/bytedance/embedapplog/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;",
        "Ljava/util/Comparator<",
        "Lcom/bytedance/embedapplog/ap;",
        ">;"
    }
.end annotation


# static fields
.field private static b:Lcom/bytedance/embedapplog/k;


# instance fields
.field public a:Landroid/app/Application;

.field private c:Lcom/bytedance/embedapplog/g;

.field private d:Z

.field private e:Lcom/bytedance/embedapplog/y;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/ap;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/bytedance/embedapplog/aq;

.field private h:Lcom/bytedance/embedapplog/z;

.field private i:Landroid/os/Handler;

.field private j:Lcom/bytedance/embedapplog/q;

.field private k:Lcom/bytedance/embedapplog/util/UriConfig;

.field private l:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static a()V
    .locals 2

    .line 75
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 76
    invoke-direct {v0, v1}, Lcom/bytedance/embedapplog/k;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/bytedance/embedapplog/ap;)V
    .locals 7

    .line 345
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "Init comes First!"

    .line 347
    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bg;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 348
    invoke-static {p0}, Lcom/bytedance/embedapplog/s;->a(Lcom/bytedance/embedapplog/ap;)V

    return-void

    .line 351
    :cond_0
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ap;->a:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    .line 352
    invoke-static {v1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    .line 355
    :cond_1
    instance-of v1, p0, Lcom/bytedance/embedapplog/ax;

    if-eqz v1, :cond_2

    .line 357
    move-object v1, p0

    check-cast v1, Lcom/bytedance/embedapplog/ax;

    iget-object v2, v0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/y;->h()I

    move-result v2

    iput v2, v1, Lcom/bytedance/embedapplog/ax;->i:I

    .line 360
    :cond_2
    iget-object v1, v0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 361
    :try_start_0
    iget-object v2, v0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 362
    iget-object v3, v0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    rem-int/lit8 p0, v2, 0xa

    if-nez p0, :cond_4

    iget-object p0, v0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    if-eqz p0, :cond_4

    const/4 v1, 0x4

    .line 365
    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 366
    iget-object p0, v0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    if-nez v2, :cond_3

    const-wide/16 v2, 0x1f4

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0xfa

    :goto_0
    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_4
    return-void

    :catchall_0
    move-exception p0

    .line 363
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method public static a([Ljava/lang/String;)V
    .locals 3

    .line 381
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    if-nez v0, :cond_0

    .line 383
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Init comes First!"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    return-void

    .line 386
    :cond_0
    iget-object v1, v0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    if-eqz v1, :cond_1

    const/4 v2, 0x4

    .line 387
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iget-object v0, v0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method private b([Ljava/lang/String;)V
    .locals 6

    .line 254
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    monitor-enter v0

    .line 255
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 256
    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 257
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 260
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    array-length v3, p1

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 261
    array-length v2, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, p1, v3

    .line 262
    invoke-static {v4}, Lcom/bytedance/embedapplog/ap;->a(Ljava/lang/String;)Lcom/bytedance/embedapplog/ap;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 266
    :cond_0
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {p1, v1}, Lcom/bytedance/embedapplog/y;->a(Ljava/util/ArrayList;)Z

    move-result p1

    .line 268
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 269
    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/y;->q()Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez p1, :cond_2

    .line 270
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/16 v2, 0x64

    if-le p1, v2, :cond_1

    goto :goto_1

    .line 302
    :cond_1
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    monitor-enter p1

    .line 303
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 304
    monitor-exit p1

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 271
    :cond_2
    :goto_1
    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 272
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 275
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/embedapplog/ap;

    .line 276
    iget-object v4, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    invoke-virtual {v4, v3, p1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 278
    invoke-direct {p0}, Lcom/bytedance/embedapplog/k;->l()V

    .line 280
    :cond_4
    instance-of v4, v3, Lcom/bytedance/embedapplog/aw;

    if-eqz v4, :cond_3

    .line 282
    invoke-static {v3}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;)Z

    move-result v0

    const/4 v2, 0x1

    move v2, v0

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_7

    const/4 v0, 0x7

    if-eqz v2, :cond_6

    .line 288
    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3

    .line 291
    :cond_6
    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/y;->w()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 295
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->g:Lcom/bytedance/embedapplog/aq;

    invoke-virtual {v0, p1}, Lcom/bytedance/embedapplog/aq;->a(Ljava/util/ArrayList;)V

    .line 297
    iget-boolean p1, p0, Lcom/bytedance/embedapplog/k;->d:Z

    if-nez p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/q;->b()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    if-eqz p1, :cond_b

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAutoActiveState()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 298
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/k;->j()Z

    goto :goto_5

    .line 307
    :cond_8
    new-instance p1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->a:Landroid/app/Application;

    const-class v3, Lcom/bytedance/embedapplog/collector/Collector;

    invoke-direct {p1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 309
    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    :goto_4
    if-ge v0, v2, :cond_9

    .line 312
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/bytedance/embedapplog/ap;

    invoke-virtual {v5}, Lcom/bytedance/embedapplog/ap;->e()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v0

    .line 313
    aget-object v5, v3, v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    const v0, 0x4b000

    if-lt v4, v0, :cond_a

    const/4 v0, 0x0

    .line 317
    invoke-static {v0}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    :cond_a
    const-string v0, "EMBED_K_DATA"

    .line 319
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 321
    :try_start_2
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->a:Landroid/app/Application;

    invoke-virtual {v0, p1}, Landroid/app/Application;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    :catch_0
    move-exception p1

    .line 323
    invoke-static {p1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    :cond_b
    :goto_5
    return-void

    :catchall_1
    move-exception p1

    .line 257
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public static f()Lcom/bytedance/embedapplog/k;
    .locals 2

    .line 101
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    if-nez v0, :cond_1

    .line 102
    const-class v0, Lcom/bytedance/embedapplog/k;

    monitor-enter v0

    .line 103
    :try_start_0
    sget-object v1, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    if-nez v1, :cond_0

    .line 105
    new-instance v1, Lcom/bytedance/embedapplog/k;

    invoke-direct {v1}, Lcom/bytedance/embedapplog/k;-><init>()V

    .line 106
    sput-object v1, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    .line 108
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 110
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    return-object v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .line 114
    sget-object v0, Lcom/bytedance/embedapplog/k;->b:Lcom/bytedance/embedapplog/k;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, v0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, v0, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private k()V
    .locals 3

    .line 231
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->c:Lcom/bytedance/embedapplog/g;

    if-nez v0, :cond_1

    .line 233
    new-instance v0, Lcom/bytedance/embedapplog/g;

    invoke-direct {v0, p0}, Lcom/bytedance/embedapplog/g;-><init>(Lcom/bytedance/embedapplog/k;)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->c:Lcom/bytedance/embedapplog/g;

    .line 234
    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->c:Lcom/bytedance/embedapplog/g;

    if-eqz v0, :cond_1

    .line 237
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/g;->f()V

    const/4 v0, 0x0

    .line 238
    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->c:Lcom/bytedance/embedapplog/g;

    :cond_1
    :goto_0
    return-void
.end method

.method private l()V
    .locals 4

    .line 330
    sget-boolean v0, Lcom/bytedance/embedapplog/bg;->b:Z

    if-eqz v0, :cond_0

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packAndSend once, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    iget-object v1, v1, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", hadUI:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/q;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 334
    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    new-instance v2, Lcom/bytedance/embedapplog/l;

    invoke-direct {v2, p0}, Lcom/bytedance/embedapplog/l;-><init>(Lcom/bytedance/embedapplog/k;)V

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 335
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    new-instance v2, Lcom/bytedance/embedapplog/m;

    invoke-direct {v2, p0}, Lcom/bytedance/embedapplog/m;-><init>(Lcom/bytedance/embedapplog/k;)V

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/embedapplog/ap;Lcom/bytedance/embedapplog/ap;)I
    .locals 3

    .line 394
    iget-wide v0, p1, Lcom/bytedance/embedapplog/ap;->a:J

    iget-wide p1, p2, Lcom/bytedance/embedapplog/ap;->a:J

    sub-long/2addr v0, p1

    const-wide/16 p1, 0x0

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    if-lez v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public a(Landroid/app/Application;Lcom/bytedance/embedapplog/y;Lcom/bytedance/embedapplog/z;Lcom/bytedance/embedapplog/f;)V
    .locals 1

    .line 137
    iput-object p1, p0, Lcom/bytedance/embedapplog/k;->a:Landroid/app/Application;

    .line 138
    new-instance v0, Lcom/bytedance/embedapplog/aq;

    invoke-direct {v0, p1, p3, p2}, Lcom/bytedance/embedapplog/aq;-><init>(Landroid/app/Application;Lcom/bytedance/embedapplog/z;Lcom/bytedance/embedapplog/y;)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->g:Lcom/bytedance/embedapplog/aq;

    .line 139
    iput-object p2, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    .line 140
    iput-object p3, p0, Lcom/bytedance/embedapplog/k;->h:Lcom/bytedance/embedapplog/z;

    .line 141
    new-instance p1, Lcom/bytedance/embedapplog/q;

    invoke-direct {p1, p3, p2}, Lcom/bytedance/embedapplog/q;-><init>(Lcom/bytedance/embedapplog/z;Lcom/bytedance/embedapplog/y;)V

    iput-object p1, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    .line 143
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->a:Landroid/app/Application;

    invoke-virtual {p1, p4}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 145
    new-instance p1, Landroid/os/HandlerThread;

    const-string p3, "bd_tracker_w"

    invoke-direct {p1, p3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 147
    new-instance p3, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {p3, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p3, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    const/4 p1, 0x1

    .line 148
    invoke-virtual {p3, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 150
    invoke-virtual {p2}, Lcom/bytedance/embedapplog/y;->h()I

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lcom/bytedance/embedapplog/bf;->a(Z)V

    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->a:Landroid/app/Application;

    return-object v0
.end method

.method public c()Lcom/bytedance/embedapplog/aq;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->g:Lcom/bytedance/embedapplog/aq;

    return-object v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 36
    check-cast p1, Lcom/bytedance/embedapplog/ap;

    check-cast p2, Lcom/bytedance/embedapplog/ap;

    invoke-virtual {p0, p1, p2}, Lcom/bytedance/embedapplog/k;->a(Lcom/bytedance/embedapplog/ap;Lcom/bytedance/embedapplog/ap;)I

    move-result p1

    return p1
.end method

.method public d()Lcom/bytedance/embedapplog/y;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    return-object v0
.end method

.method public e()Lcom/bytedance/embedapplog/z;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->h:Lcom/bytedance/embedapplog/z;

    return-object v0
.end method

.method public h()Lcom/bytedance/embedapplog/q;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->j:Lcom/bytedance/embedapplog/q;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .line 155
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x1

    if-eq v0, v4, :cond_8

    const-wide/32 v5, 0x337f9800

    const/4 v7, 0x6

    if-eq v0, v1, :cond_5

    if-eq v0, v3, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    if-eq v0, v7, :cond_1

    const/4 p1, 0x7

    if-eq v0, p1, :cond_0

    .line 225
    invoke-static {v2}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    monitor-enter v0

    .line 219
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-static {}, Lcom/bytedance/embedapplog/q;->d()Lcom/bytedance/embedapplog/q$a;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-direct {p0, v2}, Lcom/bytedance/embedapplog/k;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    :catchall_0
    move-exception p1

    .line 220
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 198
    :cond_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/bytedance/embedapplog/i;

    .line 199
    invoke-virtual {p1}, Lcom/bytedance/embedapplog/i;->g()Z

    move-result v0

    if-nez v0, :cond_c

    .line 200
    invoke-virtual {p1}, Lcom/bytedance/embedapplog/i;->h()J

    move-result-wide v0

    cmp-long v2, v0, v5

    if-gez v2, :cond_2

    .line 202
    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    iget-object v3, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {v3, v7, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 205
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/embedapplog/k;->k()V

    goto/16 :goto_2

    .line 214
    :cond_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/k;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 210
    :cond_4
    invoke-direct {p0, v2}, Lcom/bytedance/embedapplog/k;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 181
    :cond_5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 182
    new-instance v0, Lcom/bytedance/embedapplog/o;

    invoke-direct {v0, p0}, Lcom/bytedance/embedapplog/o;-><init>(Lcom/bytedance/embedapplog/k;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    new-instance v0, Lcom/bytedance/embedapplog/j;

    invoke-direct {v0, p0}, Lcom/bytedance/embedapplog/j;-><init>(Lcom/bytedance/embedapplog/k;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    new-instance v0, Lcom/bytedance/embedapplog/n;

    invoke-direct {v0, p0}, Lcom/bytedance/embedapplog/n;-><init>(Lcom/bytedance/embedapplog/k;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    new-instance v0, Lcom/bytedance/embedapplog/p;

    invoke-direct {v0, p0}, Lcom/bytedance/embedapplog/p;-><init>(Lcom/bytedance/embedapplog/k;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/embedapplog/i;

    .line 188
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/i;->h()J

    move-result-wide v1

    cmp-long v3, v1, v5

    if-gez v3, :cond_6

    .line 190
    iget-object v3, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    iget-object v8, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {v8, v7, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 194
    :cond_7
    invoke-direct {p0}, Lcom/bytedance/embedapplog/k;->k()V

    goto :goto_2

    .line 157
    :cond_8
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/y;->v()Z

    move-result p1

    sput-boolean p1, Lcom/bytedance/embedapplog/bg;->a:Z

    .line 158
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->h:Lcom/bytedance/embedapplog/z;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/z;->e()Z

    move-result p1

    const-wide/16 v5, 0x3e8

    if-eqz p1, :cond_a

    .line 159
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/y;->q()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 160
    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "bd_tracker_n"

    invoke-direct {p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 162
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    .line 164
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 166
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_9

    .line 167
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 168
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {p1, v3, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_9
    const-string p1, "net|worker start"

    .line 170
    invoke-static {p1, v2}, Lcom/bytedance/embedapplog/bg;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 173
    :cond_a
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 174
    iget-object p1, p0, Lcom/bytedance/embedapplog/k;->l:Landroid/os/Handler;

    invoke-virtual {p1, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 177
    :cond_b
    :goto_1
    invoke-static {}, Lcom/bytedance/embedapplog/s;->a()V

    :cond_c
    :goto_2
    return v4
.end method

.method public i()Lcom/bytedance/embedapplog/util/UriConfig;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->k:Lcom/bytedance/embedapplog/util/UriConfig;

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->e:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->O()Lcom/bytedance/embedapplog/InitConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/InitConfig;->getUriConfig()Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->k:Lcom/bytedance/embedapplog/util/UriConfig;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 129
    invoke-static {v0}, Lcom/bytedance/embedapplog/util/a;->a(I)Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/k;->k:Lcom/bytedance/embedapplog/util/UriConfig;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/k;->k:Lcom/bytedance/embedapplog/util/UriConfig;

    return-object v0
.end method

.method public j()Z
    .locals 4

    const/4 v0, 0x1

    .line 243
    iput-boolean v0, p0, Lcom/bytedance/embedapplog/k;->d:Z

    .line 244
    new-instance v1, Lcom/bytedance/embedapplog/h;

    invoke-direct {v1, p0}, Lcom/bytedance/embedapplog/h;-><init>(Lcom/bytedance/embedapplog/k;)V

    .line 245
    iget-object v2, p0, Lcom/bytedance/embedapplog/k;->i:Landroid/os/Handler;

    if-eqz v2, :cond_0

    const/4 v3, 0x6

    .line 246
    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
