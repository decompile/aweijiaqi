.class Lcom/bytedance/embedapplog/h;
.super Lcom/bytedance/embedapplog/i;
.source "SourceFile"


# instance fields
.field private b:Z


# direct methods
.method constructor <init>(Lcom/bytedance/embedapplog/k;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/i;-><init>(Lcom/bytedance/embedapplog/k;)V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method b()J
    .locals 2

    .line 30
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/h;->b:Z

    if-eqz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 35
    sget-object v0, Lcom/bytedance/embedapplog/o;->b:[J

    return-object v0
.end method

.method d()Z
    .locals 6

    .line 40
    iget-object v0, p0, Lcom/bytedance/embedapplog/h;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->o()I

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/bytedance/embedapplog/h;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->a()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/bytedance/embedapplog/h;->a:Lcom/bytedance/embedapplog/k;

    .line 44
    invoke-virtual {v1}, Lcom/bytedance/embedapplog/k;->b()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/embedapplog/h;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/z;->a()Lorg/json/JSONObject;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/embedapplog/h;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/k;->i()Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/util/UriConfig;->getActiveUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getIAppParam()Lcom/bytedance/embedapplog/IAppParam;

    move-result-object v5

    .line 43
    invoke-static {v1, v2, v3, v4, v5}, Lcom/bytedance/embedapplog/ao;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/IAppParam;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/bytedance/embedapplog/an;->b(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/embedapplog/h;->b:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 46
    invoke-static {v0}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    .line 49
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/h;->b:Z

    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "ac"

    return-object v0
.end method
