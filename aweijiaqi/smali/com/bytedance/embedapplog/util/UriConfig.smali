.class public Lcom/bytedance/embedapplog/util/UriConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/embedapplog/util/UriConfig$a;
    }
.end annotation


# static fields
.field public static final PATH_AB:Ljava/lang/String; = "/service/2/abtest_config/"

.field public static final PATH_ACTIVE:Ljava/lang/String; = "/service/2/app_alert_check/"

.field public static final PATH_CONFIG:Ljava/lang/String; = "/service/2/log_settings/"

.field public static final PATH_REGISTER:Ljava/lang/String; = "/service/2/device_register_only/"

.field public static final PATH_SEND:Ljava/lang/String; = "/service/2/app_log/"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/bytedance/embedapplog/util/UriConfig$a;)V
    .locals 1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->a:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->b(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->b:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->c(Lcom/bytedance/embedapplog/util/UriConfig$a;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->c:[Ljava/lang/String;

    .line 73
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->d(Lcom/bytedance/embedapplog/util/UriConfig$a;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->d:[Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->e(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->e:Ljava/lang/String;

    .line 75
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->f(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->f:Ljava/lang/String;

    .line 76
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->g(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->g:Ljava/lang/String;

    .line 77
    invoke-static {p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->h(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig;->h:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/embedapplog/util/UriConfig$a;Lcom/bytedance/embedapplog/util/UriConfig$1;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/util/UriConfig;-><init>(Lcom/bytedance/embedapplog/util/UriConfig$a;)V

    return-void
.end method

.method public static createByDomain(Ljava/lang/String;[Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig;
    .locals 7

    .line 178
    new-instance v0, Lcom/bytedance/embedapplog/util/UriConfig$a;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;-><init>()V

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/service/2/device_register_only/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/service/2/app_alert_check/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/embedapplog/util/UriConfig$a;->b(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    const/4 v1, 0x0

    const-string v2, "/service/2/app_log/"

    const/4 v3, 0x1

    if-eqz p1, :cond_2

    .line 180
    array-length v4, p1

    if-nez v4, :cond_0

    goto :goto_1

    .line 183
    :cond_0
    array-length v4, p1

    add-int/2addr v4, v3

    new-array v5, v4, [Ljava/lang/String;

    .line 184
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    :goto_0
    if-ge v3, v4, :cond_1

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v6, v3, -0x1

    aget-object v6, p1, v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {v0, v5}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    goto :goto_2

    :cond_2
    :goto_1
    new-array p1, v3, [Ljava/lang/String;

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v1

    invoke-virtual {v0, p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    .line 190
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/service/2/log_settings/"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->c(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/service/2/abtest_config/"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/bytedance/embedapplog/util/UriConfig$a;->d(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    .line 191
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a()Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object p0

    return-object p0
.end method

.method public static createUriConfig(I)Lcom/bytedance/embedapplog/util/UriConfig;
    .locals 0

    .line 168
    invoke-static {p0}, Lcom/bytedance/embedapplog/util/a;->a(I)Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getAbUri()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getActiveUri()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getMonitorUri()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileUri()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getRealUris()[Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public getRegisterUri()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getSendUris()[Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public getSettingUri()Ljava/lang/String;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/UriConfig;->e:Ljava/lang/String;

    return-object v0
.end method
