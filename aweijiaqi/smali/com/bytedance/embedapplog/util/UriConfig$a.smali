.class public Lcom/bytedance/embedapplog/util/UriConfig$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/embedapplog/util/UriConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/embedapplog/util/UriConfig$a;)[Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->c:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/embedapplog/util/UriConfig$a;)[Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->d:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->g:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/embedapplog/util/UriConfig$a;)Ljava/lang/String;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->h:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->c:[Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/bytedance/embedapplog/util/UriConfig;
    .locals 2

    .line 131
    new-instance v0, Lcom/bytedance/embedapplog/util/UriConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/embedapplog/util/UriConfig;-><init>(Lcom/bytedance/embedapplog/util/UriConfig$a;Lcom/bytedance/embedapplog/util/UriConfig$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public b([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->d:[Ljava/lang/String;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/bytedance/embedapplog/util/UriConfig$a;->h:Ljava/lang/String;

    return-object p0
.end method
