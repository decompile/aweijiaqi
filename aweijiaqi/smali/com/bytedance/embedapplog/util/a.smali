.class public Lcom/bytedance/embedapplog/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/bytedance/embedapplog/util/UriConfig;

.field private static final b:Lcom/bytedance/embedapplog/util/UriConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 23
    new-instance v0, Lcom/bytedance/embedapplog/util/UriConfig$a;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;-><init>()V

    const-string v1, "https://log.snssdk.com/service/2/device_register_only/"

    .line 24
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://ichannel.snssdk.com/service/2/app_alert_check/"

    .line 25
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->b(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://log.snssdk.com/service/2/app_log/"

    const-string v2, "https://applog.snssdk.com/service/2/app_log/"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://rtlog.snssdk.com/service/2/app_log/"

    const-string v2, "https://rtapplog.snssdk.com/service/2/app_log/"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->b([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://log.snssdk.com/service/2/log_settings/"

    .line 34
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->c(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a()Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/util/a;->a:Lcom/bytedance/embedapplog/util/UriConfig;

    .line 37
    new-instance v0, Lcom/bytedance/embedapplog/util/UriConfig$a;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;-><init>()V

    const-string v1, "https://toblog.ctobsnssdk.com/service/2/device_register_only/"

    .line 38
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://toblog.ctobsnssdk.com/service/2/app_alert_check/"

    .line 39
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->b(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://toblog.ctobsnssdk.com/service/2/app_log/"

    const-string v2, "https://tobapplog.ctobsnssdk.com/service/2/app_log/"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a([Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://toblog.ctobsnssdk.com/service/2/log_settings/"

    .line 44
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->c(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://toblog.ctobsnssdk.com/service/2/abtest_config/"

    .line 45
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->d(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    const-string v1, "https://success.ctobsnssdk.com/service/2/app_log/"

    .line 46
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/util/UriConfig$a;->e(Ljava/lang/String;)Lcom/bytedance/embedapplog/util/UriConfig$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/util/UriConfig$a;->a()Lcom/bytedance/embedapplog/util/UriConfig;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/util/a;->b:Lcom/bytedance/embedapplog/util/UriConfig;

    return-void
.end method

.method public static final a(I)Lcom/bytedance/embedapplog/util/UriConfig;
    .locals 0

    .line 20
    sget-object p0, Lcom/bytedance/embedapplog/util/a;->b:Lcom/bytedance/embedapplog/util/UriConfig;

    return-object p0
.end method
