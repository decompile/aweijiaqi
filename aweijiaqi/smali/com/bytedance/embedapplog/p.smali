.class Lcom/bytedance/embedapplog/p;
.super Lcom/bytedance/embedapplog/i;
.source "SourceFile"


# static fields
.field private static final b:[J


# instance fields
.field private c:J

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/32 v2, 0xea60

    aput-wide v2, v0, v1

    .line 18
    sput-object v0, Lcom/bytedance/embedapplog/p;->b:[J

    return-void
.end method

.method constructor <init>(Lcom/bytedance/embedapplog/k;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/i;-><init>(Lcom/bytedance/embedapplog/k;)V

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method b()J
    .locals 7

    .line 35
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->d()Lcom/bytedance/embedapplog/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->x()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    :goto_0
    move-wide v0, v2

    goto :goto_1

    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-gtz v6, :cond_1

    goto :goto_0

    .line 41
    :cond_1
    :goto_1
    sget-object v2, Lcom/bytedance/embedapplog/p;->b:[J

    const/4 v3, 0x0

    aput-wide v0, v2, v3

    .line 42
    iget-wide v2, p0, Lcom/bytedance/embedapplog/p;->c:J

    add-long/2addr v2, v0

    return-wide v2
.end method

.method c()[J
    .locals 1

    .line 47
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:[J

    return-object v0
.end method

.method public d()Z
    .locals 8

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/embedapplog/p;->d:J

    iget-object v4, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/k;->d()Lcom/bytedance/embedapplog/y;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/y;->x()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 54
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->b()Lorg/json/JSONObject;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/k;->h()Lcom/bytedance/embedapplog/q;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v1}, Lcom/bytedance/embedapplog/q;->a()Lcom/bytedance/embedapplog/au;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 60
    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/k;->c()Lcom/bytedance/embedapplog/aq;

    move-result-object v3

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/q;->b()Z

    move-result v1

    invoke-virtual {v3, v0, v2, v1}, Lcom/bytedance/embedapplog/aq;->a(Lorg/json/JSONObject;Lcom/bytedance/embedapplog/au;Z)Z

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/p;->d:J

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/k;->c()Lcom/bytedance/embedapplog/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/aq;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 70
    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    iget-object v4, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/k;->b()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v5}, Lcom/bytedance/embedapplog/k;->e()Lcom/bytedance/embedapplog/z;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/embedapplog/z;->a()Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/bytedance/embedapplog/ao;->a(Lcom/bytedance/embedapplog/k;Landroid/content/Context;Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v3

    .line 71
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/bytedance/embedapplog/av;

    .line 72
    iget-object v6, v5, Lcom/bytedance/embedapplog/av;->h:[B

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v7}, Lcom/bytedance/embedapplog/k;->d()Lcom/bytedance/embedapplog/y;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/bytedance/embedapplog/an;->a([Ljava/lang/String;[BLcom/bytedance/embedapplog/y;)I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 74
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    iput v6, v5, Lcom/bytedance/embedapplog/av;->j:I

    .line 77
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 82
    :cond_3
    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->a:Lcom/bytedance/embedapplog/k;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/k;->c()Lcom/bytedance/embedapplog/aq;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/bytedance/embedapplog/aq;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 85
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/bytedance/embedapplog/p;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/bytedance/embedapplog/bg;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_5

    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/p;->c:J

    const/4 v0, 0x1

    return v0

    :cond_5
    const/4 v0, 0x0

    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "s"

    return-object v0
.end method
