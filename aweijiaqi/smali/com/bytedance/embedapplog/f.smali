.class public Lcom/bytedance/embedapplog/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field private static a:I = 0x0

.field private static b:Lcom/bytedance/embedapplog/aw; = null

.field private static c:Lcom/bytedance/embedapplog/aw; = null

.field private static d:J = 0x0L

.field private static e:Ljava/lang/String; = null

.field private static f:J = 0x0L

.field private static g:Ljava/lang/String; = null

.field private static h:I = -0x1

.field private static i:Ljava/lang/Object;

.field private static j:Ljava/lang/Object;

.field private static final l:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final k:Lcom/bytedance/embedapplog/IPicker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 51
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/bytedance/embedapplog/f;->l:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/bytedance/embedapplog/IPicker;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/bytedance/embedapplog/f;->k:Lcom/bytedance/embedapplog/IPicker;

    return-void
.end method

.method public static a(Lcom/bytedance/embedapplog/aw;J)Lcom/bytedance/embedapplog/aw;
    .locals 3

    .line 161
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/aw;->g()Lcom/bytedance/embedapplog/ap;

    move-result-object v0

    check-cast v0, Lcom/bytedance/embedapplog/aw;

    .line 162
    iput-wide p1, v0, Lcom/bytedance/embedapplog/aw;->a:J

    .line 163
    iget-wide v1, p0, Lcom/bytedance/embedapplog/aw;->a:J

    sub-long/2addr p1, v1

    const-wide/16 v1, 0x0

    cmp-long p0, p1, v1

    if-ltz p0, :cond_0

    .line 165
    iput-wide p1, v0, Lcom/bytedance/embedapplog/aw;->h:J

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 167
    invoke-static {p0}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    .line 169
    :goto_0
    invoke-static {v0}, Lcom/bytedance/embedapplog/k;->a(Lcom/bytedance/embedapplog/ap;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/bytedance/embedapplog/aw;
    .locals 2

    .line 147
    new-instance v0, Lcom/bytedance/embedapplog/aw;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/aw;-><init>()V

    .line 148
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ":"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/bytedance/embedapplog/aw;->j:Ljava/lang/String;

    goto :goto_0

    .line 151
    :cond_0
    iput-object p0, v0, Lcom/bytedance/embedapplog/aw;->j:Ljava/lang/String;

    .line 153
    :goto_0
    iput-wide p2, v0, Lcom/bytedance/embedapplog/aw;->a:J

    const-wide/16 p0, -0x1

    .line 154
    iput-wide p0, v0, Lcom/bytedance/embedapplog/aw;->h:J

    if-eqz p4, :cond_1

    goto :goto_1

    :cond_1
    const-string p4, ""

    .line 155
    :goto_1
    iput-object p4, v0, Lcom/bytedance/embedapplog/aw;->i:Ljava/lang/String;

    .line 156
    invoke-static {v0}, Lcom/bytedance/embedapplog/k;->a(Lcom/bytedance/embedapplog/ap;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 210
    sget-object p2, Lcom/bytedance/embedapplog/f;->l:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .line 205
    sget-object v0, Lcom/bytedance/embedapplog/f;->l:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 3

    .line 130
    sget-object v0, Lcom/bytedance/embedapplog/f;->c:Lcom/bytedance/embedapplog/aw;

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/bytedance/embedapplog/f;->j:Ljava/lang/Object;

    invoke-static {v0}, Lcom/bytedance/embedapplog/f;->a(Ljava/lang/Object;)V

    .line 134
    :cond_0
    sget-object v0, Lcom/bytedance/embedapplog/f;->b:Lcom/bytedance/embedapplog/aw;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, v0, Lcom/bytedance/embedapplog/aw;->j:Ljava/lang/String;

    sput-object v0, Lcom/bytedance/embedapplog/f;->e:Ljava/lang/String;

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/bytedance/embedapplog/f;->d:J

    .line 137
    sget-object v2, Lcom/bytedance/embedapplog/f;->b:Lcom/bytedance/embedapplog/aw;

    invoke-static {v2, v0, v1}, Lcom/bytedance/embedapplog/f;->a(Lcom/bytedance/embedapplog/aw;J)Lcom/bytedance/embedapplog/aw;

    const/4 v0, 0x0

    .line 138
    sput-object v0, Lcom/bytedance/embedapplog/f;->b:Lcom/bytedance/embedapplog/aw;

    .line 139
    invoke-virtual {p1}, Landroid/app/Activity;->isChild()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, -0x1

    .line 140
    sput p1, Lcom/bytedance/embedapplog/f;->h:I

    .line 141
    sput-object v0, Lcom/bytedance/embedapplog/f;->i:Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 5

    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 115
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/bytedance/embedapplog/f;->e:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v2, v4, v0, v1, v3}, Lcom/bytedance/embedapplog/f;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/bytedance/embedapplog/aw;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/f;->b:Lcom/bytedance/embedapplog/aw;

    .line 117
    sget-object v1, Lcom/bytedance/embedapplog/f;->l:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/bytedance/embedapplog/aw;->k:I

    .line 118
    invoke-virtual {p1}, Landroid/app/Activity;->isChild()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lcom/bytedance/embedapplog/f;->h:I

    .line 121
    sput-object p1, Lcom/bytedance/embedapplog/f;->i:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 123
    invoke-static {p1}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .line 175
    sget p1, Lcom/bytedance/embedapplog/f;->a:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    sput p1, Lcom/bytedance/embedapplog/f;->a:I

    if-ne p1, v0, :cond_0

    .line 176
    iget-object p1, p0, Lcom/bytedance/embedapplog/f;->k:Lcom/bytedance/embedapplog/IPicker;

    if-eqz p1, :cond_0

    .line 177
    invoke-interface {p1, v0}, Lcom/bytedance/embedapplog/IPicker;->show(Z)V

    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .line 183
    sget-object p1, Lcom/bytedance/embedapplog/f;->e:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 184
    sget p1, Lcom/bytedance/embedapplog/f;->a:I

    add-int/lit8 p1, p1, -0x1

    sput p1, Lcom/bytedance/embedapplog/f;->a:I

    if-gtz p1, :cond_0

    const/4 p1, 0x0

    .line 186
    sput-object p1, Lcom/bytedance/embedapplog/f;->e:Ljava/lang/String;

    .line 187
    sput-object p1, Lcom/bytedance/embedapplog/f;->g:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 188
    sput-wide v0, Lcom/bytedance/embedapplog/f;->f:J

    .line 189
    sput-wide v0, Lcom/bytedance/embedapplog/f;->d:J

    .line 191
    iget-object p1, p0, Lcom/bytedance/embedapplog/f;->k:Lcom/bytedance/embedapplog/IPicker;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 192
    invoke-interface {p1, v0}, Lcom/bytedance/embedapplog/IPicker;->show(Z)V

    :cond_0
    return-void
.end method
