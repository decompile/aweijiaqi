.class public Lcom/bytedance/embedapplog/q;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/embedapplog/q$a;
    }
.end annotation


# static fields
.field private static f:J

.field private static o:Lcom/bytedance/embedapplog/q$a;


# instance fields
.field a:Ljava/lang/String;

.field private final b:Lcom/bytedance/embedapplog/y;

.field private final c:Lcom/bytedance/embedapplog/z;

.field private d:Lcom/bytedance/embedapplog/aw;

.field private e:Lcom/bytedance/embedapplog/aw;

.field private g:J

.field private h:I

.field private i:J

.field private volatile j:Z

.field private k:J

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Lcom/bytedance/embedapplog/au;


# direct methods
.method constructor <init>(Lcom/bytedance/embedapplog/z;Lcom/bytedance/embedapplog/y;)V
    .locals 2

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 54
    iput-wide v0, p0, Lcom/bytedance/embedapplog/q;->i:J

    .line 67
    iput-object p1, p0, Lcom/bytedance/embedapplog/q;->c:Lcom/bytedance/embedapplog/z;

    .line 68
    iput-object p2, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    return-void
.end method

.method public static a(Lcom/bytedance/embedapplog/y;)J
    .locals 9

    .line 242
    sget-wide v0, Lcom/bytedance/embedapplog/q;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/bytedance/embedapplog/q;->f:J

    const-wide/16 v2, 0x3e8

    .line 243
    rem-long v4, v0, v2

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-nez v8, :cond_0

    add-long/2addr v0, v2

    .line 244
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/embedapplog/y;->a(J)V

    .line 246
    :cond_0
    sget-wide v0, Lcom/bytedance/embedapplog/q;->f:J

    return-wide v0
.end method

.method private declared-synchronized a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/embedapplog/ap;",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/ap;",
            ">;Z)V"
        }
    .end annotation

    monitor-enter p0

    .line 102
    :try_start_0
    instance-of v0, p1, Lcom/bytedance/embedapplog/q$a;

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_0

    move-wide v3, v1

    goto :goto_0

    :cond_0
    iget-wide v3, p1, Lcom/bytedance/embedapplog/ap;->a:J

    .line 104
    :goto_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->B()J

    move-result-wide v5

    sput-wide v5, Lcom/bytedance/embedapplog/q;->f:J

    .line 106
    iput-wide v3, p0, Lcom/bytedance/embedapplog/q;->i:J

    .line 107
    iput-boolean p3, p0, Lcom/bytedance/embedapplog/q;->j:Z

    const-wide/16 v5, 0x0

    .line 108
    iput-wide v5, p0, Lcom/bytedance/embedapplog/q;->k:J

    .line 110
    sget-boolean v0, Lcom/bytedance/embedapplog/bg;->b:Z

    const/4 v5, 0x0

    if-eqz v0, :cond_1

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startSession, "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", hadUi:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v6, " data:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v5}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    if-eqz p3, :cond_4

    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v7, 0x5

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/q;->m:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->c()I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/q;->l:I

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 122
    iput-object p1, p0, Lcom/bytedance/embedapplog/q;->m:Ljava/lang/String;

    .line 123
    iput v6, p0, Lcom/bytedance/embedapplog/q;->l:I

    goto :goto_1

    .line 125
    :cond_3
    iget v0, p0, Lcom/bytedance/embedapplog/q;->l:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/bytedance/embedapplog/q;->l:I

    .line 127
    :goto_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    iget v6, p0, Lcom/bytedance/embedapplog/q;->l:I

    invoke-virtual {v0, p1, v6}, Lcom/bytedance/embedapplog/y;->a(Ljava/lang/String;I)V

    const/4 p1, 0x0

    .line 128
    iput p1, p0, Lcom/bytedance/embedapplog/q;->h:I

    :cond_4
    cmp-long p1, v3, v1

    if-eqz p1, :cond_6

    .line 132
    new-instance p1, Lcom/bytedance/embedapplog/au;

    invoke-direct {p1}, Lcom/bytedance/embedapplog/au;-><init>()V

    .line 133
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    iput-object v0, p1, Lcom/bytedance/embedapplog/au;->c:Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-static {v0}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/y;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/bytedance/embedapplog/au;->b:J

    .line 135
    iget-wide v0, p0, Lcom/bytedance/embedapplog/q;->i:J

    iput-wide v0, p1, Lcom/bytedance/embedapplog/au;->a:J

    .line 136
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->c:Lcom/bytedance/embedapplog/z;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/au;->i:Ljava/lang/String;

    .line 137
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->c:Lcom/bytedance/embedapplog/z;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->c()I

    move-result v0

    iput v0, p1, Lcom/bytedance/embedapplog/au;->h:I

    .line 138
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAbConfigVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/au;->e:Ljava/lang/String;

    .line 140
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAbSDKVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/au;->f:Ljava/lang/String;

    .line 142
    :cond_5
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iput-object p1, p0, Lcom/bytedance/embedapplog/q;->n:Lcom/bytedance/embedapplog/au;

    .line 144
    sget-boolean p2, Lcom/bytedance/embedapplog/bg;->b:Z

    if-eqz p2, :cond_6

    .line 145
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "gen launch, "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/bytedance/embedapplog/au;->c:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", hadUi:"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v5}, Lcom/bytedance/embedapplog/bg;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :cond_6
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static a(Lcom/bytedance/embedapplog/ap;)Z
    .locals 1

    .line 151
    instance-of v0, p0, Lcom/bytedance/embedapplog/aw;

    if-eqz v0, :cond_0

    .line 152
    check-cast p0, Lcom/bytedance/embedapplog/aw;

    .line 153
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/aw;->i()Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static d()Lcom/bytedance/embedapplog/q$a;
    .locals 3

    .line 259
    sget-object v0, Lcom/bytedance/embedapplog/q;->o:Lcom/bytedance/embedapplog/q$a;

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Lcom/bytedance/embedapplog/q$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/bytedance/embedapplog/q$a;-><init>(Lcom/bytedance/embedapplog/q$1;)V

    sput-object v0, Lcom/bytedance/embedapplog/q;->o:Lcom/bytedance/embedapplog/q$a;

    .line 262
    :cond_0
    sget-object v0, Lcom/bytedance/embedapplog/q;->o:Lcom/bytedance/embedapplog/q$a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/bytedance/embedapplog/q$a;->a:J

    .line 263
    sget-object v0, Lcom/bytedance/embedapplog/q;->o:Lcom/bytedance/embedapplog/q$a;

    return-object v0
.end method


# virtual methods
.method declared-synchronized a(JJ)Landroid/os/Bundle;
    .locals 5

    monitor-enter p0

    const/4 v0, 0x0

    .line 81
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/y;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/embedapplog/q;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/bytedance/embedapplog/q;->g:J

    sub-long v1, p1, v1

    cmp-long v3, v1, p3

    if-lez v3, :cond_0

    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string p3, "session_no"

    .line 83
    iget p4, p0, Lcom/bytedance/embedapplog/q;->l:I

    invoke-virtual {v0, p3, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p3, "send_times"

    .line 84
    iget p4, p0, Lcom/bytedance/embedapplog/q;->h:I

    add-int/lit8 p4, p4, 0x1

    iput p4, p0, Lcom/bytedance/embedapplog/q;->h:I

    invoke-virtual {v0, p3, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p3, "current_duration"

    .line 85
    iget-wide v1, p0, Lcom/bytedance/embedapplog/q;->g:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, p3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string p3, "session_start_time"

    .line 86
    iget-wide v1, p0, Lcom/bytedance/embedapplog/q;->i:J

    invoke-static {v1, v2}, Lcom/bytedance/embedapplog/ap;->a(J)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iput-wide p1, p0, Lcom/bytedance/embedapplog/q;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a()Lcom/bytedance/embedapplog/au;
    .locals 1

    monitor-enter p0

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->n:Lcom/bytedance/embedapplog/au;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/embedapplog/ap;",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/ap;",
            ">;)Z"
        }
    .end annotation

    .line 166
    instance-of v0, p1, Lcom/bytedance/embedapplog/aw;

    .line 167
    invoke-static {p1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;)Z

    move-result v1

    .line 169
    iget-wide v2, p0, Lcom/bytedance/embedapplog/q;->i:J

    const/4 v4, 0x1

    const-wide/16 v5, 0x0

    const-wide/16 v7, -0x1

    cmp-long v9, v2, v7

    if-nez v9, :cond_0

    .line 171
    invoke-static {p1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;)Z

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 172
    :cond_0
    iget-boolean v2, p0, Lcom/bytedance/embedapplog/q;->j:Z

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 174
    invoke-direct {p0, p1, p2, v4}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 175
    :cond_1
    iget-wide v2, p0, Lcom/bytedance/embedapplog/q;->k:J

    cmp-long v7, v2, v5

    if-eqz v7, :cond_2

    iget-wide v2, p1, Lcom/bytedance/embedapplog/ap;->a:J

    iget-wide v7, p0, Lcom/bytedance/embedapplog/q;->k:J

    iget-object v9, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v9}, Lcom/bytedance/embedapplog/y;->w()J

    move-result-wide v9

    add-long/2addr v7, v9

    cmp-long v9, v2, v7

    if-lez v9, :cond_2

    .line 177
    invoke-direct {p0, p1, p2, v1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 178
    :cond_2
    iget-wide v2, p0, Lcom/bytedance/embedapplog/q;->i:J

    iget-wide v7, p1, Lcom/bytedance/embedapplog/ap;->a:J

    const-wide/32 v9, 0x6ddd00

    add-long/2addr v7, v9

    cmp-long v9, v2, v7

    if-lez v9, :cond_3

    .line 180
    invoke-direct {p0, p1, p2, v1}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/ap;Ljava/util/ArrayList;Z)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    if-eqz v0, :cond_8

    .line 186
    move-object v0, p1

    check-cast v0, Lcom/bytedance/embedapplog/aw;

    .line 187
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/aw;->i()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 188
    iget-wide v1, p1, Lcom/bytedance/embedapplog/ap;->a:J

    iput-wide v1, p0, Lcom/bytedance/embedapplog/q;->g:J

    .line 189
    iput-wide v5, p0, Lcom/bytedance/embedapplog/q;->k:J

    .line 192
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object p2, v0, Lcom/bytedance/embedapplog/aw;->i:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_9

    .line 195
    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->e:Lcom/bytedance/embedapplog/aw;

    const-wide/16 v1, 0x1f4

    if-eqz p2, :cond_4

    iget-wide v5, v0, Lcom/bytedance/embedapplog/aw;->a:J

    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->e:Lcom/bytedance/embedapplog/aw;

    iget-wide v7, p2, Lcom/bytedance/embedapplog/aw;->a:J

    sub-long/2addr v5, v7

    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->e:Lcom/bytedance/embedapplog/aw;

    iget-wide v7, p2, Lcom/bytedance/embedapplog/aw;->h:J

    sub-long/2addr v5, v7

    cmp-long p2, v5, v1

    if-gez p2, :cond_4

    .line 197
    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->e:Lcom/bytedance/embedapplog/aw;

    iget-object p2, p2, Lcom/bytedance/embedapplog/aw;->j:Ljava/lang/String;

    iput-object p2, v0, Lcom/bytedance/embedapplog/aw;->i:Ljava/lang/String;

    goto :goto_1

    .line 198
    :cond_4
    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    if-eqz p2, :cond_9

    iget-wide v5, v0, Lcom/bytedance/embedapplog/aw;->a:J

    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    iget-wide v7, p2, Lcom/bytedance/embedapplog/aw;->a:J

    sub-long/2addr v5, v7

    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    iget-wide v7, p2, Lcom/bytedance/embedapplog/aw;->h:J

    sub-long/2addr v5, v7

    cmp-long p2, v5, v1

    if-gez p2, :cond_9

    .line 200
    iget-object p2, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    iget-object p2, p2, Lcom/bytedance/embedapplog/aw;->j:Ljava/lang/String;

    iput-object p2, v0, Lcom/bytedance/embedapplog/aw;->i:Ljava/lang/String;

    goto :goto_1

    .line 204
    :cond_5
    iget-wide v1, p1, Lcom/bytedance/embedapplog/ap;->a:J

    invoke-virtual {p0, v1, v2, v5, v6}, Lcom/bytedance/embedapplog/q;->a(JJ)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_6

    const-string v2, "play_session"

    .line 206
    invoke-static {v2, v1}, Lcom/bytedance/embedapplog/AppLog;->onEventV3(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 209
    :cond_6
    iput-wide v5, p0, Lcom/bytedance/embedapplog/q;->g:J

    .line 210
    iget-wide v1, v0, Lcom/bytedance/embedapplog/aw;->a:J

    iput-wide v1, p0, Lcom/bytedance/embedapplog/q;->k:J

    .line 212
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/aw;->j()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 215
    iput-object v0, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    goto :goto_1

    .line 217
    :cond_7
    iput-object v0, p0, Lcom/bytedance/embedapplog/q;->e:Lcom/bytedance/embedapplog/aw;

    const/4 p2, 0x0

    .line 218
    iput-object p2, p0, Lcom/bytedance/embedapplog/q;->d:Lcom/bytedance/embedapplog/aw;

    goto :goto_1

    .line 221
    :cond_8
    instance-of v0, p1, Lcom/bytedance/embedapplog/q$a;

    if-nez v0, :cond_9

    .line 222
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    :cond_9
    :goto_1
    invoke-virtual {p0, p1}, Lcom/bytedance/embedapplog/q;->b(Lcom/bytedance/embedapplog/ap;)V

    return v4
.end method

.method public b(Lcom/bytedance/embedapplog/ap;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->c:Lcom/bytedance/embedapplog/z;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/z;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/ap;->d:Ljava/lang/String;

    .line 232
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->a:Ljava/lang/String;

    iput-object v0, p1, Lcom/bytedance/embedapplog/ap;->c:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-static {v0}, Lcom/bytedance/embedapplog/q;->a(Lcom/bytedance/embedapplog/y;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/bytedance/embedapplog/ap;->b:J

    .line 234
    iget-object v0, p0, Lcom/bytedance/embedapplog/q;->b:Lcom/bytedance/embedapplog/y;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/y;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAbConfigVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/ap;->e:Ljava/lang/String;

    .line 236
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAbSDKVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/bytedance/embedapplog/ap;->f:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/q;->j:Z

    return v0
.end method

.method c()Z
    .locals 5

    .line 97
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/bytedance/embedapplog/q;->k:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
