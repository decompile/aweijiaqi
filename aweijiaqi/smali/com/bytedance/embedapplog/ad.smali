.class Lcom/bytedance/embedapplog/ad;
.super Lcom/bytedance/embedapplog/t;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lcom/bytedance/embedapplog/y;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/y;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21
    invoke-direct {p0, v0, v1}, Lcom/bytedance/embedapplog/t;-><init>(ZZ)V

    .line 22
    iput-object p1, p0, Lcom/bytedance/embedapplog/ad;->e:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/y;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 2

    .line 28
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getInitConfig()Lcom/bytedance/embedapplog/InitConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getInitConfig()Lcom/bytedance/embedapplog/InitConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/InitConfig;->getSensitiveInfoProvider()Lcom/bytedance/embedapplog/ISensitiveInfoProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/embedapplog/ISensitiveInfoProvider;->getMac()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 29
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "mc"

    .line 30
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    const/4 p1, 0x1

    return p1
.end method
