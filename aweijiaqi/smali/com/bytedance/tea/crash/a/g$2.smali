.class final Lcom/bytedance/tea/crash/a/g$2;
.super Ljava/lang/Object;
.source "LooperMonitor.java"

# interfaces
.implements Landroid/util/Printer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/tea/crash/a/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public println(Ljava/lang/String;)V
    .locals 17

    .line 240
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->i()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    sput-wide v0, Lcom/bytedance/tea/crash/a/g;->b:J

    .line 241
    sget-wide v0, Lcom/bytedance/tea/crash/a/g;->a:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    return-void

    .line 244
    :cond_0
    sget-wide v0, Lcom/bytedance/tea/crash/a/g;->b:J

    sget-wide v4, Lcom/bytedance/tea/crash/a/g;->a:J

    sub-long v11, v0, v4

    cmp-long v0, v11, v2

    if-gtz v0, :cond_1

    return-void

    .line 250
    :cond_1
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->l()J

    move-result-wide v0

    .line 251
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    const/16 v16, 0x0

    const/4 v6, 0x1

    cmp-long v7, v11, v4

    if-nez v7, :cond_2

    .line 254
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->k()I

    move-result v4

    if-le v4, v6, :cond_2

    const/16 v4, 0x9

    const/16 v13, 0x9

    goto :goto_0

    :cond_2
    if-nez v7, :cond_3

    .line 257
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->k()I

    move-result v4

    if-ne v4, v6, :cond_3

    const/4 v4, 0x2

    const/4 v13, 0x2

    goto :goto_0

    :cond_3
    if-lez v7, :cond_4

    .line 260
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->k()I

    move-result v4

    if-le v4, v6, :cond_4

    const/4 v4, 0x4

    const/4 v13, 0x4

    goto :goto_0

    :cond_4
    if-lez v7, :cond_5

    .line 263
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->k()I

    move-result v4

    if-ne v4, v6, :cond_5

    const/16 v4, 0x8

    const/16 v13, 0x8

    goto :goto_0

    :cond_5
    const/4 v13, 0x0

    .line 268
    :goto_0
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->m()Z

    move-result v4

    if-nez v4, :cond_6

    .line 269
    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->n()Lcom/bytedance/tea/crash/a/g$a;

    move-result-object v6

    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->o()J

    move-result-wide v4

    sub-long v7, v0, v4

    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->p()J

    move-result-wide v4

    sub-long v9, v2, v4

    invoke-static {}, Lcom/bytedance/tea/crash/a/g;->k()I

    move-result v14

    move-object/from16 v15, p1

    invoke-static/range {v6 .. v15}, Lcom/bytedance/tea/crash/a/g;->a(Lcom/bytedance/tea/crash/a/g$a;JJJIILjava/lang/String;)V

    .line 271
    :cond_6
    invoke-static {v0, v1}, Lcom/bytedance/tea/crash/a/g;->b(J)J

    .line 272
    invoke-static {v2, v3}, Lcom/bytedance/tea/crash/a/g;->c(J)J

    .line 273
    invoke-static/range {v16 .. v16}, Lcom/bytedance/tea/crash/a/g;->b(I)I

    const-wide/16 v0, -0x1

    .line 274
    sput-wide v0, Lcom/bytedance/tea/crash/a/g;->a:J

    return-void
.end method
