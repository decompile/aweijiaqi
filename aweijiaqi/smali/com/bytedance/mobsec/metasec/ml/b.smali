.class public Lcom/bytedance/mobsec/metasec/ml/b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/mobsec/metasec/ml/b$a;
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;


# instance fields
.field private a:Landroid/graphics/Point;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/bytedance/mobsec/metasec/ml/b$a;

.field private d:Lcom/bytedance/mobsec/metasec/ml/b$a;

.field private e:Landroid/os/HandlerThread;

.field private f:Landroid/content/Context;

.field private g:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v8, v2, [B

    fill-array-data v8, :array_0

    const v3, 0x1000001

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-string v7, "8ea7ac"

    invoke-static/range {v3 .. v8}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v1, v4

    const/16 v3, 0x9

    new-array v10, v3, [B

    fill-array-data v10, :array_1

    const v5, 0x1000001

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const-string v9, "5ec0a3"

    invoke-static/range {v5 .. v10}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v5, v1, v6

    sput-object v1, Lcom/bytedance/mobsec/metasec/ml/b;->i:[Ljava/lang/String;

    const/4 v1, 0x4

    new-array v5, v1, [Ljava/lang/String;

    new-array v12, v2, [B

    fill-array-data v12, :array_2

    const v7, 0x1000001

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-string v11, "b5464f"

    invoke-static/range {v7 .. v12}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    aput-object v7, v5, v4

    new-array v13, v3, [B

    fill-array-data v13, :array_3

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "047856"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    aput-object v7, v5, v6

    new-array v13, v2, [B

    fill-array-data v13, :array_4

    const-string v12, "776176"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    aput-object v7, v5, v0

    const/4 v7, 0x6

    new-array v13, v7, [B

    fill-array-data v13, :array_5

    const-string v12, "169526"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x3

    aput-object v8, v5, v9

    sput-object v5, Lcom/bytedance/mobsec/metasec/ml/b;->j:[Ljava/lang/String;

    const/16 v5, 0xc

    new-array v5, v5, [Ljava/lang/String;

    const/16 v8, 0xa

    new-array v15, v8, [B

    fill-array-data v15, :array_6

    const v10, 0x1000001

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-string v14, "6fcf17"

    invoke-static/range {v10 .. v15}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    aput-object v10, v5, v4

    const/16 v4, 0xb

    new-array v15, v4, [B

    fill-array-data v15, :array_7

    const v10, 0x1000001

    const-string v14, "15143f"

    invoke-static/range {v10 .. v15}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    aput-object v10, v5, v6

    new-array v6, v4, [B

    fill-array-data v6, :array_8

    const v11, 0x1000001

    const/4 v12, 0x0

    const-wide/16 v13, 0x0

    const-string v15, "229f26"

    move-object/from16 v16, v6

    invoke-static/range {v11 .. v16}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    aput-object v6, v5, v0

    new-array v15, v4, [B

    fill-array-data v15, :array_9

    const v10, 0x1000001

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-string v14, "fbee72"

    invoke-static/range {v10 .. v15}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v9

    const/16 v0, 0xd

    new-array v14, v0, [B

    fill-array-data v14, :array_a

    const v9, 0x1000001

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-string v13, "856b74"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v1

    const/16 v0, 0xe

    new-array v14, v0, [B

    fill-array-data v14, :array_b

    const-string v13, "0e3a2d"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v2

    new-array v14, v0, [B

    fill-array-data v14, :array_c

    const-string v13, "0e63be"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v7

    new-array v14, v0, [B

    fill-array-data v14, :array_d

    const-string v13, "0f0845"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x7

    aput-object v0, v5, v1

    new-array v14, v3, [B

    fill-array-data v14, :array_e

    const-string v13, "b645c2"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v1, 0x8

    aput-object v0, v5, v1

    new-array v14, v8, [B

    fill-array-data v14, :array_f

    const-string v13, "0a8303"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v3

    new-array v14, v8, [B

    fill-array-data v14, :array_10

    const-string v13, "a5cc37"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v8

    new-array v14, v8, [B

    fill-array-data v14, :array_11

    const-string v13, "296531"

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v4

    sput-object v5, Lcom/bytedance/mobsec/metasec/ml/b;->k:[Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 1
        0x16t
        0x63t
        0x13t
        0x57t
        0x5ft
    .end array-data

    nop

    :array_1
    .array-data 1
        0x20t
        0x66t
        0x4t
        0x41t
        0x4at
        0x25t
        0x3dt
        0x41t
        0x3ct
    .end array-data

    nop

    :array_2
    .array-data 1
        0x4ct
        0x33t
        0x46t
        0x56t
        0xat
    .end array-data

    nop

    :array_3
    .array-data 1
        0x25t
        0x37t
        0x50t
        0x49t
        0x1et
        0x20t
        0x38t
        0x10t
        0x68t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x31t
        0x3ct
        0x41t
        0x51t
        0x0t
    .end array-data

    nop

    :array_5
    .array-data 1
        0x28t
        0x31t
        0x43t
        0x46t
        0x5t
        0x35t
    .end array-data

    nop

    :array_6
    .array-data 1
        0x34t
        0x67t
        0x2t
        0x17t
        0xbt
        0x2et
        0x26t
        0x4ft
        0x3dt
        0x22t
    .end array-data

    nop

    :array_7
    .array-data 1
        0x33t
        0x34t
        0x50t
        0x45t
        0x9t
        0x7ft
        0xdt
        0x7t
        0x68t
        0x6bt
        0x34t
    .end array-data

    :array_8
    .array-data 1
        0x30t
        0x33t
        0x58t
        0x17t
        0x8t
        0x2ft
        0x7ct
        0x0t
        0x60t
        0x39t
        0x37t
    .end array-data

    :array_9
    .array-data 1
        0x64t
        0x63t
        0x4t
        0x14t
        0xdt
        0x2bt
        0x25t
        0x50t
        0x3ct
        0x3at
        0x63t
    .end array-data

    :array_a
    .array-data 1
        0x3at
        0x34t
        0x57t
        0x13t
        0xdt
        0x2dt
        0x38t
        0x15t
        0x77t
        0x26t
        0x3ct
        0x25t
        0x40t
    .end array-data

    nop

    :array_b
    .array-data 1
        0x32t
        0x64t
        0x52t
        0x10t
        0x8t
        0x7dt
        0xct
        0x47t
        0x63t
        0x21t
        0x35t
        0x72t
        0x52t
        0x10t
    .end array-data

    nop

    :array_c
    .array-data 1
        0x32t
        0x64t
        0x57t
        0x42t
        0x58t
        0x7ct
        0x7et
        0x47t
        0x66t
        0x73t
        0x35t
        0x72t
        0x57t
        0x42t
    .end array-data

    nop

    :array_d
    .array-data 1
        0x32t
        0x67t
        0x51t
        0x49t
        0xet
        0x2ct
        0x73t
        0x44t
        0x60t
        0x78t
        0x35t
        0x71t
        0x51t
        0x49t
    .end array-data

    nop

    :array_e
    .array-data 1
        0x60t
        0x37t
        0x55t
        0x44t
        0x59t
        0x2bt
        0x62t
        0x16t
        0x75t
    .end array-data

    nop

    :array_f
    .array-data 1
        0x32t
        0x60t
        0x59t
        0x42t
        0xat
        0x2at
        0xct
        0x43t
        0x68t
        0x73t
    .end array-data

    nop

    :array_10
    .array-data 1
        0x63t
        0x34t
        0x2t
        0x12t
        0x9t
        0x2et
        0x2ft
        0x17t
        0x33t
        0x23t
    .end array-data

    nop

    :array_11
    .array-data 1
        0x30t
        0x38t
        0x57t
        0x44t
        0x9t
        0x28t
        0x71t
        0x1bt
        0x66t
        0x75t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 34
    .annotation runtime Lcom/bytedance/JProtect;
    .end annotation

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->b:Ljava/util/List;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->g:Z

    iput-boolean v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->h:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, v0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Application;

    const/16 v4, 0x20

    const/16 v5, 0x1a

    const/16 v6, 0xc

    const/16 v7, 0x68

    const/16 v8, 0x37

    const/16 v10, 0xb

    const/16 v11, 0xa

    const/16 v12, 0x9

    const/16 v13, 0x8

    const/4 v14, 0x7

    const/4 v15, 0x5

    const/16 v16, 0x4

    const/16 v17, 0x3

    const/4 v3, 0x6

    const/4 v9, 0x2

    const/16 v20, 0x1

    if-nez v2, :cond_1

    :try_start_0
    new-array v2, v5, [B

    aput-byte v4, v2, v1

    const/16 v21, 0x6e

    aput-byte v21, v2, v20

    const/16 v21, 0x4f

    aput-byte v21, v2, v9

    aput-byte v1, v2, v17

    const/16 v22, 0x54

    aput-byte v22, v2, v16

    const/16 v22, 0x29

    aput-byte v22, v2, v15

    aput-byte v8, v2, v3

    const/16 v27, 0xd

    aput-byte v27, v2, v14

    aput-byte v7, v2, v13

    const/16 v22, 0x26

    aput-byte v22, v2, v12

    const/16 v22, 0x31

    aput-byte v22, v2, v11

    const/16 v22, 0x2e

    aput-byte v22, v2, v10

    const/16 v22, 0x6a

    aput-byte v22, v2, v6

    const/16 v19, 0x11

    aput-byte v19, v2, v27

    const/16 v22, 0xe

    aput-byte v21, v2, v22

    const/16 v21, 0xf

    const/16 v22, 0x29

    aput-byte v22, v2, v21

    const/16 v21, 0x10

    const/16 v22, 0x25

    aput-byte v22, v2, v21

    const/16 v21, 0x4a

    const/16 v19, 0x11

    aput-byte v21, v2, v19

    const/16 v21, 0x12

    const/16 v22, 0x7d

    aput-byte v22, v2, v21

    const/16 v21, 0x13

    const/16 v22, 0x2f

    aput-byte v22, v2, v21

    const/16 v21, 0x14

    const/16 v22, 0x15

    aput-byte v22, v2, v21

    const/16 v21, 0x15

    aput-byte v7, v2, v21

    const/16 v21, 0x16

    const/16 v22, 0x59

    aput-byte v22, v2, v21

    const/16 v21, 0x17

    const/16 v22, 0x17

    aput-byte v22, v2, v21

    const/16 v21, 0x18

    const/16 v22, 0x5a

    aput-byte v22, v2, v21

    const/16 v21, 0x19

    const/16 v22, 0x24

    aput-byte v22, v2, v21

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "0b8fd7"

    move-object/from16 v26, v2

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/16 v4, 0x12

    new-array v4, v4, [B

    const/16 v22, 0x71

    aput-byte v22, v4, v1

    const/16 v22, 0x23

    aput-byte v22, v4, v20

    const/16 v22, 0x57

    aput-byte v22, v4, v9

    const/16 v22, 0x5f

    aput-byte v22, v4, v17

    aput-byte v12, v4, v16

    const/16 v22, 0x2b

    aput-byte v22, v4, v15

    const/16 v22, 0x74

    aput-byte v22, v4, v3

    const/16 v22, 0x34

    aput-byte v22, v4, v14

    const/16 v22, 0x77

    aput-byte v22, v4, v13

    const/16 v22, 0x79

    aput-byte v22, v4, v12

    const/16 v22, 0x7e

    aput-byte v22, v4, v11

    const/16 v22, 0x3f

    aput-byte v22, v4, v10

    const/16 v22, 0x46

    aput-byte v22, v4, v6

    const/16 v22, 0x4c

    aput-byte v22, v4, v27

    const/16 v22, 0xe

    const/16 v23, 0x18

    aput-byte v23, v4, v22

    const/16 v22, 0xf

    const/16 v23, 0x2c

    aput-byte v23, v4, v22

    const/16 v22, 0x10

    const/16 v23, 0x6f

    aput-byte v23, v4, v22

    const/16 v18, 0x1b

    const/16 v19, 0x11

    aput-byte v18, v4, v19

    const v28, 0x1000001

    const/16 v29, 0x0

    const-wide/16 v30, 0x0

    const-string v32, "c46932"

    move-object/from16 v33, v4

    invoke-static/range {v28 .. v33}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-array v6, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v2, v4, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    check-cast v2, Landroid/app/Application;

    goto :goto_0

    :catchall_0
    const/4 v4, 0x0

    :catchall_1
    move-object v2, v4

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    :cond_0
    check-cast v2, Landroid/content/Context;

    iput-object v2, v0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    :try_start_2
    new-array v2, v3, [B

    aput-byte v8, v2, v1

    aput-byte v7, v2, v20

    const/16 v6, 0x1b

    aput-byte v6, v2, v9

    const/16 v6, 0x42

    aput-byte v6, v2, v17

    const/16 v6, 0x54

    aput-byte v6, v2, v16

    const/16 v6, 0x61

    aput-byte v6, v2, v15

    const v23, 0x1000001

    const/16 v24, 0x0

    const-wide/16 v25, 0x0

    const-string v27, "1cf2da"

    move-object/from16 v28, v2

    invoke-static/range {v23 .. v28}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v6, p1

    invoke-virtual {v6, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    if-nez v2, :cond_2

    goto/16 :goto_3

    :cond_2
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-lt v4, v7, :cond_3

    invoke-virtual {v2, v6}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto/16 :goto_2

    :cond_3
    :try_start_4
    const-class v4, Landroid/view/Display;

    new-array v7, v10, [B

    const/16 v18, 0x25

    aput-byte v18, v7, v1

    const/16 v18, 0x35

    aput-byte v18, v7, v20

    const/16 v18, 0x55

    aput-byte v18, v7, v9

    const/16 v18, 0x22

    aput-byte v18, v7, v17

    aput-byte v13, v7, v16

    aput-byte v8, v7, v15

    aput-byte v14, v7, v3

    aput-byte v5, v7, v14

    const/16 v5, 0x67

    aput-byte v5, v7, v13

    const/16 v5, 0x20

    aput-byte v5, v7, v12

    const/16 v5, 0x2a

    aput-byte v5, v7, v11

    const v23, 0x1000001

    const/16 v24, 0x0

    const-wide/16 v25, 0x0

    const-string v27, "322d67"

    move-object/from16 v28, v7

    invoke-static/range {v23 .. v28}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-array v7, v1, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const-class v5, Landroid/view/Display;

    const/16 v7, 0xc

    new-array v7, v7, [B

    const/16 v8, 0x23

    aput-byte v8, v7, v1

    const/16 v8, 0x62

    aput-byte v8, v7, v20

    const/16 v8, 0x50

    aput-byte v8, v7, v9

    const/16 v8, 0x72

    aput-byte v8, v7, v17

    aput-byte v11, v7, v16

    const/16 v8, 0x64

    aput-byte v8, v7, v15

    const/16 v8, 0x1e

    aput-byte v8, v7, v3

    const/16 v3, 0x41

    aput-byte v3, v7, v14

    const/16 v3, 0x6f

    aput-byte v3, v7, v13

    const/16 v3, 0x63

    aput-byte v3, v7, v12

    const/16 v3, 0x2c

    aput-byte v3, v7, v11

    const/16 v3, 0x73

    aput-byte v3, v7, v10

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "5e744d"

    move-object/from16 v26, v7

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-array v7, v1, [Ljava/lang/Class;

    invoke-virtual {v5, v3, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v4, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v6, v4, v3}, Landroid/graphics/Point;->set(II)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_2

    :catchall_2
    :try_start_5
    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v4

    invoke-virtual {v6, v3, v4}, Landroid/graphics/Point;->set(II)V

    invoke-virtual {v2, v6}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :catchall_3
    :goto_2
    move-object v3, v6

    goto :goto_4

    :catchall_4
    :goto_3
    move-object v3, v4

    :goto_4
    iput-object v3, v0, Lcom/bytedance/mobsec/metasec/ml/b;->a:Landroid/graphics/Point;

    new-instance v2, Landroid/os/HandlerThread;

    new-array v8, v9, [B

    fill-array-data v8, :array_0

    const v3, 0x1000001

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-string v7, "861e4c"

    invoke-static/range {v3 .. v8}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, v0, Lcom/bytedance/mobsec/metasec/ml/b;->e:Landroid/os/HandlerThread;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    if-lt v2, v3, :cond_4

    const/4 v1, 0x1

    :cond_4
    iput-boolean v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->h:Z

    return-void

    nop

    :array_0
    .array-data 1
        0x1at
        0x7t
    .end array-data
.end method

.method private a(Landroid/net/Uri;)V
    .locals 35
    .annotation runtime Lcom/bytedance/JProtect;
    .end annotation

    move-object/from16 v0, p0

    invoke-static {}, Lms/bd/c/c0;->b()I

    move-result v1

    const/16 v2, 0x20

    const/16 v5, 0x9

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x1

    if-eq v1, v8, :cond_0

    const/16 v1, 0x21

    const/16 v3, 0x28

    const/16 v4, 0x21

    const/4 v5, 0x1

    goto/16 :goto_a

    :cond_0
    :try_start_0
    iget-object v9, v0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0x10

    if-ge v9, v15, :cond_1

    sget-object v9, Lcom/bytedance/mobsec/metasec/ml/b;->i:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v9, Lcom/bytedance/mobsec/metasec/ml/b;->j:[Ljava/lang/String;

    :goto_0
    move-object v12, v9

    const/16 v9, 0x17

    new-array v9, v9, [B

    const/4 v14, 0x0

    aput-byte v2, v9, v14

    const/16 v11, 0x33

    aput-byte v11, v9, v8

    aput-byte v7, v9, v6

    const/16 v11, 0x42

    aput-byte v11, v9, v7

    const/16 v11, 0x37

    const/16 v22, 0x4

    aput-byte v11, v9, v22

    const/16 v23, 0x22

    const/4 v13, 0x5

    aput-byte v23, v9, v13

    const/16 v24, 0x32

    const/4 v11, 0x6

    aput-byte v24, v9, v11

    const/16 v16, 0x15

    const/16 v25, 0x7

    aput-byte v16, v9, v25

    const/16 v17, 0x30

    const/16 v26, 0x8

    aput-byte v17, v9, v26

    const/16 v27, 0x67

    aput-byte v27, v9, v5

    const/16 v18, 0xa

    const/16 v19, 0x64

    aput-byte v19, v9, v18

    const/16 v18, 0x36

    const/16 v19, 0xb

    aput-byte v18, v9, v19

    const/16 v18, 0xc

    const/16 v20, 0x12

    aput-byte v20, v9, v18

    const/16 v18, 0xd

    const/16 v20, 0x54

    aput-byte v20, v9, v18

    const/16 v18, 0xe

    aput-byte v19, v9, v18

    const/16 v18, 0xf

    const/16 v19, 0x63

    aput-byte v19, v9, v18

    const/16 v18, 0x3a

    aput-byte v18, v9, v15

    const/16 v18, 0x11

    const/16 v28, 0x18

    aput-byte v28, v9, v18

    const/16 v18, 0x12

    const/16 v19, 0x38

    aput-byte v19, v9, v18

    const/16 v18, 0x13

    const/16 v19, 0x6a

    aput-byte v19, v9, v18

    const/16 v18, 0x14

    aput-byte v17, v9, v18

    const/16 v17, 0x72

    aput-byte v17, v9, v16

    const/16 v16, 0x16

    const/16 v17, 0x46

    aput-byte v17, v9, v16

    const/16 v29, 0x0

    const/16 v30, 0x0

    const v16, 0x1000001

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    const-string v20, "50d374"

    move-object/from16 v21, v9

    invoke-static/range {v16 .. v21}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const/4 v1, 0x6

    move-object/from16 v11, p1

    const/4 v2, 0x5

    move-object/from16 v13, v29

    const/4 v3, 0x0

    move-object/from16 v14, v30

    const/16 v4, 0x10

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v9, :cond_3

    if-eqz v9, :cond_16

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    :cond_2
    const/16 v3, 0x28

    const/16 v4, 0x52

    const/4 v5, 0x3

    goto/16 :goto_b

    :cond_3
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v10, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    return-void

    :cond_5
    :try_start_2
    new-array v15, v2, [B

    aput-byte v28, v15, v3

    const/16 v10, 0x62

    aput-byte v10, v15, v8

    const/16 v10, 0x43

    aput-byte v10, v15, v6

    aput-byte v8, v15, v7

    const/16 v10, 0xc

    aput-byte v10, v15, v22

    const v10, 0x1000001

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-string v14, "6d1a24"

    invoke-static/range {v10 .. v15}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    new-array v15, v5, [B

    const/16 v11, 0x73

    aput-byte v11, v15, v3

    const/16 v11, 0x3b

    aput-byte v11, v15, v8

    const/16 v11, 0x5f

    aput-byte v11, v15, v6

    const/16 v11, 0x49

    aput-byte v11, v15, v7

    aput-byte v28, v15, v22

    aput-byte v23, v15, v2

    const/16 v11, 0x6e

    aput-byte v11, v15, v1

    const/16 v11, 0x1c

    aput-byte v11, v15, v25

    aput-byte v27, v15, v26

    const v11, 0x1000001

    const/4 v12, 0x0

    const-wide/16 v13, 0x0

    const-string v16, "f88834"

    move-object/from16 v18, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v18

    invoke-static/range {v11 .. v16}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v14, -0x1

    if-lt v12, v4, :cond_6

    new-array v4, v2, [B

    aput-byte v24, v4, v3

    const/16 v12, 0x6d

    aput-byte v12, v4, v8

    const/16 v12, 0x13

    aput-byte v12, v4, v6

    const/16 v12, 0x52

    aput-byte v12, v4, v7

    aput-byte v8, v4, v22

    const v29, 0x1000001

    const/16 v30, 0x0

    const-wide/16 v31, 0x0

    const-string v33, "4fd269"

    move-object/from16 v34, v4

    invoke-static/range {v29 .. v34}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    new-array v1, v1, [B

    const/16 v13, 0x7d

    aput-byte v13, v1, v3

    const/16 v13, 0x66

    aput-byte v13, v1, v8

    aput-byte v28, v1, v6

    const/16 v13, 0x11

    aput-byte v13, v1, v7

    const/16 v13, 0xe

    aput-byte v13, v1, v22

    const/16 v13, 0x3b

    aput-byte v13, v1, v2

    const v29, 0x1000001

    const/16 v30, 0x0

    const-wide/16 v31, 0x0

    const-string v33, "dabb98"

    move-object/from16 v34, v1

    invoke-static/range {v29 .. v34}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    :cond_6
    const/16 v12, 0x52

    const/4 v1, -0x1

    const/4 v4, -0x1

    :goto_1
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    if-ltz v4, :cond_7

    if-ltz v1, :cond_7

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_2

    :cond_7
    const/4 v1, -0x1

    const/4 v4, -0x1

    :goto_2
    if-eq v4, v14, :cond_c

    if-eq v1, v14, :cond_c

    iget-object v13, v0, Lcom/bytedance/mobsec/metasec/ml/b;->a:Landroid/graphics/Point;

    if-nez v13, :cond_8

    goto :goto_4

    :cond_8
    iget v15, v13, Landroid/graphics/Point;->x:I

    if-gt v4, v15, :cond_9

    iget v13, v13, Landroid/graphics/Point;->y:I

    if-le v1, v13, :cond_a

    :cond_9
    iget-object v13, v0, Lcom/bytedance/mobsec/metasec/ml/b;->a:Landroid/graphics/Point;

    iget v15, v13, Landroid/graphics/Point;->x:I

    if-gt v1, v15, :cond_b

    iget v1, v13, Landroid/graphics/Point;->y:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-le v4, v1, :cond_a

    goto :goto_3

    :cond_a
    const/4 v1, 0x1

    goto :goto_5

    :cond_b
    :goto_3
    const/4 v1, -0x1

    goto :goto_5

    :cond_c
    :goto_4
    const/4 v1, 0x0

    :goto_5
    if-ne v1, v14, :cond_e

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_c

    :cond_d
    const/16 v4, 0x52

    goto :goto_d

    :cond_e
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    sub-long/2addr v12, v10

    const-wide/16 v10, 0x2710

    cmp-long v1, v12, v10

    if-lez v1, :cond_10

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_f
    return-void

    :cond_10
    :try_start_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    goto :goto_7

    :cond_11
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/bytedance/mobsec/metasec/ml/b;->k:[Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    array-length v10, v4

    const/4 v11, 0x0

    :goto_6
    if-ge v11, v10, :cond_13

    :try_start_5
    aget-object v12, v4, v11

    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v12, :cond_12

    const/4 v1, 0x1

    goto :goto_8

    :cond_12
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    :cond_13
    :goto_7
    const/4 v1, -0x1

    :goto_8
    if-ne v1, v14, :cond_17

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_e

    :cond_14
    const/16 v1, 0x35

    const/16 v3, 0x35

    :goto_9
    const/16 v4, 0x20

    :cond_15
    :goto_a
    :pswitch_0
    packed-switch v4, :pswitch_data_0

    goto :goto_9

    :goto_b
    :pswitch_1
    packed-switch v3, :pswitch_data_1

    goto :goto_d

    :pswitch_2
    if-eq v5, v6, :cond_15

    if-eq v5, v7, :cond_16

    nop

    :cond_16
    :goto_c
    :pswitch_3
    return-void

    :goto_d
    const/16 v3, 0x27

    goto :goto_b

    :goto_e
    :pswitch_4
    return-void

    :cond_17
    :try_start_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    goto :goto_f

    :cond_18
    iget-object v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    :goto_f
    const/4 v8, -0x1

    goto :goto_10

    :cond_19
    iget-object v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v4, 0x14

    if-lt v1, v4, :cond_1a

    iget-object v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1a
    iget-object v1, v0, Lcom/bytedance/mobsec/metasec/ml/b;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_10
    if-ne v8, v14, :cond_1c

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1b
    return-void

    :cond_1c
    const v1, 0x10000002

    :try_start_7
    invoke-static {v1}, Lms/bd/c/b;->a(I)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1d

    goto :goto_12

    :catchall_0
    move-object v1, v9

    goto :goto_11

    :catchall_1
    const/4 v1, 0x0

    :goto_11
    if-eqz v1, :cond_1d

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1d

    move-object v9, v1

    :goto_12
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1d
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x26
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/bytedance/mobsec/metasec/ml/b;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/bytedance/mobsec/metasec/ml/b;->a(Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/bytedance/mobsec/metasec/ml/b;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/bytedance/mobsec/metasec/ml/b$a;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v2, v0}, Lcom/bytedance/mobsec/metasec/ml/b$a;-><init>(Lcom/bytedance/mobsec/metasec/ml/b;Landroid/net/Uri;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/bytedance/mobsec/metasec/ml/b;->c:Lcom/bytedance/mobsec/metasec/ml/b$a;

    new-instance v1, Lcom/bytedance/mobsec/metasec/ml/b$a;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v2, v0}, Lcom/bytedance/mobsec/metasec/ml/b$a;-><init>(Lcom/bytedance/mobsec/metasec/ml/b;Landroid/net/Uri;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/bytedance/mobsec/metasec/ml/b;->d:Lcom/bytedance/mobsec/metasec/ml/b$a;

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/bytedance/mobsec/metasec/ml/b;->h:Z

    iget-object v3, p0, Lcom/bytedance/mobsec/metasec/ml/b;->c:Lcom/bytedance/mobsec/metasec/ml/b$a;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/bytedance/mobsec/metasec/ml/b;->h:Z

    iget-object v3, p0, Lcom/bytedance/mobsec/metasec/ml/b;->d:Lcom/bytedance/mobsec/metasec/ml/b$a;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    monitor-exit p0

    return-void

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->c:Lcom/bytedance/mobsec/metasec/ml/b$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/mobsec/metasec/ml/b;->c:Lcom/bytedance/mobsec/metasec/ml/b$a;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    :try_start_2
    iput-object v1, p0, Lcom/bytedance/mobsec/metasec/ml/b;->c:Lcom/bytedance/mobsec/metasec/ml/b$a;

    :cond_0
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->d:Lcom/bytedance/mobsec/metasec/ml/b$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v0, :cond_1

    :try_start_3
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/mobsec/metasec/ml/b;->d:Lcom/bytedance/mobsec/metasec/ml/b$a;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    :try_start_4
    iput-object v1, p0, Lcom/bytedance/mobsec/metasec/ml/b;->d:Lcom/bytedance/mobsec/metasec/ml/b$a;

    :cond_1
    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/b;->e:Landroid/os/HandlerThread;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    if-eqz v0, :cond_2

    :try_start_5
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    :cond_2
    monitor-exit p0

    return-void

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/bytedance/mobsec/metasec/ml/b;->b()V

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method
