.class public final Lcom/bytedance/mobsec/metasec/ml/MSManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lms/bd/c/m1$a;


# instance fields
.field private a:Lms/bd/c/m1$a;


# direct methods
.method constructor <init>(Lms/bd/c/m1$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    return-void
.end method


# virtual methods
.method public doHttpReqSignByUrl(Ljava/lang/String;[B)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1, p2}, Lms/bd/c/m1$a;->doHttpReqSignByUrl(Ljava/lang/String;[B)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public getSecDeviceToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0}, Lms/bd/c/m1$a;->getSecDeviceToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public report(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1}, Lms/bd/c/m1$a;->report(Ljava/lang/String;)V

    return-void
.end method

.method public setBDDeviceID(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1}, Lms/bd/c/m1$a;->setBDDeviceID(Ljava/lang/String;)V

    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1}, Lms/bd/c/m1$a;->setDeviceID(Ljava/lang/String;)V

    return-void
.end method

.method public setInstallID(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1}, Lms/bd/c/m1$a;->setInstallID(Ljava/lang/String;)V

    return-void
.end method

.method public setSessionID(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/mobsec/metasec/ml/MSManager;->a:Lms/bd/c/m1$a;

    invoke-interface {v0, p1}, Lms/bd/c/m1$a;->setSessionID(Ljava/lang/String;)V

    return-void
.end method
