.class public Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;
.super Lms/bd/c/e0$a;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/mobsec/metasec/ml/MSConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lms/bd/c/e0$a<",
        "Lcom/bytedance/mobsec/metasec/ml/MSConfig$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lms/bd/c/e0$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lms/bd/c/e0$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/bytedance/mobsec/metasec/ml/MSConfig;
    .locals 3

    new-instance v0, Lcom/bytedance/mobsec/metasec/ml/MSConfig;

    invoke-virtual {p0}, Lms/bd/c/e0$a;->b()Lms/bd/c/e0;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/bytedance/mobsec/metasec/ml/MSConfig;-><init>(Lms/bd/c/e0;Lcom/bytedance/mobsec/metasec/ml/MSConfig$a;)V

    return-object v0
.end method
