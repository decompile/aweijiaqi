.class public final Lcom/bytedance/mobsec/metasec/ml/MSManagerUtils;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized get(Ljava/lang/String;)Lcom/bytedance/mobsec/metasec/ml/MSManager;
    .locals 2

    const-class v0, Lcom/bytedance/mobsec/metasec/ml/MSManagerUtils;

    monitor-enter v0

    :try_start_0
    invoke-static {p0}, Lms/bd/c/m1;->a(Ljava/lang/String;)Lms/bd/c/m1$a;

    move-result-object p0

    if-eqz p0, :cond_0

    new-instance v1, Lcom/bytedance/mobsec/metasec/ml/MSManager;

    invoke-direct {v1, p0}, Lcom/bytedance/mobsec/metasec/ml/MSManager;-><init>(Lms/bd/c/m1$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized init(Landroid/content/Context;Lcom/bytedance/mobsec/metasec/ml/MSConfig;)Z
    .locals 2

    const-class v0, Lcom/bytedance/mobsec/metasec/ml/MSManagerUtils;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/mobsec/metasec/ml/MSConfig;->b()Lms/bd/c/e0;

    move-result-object p1

    const-string v1, "metasec_ml"

    invoke-static {p0, p1, v1}, Lms/bd/c/m1;->a(Landroid/content/Context;Lms/bd/c/e0;Ljava/lang/String;)Z

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static versionInfo()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lms/bd/c/m1;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
