.class final Lcom/bytedance/mobsec/metasec/ml/a$d;
.super Lms/bd/c/b$a;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/mobsec/metasec/ml/a;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lms/bd/c/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-static {}, Lms/bd/c/a;->a()Lms/bd/c/a;

    move-result-object p1

    invoke-virtual {p1}, Lms/bd/c/a;->b()Landroid/content/Context;

    move-result-object p1

    .line 1
    invoke-static {}, Lms/bd/c/c0;->b()I

    move-result p2

    const/4 p3, 0x1

    if-eq p2, p3, :cond_0

    goto/16 :goto_1

    :cond_0
    :try_start_0
    const-string p2, "location"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/location/LocationManager;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    new-instance p4, Ljava/lang/String;

    const-string p5, "6765744c6173744b6e6f776e4c6f636174696f6e"

    invoke-static {p5}, Lms/bd/c/c0;->a(Ljava/lang/String;)[B

    move-result-object p5

    invoke-direct {p4, p5}, Ljava/lang/String;-><init>([B)V

    new-array p5, p3, [Ljava/lang/Class;

    const-class v0, Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v0, p5, v1

    invoke-virtual {p2, p4, p5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p2

    invoke-virtual {p2, p3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array p4, p3, [Ljava/lang/Object;

    const-string p5, "gps"

    aput-object p5, p4, v1

    invoke-virtual {p2, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroid/location/Location;

    if-eqz p4, :cond_1

    goto :goto_0

    :cond_1
    new-array p4, p3, [Ljava/lang/Object;

    const-string p5, "network"

    aput-object p5, p4, v1

    invoke-virtual {p2, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroid/location/Location;

    if-eqz p4, :cond_2

    goto :goto_0

    :cond_2
    new-array p4, p3, [Ljava/lang/Object;

    const-string p5, "passive"

    aput-object p5, p4, v1

    invoke-virtual {p2, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object p4, p1

    check-cast p4, Landroid/location/Location;

    :goto_0
    if-eqz p4, :cond_4

    new-instance p1, Ljava/lang/String;

    const-string p2, "616e64726f69642e6c6f636174696f6e2e4c6f636174696f6e"

    invoke-static {p2}, Lms/bd/c/c0;->a(Ljava/lang/String;)[B

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/String;-><init>([B)V

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    if-eqz p1, :cond_4

    new-instance p2, Ljava/lang/String;

    const-string p5, "6765744c6f6e676974756465"

    invoke-static {p5}, Lms/bd/c/c0;->a(Ljava/lang/String;)[B

    move-result-object p5

    invoke-direct {p2, p5}, Ljava/lang/String;-><init>([B)V

    new-array p5, v1, [Ljava/lang/Class;

    invoke-virtual {p1, p2, p5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p2

    new-instance p5, Ljava/lang/String;

    const-string v0, "6765744c61746974756465"

    invoke-static {v0}, Lms/bd/c/c0;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p5, v0}, Ljava/lang/String;-><init>([B)V

    new-array v0, v1, [Ljava/lang/Class;

    invoke-virtual {p1, p5, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p2, :cond_4

    if-eqz p1, :cond_4

    invoke-virtual {p2, p3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    invoke-virtual {p1, p3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, ""

    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array p5, v1, [Ljava/lang/Object;

    invoke-virtual {p2, p4, p5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ","

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array p2, v1, [Ljava/lang/Object;

    invoke-virtual {p1, p4, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "null LM"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_4
    :goto_1
    const/4 p1, 0x0

    :goto_2
    invoke-static {p1}, Lms/bd/c/a2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
