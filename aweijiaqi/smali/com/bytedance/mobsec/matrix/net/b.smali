.class public final Lcom/bytedance/mobsec/matrix/net/b;
.super Lcom/bytedance/mobsec/matrix/net/a;
.source ""


# instance fields
.field private a:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/mobsec/matrix/net/a;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)[Ljava/lang/Object;
    .locals 32

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p4

    new-instance v3, Lms/bd/c/j$a;

    invoke-direct {v3}, Lms/bd/c/j$a;-><init>()V

    const/16 v5, 0x30

    const/4 v8, 0x0

    const/4 v10, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x4

    const/4 v14, 0x3

    const/4 v15, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/16 v17, -0x1

    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    instance-of v11, v7, Ljavax/net/ssl/HttpsURLConnection;

    const/16 v20, 0x1f

    if-eqz v11, :cond_2

    iget-object v11, v1, Lcom/bytedance/mobsec/matrix/net/b;->a:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v11, :cond_1

    const-class v11, Lcom/bytedance/mobsec/matrix/net/b;

    monitor-enter v11
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v9, v1, Lcom/bytedance/mobsec/matrix/net/b;->a:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v9, :cond_0

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "5187f3"

    :try_start_3
    new-array v9, v14, [B

    const/16 v27, 0x10

    aput-byte v27, v9, v6

    aput-byte v20, v9, v4

    const/16 v27, 0x78

    aput-byte v27, v9, v15

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v9

    invoke-virtual {v9, v8, v8, v8}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {v9}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v9

    iput-object v9, v1, Lcom/bytedance/mobsec/matrix/net/b;->a:Ljavax/net/ssl/SSLSocketFactory;

    :cond_0
    monitor-exit v11

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :cond_1
    :goto_0
    move-object v9, v7

    check-cast v9, Ljavax/net/ssl/HttpsURLConnection;

    iget-object v11, v1, Lcom/bytedance/mobsec/matrix/net/b;->a:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v9, v11}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_2
    const/16 v9, 0x2710

    invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/16 v9, 0x1388

    invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "042ea3"

    :try_start_5
    new-array v9, v12, [B

    aput-byte v6, v9, v6

    const/16 v11, 0x35

    aput-byte v11, v9, v4

    const/16 v11, 0x42

    aput-byte v11, v9, v15

    const/16 v28, 0x14

    aput-byte v28, v9, v14

    const/16 v27, 0x4e

    aput-byte v27, v9, v13

    aput-byte v5, v9, v10

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "cdc3db"

    :try_start_6
    new-array v8, v14, [B

    const/16 v27, 0x38

    aput-byte v27, v8, v6

    const/16 v27, 0x29

    aput-byte v27, v8, v4

    const/16 v27, 0x5a

    aput-byte v27, v8, v15

    move-object/from16 v27, v8

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v9, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "f4d8a3"

    const/16 v8, 0xa

    :try_start_7
    new-array v9, v8, [B

    const/16 v8, 0x54

    aput-byte v8, v9, v6

    const/16 v27, 0x39

    aput-byte v27, v9, v4

    const/16 v27, 0x19

    aput-byte v27, v9, v15

    aput-byte v11, v9, v14

    const/16 v27, 0x5b

    aput-byte v27, v9, v13

    const/16 v27, 0x27

    aput-byte v27, v9, v10

    const/16 v27, 0x71

    aput-byte v27, v9, v12

    const/16 v27, 0x1c

    const/16 v19, 0x7

    aput-byte v27, v9, v19

    const/16 v27, 0x3a

    const/16 v18, 0x8

    aput-byte v27, v9, v18

    const/16 v30, 0x66

    const/16 v16, 0x9

    aput-byte v30, v9, v16

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "b30ff7"

    const/16 v5, 0xa

    :try_start_8
    new-array v8, v5, [B

    const/16 v5, 0x58

    aput-byte v5, v8, v6

    const/16 v5, 0x34

    aput-byte v5, v8, v4

    const/16 v5, 0x46

    aput-byte v5, v8, v15

    aput-byte v15, v8, v14

    aput-byte v28, v8, v13

    aput-byte v4, v8, v10

    const/16 v5, 0x6d

    aput-byte v5, v8, v12

    const/16 v5, 0x1b

    const/16 v19, 0x7

    aput-byte v5, v8, v19

    const/16 v5, 0x77

    const/16 v18, 0x8

    aput-byte v5, v8, v18

    const/16 v27, 0x33

    const/16 v16, 0x9

    aput-byte v27, v8, v16

    move-object/from16 v27, v8

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v9, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "3fbf77"

    const/16 v8, 0xa

    :try_start_9
    new-array v9, v8, [B

    const/16 v8, 0x17

    aput-byte v8, v9, v6

    aput-byte v5, v9, v4

    aput-byte v28, v9, v15

    aput-byte v6, v9, v14

    const/16 v8, 0x45

    aput-byte v8, v9, v13

    aput-byte v4, v9, v10

    const/16 v8, 0x37

    aput-byte v8, v9, v12

    const/4 v8, 0x7

    aput-byte v11, v9, v8

    const/16 v8, 0x3d

    const/16 v11, 0x8

    aput-byte v8, v9, v11

    const/16 v8, 0x22

    const/16 v11, 0x9

    aput-byte v8, v9, v11

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "9d660b"

    const/16 v9, 0xf

    :try_start_a
    new-array v9, v9, [B

    const/16 v11, 0xa

    aput-byte v11, v9, v6

    const/16 v11, 0x7f

    aput-byte v11, v9, v4

    const/16 v11, 0x51

    aput-byte v11, v9, v15

    const/16 v11, 0x47

    aput-byte v11, v9, v14

    const/16 v11, 0x2b

    aput-byte v11, v9, v13

    const/16 v11, 0x74

    aput-byte v11, v9, v10

    const/16 v11, 0x34

    aput-byte v11, v9, v12

    const/16 v11, 0x46

    const/16 v19, 0x7

    aput-byte v11, v9, v19

    const/16 v11, 0x62

    const/16 v18, 0x8

    aput-byte v11, v9, v18

    const/16 v11, 0x2b

    const/16 v16, 0x9

    aput-byte v11, v9, v16

    const/16 v11, 0xa

    aput-byte v10, v9, v11

    const/16 v11, 0x55

    const/16 v31, 0xb

    aput-byte v11, v9, v31

    const/16 v11, 0x76

    const/16 v5, 0xc

    aput-byte v11, v9, v5

    const/16 v11, 0xd

    aput-byte v30, v9, v11

    const/16 v11, 0xe

    const/16 v27, 0x24

    aput-byte v27, v9, v11

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "a6c4ae"

    :try_start_b
    new-array v8, v5, [B

    const/16 v9, 0x53

    aput-byte v9, v8, v6

    const/16 v9, 0x3b

    aput-byte v9, v8, v4

    const/16 v9, 0x1e

    aput-byte v9, v8, v15

    const/16 v9, 0x54

    aput-byte v9, v8, v14

    const/16 v9, 0x5b

    aput-byte v9, v8, v13

    const/16 v9, 0x7c

    aput-byte v9, v8, v10

    const/16 v9, 0x76

    aput-byte v9, v8, v12

    const/16 v9, 0x5a

    const/4 v11, 0x7

    aput-byte v9, v8, v11

    const/16 v9, 0x8

    aput-byte v12, v8, v9

    const/16 v9, 0x7d

    const/16 v11, 0x9

    aput-byte v9, v8, v11

    const/16 v9, 0x60

    const/16 v11, 0xa

    aput-byte v9, v8, v11

    const/16 v9, 0x31

    aput-byte v9, v8, v31

    move-object/from16 v27, v8

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "cacf8e"

    const/16 v9, 0x18

    :try_start_c
    new-array v9, v9, [B

    const/16 v11, 0x73

    aput-byte v11, v9, v6

    const/16 v11, 0x73

    aput-byte v11, v9, v4

    aput-byte v6, v9, v15

    const/16 v11, 0x1e

    aput-byte v11, v9, v14

    const/16 v11, 0xe

    aput-byte v11, v9, v13

    const/16 v11, 0x71

    aput-byte v11, v9, v10

    const/16 v11, 0x61

    aput-byte v11, v9, v12

    const/16 v11, 0x54

    const/16 v19, 0x7

    aput-byte v11, v9, v19

    const/16 v11, 0x3b

    const/16 v18, 0x8

    aput-byte v11, v9, v18

    const/16 v11, 0x39

    const/16 v16, 0x9

    aput-byte v11, v9, v16

    const/16 v11, 0x7c

    const/16 v21, 0xa

    aput-byte v11, v9, v21

    const/16 v11, 0x2c

    aput-byte v11, v9, v31

    aput-byte v20, v9, v5

    const/16 v11, 0xd

    const/16 v27, 0x11

    aput-byte v27, v9, v11

    const/16 v11, 0xe

    const/16 v27, 0x13

    aput-byte v27, v9, v11

    const/16 v11, 0xf

    const/16 v27, 0x77

    aput-byte v27, v9, v11

    const/16 v11, 0x10

    const/16 v27, 0x74

    aput-byte v27, v9, v11

    const/16 v11, 0x11

    const/16 v27, 0xd

    aput-byte v27, v9, v11

    const/16 v11, 0x12

    const/16 v27, 0x21

    aput-byte v27, v9, v11

    const/16 v11, 0x13

    const/16 v27, 0x22

    aput-byte v27, v9, v11

    const/16 v11, 0x60

    aput-byte v11, v9, v28

    const/16 v11, 0x15

    aput-byte v30, v9, v11

    const/16 v11, 0x16

    const/16 v27, 0x11

    aput-byte v27, v9, v11

    const/16 v11, 0x17

    aput-byte v20, v9, v11

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p3, :cond_3

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    if-lez v8, :cond_3

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "6c53b7"

    :try_start_d
    new-array v8, v12, [B

    aput-byte v13, v8, v6

    const/16 v9, 0x6e

    aput-byte v9, v8, v4

    const/16 v9, 0x49

    aput-byte v9, v8, v15

    const/16 v9, 0x4c

    aput-byte v9, v8, v14

    const/16 v9, 0x54

    aput-byte v9, v8, v13

    const/16 v9, 0x25

    aput-byte v9, v8, v10

    move-object/from16 v27, v8

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "23df11"

    :try_start_e
    new-array v5, v5, [B

    const/16 v9, 0x30

    aput-byte v9, v5, v6

    const/16 v9, 0x34

    aput-byte v9, v5, v4

    aput-byte v13, v5, v15

    aput-byte v4, v5, v14

    const/4 v9, 0x7

    aput-byte v9, v5, v13

    const/16 v11, 0x29

    aput-byte v11, v5, v10

    const/16 v11, 0x3f

    aput-byte v11, v5, v12

    const/16 v11, 0x1b

    aput-byte v11, v5, v9

    const/16 v9, 0x31

    const/16 v11, 0x8

    aput-byte v9, v5, v11

    const/16 v9, 0x6b

    const/16 v11, 0x9

    aput-byte v9, v5, v11

    const/16 v9, 0xa

    aput-byte v30, v5, v9

    const/16 v9, 0x22

    aput-byte v9, v5, v31

    move-object/from16 v27, v5

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-array v9, v4, [Ljava/lang/Object;

    aput-object p3, v9, v6

    invoke-static {v5, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v8, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3, v0}, Lms/bd/c/j$a;->a(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    if-eqz p5, :cond_4

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "79978a"

    :try_start_f
    new-array v0, v13, [B

    const/16 v5, 0x16

    aput-byte v5, v0, v6

    aput-byte v28, v0, v4

    const/16 v5, 0x79

    aput-byte v5, v0, v15

    const/16 v5, 0x77

    aput-byte v5, v0, v14

    move-object/from16 v27, v0

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    new-instance v5, Ljava/io/DataOutputStream;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    move-object/from16 v0, p2

    :try_start_10
    invoke-virtual {v5, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "3b0f90"

    const/16 v8, 0xa

    :try_start_11
    new-array v9, v8, [B

    const/16 v8, 0x3a

    aput-byte v8, v9, v6

    const/16 v8, 0x2d

    aput-byte v8, v9, v4

    const/16 v8, 0x57

    aput-byte v8, v9, v15

    aput-byte v12, v9, v14

    const/16 v8, 0x4b

    aput-byte v8, v9, v13

    const/16 v8, 0x2b

    aput-byte v8, v9, v10

    const/16 v8, 0x3f

    aput-byte v8, v9, v12

    const/16 v8, 0x44

    const/4 v11, 0x7

    aput-byte v8, v9, v11

    const/16 v8, 0x68

    const/16 v11, 0x8

    aput-byte v8, v9, v11

    const/16 v8, 0x32

    const/16 v11, 0x9

    aput-byte v8, v9, v11

    move-object/from16 v27, v9

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8, v0}, Lms/bd/c/j$a;->a(Ljava/lang/String;I)V

    const/16 v8, 0xc8

    if-ne v0, v8, :cond_8

    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    if-eqz v2, :cond_5

    :try_start_12
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :goto_2
    const/16 v2, 0x100

    new-array v2, v2, [B

    :goto_3
    invoke-virtual {v8, v2}, Ljava/io/InputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_6

    invoke-virtual {v9, v2, v6, v11}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_3

    :cond_6
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V

    instance-of v2, v9, Ljava/io/ByteArrayOutputStream;

    if-eqz v2, :cond_7

    move-object v2, v9

    check-cast v2, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    move-object/from16 v29, v2

    goto :goto_5

    :cond_8
    const/4 v8, 0x0

    const/16 v29, 0x0

    :goto_5
    invoke-virtual {v1, v8}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/InputStream;)V

    invoke-virtual {v1, v5}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/OutputStream;)V

    :try_start_13
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0

    goto :goto_6

    :catch_0
    const/4 v2, 0x7

    new-array v12, v2, [B

    fill-array-data v12, :array_0

    const v7, 0x1000001

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-string v11, "83bd8b"

    invoke-static/range {v7 .. v12}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :goto_6
    move-object/from16 v8, v29

    goto/16 :goto_c

    :catch_1
    move/from16 v17, v0

    goto :goto_7

    :catchall_1
    move-exception v0

    move-object v8, v5

    const/4 v2, 0x0

    goto/16 :goto_d

    :catch_2
    :goto_7
    move/from16 v0, v17

    goto :goto_9

    :catchall_2
    move-exception v0

    goto :goto_8

    :catchall_3
    move-exception v0

    const/4 v7, 0x0

    :goto_8
    const/4 v2, 0x0

    const/4 v8, 0x0

    goto/16 :goto_d

    :catch_3
    const/4 v7, 0x0

    :catch_4
    const/4 v0, -0x1

    const/4 v5, 0x0

    :goto_9
    const/4 v8, 0x0

    :catch_5
    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "2852b4"

    :try_start_14
    new-array v2, v12, [B

    const/16 v9, 0x30

    aput-byte v9, v2, v6

    const/16 v9, 0x32

    aput-byte v9, v2, v4

    const/16 v11, 0x45

    aput-byte v11, v2, v15

    const/16 v11, 0x79

    aput-byte v11, v2, v14

    const/16 v11, 0x55

    aput-byte v11, v2, v13

    aput-byte v9, v2, v10

    move-object/from16 v27, v2

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    if-eqz v7, :cond_9

    const v22, 0x1000001

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v26, "9f7bad"

    const/16 v2, 0xa

    :try_start_15
    new-array v2, v2, [B

    const/16 v9, 0x30

    aput-byte v9, v2, v6

    const/16 v9, 0x29

    aput-byte v9, v2, v4

    const/16 v9, 0x50

    aput-byte v9, v2, v15

    aput-byte v15, v2, v14

    const/16 v9, 0x13

    aput-byte v9, v2, v13

    const/16 v9, 0x7f

    aput-byte v9, v2, v10

    const/16 v9, 0x35

    aput-byte v9, v2, v12

    const/16 v9, 0x40

    const/4 v10, 0x7

    aput-byte v9, v2, v10

    const/16 v9, 0x6f

    const/16 v10, 0x8

    aput-byte v9, v2, v10

    const/16 v9, 0x36

    const/16 v10, 0x9

    aput-byte v9, v2, v10

    move-object/from16 v27, v2

    invoke-static/range {v22 .. v27}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_a

    :cond_9
    const/4 v2, 0x0

    :goto_a
    invoke-virtual {v3, v2, v0}, Lms/bd/c/j$a;->b(Ljava/lang/String;I)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    invoke-virtual {v1, v8}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/InputStream;)V

    invoke-virtual {v1, v5}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/OutputStream;)V

    if-eqz v7, :cond_a

    :try_start_16
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_6

    goto :goto_b

    :catch_6
    const/4 v2, 0x7

    new-array v12, v2, [B

    fill-array-data v12, :array_1

    const v7, 0x1000001

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-string v11, "fdbbb4"

    invoke-static/range {v7 .. v12}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :cond_a
    :goto_b
    const/4 v8, 0x0

    :goto_c
    new-array v2, v15, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    aput-object v8, v2, v4

    return-object v2

    :catchall_4
    move-exception v0

    move-object v2, v8

    move-object v8, v5

    :goto_d
    invoke-virtual {v1, v2}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/InputStream;)V

    invoke-virtual {v1, v8}, Lcom/bytedance/mobsec/matrix/net/a;->a(Ljava/io/OutputStream;)V

    if-eqz v7, :cond_b

    :try_start_17
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_7

    goto :goto_e

    :catch_7
    const/4 v2, 0x7

    new-array v8, v2, [B

    fill-array-data v8, :array_2

    const v3, 0x1000001

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-string v7, "df3b37"

    invoke-static/range {v3 .. v8}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :cond_b
    :goto_e
    throw v0

    :array_0
    .array-data 1
        0x3at
        0x39t
        0x12t
        0x2ft
        0xft
        0x64t
        0x69t
    .end array-data

    :array_1
    .array-data 1
        0x64t
        0x6et
        0x12t
        0x29t
        0x55t
        0x32t
        0x37t
    .end array-data

    :array_2
    .array-data 1
        0x66t
        0x6ct
        0x43t
        0x29t
        0x4t
        0x31t
        0x35t
    .end array-data
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/mobsec/matrix/net/b;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;)[Ljava/lang/Object;
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/mobsec/matrix/net/b;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 6

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/mobsec/matrix/net/b;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Z)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
