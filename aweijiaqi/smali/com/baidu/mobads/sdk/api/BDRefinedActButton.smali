.class public Lcom/baidu/mobads/sdk/api/BDRefinedActButton;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private mAdView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mLoader:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 21
    invoke-direct {p0, p1, v0, v1}, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 4

    .line 43
    :try_start_0
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Class;

    .line 44
    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    .line 46
    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/ay;->a(Landroid/content/Context;)Ljava/lang/ClassLoader;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    .line 47
    sget-object p1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    .line 48
    invoke-static {p1, v2, v1, v0}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 51
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 55
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public setAdData(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 7

    .line 34
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 35
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setAdData"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setButtonBackgroundColor(I)V
    .locals 7

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 97
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 99
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setButtonBackgroundColor"

    move-object v6, v0

    .line 97
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setButtonFontSizeSp(I)V
    .locals 7

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 75
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 77
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setButtonFontSizeSp"

    move-object v6, v0

    .line 75
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setButtonFontTypeFace(Landroid/graphics/Typeface;)V
    .locals 7

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 64
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/Typeface;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setButtonFontTypeFace"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setButtonTextColor(I)V
    .locals 7

    .line 85
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 86
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDRefinedActButton;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 88
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setButtonTextColor"

    move-object v6, v0

    .line 86
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
