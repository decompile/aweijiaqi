.class public Lcom/baidu/mobads/sdk/api/XAdNativeResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/sdk/api/NativeResponse;


# instance fields
.field private isDownloadApp:Z

.field private mAdDislikeListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdDislikeListener;

.field private mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

.field private mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

.field private mAdPrivacyListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;

.field private mCxt:Landroid/content/Context;

.field private mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

.field private mUriUtils:Lcom/baidu/mobads/sdk/internal/bw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/cc;Lcom/baidu/mobads/sdk/internal/a;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    .line 34
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mCxt:Landroid/content/Context;

    .line 35
    iput-object p3, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    .line 36
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    .line 37
    invoke-virtual {p3}, Lcom/baidu/mobads/sdk/internal/a;->p()I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    .line 38
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    .line 40
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bw;->a()Lcom/baidu/mobads/sdk/internal/bw;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mUriUtils:Lcom/baidu/mobads/sdk/internal/bw;

    return-void
.end method

.method private getActionType()I
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->p()I

    move-result v0

    return v0
.end method

.method private getAdInterListener()Lcom/baidu/mobads/sdk/api/IAdInterListener;
    .locals 1

    .line 607
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, v0, Lcom/baidu/mobads/sdk/internal/cc;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public clearImpressionTaskWhenBack()V
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cc;->j()V

    :cond_0
    return-void
.end method

.method public getActButtonString()Ljava/lang/String;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->K()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAdDislikeListener()Lcom/baidu/mobads/sdk/api/NativeResponse$AdDislikeListener;
    .locals 1

    .line 577
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdDislikeListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdDislikeListener;

    return-object v0
.end method

.method public getAdLogoUrl()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/mob-adIcon_2x.png"

    return-object v0
.end method

.method public getAdMaterialType()Ljava/lang/String;
    .locals 2

    .line 394
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-nez v0, :cond_0

    .line 395
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 397
    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->w()Ljava/lang/String;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->w()Ljava/lang/String;

    move-result-object v0

    const-string v1, "html"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 402
    :cond_2
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPackage()Ljava/lang/String;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAppPermissionLink()Ljava/lang/String;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->C()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAppPrivacyLink()Ljava/lang/String;
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAppSize()J
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->j()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getBaiduLogoUrl()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/new-bg-logo.png"

    return-object v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .line 478
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 479
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getContainerHeight()I
    .locals 1

    .line 428
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 429
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->s()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getContainerSizeType()I
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->t()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getContainerWidth()I
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->r()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getDownloadStatus()I
    .locals 3

    .line 137
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mCxt:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/ae;->a(Landroid/content/Context;)Lcom/baidu/mobads/sdk/internal/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mCxt:Landroid/content/Context;

    .line 139
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/sdk/internal/ae;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public getDuration()I
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->v()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getECPMLevel()Ljava/lang/String;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 446
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getExtraParams()Lorg/json/JSONObject;
    .locals 1

    .line 494
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->G()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtras()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 195
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, v1, Lcom/baidu/mobads/sdk/internal/cc;->n:Ljava/lang/String;

    const-string v2, "appsid"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public getHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 378
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->d()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    const-string v0, ""

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getMainPicHeight()I
    .locals 1

    .line 470
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->f()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getMainPicWidth()I
    .locals 1

    .line 462
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->e()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getMarketingDesc()Ljava/lang/String;
    .locals 1

    .line 600
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 601
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getMarketingICONUrl()Ljava/lang/String;
    .locals 1

    .line 590
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 591
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getMarketingPendant()Ljava/lang/String;
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 332
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->J()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getMaterialType()Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-nez v0, :cond_0

    .line 364
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    return-object v0

    .line 366
    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->w()Ljava/lang/String;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    return-object v0

    .line 368
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->w()Ljava/lang/String;

    move-result-object v0

    const-string v1, "html"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 369
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    return-object v0

    .line 371
    :cond_2
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    return-object v0
.end method

.method public getMultiPicUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->E()Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->A()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getStyleType()I
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->u()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getThirdTrackers(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 615
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 619
    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 621
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v2}, Lcom/baidu/mobads/sdk/internal/a;->L()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 623
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 624
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 626
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 627
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 628
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 629
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    return-object v0

    :catchall_0
    move-exception p1

    .line 636
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v1
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 517
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 347
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    .line 386
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cc;->q()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleClick(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    .line 263
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->handleClick(Landroid/view/View;I)V

    return-void
.end method

.method public handleClick(Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x0

    .line 268
    invoke-virtual {p0, p1, p2, v0}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->handleClick(Landroid/view/View;IZ)V

    return-void
.end method

.method public handleClick(Landroid/view/View;IZ)V
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v0

    :try_start_0
    const-string v1, "progress"

    .line 282
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "use_dialog_frame"

    .line 283
    invoke-virtual {v0, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string p2, "isDownloadApp"

    .line 284
    iget-boolean p3, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    invoke-virtual {v0, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    :catchall_0
    iget-object p2, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {p2, p1, v0}, Lcom/baidu/mobads/sdk/internal/cc;->b(Landroid/view/View;Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public handleClick(Landroid/view/View;Z)V
    .locals 1

    const/4 v0, -0x1

    .line 273
    invoke-virtual {p0, p1, v0, p2}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->handleClick(Landroid/view/View;IZ)V

    return-void
.end method

.method public isAdAvailable(Landroid/content/Context;)Z
    .locals 5

    .line 146
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 150
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/a;->x()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/a;->D()J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-gtz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public isAutoPlay()Z
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->k()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isNeedDownloadApp()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    return v0
.end method

.method public isNonWifiAutoPlay()Z
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->l()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public onADExposed()V
    .locals 1

    .line 529
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 530
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;->onADExposed()V

    :cond_0
    return-void
.end method

.method public onADExposureFailed(I)V
    .locals 1

    .line 535
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 536
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;->onADExposureFailed(I)V

    :cond_0
    return-void
.end method

.method public onADPermissionShow(Z)V
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdPrivacyListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 549
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;->onADPermissionShow()V

    goto :goto_0

    .line 551
    :cond_0
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;->onADPermissionClose()V

    :cond_1
    :goto_0
    return-void
.end method

.method public onADPrivacyClick()V
    .locals 1

    .line 567
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdPrivacyListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;

    if-eqz v0, :cond_0

    .line 568
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;->onADPrivacyClick()V

    :cond_0
    return-void
.end method

.method public onADStatusChanged()V
    .locals 1

    .line 541
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 542
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;->onADStatusChanged()V

    :cond_0
    return-void
.end method

.method public onAdClick()V
    .locals 1

    .line 523
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 524
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method public onAdDownloadWindow(Z)V
    .locals 2

    .line 556
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdPrivacyListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;

    if-eqz v0, :cond_1

    .line 557
    instance-of v1, v0, Lcom/baidu/mobads/sdk/api/NativeResponse$AdDownloadWindowListener;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    .line 559
    check-cast v0, Lcom/baidu/mobads/sdk/api/NativeResponse$AdDownloadWindowListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdDownloadWindowListener;->adDownloadWindowShow()V

    goto :goto_0

    .line 561
    :cond_0
    check-cast v0, Lcom/baidu/mobads/sdk/api/NativeResponse$AdDownloadWindowListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdDownloadWindowListener;->adDownloadWindowClose()V

    :cond_1
    :goto_0
    return-void
.end method

.method public onAdUnionClick()V
    .locals 1

    .line 581
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 582
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;->onAdUnionClick()V

    :cond_0
    return-void
.end method

.method public pauseAppDownload()V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mCxt:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v0

    :try_start_0
    const-string v1, "pk"

    .line 107
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "msg"

    const-string v2, "pauseDownload"

    .line 108
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :catch_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mCxt:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/sdk/internal/ae;->a(Landroid/content/Context;)Lcom/baidu/mobads/sdk/internal/ae;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/sdk/internal/ae;->a(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public permissionClick()V
    .locals 3

    .line 211
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v1, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->C()Ljava/lang/String;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v1

    :try_start_0
    const-string v2, "permissionUrl"

    .line 215
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "msg"

    const-string v2, "permissionClick"

    .line 216
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :catch_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public preloadVideoMaterial()V
    .locals 3

    .line 504
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v0

    :try_start_0
    const-string v1, "msg"

    const-string v2, "preloadVideoMaterial"

    .line 507
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    :catch_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public privacyClick()V
    .locals 3

    .line 226
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v1, :cond_0

    .line 227
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->B()Ljava/lang/String;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v1

    :try_start_0
    const-string v2, "privacy_link"

    .line 230
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "msg"

    const-string v2, "privacyClick"

    .line 231
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :catch_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public recordImpression(Landroid/view/View;)V
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v1, :cond_0

    .line 257
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/baidu/mobads/sdk/internal/cc;->a(Landroid/view/View;Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public registerViewForInteraction(Landroid/view/View;Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;)V
    .locals 0

    .line 452
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->recordImpression(Landroid/view/View;)V

    .line 453
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInteractionListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdInteractionListener;

    return-void
.end method

.method public resumeAppDownload()V
    .locals 3

    .line 119
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v0

    :try_start_0
    const-string v1, "msg"

    const-string v2, "resumeDownload"

    .line 123
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :catch_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public setAdDislikeListener(Lcom/baidu/mobads/sdk/api/NativeResponse$AdDislikeListener;)V
    .locals 0

    .line 573
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdDislikeListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdDislikeListener;

    return-void
.end method

.method public setAdPrivacyListener(Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;)V
    .locals 0

    .line 457
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdPrivacyListener:Lcom/baidu/mobads/sdk/api/NativeResponse$AdPrivacyListener;

    return-void
.end method

.method public setIsDownloadApp(Z)V
    .locals 0

    .line 99
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->isDownloadApp:Z

    return-void
.end method

.method public unionLogoClick()V
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mUriUtils:Lcom/baidu/mobads/sdk/internal/bw;

    if-eqz v0, :cond_0

    const-string v1, "http://union.baidu.com/"

    .line 242
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/bw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mAdInstanceInfo:Lcom/baidu/mobads/sdk/internal/a;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v1

    :try_start_0
    const-string v2, "unionUrl"

    .line 245
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "msg"

    const-string v2, "unionLogoClick"

    .line 246
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :catchall_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->mFeedsProd:Lcom/baidu/mobads/sdk/internal/cc;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method
