.class public Lcom/baidu/mobads/sdk/api/RouteInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private destination:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private path:Ljava/lang/String;

.field private rawType:Ljavax/lang/model/element/Element;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->destination:Ljava/lang/Class;

    .line 30
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->path:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljavax/lang/model/element/Element;Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->rawType:Ljavax/lang/model/element/Element;

    .line 41
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->path:Ljava/lang/String;

    return-void
.end method

.method public static build(Ljava/lang/Class;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RouteInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/baidu/mobads/sdk/api/RouteInfo;"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/baidu/mobads/sdk/api/RouteInfo;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobads/sdk/api/RouteInfo;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getDestination()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->destination:Ljava/lang/Class;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getRawType()Ljavax/lang/model/element/Element;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->rawType:Ljavax/lang/model/element/Element;

    return-object v0
.end method

.method public setDestination(Ljava/lang/Class;)Lcom/baidu/mobads/sdk/api/RouteInfo;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/baidu/mobads/sdk/api/RouteInfo;"
        }
    .end annotation

    .line 49
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->destination:Ljava/lang/Class;

    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RouteInfo;
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/RouteInfo;->path:Ljava/lang/String;

    return-object p0
.end method
