.class public Lcom/baidu/mobads/sdk/api/VideoViewManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/VideoViewManager$Inner;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "VideoViewManager"


# instance fields
.field private mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

.field private mVideoViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/sdk/api/CpuVideoView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/baidu/mobads/sdk/api/VideoViewManager$1;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/api/VideoViewManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobads/sdk/api/VideoViewManager;
    .locals 1

    .line 22
    invoke-static {}, Lcom/baidu/mobads/sdk/api/VideoViewManager$Inner;->access$000()Lcom/baidu/mobads/sdk/api/VideoViewManager;

    move-result-object v0

    return-object v0
.end method

.method private release()V
    .locals 1

    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    return-void
.end method


# virtual methods
.method public isExistedOtherPlay()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    if-nez v0, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public manageItem(Lcom/baidu/mobads/sdk/api/CpuVideoView;)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/CpuVideoView;

    if-eq v1, p1, :cond_1

    .line 68
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->pause()V

    goto :goto_0

    .line 71
    :cond_2
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 72
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->play()V

    return-void
.end method

.method public onWindowFocusChanged(Lcom/baidu/mobads/sdk/api/CpuVideoView;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 53
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mCurVideoView:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    if-ne p1, v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->play()V

    goto :goto_1

    :cond_0
    if-nez p2, :cond_1

    .line 56
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 57
    invoke-virtual {p2}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->pause()V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public removeItem(Lcom/baidu/mobads/sdk/api/CpuVideoView;)V
    .locals 1

    .line 37
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->removeAllViews()V

    .line 38
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 40
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/VideoViewManager;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 41
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/api/VideoViewManager;->release()V

    :cond_0
    return-void
.end method
