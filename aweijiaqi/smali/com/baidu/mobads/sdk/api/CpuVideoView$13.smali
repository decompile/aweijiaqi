.class Lcom/baidu/mobads/sdk/api/CpuVideoView$13;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/sdk/api/CpuVideoView;)V
    .locals 0

    .line 1018
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1021
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 1022
    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1400(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Lcom/baidu/mobads/sdk/api/IBasicCPUData;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IBasicCPUData;->isAutoplay()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 1023
    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1500(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 1024
    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$200(Lcom/baidu/mobads/sdk/api/CpuVideoView;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1025
    invoke-static {}, Lcom/baidu/mobads/sdk/api/VideoViewManager;->getInstance()Lcom/baidu/mobads/sdk/api/VideoViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/VideoViewManager;->isExistedOtherPlay()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    .line 1026
    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1600(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1027
    invoke-static {}, Lcom/baidu/mobads/sdk/api/VideoViewManager;->getInstance()Lcom/baidu/mobads/sdk/api/VideoViewManager;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/api/VideoViewManager;->manageItem(Lcom/baidu/mobads/sdk/api/CpuVideoView;)V

    goto :goto_0

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1600(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$200(Lcom/baidu/mobads/sdk/api/CpuVideoView;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1030
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->pause()V

    .line 1033
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$13;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, p0, v1, v2}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
