.class public Lcom/baidu/mobads/sdk/api/BdShellActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;
    }
.end annotation


# static fields
.field private static canShowWhenLock:Z

.field private static mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;


# instance fields
.field private mAdLogger:Lcom/baidu/mobads/sdk/internal/az;

.field private mLoader:Ljava/lang/ClassLoader;

.field private mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 132
    sget-object v0, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;->ACTION_BAR_WHITE_THEME:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    sput-object v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mAdLogger:Lcom/baidu/mobads/sdk/internal/az;

    return-void
.end method

.method public static canLpShowWhenLocked(Z)V
    .locals 0

    .line 155
    sput-boolean p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->canShowWhenLock:Z

    return-void
.end method

.method public static getActionBarColorTheme()Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;
    .locals 1

    .line 136
    sget-object v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    return-object v0
.end method

.method public static getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 34
    const-class v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;

    return-object v0
.end method

.method public static getLpShowWhenLocked()Z
    .locals 1

    .line 159
    sget-boolean v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->canShowWhenLock:Z

    return v0
.end method

.method public static setActionBarColor(IIII)V
    .locals 1

    .line 147
    new-instance v0, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    return-void
.end method

.method public static setActionBarColorTheme(Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 141
    new-instance v0, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;-><init>(Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 165
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 170
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 176
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 181
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 186
    invoke-interface {v0, p1, p2, p3}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onActivityResult(IILandroid/content/Intent;)V

    .line 188
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .line 359
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 360
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 361
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onAttachedToWindow()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 375
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 376
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 377
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onBackPressed()Z

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 193
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 195
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 202
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/BdShellActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 205
    :try_start_0
    invoke-static {p0}, Lcom/baidu/mobads/sdk/internal/ay;->a(Landroid/content/Context;)Ljava/lang/ClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mLoader:Ljava/lang/ClassLoader;

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    const-string v1, ""

    if-eqz v0, :cond_1

    const-string v1, "activityImplName"

    .line 212
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "theme"

    .line 213
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Dialog"

    .line 214
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    sget v2, Lcom/baidu/mobads/proxy/R$style;->bd_activity_dialog_theme:I

    invoke-virtual {p0, v2}, Lcom/baidu/mobads/sdk/api/BdShellActivity;->setTheme(I)V

    :cond_1
    const/4 v2, 0x0

    .line 220
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 221
    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mLoader:Ljava/lang/ClassLoader;

    invoke-static {v1, v3}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    :try_start_1
    new-array v4, v3, [Ljava/lang/Class;

    .line 223
    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Object;

    .line 224
    invoke-virtual {v1, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 226
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v3

    invoke-virtual {v4, v5}, Lcom/baidu/mobads/sdk/internal/az;->d([Ljava/lang/Object;)I

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 230
    check-cast v2, Lcom/baidu/mobads/sdk/api/IActivityImpl;

    iput-object v2, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    .line 232
    :cond_3
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v1, :cond_4

    .line 233
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    const-string v2, "bar_close_color"

    .line 235
    sget-object v3, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    iget v3, v3, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;->closeColor:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "bar_pro_color"

    .line 236
    sget-object v3, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    iget v3, v3, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;->progressColor:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "bar_title_color"

    .line 237
    sget-object v3, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    iget v3, v3, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;->titleColor:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "bar_bg_color"

    .line 238
    sget-object v3, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mSActionBarColorTheme:Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;

    iget v3, v3, Lcom/baidu/mobads/sdk/api/BdShellActivity$ActionBarColorTheme;->backgroundColor:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_0
    move-exception v2

    .line 240
    :try_start_4
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 242
    :goto_1
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->setLpBussParam(Lorg/json/JSONObject;)V

    .line 243
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    invoke-interface {v1, p0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->setActivity(Landroid/app/Activity;)V

    if-eqz v0, :cond_4

    .line 245
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onCreate(Landroid/os/Bundle;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 249
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/az;->c(Ljava/lang/Throwable;)I

    :cond_4
    :goto_2
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 255
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onDestroy()V

    .line 257
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 367
    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    .line 368
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 369
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onDetachedFromWindow()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 263
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 268
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 274
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 279
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 284
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onNewIntent(Landroid/content/Intent;)V

    .line 286
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 291
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onPause()V

    .line 293
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 297
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 298
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 299
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 304
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 305
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 306
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onResume()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 311
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 313
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 318
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 319
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 320
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onStart()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 325
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 326
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 327
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onStop()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 334
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    .line 339
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 343
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 344
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 345
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public overridePendingTransition(II)V
    .locals 1

    .line 351
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 352
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BdShellActivity;->mProxyActivity:Lcom/baidu/mobads/sdk/api/IActivityImpl;

    if-eqz v0, :cond_0

    .line 353
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IActivityImpl;->overridePendingTransition(II)V

    :cond_0
    return-void
.end method
