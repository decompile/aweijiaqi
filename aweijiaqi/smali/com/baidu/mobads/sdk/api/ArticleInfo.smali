.class public Lcom/baidu/mobads/sdk/api/ArticleInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/ArticleInfo$ValidSexValue;
    }
.end annotation


# static fields
.field public static final CONTENT_CATEGORY:Ljava/lang/String; = "page_content_category"

.field public static final CONTENT_LABEL:Ljava/lang/String; = "page_content_label"

.field public static final FAVORITE_BOOK:Ljava/lang/String; = "fav_book"

.field public static final PAGE_ID:Ljava/lang/String; = "page_content_id"

.field public static final PAGE_TITLE:Ljava/lang/String; = "page_title"

.field public static final PREDEFINED_KEYS:[Ljava/lang/String;

.field public static final QUERY_WORD:Ljava/lang/String; = "qw"

.field public static final USER_SEX:Ljava/lang/String; = "sex"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "sex"

    const-string v1, "fav_book"

    const-string v2, "page_title"

    const-string v3, "page_content_id"

    const-string v4, "page_content_category"

    const-string v5, "page_content_label"

    const-string v6, "qw"

    .line 13
    filled-new-array/range {v0 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/sdk/api/ArticleInfo;->PREDEFINED_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
