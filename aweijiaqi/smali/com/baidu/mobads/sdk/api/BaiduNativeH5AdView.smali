.class public Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;
    }
.end annotation


# instance fields
.field private mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

.field private mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

.field private mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

.field private mRequestParameters:Lcom/baidu/mobads/sdk/api/RequestParameters;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->initView(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 19
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    const/4 p2, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->initView(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 19
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    const/4 p2, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->initView(Landroid/content/Context;I)V

    return-void
.end method

.method private cancel()V
    .locals 0

    return-void
.end method

.method private initView(Landroid/content/Context;I)V
    .locals 0

    if-eqz p2, :cond_0

    .line 28
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method

.method private onDetach()V
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->cancel()V

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cb;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getAdPlacement()Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    return-object v0
.end method

.method public isAdDataLoaded()Z
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cb;->r()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public makeRequest(Lcom/baidu/mobads/sdk/api/RequestParameters;)V
    .locals 3

    if-nez p1, :cond_0

    .line 59
    new-instance p1, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    invoke-direct {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->build()Lcom/baidu/mobads/sdk/api/RequestParameters;

    move-result-object p1

    .line 62
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mRequestParameters:Lcom/baidu/mobads/sdk/api/RequestParameters;

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    if-eqz v0, :cond_1

    .line 64
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->onDetach()V

    .line 67
    :cond_1
    new-instance v0, Lcom/baidu/mobads/sdk/internal/cb;

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "feed"

    invoke-direct {v0, v1, v2, p0}, Lcom/baidu/mobads/sdk/internal/cb;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    .line 68
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/cb;->a(Lcom/baidu/mobads/sdk/api/RequestParameters;)V

    .line 69
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/cb;->a(Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;)V

    .line 70
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getSessionId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/cb;->a(I)V

    .line 71
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getPosistionId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/cb;->c(I)V

    .line 72
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getSequenceId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/cb;->d(I)V

    .line 73
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz p1, :cond_2

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/cb;->a(Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;)V

    .line 76
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    if-eqz p1, :cond_5

    .line 77
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->hasValidResponse()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 78
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/cb;->e()Z

    move-result p1

    if-eqz p1, :cond_5

    return-void

    .line 82
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/cb;->b(Z)V

    .line 83
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getRequestStarted()Z

    move-result p1

    if-eqz p1, :cond_4

    return-void

    .line 86
    :cond_4
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 90
    :cond_5
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/cb;->a_()V

    return-void
.end method

.method public recordImpression()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getResponse()Lcom/baidu/mobads/sdk/internal/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    .line 96
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->isWinSended()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/cb;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getResponse()Lcom/baidu/mobads/sdk/internal/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/a;->M()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/baidu/mobads/sdk/internal/cb;->a(Landroid/view/View;Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public setAdPlacement(Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    return-void
.end method

.method public setAdPlacementData(Ljava/lang/Object;)V
    .locals 5

    .line 112
    new-instance v0, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    invoke-direct {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;-><init>()V

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    new-array v3, v1, [Ljava/lang/Object;

    const-string v4, "getApId"

    .line 113
    invoke-static {p1, v4, v2, v3}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setApId(Ljava/lang/String;)V

    new-array v2, v1, [Ljava/lang/Class;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "getAppSid"

    .line 114
    invoke-static {p1, v3, v2, v1}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 116
    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdPlacement:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    return-void
.end method

.method public setEventListener(Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->mAdViewListener:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    return-void
.end method
