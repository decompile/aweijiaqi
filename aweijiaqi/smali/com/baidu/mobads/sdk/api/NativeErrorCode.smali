.class public final enum Lcom/baidu/mobads/sdk/api/NativeErrorCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/sdk/api/NativeErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/baidu/mobads/sdk/api/NativeErrorCode;

.field public static final enum CONFIG_ERROR:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

.field public static final enum LOAD_AD_FAILED:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

.field public static final enum UNKNOWN:Lcom/baidu/mobads/sdk/api/NativeErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 4
    new-instance v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/baidu/mobads/sdk/api/NativeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->UNKNOWN:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    new-instance v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    const/4 v2, 0x1

    const-string v3, "LOAD_AD_FAILED"

    invoke-direct {v0, v3, v2}, Lcom/baidu/mobads/sdk/api/NativeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->LOAD_AD_FAILED:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    new-instance v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    const/4 v3, 0x2

    const-string v4, "INTERNAL_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/baidu/mobads/sdk/api/NativeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->INTERNAL_ERROR:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    new-instance v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    const/4 v4, 0x3

    const-string v5, "CONFIG_ERROR"

    invoke-direct {v0, v5, v4}, Lcom/baidu/mobads/sdk/api/NativeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->CONFIG_ERROR:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    const/4 v5, 0x4

    new-array v5, v5, [Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    .line 3
    sget-object v6, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->UNKNOWN:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    aput-object v6, v5, v1

    sget-object v1, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->LOAD_AD_FAILED:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    aput-object v1, v5, v2

    sget-object v1, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->INTERNAL_ERROR:Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    aput-object v1, v5, v3

    aput-object v0, v5, v4

    sput-object v5, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->$VALUES:[Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/NativeErrorCode;
    .locals 1

    .line 3
    const-class v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/sdk/api/NativeErrorCode;
    .locals 1

    .line 3
    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeErrorCode;->$VALUES:[Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    invoke-virtual {v0}, [Lcom/baidu/mobads/sdk/api/NativeErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/sdk/api/NativeErrorCode;

    return-object v0
.end method
