.class Lcom/baidu/mobads/sdk/api/CpuVideoView$12;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/sdk/api/CpuVideoView;)V
    .locals 0

    .line 993
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 996
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$800(Lcom/baidu/mobads/sdk/api/CpuVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v2, v2, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$802(Lcom/baidu/mobads/sdk/api/CpuVideoView;I)I

    .line 997
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v0, v0, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    .line 998
    rem-int/lit8 v1, v0, 0x3c

    .line 999
    div-int/lit8 v0, v0, 0x3c

    .line 1000
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v4, v1

    const-string v0, "%02d:%02d"

    invoke-static {v2, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1001
    iget-object v4, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v4}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1100(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1003
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v2, v2, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->getDuration()I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    .line 1004
    rem-int/lit8 v4, v2, 0x3c

    .line 1005
    div-int/lit8 v2, v2, 0x3c

    .line 1006
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v6, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1008
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v1}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1200(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1010
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1300(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v1, v1, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1011
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->access$1300(Lcom/baidu/mobads/sdk/api/CpuVideoView;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    iget-object v1, v1, Lcom/baidu/mobads/sdk/api/CpuVideoView;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1012
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/CpuVideoView$12;->this$0:Lcom/baidu/mobads/sdk/api/CpuVideoView;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, p0, v1, v2}, Lcom/baidu/mobads/sdk/api/CpuVideoView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
