.class public Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final PERMISSION_APP_LIST:Ljava/lang/String; = "permission_app_list"

.field private static final PERMISSION_LOCATION:Ljava/lang/String; = "permission_location"

.field private static final PERMISSION_PHONE_STATE:Ljava/lang/String; = "permission_read_phone_state"

.field private static final PERMISSION_STORAGE:Ljava/lang/String; = "permission_storage"

.field private static mAccessAppListGranted:Z = true

.field private static mAccessLocationGranted:Z = false

.field private static mExternalStorageGranted:Z = false

.field private static mReadPhoneStateGranted:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPermissionInfo()Lorg/json/JSONObject;
    .locals 5

    const-string v0, ""

    .line 65
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "permission_location"

    .line 67
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v4, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mAccessLocationGranted:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "permission_storage"

    .line 68
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v4, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mExternalStorageGranted:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "permission_app_list"

    .line 69
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v4, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mAccessAppListGranted:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "permission_read_phone_state"

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v0, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mReadPhoneStateGranted:Z

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 72
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    return-object v1
.end method

.method public static postPermissionInfoRemote()V
    .locals 3

    .line 78
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/s;->a()Lcom/baidu/mobads/sdk/internal/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/s;->c()Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->getPermissionInfo()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "permission_module"

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;->onTaskDistribute(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_0
    return-void
.end method

.method public static setPermissionAppList(Z)V
    .locals 0

    .line 60
    sput-boolean p0, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mAccessAppListGranted:Z

    .line 61
    invoke-static {}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->postPermissionInfoRemote()V

    return-void
.end method

.method public static setPermissionLocation(Z)V
    .locals 0

    .line 40
    sput-boolean p0, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mAccessLocationGranted:Z

    .line 41
    invoke-static {}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->postPermissionInfoRemote()V

    return-void
.end method

.method public static setPermissionReadDeviceID(Z)V
    .locals 0

    .line 30
    sput-boolean p0, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mReadPhoneStateGranted:Z

    .line 31
    invoke-static {}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->postPermissionInfoRemote()V

    return-void
.end method

.method public static setPermissionStorage(Z)V
    .locals 0

    .line 50
    sput-boolean p0, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->mExternalStorageGranted:Z

    .line 51
    invoke-static {}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->postPermissionInfoRemote()V

    return-void
.end method
