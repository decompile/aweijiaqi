.class public Lcom/baidu/mobads/sdk/api/StyleParams;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/StyleParams$Builder;
    }
.end annotation


# instance fields
.field public mBrandBottomDp:I

.field public mBrandFontColor:I

.field public mBrandFontSizeSp:I

.field public mBrandFontTypeFace:Landroid/graphics/Typeface;

.field public mBrandLeftDp:I

.field public mBrandRightDp:I

.field public mButtonBackgroundColor:I

.field public mButtonBottomDp:I

.field public mButtonFontColor:I

.field public mButtonFontSizeSp:I

.field public mButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mButtonForegroundColor:I

.field public mButtonHeightDp:I

.field public mButtonLeftDp:I

.field public mButtonRightDp:I

.field public mButtonTopDp:I

.field public mButtonWidthDp:I

.field public mDislikeBottomDp:I

.field public mDislikeLeftDp:I

.field public mDislikeRightDp:I

.field public mDislikeTopDp:I

.field public mFirstPicBottomDp:I

.field public mFirstPicHeightDp:I

.field public mFirstPicLeftDp:I

.field public mFirstPicRightDp:I

.field public mFirstPicTopDp:I

.field public mFirstPicWidthDp:I

.field public mIconBottomDp:I

.field public mIconHeightDp:I

.field public mIconLeftDp:I

.field public mIconRightDp:I

.field public mIconTopDp:I

.field public mIconWidthDp:I

.field public mImageBackground:Landroid/graphics/drawable/Drawable;

.field public mImageBackgroundColor:I

.field public mIsShowActionButton:Z

.field public mIsShowDownloadInfo:Z

.field public mSmartDownloadAppNameBottomDp:I

.field public mSmartDownloadAppNameLeftDp:I

.field public mSmartDownloadAppNameRightDp:I

.field public mSmartDownloadAppNameTextColor:I

.field public mSmartDownloadAppNameTextSizeSp:I

.field public mSmartDownloadAppNameTopDp:I

.field public mSmartDownloadButtonBackgroundColor:I

.field public mSmartDownloadButtonBottomDp:I

.field public mSmartDownloadButtonFontColor:I

.field public mSmartDownloadButtonFontSizeSp:I

.field public mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mSmartDownloadButtonForegroundColor:I

.field public mSmartDownloadButtonHeightDp:I

.field public mSmartDownloadButtonLeftDp:I

.field public mSmartDownloadButtonRightDp:I

.field public mSmartDownloadButtonTopDp:I

.field public mSmartDownloadButtonWidthDp:I

.field public mSmartDownloadCompanyBottomDp:I

.field public mSmartDownloadCompanyLeftDp:I

.field public mSmartDownloadCompanyRightDp:I

.field public mSmartDownloadCompanyTextColor:I

.field public mSmartDownloadCompanyTextSizeSp:I

.field public mSmartDownloadCompanyTopDp:I

.field public mSmartDownloadPermissionBottomDp:I

.field public mSmartDownloadPermissionLeftDp:I

.field public mSmartDownloadPermissionRightDp:I

.field public mSmartDownloadPermissionTextColor:I

.field public mSmartDownloadPermissionTextSizeSp:I

.field public mSmartDownloadPermissionTopDp:I

.field public mSmartDownloadPrivacyBottomDp:I

.field public mSmartDownloadPrivacyLeftDp:I

.field public mSmartDownloadPrivacyRightDp:I

.field public mSmartDownloadPrivacyTextColor:I

.field public mSmartDownloadPrivacyTextSizeSp:I

.field public mSmartDownloadPrivacyTopDp:I

.field public mSmartDownloadVersionBottomDp:I

.field public mSmartDownloadVersionLeftDp:I

.field public mSmartDownloadVersionRightDp:I

.field public mSmartDownloadVersionTextColor:I

.field public mSmartDownloadVersionTextSizeSp:I

.field public mSmartDownloadVersionTopDp:I

.field public mSmartDownloadViewBackgroundColor:I

.field public mSmartDownloadViewBottomDp:I

.field public mSmartDownloadViewLeftDp:I

.field public mSmartDownloadViewRightDp:I

.field public mSmartDownloadViewTopDp:I

.field public mThreePicBottomDp:I

.field public mThreePicHeightDp:I

.field public mThreePicLeftDp:I

.field public mThreePicRightDp:I

.field public mThreePicTopDp:I

.field public mThreePicWidthDp:I

.field public mTitleBottomDp:I

.field public mTitleFontColor:I

.field public mTitleFontSizeSp:I

.field public mTitleFontTypeFace:Landroid/graphics/Typeface;

.field public mTitleLeftDp:I

.field public mTitleRightDp:I

.field public mTitleTopDp:I

.field public mTwoPicBottomDp:I

.field public mTwoPicHeightDp:I

.field public mTwoPicLeftDp:I

.field public mTwoPicRightDp:I

.field public mTwoPicTopDp:I

.field public mTwoPicWidthDp:I

.field public useDislike:Z


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/sdk/api/StyleParams$Builder;)V
    .locals 1

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 223
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->useDislike:Z

    const/4 v0, -0x1

    .line 225
    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeTopDp:I

    .line 227
    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeBottomDp:I

    .line 229
    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeLeftDp:I

    .line 231
    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeRightDp:I

    .line 235
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadViewTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewTopDp:I

    .line 236
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadViewBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewBottomDp:I

    .line 237
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadViewLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewLeftDp:I

    .line 238
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadViewRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewRightDp:I

    .line 240
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadViewBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewBackgroundColor:I

    .line 243
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTextSizeSp:I

    .line 244
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyTextColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTextColor:I

    .line 245
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTopDp:I

    .line 246
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyBottomDp:I

    .line 247
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyLeftDp:I

    .line 248
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadCompanyRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyRightDp:I

    .line 251
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTextSizeSp:I

    .line 252
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionTextColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTextColor:I

    .line 253
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTopDp:I

    .line 254
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionBottomDp:I

    .line 255
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionLeftDp:I

    .line 256
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadVersionRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionRightDp:I

    .line 259
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTextSizeSp:I

    .line 260
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionTextColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTextColor:I

    .line 261
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTopDp:I

    .line 262
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionBottomDp:I

    .line 263
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionLeftDp:I

    .line 264
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPermissionRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionRightDp:I

    .line 267
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTextSizeSp:I

    .line 268
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyTextColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTextColor:I

    .line 269
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTopDp:I

    .line 270
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyBottomDp:I

    .line 271
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyLeftDp:I

    .line 272
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadPrivacyRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyRightDp:I

    .line 275
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTextSizeSp:I

    .line 276
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameTextColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTextColor:I

    .line 277
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTopDp:I

    .line 278
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameBottomDp:I

    .line 279
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameLeftDp:I

    .line 280
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadAppNameRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameRightDp:I

    .line 283
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonWidthDp:I

    .line 284
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonHeightDp:I

    .line 285
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonLeftDp:I

    .line 286
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonRightDp:I

    .line 287
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonTopDp:I

    .line 288
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonBottomDp:I

    .line 289
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonForegroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonForegroundColor:I

    .line 290
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonBackgroundColor:I

    .line 291
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontSizeSp:I

    .line 292
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonFontColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontColor:I

    .line 293
    iget-object v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    .line 295
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconWidthDp:I

    .line 296
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconHeightDp:I

    .line 297
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconTopDp:I

    .line 298
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconBottomDp:I

    .line 299
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconLeftDp:I

    .line 300
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIconRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconRightDp:I

    .line 302
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleLeftDp:I

    .line 303
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleRightDp:I

    .line 304
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleTopDp:I

    .line 305
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleBottomDp:I

    .line 306
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontSizeSp:I

    .line 307
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleFontColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontColor:I

    .line 308
    iget-object v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    .line 310
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicWidthDp:I

    .line 311
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicHeightDp:I

    .line 312
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicTopDp:I

    .line 313
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicBottomDp:I

    .line 314
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicLeftDp:I

    .line 315
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mFirstPicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicRightDp:I

    .line 317
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicWidthDp:I

    .line 318
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicHeightDp:I

    .line 319
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicTopDp:I

    .line 320
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicBottomDp:I

    .line 321
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicLeftDp:I

    .line 322
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mTwoPicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicRightDp:I

    .line 324
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicWidthDp:I

    .line 325
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicHeightDp:I

    .line 326
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicTopDp:I

    .line 327
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicBottomDp:I

    .line 328
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicLeftDp:I

    .line 329
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mThreePicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicRightDp:I

    .line 331
    iget-object v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mImageBackground:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mImageBackground:Landroid/graphics/drawable/Drawable;

    .line 332
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mImageBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mImageBackgroundColor:I

    .line 334
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandLeftDp:I

    .line 335
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandRightDp:I

    .line 336
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandBottomDp:I

    .line 337
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontSizeSp:I

    .line 338
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandFontColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontColor:I

    .line 339
    iget-object v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    .line 341
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonWidthDp:I

    .line 342
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonHeightDp:I

    .line 343
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonLeftDp:I

    .line 344
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonRightDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonRightDp:I

    .line 345
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonTopDp:I

    .line 346
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonBottomDp:I

    .line 347
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonFontColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontColor:I

    .line 348
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontSizeSp:I

    .line 349
    iget-object v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    .line 350
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonForegroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonForegroundColor:I

    .line 351
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mButtonBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonBackgroundColor:I

    .line 352
    iget-boolean v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIsShowDownloadInfo:Z

    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIsShowDownloadInfo:Z

    .line 353
    iget-boolean v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mIsShowActionButton:Z

    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIsShowActionButton:Z

    .line 355
    iget-boolean v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->useDislike:Z

    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->useDislike:Z

    .line 356
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mDislikeTopDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeTopDp:I

    .line 357
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mDislikeBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeBottomDp:I

    .line 358
    iget v0, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mDislikeLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeLeftDp:I

    .line 359
    iget p1, p1, Lcom/baidu/mobads/sdk/api/StyleParams$Builder;->mDislikeRightDp:I

    iput p1, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeRightDp:I

    return-void
.end method


# virtual methods
.method public getBrandBottomDp()I
    .locals 1

    .line 499
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandBottomDp:I

    return v0
.end method

.method public getBrandFontColor()I
    .locals 1

    .line 503
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontColor:I

    return v0
.end method

.method public getBrandFontSizeSp()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontSizeSp:I

    return v0
.end method

.method public getBrandFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 505
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getBrandLeftDp()I
    .locals 1

    .line 495
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandLeftDp:I

    return v0
.end method

.method public getBrandRightDp()I
    .locals 1

    .line 497
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mBrandRightDp:I

    return v0
.end method

.method public getButtonBackgroundColor()I
    .locals 1

    .line 527
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonBackgroundColor:I

    return v0
.end method

.method public getButtonBottomDp()I
    .locals 1

    .line 517
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonBottomDp:I

    return v0
.end method

.method public getButtonFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 523
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getButtonForegroundColor()I
    .locals 1

    .line 525
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonForegroundColor:I

    return v0
.end method

.method public getButtonHeightDp()I
    .locals 1

    .line 509
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonHeightDp:I

    return v0
.end method

.method public getButtonLeftDp()I
    .locals 1

    .line 511
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonLeftDp:I

    return v0
.end method

.method public getButtonRightDp()I
    .locals 1

    .line 513
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonRightDp:I

    return v0
.end method

.method public getButtonTextColor()I
    .locals 1

    .line 521
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontColor:I

    return v0
.end method

.method public getButtonTextSizeSp()I
    .locals 1

    .line 519
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonFontSizeSp:I

    return v0
.end method

.method public getButtonTopDp()I
    .locals 1

    .line 515
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonTopDp:I

    return v0
.end method

.method public getButtonWidthDp()I
    .locals 1

    .line 507
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mButtonWidthDp:I

    return v0
.end method

.method public getDislikeBottomDp()I
    .locals 1

    .line 710
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeBottomDp:I

    return v0
.end method

.method public getDislikeLeftDp()I
    .locals 1

    .line 713
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeLeftDp:I

    return v0
.end method

.method public getDislikeRightDp()I
    .locals 1

    .line 716
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeRightDp:I

    return v0
.end method

.method public getDislikeTopDp()I
    .locals 1

    .line 707
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mDislikeTopDp:I

    return v0
.end method

.method public getFirstPicBottomDp()I
    .locals 1

    .line 428
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicBottomDp:I

    return v0
.end method

.method public getFirstPicHeightDp()I
    .locals 1

    .line 420
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicHeightDp:I

    return v0
.end method

.method public getFirstPicLeftDp()I
    .locals 1

    .line 432
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicLeftDp:I

    return v0
.end method

.method public getFirstPicRightDp()I
    .locals 1

    .line 436
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicRightDp:I

    return v0
.end method

.method public getFirstPicTopDp()I
    .locals 1

    .line 424
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicTopDp:I

    return v0
.end method

.method public getFirstPicWidthDp()I
    .locals 1

    .line 416
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mFirstPicWidthDp:I

    return v0
.end method

.method public getIconBottomDp()I
    .locals 1

    .line 376
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconBottomDp:I

    return v0
.end method

.method public getIconHeightDp()I
    .locals 1

    .line 368
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconHeightDp:I

    return v0
.end method

.method public getIconLeftDp()I
    .locals 1

    .line 380
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconLeftDp:I

    return v0
.end method

.method public getIconRightDp()I
    .locals 1

    .line 384
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconRightDp:I

    return v0
.end method

.method public getIconTopDp()I
    .locals 1

    .line 372
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconTopDp:I

    return v0
.end method

.method public getIconWidthDp()I
    .locals 1

    .line 364
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIconWidthDp:I

    return v0
.end method

.method public getImageBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 492
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mImageBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageBackgroundColor()I
    .locals 1

    .line 488
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mImageBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadAppNameBottomDp()I
    .locals 1

    .line 648
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameBottomDp:I

    return v0
.end method

.method public getSmartDownloadAppNameLeftDp()I
    .locals 1

    .line 652
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameLeftDp:I

    return v0
.end method

.method public getSmartDownloadAppNameRightDp()I
    .locals 1

    .line 656
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameRightDp:I

    return v0
.end method

.method public getSmartDownloadAppNameTextColor()I
    .locals 1

    .line 640
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTextColor:I

    return v0
.end method

.method public getSmartDownloadAppNameTextSizeSp()I
    .locals 1

    .line 636
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadAppNameTopDp()I
    .locals 1

    .line 644
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadAppNameTopDp:I

    return v0
.end method

.method public getSmartDownloadButtonBackgroundColor()I
    .locals 1

    .line 688
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadButtonBottomDp()I
    .locals 1

    .line 680
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonBottomDp:I

    return v0
.end method

.method public getSmartDownloadButtonFontColor()I
    .locals 1

    .line 696
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontColor:I

    return v0
.end method

.method public getSmartDownloadButtonFontSizeSp()I
    .locals 1

    .line 692
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontSizeSp:I

    return v0
.end method

.method public getSmartDownloadButtonFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 720
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getSmartDownloadButtonForegroundColor()I
    .locals 1

    .line 684
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonForegroundColor:I

    return v0
.end method

.method public getSmartDownloadButtonHeightDp()I
    .locals 1

    .line 664
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonHeightDp:I

    return v0
.end method

.method public getSmartDownloadButtonLeftDp()I
    .locals 1

    .line 668
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonLeftDp:I

    return v0
.end method

.method public getSmartDownloadButtonRightDp()I
    .locals 1

    .line 672
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonRightDp:I

    return v0
.end method

.method public getSmartDownloadButtonTopDp()I
    .locals 1

    .line 676
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonTopDp:I

    return v0
.end method

.method public getSmartDownloadButtonWidthDp()I
    .locals 1

    .line 660
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadButtonWidthDp:I

    return v0
.end method

.method public getSmartDownloadCompanyBottomDp()I
    .locals 1

    .line 554
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyBottomDp:I

    return v0
.end method

.method public getSmartDownloadCompanyLeftDp()I
    .locals 1

    .line 558
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyLeftDp:I

    return v0
.end method

.method public getSmartDownloadCompanyRightDp()I
    .locals 1

    .line 562
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyRightDp:I

    return v0
.end method

.method public getSmartDownloadCompanyTextColor()I
    .locals 1

    .line 546
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTextColor:I

    return v0
.end method

.method public getSmartDownloadCompanyTextSizeSp()I
    .locals 1

    .line 542
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadCompanyTopDp()I
    .locals 1

    .line 550
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadCompanyTopDp:I

    return v0
.end method

.method public getSmartDownloadPermissionBottomDp()I
    .locals 1

    .line 600
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionBottomDp:I

    return v0
.end method

.method public getSmartDownloadPermissionLeftDp()I
    .locals 1

    .line 604
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionLeftDp:I

    return v0
.end method

.method public getSmartDownloadPermissionRightDp()I
    .locals 1

    .line 608
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionRightDp:I

    return v0
.end method

.method public getSmartDownloadPermissionTextColor()I
    .locals 1

    .line 592
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTextColor:I

    return v0
.end method

.method public getSmartDownloadPermissionTextSizeSp()I
    .locals 1

    .line 589
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadPermissionTopDp()I
    .locals 1

    .line 596
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPermissionTopDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyBottomDp()I
    .locals 1

    .line 624
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyBottomDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyLeftDp()I
    .locals 1

    .line 628
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyLeftDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyRightDp()I
    .locals 1

    .line 632
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyRightDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyTextColor()I
    .locals 1

    .line 616
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTextColor:I

    return v0
.end method

.method public getSmartDownloadPrivacyTextSizeSp()I
    .locals 1

    .line 612
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadPrivacyTopDp()I
    .locals 1

    .line 620
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadPrivacyTopDp:I

    return v0
.end method

.method public getSmartDownloadVersionBottomDp()I
    .locals 1

    .line 578
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionBottomDp:I

    return v0
.end method

.method public getSmartDownloadVersionLeftDp()I
    .locals 1

    .line 582
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionLeftDp:I

    return v0
.end method

.method public getSmartDownloadVersionRightDp()I
    .locals 1

    .line 586
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionRightDp:I

    return v0
.end method

.method public getSmartDownloadVersionTextColor()I
    .locals 1

    .line 570
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTextColor:I

    return v0
.end method

.method public getSmartDownloadVersionTextSizeSp()I
    .locals 1

    .line 566
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadVersionTopDp()I
    .locals 1

    .line 574
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadVersionTopDp:I

    return v0
.end method

.method public getSmartDownloadViewBackgroundColor()I
    .locals 1

    .line 538
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadViewBottomDp()I
    .locals 1

    .line 535
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewBottomDp:I

    return v0
.end method

.method public getSmartDownloadViewLeftDp()I
    .locals 1

    .line 529
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewLeftDp:I

    return v0
.end method

.method public getSmartDownloadViewRightDp()I
    .locals 1

    .line 531
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewRightDp:I

    return v0
.end method

.method public getSmartDownloadViewTopDp()I
    .locals 1

    .line 533
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mSmartDownloadViewTopDp:I

    return v0
.end method

.method public getThreePicBottomDp()I
    .locals 1

    .line 476
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicBottomDp:I

    return v0
.end method

.method public getThreePicHeightDp()I
    .locals 1

    .line 468
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicHeightDp:I

    return v0
.end method

.method public getThreePicLeftDp()I
    .locals 1

    .line 480
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicLeftDp:I

    return v0
.end method

.method public getThreePicRightDp()I
    .locals 1

    .line 484
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicRightDp:I

    return v0
.end method

.method public getThreePicTopDp()I
    .locals 1

    .line 472
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicTopDp:I

    return v0
.end method

.method public getThreePicWidthDp()I
    .locals 1

    .line 464
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mThreePicWidthDp:I

    return v0
.end method

.method public getTitleBottomDp()I
    .locals 1

    .line 400
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleBottomDp:I

    return v0
.end method

.method public getTitleFontColor()I
    .locals 1

    .line 408
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontColor:I

    return v0
.end method

.method public getTitleFontSizeSp()I
    .locals 1

    .line 404
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontSizeSp:I

    return v0
.end method

.method public getTitleFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getTitleLeftDp()I
    .locals 1

    .line 388
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleLeftDp:I

    return v0
.end method

.method public getTitleRightDp()I
    .locals 1

    .line 392
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleRightDp:I

    return v0
.end method

.method public getTitleTopDp()I
    .locals 1

    .line 396
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTitleTopDp:I

    return v0
.end method

.method public getTwoPicBottomDp()I
    .locals 1

    .line 452
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicBottomDp:I

    return v0
.end method

.method public getTwoPicHeightDp()I
    .locals 1

    .line 444
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicHeightDp:I

    return v0
.end method

.method public getTwoPicLeftDp()I
    .locals 1

    .line 456
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicLeftDp:I

    return v0
.end method

.method public getTwoPicRightDp()I
    .locals 1

    .line 460
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicRightDp:I

    return v0
.end method

.method public getTwoPicTopDp()I
    .locals 1

    .line 448
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicTopDp:I

    return v0
.end method

.method public getTwoPicWidthDp()I
    .locals 1

    .line 440
    iget v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mTwoPicWidthDp:I

    return v0
.end method

.method public getUseDislike()Z
    .locals 1

    .line 704
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->useDislike:Z

    return v0
.end method

.method public isShowActionButton()Z
    .locals 1

    .line 700
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIsShowActionButton:Z

    return v0
.end method

.method public isShowDownloadInfo()Z
    .locals 1

    .line 723
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/api/StyleParams;->mIsShowDownloadInfo:Z

    return v0
.end method
