.class public final enum Lcom/baidu/mobads/sdk/api/AdSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/sdk/api/AdSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum Banner:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum FeedH5TemplateNative:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum FeedNative:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialForVideoBeforePlay:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialForVideoPausePlay:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialGame:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialOther:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialReader:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum InterstitialRefresh:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum PrerollVideoNative:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum RewardVideo:Lcom/baidu/mobads/sdk/api/AdSize;

.field public static final enum Square:Lcom/baidu/mobads/sdk/api/AdSize;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v1, 0x0

    const-string v2, "Banner"

    invoke-direct {v0, v2, v1, v1}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->Banner:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 15
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v2, 0x1

    const-string v3, "Square"

    invoke-direct {v0, v3, v2, v2}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->Square:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 31
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v3, 0x2

    const/4 v4, 0x6

    const-string v5, "InterstitialGame"

    invoke-direct {v0, v5, v3, v4}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialGame:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 35
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v5, 0x3

    const/4 v6, 0x7

    const-string v7, "InterstitialReader"

    invoke-direct {v0, v7, v5, v6}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialReader:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 43
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v7, 0x4

    const/16 v8, 0x9

    const-string v9, "InterstitialRefresh"

    invoke-direct {v0, v9, v7, v8}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialRefresh:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 47
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v9, 0x5

    const/16 v10, 0xa

    const-string v11, "InterstitialOther"

    invoke-direct {v0, v11, v9, v10}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialOther:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 51
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/16 v11, 0xc

    const-string v12, "InterstitialForVideoBeforePlay"

    invoke-direct {v0, v12, v4, v11}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 55
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const-string v12, "InterstitialForVideoPausePlay"

    const/16 v13, 0xd

    invoke-direct {v0, v12, v6, v13}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialForVideoPausePlay:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 59
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/16 v12, 0x8

    const-string v13, "RewardVideo"

    const/16 v14, 0xe

    invoke-direct {v0, v13, v12, v14}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->RewardVideo:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 66
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const-string v13, "PrerollVideoNative"

    const/16 v14, 0xf

    invoke-direct {v0, v13, v8, v14}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 70
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const-string v13, "FeedNative"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v10, v14}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->FeedNative:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 74
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdSize;

    const/16 v13, 0xb

    const-string v14, "FeedH5TemplateNative"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v13, v15}, Lcom/baidu/mobads/sdk/api/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->FeedH5TemplateNative:Lcom/baidu/mobads/sdk/api/AdSize;

    new-array v11, v11, [Lcom/baidu/mobads/sdk/api/AdSize;

    .line 7
    sget-object v14, Lcom/baidu/mobads/sdk/api/AdSize;->Banner:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v14, v11, v1

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->Square:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v2

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialGame:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v3

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialReader:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v5

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialRefresh:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v7

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialOther:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v9

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v4

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialForVideoPausePlay:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v6

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->RewardVideo:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v12

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v8

    sget-object v1, Lcom/baidu/mobads/sdk/api/AdSize;->FeedNative:Lcom/baidu/mobads/sdk/api/AdSize;

    aput-object v1, v11, v10

    aput-object v0, v11, v13

    sput-object v11, Lcom/baidu/mobads/sdk/api/AdSize;->$VALUES:[Lcom/baidu/mobads/sdk/api/AdSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput p3, p0, Lcom/baidu/mobads/sdk/api/AdSize;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/AdSize;
    .locals 1

    .line 7
    const-class v0, Lcom/baidu/mobads/sdk/api/AdSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/sdk/api/AdSize;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/sdk/api/AdSize;
    .locals 1

    .line 7
    sget-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->$VALUES:[Lcom/baidu/mobads/sdk/api/AdSize;

    invoke-virtual {v0}, [Lcom/baidu/mobads/sdk/api/AdSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/sdk/api/AdSize;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/baidu/mobads/sdk/api/AdSize;->value:I

    return v0
.end method
