.class public final Lcom/baidu/mobads/sdk/api/AdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# static fields
.field private static final AUTOPLAY:Z = true

.field private static final HEIGHT_FACTOR:F = 0.15f

.field protected static final P_VERSION:Ljava/lang/String; = "3.61"


# instance fields
.field private hasCalledRequestMethod:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mAdProd:Lcom/baidu/mobads/sdk/internal/by;

.field private mAppSid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/AdView;->hasCalledRequestMethod:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ZLcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V
    .locals 6

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p4, 0x0

    invoke-direct {p2, p4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/AdView;->hasCalledRequestMethod:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 70
    new-instance p2, Lcom/baidu/mobads/sdk/internal/bx;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/sdk/internal/bx;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance p4, Lcom/baidu/mobads/sdk/internal/by;

    move-object v0, p4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/sdk/internal/by;-><init>(Lcom/baidu/mobads/sdk/api/AdView;Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Z)V

    iput-object p4, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    if-eqz p4, :cond_0

    .line 73
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAppSid:Ljava/lang/String;

    invoke-virtual {p4, p1}, Lcom/baidu/mobads/sdk/internal/by;->e(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance p1, Lcom/baidu/mobads/sdk/api/AdView$1;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/sdk/api/AdView$1;-><init>(Lcom/baidu/mobads/sdk/api/AdView;)V

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/sdk/internal/bx;->a(Lcom/baidu/mobads/sdk/internal/bx$a;)V

    .line 108
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p1, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p2, p1}, Lcom/baidu/mobads/sdk/api/AdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 58
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/baidu/mobads/sdk/api/AdView;-><init>(Landroid/content/Context;ZLcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 46
    sget-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->Banner:Lcom/baidu/mobads/sdk/api/AdSize;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/baidu/mobads/sdk/api/AdView;-><init>(Landroid/content/Context;ZLcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ZLcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 65
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/sdk/api/AdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ZLcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/mobads/sdk/api/AdView;)Lcom/baidu/mobads/sdk/internal/by;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    return-object p0
.end method

.method static synthetic access$100(Lcom/baidu/mobads/sdk/api/AdView;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/api/AdView;->callRequest()V

    return-void
.end method

.method private callRequest()V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->hasCalledRequestMethod:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->hasCalledRequestMethod:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/by;->a_()V

    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/by;->d()V

    :cond_0
    return-void
.end method

.method public setAppSid(Ljava/lang/String;)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAppSid:Ljava/lang/String;

    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 12

    .line 126
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AdView.setLayoutParams="

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 127
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/AdView;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/AdView;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/az;->a([Ljava/lang/Object;)I

    .line 129
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 130
    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 132
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 133
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/AdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 135
    iget v8, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 136
    iget v9, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 137
    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 138
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v10

    new-array v7, v7, [Ljava/lang/Object;

    const-string v11, "AdView.setLayoutParams"

    aput-object v11, v7, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v7, v6

    invoke-virtual {v10, v7}, Lcom/baidu/mobads/sdk/internal/az;->a([Ljava/lang/Object;)I

    if-gtz v0, :cond_0

    .line 141
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    int-to-float v7, v0

    const/high16 v10, 0x43480000    # 200.0f

    mul-float v10, v10, v2

    cmpg-float v7, v7, v10

    if-gez v7, :cond_1

    float-to-int v0, v10

    :cond_1
    :goto_0
    if-gtz v1, :cond_2

    .line 147
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float v1, v1, v2

    float-to-int v1, v1

    goto :goto_1

    :cond_2
    if-lez v1, :cond_3

    int-to-float v7, v1

    const/high16 v8, 0x41f00000    # 30.0f

    mul-float v2, v2, v8

    cmpg-float v7, v7, v2

    if-gez v7, :cond_3

    float-to-int v1, v2

    .line 151
    :cond_3
    :goto_1
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 152
    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 153
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    if-eqz v2, :cond_4

    .line 154
    invoke-virtual {v2, v0}, Lcom/baidu/mobads/sdk/internal/by;->a(I)V

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/by;->c(I)V

    .line 157
    :cond_4
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "AdView.setLayoutParams adapter"

    aput-object v2, v1, v3

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 158
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/az;->a([Ljava/lang/Object;)I

    .line 159
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setListener(Lcom/baidu/mobads/sdk/api/AdViewListener;)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/AdView;->mAdProd:Lcom/baidu/mobads/sdk/internal/by;

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/by;->a(Lcom/baidu/mobads/sdk/api/AdViewListener;)V

    :cond_0
    return-void
.end method
