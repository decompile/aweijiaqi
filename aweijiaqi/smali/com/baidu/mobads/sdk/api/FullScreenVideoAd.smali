.class public Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FullScreenVideoAd"


# instance fields
.field private mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

.field private mAppSid:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;Z)V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Lcom/baidu/mobads/sdk/internal/cd;

    invoke-direct {v0, p1, p2, p4}, Lcom/baidu/mobads/sdk/internal/cd;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    .line 76
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAppSid:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/cl;->e(Ljava/lang/String;)V

    .line 77
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/sdk/internal/cl;->a(Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;)V

    return-void
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->r()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized load()V
    .locals 1

    monitor-enter p0

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->a_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAppSid(Ljava/lang/String;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAppSid:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized show()V
    .locals 1

    monitor-enter p0

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
