.class public Lcom/baidu/mobads/sdk/api/BDMarketingLabel;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private mAdView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mLoader:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mContext:Landroid/content/Context;

    .line 22
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 4

    .line 43
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Class;

    .line 44
    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    .line 46
    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/ay;->a(Landroid/content/Context;)Ljava/lang/ClassLoader;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mLoader:Ljava/lang/ClassLoader;

    .line 47
    sget-object p1, Lcom/baidu/mobads/sdk/internal/p;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mLoader:Ljava/lang/ClassLoader;

    .line 48
    invoke-static {p1, v2, v1, v0}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 51
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public setAdData(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 7

    .line 35
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 36
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setAdData"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLabelFontSizeSp(I)V
    .locals 7

    .line 57
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 58
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 60
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setLabelFontSizeSp"

    move-object v6, v0

    .line 58
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLabelFontTypeFace(Landroid/graphics/Typeface;)V
    .locals 7

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 66
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingLabel;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/Typeface;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setLabelFontTypeFace"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
