.class public Lcom/baidu/mobads/sdk/api/RewardVideoAd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/RewardVideoAd$RewardVideoAdListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RewardVideoAd"


# instance fields
.field private mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RewardVideoAd$RewardVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/sdk/api/RewardVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RewardVideoAd$RewardVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RewardVideoAd$RewardVideoAdListener;Z)V
    .locals 1

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mContext:Landroid/content/Context;

    .line 67
    new-instance v0, Lcom/baidu/mobads/sdk/internal/cl;

    invoke-direct {v0, p1, p2, p4}, Lcom/baidu/mobads/sdk/internal/cl;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    .line 68
    invoke-virtual {v0, p3}, Lcom/baidu/mobads/sdk/internal/cl;->a(Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;)V

    return-void
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->r()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized load()V
    .locals 1

    monitor-enter p0

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->a_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAppSid(Ljava/lang/String;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/cl;->e(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized show()V
    .locals 1

    monitor-enter p0

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cl;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cl;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
