.class public Lcom/baidu/mobads/sdk/api/SplashAd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;,
        Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;,
        Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;
    }
.end annotation


# static fields
.field public static final KEY_DISPLAY_DOWNLOADINFO:Ljava/lang/String; = "displayDownloadInfo"

.field public static final KEY_FETCHAD:Ljava/lang/String; = "fetchAd"

.field public static final KEY_LIMIT_REGION_CLICK:Ljava/lang/String; = "region_click"

.field public static final KEY_POPDIALOG_DOWNLOAD:Ljava/lang/String; = "use_dialog_frame"

.field public static final KEY_TIMEOUT:Ljava/lang/String; = "timeout"

.field private static final RT_SPLASH_LOAD_AD_TIMEOUT:I = 0x1068


# instance fields
.field private mAdPlaceId:Ljava/lang/String;

.field private mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

.field private mAppSid:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDisplayDownInfo:Z

.field private mDownloadDialogListener:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

.field private mFetchAd:Z

.field private mFetchNotShow:Z

.field private mLimitRegionClick:Ljava/lang/Boolean;

.field private mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

.field private mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

.field private mPopDialogIfDL:Ljava/lang/Boolean;

.field private mTimeout:I

.field private mTipStyle:I

.field private mViewParent:Landroid/view/ViewGroup;

.field private splashAdView:Lcom/baidu/mobads/sdk/internal/bx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RequestParameters;Lcom/baidu/mobads/sdk/api/SplashAdListener;)V
    .locals 3

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    .line 39
    iput v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTipStyle:I

    const/4 v0, 0x1

    .line 52
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchAd:Z

    const/4 v1, 0x0

    .line 58
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 54
    iput-boolean v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchNotShow:Z

    .line 56
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDisplayDownInfo:Z

    .line 58
    iput-object v2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mPopDialogIfDL:Ljava/lang/Boolean;

    .line 60
    iput-object v2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mLimitRegionClick:Ljava/lang/Boolean;

    const/16 v0, 0x1068

    .line 62
    iput v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTimeout:I

    .line 64
    new-instance v0, Lcom/baidu/mobads/sdk/api/SplashAd$1;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/api/SplashAd$1;-><init>(Lcom/baidu/mobads/sdk/api/SplashAd;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    .line 117
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    .line 118
    iput-object p2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdPlaceId:Ljava/lang/String;

    if-eqz p4, :cond_0

    .line 120
    iput-object p4, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    .line 122
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 123
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    const-string p2, "\u8bf7\u60a8\u8f93\u5165\u6b63\u786e\u7684\u5e7f\u544a\u4f4dID"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    return-void

    .line 126
    :cond_1
    iput-object p3, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    if-eqz p3, :cond_6

    .line 127
    invoke-virtual {p3}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 128
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    const-string p2, "fetchAd"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 129
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 130
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchAd:Z

    .line 133
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    const-string p2, "displayDownloadInfo"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 134
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 135
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDisplayDownInfo:Z

    .line 138
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    const-string p2, "use_dialog_frame"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 139
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 140
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mPopDialogIfDL:Ljava/lang/Boolean;

    .line 142
    :cond_4
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    const-string p2, "region_click"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 143
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_5

    .line 144
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mLimitRegionClick:Ljava/lang/Boolean;

    .line 146
    :cond_5
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    const-string p2, "timeout"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 147
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 148
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTimeout:I

    :cond_6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/SplashAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/baidu/mobads/sdk/api/SplashAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RequestParameters;Lcom/baidu/mobads/sdk/api/SplashAdListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/mobads/sdk/api/SplashAd;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchNotShow:Z

    return p0
.end method

.method static synthetic access$002(Lcom/baidu/mobads/sdk/api/SplashAd;Z)Z
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchNotShow:Z

    return p1
.end method

.method static synthetic access$100(Lcom/baidu/mobads/sdk/api/SplashAd;)Lcom/baidu/mobads/sdk/internal/cm;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/baidu/mobads/sdk/api/SplashAd;)Ljava/lang/Boolean;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mPopDialogIfDL:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$102(Lcom/baidu/mobads/sdk/api/SplashAd;Lcom/baidu/mobads/sdk/internal/cm;)Lcom/baidu/mobads/sdk/internal/cm;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/baidu/mobads/sdk/api/SplashAd;)Ljava/lang/Boolean;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mLimitRegionClick:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/baidu/mobads/sdk/api/SplashAd;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAppSid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/baidu/mobads/sdk/api/SplashAd;)Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDownloadDialogListener:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/baidu/mobads/sdk/api/SplashAd;Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/SplashAd;->callAdFailed(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/baidu/mobads/sdk/api/SplashAd;)Landroid/content/Context;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$400(Lcom/baidu/mobads/sdk/api/SplashAd;)Lcom/baidu/mobads/sdk/api/RequestParameters;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    return-object p0
.end method

.method static synthetic access$500(Lcom/baidu/mobads/sdk/api/SplashAd;)Lcom/baidu/mobads/sdk/api/SplashAdListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    return-object p0
.end method

.method static synthetic access$600(Lcom/baidu/mobads/sdk/api/SplashAd;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdPlaceId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/baidu/mobads/sdk/api/SplashAd;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTipStyle:I

    return p0
.end method

.method static synthetic access$800(Lcom/baidu/mobads/sdk/api/SplashAd;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTimeout:I

    return p0
.end method

.method static synthetic access$900(Lcom/baidu/mobads/sdk/api/SplashAd;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDisplayDownInfo:Z

    return p0
.end method

.method private addZeroPxSurfaceViewAvoidBlink(Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 2

    .line 329
    :try_start_0
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 330
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {p2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 331
    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 333
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private callAdFailed(Ljava/lang/String;)V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static registerEnterTransition(Landroid/app/Activity;IILcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V
    .locals 2

    .line 468
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "right_margin"

    .line 470
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "bottom_margin"

    .line 471
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 473
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/Throwable;)I

    .line 475
    :goto_0
    invoke-static {p0, v0, p3}, Lcom/baidu/mobads/sdk/internal/cm;->a(Landroid/app/Activity;Lorg/json/JSONObject;Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V

    return-void
.end method

.method public static registerEnterTransition(Landroid/app/Activity;Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 463
    invoke-static {p0, v0, p1}, Lcom/baidu/mobads/sdk/internal/cm;->a(Landroid/app/Activity;Lorg/json/JSONObject;Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cm;->d()V

    :cond_0
    const/4 v0, 0x0

    .line 310
    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    return-void
.end method

.method public finishAndJump(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x0

    .line 439
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/SplashAd;->finishAndJump(Landroid/content/Intent;Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;)V

    return-void
.end method

.method public finishAndJump(Landroid/content/Intent;Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;)V
    .locals 1

    .line 448
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/sdk/internal/cm;->a(Landroid/content/Intent;Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;)V

    :cond_0
    return-void
.end method

.method public getECPMLevel()Ljava/lang/String;
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cm;->r()Lcom/baidu/mobads/sdk/internal/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/a;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final load()V
    .locals 14

    .line 157
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bx;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/sdk/internal/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    .line 159
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 161
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cm;->l()V

    const/4 v0, 0x0

    .line 165
    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/ah;->e(Landroid/content/Context;)F

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/mobads/sdk/internal/ah;->a(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 172
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 173
    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    if-eqz v3, :cond_2

    .line 174
    invoke-virtual {v3}, Lcom/baidu/mobads/sdk/api/RequestParameters;->isCustomSize()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 175
    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {v3}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getWidth()I

    move-result v3

    if-lez v3, :cond_1

    .line 176
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {v2}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v0

    float-to-int v2, v2

    .line 178
    :cond_1
    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {v3}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getHeight()I

    move-result v3

    if-lez v3, :cond_2

    .line 179
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v0

    float-to-int v1, v1

    :cond_2
    move v8, v1

    move v7, v2

    int-to-float v1, v7

    const/high16 v2, 0x43480000    # 200.0f

    mul-float v2, v2, v0

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_5

    int-to-float v1, v8

    const/high16 v2, 0x43160000    # 150.0f

    mul-float v0, v0, v2

    cmpg-float v0, v1, v0

    if-gez v0, :cond_3

    goto :goto_0

    .line 193
    :cond_3
    new-instance v0, Lcom/baidu/mobads/sdk/internal/cm;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    iget-object v6, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdPlaceId:Ljava/lang/String;

    iget v9, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTipStyle:I

    iget v10, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mTimeout:I

    iget-boolean v11, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDisplayDownInfo:Z

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mPopDialogIfDL:Ljava/lang/Boolean;

    .line 194
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mLimitRegionClick:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v3, v0

    invoke-direct/range {v3 .. v13}, Lcom/baidu/mobads/sdk/internal/cm;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;IIIIZZZ)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    .line 195
    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAppSid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/cm;->e(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/baidu/mobads/sdk/internal/cm;->o:Z

    .line 197
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mParameter:Lcom/baidu/mobads/sdk/api/RequestParameters;

    if-eqz v0, :cond_4

    .line 198
    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    invoke-virtual {v2, v0}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lcom/baidu/mobads/sdk/api/RequestParameters;)V

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lcom/baidu/mobads/sdk/api/SplashAdListener;)V

    .line 201
    iput-boolean v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mFetchNotShow:Z

    .line 202
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDownloadDialogListener:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;)V

    .line 203
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/cm;->a_()V

    return-void

    .line 186
    :cond_5
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/ca;->a()Lcom/baidu/mobads/sdk/internal/ca;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/baidu/mobads/sdk/internal/aw;->b:Lcom/baidu/mobads/sdk/internal/aw;

    const-string v2, "\u5f00\u5c4f\u663e\u793a\u533a\u57df\u592a\u5c0f,\u5bbd\u5ea6\u81f3\u5c11200dp,\u9ad8\u5ea6\u81f3\u5c11150dp"

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/sdk/internal/ca;->a(Lcom/baidu/mobads/sdk/internal/aw;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/az;->c(Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mListener:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdDismissed()V

    return-void
.end method

.method public loadAndShow(Landroid/view/ViewGroup;)V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/SplashAd;->addZeroPxSurfaceViewAvoidBlink(Landroid/view/ViewGroup;Landroid/content/Context;)V

    .line 345
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bx;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/sdk/internal/bx;-><init>(Landroid/content/Context;)V

    .line 346
    new-instance v1, Lcom/baidu/mobads/sdk/api/SplashAd$3;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/sdk/api/SplashAd$3;-><init>(Lcom/baidu/mobads/sdk/api/SplashAd;Lcom/baidu/mobads/sdk/internal/bx;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/bx;->a(Lcom/baidu/mobads/sdk/internal/bx$a;)V

    .line 423
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 425
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 426
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setAppSid(Ljava/lang/String;)V
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAppSid:Ljava/lang/String;

    return-void
.end method

.method public setDownloadDialogListener(Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;)V
    .locals 0

    .line 478
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mDownloadDialogListener:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    return-void
.end method

.method public final show(Landroid/view/ViewGroup;)V
    .locals 1

    .line 211
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mViewParent:Landroid/view/ViewGroup;

    if-eqz p1, :cond_1

    .line 212
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_1

    .line 213
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bx;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 214
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/cm;->l()V

    const-string p1, "\u5c55\u73b0\u5931\u8d25\uff0c\u8bf7\u91cd\u65b0load"

    .line 215
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/SplashAd;->callAdFailed(Ljava/lang/String;)V

    return-void

    .line 219
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    new-instance v0, Lcom/baidu/mobads/sdk/api/SplashAd$2;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/api/SplashAd$2;-><init>(Lcom/baidu/mobads/sdk/api/SplashAd;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/bx;->a(Lcom/baidu/mobads/sdk/internal/bx$a;)V

    .line 268
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mViewParent:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->splashAdView:Lcom/baidu/mobads/sdk/internal/bx;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 270
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/sdk/api/SplashAd;->mAdProd:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz p1, :cond_2

    .line 271
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/cm;->l()V

    :cond_2
    const-string p1, "\u5c55\u73b0\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5splashAd\u53c2\u6570\u662f\u5426\u6b63\u786e"

    .line 273
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/SplashAd;->callAdFailed(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
