.class public Lcom/baidu/mobads/sdk/api/BDMarketingTextView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private mAdView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mLoader:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mContext:Landroid/content/Context;

    .line 23
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private initView(Landroid/content/Context;)V
    .locals 4

    .line 48
    :try_start_0
    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Class;

    .line 49
    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    .line 51
    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/ay;->a(Landroid/content/Context;)Ljava/lang/ClassLoader;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    .line 52
    sget-object p1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    .line 53
    invoke-static {p1, v2, v1, v0}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 56
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public setAdData(Lcom/baidu/mobads/sdk/api/NativeResponse;Ljava/lang/String;)V
    .locals 8

    .line 39
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 40
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const-class v4, Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v4, v5, v7

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    aput-object p2, v0, v7

    const-string v4, "setAdData"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 7

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 98
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Landroid/text/TextUtils$TruncateAt;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setEllipsize"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLabelFontSizeSp(I)V
    .locals 7

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 122
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 124
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setLabelFontSizeSp"

    move-object v6, v0

    .line 122
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLabelFontTypeFace(Landroid/graphics/Typeface;)V
    .locals 7

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 130
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/Typeface;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setLabelFontTypeFace"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLabelVisibility(I)V
    .locals 7

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 114
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 116
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setLabelVisibility"

    move-object v6, v0

    .line 114
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setLineSpacingExtra(I)V
    .locals 7

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 106
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 108
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setLineSpacingExtra"

    move-object v6, v0

    .line 106
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setTextFontColor(I)V
    .locals 7

    .line 81
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 82
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 84
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setTextFontColor"

    move-object v6, v0

    .line 82
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setTextFontSizeSp(I)V
    .locals 7

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 66
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 68
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setTextFontSizeSp"

    move-object v6, v0

    .line 66
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setTextFontTypeFace(Landroid/graphics/Typeface;)V
    .locals 7

    .line 73
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 74
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/Typeface;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v6

    const-string v4, "setTextFontTypeFace"

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setTextMaxLines(I)V
    .locals 7

    .line 89
    iget-object v0, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 90
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/api/BDMarketingTextView;->mLoader:Ljava/lang/ClassLoader;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    new-array v0, v0, [Ljava/lang/Object;

    .line 92
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v6

    const-string v4, "setTextMaxLines"

    move-object v6, v0

    .line 90
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
