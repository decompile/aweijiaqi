.class public Lcom/baidu/mobads/sdk/internal/w;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"


# instance fields
.field private o:I

.field private p:I

.field private q:[I

.field private r:Z

.field private s:I

.field private t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

.field private v:Lcom/baidu/mobads/sdk/api/NativeCPUManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/NativeCPUManager;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/w;->n:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/w;->v:Lcom/baidu/mobads/sdk/api/NativeCPUManager;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .line 88
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/w;->s:I

    return-void
.end method

.method public a(II[IZLjava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[IZ",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 47
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/w;->p:I

    .line 48
    iput p2, p0, Lcom/baidu/mobads/sdk/internal/w;->o:I

    .line 49
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/w;->q:[I

    .line 50
    iput-boolean p4, p0, Lcom/baidu/mobads/sdk/internal/w;->r:Z

    .line 51
    iput-object p5, p0, Lcom/baidu/mobads/sdk/internal/w;->t:Ljava/util/HashMap;

    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 1

    .line 114
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(ILjava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_0

    .line 116
    invoke-interface {v0, p2, p1}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onAdError(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 5

    .line 93
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_1

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 96
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v1, "cpuAdList"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 97
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 98
    new-instance v2, Lcom/baidu/mobads/sdk/internal/x;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/w;->g:Landroid/content/Context;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/w;->v:Lcom/baidu/mobads/sdk/api/NativeCPUManager;

    invoke-direct {v2, v3, v1, v4}, Lcom/baidu/mobads/sdk/internal/x;-><init>(Landroid/content/Context;Ljava/lang/Object;Lcom/baidu/mobads/sdk/api/NativeCPUManager;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    invoke-interface {p1, v0}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onAdLoaded(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 1

    .line 106
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;I)V

    .line 107
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_0

    .line 108
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onAdError(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public a_()V
    .locals 6

    const-string v0, "cpu"

    const-string v1, "prod"

    .line 56
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/w;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/w;->k:Z

    return-void

    :cond_0
    const/4 v2, 0x1

    .line 60
    iput-boolean v2, p0, Lcom/baidu/mobads/sdk/internal/w;->k:Z

    .line 61
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 62
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 64
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 65
    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 66
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/w;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v5, v4}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 67
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/w;->h()V

    .line 68
    invoke-virtual {v2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "appsid"

    .line 69
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/w;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "pageIndex"

    .line 70
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/w;->p:I

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "pageSize"

    .line 71
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/w;->o:I

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "channels"

    .line 72
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/w;->q:[I

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "showAd"

    .line 73
    iget-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/w;->r:Z

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "appid"

    .line 75
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/w;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    const-string v0, "timeout"

    .line 78
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/w;->s:I

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->t:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/j;->a(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 82
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 124
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 126
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_1

    .line 127
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onAdStatusChanged(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected c()V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_0

    .line 141
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onVideoDownloadFailed()V

    :cond_0
    return-void
.end method

.method protected c(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 148
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v0, "position"

    .line 149
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v1, "mislikereason"

    .line 150
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 151
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0, p1}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onDisLikeAdClick(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected c_()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/w;->u:Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;

    if-eqz v0, :cond_0

    .line 134
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/NativeCPUManager$CPUAdListener;->onVideoDownloadSuccess()V

    :cond_0
    return-void
.end method
