.class public Lcom/baidu/mobads/sdk/internal/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final a:Ljava/lang/String; = "MD5"

.field public static final b:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/baidu/mobads/sdk/internal/bd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lorg/json/JSONObject;

.field private d:D

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 102
    new-instance v0, Lcom/baidu/mobads/sdk/internal/be;

    invoke-direct {v0}, Lcom/baidu/mobads/sdk/internal/be;-><init>()V

    sput-object v0, Lcom/baidu/mobads/sdk/internal/bd;->b:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->f:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->e:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->d:D

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->g:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->h:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/baidu/mobads/sdk/internal/be;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/bd;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->d:D

    .line 55
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->e:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->f:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->a()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    .line 58
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->g:Ljava/lang/String;

    .line 59
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->h:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/baidu/mobads/sdk/internal/bd;->c:Lorg/json/JSONObject;

    const-string p1, "version"

    .line 41
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/sdk/internal/bd;->d:D

    .line 42
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->c:Lorg/json/JSONObject;

    const-string v2, "url"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->e:Ljava/lang/String;

    .line 43
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->c:Lorg/json/JSONObject;

    const-string v2, "sign"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->f:Ljava/lang/String;

    .line 44
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    const-string p1, ""

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bd;->g:Ljava/lang/String;

    .line 46
    iput v1, p0, Lcom/baidu/mobads/sdk/internal/bd;->h:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 48
    :catch_0
    iput v1, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    .line 50
    :goto_0
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/bd;->c()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 2

    .line 74
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public b()D
    .locals 2

    .line 83
    iget-wide v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->d:D

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .line 87
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bw;->a()Lcom/baidu/mobads/sdk/internal/bw;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bd;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/bw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->f:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->g:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/Boolean;
    .locals 2

    .line 99
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->c:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 126
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->f:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->i:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-wide v0, p0, Lcom/baidu/mobads/sdk/internal/bd;->d:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 130
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->g:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget p2, p0, Lcom/baidu/mobads/sdk/internal/bd;->h:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
