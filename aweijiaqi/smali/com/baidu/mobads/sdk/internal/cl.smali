.class public Lcom/baidu/mobads/sdk/internal/cl;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"


# instance fields
.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

.field private final r:Ljava/lang/String;

.field private s:I

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "rvideo"

    .line 30
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/sdk/internal/cl;-><init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    .line 36
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cl;->p:Ljava/lang/String;

    .line 37
    iput-boolean p3, p0, Lcom/baidu/mobads/sdk/internal/cl;->o:Z

    .line 38
    iput-object p4, p0, Lcom/baidu/mobads/sdk/internal/cl;->r:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/String;)V
    .locals 0

    .line 174
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(ILjava/lang/String;)V

    .line 176
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz p1, :cond_0

    .line 177
    invoke-interface {p1, p2}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdFailed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    .line 110
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz p1, :cond_0

    .line 111
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdLoaded()V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 0

    .line 165
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;I)V

    .line 167
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz p2, :cond_0

    .line 168
    invoke-interface {p2, p1}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdFailed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a_()V
    .locals 6

    const-string v0, "prod"

    const-string v1, ""

    .line 52
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->k:Z

    return-void

    :cond_0
    const/4 v2, 0x1

    .line 56
    iput-boolean v2, p0, Lcom/baidu/mobads/sdk/internal/cl;->k:Z

    .line 57
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 58
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 60
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 61
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cl;->r:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v5, v4}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 63
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cl;->h()V

    .line 65
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "apid"

    .line 66
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->p:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "fet"

    const-string v4, "ANTI,MSSP,VIDEO,NMON"

    .line 67
    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "n"

    const-string v4, "1"

    .line 68
    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const/16 v0, 0xa

    const-string v4, "at"

    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "appid"

    .line 73
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/ah;->a(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->s:I

    .line 77
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->t:I

    .line 78
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    .line 79
    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 80
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, p0, Lcom/baidu/mobads/sdk/internal/cl;->s:I

    .line 81
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->t:I

    :cond_2
    const-string v0, "w"

    .line 83
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/baidu/mobads/sdk/internal/cl;->s:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "h"

    .line 84
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/baidu/mobads/sdk/internal/cl;->t:I

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "timeout"

    const/16 v1, 0x1f40

    .line 86
    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "useSurfaceView"

    .line 87
    iget-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/cl;->o:Z

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 91
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method protected c()V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 159
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onVideoDownloadFailed()V

    :cond_0
    return-void
.end method

.method protected c_()V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 151
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onVideoDownloadSuccess()V

    :cond_0
    return-void
.end method

.method protected d(Ljava/lang/String;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdSkip(F)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->showAd()V

    :cond_0
    return-void
.end method

.method protected f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 130
    invoke-super {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;->f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V

    if-eqz p1, :cond_0

    .line 132
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v0, "play_scale"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_1

    .line 136
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdClose(F)V

    :cond_1
    return-void
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    .line 117
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz p1, :cond_0

    .line 118
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 105
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method protected n()V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->q:Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;

    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/ScreenVideoAdListener;->playCompletion()V

    :cond_0
    return-void
.end method

.method public r()Z
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cl;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->isAdReady()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
