.class public Lcom/baidu/mobads/sdk/internal/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x40

    new-array v0, v0, [B

    .line 8
    fill-array-data v0, :array_0

    sput-object v0, Lcom/baidu/mobads/sdk/internal/g;->a:[B

    const/16 v0, 0x80

    new-array v0, v0, [B

    .line 17
    sput-object v0, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    const/4 v0, 0x0

    .line 20
    :goto_0
    sget-object v1, Lcom/baidu/mobads/sdk/internal/g;->a:[B

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 21
    sget-object v2, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    aget-byte v1, v1, v0

    int-to-byte v3, v0

    aput-byte v3, v2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 1
        0x30t
        0x4bt
        0x61t
        0x6at
        0x44t
        0x37t
        0x41t
        0x5at
        0x63t
        0x46t
        0x32t
        0x51t
        0x6et
        0x50t
        0x72t
        0x35t
        0x66t
        0x77t
        0x69t
        0x48t
        0x52t
        0x4et
        0x79t
        0x67t
        0x6dt
        0x75t
        0x70t
        0x55t
        0x54t
        0x49t
        0x58t
        0x78t
        0x36t
        0x39t
        0x42t
        0x57t
        0x62t
        0x2dt
        0x68t
        0x4dt
        0x43t
        0x47t
        0x4at
        0x6ft
        0x5ft
        0x56t
        0x38t
        0x45t
        0x73t
        0x6bt
        0x7at
        0x31t
        0x59t
        0x64t
        0x76t
        0x4ct
        0x33t
        0x34t
        0x6ct
        0x65t
        0x74t
        0x71t
        0x53t
        0x4ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(B)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x24

    if-ne p1, v1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-ltz p1, :cond_3

    const/16 v2, 0x80

    if-lt p1, v2, :cond_1

    goto :goto_0

    .line 144
    :cond_1
    sget-object v2, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    aget-byte p1, v2, p1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    return v1

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v1
.end method

.method private c(Ljava/lang/String;)[B
    .locals 10

    .line 55
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/g;->d(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    if-eqz p1, :cond_7

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    goto/16 :goto_3

    .line 70
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_2

    .line 71
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    div-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v1, :cond_3

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    div-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    div-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [B

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 77
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v2

    if-ge v3, v5, :cond_4

    .line 78
    sget-object v5, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget-byte v5, v5, v6

    .line 79
    sget-object v6, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aget-byte v6, v6, v7

    .line 80
    sget-object v7, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    add-int/lit8 v8, v3, 0x2

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    aget-byte v7, v7, v8

    .line 81
    sget-object v8, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    add-int/lit8 v9, v3, 0x3

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    aget-byte v8, v8, v9

    shl-int/lit8 v5, v5, 0x2

    shr-int/lit8 v9, v6, 0x4

    or-int/2addr v5, v9

    int-to-byte v5, v5

    .line 82
    aput-byte v5, v0, v4

    add-int/lit8 v5, v4, 0x1

    shl-int/2addr v6, v2

    shr-int/lit8 v9, v7, 0x2

    or-int/2addr v6, v9

    int-to-byte v6, v6

    .line 83
    aput-byte v6, v0, v5

    add-int/lit8 v5, v4, 0x2

    shl-int/lit8 v6, v7, 0x6

    or-int/2addr v6, v8

    int-to-byte v6, v6

    .line 84
    aput-byte v6, v0, v5

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v4, v4, 0x3

    goto :goto_1

    .line 86
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v1, :cond_5

    .line 87
    sget-object v1, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aget-byte v1, v1, v3

    .line 88
    sget-object v3, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result p1

    aget-byte p1, v3, p1

    .line 89
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    shl-int/lit8 v1, v1, 0x2

    shr-int/2addr p1, v2

    or-int/2addr p1, v1

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto/16 :goto_2

    .line 90
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v1, :cond_6

    .line 91
    sget-object v1, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aget-byte v1, v1, v3

    .line 92
    sget-object v3, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aget-byte v3, v3, v4

    .line 93
    sget-object v4, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result p1

    aget-byte p1, v4, p1

    .line 94
    array-length v4, v0

    add-int/lit8 v4, v4, -0x2

    shl-int/lit8 v1, v1, 0x2

    shr-int/lit8 v5, v3, 0x4

    or-int/2addr v1, v5

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    .line 95
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    shl-int/lit8 v2, v3, 0x4

    shr-int/lit8 p1, p1, 0x2

    or-int/2addr p1, v2

    int-to-byte p1, p1

    aput-byte p1, v0, v1

    goto :goto_2

    .line 97
    :cond_6
    sget-object v1, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aget-byte v1, v1, v3

    .line 98
    sget-object v3, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aget-byte v3, v3, v4

    .line 99
    sget-object v4, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aget-byte v4, v4, v5

    .line 100
    sget-object v5, Lcom/baidu/mobads/sdk/internal/g;->b:[B

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result p1

    aget-byte p1, v5, p1

    .line 101
    array-length v5, v0

    add-int/lit8 v5, v5, -0x3

    shl-int/lit8 v1, v1, 0x2

    shr-int/lit8 v6, v3, 0x4

    or-int/2addr v1, v6

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    .line 102
    array-length v1, v0

    add-int/lit8 v1, v1, -0x2

    shl-int/lit8 v2, v3, 0x4

    shr-int/lit8 v3, v4, 0x2

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 103
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    shl-int/lit8 v2, v4, 0x6

    or-int/2addr p1, v2

    int-to-byte p1, p1

    aput-byte p1, v0, v1

    :goto_2
    return-object v0

    :cond_7
    :goto_3
    return-object v1
.end method

.method private d(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 131
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 132
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/sdk/internal/g;->a(B)Z

    move-result v3

    if-nez v3, :cond_1

    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .line 26
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 32
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 33
    array-length v0, v0

    const/4 v1, 0x3

    rem-int/2addr v0, v1

    :goto_0
    if-lez v0, :cond_1

    if-ge v0, v1, :cond_1

    .line 36
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "$"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 40
    array-length v0, p1

    div-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    new-array v0, v0, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 42
    :goto_1
    array-length v4, p1

    if-ge v2, v4, :cond_2

    .line 44
    sget-object v4, Lcom/baidu/mobads/sdk/internal/g;->a:[B

    aget-byte v5, p1, v2

    and-int/lit16 v5, v5, 0xfc

    shr-int/lit8 v5, v5, 0x2

    aget-byte v5, v4, v5

    aput-byte v5, v0, v3

    add-int/lit8 v5, v3, 0x1

    .line 45
    aget-byte v6, p1, v2

    and-int/2addr v6, v1

    shl-int/lit8 v6, v6, 0x4

    add-int/lit8 v7, v2, 0x1

    aget-byte v8, p1, v7

    and-int/lit16 v8, v8, 0xf0

    shr-int/lit8 v8, v8, 0x4

    add-int/2addr v6, v8

    aget-byte v6, v4, v6

    aput-byte v6, v0, v5

    add-int/lit8 v5, v3, 0x2

    .line 46
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0xf

    shl-int/lit8 v6, v6, 0x2

    add-int/lit8 v7, v2, 0x2

    aget-byte v8, p1, v7

    and-int/lit16 v8, v8, 0xc0

    shr-int/lit8 v8, v8, 0x6

    add-int/2addr v6, v8

    aget-byte v6, v4, v6

    aput-byte v6, v0, v5

    add-int/lit8 v5, v3, 0x3

    .line 47
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0x3f

    aget-byte v4, v4, v6

    aput-byte v4, v0, v5

    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v3, v3, 0x4

    goto :goto_1

    .line 50
    :cond_2
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/String;-><init>([B)V

    return-object p1
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_2

    .line 109
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    goto :goto_1

    .line 114
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/g;->c(Ljava/lang/String;)[B

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    :goto_0
    const-string p1, "$"

    .line 116
    invoke-virtual {v0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 117
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    return-object v0

    :catch_0
    const-string p1, ""

    return-object p1

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method
