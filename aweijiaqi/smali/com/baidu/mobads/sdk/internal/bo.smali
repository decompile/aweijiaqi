.class public Lcom/baidu/mobads/sdk/internal/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/bo$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "APKParser"

.field private static final g:Ljava/lang/String; = "__xadsdk_downloaded__version__"

.field private static final h:Ljava/lang/String; = "version"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/net/URL;

.field private d:Ljava/lang/String;

.field private final e:Lcom/baidu/mobads/sdk/internal/bd;

.field private f:Lcom/baidu/mobads/sdk/internal/bo$a;

.field private i:Landroid/content/SharedPreferences;

.field private j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/internal/bd;Lcom/baidu/mobads/sdk/internal/bo$a;)V
    .locals 1

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->c:Ljava/net/URL;

    .line 33
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->d:Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bp;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/internal/bp;-><init>(Lcom/baidu/mobads/sdk/internal/bo;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 66
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->d:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/bo;->e:Lcom/baidu/mobads/sdk/internal/bd;

    .line 68
    invoke-direct {p0, p1, p4}, Lcom/baidu/mobads/sdk/internal/bo;->a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bo$a;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;Lcom/baidu/mobads/sdk/internal/bd;Lcom/baidu/mobads/sdk/internal/bo$a;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->c:Ljava/net/URL;

    .line 33
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->d:Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bp;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/internal/bp;-><init>(Lcom/baidu/mobads/sdk/internal/bo;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 60
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->c:Ljava/net/URL;

    .line 61
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/bo;->e:Lcom/baidu/mobads/sdk/internal/bd;

    .line 62
    invoke-direct {p0, p1, p4}, Lcom/baidu/mobads/sdk/internal/bo;->a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bo$a;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bo$a;)V
    .locals 1

    .line 72
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bo;->b:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->f:Lcom/baidu/mobads/sdk/internal/bo$a;

    const-string p2, "__xadsdk_downloaded__version__"

    const/4 v0, 0x0

    .line 75
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bo;->i:Landroid/content/SharedPreferences;

    .line 77
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {p1, p2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 89
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bo;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bo;->c:Ljava/net/URL;

    :goto_0
    move-object v3, v0

    .line 90
    new-instance v0, Lcom/baidu/mobads/sdk/internal/aa;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bo;->b:Landroid/content/Context;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/sdk/internal/aa;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 92
    invoke-interface {v0, p0}, Lcom/baidu/mobads/sdk/internal/n;->addObserver(Ljava/util/Observer;)V

    .line 93
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/n;->a()V

    .line 96
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bo;->i:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 97
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->e:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {p2}, Lcom/baidu/mobads/sdk/internal/bd;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "version"

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 102
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x9

    if-lt p2, v0, :cond_1

    .line 103
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 105
    :cond_1
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4

    .line 111
    check-cast p1, Lcom/baidu/mobads/sdk/internal/n;

    .line 113
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/internal/n;->l()Lcom/baidu/mobads/sdk/internal/n$a;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/sdk/internal/n$a;->e:Lcom/baidu/mobads/sdk/internal/n$a;

    if-ne p2, v0, :cond_0

    .line 114
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->f:Lcom/baidu/mobads/sdk/internal/bo$a;

    new-instance v0, Lcom/baidu/mobads/sdk/internal/bd;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bo;->e:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/internal/n;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/baidu/mobads/sdk/internal/bd;-><init>(Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-interface {p2, v0}, Lcom/baidu/mobads/sdk/internal/bo$a;->a(Lcom/baidu/mobads/sdk/internal/bd;)V

    .line 116
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/internal/n;->l()Lcom/baidu/mobads/sdk/internal/n$a;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/sdk/internal/n$a;->f:Lcom/baidu/mobads/sdk/internal/n$a;

    if-ne p2, v0, :cond_1

    .line 117
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/bo;->f:Lcom/baidu/mobads/sdk/internal/bo$a;

    new-instance v0, Lcom/baidu/mobads/sdk/internal/bd;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bo;->e:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/internal/n;->g()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/baidu/mobads/sdk/internal/bd;-><init>(Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-interface {p2, v0}, Lcom/baidu/mobads/sdk/internal/bo$a;->b(Lcom/baidu/mobads/sdk/internal/bd;)V

    :cond_1
    return-void
.end method
