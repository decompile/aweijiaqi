.class public Lcom/baidu/mobads/sdk/internal/cj;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/sdk/internal/r;


# static fields
.field private static final o:Ljava/lang/String; = "preload_end"


# instance fields
.field private p:Lcom/baidu/mobads/sdk/api/AdSize;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z

.field private t:Landroid/widget/RelativeLayout;

.field private u:Lcom/baidu/mobads/sdk/api/InterstitialAd;

.field private v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/sdk/api/InterstitialAd;Lcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 38
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    .line 39
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    .line 66
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/cj;->u:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    .line 67
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cj;->t:Landroid/widget/RelativeLayout;

    .line 68
    iput-object p4, p0, Lcom/baidu/mobads/sdk/internal/cj;->p:Lcom/baidu/mobads/sdk/api/AdSize;

    .line 69
    iput-object p5, p0, Lcom/baidu/mobads/sdk/internal/cj;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/sdk/api/InterstitialAd;Ljava/lang/String;)V
    .locals 6

    .line 53
    sget-object v4, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialGame:Lcom/baidu/mobads/sdk/api/AdSize;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/sdk/internal/cj;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/sdk/api/InterstitialAd;Lcom/baidu/mobads/sdk/api/AdSize;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->showAd()V

    return-void
.end method

.method public a(II)V
    .locals 2

    .line 127
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "w"

    .line 130
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "h"

    .line 131
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :catch_0
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/sdk/internal/cj;->a(Lorg/json/JSONObject;)V

    .line 137
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->showAd()V

    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 143
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cj;->b_()V

    return-void
.end method

.method public a(Landroid/widget/RelativeLayout;)V
    .locals 4

    .line 164
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 165
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    const-string v2, "event_type"

    const-string v3, "interstitial_set_video_parent"

    .line 167
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "interstitial_video_parent"

    .line 168
    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 170
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/Throwable;)I

    .line 172
    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/sdk/internal/cj;->a(Lorg/json/JSONObject;Ljava/util/Map;)V

    .line 173
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cj;->b_()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 186
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v0, "preload_end"

    .line 192
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 193
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    .line 194
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    if-eqz p1, :cond_0

    .line 195
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/InterstitialAdListener;->onAdReady()V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/InterstitialAdListener;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 79
    invoke-super {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;->e(Ljava/lang/String;)V

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 0

    .line 224
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    if-eqz p2, :cond_0

    .line 225
    invoke-interface {p2, p1}, Lcom/baidu/mobads/sdk/api/InterstitialAdListener;->onAdFailed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a_()V
    .locals 8

    const-string v0, "0"

    const-string v1, "int"

    const-string v2, "prod"

    .line 84
    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v3, :cond_0

    const/4 v0, 0x0

    .line 85
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->k:Z

    return-void

    :cond_0
    const/4 v3, 0x1

    .line 88
    iput-boolean v3, p0, Lcom/baidu/mobads/sdk/internal/cj;->k:Z

    .line 89
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 90
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 92
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 93
    invoke-virtual {v6, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v7, v6}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 95
    iget-object v6, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cj;->t:Landroid/widget/RelativeLayout;

    invoke-interface {v6, v7}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->setAdContainer(Landroid/widget/RelativeLayout;)V

    .line 96
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cj;->h()V

    .line 97
    invoke-virtual {v4, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "apid"

    .line 98
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cj;->q:Ljava/lang/String;

    invoke-virtual {v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "at"

    const-string v2, "2"

    .line 99
    invoke-virtual {v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "w"

    .line 100
    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "h"

    .line 101
    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "appid"

    .line 103
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cj;->n:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 106
    :cond_1
    sget-object v0, Lcom/baidu/mobads/sdk/api/AdSize;->InterstitialGame:Lcom/baidu/mobads/sdk/api/AdSize;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cj;->p:Lcom/baidu/mobads/sdk/api/AdSize;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/api/AdSize;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ABILITY"

    const-string v1, "PAUSE,"

    .line 107
    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const-string v0, "APT"

    .line 109
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cj;->p:Lcom/baidu/mobads/sdk/api/AdSize;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/AdSize;->getValue()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "onlyLoadAd"

    .line 110
    invoke-virtual {v5, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 112
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v4, v5}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 181
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    return v0
.end method

.method public b_()V
    .locals 2

    .line 147
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 148
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    const/4 v0, 0x0

    .line 149
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    .line 158
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->showAd()V

    return-void

    .line 151
    :cond_0
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->h:Lcom/baidu/mobads/sdk/internal/az;

    const-string v1, "interstitial ad is showing now"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/az;->b(Ljava/lang/String;)I

    goto :goto_0

    .line 153
    :cond_1
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->r:Z

    if-nez v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->h:Lcom/baidu/mobads/sdk/internal/az;

    const-string v1, "interstitial ad is not ready"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/az;->b(Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void
.end method

.method public e()V
    .locals 0

    .line 74
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cj;->a_()V

    return-void
.end method

.method protected f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    const/4 p1, 0x0

    .line 216
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->s:Z

    .line 217
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    if-eqz p1, :cond_0

    .line 218
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/InterstitialAdListener;->onAdDismissed()V

    :cond_0
    return-void
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 209
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    if-eqz p1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->u:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    invoke-interface {p1, v0}, Lcom/baidu/mobads/sdk/api/InterstitialAdListener;->onAdClick(Lcom/baidu/mobads/sdk/api/InterstitialAd;)V

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cj;->v:Lcom/baidu/mobads/sdk/api/InterstitialAdListener;

    if-eqz v0, :cond_0

    .line 203
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/InterstitialAdListener;->onAdPresent()V

    :cond_0
    return-void
.end method

.method public r()V
    .locals 0

    return-void
.end method
