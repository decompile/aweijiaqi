.class public Lcom/baidu/mobads/sdk/internal/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/sdk/internal/e$a;


# instance fields
.field private a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 68
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onLpClosed()V

    :cond_0
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 27
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onNoAd(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 1

    .line 40
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    if-eqz v0, :cond_0

    .line 41
    check-cast p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onADExposed()V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/NativeResponse;I)V
    .locals 1

    .line 47
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    if-eqz v0, :cond_0

    .line 48
    check-cast p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onADExposureFailed(I)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/sdk/api/NativeResponse;",
            ">;)V"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 20
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onNativeLoad(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onVideoDownloadSuccess()V

    :cond_0
    return-void
.end method

.method public b(ILjava/lang/String;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 34
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onNativeFail(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$PortraitVideoAdListener;

    if-eqz v1, :cond_0

    .line 56
    check-cast v0, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$PortraitVideoAdListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$PortraitVideoAdListener;->onAdClick()V

    return-void

    .line 60
    :cond_0
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    if-eqz v0, :cond_1

    .line 61
    check-cast p1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onAdClick()V

    :cond_1
    return-void
.end method

.method public c()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/v;->a:Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;->onVideoDownloadFailed()V

    :cond_0
    return-void
.end method
