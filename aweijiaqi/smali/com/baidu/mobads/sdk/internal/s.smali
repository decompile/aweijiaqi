.class public Lcom/baidu/mobads/sdk/internal/s;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/s$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "LoadRemoteDex"

.field private static i:Lcom/baidu/mobads/sdk/internal/s;


# instance fields
.field private b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

.field private c:Lcom/baidu/mobads/sdk/internal/av;

.field private d:I

.field private e:Landroid/os/Handler;

.field private f:Ljava/lang/Runnable;

.field private g:Landroid/content/Context;

.field private h:Lcom/baidu/mobads/sdk/internal/az;

.field private j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private k:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1388

    .line 21
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/s;->d:I

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->e:Landroid/os/Handler;

    .line 26
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->h:Lcom/baidu/mobads/sdk/internal/az;

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/s;)Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/s;Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;)Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    return-object p1
.end method

.method public static a()Lcom/baidu/mobads/sdk/internal/s;
    .locals 2

    .line 33
    sget-object v0, Lcom/baidu/mobads/sdk/internal/s;->i:Lcom/baidu/mobads/sdk/internal/s;

    if-nez v0, :cond_1

    .line 34
    const-class v0, Lcom/baidu/mobads/sdk/internal/s;

    monitor-enter v0

    .line 35
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/sdk/internal/s;->i:Lcom/baidu/mobads/sdk/internal/s;

    if-nez v1, :cond_0

    .line 36
    new-instance v1, Lcom/baidu/mobads/sdk/internal/s;

    invoke-direct {v1}, Lcom/baidu/mobads/sdk/internal/s;-><init>()V

    sput-object v1, Lcom/baidu/mobads/sdk/internal/s;->i:Lcom/baidu/mobads/sdk/internal/s;

    .line 38
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 40
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/sdk/internal/s;->i:Lcom/baidu/mobads/sdk/internal/s;

    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/s;Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/s;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->h:Lcom/baidu/mobads/sdk/internal/az;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u52a0\u8f7ddex\u5931\u8d25\u539f\u56e0="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "LoadRemoteDex"

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 180
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->i()V

    .line 181
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/k;->a()Lcom/baidu/mobads/sdk/internal/k;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/internal/k;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/sdk/internal/s;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->k()V

    return-void
.end method

.method private f()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 81
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->h()V

    goto :goto_0

    .line 84
    :cond_0
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->g()V

    :goto_0
    return-void
.end method

.method private g()V
    .locals 4

    .line 89
    const-class v0, Lcom/baidu/mobads/sdk/internal/s;

    monitor-enter v0

    .line 90
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 91
    sget-object v2, Lcom/baidu/mobads/sdk/internal/p;->aq:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x1

    .line 94
    :try_start_1
    invoke-static {v2, v3, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 95
    new-instance v2, Lcom/baidu/mobads/sdk/internal/av;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    invoke-direct {v2, v1, v3}, Lcom/baidu/mobads/sdk/internal/av;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/baidu/mobads/sdk/internal/s;->c:Lcom/baidu/mobads/sdk/internal/av;

    .line 96
    invoke-virtual {v2}, Lcom/baidu/mobads/sdk/internal/av;->a()Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    .line 97
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->k()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    const-string v1, "\u53cd\u5c04\u8c03\u7528remote\u5931\u8d25"

    .line 99
    invoke-direct {p0, v1}, Lcom/baidu/mobads/sdk/internal/s;->a(Ljava/lang/String;)V

    .line 101
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private h()V
    .locals 3

    .line 105
    new-instance v0, Lcom/baidu/mobads/sdk/internal/t;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/internal/t;-><init>(Lcom/baidu/mobads/sdk/internal/s;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->f:Ljava/lang/Runnable;

    .line 111
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->j()V

    .line 112
    sget-object v0, Lcom/baidu/mobads/sdk/internal/f;->a:Lcom/baidu/mobads/sdk/internal/bf;

    if-nez v0, :cond_1

    .line 113
    const-class v0, Lcom/baidu/mobads/sdk/internal/bf;

    monitor-enter v0

    .line 114
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/sdk/internal/f;->a:Lcom/baidu/mobads/sdk/internal/bf;

    if-nez v1, :cond_0

    .line 115
    new-instance v1, Lcom/baidu/mobads/sdk/internal/bf;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/baidu/mobads/sdk/internal/bf;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/sdk/internal/f;->a:Lcom/baidu/mobads/sdk/internal/bf;

    .line 117
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 124
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    if-eqz v0, :cond_2

    .line 125
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->k()V

    goto :goto_1

    .line 129
    :cond_2
    sget-object v0, Lcom/baidu/mobads/sdk/internal/f;->a:Lcom/baidu/mobads/sdk/internal/bf;

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->h:Lcom/baidu/mobads/sdk/internal/az;

    const-string v1, "LoadRemoteDex"

    const-string v2, "start load apk"

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    sget-object v0, Lcom/baidu/mobads/sdk/internal/f;->a:Lcom/baidu/mobads/sdk/internal/bf;

    new-instance v1, Lcom/baidu/mobads/sdk/internal/u;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/sdk/internal/u;-><init>(Lcom/baidu/mobads/sdk/internal/s;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf$c;)V

    goto :goto_1

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->h:Lcom/baidu/mobads/sdk/internal/az;

    const-string v1, "LoadRemoteDex"

    const-string v2, "BaiduXAdSDKContext.mApkLoader == null,not load apk"

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method private i()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/s;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    .line 160
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->f:Ljava/lang/Runnable;

    return-void
.end method

.method private j()V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 165
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/s;->e:Landroid/os/Handler;

    iget v2, p0, Lcom/baidu/mobads/sdk/internal/s;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 171
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->i()V

    .line 172
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/k;->a()Lcom/baidu/mobads/sdk/internal/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/k;->a(I)V

    .line 173
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/bt;->a(Landroid/content/Context;)Lcom/baidu/mobads/sdk/internal/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bt;->b()V

    .line 174
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/bt;->a(Landroid/content/Context;)Lcom/baidu/mobads/sdk/internal/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bt;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/s$a;)V
    .locals 2

    if-nez p1, :cond_0

    .line 48
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->h:Lcom/baidu/mobads/sdk/internal/az;

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "LoadRemoteDex"

    aput-object v1, p2, v0

    const/4 v0, 0x1

    const-string v1, "init Context is null,error"

    aput-object v1, p2, v0

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/sdk/internal/az;->c([Ljava/lang/Object;)I

    return-void

    .line 51
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    .line 52
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/k;->a()Lcom/baidu/mobads/sdk/internal/k;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/sdk/internal/k;->a(Lcom/baidu/mobads/sdk/internal/s$a;)V

    .line 53
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    if-eqz p1, :cond_1

    .line 54
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->k()V

    return-void

    .line 57
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_2

    .line 58
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->f()V

    :cond_2
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    return-object v0
.end method

.method public c()Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 73
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/s;->f()V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/s;->b:Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;

    invoke-interface {v1}, Lcom/baidu/mobads/sdk/api/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public e()Z
    .locals 1

    .line 194
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/s;->k:Z

    return v0
.end method
