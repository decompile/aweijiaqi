.class public abstract Lcom/baidu/mobads/sdk/internal/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/ar$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String; = "XAbstractProdTemplate"

.field public static final b:Ljava/lang/String; = "error_message"

.field public static final c:Ljava/lang/String; = "error_code"

.field protected static final d:Ljava/lang/String; = "instanceInfo"

.field protected static final e:Ljava/lang/String; = "showState"


# instance fields
.field protected f:Landroid/widget/RelativeLayout;

.field protected g:Landroid/content/Context;

.field protected h:Lcom/baidu/mobads/sdk/internal/az;

.field public i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

.field public j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

.field public k:Z

.field public l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->h:Lcom/baidu/mobads/sdk/internal/az;

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->k:Z

    .line 41
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ar;->g:Landroid/content/Context;

    .line 42
    new-instance p1, Lcom/baidu/mobads/sdk/internal/ar$a;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/sdk/internal/ar$a;-><init>(Lcom/baidu/mobads/sdk/internal/ar;)V

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    .line 43
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/s;->a()Lcom/baidu/mobads/sdk/internal/s;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->g:Landroid/content/Context;

    new-instance v1, Lcom/baidu/mobads/sdk/internal/as;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/sdk/internal/as;-><init>(Lcom/baidu/mobads/sdk/internal/ar;)V

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/sdk/internal/s;->a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/s$a;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    .line 430
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bdsdk://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "?jsonObj="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 371
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 372
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 374
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/sdk/internal/at;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/sdk/internal/at;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 414
    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 419
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    :cond_1
    :goto_0
    return-object v0
.end method

.method protected a(ILjava/lang/String;)V
    .locals 0

    .line 199
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ar;->l()V

    return-void
.end method

.method public a(Landroid/view/View;Lorg/json/JSONObject;)V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    const-string v1, "onAdImpression"

    .line 80
    invoke-direct {p0, v1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onAdTaskProcess(Landroid/view/View;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 0

    .line 204
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ar;->l()V

    return-void
.end method

.method protected a(Ljava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 388
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->l:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 389
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->l:Ljava/util/HashMap;

    goto :goto_0

    .line 391
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :goto_0
    if-eqz p1, :cond_1

    .line 393
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 394
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 396
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 398
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->l:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    nop

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    const-string v1, "onHandleEvent"

    .line 92
    invoke-direct {p0, v1, p1}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onAdTaskProcess(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    const-string v1, "onHandleEvent"

    .line 98
    invoke-direct {p0, v1, p1}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onAdTaskProcess(Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 156
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 162
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public abstract a_()V
.end method

.method public b(I)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 150
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onWindowVisibilityChanged(I)V

    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;Lorg/json/JSONObject;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    const-string v1, "onAdClick"

    .line 86
    invoke-direct {p0, v1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onAdTaskProcess(Landroid/view/View;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected b(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected b(Ljava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected c(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected c_()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 169
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->destroyAd()V

    :cond_0
    return-void
.end method

.method protected d(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method protected d(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected e(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ar;->n:Ljava/lang/String;

    return-void
.end method

.method public f()V
    .locals 6

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/sdk/internal/ay;->a(Landroid/content/Context;)Ljava/lang/ClassLoader;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->j:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/ar;->g:Landroid/content/Context;

    aput-object v4, v2, v5

    .line 67
    invoke-static {v1, v0, v3, v2}, Lcom/baidu/mobads/sdk/internal/ag;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/sdk/api/IAdInterListener;

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    .line 69
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->k:Z

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ar;->a_()V

    :cond_0
    return-void
.end method

.method protected f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    .line 194
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ar;->l()V

    return-void
.end method

.method public g()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x1

    .line 75
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;I)V

    return-void
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 3

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 106
    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->E:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 107
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->G:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 108
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->I:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 109
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->J:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 110
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->T:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->U:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 114
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->K:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->L:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->H:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->A:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 119
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->X:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->Y:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->W:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->S:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->Z:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->aa:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 126
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->ab:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 128
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->ac:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->ad:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 130
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->ae:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ar;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 138
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onAttachedToWindow()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 144
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->onDetachedFromWindow()V

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 0

    return-void
.end method

.method public l()V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 210
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->removeAllListeners()V

    :cond_0
    return-void
.end method

.method protected m()V
    .locals 0

    return-void
.end method

.method protected n()V
    .locals 0

    return-void
.end method

.method protected o()V
    .locals 0

    return-void
.end method

.method protected p()V
    .locals 0

    return-void
.end method

.method public q()Landroid/view/View;
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ar;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 408
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->getAdContainerView()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
