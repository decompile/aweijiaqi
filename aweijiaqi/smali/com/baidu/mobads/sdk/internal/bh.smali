.class Lcom/baidu/mobads/sdk/internal/bh;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/sdk/internal/bf;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/sdk/internal/bf;Landroid/os/Looper;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .line 119
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "APK_INFO"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/sdk/internal/bd;

    const-string v1, "OK"

    .line 122
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "ApkLoader"

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    .line 123
    new-instance v0, Lcom/baidu/mobads/sdk/internal/ay;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {v4}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v1, v4, p1}, Lcom/baidu/mobads/sdk/internal/ay;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bd;)V

    .line 132
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    iget-object v1, v1, Lcom/baidu/mobads/sdk/internal/bf;->u:Landroid/os/Handler;

    sget-object v4, Lcom/baidu/mobads/sdk/internal/bf;->t:Landroid/os/Handler;

    if-ne v1, v4, :cond_1

    .line 134
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/ay;->a()V

    .line 136
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bf;->f()Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/ay;->a(Ljava/lang/String;)V

    .line 138
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    if-eqz v1, :cond_0

    .line 139
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v4

    iput-wide v4, v1, Lcom/baidu/mobads/sdk/internal/av;->b:D

    .line 142
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/bf;->b(Lcom/baidu/mobads/sdk/internal/bf;)V

    .line 143
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/bf;->c(Lcom/baidu/mobads/sdk/internal/bf;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 145
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1, v3}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;Z)Z

    .line 146
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {v1}, Lcom/baidu/mobads/sdk/internal/bf;->d(Lcom/baidu/mobads/sdk/internal/bf;)Z

    move-result v1

    const-string v4, "load remote file just downloaded"

    invoke-static {p1, v1, v4}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;ZLjava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1, v0}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;Lcom/baidu/mobads/sdk/internal/ay;)V

    .line 151
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bf;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/internal/ay;->a(Ljava/lang/String;)V

    .line 152
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/baidu/mobads/sdk/internal/bf;->b(Lcom/baidu/mobads/sdk/internal/bf;Z)V
    :try_end_0
    .catch Lcom/baidu/mobads/sdk/internal/bf$a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/ay;->delete()Z

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 155
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download apk file failed: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/bf$a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 156
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {v1, v3}, Lcom/baidu/mobads/sdk/internal/bf;->b(Lcom/baidu/mobads/sdk/internal/bf;Z)V

    .line 157
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {v1}, Lcom/baidu/mobads/sdk/internal/bf;->e(Lcom/baidu/mobads/sdk/internal/bf;)Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v1

    invoke-virtual {v1, v2, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :goto_1
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/ay;->delete()Z

    throw p1

    .line 162
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/bf;->e(Lcom/baidu/mobads/sdk/internal/bf;)Lcom/baidu/mobads/sdk/internal/az;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mOnApkDownloadCompleted: download failed, code: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1, v3}, Lcom/baidu/mobads/sdk/internal/bf;->b(Lcom/baidu/mobads/sdk/internal/bf;Z)V

    .line 164
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/bf;->c(Lcom/baidu/mobads/sdk/internal/bf;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 165
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    invoke-static {p1, v3}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;Z)Z

    .line 166
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bh;->a:Lcom/baidu/mobads/sdk/internal/bf;

    const-string v0, "Refused to download remote for version..."

    invoke-static {p1, v3, v0}, Lcom/baidu/mobads/sdk/internal/bf;->a(Lcom/baidu/mobads/sdk/internal/bf;ZLjava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method
