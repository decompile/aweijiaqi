.class public Lcom/baidu/mobads/sdk/internal/bx;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/bx$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String; = "ContainerView"


# instance fields
.field private b:Lcom/baidu/mobads/sdk/internal/bx$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobads/sdk/internal/bx$a;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 57
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 58
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/bx$a;->onAttachedToWindow()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 66
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz v0, :cond_0

    .line 67
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/bx$a;->onDetachedFromWindow()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz v0, :cond_0

    .line 90
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/internal/bx$a;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1

    .line 92
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    const/4 p1, 0x0

    return p1
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 49
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    if-eqz p1, :cond_0

    .line 50
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz p1, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/bx;->getWidth()I

    move-result p2

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/bx;->getHeight()I

    move-result p3

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/sdk/internal/bx$a;->onLayoutComplete(II)V

    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 81
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    .line 82
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz v0, :cond_0

    .line 83
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/internal/bx$a;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .line 73
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowVisibilityChanged(I)V

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bx;->b:Lcom/baidu/mobads/sdk/internal/bx$a;

    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/internal/bx$a;->onWindowVisibilityChanged(I)V

    :cond_0
    return-void
.end method
