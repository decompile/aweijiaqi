.class public Lcom/baidu/mobads/sdk/internal/bb;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "ApkDownloadThread"

.field private static final c:I = 0xdbba0

.field private static volatile h:Lcom/baidu/mobads/sdk/internal/bb;


# instance fields
.field a:Lcom/baidu/mobads/sdk/internal/bo$a;

.field private volatile d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:D

.field private g:Landroid/os/Handler;

.field private final i:Landroid/content/Context;

.field private j:Lcom/baidu/mobads/sdk/internal/bo;

.field private final k:Lcom/baidu/mobads/sdk/internal/bd;

.field private l:Lcom/baidu/mobads/sdk/internal/az;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->e:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->j:Lcom/baidu/mobads/sdk/internal/bo;

    .line 41
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    .line 46
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bc;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/sdk/internal/bc;-><init>(Lcom/baidu/mobads/sdk/internal/bb;)V

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->a:Lcom/baidu/mobads/sdk/internal/bo$a;

    .line 76
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bb;->i:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    .line 79
    invoke-virtual {p2}, Lcom/baidu/mobads/sdk/internal/bd;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/sdk/internal/bb;->a(Ljava/lang/String;)V

    .line 81
    iput-object p4, p0, Lcom/baidu/mobads/sdk/internal/bb;->g:Landroid/os/Handler;

    .line 82
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/bb;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Landroid/os/Handler;)Lcom/baidu/mobads/sdk/internal/bb;
    .locals 1

    .line 69
    sget-object v0, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bb;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/baidu/mobads/sdk/internal/bb;-><init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;Landroid/os/Handler;)V

    sput-object v0, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;

    .line 72
    :cond_0
    sget-object p0, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/bb;)Lcom/baidu/mobads/sdk/internal/bb;
    .locals 0

    .line 20
    sput-object p0, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;

    return-object p0
.end method

.method private a()Ljava/lang/String;
    .locals 5

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "__xadsdk__remote__final__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 182
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 185
    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/bb;->j:Lcom/baidu/mobads/sdk/internal/bo;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/bb;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/baidu/mobads/sdk/internal/bo;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 187
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 188
    throw v0
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/bb;Ljava/lang/String;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/sdk/internal/bb;->a(Ljava/lang/String;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/baidu/mobads/sdk/internal/bd;Ljava/lang/String;)V
    .locals 2

    const-string p3, "OK"

    .line 165
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    const-string p3, "ERROR"

    .line 166
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 167
    :cond_0
    iget-object p3, p0, Lcom/baidu/mobads/sdk/internal/bb;->g:Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p3

    .line 168
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "APK_INFO"

    .line 170
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string p2, "CODE"

    .line 171
    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 174
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/bb;->g:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 10

    const-string v0, "ApkDownloadThread"

    .line 202
    :try_start_0
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 203
    new-instance v2, Lcom/baidu/mobads/sdk/internal/bo;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/bb;->i:Landroid/content/Context;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/bb;->a:Lcom/baidu/mobads/sdk/internal/bo$a;

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/baidu/mobads/sdk/internal/bo;-><init>(Landroid/content/Context;Ljava/net/URL;Lcom/baidu/mobads/sdk/internal/bd;Lcom/baidu/mobads/sdk/internal/bo$a;)V

    iput-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->j:Lcom/baidu/mobads/sdk/internal/bo;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto/16 :goto_2

    .line 205
    :catch_1
    :try_start_1
    new-instance v1, Lcom/baidu/mobads/sdk/internal/bo;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->i:Landroid/content/Context;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/bb;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/bb;->a:Lcom/baidu/mobads/sdk/internal/bo$a;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/baidu/mobads/sdk/internal/bo;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/internal/bd;Lcom/baidu/mobads/sdk/internal/bo$a;)V

    iput-object v1, p0, Lcom/baidu/mobads/sdk/internal/bb;->j:Lcom/baidu/mobads/sdk/internal/bo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 216
    :goto_0
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->q:Lcom/baidu/mobads/sdk/internal/av;

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_0

    .line 217
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->q:Lcom/baidu/mobads/sdk/internal/av;

    iget-wide v4, v1, Lcom/baidu/mobads/sdk/internal/av;->b:D

    goto :goto_1

    .line 218
    :cond_0
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    if-eqz v1, :cond_2

    .line 219
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    iget-wide v4, v1, Lcom/baidu/mobads/sdk/internal/av;->b:D

    cmpl-double v1, v4, v2

    if-lez v1, :cond_1

    .line 220
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    iget-wide v4, v1, Lcom/baidu/mobads/sdk/internal/av;->b:D

    goto :goto_1

    .line 222
    :cond_1
    sget-object v1, Lcom/baidu/mobads/sdk/internal/bf;->p:Lcom/baidu/mobads/sdk/internal/av;

    iget-wide v4, v1, Lcom/baidu/mobads/sdk/internal/av;->b:D

    goto :goto_1

    :cond_2
    move-wide v4, v2

    .line 227
    :goto_1
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isNewApkAvailable: local apk version is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v7, ", remote apk version: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    .line 229
    invoke-virtual {v7}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 227
    invoke-virtual {v1, v0, v6}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    const/4 v6, 0x0

    cmpl-double v7, v4, v2

    if-lez v7, :cond_4

    .line 232
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {v4}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v4

    cmpl-double v7, v4, v2

    if-lez v7, :cond_3

    .line 233
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    const-string v3, "remote not null, local apk version is null, force upgrade"

    invoke-virtual {v2, v0, v3}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->f:D

    return v1

    .line 237
    :cond_3
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    const-string v2, "remote is null, local apk version is null, do not upgrade"

    invoke-virtual {v1, v0, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    .line 241
    :cond_4
    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {v7}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v7

    cmpg-double v9, v7, v2

    if-gtz v9, :cond_5

    .line 242
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remote apk version is: null, local apk version is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, ", do not upgrade"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    .line 246
    :cond_5
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v2

    cmpl-double v0, v2, v4

    if-lez v0, :cond_6

    .line 248
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/bb;->k:Lcom/baidu/mobads/sdk/internal/bd;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bd;->b()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->f:D

    return v1

    :cond_6
    return v6

    .line 209
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parse apk failed, error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    invoke-virtual {v2, v0, v1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    new-instance v0, Lcom/baidu/mobads/sdk/internal/bf$a;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/sdk/internal/bf$a;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/bb;->d:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/bb;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 6

    const-string v0, "ApkDownloadThread"

    .line 94
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/bb;->b()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    const-string v2, "download apk successfully, downloader exit"

    .line 99
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/bb;->a()Ljava/lang/String;

    .line 102
    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    invoke-virtual {v3, v0, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    sput-object v1, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 105
    :try_start_2
    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create File or HTTP Get failed, exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/bb;->l:Lcom/baidu/mobads/sdk/internal/az;

    const-string v3, "no newer apk, downloader exit"

    invoke-virtual {v2, v0, v3}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    sput-object v1, Lcom/baidu/mobads/sdk/internal/bb;->h:Lcom/baidu/mobads/sdk/internal/bb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    return-void
.end method
