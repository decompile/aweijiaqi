.class public Lcom/baidu/mobads/sdk/internal/cc;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"


# instance fields
.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/baidu/mobads/sdk/api/NativeResponse;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:I

.field private v:Lcom/baidu/mobads/sdk/api/RequestParameters;

.field private w:Z

.field private x:Lcom/baidu/mobads/sdk/internal/e$a;

.field private y:Lcom/baidu/mobads/sdk/internal/e$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    const/16 p1, 0x1f40

    .line 30
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->p:I

    const/4 p1, 0x0

    .line 40
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->w:Z

    .line 46
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cc;->s:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/cc;->r:Ljava/lang/String;

    .line 48
    iput-boolean p4, p0, Lcom/baidu/mobads/sdk/internal/cc;->q:Z

    .line 49
    iput p5, p0, Lcom/baidu/mobads/sdk/internal/cc;->p:I

    const/16 p1, 0x258

    .line 50
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->t:I

    const/16 p1, 0x1f4

    .line 51
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->u:I

    return-void
.end method

.method private a(Lcom/baidu/mobads/sdk/internal/a;)I
    .locals 2

    if-eqz p1, :cond_0

    .line 317
    :try_start_0
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/a;->p()I

    move-result v0

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    .line 319
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/a;->q()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "fb_act"

    .line 320
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return p1

    :catchall_0
    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method protected a(ILjava/lang/String;)V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_0

    .line 194
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/internal/e$a;->a(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/RequestParameters;)V
    .locals 2

    .line 115
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getWidth()I

    move-result v0

    .line 116
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getHeight()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 118
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->t:I

    .line 119
    iput v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->u:I

    .line 121
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->v:Lcom/baidu/mobads/sdk/api/RequestParameters;

    .line 123
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/sdk/internal/cc;->a(Ljava/util/Map;)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/internal/e$a;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/internal/e$b;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->y:Lcom/baidu/mobads/sdk/internal/e$b;

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_0

    .line 187
    invoke-interface {v0, p2, p1}, Lcom/baidu/mobads/sdk/internal/e$a;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;Z)V
    .locals 3

    .line 291
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 292
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 294
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    invoke-virtual {v1, p2}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onADPermissionShow(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a_()V
    .locals 8

    const-string v0, "prod"

    .line 64
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 65
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->k:Z

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 68
    iput-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->k:Z

    .line 69
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 70
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 72
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 73
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->r:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v5, v4}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 75
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->f:Landroid/widget/RelativeLayout;

    invoke-interface {v4, v5}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->setAdContainer(Landroid/widget/RelativeLayout;)V

    .line 76
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cc;->h()V

    .line 77
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "apid"

    .line 78
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->s:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bw;->a()Lcom/baidu/mobads/sdk/internal/bw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/bw;->b()Z

    move-result v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "fet"

    if-eqz v0, :cond_1

    :try_start_1
    const-string v0, "ANTI,MSSP,VIDEO,NMON,HTML"

    .line 80
    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_1
    const-string v0, "ANTI,MSSP,VIDEO,NMON,HTML,CLICK2VIDEO"

    .line 82
    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_0
    const-string v0, "n"

    const-string v5, "1"

    .line 84
    invoke-virtual {v2, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 85
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "appid"

    .line 86
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const/4 v0, 0x2

    const-string v5, "video"

    .line 89
    iget-object v6, p0, Lcom/baidu/mobads/sdk/internal/cc;->r:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v6, "at"

    const-string v7, ""

    if-eqz v5, :cond_3

    :try_start_2
    const-string v0, "10"

    .line 90
    invoke-virtual {v2, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "mimetype"

    const-string v5, "video/mp4,image/jpg,image/gif,image/png"

    .line 91
    invoke-virtual {v2, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ANTI,HTML,MSSP,VIDEO,NMON"

    .line 92
    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 94
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_1
    const-string v0, "w"

    .line 96
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->t:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "h"

    .line 97
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/baidu/mobads/sdk/internal/cc;->u:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 99
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->l:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/sdk/internal/cc;->a(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    .line 100
    invoke-static {v2, v0}, Lcom/baidu/mobads/sdk/internal/j;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v0, "timeout"

    .line 102
    iget v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->p:I

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "isCacheVideo"

    .line 103
    iget-boolean v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->q:Z

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "cacheVideoOnlyWifi"

    .line 104
    iget-boolean v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->w:Z

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "appConfirmPolicy"

    .line 105
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->v:Lcom/baidu/mobads/sdk/api/RequestParameters;

    if-nez v4, :cond_4

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->v:Lcom/baidu/mobads/sdk/api/RequestParameters;

    .line 106
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    .line 105
    :goto_2
    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 109
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 111
    :goto_3
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->y:Lcom/baidu/mobads/sdk/internal/e$b;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 138
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 139
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 141
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cc;->y:Lcom/baidu/mobads/sdk/internal/e$b;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/sdk/internal/e$b;->a(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 3

    .line 267
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 268
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 270
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onAdUnionClick()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected b(Ljava/lang/String;Z)V
    .locals 3

    .line 302
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 303
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 304
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 305
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 306
    invoke-virtual {v1, p2}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onAdDownloadWindow(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 132
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cc;->w:Z

    return-void
.end method

.method protected c()V
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_0

    .line 261
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/e$a;->c()V

    :cond_0
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 3

    .line 279
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 280
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 281
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 282
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->onADPrivacyClick()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected c_()V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_0

    .line 254
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/e$a;->b()V

    :cond_0
    return-void
.end method

.method protected d(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 3

    .line 200
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 201
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 204
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/sdk/internal/e$a;->a(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public e()Lcom/baidu/mobads/sdk/api/RequestParameters;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->v:Lcom/baidu/mobads/sdk/api/RequestParameters;

    return-object v0
.end method

.method protected e(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 218
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    .line 219
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_2

    const-string v0, "instanceInfo"

    .line 220
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    .line 221
    :goto_0
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 222
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    if-eqz v2, :cond_1

    .line 223
    invoke-virtual {v2}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "showState"

    .line 224
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 225
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v4, v2, v3}, Lcom/baidu/mobads/sdk/internal/e$a;->a(Lcom/baidu/mobads/sdk/api/NativeResponse;I)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 3

    .line 233
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 235
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 236
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    .line 237
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/sdk/internal/e$a;->b(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected k()V
    .locals 11

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_6

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/baidu/mobads/sdk/internal/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/b;

    move-result-object v1

    .line 153
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 154
    :goto_0
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/b;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 155
    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/internal/b;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/baidu/mobads/sdk/internal/a;

    .line 156
    invoke-virtual {v5}, Lcom/baidu/mobads/sdk/internal/a;->m()Ljava/lang/String;

    move-result-object v6

    .line 160
    invoke-virtual {v5}, Lcom/baidu/mobads/sdk/internal/a;->p()I

    move-result v7

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-ne v7, v8, :cond_2

    if-eqz v6, :cond_1

    const-string v7, ""

    .line 161
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "null"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    .line 164
    :cond_0
    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cc;->g:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/baidu/mobads/sdk/internal/ba;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v6, 0x0

    goto :goto_4

    .line 167
    :cond_2
    invoke-virtual {v5}, Lcom/baidu/mobads/sdk/internal/a;->p()I

    move-result v7

    const/16 v10, 0x200

    if-ne v7, v10, :cond_3

    .line 168
    invoke-direct {p0, v5}, Lcom/baidu/mobads/sdk/internal/cc;->a(Lcom/baidu/mobads/sdk/internal/a;)I

    move-result v7

    if-ne v7, v8, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 169
    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cc;->g:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/baidu/mobads/sdk/internal/ba;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    :goto_2
    xor-int/2addr v6, v9

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    const/4 v9, 0x0

    :goto_4
    if-nez v9, :cond_4

    .line 173
    new-instance v7, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;

    iget-object v8, p0, Lcom/baidu/mobads/sdk/internal/cc;->g:Landroid/content/Context;

    invoke-direct {v7, v8, p0, v5}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;-><init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/internal/cc;Lcom/baidu/mobads/sdk/internal/a;)V

    .line 174
    invoke-virtual {v7, v6}, Lcom/baidu/mobads/sdk/api/XAdNativeResponse;->setIsDownloadApp(Z)V

    .line 175
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 178
    :cond_5
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->o:Ljava/util/List;

    .line 179
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/sdk/internal/e$a;->a(Ljava/util/List;)V

    :cond_6
    return-void
.end method

.method protected m()V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cc;->x:Lcom/baidu/mobads/sdk/internal/e$a;

    if-eqz v0, :cond_0

    .line 247
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/internal/e$a;->a()V

    :cond_0
    return-void
.end method
