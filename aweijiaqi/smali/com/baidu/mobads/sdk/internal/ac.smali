.class public Lcom/baidu/mobads/sdk/internal/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/ac$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "PluginLoader"

.field private static b:Ljava/lang/ClassLoader;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;
    .locals 2

    .line 45
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/ac;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p3

    .line 49
    :cond_0
    sget-object v0, Lcom/baidu/mobads/sdk/internal/ac;->b:Ljava/lang/ClassLoader;

    if-nez v0, :cond_1

    .line 51
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/c;->a()Lcom/baidu/mobads/sdk/internal/c;

    move-result-object v0

    const-string v1, "remote_adserv"

    .line 52
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/c;->a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/ad;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/sdk/internal/m;

    if-eqz v0, :cond_1

    .line 54
    invoke-interface {v0, p0, p1, p2, p3}, Lcom/baidu/mobads/sdk/internal/m;->getClassLoaderFromJar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;

    move-result-object p0

    sput-object p0, Lcom/baidu/mobads/sdk/internal/ac;->b:Ljava/lang/ClassLoader;

    .line 57
    :cond_1
    sget-object p0, Lcom/baidu/mobads/sdk/internal/ac;->b:Ljava/lang/ClassLoader;

    return-object p0
.end method

.method public static a(DLcom/baidu/mobads/sdk/internal/ab$b;Lcom/baidu/mobads/sdk/internal/ac$a;)V
    .locals 2

    .line 25
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/c;->a()Lcom/baidu/mobads/sdk/internal/c;

    move-result-object v0

    const-string v1, "remote_adserv"

    .line 26
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/c;->a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/internal/ad;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/sdk/internal/m;

    if-eqz v0, :cond_0

    .line 28
    invoke-interface {v0, p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/m;->startLoadRemotePhp(DLcom/baidu/mobads/sdk/internal/ab$b;)V

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 32
    invoke-interface {p3}, Lcom/baidu/mobads/sdk/internal/ac$a;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method public static a()Z
    .locals 1

    .line 39
    sget-object v0, Lcom/baidu/mobads/sdk/internal/bs;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
