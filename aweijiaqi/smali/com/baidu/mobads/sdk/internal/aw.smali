.class public final enum Lcom/baidu/mobads/sdk/internal/aw;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/sdk/internal/aw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobads/sdk/internal/aw;

.field public static final enum b:Lcom/baidu/mobads/sdk/internal/aw;

.field public static final c:Ljava/lang/String; = "msg"

.field private static final synthetic f:[Lcom/baidu/mobads/sdk/internal/aw;


# instance fields
.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 4
    new-instance v0, Lcom/baidu/mobads/sdk/internal/aw;

    const/4 v1, 0x0

    const-string v2, "INTERFACE_USE_PROBLEM"

    const v3, 0xf6951

    const-string v4, "\u63a5\u53e3\u4f7f\u7528\u95ee\u9898"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/baidu/mobads/sdk/internal/aw;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/sdk/internal/aw;->a:Lcom/baidu/mobads/sdk/internal/aw;

    .line 5
    new-instance v0, Lcom/baidu/mobads/sdk/internal/aw;

    const/4 v2, 0x1

    const-string v3, "SHOW_STANDARD_UNFIT"

    const v4, 0x2e6301

    const-string v5, "\u5bb9\u5668\u5927\u5c0f\u4e0d\u8fbe\u6807"

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/baidu/mobads/sdk/internal/aw;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/sdk/internal/aw;->b:Lcom/baidu/mobads/sdk/internal/aw;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/baidu/mobads/sdk/internal/aw;

    .line 3
    sget-object v4, Lcom/baidu/mobads/sdk/internal/aw;->a:Lcom/baidu/mobads/sdk/internal/aw;

    aput-object v4, v3, v1

    aput-object v0, v3, v2

    sput-object v3, Lcom/baidu/mobads/sdk/internal/aw;->f:[Lcom/baidu/mobads/sdk/internal/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput p3, p0, Lcom/baidu/mobads/sdk/internal/aw;->d:I

    .line 14
    iput-object p4, p0, Lcom/baidu/mobads/sdk/internal/aw;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/aw;
    .locals 1

    .line 3
    const-class v0, Lcom/baidu/mobads/sdk/internal/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/sdk/internal/aw;

    return-object p0
.end method

.method public static a()[Lcom/baidu/mobads/sdk/internal/aw;
    .locals 1

    .line 3
    sget-object v0, Lcom/baidu/mobads/sdk/internal/aw;->f:[Lcom/baidu/mobads/sdk/internal/aw;

    invoke-virtual {v0}, [Lcom/baidu/mobads/sdk/internal/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/sdk/internal/aw;

    return-object v0
.end method


# virtual methods
.method public b()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/aw;->d:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/aw;->e:Ljava/lang/String;

    return-object v0
.end method
