.class public Lcom/baidu/mobads/sdk/internal/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/sdk/internal/ab$a;,
        Lcom/baidu/mobads/sdk/internal/ab$c;,
        Lcom/baidu/mobads/sdk/internal/ab$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String; = "OAdURLConnection"

.field public static final b:Ljava/lang/String; = "POST"

.field public static final c:Ljava/lang/String; = "GET"

.field public static final d:Ljava/lang/String; = "application/json"

.field public static final e:Ljava/lang/String; = "text/plain"


# instance fields
.field private f:Ljava/net/HttpURLConnection;

.field private g:Lcom/baidu/mobads/sdk/internal/az;

.field private h:Lcom/baidu/mobads/sdk/internal/ab$b;

.field private i:Lcom/baidu/mobads/sdk/internal/ab$c;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:Z

.field private q:Landroid/net/Uri$Builder;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "GET"

    .line 53
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/sdk/internal/ab;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->g:Lcom/baidu/mobads/sdk/internal/az;

    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    .line 28
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    const-string v1, "text/plain"

    .line 42
    iput-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->m:Ljava/lang/String;

    const/16 v1, 0x2710

    .line 44
    iput v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->n:I

    .line 46
    iput v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->o:I

    const/4 v1, 0x0

    .line 48
    iput-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->p:Z

    .line 50
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->q:Landroid/net/Uri$Builder;

    .line 57
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->j:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/ab;->k:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 2

    .line 255
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12d

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "Location"

    .line 257
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 261
    :try_start_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getConnectTimeout()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/4 p1, 0x0

    .line 262
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const-string p1, "Range"

    const-string v1, "bytes=0-"

    .line 263
    invoke-virtual {v0, p1, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-object p1, v0

    :catch_1
    :cond_1
    return-object p1
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/ab;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/ab;->e()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/net/HttpURLConnection;)V
    .locals 4

    const/4 v0, 0x0

    .line 145
    :try_start_0
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 146
    :try_start_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    const-string v3, "UTF-8"

    invoke-direct {v2, p2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 148
    :try_start_2
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    if-eqz p2, :cond_0

    .line 155
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object p2, v0

    :goto_0
    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    :cond_1
    if-eqz p2, :cond_2

    .line 155
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    :cond_2
    throw p1
.end method

.method static synthetic b(Lcom/baidu/mobads/sdk/internal/ab;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/ab;->f()V

    return-void
.end method

.method private e()V
    .locals 6

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 99
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->j:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bw;->a()Lcom/baidu/mobads/sdk/internal/bw;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/internal/bw;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    .line 102
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->n:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 103
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    const-string v0, "http.keepAlive"

    const-string v1, "false"

    .line 104
    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    iget-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->p:Z

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const-string v1, "Content-type"

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const-string v1, "Connection"

    const-string v2, "keep-alive"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const-string v1, "Cache-Control"

    const-string v2, "no-cache"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->k:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->q:Landroid/net/Uri$Builder;

    if-eqz v0, :cond_3

    .line 123
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->q:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/sdk/internal/ab;->a(Ljava/lang/String;Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 130
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    const/4 v2, 0x0

    const-string v3, "Net Create RuntimeError: "

    if-eqz v1, :cond_2

    .line 131
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v2}, Lcom/baidu/mobads/sdk/internal/ab$b;->a(Ljava/lang/String;I)V

    .line 133
    :cond_2
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    if-eqz v1, :cond_3

    .line 134
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v2}, Lcom/baidu/mobads/sdk/internal/ab$c;->onFail(Ljava/lang/String;I)V

    :cond_3
    :goto_0
    return-void
.end method

.method private f()V
    .locals 6

    const/4 v0, 0x0

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 165
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->g:Lcom/baidu/mobads/sdk/internal/az;

    const-string v2, "OAdURLConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    .line 166
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " connect code :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-virtual {v1, v2, v3}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0x12e

    if-eq v1, v2, :cond_0

    const/16 v2, 0x12d

    if-ne v1, v2, :cond_1

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 172
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-direct {p0, v1}, Lcom/baidu/mobads/sdk/internal/ab;->a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    .line 174
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 177
    :cond_1
    div-int/lit8 v2, v1, 0x64

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    .line 179
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/sdk/internal/ab$b;->a(Ljava/lang/String;I)V

    .line 182
    :cond_2
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    if-eqz v2, :cond_5

    .line 183
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/sdk/internal/ab$c;->onFail(Ljava/lang/String;I)V

    goto :goto_0

    .line 187
    :cond_3
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/bw;->a()Lcom/baidu/mobads/sdk/internal/bw;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/sdk/internal/bw;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 188
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    if-eqz v2, :cond_4

    .line 190
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ab;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/sdk/internal/ab$b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_4
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    if-eqz v2, :cond_5

    .line 194
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/sdk/internal/ab$c;->onSuccess(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_8

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 199
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v3, "Net Connect RuntimeError: "

    if-eqz v2, :cond_6

    .line 200
    :try_start_2
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Lcom/baidu/mobads/sdk/internal/ab$b;->a(Ljava/lang/String;I)V

    .line 202
    :cond_6
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    if-eqz v2, :cond_7

    .line 203
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Lcom/baidu/mobads/sdk/internal/ab$c;->onFail(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206
    :cond_7
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_8

    .line 207
    :goto_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_8
    return-void

    .line 206
    :goto_2
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_9

    .line 207
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .line 63
    invoke-direct {p0}, Lcom/baidu/mobads/sdk/internal/ab;->e()V

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 67
    :try_start_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    div-int/lit8 v0, v0, 0x64
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    return-object v1

    .line 72
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/ab;->c()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_2

    .line 78
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    return-object v0

    :catchall_0
    nop

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_3

    .line 78
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    return-object v1
.end method

.method public a(I)V
    .locals 0

    .line 299
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->n:I

    return-void
.end method

.method public a(Landroid/net/Uri$Builder;)V
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->q:Landroid/net/Uri$Builder;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/internal/ab$b;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->h:Lcom/baidu/mobads/sdk/internal/ab$b;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/internal/ab$c;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->i:Lcom/baidu/mobads/sdk/internal/ab$c;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->m:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 241
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 242
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v2, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .line 87
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/am;->a()Lcom/baidu/mobads/sdk/internal/am;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/sdk/internal/ab$a;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/sdk/internal/ab$a;-><init>(Lcom/baidu/mobads/sdk/internal/ab;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/internal/am;->a(Lcom/baidu/mobads/sdk/internal/h;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public b(I)V
    .locals 0

    .line 304
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/ab;->o:I

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 219
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v2, 0x80

    new-array v2, v2, [B

    .line 222
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    .line 223
    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 225
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 226
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 229
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    return-object v1

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 232
    :cond_2
    throw v1
.end method

.method public d()V
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/ab;->f:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 278
    :try_start_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 283
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OAdURLConnection"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void
.end method
