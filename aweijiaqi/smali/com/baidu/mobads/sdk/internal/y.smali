.class Lcom/baidu/mobads/sdk/internal/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

.field final synthetic b:Lcom/baidu/mobads/sdk/internal/x;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/sdk/internal/x;Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;)V
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->b:Lcom/baidu/mobads/sdk/internal/x;

    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 317
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "invoke: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "NativeCPUAdData"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 321
    :cond_0
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "onAdDownloadWindowShow"

    .line 323
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 324
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onAdDownloadWindowShow()V

    goto :goto_0

    :cond_1
    const-string p2, "onPermissionShow"

    .line 325
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 326
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onPermissionShow()V

    goto :goto_0

    :cond_2
    const-string p2, "onPermissionClose"

    .line 327
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 328
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onPermissionClose()V

    goto :goto_0

    :cond_3
    const-string p2, "onPrivacyClick"

    .line 329
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 330
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onPrivacyClick()V

    goto :goto_0

    :cond_4
    const-string p2, "onPrivacyLpClose"

    .line 331
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 332
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onPrivacyLpClose()V

    goto :goto_0

    :cond_5
    const-string p2, "onNotifyPerformance"

    .line 333
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    if-eqz p3, :cond_6

    .line 334
    array-length p1, p3

    const/4 p2, 0x1

    if-lt p1, p2, :cond_6

    const/4 p1, 0x0

    aget-object p2, p3, p1

    instance-of p2, p2, Ljava/lang/String;

    if-eqz p2, :cond_6

    .line 335
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/y;->a:Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;

    aget-object p1, p3, p1

    check-cast p1, Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/baidu/mobads/sdk/api/IBasicCPUData$CpuNativeStatusCB;->onNotifyPerformance(Ljava/lang/String;)V

    :cond_6
    :goto_0
    return-object v0
.end method
