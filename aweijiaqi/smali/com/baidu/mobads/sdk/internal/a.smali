.class public Lcom/baidu/mobads/sdk/internal/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "none"

.field public static final b:Ljava/lang/String; = "text"

.field public static final c:Ljava/lang/String; = "static_image"

.field public static final d:Ljava/lang/String; = "gif"

.field public static final e:Ljava/lang/String; = "rich_media"

.field public static final f:Ljava/lang/String; = "html"

.field public static final g:Ljava/lang/String; = "hybrid"

.field public static final h:Ljava/lang/String; = "video"

.field private static final i:J = 0x1b7740L


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lorg/json/JSONObject;

.field private M:J

.field private N:J

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Lorg/json/JSONObject;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:Ljava/lang/String;

.field private q:J

.field private r:I

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 39
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/a;->s:I

    const-string v0, "none"

    .line 60
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/baidu/mobads/sdk/internal/a;
    .locals 8

    const/4 v0, 0x0

    if-eqz p0, :cond_a

    .line 80
    new-instance v1, Lcom/baidu/mobads/sdk/internal/a;

    invoke-direct {v1}, Lcom/baidu/mobads/sdk/internal/a;-><init>()V

    const-string v2, "remoteParams"

    .line 81
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    const-string v2, "tit"

    .line 82
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->j:Ljava/lang/String;

    const-string v2, "desc"

    .line 83
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->k:Ljava/lang/String;

    const-string v2, "icon"

    .line 84
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->l:Ljava/lang/String;

    const-string v2, "w_picurl"

    .line 85
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->m:Ljava/lang/String;

    const-string v2, "w"

    .line 86
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/baidu/mobads/sdk/internal/a;->n:I

    const-string v2, "h"

    .line 87
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/baidu/mobads/sdk/internal/a;->o:I

    const-string v2, "appname"

    .line 89
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/baidu/mobads/sdk/internal/a;->p:Ljava/lang/String;

    const-string v2, "adLogo"

    .line 90
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/baidu/mobads/sdk/internal/a;->P:Ljava/lang/String;

    const-string v3, "baiduLogo"

    .line 91
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->Q:Ljava/lang/String;

    :try_start_0
    const-string v4, "control_flags"

    .line 94
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 95
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 96
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "innovate"

    .line 97
    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 98
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 99
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "gjico"

    .line 100
    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->R:Ljava/lang/String;

    const-string v4, "gjtxt"

    .line 101
    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->S:Ljava/lang/String;

    const-string v4, "gjurl"

    .line 102
    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->T:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    .line 108
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_0
    move-exception v4

    .line 106
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    :goto_0
    const-string v4, "btn"

    .line 111
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->U:Ljava/lang/String;

    const-string v4, "monitors"

    .line 112
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    iput-object v4, v1, Lcom/baidu/mobads/sdk/internal/a;->V:Lorg/json/JSONObject;

    const-string v4, "sz"

    .line 113
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/baidu/mobads/sdk/internal/a;->q:J

    const/4 v4, 0x0

    const-string v5, "auto_play"

    .line 114
    invoke-virtual {p0, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v1, Lcom/baidu/mobads/sdk/internal/a;->r:I

    const/4 v5, 0x1

    const-string v6, "auto_play_non_wifi"

    .line 115
    invoke-virtual {p0, v6, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v1, Lcom/baidu/mobads/sdk/internal/a;->s:I

    const-string v5, "pk"

    .line 116
    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/baidu/mobads/sdk/internal/a;->u:Ljava/lang/String;

    const-string v5, "act"

    .line 117
    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/baidu/mobads/sdk/internal/a;->B:I

    const-string v5, ""

    const-string v6, "apo"

    .line 118
    invoke-virtual {p0, v6, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/baidu/mobads/sdk/internal/a;->C:Ljava/lang/String;

    const-string v6, "bidlayer"

    .line 119
    invoke-virtual {p0, v6, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/baidu/mobads/sdk/internal/a;->t:Ljava/lang/String;

    const-string v6, "container_width"

    .line 121
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/baidu/mobads/sdk/internal/a;->D:I

    const-string v6, "container_height"

    .line 122
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/baidu/mobads/sdk/internal/a;->E:I

    const-string v6, "size_type"

    .line 123
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/baidu/mobads/sdk/internal/a;->F:I

    const-string v6, "style_type"

    .line 124
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/baidu/mobads/sdk/internal/a;->G:I

    const-string v6, "vurl"

    .line 126
    invoke-virtual {p0, v6, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/baidu/mobads/sdk/internal/a;->v:Ljava/lang/String;

    const-string v6, "duration"

    .line 127
    invoke-virtual {p0, v6, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v1, Lcom/baidu/mobads/sdk/internal/a;->H:I

    const-string v6, "type"

    .line 129
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    const-string v6, "html"

    .line 130
    invoke-virtual {p0, v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->w:Ljava/lang/String;

    const-string v0, "app_version"

    .line 132
    invoke-virtual {p0, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->y:Ljava/lang/String;

    const-string v0, "publisher"

    .line 133
    invoke-virtual {p0, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->x:Ljava/lang/String;

    const-string v0, "permission_link"

    .line 134
    invoke-virtual {p0, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->A:Ljava/lang/String;

    const-string v0, "privacy_link"

    .line 135
    invoke-virtual {p0, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->z:Ljava/lang/String;

    :try_start_1
    const-string v0, "morepics"

    .line 138
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 139
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->K:Ljava/util/List;

    .line 141
    :goto_1
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 142
    iget-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->K:Ljava/util/List;

    invoke-virtual {p0, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_1
    nop

    .line 149
    :cond_1
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->w:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 150
    iput-object v6, v1, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    goto/16 :goto_3

    .line 151
    :cond_2
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    if-eqz p0, :cond_8

    const-string v0, "text"

    .line 152
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 153
    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    goto :goto_3

    .line 154
    :cond_3
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    const-string v0, "image"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 155
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->m:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_8

    .line 156
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->m:Ljava/lang/String;

    .line 157
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 156
    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x2e

    .line 157
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p0

    if-ltz p0, :cond_4

    .line 160
    iget-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->m:Ljava/lang/String;

    .line 161
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 160
    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :cond_4
    move-object p0, v5

    :goto_2
    const-string v0, ".gif"

    .line 163
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_5

    const-string p0, "gif"

    .line 164
    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    goto :goto_3

    :cond_5
    const-string p0, "static_image"

    .line 166
    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    goto :goto_3

    .line 169
    :cond_6
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    const-string v0, "rm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    const-string p0, "rich_media"

    .line 170
    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    goto :goto_3

    .line 171
    :cond_7
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->I:Ljava/lang/String;

    const-string v0, "video"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 172
    iput-object v0, v1, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    .line 175
    :cond_8
    :goto_3
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    if-eqz p0, :cond_9

    const-wide/16 v6, 0x0

    const-string v0, "createTime"

    .line 176
    invoke-virtual {p0, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, v1, Lcom/baidu/mobads/sdk/internal/a;->M:J

    .line 177
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    const-wide/32 v6, 0x1b7740

    const-string v0, "expireTime"

    invoke-virtual {p0, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, v1, Lcom/baidu/mobads/sdk/internal/a;->N:J

    .line 178
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    sget-object v0, Lcom/baidu/mobads/sdk/internal/p;->ao:Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->P:Ljava/lang/String;

    .line 179
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    sget-object v0, Lcom/baidu/mobads/sdk/internal/p;->ap:Ljava/lang/String;

    invoke-virtual {p0, v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->Q:Ljava/lang/String;

    .line 180
    iget-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    const-string v0, "uniqueId"

    invoke-virtual {p0, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/baidu/mobads/sdk/internal/a;->O:Ljava/lang/String;

    :cond_9
    return-object v1

    :cond_a
    return-object v0
.end method

.method public static a(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/sdk/internal/a;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    const/4 v1, 0x0

    .line 190
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 192
    :try_start_0
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/baidu/mobads/sdk/internal/a;->a(Lorg/json/JSONObject;)Lcom/baidu/mobads/sdk/internal/a;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 194
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->x:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->z:Ljava/lang/String;

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->A:Ljava/lang/String;

    return-object v0
.end method

.method public D()J
    .locals 2

    .line 318
    iget-wide v0, p0, Lcom/baidu/mobads/sdk/internal/a;->N:J

    return-wide v0
.end method

.method public E()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->K:Ljava/util/List;

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->O:Ljava/lang/String;

    return-object v0
.end method

.method public G()Lorg/json/JSONObject;
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->L:Lorg/json/JSONObject;

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->R:Ljava/lang/String;

    return-object v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->S:Ljava/lang/String;

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->T:Ljava/lang/String;

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 2

    .line 355
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->U:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    const-string v0, ""

    return-object v0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->U:Ljava/lang/String;

    return-object v0
.end method

.method public L()Lorg/json/JSONObject;
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->V:Lorg/json/JSONObject;

    return-object v0
.end method

.method public M()Lorg/json/JSONObject;
    .locals 3

    .line 370
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "uniqueId"

    .line 371
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/a;->O:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "tit"

    .line 372
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "desc"

    .line 373
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "pk"

    .line 374
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/a;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "appname"

    .line 375
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/a;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "act"

    .line 376
    iget v2, p0, Lcom/baidu/mobads/sdk/internal/a;->B:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->m:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .line 218
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->n:I

    return v0
.end method

.method public f()I
    .locals 1

    .line 222
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->o:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->P:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public j()J
    .locals 2

    .line 238
    iget-wide v0, p0, Lcom/baidu/mobads/sdk/internal/a;->q:J

    return-wide v0
.end method

.method public k()I
    .locals 1

    .line 242
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->r:I

    return v0
.end method

.method public l()I
    .locals 1

    .line 246
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->s:I

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->u:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->v:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->w:Ljava/lang/String;

    return-object v0
.end method

.method public p()I
    .locals 1

    .line 262
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->B:I

    return v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->C:Ljava/lang/String;

    return-object v0
.end method

.method public r()I
    .locals 1

    .line 270
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->D:I

    return v0
.end method

.method public s()I
    .locals 1

    .line 274
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->E:I

    return v0
.end method

.method public t()I
    .locals 1

    .line 278
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->F:I

    return v0
.end method

.method public u()I
    .locals 1

    .line 282
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->G:I

    return v0
.end method

.method public v()I
    .locals 1

    .line 286
    iget v0, p0, Lcom/baidu/mobads/sdk/internal/a;->H:I

    return v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->J:Ljava/lang/String;

    return-object v0
.end method

.method public x()J
    .locals 2

    .line 294
    iget-wide v0, p0, Lcom/baidu/mobads/sdk/internal/a;->M:J

    return-wide v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->t:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/a;->y:Ljava/lang/String;

    return-object v0
.end method
