.class public Lcom/baidu/mobads/sdk/internal/cm;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"


# static fields
.field private static B:I

.field private static G:Lcom/baidu/mobads/sdk/internal/cm;


# instance fields
.field private A:Z

.field private C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

.field private D:Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;

.field private E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

.field private F:Lcom/baidu/mobads/sdk/api/RequestParameters;

.field private H:Lcom/baidu/mobads/sdk/internal/a;

.field private I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field private s:Landroid/widget/RelativeLayout;

.field private t:Ljava/lang/String;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;IIIIZZZ)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 46
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->p:Z

    .line 47
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->q:Z

    .line 48
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->r:Z

    .line 61
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cm;->s:Landroid/widget/RelativeLayout;

    .line 62
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/cm;->t:Ljava/lang/String;

    .line 63
    iput p4, p0, Lcom/baidu/mobads/sdk/internal/cm;->u:I

    .line 64
    iput p5, p0, Lcom/baidu/mobads/sdk/internal/cm;->v:I

    .line 65
    iput p6, p0, Lcom/baidu/mobads/sdk/internal/cm;->w:I

    .line 66
    iput p7, p0, Lcom/baidu/mobads/sdk/internal/cm;->x:I

    .line 67
    iput-boolean p8, p0, Lcom/baidu/mobads/sdk/internal/cm;->y:Z

    .line 68
    iput-boolean p9, p0, Lcom/baidu/mobads/sdk/internal/cm;->z:Z

    .line 69
    iput-boolean p10, p0, Lcom/baidu/mobads/sdk/internal/cm;->A:Z

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/sdk/internal/cm;)Lcom/baidu/mobads/sdk/internal/cm;
    .locals 0

    .line 31
    sput-object p0, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    return-object p0
.end method

.method public static a(I)V
    .locals 0

    .line 139
    sput p0, Lcom/baidu/mobads/sdk/internal/cm;->B:I

    return-void
.end method

.method public static a(Landroid/app/Activity;Lorg/json/JSONObject;Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V
    .locals 4

    .line 194
    sget-object v0, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {v0, p2}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V

    .line 196
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 197
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "event_type"

    const-string v3, "splash_focus_register_transition"

    .line 199
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "splash_focus_params"

    .line 200
    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "splash_focus_activity"

    .line 201
    invoke-interface {p2, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object p0, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    invoke-virtual {p0, v0, p2}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lorg/json/JSONObject;Ljava/util/Map;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 206
    :try_start_1
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/sdk/internal/az;->c(Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_0
    move-exception p0

    .line 204
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/sdk/internal/az;->c(Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 208
    :goto_0
    sput-object v1, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    goto :goto_1

    :catchall_1
    move-exception p0

    sput-object v1, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    throw p0

    :cond_0
    :goto_1
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 269
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->r:Z

    .line 270
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    const-string v1, "\u5e7f\u544a\u65e0\u586b\u5145"

    .line 271
    invoke-interface {v0, v1}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    .line 273
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(Landroid/content/Intent;Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;)V
    .locals 3

    .line 150
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->p:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->r:Z

    if-nez v0, :cond_0

    .line 151
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cm;->D:Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;

    .line 152
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    const-string v1, "event_type"

    const-string v2, "splash_focus_start_activity"

    .line 155
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "splash_focus_user_intent"

    .line 156
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 158
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/az;->a()Lcom/baidu/mobads/sdk/internal/az;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobads/sdk/internal/az;->a(Ljava/lang/Throwable;)I

    .line 161
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->removeAllListeners()V

    .line 163
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->V:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cm;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {p1, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 164
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->E:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cm;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {p1, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 165
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->T:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cm;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {p1, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 166
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    sget-object v1, Lcom/baidu/mobads/sdk/internal/p;->U:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cm;->i:Lcom/baidu/mobads/sdk/api/IOAdEventListener;

    invoke-interface {p1, v1, v2}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/sdk/api/IOAdEventListener;)V

    .line 167
    invoke-virtual {p0, p2, v0}, Lcom/baidu/mobads/sdk/internal/cm;->a(Lorg/json/JSONObject;Ljava/util/Map;)V

    const/4 p1, 0x0

    .line 169
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    .line 170
    sput-object p0, Lcom/baidu/mobads/sdk/internal/cm;->G:Lcom/baidu/mobads/sdk/internal/cm;

    .line 172
    invoke-static {}, Lcom/baidu/mobads/sdk/internal/am;->a()Lcom/baidu/mobads/sdk/internal/am;

    move-result-object p1

    new-instance p2, Lcom/baidu/mobads/sdk/internal/cn;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/sdk/internal/cn;-><init>(Lcom/baidu/mobads/sdk/internal/cm;)V

    const-wide/16 v0, 0x3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/baidu/mobads/sdk/internal/am;->a(Lcom/baidu/mobads/sdk/internal/h;JLjava/util/concurrent/TimeUnit;)V

    goto :goto_1

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->g:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    if-eqz p2, :cond_1

    .line 183
    invoke-interface {p2}, Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;->onFinishActivity()V

    goto :goto_1

    .line 185
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->g:Landroid/content/Context;

    instance-of p1, p1, Landroid/app/Activity;

    if-eqz p1, :cond_2

    .line 186
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->g:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_2
    :goto_1
    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 231
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/b;

    move-result-object p1

    .line 232
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/b;->a()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 233
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 234
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/sdk/internal/a;

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->H:Lcom/baidu/mobads/sdk/internal/a;

    .line 237
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz p1, :cond_1

    .line 238
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onADLoaded()V

    :cond_1
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/RequestParameters;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->F:Lcom/baidu/mobads/sdk/api/RequestParameters;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;)V
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/SplashAdListener;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x1

    .line 278
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->r:Z

    .line 279
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    .line 280
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    .line 282
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/sdk/internal/ar;->a(Ljava/lang/String;I)V

    return-void
.end method

.method protected a(Ljava/lang/String;Z)V
    .locals 1

    .line 308
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz p1, :cond_1

    .line 309
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 311
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->onADPermissionShow()V

    goto :goto_0

    .line 313
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->onADPermissionClose()V

    :cond_1
    :goto_0
    return-void
.end method

.method public a_()V
    .locals 8

    const-string v0, "rsplash"

    const-string v1, "prod"

    const-string v2, ""

    .line 85
    iget-object v3, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v3, :cond_0

    const/4 v0, 0x0

    .line 86
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->k:Z

    return-void

    :cond_0
    const/4 v3, 0x1

    .line 89
    iput-boolean v3, p0, Lcom/baidu/mobads/sdk/internal/cm;->k:Z

    .line 90
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 91
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 93
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 94
    invoke-virtual {v6, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v7, v6}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 96
    iget-object v6, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    iget-object v7, p0, Lcom/baidu/mobads/sdk/internal/cm;->s:Landroid/widget/RelativeLayout;

    invoke-interface {v6, v7}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->setAdContainer(Landroid/widget/RelativeLayout;)V

    .line 97
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cm;->h()V

    .line 98
    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "apid"

    .line 99
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cm;->t:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "fet"

    const-string v1, "ANTI,HTML,MSSP,VIDEO,RSPLASHHTML"

    .line 100
    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "n"

    const-string v1, "1"

    .line 101
    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const/16 v0, 0x1a

    const-string v1, "at"

    .line 105
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "mimetype"

    const-string v1, "video/mp4,image/jpg,image/gif,image/png"

    .line 106
    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "w"

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/baidu/mobads/sdk/internal/cm;->u:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "h"

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/baidu/mobads/sdk/internal/cm;->v:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 110
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "appid"

    .line 111
    iget-object v1, p0, Lcom/baidu/mobads/sdk/internal/cm;->n:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    const-string v0, "timeout"

    .line 114
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/cm;->x:I

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "splashTipStyle"

    .line 115
    iget v1, p0, Lcom/baidu/mobads/sdk/internal/cm;->w:I

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "bitmapDisplayMode"

    .line 116
    sget v1, Lcom/baidu/mobads/sdk/internal/cm;->B:I

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "countDownNew"

    const-string v1, "true"

    .line 118
    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "Display_Down_Info"

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lcom/baidu/mobads/sdk/internal/cm;->y:Z

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "popDialogIfDl"

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lcom/baidu/mobads/sdk/internal/cm;->z:Z

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "limitRegionClick"

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/baidu/mobads/sdk/internal/cm;->A:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "needCache"

    .line 122
    invoke-virtual {v5, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "onlyLoadAd"

    .line 123
    iget-boolean v1, p0, Lcom/baidu/mobads/sdk/internal/cm;->o:Z

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "cacheVideoOnlyWifi"

    .line 124
    invoke-virtual {v5, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->F:Lcom/baidu/mobads/sdk/api/RequestParameters;

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->F:Lcom/baidu/mobads/sdk/api/RequestParameters;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/sdk/internal/cm;->a(Ljava/util/Map;)V

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->l:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/sdk/internal/cm;->a(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    .line 131
    invoke-static {v5, v0}, Lcom/baidu/mobads/sdk/internal/j;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 133
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v4, v5}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method protected b(Ljava/lang/String;Z)V
    .locals 1

    .line 320
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz p1, :cond_1

    .line 321
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 323
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->adDownloadWindowShow()V

    goto :goto_0

    .line 325
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->adDownloadWindowClose()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 1

    .line 340
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz p1, :cond_0

    .line 341
    instance-of v0, p1, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz v0, :cond_0

    .line 342
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->onADPrivacyLpShow()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->showAd()V

    :cond_0
    return-void
.end method

.method protected f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 251
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->q:Z

    if-nez v0, :cond_1

    .line 253
    invoke-super {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;->f(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V

    .line 254
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    .line 255
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdDismissed()V

    :cond_0
    const/4 v0, 0x1

    .line 257
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->q:Z

    .line 260
    :cond_1
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    .line 262
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

    if-eqz v0, :cond_2

    const-string v0, "splash_close_reason"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 263
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;->onAdClose()V

    :cond_2
    return-void
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 0

    const/4 p1, 0x1

    .line 219
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->p:Z

    .line 220
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz p1, :cond_0

    .line 221
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdClick()V

    .line 223
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cm;->E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

    if-eqz p1, :cond_1

    .line 224
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;->onAdClick()V

    :cond_1
    return-void
.end method

.method protected k()V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    .line 245
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAdListener;->onAdPresent()V

    :cond_0
    return-void
.end method

.method protected m()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->C:Lcom/baidu/mobads/sdk/api/SplashAdListener;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/baidu/mobads/sdk/api/SplashLpCloseListener;

    if-eqz v1, :cond_0

    .line 288
    check-cast v0, Lcom/baidu/mobads/sdk/api/SplashLpCloseListener;

    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashLpCloseListener;->onLpClosed()V

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->E:Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;

    if-eqz v0, :cond_1

    .line 291
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashFocusAdListener;->onLpClosed()V

    .line 293
    :cond_1
    invoke-super {p0}, Lcom/baidu/mobads/sdk/internal/ar;->m()V

    return-void
.end method

.method protected o()V
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->D:Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;

    if-eqz v0, :cond_0

    .line 299
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAd$OnFinishListener;->onFinishActivity()V

    goto :goto_0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->g:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->g:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 303
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/baidu/mobads/sdk/internal/ar;->o()V

    return-void
.end method

.method protected p()V
    .locals 2

    .line 332
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->I:Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz v0, :cond_0

    .line 333
    instance-of v1, v0, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;

    if-eqz v1, :cond_0

    .line 334
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/SplashAd$SplashAdDownloadDialogListener;->onADPrivacyLpClose()V

    :cond_0
    return-void
.end method

.method public r()Lcom/baidu/mobads/sdk/internal/a;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cm;->H:Lcom/baidu/mobads/sdk/internal/a;

    return-object v0
.end method
