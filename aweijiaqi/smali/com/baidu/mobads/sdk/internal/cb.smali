.class public Lcom/baidu/mobads/sdk/internal/cb;
.super Lcom/baidu/mobads/sdk/internal/ar;
.source "SourceFile"


# instance fields
.field private A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

.field o:Lcom/baidu/mobads/sdk/internal/a;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;

.field private u:I

.field private v:I

.field private w:I

.field private x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;)V
    .locals 1

    .line 39
    invoke-direct {p0, p1}, Lcom/baidu/mobads/sdk/internal/ar;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 26
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->u:I

    .line 27
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->v:I

    .line 28
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->w:I

    const/4 p1, 0x0

    .line 30
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->y:Z

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->o:Lcom/baidu/mobads/sdk/internal/a;

    .line 35
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->z:Z

    .line 40
    iput-object p3, p0, Lcom/baidu/mobads/sdk/internal/cb;->t:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;

    .line 41
    invoke-virtual {p3}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getApId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->q:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/baidu/mobads/sdk/internal/cb;->p:Ljava/lang/String;

    .line 43
    invoke-virtual {p3}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->getAdView()Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->f:Landroid/widget/RelativeLayout;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .line 72
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->u:I

    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cb;->l()V

    .line 160
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 161
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz p1, :cond_0

    .line 162
    invoke-interface {p1, p2}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdFail(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    return-void
.end method

.method protected a(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    const/4 v0, 0x1

    .line 149
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->z:Z

    .line 150
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/sdk/internal/b;->a(Ljava/lang/String;)Lcom/baidu/mobads/sdk/internal/b;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/internal/b;->a()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/sdk/internal/a;

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->t:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setAdResponse(Lcom/baidu/mobads/sdk/internal/a;)V

    .line 153
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz p1, :cond_0

    .line 154
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdDataLoaded()V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/sdk/api/RequestParameters;)V
    .locals 1

    .line 51
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getWidth()I

    move-result v0

    .line 52
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/RequestParameters;->getHeight()I

    move-result p1

    if-lez v0, :cond_0

    if-lez p1, :cond_0

    .line 54
    iput v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->r:I

    .line 55
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->s:I

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 1

    .line 133
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 134
    iget-object p2, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz p2, :cond_0

    .line 135
    invoke-interface {p2, p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdFail(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a_()V
    .locals 6

    const-string v0, "prod"

    const-string v1, ""

    .line 87
    iget-object v2, p0, Lcom/baidu/mobads/sdk/internal/cb;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 88
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->k:Z

    return-void

    :cond_0
    const/4 v2, 0x1

    .line 91
    iput-boolean v2, p0, Lcom/baidu/mobads/sdk/internal/cb;->k:Z

    .line 93
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 94
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 96
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 97
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cb;->p:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cb;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v5, v4}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->createProdHandler(Lorg/json/JSONObject;)V

    .line 99
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cb;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    iget-object v5, p0, Lcom/baidu/mobads/sdk/internal/cb;->f:Landroid/widget/RelativeLayout;

    invoke-interface {v4, v5}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->setAdContainer(Landroid/widget/RelativeLayout;)V

    .line 100
    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/internal/cb;->h()V

    .line 101
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cb;->p:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "apid"

    .line 102
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cb;->q:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "n"

    const-string v4, "1"

    .line 103
    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "appid"

    .line 105
    iget-object v4, p0, Lcom/baidu/mobads/sdk/internal/cb;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    const/4 v0, 0x2

    const-string v4, "at"

    .line 108
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "w"

    .line 109
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/baidu/mobads/sdk/internal/cb;->r:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "h"

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/baidu/mobads/sdk/internal/cb;->s:I

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->l:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/sdk/internal/cb;->a(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    .line 113
    invoke-static {v2, v0}, Lcom/baidu/mobads/sdk/internal/j;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 117
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->j:Lcom/baidu/mobads/sdk/api/IAdInterListener;

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/sdk/api/IAdInterListener;->loadAd(Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 64
    iput-boolean p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->y:Z

    return-void
.end method

.method public c(I)V
    .locals 0

    .line 76
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->v:I

    return-void
.end method

.method public d(I)V
    .locals 0

    .line 82
    iput p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->w:I

    return-void
.end method

.method protected d(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 167
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setWinSended(Z)V

    return-void
.end method

.method public e()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->y:Z

    return v0
.end method

.method protected g(Lcom/baidu/mobads/sdk/api/IOAdEvent;)V
    .locals 1

    .line 141
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setClicked(Z)V

    .line 142
    iget-object p1, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz p1, :cond_0

    .line 143
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdClick()V

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 2

    const/4 v0, 0x1

    .line 124
    iput-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->y:Z

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->A:Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/api/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 126
    iget-object v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->x:Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    if-eqz v0, :cond_0

    .line 127
    invoke-interface {v0}, Lcom/baidu/mobads/sdk/api/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdShow()V

    :cond_0
    return-void
.end method

.method public r()Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/baidu/mobads/sdk/internal/cb;->z:Z

    return v0
.end method
