.class public final Lcom/anythink/basead/a/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/a/b$a;
    }
.end annotation


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1


# instance fields
.field c:Lcom/anythink/core/common/d/h;

.field d:Z

.field e:Z

.field f:Landroid/content/Context;

.field g:Z

.field h:Lcom/anythink/core/common/d/i;

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V
    .locals 4

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/a/b;->i:Ljava/lang/String;

    const/4 v0, 0x0

    .line 45
    iput v0, p0, Lcom/anythink/basead/a/b;->j:I

    const/4 v1, 0x1

    .line 46
    iput v1, p0, Lcom/anythink/basead/a/b;->k:I

    const/4 v2, 0x2

    .line 47
    iput v2, p0, Lcom/anythink/basead/a/b;->l:I

    const/16 v3, 0xa

    .line 49
    iput v3, p0, Lcom/anythink/basead/a/b;->m:I

    .line 50
    iput v1, p0, Lcom/anythink/basead/a/b;->n:I

    .line 51
    iput v2, p0, Lcom/anythink/basead/a/b;->o:I

    const/4 v2, 0x3

    .line 52
    iput v2, p0, Lcom/anythink/basead/a/b;->p:I

    const/4 v2, 0x4

    .line 53
    iput v2, p0, Lcom/anythink/basead/a/b;->q:I

    .line 67
    iput-object p3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    .line 68
    iput-object p2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    .line 70
    iget-object p1, p2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    .line 1170
    instance-of p2, p3, Lcom/anythink/core/common/d/f;

    if-eqz p2, :cond_0

    .line 1171
    instance-of p2, p1, Lcom/anythink/core/common/d/v;

    if-eqz p2, :cond_1

    .line 1172
    check-cast p1, Lcom/anythink/core/common/d/v;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->u()I

    move-result p1

    if-ne p1, v1, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    .line 1174
    :cond_0
    instance-of p1, p3, Lcom/anythink/core/common/d/p;

    if-eqz p1, :cond_1

    .line 1175
    check-cast p3, Lcom/anythink/core/common/d/p;

    invoke-virtual {p3}, Lcom/anythink/core/common/d/p;->y()I

    move-result p1

    if-ne p1, v1, :cond_1

    goto :goto_0

    .line 70
    :cond_1
    :goto_1
    iput-boolean v0, p0, Lcom/anythink/basead/a/b;->g:Z

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    const/4 v0, 0x0

    move-object v2, p1

    const/4 v1, 0x0

    :goto_0
    const/16 v3, 0xa

    const-string v4, ""

    if-ge v1, v3, :cond_9

    const/4 v3, 0x0

    .line 344
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 346
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v3, "GET"

    .line 347
    invoke-virtual {v5, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 349
    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const/16 v3, 0x7530

    .line 351
    invoke-virtual {v5, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 352
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 353
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v6, 0x12e

    if-ne v3, v6, :cond_3

    const-string v6, "Location"

    .line 355
    invoke-virtual {v5, v6}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-static {v2}, Lcom/anythink/basead/a/g;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ".apk"

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "http"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    .line 360
    :cond_0
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_1

    .line 377
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v0, 0x1

    :cond_3
    if-nez v0, :cond_6

    const/16 v0, 0xc8

    if-ne v3, v0, :cond_4

    goto :goto_2

    .line 370
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v6, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->b()I

    move-result v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    move-object v9, p1

    move-object v10, v2

    invoke-static/range {v6 .. v12}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v5, :cond_5

    .line 377
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    return-object v4

    :cond_6
    :goto_2
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    return-object v2

    :catchall_0
    move-exception p1

    move-object v3, v5

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v9, v2

    move-object v3, v5

    goto :goto_3

    :catchall_1
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v9, v2

    .line 373
    :goto_3
    :try_start_3
    iget-object v1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v5, v1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v7

    const-string v10, ""

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    move-object v8, p1

    invoke-static/range {v5 .. v11}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_9

    .line 377
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_5

    :goto_4
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 379
    :cond_8
    throw p1

    :cond_9
    :goto_5
    return-object v4
.end method

.method private a(ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V
    .locals 6

    .line 205
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    move-object v2, v1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    :goto_1
    const-string v3, "\\{req_id\\}"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, p2, v2, v3}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/h;J)Ljava/lang/String;

    move-result-object v0

    .line 221
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v2

    const/4 v3, 0x4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    .line 222
    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->s()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 223
    new-instance v2, Lcom/anythink/basead/c/e;

    invoke-direct {v2, v1, v1, v1}, Lcom/anythink/basead/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 225
    :cond_2
    new-instance v2, Lcom/anythink/basead/c/e;

    invoke-direct {v2, v0, v1, v1}, Lcom/anythink/basead/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :goto_2
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 234
    iget-object v4, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->s()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_a

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    const/4 v5, 0x3

    if-eq v4, v5, :cond_7

    if-eq v4, v3, :cond_4

    .line 297
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 298
    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 300
    :cond_3
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    goto/16 :goto_3

    .line 257
    :cond_4
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 258
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 259
    iget-object v1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v3, v0}, Lcom/anythink/basead/e/b/a/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;)Lcom/anythink/basead/c/e;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 261
    iget-object v1, v0, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    iput-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 262
    iget-object v0, v0, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    iput-object v0, v2, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    .line 267
    :cond_5
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 268
    iput-object v0, v2, Lcom/anythink/basead/c/e;->b:Ljava/lang/String;

    .line 269
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 271
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 272
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 274
    :cond_6
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    .line 238
    :cond_7
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    .line 239
    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 240
    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-static {v1}, Lcom/anythink/basead/e/b/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    iput-object v1, v2, Lcom/anythink/basead/c/e;->b:Ljava/lang/String;

    .line 244
    iput-object v0, v2, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    .line 245
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 249
    :cond_8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 250
    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 253
    :cond_9
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    :cond_a
    const-string v1, "http"

    .line 278
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 279
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    .line 283
    :cond_b
    iget-boolean v1, p0, Lcom/anythink/basead/a/b;->g:Z

    if-eqz v1, :cond_c

    .line 284
    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->n()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    const/4 v5, 0x0

    .line 288
    :cond_c
    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v5, :cond_e

    .line 290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 291
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 293
    :cond_d
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    :cond_e
    :goto_3
    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/a/b;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V
    .locals 6

    .line 2205
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    move-object v2, v1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    :goto_1
    const-string v3, "\\{req_id\\}"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, p2, v2, v3}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/h;J)Ljava/lang/String;

    move-result-object v0

    .line 2221
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v2

    const/4 v3, 0x4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    .line 2222
    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->s()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 2223
    new-instance v2, Lcom/anythink/basead/c/e;

    invoke-direct {v2, v1, v1, v1}, Lcom/anythink/basead/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2225
    :cond_2
    new-instance v2, Lcom/anythink/basead/c/e;

    invoke-direct {v2, v0, v1, v1}, Lcom/anythink/basead/c/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2228
    :goto_2
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 2234
    iget-object v4, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->s()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_a

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    const/4 v5, 0x3

    if-eq v4, v5, :cond_7

    if-eq v4, v3, :cond_4

    .line 2297
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2298
    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2300
    :cond_3
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    goto/16 :goto_3

    .line 2257
    :cond_4
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2258
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2259
    iget-object v1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v3, v0}, Lcom/anythink/basead/e/b/a/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;)Lcom/anythink/basead/c/e;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2261
    iget-object v1, v0, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    iput-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2262
    iget-object v0, v0, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    iput-object v0, v2, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    .line 2267
    :cond_5
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2268
    iput-object v0, v2, Lcom/anythink/basead/c/e;->b:Ljava/lang/String;

    .line 2269
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 2271
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2272
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2274
    :cond_6
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    .line 2238
    :cond_7
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->c()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    .line 2239
    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 2240
    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2242
    invoke-static {v1}, Lcom/anythink/basead/e/b/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2243
    iput-object v1, v2, Lcom/anythink/basead/c/e;->b:Ljava/lang/String;

    .line 2244
    iput-object v0, v2, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    .line 2245
    invoke-direct {p0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/e;)V

    .line 2249
    :cond_8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2250
    iget-object v1, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2253
    :cond_9
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    :cond_a
    const-string v1, "http"

    .line 2278
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2279
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    return-void

    .line 2283
    :cond_b
    iget-boolean v1, p0, Lcom/anythink/basead/a/b;->g:Z

    if-eqz v1, :cond_c

    .line 2284
    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->n()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    const/4 v5, 0x0

    .line 2288
    :cond_c
    invoke-direct {p0, v0}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v5, :cond_e

    .line 2290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2291
    iget-object v0, v2, Lcom/anythink/basead/c/e;->a:Ljava/lang/String;

    .line 2293
    :cond_d
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/basead/a/b;->a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    :cond_e
    :goto_3
    return-void
.end method

.method private a(Lcom/anythink/basead/c/e;)V
    .locals 3

    .line 440
    invoke-static {}, Lcom/anythink/basead/a/c;->a()Lcom/anythink/basead/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v1

    iget-object v2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/anythink/basead/a/c;->a(ILjava/lang/String;Lcom/anythink/basead/c/e;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V
    .locals 4

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    return-void

    .line 462
    :cond_0
    iget-boolean v1, p0, Lcom/anythink/basead/a/b;->e:Z

    const/16 v2, 0x9

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 463
    iput-boolean v3, p0, Lcom/anythink/basead/a/b;->d:Z

    .line 464
    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    if-eqz p4, :cond_2

    .line 466
    invoke-interface {p4, v3}, Lcom/anythink/basead/a/b$a;->a(Z)V

    .line 470
    :cond_2
    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v2, p1, p3}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    if-eqz p4, :cond_3

    .line 473
    invoke-interface {p4}, Lcom/anythink/basead/a/b$a;->b()V

    :cond_3
    return-void

    :cond_4
    if-nez p2, :cond_5

    .line 479
    invoke-direct {p0, p3, p4}, Lcom/anythink/basead/a/b;->c(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z

    move-result p2

    if-eqz p2, :cond_5

    return-void

    .line 482
    :cond_5
    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v2, p2, p3}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 485
    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_7

    :cond_6
    if-eqz p4, :cond_7

    .line 487
    invoke-interface {p4, v3}, Lcom/anythink/basead/a/b$a;->a(Z)V

    .line 492
    :cond_7
    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->q()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_b

    .line 493
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/anythink/basead/a/b;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    .line 494
    new-instance v1, Lcom/anythink/basead/c/c;

    invoke-direct {v1}, Lcom/anythink/basead/c/c;-><init>()V

    iput-object v1, p3, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    .line 495
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->b()Lcom/anythink/basead/c/e;

    move-result-object v1

    .line 496
    iget-object v2, p3, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    if-eqz v1, :cond_8

    iget-object v1, v1, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    goto :goto_0

    :cond_8
    const-string v1, ""

    :goto_0
    iput-object v1, v2, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    if-eqz p2, :cond_a

    const/16 p1, 0x19

    .line 499
    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {p1, p2, p3}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 500
    iput-boolean v3, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p4, :cond_9

    .line 502
    invoke-interface {p4}, Lcom/anythink/basead/a/b$a;->b()V

    :cond_9
    return-void

    :cond_a
    const/16 p2, 0x1a

    .line 506
    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {p2, v1, p3}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 510
    :cond_b
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_c

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->n()Ljava/lang/String;

    move-result-object p1

    .line 512
    :cond_c
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_e

    .line 513
    sget-object p1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string p2, "Offer click result is null."

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    new-instance p2, Lcom/anythink/basead/a/b$2;

    invoke-direct {p2, p0}, Lcom/anythink/basead/a/b$2;-><init>(Lcom/anythink/basead/a/b;)V

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 529
    iput-boolean v3, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p4, :cond_d

    .line 531
    invoke-interface {p4}, Lcom/anythink/basead/a/b$a;->b()V

    :cond_d
    return-void

    .line 536
    :cond_e
    iget-object p2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->s()I

    move-result p2

    const/4 p3, 0x2

    if-eq p2, v0, :cond_13

    if-eq p2, p3, :cond_12

    const/4 v0, 0x3

    if-eq p2, v0, :cond_11

    const/4 v0, 0x4

    if-eq p2, v0, :cond_10

    .line 570
    iget-object p2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object p2, p2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/j;->a()I

    move-result p2

    if-ne p2, p3, :cond_f

    .line 571
    new-instance p2, Lcom/anythink/basead/c/a;

    invoke-direct {p2}, Lcom/anythink/basead/c/a;-><init>()V

    .line 572
    iget-object p3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->c:Lcom/anythink/core/common/d/h;

    .line 573
    iget-object p3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->g:Lcom/anythink/core/common/d/i;

    .line 574
    iput-object p1, p2, Lcom/anythink/basead/c/a;->f:Ljava/lang/String;

    .line 576
    iget-object p1, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/anythink/basead/ui/web/WebLandPageActivity;->a(Landroid/content/Context;Lcom/anythink/basead/c/a;)V

    goto :goto_2

    .line 578
    :cond_f
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p2, p1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    .line 559
    :cond_10
    invoke-direct {p0, p1}, Lcom/anythink/basead/a/b;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 562
    :cond_11
    new-instance p2, Lcom/anythink/basead/c/a;

    invoke-direct {p2}, Lcom/anythink/basead/c/a;-><init>()V

    .line 563
    iget-object p3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->c:Lcom/anythink/core/common/d/h;

    .line 564
    iget-object p3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->g:Lcom/anythink/core/common/d/i;

    .line 565
    iput-object p1, p2, Lcom/anythink/basead/c/a;->f:Ljava/lang/String;

    .line 567
    iget-object p1, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/anythink/basead/ui/web/WebLandPageActivity;->a(Landroid/content/Context;Lcom/anythink/basead/c/a;)V

    goto :goto_2

    .line 555
    :cond_12
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p2, p1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    :cond_13
    if-eqz p1, :cond_14

    const-string p2, "http"

    .line 538
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_14

    goto :goto_1

    :cond_14
    const/4 v0, 0x0

    .line 539
    :goto_1
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p2, p1, v0}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result p2

    if-nez p2, :cond_16

    if-nez v0, :cond_16

    .line 541
    iget-object p2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object p2, p2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/j;->a()I

    move-result p2

    if-ne p2, p3, :cond_15

    .line 542
    new-instance p2, Lcom/anythink/basead/c/a;

    invoke-direct {p2}, Lcom/anythink/basead/c/a;-><init>()V

    .line 543
    iget-object p3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->c:Lcom/anythink/core/common/d/h;

    .line 544
    iget-object p3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iput-object p3, p2, Lcom/anythink/basead/c/a;->g:Lcom/anythink/core/common/d/i;

    .line 545
    iput-object p1, p2, Lcom/anythink/basead/c/a;->f:Ljava/lang/String;

    .line 547
    iget-object p1, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/anythink/basead/ui/web/WebLandPageActivity;->a(Landroid/content/Context;Lcom/anythink/basead/c/a;)V

    goto :goto_2

    .line 549
    :cond_15
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {p2, p1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 582
    :cond_16
    :goto_2
    iput-boolean v3, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p4, :cond_17

    .line 584
    invoke-interface {p4}, Lcom/anythink/basead/a/b$a;->b()V

    :cond_17
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .line 607
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    const/high16 v0, 0x10000000

    .line 609
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 610
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x1

    return p0

    :catchall_0
    move-exception p0

    .line 614
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method static synthetic a(Lcom/anythink/basead/a/b;Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z
    .locals 10

    .line 2122
    new-instance v0, Lcom/anythink/basead/c/c;

    invoke-direct {v0}, Lcom/anythink/basead/c/c;-><init>()V

    iput-object v0, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    .line 2123
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->b()Lcom/anythink/basead/c/e;

    move-result-object v0

    .line 2124
    iget-object v1, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    const-string v2, ""

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    iput-object v0, v1, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    .line 2125
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_5

    .line 2127
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    :goto_1
    const-string v3, "\\{req_id\\}"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2132
    iget-object v0, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {v0, v7, v1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2133
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 2134
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    iput-boolean v2, v0, Lcom/anythink/basead/c/b;->i:Z

    :cond_2
    const/16 v0, 0x9

    .line 2136
    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v0, v3, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 2137
    iget-object p1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x1

    const-string v8, "1"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 2138
    iput-boolean v1, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p2, :cond_3

    .line 2140
    invoke-interface {p2}, Lcom/anythink/basead/a/b$a;->b()V

    .line 2141
    invoke-interface {p2, v2}, Lcom/anythink/basead/a/b$a;->a(Z)V

    :cond_3
    return v2

    .line 2146
    :cond_4
    iget-object p1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object p0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x1

    const-string v8, "0"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    :cond_5
    return v1
.end method

.method private b()Lcom/anythink/basead/c/e;
    .locals 3

    .line 447
    invoke-static {}, Lcom/anythink/basead/a/c;->a()Lcom/anythink/basead/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v1

    iget-object v2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/anythink/basead/a/c;->a(ILjava/lang/String;)Lcom/anythink/basead/c/e;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .line 595
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".apk"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-direct {p0}, Lcom/anythink/basead/a/b;->b()Lcom/anythink/basead/c/e;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p1}, Lcom/anythink/basead/a/a;->a(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Lcom/anythink/basead/c/e;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/anythink/basead/a/b;Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/anythink/basead/a/b;->c(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z

    move-result p0

    return p0
.end method

.method private b(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z
    .locals 10

    .line 122
    new-instance v0, Lcom/anythink/basead/c/c;

    invoke-direct {v0}, Lcom/anythink/basead/c/c;-><init>()V

    iput-object v0, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    .line 123
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->b()Lcom/anythink/basead/c/e;

    move-result-object v0

    .line 124
    iget-object v1, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    const-string v2, ""

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    iput-object v0, v1, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_5

    .line 127
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    :goto_1
    const-string v3, "\\{req_id\\}"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 132
    iget-object v0, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {v0, v7, v1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    iput-boolean v2, v0, Lcom/anythink/basead/c/b;->i:Z

    :cond_2
    const/16 v0, 0x9

    .line 136
    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v0, v3, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 137
    iget-object p1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x1

    const-string v8, "1"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 138
    iput-boolean v1, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p2, :cond_3

    .line 140
    invoke-interface {p2}, Lcom/anythink/basead/a/b$a;->b()V

    .line 141
    invoke-interface {p2, v2}, Lcom/anythink/basead/a/b$a;->a(Z)V

    :cond_3
    return v2

    .line 146
    :cond_4
    iget-object p1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x1

    const-string v8, "0"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    :cond_5
    return v1
.end method

.method private c()Z
    .locals 3

    .line 620
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->c()I

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    instance-of v2, v0, Lcom/anythink/core/common/d/t;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/anythink/core/common/d/t;

    .line 621
    invoke-virtual {v0}, Lcom/anythink/core/common/d/t;->a()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private c(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)Z
    .locals 10

    .line 159
    new-instance v0, Lcom/anythink/basead/c/c;

    invoke-direct {v0}, Lcom/anythink/basead/c/c;-><init>()V

    iput-object v0, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    .line 160
    invoke-direct {p0}, Lcom/anythink/basead/a/b;->b()Lcom/anythink/basead/c/e;

    move-result-object v0

    .line 161
    iget-object v1, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    const-string v2, ""

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/anythink/basead/c/e;->c:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    iput-object v0, v1, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    const/16 v0, 0x17

    .line 162
    iget-object v1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v0, v1, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 164
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    .line 166
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    :goto_1
    const-string v3, "\\{req_id\\}"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 171
    iget-object v0, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    invoke-static {v0, v7, v1}, Lcom/anythink/basead/a/g;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    iput-boolean v2, v0, Lcom/anythink/basead/c/b;->i:Z

    :cond_2
    const/16 v0, 0x9

    .line 175
    iget-object v3, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {v0, v3, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 176
    iget-object v0, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x0

    const-string v8, "1"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 177
    iput-boolean v1, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz p2, :cond_3

    .line 179
    invoke-interface {p2}, Lcom/anythink/basead/a/b$a;->b()V

    .line 180
    invoke-interface {p2, v2}, Lcom/anythink/basead/a/b$a;->a(Z)V

    :cond_3
    const/16 p2, 0x18

    .line 182
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {p2, v0, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return v2

    .line 186
    :cond_4
    iget-object p2, p0, Lcom/anythink/basead/a/b;->f:Landroid/content/Context;

    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/anythink/basead/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_5

    const/16 p2, 0x1c

    .line 187
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {p2, v0, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    goto :goto_2

    :cond_5
    const/16 p2, 0x1d

    .line 189
    iget-object v0, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-static {p2, v0, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 191
    :goto_2
    iget-object p1, p0, Lcom/anythink/basead/a/b;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v5

    iget-object p1, p0, Lcom/anythink/basead/a/b;->c:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->b()I

    move-result v6

    const/4 v9, 0x0

    const-string v8, "0"

    invoke-static/range {v4 .. v9}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    :cond_6
    return v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    .line 628
    iput-boolean v0, p0, Lcom/anythink/basead/a/b;->e:Z

    return-void
.end method

.method public final a(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V
    .locals 2

    .line 81
    iget-boolean v0, p0, Lcom/anythink/basead/a/b;->d:Z

    if-eqz v0, :cond_0

    return-void

    .line 86
    :cond_0
    invoke-interface {p2}, Lcom/anythink/basead/a/b$a;->a()V

    const/4 v0, 0x1

    .line 89
    iput-boolean v0, p0, Lcom/anythink/basead/a/b;->d:Z

    const/4 v0, 0x0

    .line 90
    iput-boolean v0, p0, Lcom/anythink/basead/a/b;->e:Z

    .line 92
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/basead/a/b$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/basead/a/b$1;-><init>(Lcom/anythink/basead/a/b;Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method
