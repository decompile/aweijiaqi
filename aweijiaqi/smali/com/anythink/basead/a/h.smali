.class public final Lcom/anythink/basead/a/h;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/anythink/basead/c/c;)Ljava/lang/String;
    .locals 1

    .line 347
    iget-object v0, p1, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/anythink/basead/c/c;->a:Ljava/lang/String;

    :goto_0
    const-string v0, "\\{__CLICK_ID__\\}"

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Lcom/anythink/basead/c/h;)Ljava/lang/String;
    .locals 12

    const-string v0, "up_y"

    const-string v1, "utf-8"

    const-string v2, "{}"

    const-string v3, "up_x"

    const-string v4, "down_y"

    const-string v5, "down_x"

    .line 352
    iget-object v6, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    .line 353
    iget v7, p1, Lcom/anythink/basead/c/h;->e:I

    .line 354
    iget p1, p1, Lcom/anythink/basead/c/h;->f:I

    .line 357
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 359
    :try_start_0
    iget v9, v6, Lcom/anythink/basead/c/b;->e:I

    invoke-virtual {v8, v5, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 360
    iget v9, v6, Lcom/anythink/basead/c/b;->f:I

    invoke-virtual {v8, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 361
    iget v9, v6, Lcom/anythink/basead/c/b;->g:I

    invoke-virtual {v8, v3, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 362
    iget v9, v6, Lcom/anythink/basead/c/b;->h:I

    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :catch_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 369
    :try_start_1
    iget v10, v6, Lcom/anythink/basead/c/b;->e:I

    int-to-float v10, v10

    int-to-float v7, v7

    div-float/2addr v10, v7

    const/high16 v11, 0x447a0000    # 1000.0f

    mul-float v10, v10, v11

    float-to-int v10, v10

    invoke-virtual {v9, v5, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 370
    iget v5, v6, Lcom/anythink/basead/c/b;->e:I

    int-to-float v5, v5

    int-to-float p1, p1

    div-float/2addr v5, p1

    mul-float v5, v5, v11

    float-to-int v5, v5

    invoke-virtual {v9, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 371
    iget v4, v6, Lcom/anythink/basead/c/b;->g:I

    int-to-float v4, v4

    div-float/2addr v4, v7

    mul-float v4, v4, v11

    float-to-int v4, v4

    invoke-virtual {v9, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 372
    iget v3, v6, Lcom/anythink/basead/c/b;->h:I

    int-to-float v3, v3

    div-float/2addr v3, p1

    mul-float v3, v3, v11

    float-to-int p1, v3

    invoke-virtual {v9, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 378
    :catch_1
    :try_start_2
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 380
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    move-object p1, v2

    .line 384
    :goto_0
    :try_start_3
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    .line 386
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 389
    :goto_1
    iget-boolean v0, v6, Lcom/anythink/basead/c/b;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_2

    :cond_0
    const/4 v0, 0x2

    .line 391
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->a:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__DOWN_X__\\}"

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->b:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__DOWN_Y__\\}"

    .line 392
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__UP_X__\\}"

    .line 393
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->d:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__UP_Y__\\}"

    .line 394
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->e:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__RE_DOWN_X__\\}"

    .line 395
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->f:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__RE_DOWN_Y__\\}"

    .line 396
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->g:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__RE_UP_X__\\}"

    .line 397
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v6, Lcom/anythink/basead/c/b;->h:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "\\{__RE_UP_Y__\\}"

    .line 398
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "\\{ABSOLUTE_COORD\\}"

    .line 400
    invoke-virtual {p0, v1, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "\\{RELATIVE_COORD\\}"

    .line 401
    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 403
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__DPLINK_TYPE__\\}"

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected static a(Ljava/lang/String;Lcom/anythink/basead/c/h;J)Ljava/lang/String;
    .locals 5

    .line 288
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    return-object v1

    .line 293
    :cond_0
    iget-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    if-eqz v0, :cond_1

    .line 294
    invoke-static {p0, p1}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/h;)Ljava/lang/String;

    move-result-object p0

    .line 297
    :cond_1
    iget-object v0, p1, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p1, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    invoke-static {p0, v0}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/i;)Ljava/lang/String;

    move-result-object p0

    .line 301
    :cond_2
    iget-object v0, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    if-eqz v0, :cond_3

    .line 302
    iget-object v0, p1, Lcom/anythink/basead/c/h;->i:Lcom/anythink/basead/c/c;

    invoke-static {p0, v0}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/c;)Ljava/lang/String;

    move-result-object p0

    :cond_3
    const-wide/16 v2, 0x3e8

    .line 305
    div-long v2, p2, v2

    .line 308
    iget v0, p1, Lcom/anythink/basead/c/h;->c:I

    if-nez v0, :cond_4

    const-string v0, "__REQ_WIDTH__"

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p1, Lcom/anythink/basead/c/h;->c:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v4, "\\{__REQ_WIDTH__\\}"

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 309
    iget v0, p1, Lcom/anythink/basead/c/h;->d:I

    if-nez v0, :cond_5

    const-string v0, "__REQ_HEIGHT__"

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p1, Lcom/anythink/basead/c/h;->d:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v4, "\\{__REQ_HEIGHT__\\}"

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p1, Lcom/anythink/basead/c/h;->e:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "\\{__WIDTH__\\}"

    .line 310
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget p1, p1, Lcom/anythink/basead/c/h;->f:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__HEIGHT__\\}"

    .line 311
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__TS__\\}"

    .line 312
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__TS_MSEC__\\}"

    .line 313
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__END_TS__\\}"

    .line 314
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const-string p2, "\\{__END_TS_MSEC__\\}"

    .line 315
    invoke-virtual {p0, p2, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "\\{__PLAY_SEC__\\}"

    const-string p2, "0"

    .line 316
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "\\{"

    .line 321
    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "\\}"

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Lcom/anythink/basead/c/i;)Ljava/lang/String;
    .locals 6

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__VIDEO_TIME__\\}"

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__BEGIN_TIME__\\}"

    .line 327
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__END_TIME__\\}"

    .line 328
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__PLAY_FIRST_FRAME__\\}"

    .line 329
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__PLAY_LAST_FRAME__\\}"

    .line 330
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__SCENE__\\}"

    .line 331
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__TYPE__\\}"

    .line 332
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__BEHAVIOR__\\}"

    .line 333
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__STATUS__\\}"

    .line 334
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/anythink/basead/c/i;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\{__PLAY_SEC__\\}"

    .line 335
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/anythink/basead/c/i;->f:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\{__TS__\\}"

    .line 336
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/anythink/basead/c/i;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\{__TS_MSEC__\\}"

    .line 337
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/anythink/basead/c/i;->g:J

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\{__END_TS__\\}"

    .line 338
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p1, Lcom/anythink/basead/c/i;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\{__END_TS_MSEC__\\}"

    .line 339
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p1, Lcom/anythink/basead/c/i;->h:I

    div-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget p1, p1, Lcom/anythink/basead/c/i;->h:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\{__PLAY_MSEC__\\}"

    .line 341
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(ILcom/anythink/basead/c/h;Lcom/anythink/core/common/d/u;Lcom/anythink/core/common/d/w;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/anythink/basead/c/h;",
            "Lcom/anythink/core/common/d/u;",
            "Lcom/anythink/core/common/d/w;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 273
    :pswitch_1
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->an()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 270
    :pswitch_2
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->am()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 264
    :pswitch_3
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->al()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 261
    :pswitch_4
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ak()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 258
    :pswitch_5
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ai()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 255
    :pswitch_6
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->aj()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 251
    :pswitch_7
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ah()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 248
    :pswitch_8
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ag()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 245
    :pswitch_9
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->af()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 242
    :pswitch_a
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ae()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 239
    :pswitch_b
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ad()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 220
    :pswitch_c
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->P()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 236
    :pswitch_d
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ac()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 217
    :pswitch_e
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->O()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1

    .line 214
    :pswitch_f
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->N()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 233
    :pswitch_10
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->ab()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 230
    :pswitch_11
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->aa()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 227
    :pswitch_12
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->Z()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 223
    :pswitch_13
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->I()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 211
    :pswitch_14
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->K()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 208
    :pswitch_15
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->J()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 205
    :pswitch_16
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->H()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 202
    :pswitch_17
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->z()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 199
    :pswitch_18
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->B()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 196
    :pswitch_19
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->A()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 193
    :pswitch_1a
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->M()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 190
    :pswitch_1b
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->L()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 187
    :pswitch_1c
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->G()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 184
    :pswitch_1d
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->F()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 181
    :pswitch_1e
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->E()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 178
    :pswitch_1f
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->D()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 175
    :pswitch_20
    invoke-virtual {p3}, Lcom/anythink/core/common/d/w;->C()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    :goto_0
    const-string p3, ""

    .line 278
    :goto_1
    invoke-static {p3}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 282
    :cond_0
    new-instance v0, Lcom/anythink/basead/h/f;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/anythink/basead/h/f;-><init>(ILcom/anythink/core/common/d/u;Ljava/lang/String;Ljava/util/Map;)V

    .line 283
    iget-object p0, p1, Lcom/anythink/basead/c/h;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/anythink/basead/h/f;->b(Ljava/lang/String;)V

    const/4 p0, 0x0

    const/4 p1, 0x0

    .line 284
    invoke-virtual {v0, p0, p1}, Lcom/anythink/basead/h/f;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected static a(ILcom/anythink/core/common/d/u;Lcom/anythink/basead/c/h;)V
    .locals 11

    .line 31
    invoke-virtual {p1}, Lcom/anythink/core/common/d/u;->y()Lcom/anythink/core/common/d/w;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->c()Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1150
    :pswitch_1
    :try_start_0
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->b()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1147
    :pswitch_2
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->a()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1140
    :pswitch_3
    iget-object v4, p2, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    .line 1141
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->y()Ljava/util/Map;

    move-result-object v5

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 1143
    iget v4, v4, Lcom/anythink/basead/c/i;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    goto/16 :goto_1

    .line 1137
    :pswitch_4
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->x()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1134
    :pswitch_5
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->w()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1131
    :pswitch_6
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->u()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1128
    :pswitch_7
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->v()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1123
    :pswitch_8
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->Y()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1120
    :pswitch_9
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->X()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1117
    :pswitch_a
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->W()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1114
    :pswitch_b
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->V()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1111
    :pswitch_c
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->U()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1092
    :pswitch_d
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->t()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1108
    :pswitch_e
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->T()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1089
    :pswitch_f
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->s()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1086
    :pswitch_10
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->r()[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 1105
    :pswitch_11
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->S()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1102
    :pswitch_12
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->R()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1099
    :pswitch_13
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->Q()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1095
    :pswitch_14
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->m()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1083
    :pswitch_15
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->o()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1080
    :pswitch_16
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->n()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1077
    :pswitch_17
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->l()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1074
    :pswitch_18
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->d()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1071
    :pswitch_19
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->f()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1068
    :pswitch_1a
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->e()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1065
    :pswitch_1b
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->q()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1062
    :pswitch_1c
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->p()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1059
    :pswitch_1d
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->k()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1056
    :pswitch_1e
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->j()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1053
    :pswitch_1f
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->i()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1050
    :pswitch_20
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->h()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1047
    :pswitch_21
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->g()[Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :catchall_0
    move-exception v4

    goto :goto_3

    :cond_0
    :goto_0
    move-object v4, v3

    :goto_1
    if-eqz v4, :cond_1

    .line 1156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1157
    array-length v7, v4

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v7, :cond_1

    aget-object v9, v4, v8

    .line 1160
    invoke-static {v9, p2, v5, v6}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/h;J)Ljava/lang/String;

    move-result-object v9

    .line 1162
    new-instance v10, Lcom/anythink/basead/h/e;

    invoke-direct {v10, p0, v9, p1, v1}, Lcom/anythink/basead/h/e;-><init>(ILjava/lang/String;Lcom/anythink/core/common/d/u;Ljava/util/Map;)V

    invoke-virtual {v10, v2, v3}, Lcom/anythink/basead/h/e;->a(ILcom/anythink/core/common/e/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1166
    :goto_3
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    packed-switch p0, :pswitch_data_1

    :pswitch_22
    goto/16 :goto_4

    .line 1273
    :pswitch_23
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->an()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1270
    :pswitch_24
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->am()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1264
    :pswitch_25
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->al()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1261
    :pswitch_26
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ak()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1258
    :pswitch_27
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ai()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1255
    :pswitch_28
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->aj()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1251
    :pswitch_29
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ah()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1248
    :pswitch_2a
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ag()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1245
    :pswitch_2b
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->af()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1242
    :pswitch_2c
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ae()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1239
    :pswitch_2d
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ad()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1220
    :pswitch_2e
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->P()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1236
    :pswitch_2f
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ac()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1217
    :pswitch_30
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->O()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1214
    :pswitch_31
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->N()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1233
    :pswitch_32
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->ab()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1230
    :pswitch_33
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->aa()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1227
    :pswitch_34
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->Z()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1223
    :pswitch_35
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->I()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1211
    :pswitch_36
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->K()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1208
    :pswitch_37
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->J()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1205
    :pswitch_38
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->H()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1202
    :pswitch_39
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->z()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1199
    :pswitch_3a
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1196
    :pswitch_3b
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->A()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1193
    :pswitch_3c
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->M()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1190
    :pswitch_3d
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->L()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1187
    :pswitch_3e
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1184
    :pswitch_3f
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->F()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1181
    :pswitch_40
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->E()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1178
    :pswitch_41
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1175
    :pswitch_42
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :goto_4
    const-string v0, ""

    .line 1278
    :goto_5
    invoke-static {v0}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1282
    new-instance v4, Lcom/anythink/basead/h/f;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/anythink/basead/h/f;-><init>(ILcom/anythink/core/common/d/u;Ljava/lang/String;Ljava/util/Map;)V

    .line 1283
    iget-object p0, p2, Lcom/anythink/basead/c/h;->b:Ljava/lang/String;

    invoke-virtual {v4, p0}, Lcom/anythink/basead/h/f;->b(Ljava/lang/String;)V

    .line 1284
    invoke-virtual {v4, v2, v3}, Lcom/anythink/basead/h/f;->a(ILcom/anythink/core/common/e/g;)V

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_22
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_22
        :pswitch_24
        :pswitch_23
    .end packed-switch
.end method

.method private static a(ILcom/anythink/core/common/d/u;Lcom/anythink/core/common/d/w;Ljava/util/Map;Lcom/anythink/basead/c/h;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/anythink/core/common/d/u;",
            "Lcom/anythink/core/common/d/w;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/anythink/basead/c/h;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 150
    :pswitch_1
    :try_start_0
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->b()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 147
    :pswitch_2
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->a()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 140
    :pswitch_3
    iget-object v1, p4, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    .line 141
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->y()Ljava/util/Map;

    move-result-object p2

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 143
    iget v1, v1, Lcom/anythink/basead/c/i;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    goto/16 :goto_1

    .line 137
    :pswitch_4
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->x()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 134
    :pswitch_5
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->w()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 131
    :pswitch_6
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->u()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 128
    :pswitch_7
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->v()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 123
    :pswitch_8
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->Y()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 120
    :pswitch_9
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->X()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 117
    :pswitch_a
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->W()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 114
    :pswitch_b
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->V()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 111
    :pswitch_c
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->U()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 92
    :pswitch_d
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->t()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 108
    :pswitch_e
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->T()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 89
    :pswitch_f
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->s()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 86
    :pswitch_10
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->r()[Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_1

    .line 105
    :pswitch_11
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->S()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 102
    :pswitch_12
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->R()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 99
    :pswitch_13
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->Q()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 95
    :pswitch_14
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->m()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 83
    :pswitch_15
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->o()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 80
    :pswitch_16
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->n()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 77
    :pswitch_17
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->l()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 74
    :pswitch_18
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->d()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 71
    :pswitch_19
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->f()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 68
    :pswitch_1a
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->e()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 65
    :pswitch_1b
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->q()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 62
    :pswitch_1c
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->p()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 59
    :pswitch_1d
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->k()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 56
    :pswitch_1e
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->j()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 53
    :pswitch_1f
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->i()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 50
    :pswitch_20
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->h()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 47
    :pswitch_21
    invoke-virtual {p2}, Lcom/anythink/core/common/d/w;->g()[Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :catchall_0
    move-exception p0

    goto :goto_3

    :cond_0
    :goto_0
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_1

    .line 156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 157
    array-length v3, p2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v3, :cond_1

    aget-object v6, p2, v5

    .line 160
    invoke-static {v6, p4, v1, v2}, Lcom/anythink/basead/a/h;->a(Ljava/lang/String;Lcom/anythink/basead/c/h;J)Ljava/lang/String;

    move-result-object v6

    .line 162
    new-instance v7, Lcom/anythink/basead/h/e;

    invoke-direct {v7, p0, v6, p1, p3}, Lcom/anythink/basead/h/e;-><init>(ILjava/lang/String;Lcom/anythink/core/common/d/u;Ljava/util/Map;)V

    invoke-virtual {v7, v4, v0}, Lcom/anythink/basead/h/e;->a(ILcom/anythink/core/common/e/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 166
    :goto_3
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .line 409
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 413
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 414
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez p0, :cond_1

    const/4 p0, 0x0

    return p0

    :catchall_0
    :cond_1
    return v1
.end method
