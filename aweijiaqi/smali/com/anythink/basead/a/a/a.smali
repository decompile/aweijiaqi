.class public Lcom/anythink/basead/a/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/a/a/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/a/a/a$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/anythink/basead/a/a/a$a;

.field private h:Landroid/os/Handler;

.field private i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/anythink/basead/a/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/anythink/basead/a/a/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 42
    iput-object p1, p0, Lcom/anythink/basead/a/a/a;->b:Ljava/lang/String;

    .line 43
    iput-boolean p2, p0, Lcom/anythink/basead/a/a/a;->c:Z

    .line 44
    iput p3, p0, Lcom/anythink/basead/a/a/a;->d:I

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/a/a/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/basead/a/a/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 146
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->g:Lcom/anythink/basead/a/a/a$a;

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Offer load success, OfferId -> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/basead/a/a/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->g:Lcom/anythink/basead/a/a/a$a;

    invoke-interface {v0}, Lcom/anythink/basead/a/a/a$a;->a()V

    .line 150
    :cond_0
    invoke-direct {p0}, Lcom/anythink/basead/a/a/a;->b()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/a/a/a;Lcom/anythink/basead/c/f;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/anythink/basead/a/a/a;->a(Lcom/anythink/basead/c/f;)V

    return-void
.end method

.method private a(Lcom/anythink/basead/c/f;)V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 155
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->g:Lcom/anythink/basead/a/a/a$a;

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Offer load failed, OfferId -> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/basead/a/a/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->g:Lcom/anythink/basead/a/a/a$a;

    invoke-interface {v0, p1}, Lcom/anythink/basead/a/a/a$a;->a(Lcom/anythink/basead/c/f;)V

    .line 159
    :cond_0
    invoke-direct {p0}, Lcom/anythink/basead/a/a/a;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .line 163
    invoke-static {}, Lcom/anythink/basead/a/a/c;->a()Lcom/anythink/basead/a/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/anythink/basead/a/a/c;->b(Lcom/anythink/basead/a/a/c$a;)V

    .line 164
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 165
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 166
    iput-object v1, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method private c()V
    .locals 4

    .line 171
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    .line 173
    new-instance v1, Lcom/anythink/basead/a/a/a$1;

    invoke-direct {v1, p0}, Lcom/anythink/basead/a/a/a$1;-><init>(Lcom/anythink/basead/a/a/a;)V

    iget v2, p0, Lcom/anythink/basead/a/a/a;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;Lcom/anythink/basead/a/a/a$a;)V
    .locals 5

    .line 63
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/a/a/a;->e:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Lcom/anythink/basead/a/a/a;->g:Lcom/anythink/basead/a/a/a$a;

    .line 67
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/h;->a(Lcom/anythink/core/common/d/j;)Ljava/util/List;

    move-result-object p2

    if-nez p2, :cond_0

    const-string p1, "30003"

    const-string p2, "Incomplete resource allocation!"

    .line 69
    invoke-static {p1, p2}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/anythink/basead/a/a/a;->a(Lcom/anythink/basead/c/f;)V

    return-void

    .line 73
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p3

    if-nez p3, :cond_1

    .line 76
    invoke-direct {p0}, Lcom/anythink/basead/a/a/a;->a()V

    return-void

    .line 80
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p3, :cond_3

    .line 82
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 84
    invoke-static {v2}, Lcom/anythink/basead/a/a/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 85
    iget-object v3, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    :cond_3
    iget-object p2, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-nez p2, :cond_4

    .line 91
    sget-object p1, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Offer("

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, p0, Lcom/anythink/basead/a/a/a;->e:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "), all files have already exist"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lcom/anythink/basead/a/a/a;->a()V

    return-void

    .line 97
    :cond_4
    invoke-static {}, Lcom/anythink/basead/a/a/c;->a()Lcom/anythink/basead/a/a/c;

    move-result-object p3

    invoke-virtual {p3, p0}, Lcom/anythink/basead/a/a/c;->a(Lcom/anythink/basead/a/a/c$a;)V

    .line 1171
    iget-object p3, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    if-nez p3, :cond_5

    .line 1172
    new-instance p3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p3, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p3, p0, Lcom/anythink/basead/a/a/a;->h:Landroid/os/Handler;

    .line 1173
    new-instance v1, Lcom/anythink/basead/a/a/a$1;

    invoke-direct {v1, p0}, Lcom/anythink/basead/a/a/a$1;-><init>(Lcom/anythink/basead/a/a/a;)V

    iget v2, p0, Lcom/anythink/basead/a/a/a;->d:I

    int-to-long v2, v2

    invoke-virtual {p3, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 100
    :cond_5
    monitor-enter p0

    const/4 p3, 0x0

    :goto_1
    if-ge p3, p2, :cond_9

    .line 102
    :try_start_0
    iget-object v1, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 104
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 106
    invoke-static {v1}, Lcom/anythink/basead/a/a/b;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 107
    sget-object v2, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    const-string v3, "file is loading -> "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 109
    :cond_6
    invoke-static {v1}, Lcom/anythink/basead/a/a/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 110
    sget-object v2, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    const-string v3, "file exist -> "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-static {v1, v0}, Lcom/anythink/basead/a/a/b;->a(Ljava/lang/String;I)V

    .line 112
    invoke-static {}, Lcom/anythink/basead/a/a/c;->a()Lcom/anythink/basead/a/a/c;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/anythink/basead/a/a/c;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    const/4 v2, 0x1

    .line 115
    invoke-static {v1, v2}, Lcom/anythink/basead/a/a/b;->a(Ljava/lang/String;I)V

    .line 116
    sget-object v2, Lcom/anythink/basead/a/a/a;->a:Ljava/lang/String;

    const-string v3, "file not exist -> "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v2, Lcom/anythink/basead/a/a/d;

    iget-object v3, p0, Lcom/anythink/basead/a/a/a;->b:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/anythink/basead/a/a/a;->c:Z

    invoke-direct {v2, v3, v4, p1, v1}, Lcom/anythink/basead/a/a/d;-><init>(Ljava/lang/String;ZLcom/anythink/core/common/d/h;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v2}, Lcom/anythink/basead/a/a/d;->d()V

    :cond_8
    :goto_2
    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 120
    :cond_9
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 125
    monitor-enter p0

    const/4 v0, 0x0

    .line 126
    :try_start_0
    invoke-static {p1, v0}, Lcom/anythink/basead/a/a/b;->a(Ljava/lang/String;I)V

    .line 127
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 129
    iget-object p1, p0, Lcom/anythink/basead/a/a/a;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_0

    .line 130
    iget-object p1, p0, Lcom/anythink/basead/a/a/a;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/anythink/basead/a/a/a;->a()V

    .line 135
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Lcom/anythink/basead/c/f;)V
    .locals 1

    const/4 v0, 0x0

    .line 140
    invoke-static {p1, v0}, Lcom/anythink/basead/a/a/b;->a(Ljava/lang/String;I)V

    .line 141
    invoke-direct {p0, p2}, Lcom/anythink/basead/a/a/a;->a(Lcom/anythink/basead/c/f;)V

    return-void
.end method
