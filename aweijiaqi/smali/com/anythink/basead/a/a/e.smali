.class public final Lcom/anythink/basead/a/a/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/a/a/e$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/FileDescriptor;)Lcom/anythink/basead/a/a/e$a;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 60
    :cond_0
    :try_start_0
    new-instance v1, Lcom/anythink/basead/a/a/e$a;

    invoke-direct {v1}, Lcom/anythink/basead/a/a/e$a;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 63
    :try_start_1
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 65
    invoke-virtual {v0, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    const/16 p0, 0x12

    .line 67
    invoke-virtual {v0, p0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x13

    .line 69
    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 73
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 74
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v1, Lcom/anythink/basead/a/a/e$a;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception p0

    .line 76
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v0

    :goto_1
    return-object v1
.end method

.method private static a(Ljava/io/FileDescriptor;II)Lcom/anythink/basead/a/a/e$a;
    .locals 3

    .line 111
    invoke-static {p0}, Lcom/anythink/basead/a/a/e;->a(Ljava/io/FileDescriptor;)Lcom/anythink/basead/a/a/e$a;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 116
    :cond_0
    iget v0, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 117
    iget v1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v0, v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v0, v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float v1, p1

    mul-float v1, v1, v2

    int-to-float v2, p2

    div-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 124
    iput p2, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    .line 125
    iget p1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float p1, p1

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    goto :goto_0

    .line 128
    :cond_1
    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 129
    iget p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    int-to-float p1, p1

    div-float/2addr p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    :goto_0
    return-object p0
.end method

.method private static a(Ljava/lang/String;)Lcom/anythink/basead/a/a/e$a;
    .locals 3

    .line 26
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 31
    :cond_0
    :try_start_0
    new-instance v0, Lcom/anythink/basead/a/a/e$a;

    invoke-direct {v0}, Lcom/anythink/basead/a/a/e$a;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 34
    :try_start_1
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 36
    invoke-virtual {v1, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/16 p0, 0x12

    .line 38
    invoke-virtual {v1, p0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x13

    .line 40
    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 44
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v0, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 45
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v0, Lcom/anythink/basead/a/a/e$a;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    move-object v1, v0

    goto :goto_0

    :catch_1
    move-exception p0

    .line 47
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    :goto_1
    return-object v0
.end method

.method private static a(Ljava/lang/String;II)Lcom/anythink/basead/a/a/e$a;
    .locals 3

    .line 85
    invoke-static {p0}, Lcom/anythink/basead/a/a/e;->a(Ljava/lang/String;)Lcom/anythink/basead/a/a/e$a;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 90
    :cond_0
    iget v0, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 91
    iget v1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v0, v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v0, v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float v1, p1

    mul-float v1, v1, v2

    int-to-float v2, p2

    div-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 98
    iput p2, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    .line 99
    iget p1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float p1, p1

    mul-float p1, p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    goto :goto_0

    .line 102
    :cond_1
    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 103
    iget p1, p0, Lcom/anythink/basead/a/a/e$a;->a:I

    int-to-float p1, p1

    div-float/2addr p1, v0

    float-to-int p1, p1

    iput p1, p0, Lcom/anythink/basead/a/a/e$a;->b:I

    :goto_0
    return-object p0
.end method
