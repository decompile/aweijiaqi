.class public final Lcom/anythink/basead/e/a/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/e/a/a$a;
    }
.end annotation


# static fields
.field private static c:Lcom/anythink/basead/e/a/a;


# instance fields
.field a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    .line 42
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object p1, p0, Lcom/anythink/basead/e/a/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/e/a/a;)Landroid/content/Context;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    return-object p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/basead/e/a/a;
    .locals 2

    const-class v0, Lcom/anythink/basead/e/a/a;

    monitor-enter v0

    .line 46
    :try_start_0
    sget-object v1, Lcom/anythink/basead/e/a/a;->c:Lcom/anythink/basead/e/a/a;

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Lcom/anythink/basead/e/a/a;

    invoke-direct {v1, p0}, Lcom/anythink/basead/e/a/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/basead/e/a/a;->c:Lcom/anythink/basead/e/a/a;

    .line 49
    :cond_0
    sget-object p0, Lcom/anythink/basead/e/a/a;->c:Lcom/anythink/basead/e/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/basead/e/a/a;Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V
    .locals 3

    .line 171
    invoke-static {}, Lcom/anythink/basead/a/d;->a()Lcom/anythink/basead/a/d;

    iget-object v0, p2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v1, p2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    new-instance v2, Lcom/anythink/basead/e/a/a$2;

    invoke-direct {v2, p0, p2, p3, p1}, Lcom/anythink/basead/e/a/a$2;-><init>(Lcom/anythink/basead/e/a/a;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;Lcom/anythink/core/common/d/f;)V

    invoke-static {v0, p1, v1, v2}, Lcom/anythink/basead/a/d;->a(Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;Lcom/anythink/basead/a/a/a$a;)V

    return-void
.end method

.method private b(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V
    .locals 3

    .line 75
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/anythink/basead/h/a;

    invoke-direct {v0, p1}, Lcom/anythink/basead/h/a;-><init>(Lcom/anythink/core/common/d/i;)V

    const/4 v1, 0x0

    .line 82
    new-instance v2, Lcom/anythink/basead/e/a/a$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/anythink/basead/e/a/a$1;-><init>(Lcom/anythink/basead/e/a/a;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    invoke-virtual {v0, v1, v2}, Lcom/anythink/basead/h/a;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    :cond_0
    if-eqz p2, :cond_1

    .line 132
    invoke-interface {p2}, Lcom/anythink/basead/e/a/a$a;->a()V

    .line 134
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void
.end method

.method private c(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V
    .locals 7

    const/4 v0, 0x0

    .line 141
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_0

    if-eqz p2, :cond_4

    const-string p1, "30001"

    const-string v1, "No fill, offer = null!"

    .line 148
    invoke-static {p1, v1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lcom/anythink/basead/e/a/a$a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/basead/c/f;)V

    return-void

    .line 153
    :cond_0
    invoke-static {p1, v1}, Lcom/anythink/basead/e/c/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/u;)V

    .line 156
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v2

    .line 4047
    sget-object v3, Lcom/anythink/core/common/b/e;->w:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_win_notice"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v5}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    const/4 v5, 0x1

    :cond_1
    if-nez v5, :cond_2

    const/16 v0, 0xa

    .line 157
    new-instance v4, Lcom/anythink/basead/c/h;

    iget-object v5, p1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v4, v5, v6}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v4}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 159
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v4

    .line 5043
    sget-object v5, Lcom/anythink/core/common/b/e;->w:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v5, v2, v3}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    if-eqz p2, :cond_3

    .line 163
    invoke-interface {p2}, Lcom/anythink/basead/e/a/a$a;->a()V

    .line 165
    :cond_3
    invoke-direct {p0, v1, p1, p2}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;
    .locals 4

    .line 193
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    iget-object v1, p1, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    .line 6034
    sget-object v2, Lcom/anythink/core/common/b/e;->w:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v0, v2, v1, v3}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 198
    :cond_0
    :try_start_0
    iget-object v1, p1, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v3}, Lcom/anythink/basead/e/a/b;->a(Ljava/lang/String;Lorg/json/JSONObject;)Lcom/anythink/core/common/d/f;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :goto_0
    if-eqz v2, :cond_1

    .line 204
    invoke-static {p1, v2}, Lcom/anythink/basead/e/c/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/u;)V

    :cond_1
    return-object v2
.end method

.method public final a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V
    .locals 7

    .line 54
    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "20005"

    const-string v0, "Offer data is loading."

    .line 56
    invoke-static {p1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {p2, v1, p1}, Lcom/anythink/basead/e/a/a$a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/basead/c/f;)V

    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/e/a/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 1141
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    const-string p1, "30001"

    const-string v0, "No fill, offer = null!"

    .line 1148
    invoke-static {p1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {p2, v1, p1}, Lcom/anythink/basead/e/a/a$a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/basead/c/f;)V

    return-void

    .line 1153
    :cond_1
    invoke-static {p1, v0}, Lcom/anythink/basead/e/c/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/u;)V

    .line 1156
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v1, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v3

    .line 2047
    sget-object v4, Lcom/anythink/core/common/b/e;->w:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_win_notice"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5, v2}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_3

    const/16 v1, 0xa

    .line 1157
    new-instance v2, Lcom/anythink/basead/c/h;

    iget-object v5, p1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v2, v5, v6}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v0, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 1159
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v1, p0, Lcom/anythink/basead/e/a/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v2

    .line 3043
    sget-object v5, Lcom/anythink/core/common/b/e;->w:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v5, v2, v4}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1163
    :cond_3
    invoke-interface {p2}, Lcom/anythink/basead/e/a/a$a;->a()V

    .line 1165
    invoke-direct {p0, v0, p1, p2}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void

    .line 3075
    :cond_4
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    nop

    :goto_1
    if-nez v1, :cond_5

    .line 3081
    new-instance v0, Lcom/anythink/basead/h/a;

    invoke-direct {v0, p1}, Lcom/anythink/basead/h/a;-><init>(Lcom/anythink/core/common/d/i;)V

    .line 3082
    new-instance v1, Lcom/anythink/basead/e/a/a$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/basead/e/a/a$1;-><init>(Lcom/anythink/basead/e/a/a;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    invoke-virtual {v0, v2, v1}, Lcom/anythink/basead/h/a;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    .line 3132
    :cond_5
    invoke-interface {p2}, Lcom/anythink/basead/e/a/a$a;->a()V

    .line 3134
    invoke-direct {p0, v1, p1, p2}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/f;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void
.end method
