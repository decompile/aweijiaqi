.class public final Lcom/anythink/basead/e/a/b;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/lang/String;Lorg/json/JSONObject;)Lcom/anythink/core/common/d/f;
    .locals 4

    const/4 v0, 0x0

    .line 22
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/b/e$a;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "seatbid"

    if-nez v1, :cond_0

    .line 24
    :try_start_1
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    :cond_0
    move-object p1, v1

    .line 32
    :cond_1
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    .line 33
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object p1

    .line 36
    new-instance v1, Lcom/anythink/core/common/d/f;

    invoke-direct {v1}, Lcom/anythink/core/common/d/f;-><init>()V

    .line 37
    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->a(Ljava/lang/String;)V

    const-string p0, "oid"

    .line 38
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->c(Ljava/lang/String;)V

    const-string p0, "c_id"

    .line 39
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->d(Ljava/lang/String;)V

    const-string p0, "pkg"

    .line 40
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->p(Ljava/lang/String;)V

    const-string p0, "title"

    .line 41
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->e(Ljava/lang/String;)V

    const-string p0, "desc"

    .line 42
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->f(Ljava/lang/String;)V

    const-string p0, "rating"

    .line 43
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->d(I)V

    const-string p0, "icon_u"

    .line 44
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->g(Ljava/lang/String;)V

    const-string p0, "full_u"

    .line 45
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->i(Ljava/lang/String;)V

    const-string p0, "unit_type"

    .line 46
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->b(I)V

    const-string p0, "tp_logo_u"

    .line 47
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->j(Ljava/lang/String;)V

    const-string p0, "cta"

    .line 48
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->k(Ljava/lang/String;)V

    const-string p0, "video_u"

    .line 49
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->l(Ljava/lang/String;)V

    const-string p0, "video_l"

    .line 50
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    int-to-long v2, p0

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/f;->c(J)V

    const-string p0, "video_r"

    .line 51
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->r(Ljava/lang/String;)V

    const-string p0, "ec_u"

    .line 52
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->s(Ljava/lang/String;)V

    const-string p0, "store_u"

    .line 53
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->m(Ljava/lang/String;)V

    const-string p0, "link_type"

    .line 54
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->c(I)V

    const-string p0, "click_u"

    .line 55
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->o(Ljava/lang/String;)V

    const-string p0, "deeplink"

    .line 56
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->n(Ljava/lang/String;)V

    const-string p0, "crt_type"

    const/4 v2, 0x1

    .line 59
    invoke-virtual {p1, p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->g(I)V

    const-string p0, "img_list"

    .line 60
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->t(Ljava/lang/String;)V

    const-string p0, "banner_xhtml"

    .line 61
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->u(Ljava/lang/String;)V

    const-string p0, "offer_firm_id"

    .line 64
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->a(I)V

    const-string p0, "jump_url"

    .line 65
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->b(Ljava/lang/String;)V

    const-string p0, "ctrl"

    .line 68
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/anythink/core/common/d/v;->b(Ljava/lang/String;)Lcom/anythink/core/common/d/v;

    move-result-object p0

    .line 69
    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->a(Lcom/anythink/core/common/d/v;)V

    const-string p0, "tk"

    .line 71
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/anythink/core/common/d/w;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/w;

    move-result-object p0

    .line 72
    invoke-virtual {v1, p0}, Lcom/anythink/core/common/d/f;->a(Lcom/anythink/core/common/d/w;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :catchall_0
    return-object v0
.end method
