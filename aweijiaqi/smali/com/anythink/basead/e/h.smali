.class public final Lcom/anythink/basead/e/h;
.super Ljava/lang/Object;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/anythink/basead/f/a;

.field c:Lcom/anythink/basead/d/c;

.field d:Lcom/anythink/basead/a/b;

.field e:Landroid/view/View;

.field f:Z

.field g:Lcom/anythink/core/common/d/u;

.field h:Lcom/anythink/core/common/d/i;

.field i:Landroid/view/View$OnClickListener;

.field j:I

.field k:I

.field l:Lcom/anythink/basead/ui/OwnNativeAdView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/u;Lcom/anythink/core/common/d/i;)V
    .locals 1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/anythink/basead/e/h$1;

    invoke-direct {v0, p0}, Lcom/anythink/basead/e/h$1;-><init>(Lcom/anythink/basead/e/h;)V

    iput-object v0, p0, Lcom/anythink/basead/e/h;->i:Landroid/view/View$OnClickListener;

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    .line 101
    iput-object p3, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 250
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 251
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 252
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 253
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 254
    invoke-direct {p0, v1, p2}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 257
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static synthetic a(Lcom/anythink/basead/e/h;)V
    .locals 5

    .line 1295
    iget-boolean v0, p0, Lcom/anythink/basead/e/h;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1299
    iput-boolean v0, p0, Lcom/anythink/basead/e/h;->f:Z

    .line 1300
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    instance-of v0, v0, Lcom/anythink/core/common/d/t;

    if-eqz v0, :cond_0

    .line 1301
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    iget-object v4, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, v4, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)V

    .line 1304
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    if-eqz v0, :cond_1

    .line 1305
    new-instance v0, Lcom/anythink/basead/c/h;

    iget-object v1, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    iget-object v1, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/OwnNativeAdView;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->f:I

    .line 1307
    iget-object v1, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/OwnNativeAdView;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->e:I

    const/16 v1, 0x8

    .line 1308
    iget-object v2, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    invoke-static {v1, v2, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 1309
    iget-object p0, p0, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    if-eqz p0, :cond_1

    .line 1310
    invoke-interface {p0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_1
    return-void
.end method

.method private a([Lcom/anythink/basead/ui/OwnNativeAdView;Landroid/view/View;)V
    .locals 2

    .line 237
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 238
    instance-of v0, p2, Lcom/anythink/basead/ui/OwnNativeAdView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 239
    move-object v0, p2

    check-cast v0, Lcom/anythink/basead/ui/OwnNativeAdView;

    aput-object v0, p1, v1

    .line 241
    :cond_0
    check-cast p2, Landroid/view/ViewGroup;

    .line 242
    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 243
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 244
    invoke-direct {p0, p1, v0}, Lcom/anythink/basead/e/h;->a([Lcom/anythink/basead/ui/OwnNativeAdView;Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Landroid/view/View;)Z
    .locals 3

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/anythink/basead/ui/OwnNativeAdView;

    .line 220
    invoke-direct {p0, v1, p1}, Lcom/anythink/basead/e/h;->a([Lcom/anythink/basead/ui/OwnNativeAdView;Landroid/view/View;)V

    const/4 p1, 0x0

    .line 221
    aget-object v2, v1, p1

    if-nez v2, :cond_0

    .line 222
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string v1, "Register View don\'t contain OwnNativeAdView."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1

    .line 226
    :cond_0
    aget-object v2, v1, p1

    invoke-virtual {v2}, Lcom/anythink/basead/ui/OwnNativeAdView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 227
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string v1, "OwnNativeAdView View don\'t contain any child views."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1

    .line 231
    :cond_1
    aget-object p1, v1, p1

    iput-object p1, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    return v0
.end method

.method private c(Landroid/view/View;)V
    .locals 4

    .line 268
    iput-object p1, p0, Lcom/anythink/basead/e/h;->e:Landroid/view/View;

    .line 269
    new-instance v0, Lcom/anythink/basead/e/h$3;

    invoke-direct {v0, p0}, Lcom/anythink/basead/e/h$3;-><init>(Lcom/anythink/basead/e/h;)V

    .line 276
    iget-object v1, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    if-nez v1, :cond_0

    .line 277
    new-instance v1, Lcom/anythink/basead/d/c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/anythink/basead/d/c;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    instance-of v1, v1, Lcom/anythink/core/common/d/t;

    if-eqz v1, :cond_1

    .line 281
    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    iget-object v1, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    iget-object v2, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    invoke-static {v2}, Lcom/anythink/basead/e/b/a;->a(Lcom/anythink/core/common/d/i;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/anythink/basead/e/b/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 284
    :cond_1
    iget-object v1, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    instance-of v1, v1, Lcom/anythink/core/common/d/f;

    if-eqz v1, :cond_2

    .line 285
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    const/16 v3, 0x42

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;I)V

    .line 286
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v1, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    check-cast v2, Lcom/anythink/core/common/d/f;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    invoke-virtual {v1, p1, v0}, Lcom/anythink/basead/d/c;->a(Landroid/view/View;Lcom/anythink/basead/d/b;)V

    return-void
.end method

.method private j()V
    .locals 5

    .line 295
    iget-boolean v0, p0, Lcom/anythink/basead/e/h;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 299
    iput-boolean v0, p0, Lcom/anythink/basead/e/h;->f:Z

    .line 300
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    instance-of v0, v0, Lcom/anythink/core/common/d/t;

    if-eqz v0, :cond_1

    .line 301
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    iget-object v4, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v4, v4, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    if-eqz v0, :cond_2

    .line 305
    new-instance v0, Lcom/anythink/basead/c/h;

    iget-object v1, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    iget-object v1, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/OwnNativeAdView;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->f:I

    .line 307
    iget-object v1, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/OwnNativeAdView;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->e:I

    const/16 v1, 0x8

    .line 308
    iget-object v2, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    invoke-static {v1, v2, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 309
    iget-object v0, p0, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_2

    .line 310
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;ZZLcom/anythink/basead/ui/MediaAdView$a;)Landroid/view/View;
    .locals 8

    .line 160
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    if-eqz p2, :cond_1

    .line 164
    iget-object p2, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    instance-of p2, p2, Lcom/anythink/core/common/d/f;

    if-eqz p2, :cond_1

    .line 165
    new-instance p2, Lcom/anythink/basead/ui/MediaAdView;

    iget-object v4, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    iget-object v0, p0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v5, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    new-instance v7, Lcom/anythink/basead/e/h$2;

    invoke-direct {v7, p0, p4}, Lcom/anythink/basead/e/h$2;-><init>(Lcom/anythink/basead/e/h;Lcom/anythink/basead/ui/MediaAdView$a;)V

    move-object v2, p2

    move-object v3, p1

    move v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/anythink/basead/ui/MediaAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ZLcom/anythink/basead/ui/MediaAdView$a;)V

    .line 173
    iget p1, p0, Lcom/anythink/basead/e/h;->j:I

    iget p3, p0, Lcom/anythink/basead/e/h;->k:I

    invoke-virtual {p2, p1, p3}, Lcom/anythink/basead/ui/MediaAdView;->init(II)V

    .line 175
    new-instance p1, Lcom/anythink/basead/ui/OwnNativeAdView;

    iget-object p3, p0, Lcom/anythink/basead/e/h;->a:Landroid/content/Context;

    invoke-direct {p1, p3}, Lcom/anythink/basead/ui/OwnNativeAdView;-><init>(Landroid/content/Context;)V

    .line 176
    new-instance p3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 p4, -0x1

    invoke-direct {p3, p4, p4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p2, p3}, Lcom/anythink/basead/ui/OwnNativeAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    invoke-virtual {p2}, Lcom/anythink/basead/ui/MediaAdView;->getClickViews()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;Ljava/util/List;)V

    return-object p1

    :cond_1
    return-object v1
.end method

.method public final a()Lcom/anythink/core/common/d/h;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    return-object v0
.end method

.method public final a(II)V
    .locals 0

    .line 106
    iput p1, p0, Lcom/anythink/basead/e/h;->j:I

    .line 107
    iput p2, p0, Lcom/anythink/basead/e/h;->k:I

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .line 207
    invoke-direct {p0, p1}, Lcom/anythink/basead/e/h;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 210
    :cond_0
    invoke-direct {p0, p1}, Lcom/anythink/basead/e/h;->c(Landroid/view/View;)V

    .line 212
    iget-object v0, p0, Lcom/anythink/basead/e/h;->i:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1, v0}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Landroid/view/View;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 193
    invoke-direct {p0, p1}, Lcom/anythink/basead/e/h;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 196
    :cond_0
    invoke-direct {p0, p1}, Lcom/anythink/basead/e/h;->c(Landroid/view/View;)V

    if-eqz p2, :cond_2

    .line 198
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 199
    iget-object v0, p0, Lcom/anythink/basead/e/h;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    return-void

    .line 202
    :cond_2
    iget-object p2, p0, Lcom/anythink/basead/e/h;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final h()V
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {v0}, Lcom/anythink/basead/d/c;->a()V

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 2

    .line 317
    invoke-virtual {p0}, Lcom/anythink/basead/e/h;->h()V

    const/4 v0, 0x0

    .line 318
    iput-object v0, p0, Lcom/anythink/basead/e/h;->e:Landroid/view/View;

    .line 319
    iput-object v0, p0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    .line 320
    iput-object v0, p0, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    .line 321
    iput-object v0, p0, Lcom/anythink/basead/e/h;->d:Lcom/anythink/basead/a/b;

    .line 322
    iget-object v1, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    if-eqz v1, :cond_0

    .line 323
    invoke-virtual {v1}, Lcom/anythink/basead/d/c;->b()V

    .line 324
    iput-object v0, p0, Lcom/anythink/basead/e/h;->c:Lcom/anythink/basead/d/c;

    :cond_0
    return-void
.end method
