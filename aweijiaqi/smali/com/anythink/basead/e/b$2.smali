.class final Lcom/anythink/basead/e/b$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/e/b/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/e/b;->c(Lcom/anythink/basead/f/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/f/c;

.field final synthetic b:Lcom/anythink/basead/e/b;


# direct methods
.method constructor <init>(Lcom/anythink/basead/e/b;Lcom/anythink/basead/f/c;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/anythink/basead/e/b$2;->b:Lcom/anythink/basead/e/b;

    iput-object p2, p0, Lcom/anythink/basead/e/b$2;->a:Lcom/anythink/basead/f/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/anythink/basead/e/b$2;->a:Lcom/anythink/basead/f/c;

    if-eqz v0, :cond_0

    .line 168
    invoke-interface {v0}, Lcom/anythink/basead/f/c;->onAdDataLoaded()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/core/common/d/t;)V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/anythink/basead/e/b$2;->b:Lcom/anythink/basead/e/b;

    iput-object p1, v0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    .line 176
    new-instance p1, Lcom/anythink/basead/c/h;

    iget-object v0, p0, Lcom/anythink/basead/e/b$2;->b:Lcom/anythink/basead/e/b;

    iget-object v0, v0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-direct {p1, v0, v1}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/anythink/basead/e/b$2;->b:Lcom/anythink/basead/e/b;

    iget-object v0, v0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    const/16 v1, 0x21

    invoke-static {v1, v0, p1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 179
    iget-object p1, p0, Lcom/anythink/basead/e/b$2;->a:Lcom/anythink/basead/f/c;

    if-eqz p1, :cond_0

    .line 180
    invoke-interface {p1}, Lcom/anythink/basead/f/c;->onAdCacheLoaded()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/core/common/d/t;Lcom/anythink/basead/c/f;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 188
    new-instance v0, Lcom/anythink/basead/c/h;

    iget-object v1, p0, Lcom/anythink/basead/e/b$2;->b:Lcom/anythink/basead/e/b;

    iget-object v1, v1, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x22

    .line 189
    invoke-static {v1, p1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 192
    :cond_0
    iget-object p1, p0, Lcom/anythink/basead/e/b$2;->a:Lcom/anythink/basead/f/c;

    if-eqz p1, :cond_1

    .line 193
    invoke-interface {p1, p2}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    :cond_1
    return-void
.end method
