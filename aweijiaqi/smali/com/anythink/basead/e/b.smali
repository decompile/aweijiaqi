.class public abstract Lcom/anythink/basead/e/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/e/b$a;
    }
.end annotation


# instance fields
.field private a:I

.field protected b:Landroid/content/Context;

.field protected c:Lcom/anythink/core/common/d/i;

.field protected d:Lcom/anythink/basead/e/c;

.field protected e:Lcom/anythink/core/common/d/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/anythink/core/common/d/i;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    .line 46
    iput p2, p0, Lcom/anythink/basead/e/b;->a:I

    .line 47
    iput-object p3, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    return-void
.end method

.method private b(Lcom/anythink/basead/f/c;)V
    .locals 3

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/e/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    new-instance v2, Lcom/anythink/basead/e/b$1;

    invoke-direct {v2, p0, p1}, Lcom/anythink/basead/e/b$1;-><init>(Lcom/anythink/basead/e/b;Lcom/anythink/basead/f/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    const-string v0, "30001"

    const-string v1, "bidid\u3001placementid can not be null!"

    .line 115
    invoke-static {v0, v1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    .line 154
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    if-eqz p1, :cond_3

    .line 156
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-9999"

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    :cond_3
    return-void
.end method

.method private c(Lcom/anythink/basead/f/c;)V
    .locals 3

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/b/b;->a(Landroid/content/Context;)Lcom/anythink/basead/e/b/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    new-instance v2, Lcom/anythink/basead/e/b$2;

    invoke-direct {v2, p0, p1}, Lcom/anythink/basead/e/b$2;-><init>(Lcom/anythink/basead/e/b;Lcom/anythink/basead/f/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 198
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    if-eqz p1, :cond_0

    .line 200
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-9999"

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/anythink/core/common/d/u;)Ljava/lang/String;
    .locals 3

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget v1, v1, Lcom/anythink/core/common/d/i;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/u;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/anythink/basead/e/c;)V
    .locals 3

    .line 51
    iput-object p1, p0, Lcom/anythink/basead/e/b;->d:Lcom/anythink/basead/e/c;

    .line 52
    iget-object p1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object p1, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    instance-of p1, p1, Lcom/anythink/core/common/d/v;

    if-eqz p1, :cond_1

    .line 53
    iget-object p1, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object p1, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    check-cast p1, Lcom/anythink/core/common/d/v;

    iget-object v0, p0, Lcom/anythink/basead/e/b;->d:Lcom/anythink/basead/e/c;

    if-eqz p1, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    .line 1023
    :cond_0
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->a()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/common/d/v;->l(I)V

    .line 1024
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/common/d/v;->m(I)V

    .line 1026
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/common/d/v;->a(Ljava/lang/String;)V

    .line 1027
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/common/d/v;->g(I)V

    .line 1029
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->e()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/common/d/v;->f(I)V

    .line 1030
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->f()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p1, v1, v2}, Lcom/anythink/core/common/d/v;->a(J)V

    .line 1031
    invoke-virtual {v0}, Lcom/anythink/basead/e/c;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/anythink/core/common/d/v;->e(I)V

    nop

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/anythink/basead/f/c;)V
    .locals 4

    .line 91
    sget-object v0, Lcom/anythink/basead/e/b$3;->a:[I

    iget v1, p0, Lcom/anythink/basead/e/b;->a:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    aget v0, v0, v1

    const-string v1, "-9999"

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 1164
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/b/b;->a(Landroid/content/Context;)Lcom/anythink/basead/e/b/b;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    new-instance v3, Lcom/anythink/basead/e/b$2;

    invoke-direct {v3, p0, p1}, Lcom/anythink/basead/e/b$2;-><init>(Lcom/anythink/basead/e/b;Lcom/anythink/basead/f/c;)V

    invoke-virtual {v0, v2, v3}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 1198
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1200
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    :goto_0
    return-void

    .line 1113
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 1120
    :cond_2
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/e/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    new-instance v3, Lcom/anythink/basead/e/b$1;

    invoke-direct {v3, p0, p1}, Lcom/anythink/basead/e/b$1;-><init>(Lcom/anythink/basead/e/b;Lcom/anythink/basead/f/c;)V

    invoke-virtual {v0, v2, v3}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/a/a$a;)V

    return-void

    :cond_3
    :goto_1
    const-string v0, "30001"

    const-string v2, "bidid\u3001placementid can not be null!"

    .line 1115
    invoke-static {v0, v2}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_1
    move-exception v0

    .line 1154
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1156
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    .line 211
    iput-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    return-void
.end method

.method public final c()Z
    .locals 4

    .line 59
    sget-object v0, Lcom/anythink/basead/e/b$3;->a:[I

    iget v1, p0, Lcom/anythink/basead/e/b;->a:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    aget v0, v0, v1

    const/4 v1, 0x0

    if-eq v0, v2, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    return v1

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    if-nez v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/b/b;->a(Landroid/content/Context;)Lcom/anythink/basead/e/b/b;

    move-result-object v0

    iget-object v3, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    invoke-virtual {v0, v3}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/t;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/anythink/core/common/d/t;->w()Z

    move-result v3

    if-nez v3, :cond_1

    .line 73
    iput-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-static {v0, v3}, Lcom/anythink/basead/a/a/b;->a(Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    return v1

    .line 61
    :cond_3
    iget-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    if-nez v0, :cond_4

    .line 62
    iget-object v0, p0, Lcom/anythink/basead/e/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/e/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/e/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    invoke-virtual {v0, v3}, Lcom/anythink/basead/e/a/a;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    .line 65
    :cond_4
    iget-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/anythink/basead/e/b;->c:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-static {v0, v3}, Lcom/anythink/basead/a/a/b;->a(Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v2

    :cond_5
    return v1
.end method

.method public final d()Lcom/anythink/core/common/d/h;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/anythink/basead/e/b;->e:Lcom/anythink/core/common/d/u;

    return-object v0
.end method
