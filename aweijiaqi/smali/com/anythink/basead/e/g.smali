.class public final Lcom/anythink/basead/e/g;
.super Lcom/anythink/basead/e/b;


# instance fields
.field a:Lcom/anythink/basead/f/a;

.field f:Lcom/anythink/basead/ui/SplashAdView;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/anythink/core/common/d/i;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/e/b;-><init>(Landroid/content/Context;ILcom/anythink/core/common/d/i;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .locals 2

    .line 34
    invoke-super {p0}, Lcom/anythink/basead/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/basead/e/g$1;

    invoke-direct {v1, p0, p1}, Lcom/anythink/basead/e/g$1;-><init>(Lcom/anythink/basead/e/g;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/anythink/basead/e/g;->a:Lcom/anythink/basead/f/a;

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    .line 52
    iput-object v0, p0, Lcom/anythink/basead/e/g;->a:Lcom/anythink/basead/f/a;

    .line 53
    iget-object v1, p0, Lcom/anythink/basead/e/g;->f:Lcom/anythink/basead/ui/SplashAdView;

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {v1}, Lcom/anythink/basead/ui/SplashAdView;->destroy()V

    .line 55
    iput-object v0, p0, Lcom/anythink/basead/e/g;->f:Lcom/anythink/basead/ui/SplashAdView;

    :cond_0
    return-void
.end method
