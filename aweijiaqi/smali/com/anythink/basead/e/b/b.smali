.class public final Lcom/anythink/basead/e/b/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/e/b/b$a;
    }
.end annotation


# static fields
.field private static c:Lcom/anythink/basead/e/b/b;


# instance fields
.field a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/e/b/b;->b:Landroid/content/Context;

    .line 39
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object p1, p0, Lcom/anythink/basead/e/b/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/e/b/b;)Landroid/content/Context;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/anythink/basead/e/b/b;->b:Landroid/content/Context;

    return-object p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/basead/e/b/b;
    .locals 2

    const-class v0, Lcom/anythink/basead/e/b/b;

    monitor-enter v0

    .line 43
    :try_start_0
    sget-object v1, Lcom/anythink/basead/e/b/b;->c:Lcom/anythink/basead/e/b/b;

    if-nez v1, :cond_0

    .line 44
    new-instance v1, Lcom/anythink/basead/e/b/b;

    invoke-direct {v1, p0}, Lcom/anythink/basead/e/b/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/basead/e/b/b;->c:Lcom/anythink/basead/e/b/b;

    .line 46
    :cond_0
    sget-object p0, Lcom/anythink/basead/e/b/b;->c:Lcom/anythink/basead/e/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/basead/e/b/b;Lcom/anythink/core/common/d/t;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/t;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/t;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    .locals 3

    .line 171
    invoke-static {}, Lcom/anythink/basead/a/d;->a()Lcom/anythink/basead/a/d;

    iget-object v0, p2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v1, p2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    new-instance v2, Lcom/anythink/basead/e/b/b$2;

    invoke-direct {v2, p0, p2, p3, p1}, Lcom/anythink/basead/e/b/b$2;-><init>(Lcom/anythink/basead/e/b/b;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;Lcom/anythink/core/common/d/t;)V

    invoke-static {v0, p1, v1, v2}, Lcom/anythink/basead/a/d;->a(Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;Lcom/anythink/basead/a/a/a$a;)V

    return-void
.end method

.method private b(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    .locals 7

    .line 65
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/t;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 70
    invoke-virtual {v0}, Lcom/anythink/core/common/d/t;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_1

    .line 164
    invoke-interface {p2}, Lcom/anythink/basead/e/b/b$a;->a()V

    .line 166
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/t;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V

    return-void

    .line 73
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 75
    :try_start_1
    iget-object v0, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 76
    aget-object v2, v0, v1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x1

    .line 77
    :try_start_2
    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    :catchall_1
    const/4 v2, 0x0

    :catchall_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 82
    :goto_2
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/basead/e/b/b;->b:Landroid/content/Context;

    iget-object v5, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v6, p1, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    .line 84
    invoke-static {v5, v6}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 83
    invoke-virtual {v3, v4, v5}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 86
    new-instance v4, Lcom/anythink/basead/h/d;

    invoke-direct {v4, p1, v2, v0, v3}, Lcom/anythink/basead/h/d;-><init>(Lcom/anythink/core/common/d/i;II[Ljava/lang/String;)V

    .line 87
    new-instance v0, Lcom/anythink/basead/e/b/b$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/anythink/basead/e/b/b$1;-><init>(Lcom/anythink/basead/e/b/b;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V

    invoke-virtual {v4, v1, v0}, Lcom/anythink/basead/h/d;->a(ILcom/anythink/core/common/e/g;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/t;
    .locals 4

    .line 192
    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    invoke-static {p1}, Lcom/anythink/basead/e/b/a;->a(Lcom/anythink/core/common/d/i;)Ljava/lang/String;

    move-result-object v0

    .line 194
    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    iget-object v1, p0, Lcom/anythink/basead/e/b/b;->b:Landroid/content/Context;

    .line 2035
    sget-object v2, Lcom/anythink/core/common/b/e;->u:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v1, v2, v0, v3}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 199
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1}, Lcom/anythink/basead/e/b/c;->a(Lcom/anythink/core/common/d/i;Lorg/json/JSONObject;)Lcom/anythink/core/common/d/t;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :goto_0
    if-eqz v2, :cond_1

    .line 205
    invoke-static {p1, v2}, Lcom/anythink/basead/e/c/a;->a(Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/u;)V

    :cond_1
    return-object v2
.end method

.method public final a(Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V
    .locals 7

    .line 51
    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    invoke-static {p1}, Lcom/anythink/basead/e/b/a;->a(Lcom/anythink/core/common/d/i;)Ljava/lang/String;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/anythink/basead/e/b/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/anythink/basead/e/b/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, "20005"

    const-string v0, "Offer data is loading."

    .line 54
    invoke-static {p1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {p2, v2, p1}, Lcom/anythink/basead/e/b/b$a;->a(Lcom/anythink/core/common/d/t;Lcom/anythink/basead/c/f;)V

    return-void

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/anythink/basead/e/b/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/i;)Lcom/anythink/core/common/d/t;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :goto_0
    if-eqz v2, :cond_2

    .line 1070
    invoke-virtual {v2}, Lcom/anythink/core/common/d/t;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 1164
    :cond_1
    invoke-interface {p2}, Lcom/anythink/basead/e/b/b$a;->a()V

    .line 1166
    invoke-direct {p0, v2, p1, p2}, Lcom/anythink/basead/e/b/b;->a(Lcom/anythink/core/common/d/t;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V

    return-void

    .line 1073
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 1075
    :try_start_1
    iget-object v0, p1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1076
    aget-object v2, v0, v1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x1

    .line 1077
    :try_start_2
    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    :catchall_1
    const/4 v2, 0x0

    :catchall_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1082
    :goto_2
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/basead/e/b/b;->b:Landroid/content/Context;

    iget-object v5, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v6, p1, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    .line 1084
    invoke-static {v5, v6}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1083
    invoke-virtual {v3, v4, v5}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1086
    new-instance v4, Lcom/anythink/basead/h/d;

    invoke-direct {v4, p1, v2, v0, v3}, Lcom/anythink/basead/h/d;-><init>(Lcom/anythink/core/common/d/i;II[Ljava/lang/String;)V

    .line 1087
    new-instance v0, Lcom/anythink/basead/e/b/b$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/anythink/basead/e/b/b$1;-><init>(Lcom/anythink/basead/e/b/b;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/e/b/b$a;)V

    invoke-virtual {v4, v1, v0}, Lcom/anythink/basead/h/d;->a(ILcom/anythink/core/common/e/g;)V

    return-void
.end method
