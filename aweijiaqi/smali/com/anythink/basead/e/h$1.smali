.class final Lcom/anythink/basead/e/h$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/basead/e/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/e/h;


# direct methods
.method constructor <init>(Lcom/anythink/basead/e/h;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 58
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    if-eqz v0, :cond_2

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 60
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->d:Lcom/anythink/basead/a/b;

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    new-instance v1, Lcom/anythink/basead/a/b;

    iget-object v2, v0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v3, v3, Lcom/anythink/basead/e/h;->g:Lcom/anythink/core/common/d/u;

    invoke-direct {v1, p1, v2, v3}, Lcom/anythink/basead/a/b;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V

    iput-object v1, v0, Lcom/anythink/basead/e/h;->d:Lcom/anythink/basead/a/b;

    .line 64
    :cond_0
    iget-object p1, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object p1, p1, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    if-eqz p1, :cond_1

    .line 65
    iget-object p1, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object p1, p1, Lcom/anythink/basead/e/h;->b:Lcom/anythink/basead/f/a;

    invoke-interface {p1}, Lcom/anythink/basead/f/a;->onAdClick()V

    .line 68
    :cond_1
    new-instance p1, Lcom/anythink/basead/c/h;

    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->h:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-direct {p1, v0, v1}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/OwnNativeAdView;->getHeight()I

    move-result v0

    iput v0, p1, Lcom/anythink/basead/c/h;->f:I

    .line 70
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/OwnNativeAdView;->getWidth()I

    move-result v0

    iput v0, p1, Lcom/anythink/basead/c/h;->e:I

    .line 71
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->l:Lcom/anythink/basead/ui/OwnNativeAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/OwnNativeAdView;->getAdClickRecord()Lcom/anythink/basead/c/b;

    move-result-object v0

    iput-object v0, p1, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    .line 74
    iget-object v0, p0, Lcom/anythink/basead/e/h$1;->a:Lcom/anythink/basead/e/h;

    iget-object v0, v0, Lcom/anythink/basead/e/h;->d:Lcom/anythink/basead/a/b;

    new-instance v1, Lcom/anythink/basead/e/h$1$1;

    invoke-direct {v1, p0}, Lcom/anythink/basead/e/h$1$1;-><init>(Lcom/anythink/basead/e/h$1;)V

    invoke-virtual {v0, p1, v1}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    :cond_2
    return-void
.end method
