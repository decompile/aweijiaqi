.class public Lcom/anythink/basead/e/f;
.super Lcom/anythink/basead/e/b;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private f:Lcom/anythink/basead/f/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const-class v0, Lcom/anythink/basead/e/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/e/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/anythink/core/common/d/i;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/e/b;-><init>(Landroid/content/Context;ILcom/anythink/core/common/d/i;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/e/f;)Lcom/anythink/basead/f/e;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/anythink/basead/f/e;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 45
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/e/f;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 46
    iget-object p1, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    if-eqz p1, :cond_0

    .line 47
    iget-object p1, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    const-string v1, "30001"

    const-string v2, "No fill, offer = null!"

    invoke-static {v1, v2}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/anythink/basead/f/e;->onVideoShowFailed(Lcom/anythink/basead/c/f;)V

    .line 49
    :cond_0
    iput-object v0, p0, Lcom/anythink/basead/e/f;->e:Lcom/anythink/core/common/d/u;

    return-void

    :cond_1
    const-string v1, "extra_scenario"

    .line 53
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_orientation"

    .line 54
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 55
    iget-object v2, p0, Lcom/anythink/basead/e/f;->e:Lcom/anythink/core/common/d/u;

    invoke-virtual {p0, v2}, Lcom/anythink/basead/e/f;->a(Lcom/anythink/core/common/d/u;)Ljava/lang/String;

    move-result-object v2

    .line 57
    invoke-static {}, Lcom/anythink/basead/f/b;->a()Lcom/anythink/basead/f/b;

    move-result-object v3

    new-instance v4, Lcom/anythink/basead/e/f$1;

    invoke-direct {v4, p0, v2}, Lcom/anythink/basead/e/f$1;-><init>(Lcom/anythink/basead/e/f;Ljava/lang/String;)V

    invoke-virtual {v3, v2, v4}, Lcom/anythink/basead/f/b;->a(Ljava/lang/String;Lcom/anythink/basead/f/b$b;)V

    .line 121
    new-instance v3, Lcom/anythink/basead/c/a;

    invoke-direct {v3}, Lcom/anythink/basead/c/a;-><init>()V

    .line 122
    iget-object v4, p0, Lcom/anythink/basead/e/f;->e:Lcom/anythink/core/common/d/u;

    iput-object v4, v3, Lcom/anythink/basead/c/a;->c:Lcom/anythink/core/common/d/h;

    .line 123
    iput-object v2, v3, Lcom/anythink/basead/c/a;->d:Ljava/lang/String;

    const/4 v2, 0x1

    .line 124
    iput v2, v3, Lcom/anythink/basead/c/a;->a:I

    .line 125
    iget-object v2, p0, Lcom/anythink/basead/e/f;->c:Lcom/anythink/core/common/d/i;

    iput-object v2, v3, Lcom/anythink/basead/c/a;->g:Lcom/anythink/core/common/d/i;

    .line 126
    iput p1, v3, Lcom/anythink/basead/c/a;->e:I

    .line 127
    iput-object v1, v3, Lcom/anythink/basead/c/a;->b:Ljava/lang/String;

    .line 129
    iget-object p1, p0, Lcom/anythink/basead/e/f;->b:Landroid/content/Context;

    invoke-static {p1, v3}, Lcom/anythink/basead/ui/BaseAdActivity;->a(Landroid/content/Context;Lcom/anythink/basead/c/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 131
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    iget-object v1, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    if-eqz v1, :cond_2

    .line 133
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v2, "-9999"

    invoke-static {v2, p1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/anythink/basead/f/e;->onVideoShowFailed(Lcom/anythink/basead/c/f;)V

    .line 135
    :cond_2
    iput-object v0, p0, Lcom/anythink/basead/e/f;->e:Lcom/anythink/core/common/d/u;

    return-void
.end method

.method public final b()V
    .locals 1

    .line 143
    invoke-super {p0}, Lcom/anythink/basead/e/b;->b()V

    const/4 v0, 0x0

    .line 144
    iput-object v0, p0, Lcom/anythink/basead/e/f;->f:Lcom/anythink/basead/f/e;

    return-void
.end method
