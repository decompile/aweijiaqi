.class final Lcom/anythink/basead/ui/SplashAdView$11;
.super Landroid/os/CountDownTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/ui/SplashAdView;->p()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/ui/SplashAdView;


# direct methods
.method constructor <init>(Lcom/anythink/basead/ui/SplashAdView;J)V
    .locals 2

    .line 428
    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    const-wide/16 v0, 0x3e8

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 2

    .line 442
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/anythink/basead/ui/SplashAdView;->y:Z

    .line 444
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdClosed()V

    :cond_0
    return-void
.end method

.method public final onTick(J)V
    .locals 6

    .line 432
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->f()I

    move-result v0

    const-wide/16 v1, 0x1

    const-wide/16 v3, 0x3e8

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    div-long/2addr p1, v3

    add-long/2addr p1, v1

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "s "

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object p1, p1, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$11;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    div-long/2addr p1, v3

    add-long/2addr p1, v1

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " s"

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
