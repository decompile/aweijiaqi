.class public Lcom/anythink/basead/ui/BannerView;
.super Landroid/widget/RelativeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/ui/BannerView$a;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Lcom/anythink/basead/ui/BannerView$a;

.field private h:I

.field private i:Lcom/anythink/core/common/d/j;

.field private final j:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ILcom/anythink/basead/ui/BannerView$a;)V
    .locals 4

    .line 45
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 170
    new-instance v0, Lcom/anythink/basead/ui/BannerView$3;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/BannerView$3;-><init>(Lcom/anythink/basead/ui/BannerView;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    .line 46
    iput-object p5, p0, Lcom/anythink/basead/ui/BannerView;->g:Lcom/anythink/basead/ui/BannerView$a;

    .line 47
    iput p4, p0, Lcom/anythink/basead/ui/BannerView;->h:I

    .line 48
    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->i:Lcom/anythink/core/common/d/j;

    .line 1057
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    .line 1058
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p4

    const-string p5, "myoffer_bottom_banner"

    const-string v0, "layout"

    invoke-static {p4, p5, v0}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p4

    const/4 p5, 0x1

    .line 1057
    invoke-virtual {p3, p4, p0, p5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    .line 1060
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p3

    const-string p4, "id"

    const-string v0, "myoffer_banner_view_id"

    invoke-static {p3, v0, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/anythink/basead/ui/BannerView;->setId(I)V

    .line 1062
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "myoffer_iv_banner_icon"

    invoke-static {v0, v1, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    .line 1063
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "myoffer_tv_banner_title"

    invoke-static {v0, v1, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    .line 1064
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "myoffer_tv_banner_desc"

    invoke-static {v0, v1, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    .line 1065
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "myoffer_btn_banner_cta"

    invoke-static {v0, v1, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/Button;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    .line 1066
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "myoffer_iv_logo"

    invoke-static {v0, v1, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p4

    invoke-virtual {p3, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    .line 1074
    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object p3

    .line 1075
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_0

    .line 1076
    iget-object p4, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {p4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p4

    .line 1077
    iget v0, p4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1078
    iget p4, p4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1079
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/res/e;

    invoke-direct {v2, p5, p3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v3, Lcom/anythink/basead/ui/BannerView$1;

    invoke-direct {v3, p0, p3}, Lcom/anythink/basead/ui/BannerView$1;-><init>(Lcom/anythink/basead/ui/BannerView;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, p4, v3}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 1095
    :cond_0
    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object p3

    .line 1096
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_1

    .line 1097
    iget-object p4, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    invoke-virtual {p4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p4

    .line 1098
    iget v0, p4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1099
    iget p4, p4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1100
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/res/e;

    invoke-direct {v2, p5, p3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v3, Lcom/anythink/basead/ui/BannerView$2;

    invoke-direct {v3, p0, p3}, Lcom/anythink/basead/ui/BannerView$2;-><init>(Lcom/anythink/basead/ui/BannerView;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, p4, v3}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 1115
    :cond_1
    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const/16 p4, 0x8

    if-eqz p3, :cond_2

    .line 1116
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {p3, p4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1119
    :cond_2
    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 1120
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    const/4 v0, 0x2

    const/high16 v1, 0x41880000    # 17.0f

    invoke-virtual {p3, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1121
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    invoke-static {p5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1122
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    invoke-virtual {p3, p4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1125
    :cond_3
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1126
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1128
    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_4

    .line 1129
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1130
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1132
    :cond_4
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    invoke-virtual {p2, p4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1161
    :goto_0
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1162
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1163
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1164
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1167
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    iget-object p3, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/high16 p2, 0x41000000    # 8.0f

    .line 2137
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p3

    invoke-static {p5, p2, p3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    float-to-int p2, p2

    .line 2140
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p3

    const/high16 p4, 0x42920000    # 73.0f

    invoke-static {p3, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p3

    .line 2141
    iget-object p4, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {p4}, Landroid/widget/ImageView;->getVisibility()I

    move-result p4

    if-eqz p4, :cond_5

    .line 2142
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object p3

    const/high16 p4, 0x42700000    # 60.0f

    invoke-static {p3, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p3

    .line 2151
    :cond_5
    new-instance p4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 p5, -0x1

    invoke-direct {p4, p5, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p3, 0xc

    .line 2152
    invoke-virtual {p4, p3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 p3, 0xb

    .line 2153
    invoke-virtual {p4, p3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2154
    iput p2, p4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2155
    iput p2, p4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2156
    iput p2, p4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2157
    invoke-virtual {p1, p0, p4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/BannerView;)Landroid/widget/ImageView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    return-object p0
.end method

.method private a()V
    .locals 4

    .line 57
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_bottom_banner"

    const-string v3, "layout"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    .line 57
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    .line 60
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "myoffer_banner_view_id"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/BannerView;->setId(I)V

    .line 62
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_iv_banner_icon"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_tv_banner_title"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_tv_banner_desc"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_btn_banner_cta"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    .line 66
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_iv_logo"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 4

    .line 137
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41000000    # 8.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 140
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x42920000    # 73.0f

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v1

    .line 141
    iget-object v2, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x42700000    # 60.0f

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v1

    .line 151
    :cond_0
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    .line 152
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xb

    .line 153
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 154
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 155
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 156
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 157
    invoke-virtual {p1, p0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/h;)V
    .locals 7

    .line 74
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 77
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 78
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 79
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v4

    new-instance v5, Lcom/anythink/core/common/res/e;

    invoke-direct {v5, v2, v0}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v6, Lcom/anythink/basead/ui/BannerView$1;

    invoke-direct {v6, p0, v0}, Lcom/anythink/basead/ui/BannerView$1;-><init>(Lcom/anythink/basead/ui/BannerView;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v3, v1, v6}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 98
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 99
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 100
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v4

    new-instance v5, Lcom/anythink/core/common/res/e;

    invoke-direct {v5, v2, v0}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v6, Lcom/anythink/basead/ui/BannerView$2;

    invoke-direct {v6, p0, v0}, Lcom/anythink/basead/ui/BannerView$2;-><init>(Lcom/anythink/basead/ui/BannerView;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v3, v1, v6}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 115
    :cond_1
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    :cond_2
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    const/4 v3, 0x2

    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 121
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 122
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 129
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 132
    :cond_4
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/basead/ui/BannerView;)Landroid/widget/ImageView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    return-object p0
.end method

.method private b()V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerView;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/anythink/basead/ui/BannerView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/BannerView;)Lcom/anythink/core/common/d/j;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/basead/ui/BannerView;->i:Lcom/anythink/core/common/d/j;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/basead/ui/BannerView;)Landroid/widget/Button;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/basead/ui/BannerView;->f:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/basead/ui/BannerView;)Lcom/anythink/basead/ui/BannerView$a;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/basead/ui/BannerView;->g:Lcom/anythink/basead/ui/BannerView$a;

    return-object p0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 200
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getWidth()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getHeight()I

    move-result v0

    int-to-float v5, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v0

    .line 201
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 202
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/anythink/basead/ui/a/a;->a(Landroid/graphics/Canvas;III)V

    .line 203
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 191
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method
