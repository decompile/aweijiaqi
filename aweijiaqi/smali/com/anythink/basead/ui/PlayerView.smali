.class public Lcom/anythink/basead/ui/PlayerView;
.super Landroid/widget/RelativeLayout;

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/ui/PlayerView$a;,
        Lcom/anythink/basead/ui/PlayerView$b;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:Lcom/anythink/basead/ui/CountDownView;

.field private K:Landroid/widget/ImageView;

.field private L:Landroid/widget/ImageView;

.field private final M:I

.field private final N:I

.field private final O:I

.field private P:Z

.field private Q:J

.field private R:Ljava/lang/Thread;

.field private S:Landroid/widget/TextView;

.field private T:Z

.field private a:Landroid/media/MediaPlayer;

.field private b:Landroid/graphics/SurfaceTexture;

.field private c:Landroid/view/TextureView;

.field private d:Landroid/view/Surface;

.field private e:Ljava/io/FileInputStream;

.field private f:Ljava/io/FileDescriptor;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Lcom/anythink/basead/ui/PlayerView$a;

.field private w:Landroid/os/Handler;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    const-class v0, Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/anythink/basead/ui/PlayerView$a;)V
    .locals 5

    .line 106
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    .line 59
    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    const/4 v1, 0x0

    .line 68
    iput-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->r:Z

    .line 69
    iput-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->s:Z

    .line 70
    iput-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    .line 71
    iput-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    const/16 v2, 0x1d

    .line 77
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->x:I

    const/16 v2, 0x13

    .line 78
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->y:I

    .line 79
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->z:I

    const/16 v2, 0x1e

    .line 80
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->A:I

    const/16 v2, 0x46

    .line 81
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->B:I

    const/4 v2, 0x1

    .line 94
    iput v2, p0, Lcom/anythink/basead/ui/PlayerView;->M:I

    const/4 v3, 0x2

    .line 95
    iput v3, p0, Lcom/anythink/basead/ui/PlayerView;->N:I

    const/4 v3, 0x3

    .line 96
    iput v3, p0, Lcom/anythink/basead/ui/PlayerView;->O:I

    .line 107
    iput-object p2, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    .line 109
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v3, "myoffer_player_view_id"

    const-string v4, "id"

    invoke-static {p2, v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/anythink/basead/ui/PlayerView;->setId(I)V

    .line 110
    invoke-virtual {p0, v2}, Lcom/anythink/basead/ui/PlayerView;->setSaveEnabled(Z)V

    .line 1165
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1166
    invoke-virtual {p1, p0, v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 113
    new-instance p1, Lcom/anythink/basead/ui/PlayerView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/anythink/basead/ui/PlayerView$1;-><init>(Lcom/anythink/basead/ui/PlayerView;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->w:Landroid/os/Handler;

    return-void
.end method

.method static synthetic A(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->k:I

    return p0
.end method

.method static synthetic B(Lcom/anythink/basead/ui/PlayerView;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->m()V

    return-void
.end method

.method static synthetic C(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    return v0
.end method

.method static synthetic a(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    return p0
.end method

.method static synthetic a(Lcom/anythink/basead/ui/PlayerView;I)I
    .locals 0

    .line 43
    iput p1, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    return p1
.end method

.method private a()V
    .locals 9

    .line 287
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "init..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    if-eqz v0, :cond_0

    const-string v1, "40002"

    const-string v2, "Video file error!"

    .line 291
    invoke-static {v1, v2}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/anythink/basead/ui/PlayerView$a;->a(Lcom/anythink/basead/c/f;)V

    :cond_0
    return-void

    .line 1310
    :cond_1
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->x:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    .line 1311
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->y:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    .line 1312
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->z:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->E:I

    .line 1313
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->A:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->F:I

    .line 1315
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "drawable"

    const-string v3, "myoffer_video_mute"

    invoke-static {v0, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->G:I

    .line 1316
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "myoffer_video_no_mute"

    invoke-static {v0, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->H:I

    .line 1317
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "myoffer_video_close"

    invoke-static {v0, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->I:I

    .line 1321
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    if-nez v0, :cond_5

    .line 1325
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1326
    iget-object v4, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2111
    invoke-static {v4}, Lcom/anythink/basead/a/a/e;->a(Ljava/io/FileDescriptor;)Lcom/anythink/basead/a/a/e$a;

    move-result-object v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    goto :goto_0

    .line 2116
    :cond_3
    iget v6, v4, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 2117
    iget v7, v4, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v6, v6

    mul-float v6, v6, v3

    int-to-float v7, v7

    div-float/2addr v6, v7

    int-to-float v7, v5

    mul-float v7, v7, v3

    int-to-float v8, v0

    div-float/2addr v7, v8

    cmpg-float v7, v6, v7

    if-gez v7, :cond_4

    .line 2124
    iput v0, v4, Lcom/anythink/basead/a/a/e$a;->b:I

    .line 2125
    iget v0, v4, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v0, v0

    mul-float v0, v0, v6

    float-to-int v0, v0

    iput v0, v4, Lcom/anythink/basead/a/a/e$a;->a:I

    goto :goto_0

    .line 2128
    :cond_4
    iput v5, v4, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 2129
    iget v0, v4, Lcom/anythink/basead/a/a/e$a;->a:I

    int-to-float v0, v0

    div-float/2addr v0, v6

    float-to-int v0, v0

    iput v0, v4, Lcom/anythink/basead/a/a/e$a;->b:I

    :goto_0
    if-eqz v4, :cond_5

    .line 1329
    iget v0, v4, Lcom/anythink/basead/a/a/e$a;->a:I

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    .line 1330
    iget v0, v4, Lcom/anythink/basead/a/a/e$a;->b:I

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->i:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1333
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2629
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    const/4 v4, -0x1

    if-nez v0, :cond_7

    .line 2630
    new-instance v0, Landroid/view/TextureView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    .line 2631
    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2632
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setKeepScreenOn(Z)V

    .line 2634
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2635
    iget v5, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    if-eqz v5, :cond_6

    iget v6, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    if-eqz v6, :cond_6

    .line 2636
    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2637
    iget v5, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    :cond_6
    const/16 v5, 0xd

    .line 2639
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2640
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->removeAllViews()V

    .line 2641
    iget-object v5, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    invoke-virtual {p0, v5, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2643
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    new-instance v5, Lcom/anythink/basead/ui/PlayerView$2;

    invoke-direct {v5, p0}, Lcom/anythink/basead/ui/PlayerView$2;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v5}, Landroid/view/TextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3565
    :cond_7
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-nez v0, :cond_b

    .line 3566
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    .line 3567
    iget-boolean v5, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    const/4 v6, 0x0

    if-eqz v5, :cond_8

    const/4 v5, 0x0

    goto :goto_2

    :cond_8
    const/high16 v5, 0x3f800000    # 1.0f

    :goto_2
    iget-boolean v7, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    if-eqz v7, :cond_9

    const/4 v3, 0x0

    :cond_9
    invoke-virtual {v0, v5, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 3568
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 3569
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/anythink/basead/ui/PlayerView$7;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/PlayerView$7;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 3593
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/anythink/basead/ui/PlayerView$8;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/PlayerView$8;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 3600
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    if-nez v0, :cond_a

    .line 3601
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/anythink/basead/ui/PlayerView$9;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/PlayerView$9;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 3615
    :cond_a
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/anythink/basead/ui/PlayerView$10;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/PlayerView$10;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 4339
    :cond_b
    invoke-virtual {p0, v2}, Lcom/anythink/basead/ui/PlayerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 4340
    invoke-virtual {p0, v2}, Lcom/anythink/basead/ui/PlayerView;->removeViewAt(I)V

    .line 4343
    :cond_c
    new-instance v0, Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/anythink/basead/ui/CountDownView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    .line 4344
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "id"

    const-string v6, "myoffer_count_down_view_id"

    invoke-static {v3, v6, v5}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/CountDownView;->setId(I)V

    .line 4345
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4346
    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->E:I

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 4347
    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->F:I

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 4348
    iget-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Lcom/anythink/basead/ui/CountDownView;->setVisibility(I)V

    .line 4349
    iget-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {p0, v3, v2, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x2

    .line 4353
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 4354
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->removeViewAt(I)V

    .line 4357
    :cond_d
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    .line 4358
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "myoffer_btn_mute_id"

    invoke-static {v7, v8, v5}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setId(I)V

    .line 4359
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v3, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4360
    iget-object v5, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v5}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v5

    invoke-virtual {v3, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 4361
    iget v5, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v5, 0x6

    .line 4362
    iget-object v7, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v7}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v7

    invoke-virtual {v3, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v5, 0x8

    .line 4363
    iget-object v7, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v7}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v7

    invoke-virtual {v3, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 4364
    iget-object v5, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4365
    iget-object v5, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    invoke-virtual {p0, v5, v0, v3}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4367
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    if-eqz v0, :cond_e

    .line 4368
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->G:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_3

    .line 4370
    :cond_e
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->H:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 4373
    :goto_3
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    new-instance v3, Lcom/anythink/basead/ui/PlayerView$3;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/PlayerView$3;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->T:Z

    if-nez v0, :cond_10

    .line 4454
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    .line 4455
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "myoffer_feedback_text"

    const-string v7, "string"

    invoke-static {v3, v5, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 4456
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4457
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 4458
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "myoffer_bg_feedback_button"

    invoke-static {v3, v4, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 4459
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    const/high16 v0, 0x41400000    # 12.0f

    .line 4461
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 4464
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4466
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    iget v3, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    .line 4467
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/high16 v1, 0x41f00000    # 30.0f

    .line 4468
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 4470
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    if-nez v1, :cond_f

    .line 4471
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->y:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_4

    .line 4473
    :cond_f
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->B:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 4476
    :goto_4
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4477
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4479
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$5;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$5;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_10
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 2

    .line 165
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    .line 166
    invoke-virtual {p1, p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/PlayerView;Z)Z
    .locals 0

    .line 43
    iput-boolean p1, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    return p1
.end method

.method static synthetic b(Lcom/anythink/basead/ui/PlayerView;I)I
    .locals 0

    .line 43
    iput p1, p0, Lcom/anythink/basead/ui/PlayerView;->k:I

    return p1
.end method

.method static synthetic b(Lcom/anythink/basead/ui/PlayerView;)Landroid/widget/ImageView;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    return-object p0
.end method

.method private b()V
    .locals 3

    .line 310
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->x:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    .line 311
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->y:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    .line 312
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->z:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->E:I

    .line 313
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->A:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->F:I

    .line 315
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "drawable"

    const-string v2, "myoffer_video_mute"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->G:I

    .line 316
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_video_no_mute"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->H:I

    .line 317
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_video_close"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->I:I

    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/PlayerView;I)I
    .locals 0

    .line 43
    iput p1, p0, Lcom/anythink/basead/ui/PlayerView;->l:I

    return p1
.end method

.method static synthetic c(Lcom/anythink/basead/ui/PlayerView;)J
    .locals 2

    .line 43
    iget-wide v0, p0, Lcom/anythink/basead/ui/PlayerView;->Q:J

    return-wide v0
.end method

.method private c()V
    .locals 6

    .line 321
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    if-eqz v0, :cond_0

    return-void

    .line 325
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 326
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 5111
    invoke-static {v1}, Lcom/anythink/basead/a/a/e;->a(Ljava/io/FileDescriptor;)Lcom/anythink/basead/a/a/e$a;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    .line 5116
    :cond_1
    iget v3, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 5117
    iget v4, v1, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v3, v3

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float v3, v3, v5

    int-to-float v4, v4

    div-float/2addr v3, v4

    int-to-float v4, v2

    mul-float v4, v4, v5

    int-to-float v5, v0

    div-float/2addr v4, v5

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    .line 5124
    iput v0, v1, Lcom/anythink/basead/a/a/e$a;->b:I

    .line 5125
    iget v0, v1, Lcom/anythink/basead/a/a/e$a;->b:I

    int-to-float v0, v0

    mul-float v0, v0, v3

    float-to-int v0, v0

    iput v0, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    goto :goto_0

    .line 5128
    :cond_2
    iput v2, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    .line 5129
    iget v0, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v1, Lcom/anythink/basead/a/a/e$a;->b:I

    :goto_0
    if-eqz v1, :cond_3

    .line 329
    iget v0, v1, Lcom/anythink/basead/a/a/e$a;->a:I

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    .line 330
    iget v0, v1, Lcom/anythink/basead/a/a/e$a;->b:I

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->i:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-void

    :catch_0
    move-exception v0

    .line 333
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method

.method static synthetic d(Lcom/anythink/basead/ui/PlayerView;I)I
    .locals 0

    .line 43
    iput p1, p0, Lcom/anythink/basead/ui/PlayerView;->m:I

    return p1
.end method

.method private d()V
    .locals 5

    const/4 v0, 0x1

    .line 339
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 340
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->removeViewAt(I)V

    .line 343
    :cond_0
    new-instance v1, Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/anythink/basead/ui/CountDownView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    .line 344
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_count_down_view_id"

    const-string v4, "id"

    invoke-static {v2, v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/CountDownView;->setId(I)V

    .line 345
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 346
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->E:I

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 347
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->F:I

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 348
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/anythink/basead/ui/CountDownView;->setVisibility(I)V

    .line 349
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {p0, v2, v0, v1}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic d(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->s:Z

    return p0
.end method

.method static synthetic e(Lcom/anythink/basead/ui/PlayerView;I)I
    .locals 0

    .line 43
    iput p1, p0, Lcom/anythink/basead/ui/PlayerView;->n:I

    return p1
.end method

.method private e()V
    .locals 5

    const/4 v0, 0x2

    .line 353
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 354
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/PlayerView;->removeViewAt(I)V

    .line 357
    :cond_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    .line 358
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_btn_mute_id"

    const-string v4, "id"

    invoke-static {v2, v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setId(I)V

    .line 359
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    .line 360
    iget-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v3}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 361
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v2, 0x6

    .line 362
    iget-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v3}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v2, 0x8

    .line 363
    iget-object v3, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v3}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 364
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v0, v1}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 367
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 373
    :goto_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$3;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$3;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic e(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    return p0
.end method

.method private f()V
    .locals 4

    .line 406
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    .line 407
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_btn_close_id"

    const-string v3, "id"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 408
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    .line 409
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 410
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 411
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v1

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 412
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 413
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 415
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->I:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 417
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/anythink/basead/ui/a/a;->a(Landroid/view/View;I)V

    .line 419
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$4;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$4;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic f(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->s:Z

    return v0
.end method

.method static synthetic g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    return-object p0
.end method

.method private g()V
    .locals 2

    .line 5436
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/basead/ui/CountDownView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5437
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/CountDownView;->setVisibility(I)V

    .line 5442
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5443
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5448
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5449
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private h()V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/basead/ui/CountDownView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/CountDownView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->o:Z

    return p0
.end method

.method static synthetic i(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->l:I

    return p0
.end method

.method private i()V
    .locals 2

    .line 442
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .line 448
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->o:Z

    return v0
.end method

.method private k()V
    .locals 4

    .line 454
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    .line 455
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_feedback_text"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 456
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 457
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 458
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_bg_feedback_button"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 459
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 461
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 464
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 466
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    const/4 v3, -0x2

    invoke-direct {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    .line 467
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 468
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v1, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 470
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    if-nez v2, :cond_0

    .line 471
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->y:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_0

    .line 473
    :cond_0
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->B:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 476
    :goto_0
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 479
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$5;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$5;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic k(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->p:Z

    return p0
.end method

.method static synthetic l(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->m:I

    return p0
.end method

.method private l()V
    .locals 2

    .line 490
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->R:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 493
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->r:Z

    .line 494
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$6;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$6;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->R:Ljava/lang/Thread;

    .line 510
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private m()V
    .locals 1

    const/4 v0, 0x0

    .line 514
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->r:Z

    const/4 v0, 0x0

    .line 515
    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->R:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic m(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->p:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .line 537
    invoke-static {}, Lcom/anythink/basead/a/d;->a()Lcom/anythink/basead/a/d;

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/anythink/basead/a/d;->a(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    .line 544
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 548
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    if-eqz v1, :cond_1

    .line 552
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    if-eqz v0, :cond_1

    .line 554
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    .line 556
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_1
    return v1
.end method

.method static synthetic n(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->q:Z

    return p0
.end method

.method static synthetic o(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->n:I

    return p0
.end method

.method private o()V
    .locals 5

    .line 565
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-nez v0, :cond_3

    .line 566
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    .line 567
    iget-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    :goto_0
    iget-boolean v4, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 568
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 569
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$7;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$7;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 593
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$8;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$8;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 600
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    if-nez v0, :cond_2

    .line 601
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$9;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$9;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 615
    :cond_2
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$10;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$10;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    :cond_3
    return-void
.end method

.method private p()V
    .locals 3

    .line 629
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    if-nez v0, :cond_1

    .line 630
    new-instance v0, Landroid/view/TextureView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    .line 631
    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 632
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setKeepScreenOn(Z)V

    .line 634
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 635
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->h:I

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    if-eqz v2, :cond_0

    .line 636
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 637
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->i:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    :cond_0
    const/16 v1, 0xd

    .line 639
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 640
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->removeAllViews()V

    .line 641
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 643
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->c:Landroid/view/TextureView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$2;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$2;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method static synthetic p(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->q:Z

    return v0
.end method

.method private q()V
    .locals 3

    .line 656
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->a()V

    .line 658
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 660
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->valid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 663
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "video resource valid - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->valid()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 668
    :try_start_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 672
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 674
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    if-nez v0, :cond_1

    .line 675
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->b:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    .line 677
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 678
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    return-void

    .line 661
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MyOffer video resource is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 682
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    if-eqz v1, :cond_3

    .line 683
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v2, "40002"

    invoke-static {v2, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/anythink/basead/ui/PlayerView$a;->a(Lcom/anythink/basead/c/f;)V

    :cond_3
    return-void
.end method

.method static synthetic q(Lcom/anythink/basead/ui/PlayerView;)V
    .locals 2

    .line 7436
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/basead/ui/CountDownView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7437
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/CountDownView;->setVisibility(I)V

    .line 7442
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7443
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7448
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7449
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method static synthetic r(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/CountDownView;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    return p0
.end method

.method static synthetic t(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->G:I

    return p0
.end method

.method static synthetic u(Lcom/anythink/basead/ui/PlayerView;)Landroid/widget/ImageView;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->K:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/basead/ui/PlayerView;)Landroid/media/MediaPlayer;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/basead/ui/PlayerView;)I
    .locals 0

    .line 43
    iget p0, p0, Lcom/anythink/basead/ui/PlayerView;->H:I

    return p0
.end method

.method static synthetic x(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/anythink/basead/ui/PlayerView;->r:Z

    return p0
.end method

.method static synthetic y(Lcom/anythink/basead/ui/PlayerView;)Landroid/os/Handler;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/anythink/basead/ui/PlayerView;->w:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/basead/ui/PlayerView;)Z
    .locals 1

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    return v0
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .line 756
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public getVideoLength()I
    .locals 1

    .line 760
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->k:I

    return v0
.end method

.method public isPlaying()Z
    .locals 2

    .line 749
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    if-eqz v1, :cond_0

    .line 750
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public load(Ljava/lang/String;)V
    .locals 0

    .line 523
    iput-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->g:Ljava/lang/String;

    .line 525
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->a()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 789
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 790
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "onDetachedFromWindow()..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->release()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .line 189
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "onRestoreInstanceState..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    check-cast p1, Lcom/anythink/basead/ui/PlayerView$b;

    .line 191
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/basead/ui/PlayerView$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p1}, Lcom/anythink/basead/ui/PlayerView$b;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 193
    iget v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->a:I

    iput v0, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    .line 194
    iget-boolean v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->b:Z

    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->o:Z

    .line 195
    iget-boolean v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->c:Z

    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->p:Z

    .line 196
    iget-boolean v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->d:Z

    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->q:Z

    .line 197
    iget-boolean v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->e:Z

    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->s:Z

    .line 198
    iget-boolean v0, p1, Lcom/anythink/basead/ui/PlayerView$b;->f:Z

    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    .line 199
    iget-boolean p1, p1, Lcom/anythink/basead/ui/PlayerView$b;->g:Z

    iput-boolean p1, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    .line 201
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    .line 202
    :goto_0
    iget-boolean v3, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v0, p1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_2
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 171
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 173
    new-instance v2, Lcom/anythink/basead/ui/PlayerView$b;

    invoke-direct {v2, v0}, Lcom/anythink/basead/ui/PlayerView$b;-><init>(Landroid/os/Parcelable;)V

    .line 175
    iget v0, p0, Lcom/anythink/basead/ui/PlayerView;->j:I

    iput v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->a:I

    .line 176
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->o:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->b:Z

    .line 177
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->p:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->c:Z

    .line 178
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->q:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->d:Z

    .line 179
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->s:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->e:Z

    .line 180
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->t:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->f:Z

    .line 181
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    iput-boolean v0, v2, Lcom/anythink/basead/ui/PlayerView$b;->g:Z

    .line 183
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/anythink/basead/ui/PlayerView$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .line 765
    sget-object p2, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string p3, "onSurfaceTextureAvailable()..."

    invoke-static {p2, p3}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iput-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->b:Landroid/graphics/SurfaceTexture;

    .line 6656
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->a()V

    .line 6658
    :try_start_0
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 6660
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {p1}, Ljava/io/FileDescriptor;->valid()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 6663
    sget-object p1, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "video resource valid - "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {p3}, Ljava/io/FileDescriptor;->valid()Z

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6666
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    iget-object p2, p0, Lcom/anythink/basead/ui/PlayerView;->f:Ljava/io/FileDescriptor;

    invoke-virtual {p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6668
    :try_start_1
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    if-eqz p1, :cond_0

    .line 6669
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->e:Ljava/io/FileInputStream;

    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 6672
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 6674
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    if-nez p1, :cond_1

    .line 6675
    new-instance p1, Landroid/view/Surface;

    iget-object p2, p0, Lcom/anythink/basead/ui/PlayerView;->b:Landroid/graphics/SurfaceTexture;

    invoke-direct {p1, p2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    .line 6677
    :cond_1
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    iget-object p2, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    invoke-virtual {p1, p2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 6678
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->prepareAsync()V

    return-void

    .line 6661
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "MyOffer video resource is valid"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    .line 6681
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 6682
    iget-object p2, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    if-eqz p2, :cond_3

    .line 6683
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string p3, "40002"

    invoke-static {p3, p1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/anythink/basead/ui/PlayerView$a;->a(Lcom/anythink/basead/c/f;)V

    :cond_3
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .line 777
    sget-object p1, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v0, "onSurfaceTextureDestroyed()..."

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->release()V

    const/4 p1, 0x1

    return p1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public pause()V
    .locals 2

    .line 708
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "pause()"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->m()V

    .line 710
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .line 727
    iget-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    if-nez v0, :cond_0

    return-void

    .line 730
    :cond_0
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "release..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    invoke-direct {p0}, Lcom/anythink/basead/ui/PlayerView;->m()V

    const/4 v0, 0x0

    .line 732
    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->b:Landroid/graphics/SurfaceTexture;

    .line 733
    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->d:Landroid/view/Surface;

    .line 734
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 735
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 736
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 738
    :cond_1
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 739
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 740
    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    .line 742
    :cond_2
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->w:Landroid/os/Handler;

    if-eqz v1, :cond_3

    .line 743
    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_3
    const/4 v0, 0x0

    .line 745
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    return-void
.end method

.method public removeFeedbackButton()V
    .locals 2

    .line 529
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setHideFeedbackButton(Z)V
    .locals 0

    .line 519
    iput-boolean p1, p0, Lcom/anythink/basead/ui/PlayerView;->T:Z

    return-void
.end method

.method public setSetting(Lcom/anythink/core/common/d/j;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 280
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/common/d/j;->n()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    .line 281
    invoke-virtual {p1}, Lcom/anythink/core/common/d/j;->o()I

    move-result p1

    mul-int/lit16 p1, p1, 0x3e8

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/anythink/basead/ui/PlayerView;->Q:J

    .line 282
    sget-object p1, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isMute - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->P:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object p1, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showCloseTime - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/anythink/basead/ui/PlayerView;->Q:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public showCloseButton()V
    .locals 4

    .line 689
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x1

    .line 691
    iget v2, p0, Lcom/anythink/basead/ui/PlayerView;->B:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 692
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->S:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6406
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    .line 6407
    invoke-virtual {p0}, Lcom/anythink/basead/ui/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_btn_close_id"

    const-string v3, "id"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 6408
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    .line 6409
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 6410
    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->D:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/4 v1, 0x6

    .line 6411
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v2}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v1, 0x8

    .line 6412
    iget-object v2, p0, Lcom/anythink/basead/ui/PlayerView;->J:Lcom/anythink/basead/ui/CountDownView;

    invoke-virtual {v2}, Lcom/anythink/basead/ui/CountDownView;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 6413
    iget-object v1, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 6415
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->I:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 6417
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    iget v1, p0, Lcom/anythink/basead/ui/PlayerView;->C:I

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/anythink/basead/ui/a/a;->a(Landroid/view/View;I)V

    .line 6419
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->L:Landroid/widget/ImageView;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$4;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$4;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public start()V
    .locals 2

    .line 700
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "start()"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/anythink/basead/ui/PlayerView;->u:Z

    if-eqz v1, :cond_0

    .line 702
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 6490
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->R:Ljava/lang/Thread;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 6493
    iput-boolean v0, p0, Lcom/anythink/basead/ui/PlayerView;->r:Z

    .line 6494
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/anythink/basead/ui/PlayerView$6;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/PlayerView$6;-><init>(Lcom/anythink/basead/ui/PlayerView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->R:Ljava/lang/Thread;

    .line 6510
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method public stop()V
    .locals 2

    .line 716
    sget-object v0, Lcom/anythink/basead/ui/PlayerView;->TAG:Ljava/lang/String;

    const-string v1, "stop()"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 721
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView;->v:Lcom/anythink/basead/ui/PlayerView$a;

    if-eqz v0, :cond_1

    .line 722
    invoke-interface {v0}, Lcom/anythink/basead/ui/PlayerView$a;->b()V

    :cond_1
    return-void
.end method
