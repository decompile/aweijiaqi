.class public Lcom/anythink/basead/ui/SplashAdView;
.super Lcom/anythink/basead/ui/BaseAdView;


# instance fields
.field private final A:Landroid/view/View$OnClickListener;

.field a:Landroid/widget/TextView;

.field b:Ljava/lang/String;

.field c:Landroid/os/CountDownTimer;

.field d:Lcom/anythink/basead/f/a;

.field e:Lcom/anythink/basead/ui/a;

.field v:I

.field w:I

.field x:Z

.field y:Z

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;)V

    const-string p1, "Skip"

    .line 40
    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    .line 50
    new-instance p1, Lcom/anythink/basead/ui/SplashAdView$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/SplashAdView$1;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->z:Landroid/view/View$OnClickListener;

    .line 59
    new-instance p1, Lcom/anythink/basead/ui/SplashAdView$4;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/SplashAdView$4;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->A:Landroid/view/View$OnClickListener;

    const/4 p1, 0x0

    .line 399
    iput-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    .line 400
    iput-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->y:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V

    const-string p1, "Skip"

    .line 40
    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    .line 50
    new-instance p1, Lcom/anythink/basead/ui/SplashAdView$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/SplashAdView$1;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->z:Landroid/view/View$OnClickListener;

    .line 59
    new-instance p1, Lcom/anythink/basead/ui/SplashAdView$4;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/SplashAdView$4;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->A:Landroid/view/View$OnClickListener;

    const/4 p1, 0x0

    .line 399
    iput-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    .line 400
    iput-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->y:Z

    .line 73
    iput-object p4, p0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    .line 1336
    new-instance p2, Lcom/anythink/basead/ui/SplashAdView$9;

    invoke-direct {p2, p0}, Lcom/anythink/basead/ui/SplashAdView$9;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    const/16 p3, 0x64

    .line 2037
    invoke-super {p0, p3, p2}, Lcom/anythink/basead/ui/BaseAdView;->a(ILjava/lang/Runnable;)V

    .line 2371
    iget-object p2, p0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    :goto_0
    if-ge p1, p2, :cond_1

    .line 2374
    iget-object p3, p0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    if-eqz p3, :cond_0

    .line 2376
    iget-object p4, p0, Lcom/anythink/basead/ui/SplashAdView;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2380
    :cond_1
    iget-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/SplashAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method private static synthetic a(Lcom/anythink/basead/ui/SplashAdView;Ljava/lang/Runnable;)V
    .locals 1

    const/16 v0, 0x64

    .line 37
    invoke-super {p0, v0, p1}, Lcom/anythink/basead/ui/BaseAdView;->a(ILjava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/SplashAdView;)Z
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/anythink/basead/ui/SplashAdView;->o()Z

    move-result p0

    return p0
.end method

.method static synthetic d(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    return-void
.end method

.method static synthetic e(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    return-void
.end method

.method private static synthetic f(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 37
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    return-void
.end method

.method private static k()V
    .locals 0

    return-void
.end method

.method private l()V
    .locals 16

    move-object/from16 v0, p0

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "id"

    const-string v3, "myoffer_splash_ad_title"

    invoke-static {v1, v3, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "myoffer_splash_ad_install_btn"

    invoke-static {v3, v4, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "myoffer_splash_desc"

    invoke-static {v4, v5, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "myoffer_splash_self_ad_logo"

    invoke-static {v5, v6, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "myoffer_splash_ad_content_image_area"

    invoke-static {v6, v7, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "myoffer_splash_bg"

    invoke-static {v7, v8, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "myoffer_splash_icon"

    invoke-static {v8, v9, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "string"

    const-string v12, "myoffer_splash_skip_text"

    invoke-static {v10, v12, v11}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "myoffer_splash_skip"

    invoke-static {v9, v10, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "myoffer_splash_ad_logo"

    invoke-static {v9, v10, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 115
    iget-object v10, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v10}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    const/4 v12, 0x1

    const/4 v14, 0x0

    if-nez v10, :cond_0

    .line 116
    invoke-virtual {v9, v14}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 117
    invoke-virtual {v9}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    iget v10, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v15

    new-instance v14, Lcom/anythink/core/common/res/e;

    iget-object v13, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v13}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v14, v12, v13}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v13, Lcom/anythink/basead/ui/SplashAdView$5;

    invoke-direct {v13, v0, v9}, Lcom/anythink/basead/ui/SplashAdView$5;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v15, v14, v10, v10, v13}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    const/16 v10, 0x8

    goto :goto_0

    :cond_0
    const/16 v10, 0x8

    .line 133
    invoke-virtual {v9, v10}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 136
    :goto_0
    iget-object v13, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v13, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-direct/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->o()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 140
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    invoke-virtual {v6, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 144
    invoke-virtual {v8, v10}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/res/e;

    iget-object v4, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    .line 148
    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v12, v4}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit16 v6, v6, 0x273

    div-int/lit16 v6, v6, 0x4b0

    new-instance v8, Lcom/anythink/basead/ui/SplashAdView$6;

    invoke-direct {v8, v0, v7}, Lcom/anythink/basead/ui/SplashAdView$6;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    .line 147
    invoke-virtual {v1, v2, v4, v6, v8}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 176
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 177
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    .line 180
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :goto_1
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->m()I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    .line 188
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 191
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 193
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_cta_learn_more"

    invoke-static {v1, v2, v11}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    .line 196
    :goto_2
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_3

    const/16 v2, 0xe

    .line 198
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 199
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    :cond_3
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void

    .line 209
    :cond_5
    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v9, v9, Lcom/anythink/core/common/d/u;

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v9}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    const/4 v9, 0x0

    .line 210
    invoke-virtual {v8, v9}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 211
    invoke-virtual {v8, v12}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    const/4 v9, 0x3

    .line 212
    invoke-virtual {v8, v9}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    .line 213
    invoke-virtual {v8}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 214
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v10

    new-instance v11, Lcom/anythink/core/common/res/e;

    iget-object v13, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v13}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v13, Lcom/anythink/basead/ui/SplashAdView$7;

    invoke-direct {v13, v0, v8}, Lcom/anythink/basead/ui/SplashAdView$7;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v10, v11, v9, v9, v13}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    goto :goto_3

    :cond_6
    const/16 v9, 0x8

    .line 229
    invoke-virtual {v8, v9}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 232
    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v9, v9, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v9}, Lcom/anythink/core/common/d/j;->g()I

    move-result v9

    if-ne v9, v12, :cond_7

    .line 233
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v9, :cond_7

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/high16 v11, 0x42700000    # 60.0f

    invoke-static {v10, v11}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v10

    iput v10, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 240
    :cond_7
    :goto_3
    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "myoffer_rating_view"

    invoke-static {v8, v9, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/anythink/basead/ui/AppRatingView;

    .line 244
    iget-object v8, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->u()I

    move-result v8

    const/4 v9, 0x5

    if-le v8, v9, :cond_8

    const/4 v10, 0x0

    .line 246
    invoke-virtual {v2, v10}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 247
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setStarNum(I)V

    .line 248
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setRating(I)V

    :goto_4
    const/16 v8, 0x8

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    if-lez v8, :cond_9

    .line 250
    invoke-virtual {v2, v10}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 251
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setStarNum(I)V

    .line 252
    invoke-virtual {v2, v8}, Lcom/anythink/basead/ui/AppRatingView;->setRating(I)V

    goto :goto_4

    :cond_9
    const/16 v8, 0x8

    .line 254
    invoke-virtual {v2, v8}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 256
    :goto_5
    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 260
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 262
    :cond_a
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    :goto_6
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    invoke-virtual {v6}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 270
    new-instance v2, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    .line 271
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v8, -0x1

    invoke-direct {v5, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v8, 0x11

    .line 272
    iput v8, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 273
    invoke-virtual {v2, v5}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274
    invoke-virtual {v2, v12}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 275
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v8}, Lcom/anythink/basead/ui/component/RoundImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 276
    invoke-virtual {v6, v2, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v5, 0x0

    .line 277
    invoke-virtual {v6, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v5

    new-instance v6, Lcom/anythink/core/common/res/e;

    iget-object v8, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    .line 280
    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v12, v8}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit16 v9, v9, 0x273

    div-int/lit16 v9, v9, 0x4b0

    new-instance v10, Lcom/anythink/basead/ui/SplashAdView$8;

    invoke-direct {v10, v0, v2, v7}, Lcom/anythink/basead/ui/SplashAdView$8;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    .line 279
    invoke-virtual {v5, v6, v8, v9, v10}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 295
    iget-object v5, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/j;->g()I

    move-result v2

    const/4 v5, 0x2

    const/4 v6, 0x4

    if-ne v2, v5, :cond_c

    .line 299
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 300
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    .line 301
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    :cond_b
    const/4 v2, 0x0

    .line 303
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    :cond_c
    const/4 v2, 0x0

    .line 306
    iget-object v5, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 307
    iget-object v5, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    .line 310
    :cond_d
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    :goto_7
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 317
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 318
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 320
    :cond_e
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    :goto_8
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 326
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 327
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    :cond_f
    const/16 v1, 0x8

    .line 329
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 331
    :goto_9
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private m()V
    .locals 2

    .line 336
    new-instance v0, Lcom/anythink/basead/ui/SplashAdView$9;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/SplashAdView$9;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    const/16 v1, 0x64

    .line 4037
    invoke-super {p0, v1, v0}, Lcom/anythink/basead/ui/BaseAdView;->a(ILjava/lang/Runnable;)V

    return-void
.end method

.method private n()V
    .locals 4

    .line 371
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 374
    iget-object v2, p0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 376
    iget-object v3, p0, Lcom/anythink/basead/ui/SplashAdView;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/SplashAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private o()Z
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v0, Lcom/anythink/core/common/d/u;

    .line 388
    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->z()I

    move-result v0

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private p()V
    .locals 3

    .line 403
    iget-boolean v0, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 406
    iput-boolean v0, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    .line 6037
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    .line 410
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    new-instance v2, Lcom/anythink/basead/ui/SplashAdView$10;

    invoke-direct {v2, p0}, Lcom/anythink/basead/ui/SplashAdView$10;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 426
    iput-boolean v1, p0, Lcom/anythink/basead/ui/SplashAdView;->y:Z

    .line 428
    new-instance v0, Lcom/anythink/basead/ui/SplashAdView$11;

    iget-object v1, p0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->d()J

    move-result-wide v1

    invoke-direct {v0, p0, v1, v2}, Lcom/anythink/basead/ui/SplashAdView$11;-><init>(Lcom/anythink/basead/ui/SplashAdView;J)V

    iput-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->c:Landroid/os/CountDownTimer;

    .line 450
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 17

    move-object/from16 v0, p0

    .line 83
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->g()I

    move-result v1

    const-string v2, "layout"

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "myoffer_splash_ad_land_layout"

    invoke-static {v4, v5, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "myoffer_splash_ad_layout"

    invoke-static {v4, v5, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3101
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "id"

    const-string v4, "myoffer_splash_ad_title"

    invoke-static {v1, v4, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3102
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "myoffer_splash_ad_install_btn"

    invoke-static {v4, v5, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 3103
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "myoffer_splash_desc"

    invoke-static {v5, v6, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 3104
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "myoffer_splash_self_ad_logo"

    invoke-static {v6, v7, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 3106
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "myoffer_splash_ad_content_image_area"

    invoke-static {v7, v8, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    .line 3107
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "myoffer_splash_bg"

    invoke-static {v8, v9, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 3108
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "myoffer_splash_icon"

    invoke-static {v9, v10, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 3110
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "string"

    const-string v13, "myoffer_splash_skip_text"

    invoke-static {v11, v13, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lcom/anythink/basead/ui/SplashAdView;->b:Ljava/lang/String;

    .line 3111
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "myoffer_splash_skip"

    invoke-static {v10, v11, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v0, v10}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    .line 3114
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "myoffer_splash_ad_logo"

    invoke-static {v10, v11, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v0, v10}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 3115
    iget-object v11, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v11}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    const/4 v13, 0x1

    const/4 v15, 0x0

    if-nez v11, :cond_1

    .line 3116
    invoke-virtual {v10, v15}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 3117
    invoke-virtual {v10}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    iget v11, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 3118
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v3

    new-instance v15, Lcom/anythink/core/common/res/e;

    iget-object v14, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v14}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v15, v13, v14}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v14, Lcom/anythink/basead/ui/SplashAdView$5;

    invoke-direct {v14, v0, v10}, Lcom/anythink/basead/ui/SplashAdView$5;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v3, v15, v11, v11, v14}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    const/16 v3, 0x8

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    .line 3133
    invoke-virtual {v10, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 3136
    :goto_1
    iget-object v11, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3139
    invoke-direct/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->o()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3140
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3141
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3142
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3143
    invoke-virtual {v7, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 3144
    invoke-virtual {v9, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 3147
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/res/e;

    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    .line 3148
    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v13, v3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit16 v5, v5, 0x273

    div-int/lit16 v5, v5, 0x4b0

    new-instance v7, Lcom/anythink/basead/ui/SplashAdView$6;

    invoke-direct {v7, v0, v8}, Lcom/anythink/basead/ui/SplashAdView$6;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    .line 3147
    invoke-virtual {v1, v2, v3, v5, v7}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 3176
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 3177
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    .line 3180
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3182
    :goto_2
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3186
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->m()I

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    .line 3188
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3190
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3191
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 3193
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_cta_learn_more"

    invoke-static {v1, v2, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 3196
    :goto_3
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_4

    const/16 v2, 0xe

    .line 3198
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3199
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3202
    :cond_4
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    return-void

    .line 3209
    :cond_6
    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v3, v3, Lcom/anythink/core/common/d/u;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x0

    .line 3210
    invoke-virtual {v9, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 3211
    invoke-virtual {v9, v13}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    const/4 v3, 0x3

    .line 3212
    invoke-virtual {v9, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    .line 3213
    invoke-virtual {v9}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 3214
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v10

    new-instance v11, Lcom/anythink/core/common/res/e;

    iget-object v12, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v12}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v13, v12}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v12, Lcom/anythink/basead/ui/SplashAdView$7;

    invoke-direct {v12, v0, v9}, Lcom/anythink/basead/ui/SplashAdView$7;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v10, v11, v3, v3, v12}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    goto :goto_4

    :cond_7
    const/16 v3, 0x8

    .line 3229
    invoke-virtual {v9, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setVisibility(I)V

    .line 3232
    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/j;->g()I

    move-result v3

    if-ne v3, v13, :cond_8

    .line 3233
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v3, :cond_8

    .line 3235
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/high16 v11, 0x42700000    # 60.0f

    invoke-static {v10, v11}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v10

    iput v10, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3240
    :cond_8
    :goto_4
    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3243
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v9, "myoffer_rating_view"

    invoke-static {v3, v9, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/anythink/basead/ui/SplashAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/anythink/basead/ui/AppRatingView;

    .line 3244
    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->u()I

    move-result v3

    const/4 v9, 0x5

    if-le v3, v9, :cond_9

    const/4 v10, 0x0

    .line 3246
    invoke-virtual {v2, v10}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 3247
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setStarNum(I)V

    .line 3248
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setRating(I)V

    :goto_5
    const/16 v3, 0x8

    goto :goto_6

    :cond_9
    const/4 v10, 0x0

    if-lez v3, :cond_a

    .line 3250
    invoke-virtual {v2, v10}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 3251
    invoke-virtual {v2, v9}, Lcom/anythink/basead/ui/AppRatingView;->setStarNum(I)V

    .line 3252
    invoke-virtual {v2, v3}, Lcom/anythink/basead/ui/AppRatingView;->setRating(I)V

    goto :goto_5

    :cond_a
    const/16 v3, 0x8

    .line 3254
    invoke-virtual {v2, v3}, Lcom/anythink/basead/ui/AppRatingView;->setVisibility(I)V

    .line 3256
    :goto_6
    iget-object v9, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3259
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 3260
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    .line 3262
    :cond_b
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3264
    :goto_7
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3268
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 3270
    new-instance v2, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    .line 3271
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v6, 0x11

    .line 3272
    iput v6, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3273
    invoke-virtual {v2, v3}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3274
    invoke-virtual {v2, v13}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 3275
    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v6}, Lcom/anythink/basead/ui/component/RoundImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 3276
    invoke-virtual {v7, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x0

    .line 3277
    invoke-virtual {v7, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 3279
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v3

    new-instance v6, Lcom/anythink/core/common/res/e;

    iget-object v7, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    .line 3280
    invoke-virtual {v7}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v13, v7}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit16 v9, v9, 0x273

    div-int/lit16 v9, v9, 0x4b0

    new-instance v10, Lcom/anythink/basead/ui/SplashAdView$8;

    invoke-direct {v10, v0, v2, v8}, Lcom/anythink/basead/ui/SplashAdView$8;-><init>(Lcom/anythink/basead/ui/SplashAdView;Lcom/anythink/basead/ui/component/RoundImageView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    .line 3279
    invoke-virtual {v3, v6, v7, v9, v10}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 3295
    iget-object v3, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3298
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/j;->g()I

    move-result v2

    const/4 v3, 0x4

    const/4 v6, 0x2

    if-ne v2, v6, :cond_d

    .line 3299
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 3300
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    .line 3301
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    :cond_c
    const/4 v2, 0x0

    .line 3303
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    :cond_d
    const/4 v2, 0x0

    .line 3306
    iget-object v6, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v6}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 3307
    iget-object v6, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v6}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3308
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 3310
    :cond_e
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3313
    :goto_8
    iget-object v2, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3316
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 3317
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 3318
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    .line 3320
    :cond_f
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3322
    :goto_9
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3325
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 3326
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 3327
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_a

    :cond_10
    const/16 v1, 0x8

    .line 3329
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3331
    :goto_a
    iget-object v1, v0, Lcom/anythink/basead/ui/SplashAdView;->u:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(Z)V
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 477
    invoke-interface {v0, p1}, Lcom/anythink/basead/f/a;->onDeeplinkCallback(Z)V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 3

    .line 456
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/SplashAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2, v0, v1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 458
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 459
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 1

    .line 469
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 470
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdClick()V

    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 1

    .line 483
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->e:Lcom/anythink/basead/ui/a;

    if-nez v0, :cond_0

    .line 485
    new-instance v0, Lcom/anythink/basead/ui/a;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/a;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->e:Lcom/anythink/basead/ui/a;

    .line 487
    :cond_0
    new-instance v0, Lcom/anythink/basead/ui/SplashAdView$2;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/SplashAdView$2;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/SplashAdView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public destroy()V
    .locals 1

    .line 512
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->destroy()V

    const/4 v0, 0x0

    .line 514
    iput-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    return-void
.end method

.method protected final e()V
    .locals 1

    .line 498
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->e:Lcom/anythink/basead/ui/a;

    if-eqz v0, :cond_0

    .line 500
    new-instance v0, Lcom/anythink/basead/ui/SplashAdView$3;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/SplashAdView$3;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/SplashAdView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 2

    .line 393
    invoke-super {p0, p1}, Lcom/anythink/basead/ui/BaseAdView;->onWindowVisibilityChanged(I)V

    if-nez p1, :cond_0

    .line 4403
    iget-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 4406
    iput-boolean p1, p0, Lcom/anythink/basead/ui/SplashAdView;->x:Z

    .line 5037
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    .line 4410
    iget-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4411
    iget-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->a:Landroid/widget/TextView;

    new-instance v1, Lcom/anythink/basead/ui/SplashAdView$10;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/SplashAdView$10;-><init>(Lcom/anythink/basead/ui/SplashAdView;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4426
    iput-boolean v0, p0, Lcom/anythink/basead/ui/SplashAdView;->y:Z

    .line 4428
    new-instance p1, Lcom/anythink/basead/ui/SplashAdView$11;

    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->d()J

    move-result-wide v0

    invoke-direct {p1, p0, v0, v1}, Lcom/anythink/basead/ui/SplashAdView$11;-><init>(Lcom/anythink/basead/ui/SplashAdView;J)V

    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView;->c:Landroid/os/CountDownTimer;

    .line 4450
    invoke-virtual {p1}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method
