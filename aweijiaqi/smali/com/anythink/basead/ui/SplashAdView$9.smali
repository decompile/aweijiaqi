.class final Lcom/anythink/basead/ui/SplashAdView$9;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/ui/SplashAdView;->m()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/ui/SplashAdView;


# direct methods
.method constructor <init>(Lcom/anythink/basead/ui/SplashAdView;)V
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 340
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/SplashAdView;->d:Lcom/anythink/basead/f/a;

    if-nez v0, :cond_0

    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/SplashAdView;->c(Lcom/anythink/basead/ui/SplashAdView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/SplashAdView;->getWidth()I

    move-result v0

    .line 346
    iget-object v1, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/SplashAdView;->getHeight()I

    move-result v1

    .line 348
    iget-object v2, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-virtual {v2}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 349
    iget-object v3, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-virtual {v3}, Lcom/anythink/basead/ui/SplashAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v2

    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    mul-double v4, v4, v6

    double-to-int v2, v4

    int-to-double v3, v3

    mul-double v3, v3, v6

    double-to-int v3, v3

    if-ge v0, v2, :cond_1

    .line 355
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string v1, "Splash display width is less than 75% of screen width!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    if-ge v1, v3, :cond_2

    .line 357
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string v1, "Splash display height is less than 75% of screen height!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/SplashAdView;->d(Lcom/anythink/basead/ui/SplashAdView;)V

    return-void

    .line 362
    :cond_3
    iget-object v0, p0, Lcom/anythink/basead/ui/SplashAdView$9;->a:Lcom/anythink/basead/ui/SplashAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/SplashAdView;->e(Lcom/anythink/basead/ui/SplashAdView;)V

    return-void
.end method
