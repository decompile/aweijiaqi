.class final Lcom/anythink/basead/ui/FullScreenAdView$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/ui/PlayerView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/ui/FullScreenAdView;->p()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/ui/FullScreenAdView;


# direct methods
.method constructor <init>(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .line 185
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoPlayStart..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/ui/FullScreenAdView;J)J

    .line 189
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/ui/FullScreenAdView;)V

    .line 190
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->b(Lcom/anythink/basead/ui/FullScreenAdView;)V

    return-void
.end method

.method public final a(I)V
    .locals 5

    .line 195
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0, p1}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/ui/FullScreenAdView;I)V

    .line 196
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->c(Lcom/anythink/basead/ui/FullScreenAdView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->d(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/ui/BannerView;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->e(Lcom/anythink/basead/ui/FullScreenAdView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    int-to-long v0, p1

    iget-object p1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {p1}, Lcom/anythink/basead/ui/FullScreenAdView;->e(Lcom/anythink/basead/ui/FullScreenAdView;)J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-ltz p1, :cond_0

    .line 197
    iget-object p1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {p1}, Lcom/anythink/basead/ui/FullScreenAdView;->f(Lcom/anythink/basead/ui/FullScreenAdView;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/basead/c/f;)V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/FullScreenAdView;->fillVideoEndRecord(Z)Lcom/anythink/basead/c/i;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    .line 232
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    const/16 v2, 0x11

    invoke-static {v2, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 233
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0, p1}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/ui/FullScreenAdView;Lcom/anythink/basead/c/f;)V

    return-void
.end method

.method public final b()V
    .locals 2

    .line 203
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoPlayEnd..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(I)V
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    const/16 v1, 0x19

    if-eq p1, v1, :cond_2

    const/16 v1, 0x32

    if-eq p1, v1, :cond_1

    const/16 v1, 0x4b

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    sget-object p1, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoProgress75......."

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x4

    .line 250
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {p1, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    :goto_0
    return-void

    .line 245
    :cond_1
    sget-object p1, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoProgress50......."

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x3

    .line 246
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {p1, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return-void

    .line 241
    :cond_2
    sget-object p1, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoProgress25......."

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    .line 242
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {p1, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return-void
.end method

.method public final c()V
    .locals 3

    .line 209
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoPlayCompletion..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    const/4 v2, 0x5

    invoke-static {v2, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 215
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    const/16 v2, 0x1f

    invoke-static {v2, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 217
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->g(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/f/b$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->g(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/f/b$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/basead/f/b$b;->c()V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->g(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/f/b$b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->g(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/f/b$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/basead/f/b$b;->d()V

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->h(Lcom/anythink/basead/ui/FullScreenAdView;)V

    return-void
.end method

.method public final d()V
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/ui/PlayerView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/ui/PlayerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->stop()V

    .line 259
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 260
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/FullScreenAdView;->j()Lcom/anythink/basead/c/b;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    const/16 v1, 0x10

    .line 261
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v2, v2, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v2, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->h(Lcom/anythink/basead/ui/FullScreenAdView;)V

    return-void
.end method

.method public final e()V
    .locals 5

    .line 269
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->c(Lcom/anythink/basead/ui/FullScreenAdView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->e(Lcom/anythink/basead/ui/FullScreenAdView;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 270
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->f(Lcom/anythink/basead/ui/FullScreenAdView;)V

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 273
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/FullScreenAdView;->j()Lcom/anythink/basead/c/b;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    const/16 v1, 0xe

    .line 274
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v2, v2, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v2, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 276
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v0, v0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->k()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 277
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->g()V

    :cond_1
    return-void
.end method

.method public final f()V
    .locals 3

    .line 283
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoMute..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 285
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/FullScreenAdView;->j()Lcom/anythink/basead/c/b;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    .line 286
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    const/16 v2, 0xc

    invoke-static {v2, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return-void
.end method

.method public final g()V
    .locals 3

    .line 291
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "onVideoNoMute..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 293
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/FullScreenAdView;->j()Lcom/anythink/basead/c/b;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    .line 294
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v1, v1, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    const/16 v2, 0xd

    invoke-static {v2, v1, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return-void
.end method

.method public final h()V
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView$2;->a:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-static {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->j(Lcom/anythink/basead/ui/FullScreenAdView;)V

    return-void
.end method
