.class public Lcom/anythink/basead/ui/FullScreenAdView;
.super Lcom/anythink/basead/ui/BaseAdView;


# static fields
.field public static final FORMAT_INTERSTITIAL:I = 0x3

.field public static final FORMAT_REWARD_VIDEO:I = 0x1

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private A:Lcom/anythink/basead/ui/a;

.field private B:I

.field private C:I

.field private D:Z

.field private E:Lcom/anythink/basead/f/b$b;

.field private F:J

.field private G:Z

.field private H:J

.field private I:Lcom/anythink/basead/c;

.field a:I

.field b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:Z

.field private v:Z

.field private w:Landroid/widget/RelativeLayout;

.field private x:Lcom/anythink/basead/ui/PlayerView;

.field private y:Lcom/anythink/basead/ui/BannerView;

.field private z:Lcom/anythink/basead/ui/EndCardView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    const-class v0, Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;II)V
    .locals 0

    .line 74
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;)V

    .line 76
    iput p5, p0, Lcom/anythink/basead/ui/FullScreenAdView;->c:I

    .line 77
    iput p6, p0, Lcom/anythink/basead/ui/FullScreenAdView;->d:I

    return-void
.end method

.method private A()V
    .locals 3

    .line 490
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 492
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz v0, :cond_0

    .line 493
    invoke-interface {v0}, Lcom/anythink/basead/f/b$b;->b()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/FullScreenAdView;J)J
    .locals 0

    .line 38
    iput-wide p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->H:J

    return-wide p1
.end method

.method private a(I)V
    .locals 4

    .line 597
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_3

    .line 598
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v0, Lcom/anythink/core/common/d/u;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->y()Lcom/anythink/core/common/d/w;

    move-result-object v0

    .line 599
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->y()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 600
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 601
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v1, :cond_0

    .line 602
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 604
    :cond_0
    div-int/lit16 p1, p1, 0x3e8

    .line 605
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 606
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 609
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 610
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v2

    .line 612
    iget-object v3, v2, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v3, Lcom/anythink/basead/c/i;->i:I

    const/16 v1, 0x20

    .line 613
    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v3, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private a(Lcom/anythink/basead/c/f;)V
    .locals 1

    .line 498
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz v0, :cond_0

    .line 499
    invoke-interface {v0, p1}, Lcom/anythink/basead/f/b$b;->a(Lcom/anythink/basead/c/f;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 38
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/FullScreenAdView;I)V
    .locals 4

    .line 2597
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_3

    .line 2598
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v0, Lcom/anythink/core/common/d/u;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->y()Lcom/anythink/core/common/d/w;

    move-result-object v0

    .line 2599
    invoke-virtual {v0}, Lcom/anythink/core/common/d/w;->y()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2600
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 2601
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v1, :cond_0

    .line 2602
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 2604
    :cond_0
    div-int/lit16 p1, p1, 0x3e8

    .line 2605
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2606
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2609
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 2610
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->b:Ljava/util/concurrent/ConcurrentHashMap;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2611
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v2

    .line 2612
    iget-object v3, v2, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v3, Lcom/anythink/basead/c/i;->i:I

    const/16 v1, 0x20

    .line 2613
    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v3, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/FullScreenAdView;Lcom/anythink/basead/c/f;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/c/f;)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 3

    .line 2490
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 2492
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz p0, :cond_0

    .line 2493
    invoke-interface {p0}, Lcom/anythink/basead/f/b$b;->b()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/FullScreenAdView;)Z
    .locals 0

    .line 38
    iget-boolean p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->G:Z

    return p0
.end method

.method static synthetic d(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/ui/BannerView;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->y:Lcom/anythink/basead/ui/BannerView;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/basead/ui/FullScreenAdView;)J
    .locals 2

    .line 38
    iget-wide v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->F:J

    return-wide v0
.end method

.method static synthetic f(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 3405
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->o()V

    return-void
.end method

.method static synthetic g(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/f/b$b;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->q()V

    return-void
.end method

.method static synthetic i(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/ui/PlayerView;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 5

    .line 3428
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->l()V

    .line 3430
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    if-nez v0, :cond_0

    .line 3431
    new-instance v0, Lcom/anythink/basead/c;

    invoke-direct {v0}, Lcom/anythink/basead/c;-><init>()V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    .line 3433
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    new-instance v4, Lcom/anythink/basead/ui/FullScreenAdView$5;

    invoke-direct {v4, p0}, Lcom/anythink/basead/ui/FullScreenAdView$5;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/c;->a(Landroid/content/Context;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/c$a;)V

    return-void
.end method

.method static synthetic k(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 1

    const/4 v0, 0x1

    .line 3450
    iput-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    .line 3451
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v0, :cond_0

    .line 3452
    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->removeFeedbackButton()V

    .line 3455
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->z:Lcom/anythink/basead/ui/EndCardView;

    if-eqz p0, :cond_1

    .line 3456
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->removeFeedbackButton()V

    :cond_1
    return-void
.end method

.method static synthetic l(Lcom/anythink/basead/ui/FullScreenAdView;)Lcom/anythink/basead/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 3484
    iget-object p0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    if-eqz p0, :cond_0

    .line 3485
    invoke-virtual {p0}, Lcom/anythink/basead/ui/a;->b()V

    :cond_0
    return-void
.end method

.method private n()V
    .locals 2

    .line 150
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 151
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->B:I

    .line 152
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->C:I

    return-void
.end method

.method private static synthetic n(Lcom/anythink/basead/ui/FullScreenAdView;)V
    .locals 0

    .line 38
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    return-void
.end method

.method private o()V
    .locals 7

    .line 163
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->y:Lcom/anythink/basead/ui/BannerView;

    if-eqz v0, :cond_0

    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    sub-int/2addr v0, v1

    :goto_0
    if-lez v0, :cond_1

    .line 169
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 172
    :cond_1
    new-instance v0, Lcom/anythink/basead/ui/BannerView;

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v4, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    iget v5, p0, Lcom/anythink/basead/ui/FullScreenAdView;->d:I

    new-instance v6, Lcom/anythink/basead/ui/FullScreenAdView$1;

    invoke-direct {v6, p0}, Lcom/anythink/basead/ui/FullScreenAdView$1;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/anythink/basead/ui/BannerView;-><init>(Landroid/view/ViewGroup;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ILcom/anythink/basead/ui/BannerView$a;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->y:Lcom/anythink/basead/ui/BannerView;

    return-void
.end method

.method private p()V
    .locals 3

    .line 182
    new-instance v0, Lcom/anythink/basead/ui/PlayerView;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/anythink/basead/ui/FullScreenAdView$2;

    invoke-direct {v2, p0}, Lcom/anythink/basead/ui/FullScreenAdView$2;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/ui/PlayerView;-><init>(Landroid/view/ViewGroup;Lcom/anythink/basead/ui/PlayerView$a;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    .line 302
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/PlayerView;->setSetting(Lcom/anythink/core/common/d/j;)V

    .line 303
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    iget-boolean v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/PlayerView;->setHideFeedbackButton(Z)V

    .line 304
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/PlayerView;->load(Ljava/lang/String;)V

    return-void
.end method

.method private q()V
    .locals 3

    .line 308
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "showEndCard......."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 309
    iput-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    .line 314
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->s()V

    .line 318
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v0, :cond_0

    .line 319
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 320
    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    .line 323
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    const/4 v1, 0x6

    .line 324
    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-static {v1, v2, v0}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    return-void
.end method

.method private static r()V
    .locals 0

    return-void
.end method

.method private s()V
    .locals 11

    .line 333
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->G:Z

    xor-int/lit8 v7, v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->m()I

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v0, Lcom/anythink/core/common/d/u;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->z()I

    move-result v0

    if-ne v2, v0, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 347
    :goto_0
    new-instance v0, Lcom/anythink/basead/ui/EndCardView;

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->B:I

    iget v4, p0, Lcom/anythink/basead/ui/FullScreenAdView;->C:I

    iget-object v5, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v6, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    iget-boolean v8, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    new-instance v10, Lcom/anythink/basead/ui/FullScreenAdView$3;

    invoke-direct {v10, p0}, Lcom/anythink/basead/ui/FullScreenAdView$3;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/anythink/basead/ui/EndCardView;-><init>(Landroid/view/ViewGroup;IILcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ZZZLcom/anythink/basead/ui/EndCardView$a;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->z:Lcom/anythink/basead/ui/EndCardView;

    .line 372
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->G:Z

    if-eqz v0, :cond_1

    .line 2405
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->o()V

    :cond_1
    return-void
.end method

.method private t()V
    .locals 5

    .line 378
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 379
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_cta_learn_more"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "#ffffffff"

    .line 380
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v1, 0x41a00000    # 20.0f

    .line 381
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    const/16 v1, 0x11

    .line 382
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 383
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_splash_btn"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 385
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x43480000    # 200.0f

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v1

    .line 386
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x428c0000    # 70.0f

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v2

    .line 387
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41b80000    # 23.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    .line 389
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xe

    .line 390
    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xc

    .line 391
    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 392
    iput v3, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 394
    new-instance v1, Lcom/anythink/basead/ui/FullScreenAdView$4;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/FullScreenAdView$4;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    invoke-virtual {p0, v0, v4}, Lcom/anythink/basead/ui/FullScreenAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private u()V
    .locals 0

    .line 405
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->o()V

    return-void
.end method

.method private v()Z
    .locals 4

    .line 410
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->r()I

    move-result v3

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    .line 415
    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v3, Lcom/anythink/core/common/d/u;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/u;->z()I

    move-result v3

    if-ne v0, v3, :cond_1

    return v2

    :cond_1
    return v1

    :cond_2
    return v2
.end method

.method private w()V
    .locals 5

    .line 428
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->l()V

    .line 430
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    if-nez v0, :cond_0

    .line 431
    new-instance v0, Lcom/anythink/basead/c;

    invoke-direct {v0}, Lcom/anythink/basead/c;-><init>()V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v3, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    new-instance v4, Lcom/anythink/basead/ui/FullScreenAdView$5;

    invoke-direct {v4, p0}, Lcom/anythink/basead/ui/FullScreenAdView$5;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/c;->a(Landroid/content/Context;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/i;Lcom/anythink/basead/c$a;)V

    return-void
.end method

.method private x()V
    .locals 1

    const/4 v0, 0x1

    .line 450
    iput-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    .line 451
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->removeFeedbackButton()V

    .line 455
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->z:Lcom/anythink/basead/ui/EndCardView;

    if-eqz v0, :cond_1

    .line 456
    invoke-virtual {v0}, Lcom/anythink/basead/ui/EndCardView;->removeFeedbackButton()V

    :cond_1
    return-void
.end method

.method private y()V
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    if-nez v0, :cond_0

    .line 478
    new-instance v0, Lcom/anythink/basead/ui/a;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/a;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/a;->a()V

    return-void
.end method

.method private z()V
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    if-eqz v0, :cond_0

    .line 485
    invoke-virtual {v0}, Lcom/anythink/basead/ui/a;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .line 106
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_activity_ad"

    const-string v3, "layout"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method protected final a(Z)V
    .locals 1

    .line 526
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz v0, :cond_0

    .line 527
    invoke-interface {v0, p1}, Lcom/anythink/basead/f/b$b;->a(Z)V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 3

    .line 506
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2, v0, v1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 508
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz v0, :cond_0

    .line 509
    invoke-interface {v0}, Lcom/anythink/basead/f/b$b;->a()V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 1

    .line 519
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    if-eqz v0, :cond_0

    .line 520
    invoke-interface {v0}, Lcom/anythink/basead/f/b$b;->f()V

    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 2

    const/4 v0, 0x1

    .line 533
    iput-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->D:Z

    .line 2477
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    if-nez v0, :cond_0

    .line 2478
    new-instance v0, Lcom/anythink/basead/ui/a;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/a;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    .line 2480
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->A:Lcom/anythink/basead/ui/a;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/a;->a()V

    return-void
.end method

.method protected destroy()V
    .locals 1

    .line 626
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->destroy()V

    const/4 v0, 0x0

    .line 628
    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    return-void
.end method

.method protected final e()V
    .locals 1

    const/4 v0, 0x0

    .line 539
    iput-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->D:Z

    .line 540
    new-instance v0, Lcom/anythink/basead/ui/FullScreenAdView$6;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/FullScreenAdView$6;-><init>(Lcom/anythink/basead/ui/FullScreenAdView;)V

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public fillVideoEndRecord(Z)Lcom/anythink/basead/c/i;
    .locals 6

    .line 576
    new-instance v0, Lcom/anythink/basead/c/i;

    invoke-direct {v0}, Lcom/anythink/basead/c/i;-><init>()V

    .line 577
    iget v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->d:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput v1, v0, Lcom/anythink/basead/c/i;->l:I

    .line 578
    iput v3, v0, Lcom/anythink/basead/c/i;->r:I

    .line 579
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/anythink/basead/ui/PlayerView;->getVideoLength()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput v1, v0, Lcom/anythink/basead/c/i;->a:I

    .line 580
    iget v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->a:I

    div-int/lit16 v1, v1, 0x3e8

    iput v1, v0, Lcom/anythink/basead/c/i;->b:I

    .line 581
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/PlayerView;->getCurrentPosition()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    iput v1, v0, Lcom/anythink/basead/c/i;->c:I

    .line 582
    iget v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->a:I

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iput v1, v0, Lcom/anythink/basead/c/i;->d:I

    .line 583
    iget v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->a:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x2

    :goto_3
    iput v1, v0, Lcom/anythink/basead/c/i;->o:I

    .line 584
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v1}, Lcom/anythink/basead/ui/PlayerView;->getCurrentPosition()I

    move-result v1

    iget-object v5, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v5}, Lcom/anythink/basead/ui/PlayerView;->getVideoLength()I

    move-result v5

    if-ne v1, v5, :cond_4

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    :goto_4
    iput v3, v0, Lcom/anythink/basead/c/i;->e:I

    if-eqz p1, :cond_5

    const/4 v2, 0x0

    .line 585
    :cond_5
    iput v2, v0, Lcom/anythink/basead/c/i;->u:I

    .line 586
    iget-wide v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->H:J

    iput-wide v1, v0, Lcom/anythink/basead/c/i;->f:J

    .line 587
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/anythink/basead/c/i;->g:J

    .line 588
    iget-object p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {p1}, Lcom/anythink/basead/ui/PlayerView;->getCurrentPosition()I

    move-result p1

    iput p1, v0, Lcom/anythink/basead/c/i;->h:I

    .line 590
    sget-object p1, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Video End Record:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/anythink/basead/c/i;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final g()V
    .locals 2

    .line 463
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "click \u3002\u3002\u3002\u3002\u3002"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->D:Z

    if-eqz v0, :cond_0

    .line 466
    sget-object v0, Lcom/anythink/basead/ui/FullScreenAdView;->TAG:Ljava/lang/String;

    const-string v1, "during click \u3002\u3002\u3002\u3002\u3002"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    if-nez v0, :cond_1

    return-void

    .line 473
    :cond_1
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method protected final i()Lcom/anythink/basead/c/h;
    .locals 3

    .line 140
    new-instance v0, Lcom/anythink/basead/c/h;

    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->t:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->e:I

    .line 142
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->f:I

    .line 143
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 144
    invoke-virtual {p0, v1}, Lcom/anythink/basead/ui/FullScreenAdView;->fillVideoEndRecord(Z)Lcom/anythink/basead/c/i;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->h:Lcom/anythink/basead/c/i;

    :cond_0
    return-object v0
.end method

.method public init()V
    .locals 5

    .line 110
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    .line 112
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "myoffer_rl_root"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->w:Landroid/widget/RelativeLayout;

    .line 113
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_full_screen_view_id"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->setId(I)V

    .line 1150
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1151
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->B:I

    .line 1152
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->C:I

    .line 1410
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1414
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->r()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 1415
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v0, Lcom/anythink/core/common/d/u;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/u;->z()I

    move-result v0

    if-eq v1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    .line 117
    :goto_0
    iput-boolean v2, p0, Lcom/anythink/basead/ui/FullScreenAdView;->G:Z

    .line 119
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    if-eqz v0, :cond_2

    .line 120
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->q()V

    return-void

    .line 121
    :cond_2
    iget v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->c:I

    if-ne v3, v0, :cond_4

    .line 122
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->p()V

    return-void

    :cond_3
    const-string v0, "40002"

    const-string v1, "Video url no exist!"

    .line 125
    invoke-static {v0, v1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->a(Lcom/anythink/basead/c/f;)V

    return-void

    :cond_4
    if-ne v1, v0, :cond_6

    .line 128
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->r()I

    move-result v0

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 129
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->p()V

    return-void

    .line 131
    :cond_5
    invoke-direct {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->q()V

    .line 2038
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    :cond_6
    return-void
.end method

.method public isShowEndCard()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    return v0
.end method

.method protected final k()V
    .locals 3

    .line 550
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->I:Lcom/anythink/basead/c;

    invoke-virtual {v0}, Lcom/anythink/basead/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->a:I

    .line 556
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->start()V

    .line 558
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->H:J

    .line 559
    iget v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->a:I

    if-eqz v0, :cond_1

    const/16 v0, 0xf

    .line 560
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    .line 564
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method

.method protected final l()V
    .locals 3

    .line 569
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    .line 570
    iget-object v1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 571
    iget-object v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->x:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/PlayerView;->pause()V

    :cond_0
    return-void
.end method

.method protected final m()V
    .locals 0

    .line 621
    invoke-virtual {p0}, Lcom/anythink/basead/ui/FullScreenAdView;->destroy()V

    return-void
.end method

.method public needHideFeedbackButton()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    return v0
.end method

.method public setHideFeedbackButton(Z)V
    .locals 0

    .line 97
    iput-boolean p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->v:Z

    return-void
.end method

.method public setIsShowEndCard(Z)V
    .locals 0

    .line 93
    iput-boolean p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->e:Z

    return-void
.end method

.method public setListener(Lcom/anythink/basead/f/b$b;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->E:Lcom/anythink/basead/f/b$b;

    return-void
.end method

.method public setShowBannerTime(J)V
    .locals 0

    .line 85
    iput-wide p1, p0, Lcom/anythink/basead/ui/FullScreenAdView;->F:J

    return-void
.end method
