.class public abstract Lcom/anythink/basead/ui/BaseAdView;
.super Landroid/widget/RelativeLayout;


# instance fields
.field f:Lcom/anythink/core/common/d/i;

.field g:Lcom/anythink/core/common/d/h;

.field h:Lcom/anythink/basead/d/c;

.field i:Lcom/anythink/basead/a/b;

.field j:Z

.field k:Z

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:Ljava/lang/String;

.field u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V
    .locals 1

    const-string v0, ""

    .line 67
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p2, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    .line 58
    iput-object p3, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    .line 59
    iput-object p4, p0, Lcom/anythink/basead/ui/BaseAdView;->t:Ljava/lang/String;

    .line 61
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/anythink/basead/ui/BaseAdView;->u:Ljava/util/List;

    .line 63
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->a()V

    return-void
.end method

.method private k()V
    .locals 5

    .line 165
    iget-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->j:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->j:Z

    .line 170
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v0, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_1

    .line 171
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/basead/g/a/b;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/g/a/b;->a(Lcom/anythink/core/common/d/p;)V

    goto :goto_0

    .line 172
    :cond_1
    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_2

    .line 173
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v4, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v4, v4, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)V

    .line 176
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->b()V

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(ILjava/lang/Runnable;)V
    .locals 2

    if-lez p1, :cond_0

    .line 144
    new-instance v0, Lcom/anythink/basead/d/c;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/anythink/basead/d/c;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->h:Lcom/anythink/basead/d/c;

    goto :goto_0

    .line 146
    :cond_0
    new-instance p1, Lcom/anythink/basead/d/c;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/anythink/basead/d/c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/BaseAdView;->h:Lcom/anythink/basead/d/c;

    .line 149
    :goto_0
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdView;->h:Lcom/anythink/basead/d/c;

    new-instance v0, Lcom/anythink/basead/ui/BaseAdView$2;

    invoke-direct {v0, p0, p2}, Lcom/anythink/basead/ui/BaseAdView$2;-><init>(Lcom/anythink/basead/ui/BaseAdView;Ljava/lang/Runnable;)V

    invoke-virtual {p1, p0, v0}, Lcom/anythink/basead/d/c;->a(Landroid/view/View;Lcom/anythink/basead/d/b;)V

    return-void
.end method

.method protected final a(Ljava/lang/Runnable;)V
    .locals 1

    const/4 v0, 0x0

    .line 161
    invoke-virtual {p0, v0, p1}, Lcom/anythink/basead/ui/BaseAdView;->a(ILjava/lang/Runnable;)V

    return-void
.end method

.method protected abstract a(Z)V
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected destroy()V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->i:Lcom/anythink/basead/a/b;

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {v0}, Lcom/anythink/basead/a/b;->a()V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->h:Lcom/anythink/basead/d/c;

    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {v0}, Lcom/anythink/basead/d/c;->b()V

    :cond_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 197
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 207
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->n:I

    .line 208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->o:I

    .line 210
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->r:I

    .line 211
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->s:I

    goto :goto_0

    .line 199
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->l:I

    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->m:I

    .line 202
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->p:I

    .line 203
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdView;->q:I

    .line 215
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected final declared-synchronized f()V
    .locals 5

    monitor-enter p0

    .line 1165
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->j:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 1168
    iput-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->j:Z

    .line 1170
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 1171
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/basead/g/a/b;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/g/a/b;->a(Lcom/anythink/core/common/d/p;)V

    goto :goto_0

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    if-eqz v0, :cond_1

    .line 1173
    invoke-static {}, Lcom/anythink/basead/e/c/b;->a()Lcom/anythink/basead/e/c/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/anythink/basead/e/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    iget-object v4, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v4, v4, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/e/c/b;->a(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;)V

    .line 1176
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected g()V
    .locals 4

    .line 94
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->i:Lcom/anythink/basead/a/b;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/anythink/basead/a/b;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-direct {v0, v1, v2, v3}, Lcom/anythink/basead/a/b;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->i:Lcom/anythink/basead/a/b;

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->j()Lcom/anythink/basead/c/b;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/basead/c/h;->g:Lcom/anythink/basead/c/b;

    .line 101
    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->i:Lcom/anythink/basead/a/b;

    new-instance v2, Lcom/anythink/basead/ui/BaseAdView$1;

    invoke-direct {v2, p0}, Lcom/anythink/basead/ui/BaseAdView$1;-><init>(Lcom/anythink/basead/ui/BaseAdView;)V

    invoke-virtual {v1, v0, v2}, Lcom/anythink/basead/a/b;->a(Lcom/anythink/basead/c/h;Lcom/anythink/basead/a/b$a;)V

    .line 119
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->c()V

    return-void
.end method

.method protected final h()V
    .locals 3

    .line 123
    iget-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->k:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 126
    iput-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdView;->k:Z

    .line 128
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/t;

    if-eqz v0, :cond_1

    .line 129
    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/anythink/basead/e/b/a;->a()Lcom/anythink/basead/e/b/a;

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    invoke-static {v1}, Lcom/anythink/basead/e/b/a;->a(Lcom/anythink/core/common/d/i;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/basead/e/b/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 133
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/f;

    if-eqz v0, :cond_2

    .line 134
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    const/16 v2, 0x42

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;I)V

    .line 135
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/f;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_2
    return-void
.end method

.method protected i()Lcom/anythink/basead/c/h;
    .locals 3

    .line 219
    new-instance v0, Lcom/anythink/basead/c/h;

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->e:I

    .line 221
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdView;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/anythink/basead/c/h;->f:I

    return-object v0
.end method

.method protected final j()Lcom/anythink/basead/c/b;
    .locals 2

    .line 226
    new-instance v0, Lcom/anythink/basead/c/b;

    invoke-direct {v0}, Lcom/anythink/basead/c/b;-><init>()V

    .line 227
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->l:I

    iput v1, v0, Lcom/anythink/basead/c/b;->a:I

    .line 228
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->m:I

    iput v1, v0, Lcom/anythink/basead/c/b;->b:I

    .line 229
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->n:I

    iput v1, v0, Lcom/anythink/basead/c/b;->c:I

    .line 230
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->o:I

    iput v1, v0, Lcom/anythink/basead/c/b;->d:I

    .line 232
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->p:I

    iput v1, v0, Lcom/anythink/basead/c/b;->e:I

    .line 233
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->q:I

    iput v1, v0, Lcom/anythink/basead/c/b;->f:I

    .line 234
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->r:I

    iput v1, v0, Lcom/anythink/basead/c/b;->g:I

    .line 235
    iget v1, p0, Lcom/anythink/basead/ui/BaseAdView;->s:I

    iput v1, v0, Lcom/anythink/basead/c/b;->h:I

    return-object v0
.end method
