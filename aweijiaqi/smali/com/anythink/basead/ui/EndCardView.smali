.class public Lcom/anythink/basead/ui/EndCardView;
.super Landroid/widget/RelativeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/ui/EndCardView$a;
    }
.end annotation


# instance fields
.field private a:Lcom/anythink/basead/ui/EndCardView$a;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Landroid/widget/ImageView;

.field private i:Lcom/anythink/basead/ui/component/RoundImageView;

.field private j:Landroid/widget/ImageView;

.field private k:I

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/anythink/core/common/d/j;

.field private final o:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;IILcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ZZZLcom/anythink/basead/ui/EndCardView$a;)V
    .locals 5

    .line 77
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 38
    iput v0, p0, Lcom/anythink/basead/ui/EndCardView;->d:I

    const/4 v1, 0x1

    .line 39
    iput v1, p0, Lcom/anythink/basead/ui/EndCardView;->e:I

    const/4 v2, 0x2

    .line 40
    iput v2, p0, Lcom/anythink/basead/ui/EndCardView;->f:I

    const/4 v3, 0x3

    .line 41
    iput v3, p0, Lcom/anythink/basead/ui/EndCardView;->g:I

    .line 52
    new-instance v3, Lcom/anythink/basead/ui/EndCardView$1;

    invoke-direct {v3, p0}, Lcom/anythink/basead/ui/EndCardView$1;-><init>(Lcom/anythink/basead/ui/EndCardView;)V

    iput-object v3, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    .line 78
    iput-object p9, p0, Lcom/anythink/basead/ui/EndCardView;->a:Lcom/anythink/basead/ui/EndCardView$a;

    .line 80
    iput p2, p0, Lcom/anythink/basead/ui/EndCardView;->b:I

    .line 81
    iput p3, p0, Lcom/anythink/basead/ui/EndCardView;->c:I

    .line 83
    invoke-static {p5}, Lcom/anythink/basead/ui/EndCardView;->a(Lcom/anythink/core/common/d/j;)I

    move-result p2

    iput p2, p0, Lcom/anythink/basead/ui/EndCardView;->k:I

    .line 84
    iput-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->n:Lcom/anythink/core/common/d/j;

    .line 1175
    new-instance p2, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->i:Lcom/anythink/basead/ui/component/RoundImageView;

    .line 1176
    sget-object p3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, p3}, Lcom/anythink/basead/ui/component/RoundImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1178
    new-instance p2, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->h:Landroid/widget/ImageView;

    .line 1180
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p2, p3, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1183
    new-instance p5, Landroid/widget/RelativeLayout$LayoutParams;

    iget p9, p0, Lcom/anythink/basead/ui/EndCardView;->b:I

    iget v3, p0, Lcom/anythink/basead/ui/EndCardView;->c:I

    invoke-direct {p5, p9, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p9, 0xd

    .line 1184
    invoke-virtual {p5, p9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1186
    iget-object p9, p0, Lcom/anythink/basead/ui/EndCardView;->i:Lcom/anythink/basead/ui/component/RoundImageView;

    iget v3, p0, Lcom/anythink/basead/ui/EndCardView;->d:I

    invoke-virtual {p0, p9, v3, p2}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1187
    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->h:Landroid/widget/ImageView;

    iget p9, p0, Lcom/anythink/basead/ui/EndCardView;->e:I

    invoke-virtual {p0, p2, p9, p5}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1189
    new-instance p2, Lcom/anythink/basead/ui/EndCardView$3;

    invoke-direct {p2, p0, p7}, Lcom/anythink/basead/ui/EndCardView$3;-><init>(Lcom/anythink/basead/ui/EndCardView;Z)V

    iget p5, p0, Lcom/anythink/basead/ui/EndCardView;->k:I

    int-to-long v3, p5

    invoke-virtual {p0, p2, v3, v4}, Lcom/anythink/basead/ui/EndCardView;->postDelayed(Ljava/lang/Runnable;J)Z

    const/16 p2, 0xc

    if-eqz p6, :cond_0

    .line 1214
    new-instance p5, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p6

    invoke-direct {p5, p6}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    .line 1216
    new-instance p5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 p6, -0x2

    invoke-direct {p5, p6, p6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p6, 0xb

    .line 1217
    invoke-virtual {p5, p6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1218
    invoke-virtual {p5, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1219
    iget-object p6, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, p6, p5}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1221
    invoke-virtual {p4}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object p5

    .line 1222
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p6

    if-nez p6, :cond_0

    .line 1223
    iget-object p6, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {p6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p6

    .line 1224
    iget p7, p6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1225
    iget p6, p6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1226
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p9

    invoke-static {p9}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object p9

    new-instance v3, Lcom/anythink/core/common/res/e;

    invoke-direct {v3, v1, p5}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v4, Lcom/anythink/basead/ui/EndCardView$4;

    invoke-direct {v4, p0, p5}, Lcom/anythink/basead/ui/EndCardView$4;-><init>(Lcom/anythink/basead/ui/EndCardView;Ljava/lang/String;)V

    invoke-virtual {p9, v3, p7, p6, v4}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    :cond_0
    if-eqz p8, :cond_1

    .line 1302
    new-instance p5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p6

    invoke-direct {p5, p6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    .line 1303
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p6

    const-string p7, "myoffer_cta_learn_more"

    const-string p8, "string"

    invoke-static {p6, p7, p8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p6

    invoke-virtual {p5, p6}, Landroid/widget/TextView;->setText(I)V

    .line 1304
    iget-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const-string p6, "#ffffffff"

    invoke-static {p6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p6

    invoke-virtual {p5, p6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1305
    iget-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/high16 p6, 0x41a00000    # 20.0f

    invoke-virtual {p5, p6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1306
    iget-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/16 p6, 0x11

    invoke-virtual {p5, p6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1307
    iget-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p6

    const-string p7, "myoffer_splash_btn"

    const-string p8, "drawable"

    invoke-static {p6, p7, p8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p6

    invoke-virtual {p5, p6}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1309
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p5

    const/high16 p6, 0x43480000    # 200.0f

    invoke-static {p5, p6}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p5

    .line 1310
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p6

    const/high16 p7, 0x428c0000    # 70.0f

    invoke-static {p6, p7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p6

    .line 1311
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p7

    const/high16 p8, 0x41b80000    # 23.0f

    invoke-static {p7, p8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p7

    .line 1313
    new-instance p8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p8, p5, p6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p5, 0xe

    .line 1314
    invoke-virtual {p8, p5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1315
    invoke-virtual {p8, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1316
    iput p7, p8, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1318
    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    iget-object p5, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1320
    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0, p2, p8}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1209
    :cond_1
    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p2}, Lcom/anythink/basead/ui/EndCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1325
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-ne p2, v2, :cond_2

    .line 1326
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1329
    :cond_2
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p2, p3, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1330
    invoke-virtual {p1, p0, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2132
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object p1

    new-instance p2, Lcom/anythink/core/common/res/e;

    invoke-virtual {p4}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, v1, p3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    iget p3, p0, Lcom/anythink/basead/ui/EndCardView;->b:I

    iget p5, p0, Lcom/anythink/basead/ui/EndCardView;->c:I

    new-instance p6, Lcom/anythink/basead/ui/EndCardView$2;

    invoke-direct {p6, p0, p4}, Lcom/anythink/basead/ui/EndCardView$2;-><init>(Lcom/anythink/basead/ui/EndCardView;Lcom/anythink/core/common/d/h;)V

    invoke-virtual {p1, p2, p3, p5, p6}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 2170
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-void

    :catch_1
    move-exception p1

    .line 2168
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    return-void
.end method

.method private static a(Lcom/anythink/core/common/d/j;)I
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 97
    invoke-virtual {p0}, Lcom/anythink/core/common/d/j;->q()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    if-nez v1, :cond_0

    return v0

    .line 102
    :cond_0
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    const/16 v3, 0x64

    .line 103
    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    if-le v3, v1, :cond_1

    return v0

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/anythink/core/common/d/j;->r()I

    move-result v1

    .line 109
    invoke-virtual {p0}, Lcom/anythink/core/common/d/j;->s()I

    move-result p0

    if-gtz p0, :cond_2

    return v0

    :cond_2
    if-ne v1, p0, :cond_3

    return v1

    :cond_3
    sub-int/2addr p0, v1

    .line 120
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/util/Random;->nextInt(I)I

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr p0, v1

    return p0

    :catchall_0
    move-exception p0

    .line 122
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    return v0
.end method

.method static synthetic a(Lcom/anythink/basead/ui/EndCardView;)Lcom/anythink/core/common/d/j;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->n:Lcom/anythink/core/common/d/j;

    return-object p0
.end method

.method private a()V
    .locals 6

    .line 246
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 247
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_video_close"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 249
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    const/high16 v3, 0x41e80000    # 29.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 250
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-static {v2, v4, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 251
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    const/high16 v5, 0x41980000    # 19.0f

    invoke-static {v2, v5, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 252
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xb

    .line 253
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 254
    iput v2, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 255
    iput v3, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 256
    iget v2, p0, Lcom/anythink/basead/ui/EndCardView;->f:I

    invoke-virtual {p0, v0, v2, v4}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 259
    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/anythink/basead/ui/a/a;->a(Landroid/view/View;I)V

    .line 261
    new-instance v1, Lcom/anythink/basead/ui/EndCardView$5;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/EndCardView$5;-><init>(Lcom/anythink/basead/ui/EndCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 3

    .line 325
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 326
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 329
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 330
    invoke-virtual {p1, p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/h;)V
    .locals 5

    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/res/e;

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    iget v2, p0, Lcom/anythink/basead/ui/EndCardView;->b:I

    iget v3, p0, Lcom/anythink/basead/ui/EndCardView;->c:I

    new-instance v4, Lcom/anythink/basead/ui/EndCardView$2;

    invoke-direct {v4, p0, p1}, Lcom/anythink/basead/ui/EndCardView$2;-><init>(Lcom/anythink/basead/ui/EndCardView;Lcom/anythink/core/common/d/h;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 170
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-void

    :catch_1
    move-exception p1

    .line 168
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/h;ZZZ)V
    .locals 4

    .line 175
    new-instance v0, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->i:Lcom/anythink/basead/ui/component/RoundImageView;

    .line 176
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/component/RoundImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 178
    new-instance v0, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->h:Landroid/widget/ImageView;

    .line 180
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 183
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, p0, Lcom/anythink/basead/ui/EndCardView;->b:I

    iget v3, p0, Lcom/anythink/basead/ui/EndCardView;->c:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    .line 184
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 186
    iget-object v2, p0, Lcom/anythink/basead/ui/EndCardView;->i:Lcom/anythink/basead/ui/component/RoundImageView;

    iget v3, p0, Lcom/anythink/basead/ui/EndCardView;->d:I

    invoke-virtual {p0, v2, v3, v0}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 187
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->h:Landroid/widget/ImageView;

    iget v2, p0, Lcom/anythink/basead/ui/EndCardView;->e:I

    invoke-virtual {p0, v0, v2, v1}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 189
    new-instance v0, Lcom/anythink/basead/ui/EndCardView$3;

    invoke-direct {v0, p0, p3}, Lcom/anythink/basead/ui/EndCardView$3;-><init>(Lcom/anythink/basead/ui/EndCardView;Z)V

    iget p3, p0, Lcom/anythink/basead/ui/EndCardView;->k:I

    int-to-long v1, p3

    invoke-virtual {p0, v0, v1, v2}, Lcom/anythink/basead/ui/EndCardView;->postDelayed(Ljava/lang/Runnable;J)Z

    const/16 p3, 0xc

    if-eqz p2, :cond_0

    .line 2214
    new-instance p2, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    .line 2216
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xb

    .line 2217
    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2218
    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2219
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p2}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2221
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object p1

    .line 2222
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 2223
    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    .line 2224
    iget v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2225
    iget p2, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2226
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/res/e;

    const/4 v3, 0x1

    invoke-direct {v2, v3, p1}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v3, Lcom/anythink/basead/ui/EndCardView$4;

    invoke-direct {v3, p0, p1}, Lcom/anythink/basead/ui/EndCardView$4;-><init>(Lcom/anythink/basead/ui/EndCardView;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0, p2, v3}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    :cond_0
    if-eqz p4, :cond_1

    .line 2302
    new-instance p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    .line 2303
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p4, "myoffer_cta_learn_more"

    const-string v0, "string"

    invoke-static {p2, p4, v0}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 2304
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const-string p2, "#ffffffff"

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2305
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/high16 p2, 0x41a00000    # 20.0f

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2306
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/16 p2, 0x11

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setGravity(I)V

    .line 2307
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p4, "myoffer_splash_btn"

    const-string v0, "drawable"

    invoke-static {p2, p4, v0}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 2309
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p1

    const/high16 p2, 0x43480000    # 200.0f

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p1

    .line 2310
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p2

    const/high16 p4, 0x428c0000    # 70.0f

    invoke-static {p2, p4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p2

    .line 2311
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object p4

    const/high16 v0, 0x41b80000    # 23.0f

    invoke-static {p4, v0}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result p4

    .line 2313
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p1, 0xe

    .line 2314
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2315
    invoke-virtual {v0, p3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2316
    iput p4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2318
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2320
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0, p1, v0}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    :cond_1
    iget-object p1, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/EndCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/basead/ui/EndCardView;)Lcom/anythink/basead/ui/EndCardView$a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->a:Lcom/anythink/basead/ui/EndCardView$a;

    return-object p0
.end method

.method private b()V
    .locals 4

    .line 272
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    .line 273
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_feedback_text"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 274
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 275
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 276
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_bg_feedback_button"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 277
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 279
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 280
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/high16 v3, 0x40800000    # 4.0f

    invoke-static {v1, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 281
    iget-object v3, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 283
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v2, 0x41e80000    # 29.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 284
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xb

    .line 285
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 286
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v1, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 287
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v3, 0x428c0000    # 70.0f

    invoke-static {v1, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 289
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    new-instance v1, Lcom/anythink/basead/ui/EndCardView$6;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/EndCardView$6;-><init>(Lcom/anythink/basead/ui/EndCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private b(Lcom/anythink/core/common/d/h;)V
    .locals 5

    .line 214
    new-instance v0, Lcom/anythink/basead/ui/component/RoundImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/component/RoundImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    .line 216
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    .line 217
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xc

    .line 218
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 219
    iget-object v1, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object p1

    .line 222
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 224
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 225
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 226
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v2

    new-instance v3, Lcom/anythink/core/common/res/e;

    const/4 v4, 0x1

    invoke-direct {v3, v4, p1}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v4, Lcom/anythink/basead/ui/EndCardView$4;

    invoke-direct {v4, p0, p1}, Lcom/anythink/basead/ui/EndCardView$4;-><init>(Lcom/anythink/basead/ui/EndCardView;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v1, v0, v4}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/EndCardView;)Landroid/widget/TextView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    return-object p0
.end method

.method private c()V
    .locals 4

    .line 302
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    .line 303
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_cta_learn_more"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 304
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const-string v1, "#ffffffff"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 305
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 306
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 307
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_splash_btn"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 309
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v0

    .line 310
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v1

    .line 311
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41b80000    # 23.0f

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v2

    .line 313
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xe

    .line 314
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xc

    .line 315
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 316
    iput v2, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 318
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/anythink/basead/ui/EndCardView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->l:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v3}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic d(Lcom/anythink/basead/ui/EndCardView;)Landroid/widget/ImageView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->h:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/basead/ui/EndCardView;)Lcom/anythink/basead/ui/component/RoundImageView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->i:Lcom/anythink/basead/ui/component/RoundImageView;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/basead/ui/EndCardView;)V
    .locals 4

    .line 3272
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    .line 3273
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_feedback_text"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 3274
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3275
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 3276
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_bg_feedback_button"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 3277
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 3279
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 3280
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/high16 v3, 0x40800000    # 4.0f

    invoke-static {v1, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 3281
    iget-object v3, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 3283
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v2, 0x41e80000    # 29.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 3284
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xb

    .line 3285
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3286
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v1, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3287
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v3, 0x428c0000    # 70.0f

    invoke-static {v1, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3289
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3291
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    new-instance v1, Lcom/anythink/basead/ui/EndCardView$6;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/EndCardView$6;-><init>(Lcom/anythink/basead/ui/EndCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic g(Lcom/anythink/basead/ui/EndCardView;)V
    .locals 6

    .line 4246
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 4247
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_video_close"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 4249
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    const/high16 v3, 0x41e80000    # 29.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 4250
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-static {v2, v4, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 4251
    invoke-virtual {p0}, Lcom/anythink/basead/ui/EndCardView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    const/high16 v5, 0x41980000    # 19.0f

    invoke-static {v2, v5, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 4252
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xb

    .line 4253
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 4254
    iput v2, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 4255
    iput v3, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 4256
    iget v2, p0, Lcom/anythink/basead/ui/EndCardView;->f:I

    invoke-virtual {p0, v0, v2, v4}, Lcom/anythink/basead/ui/EndCardView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4259
    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/anythink/basead/ui/a/a;->a(Landroid/view/View;I)V

    .line 4261
    new-instance v1, Lcom/anythink/basead/ui/EndCardView$5;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/EndCardView$5;-><init>(Lcom/anythink/basead/ui/EndCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic h(Lcom/anythink/basead/ui/EndCardView;)Landroid/widget/ImageView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/basead/ui/EndCardView;->j:Landroid/widget/ImageView;

    return-object p0
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 0

    .line 335
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public removeFeedbackButton()V
    .locals 2

    .line 339
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/anythink/basead/ui/EndCardView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method
