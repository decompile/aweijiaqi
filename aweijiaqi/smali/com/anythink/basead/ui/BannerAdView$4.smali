.class final Lcom/anythink/basead/ui/BannerAdView$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/common/res/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/ui/BannerAdView;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/anythink/core/common/res/image/RecycleImageView;

.field final synthetic c:Lcom/anythink/core/common/res/image/RecycleImageView;

.field final synthetic d:Lcom/anythink/basead/ui/BannerAdView;


# direct methods
.method constructor <init>(Lcom/anythink/basead/ui/BannerAdView;Ljava/lang/String;Lcom/anythink/core/common/res/image/RecycleImageView;Lcom/anythink/core/common/res/image/RecycleImageView;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/anythink/basead/ui/BannerAdView$4;->d:Lcom/anythink/basead/ui/BannerAdView;

    iput-object p2, p0, Lcom/anythink/basead/ui/BannerAdView$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/basead/ui/BannerAdView$4;->b:Lcom/anythink/core/common/res/image/RecycleImageView;

    iput-object p4, p0, Lcom/anythink/basead/ui/BannerAdView$4;->c:Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFail(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final onSuccess(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView$4;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 238
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView$4;->b:Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/res/image/RecycleImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 241
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView$4;->d:Lcom/anythink/basead/ui/BannerAdView;

    new-instance v0, Lcom/anythink/basead/ui/BannerAdView$4$1;

    invoke-direct {v0, p0, p2}, Lcom/anythink/basead/ui/BannerAdView$4$1;-><init>(Lcom/anythink/basead/ui/BannerAdView$4;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Lcom/anythink/basead/ui/BannerAdView;->post(Ljava/lang/Runnable;)Z

    .line 260
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView$4;->d:Lcom/anythink/basead/ui/BannerAdView;

    invoke-virtual {p1}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/b;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 261
    iget-object p2, p0, Lcom/anythink/basead/ui/BannerAdView$4;->c:Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual {p2, p1}, Lcom/anythink/core/common/res/image/RecycleImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
