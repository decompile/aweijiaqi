.class public Lcom/anythink/basead/ui/BaseAdActivity;
.super Landroid/app/Activity;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/anythink/basead/ui/FullScreenAdView;

.field private c:Lcom/anythink/core/common/d/i;

.field private d:Lcom/anythink/core/common/d/h;

.field private e:Ljava/lang/String;

.field private f:Lcom/anythink/basead/f/b$b;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:J

.field private j:I

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    const-class v0, Lcom/anythink/basead/ui/BaseAdActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/BaseAdActivity;)Lcom/anythink/basead/f/b$b;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 73
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string v1, "extra_scenario"

    .line 76
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->g:Ljava/lang/String;

    const-string v1, "extra_ad_format"

    const/4 v2, 0x1

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->h:I

    const-string v1, "extra_offer_ad"

    .line 78
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/common/d/h;

    iput-object v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->d:Lcom/anythink/core/common/d/h;

    const-string v1, "extra_request_info"

    .line 79
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/common/d/i;

    iput-object v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    const-string v1, "extra_event_id"

    .line 80
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->e:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_0
    int-to-long v0, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    goto :goto_0

    :goto_1
    iput-wide v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->i:J

    return-void

    .line 86
    :cond_1
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Intent is null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/anythink/basead/c/a;)V
    .locals 3

    .line 54
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 56
    iget v1, p1, Lcom/anythink/basead/c/a;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 57
    const-class v1, Lcom/anythink/basead/ui/AdLandscapeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 59
    :cond_0
    const-class v1, Lcom/anythink/basead/ui/AdPortraitActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 62
    :goto_0
    iget-object v1, p1, Lcom/anythink/basead/c/a;->b:Ljava/lang/String;

    const-string v2, "extra_scenario"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    iget v1, p1, Lcom/anythink/basead/c/a;->a:I

    const-string v2, "extra_ad_format"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    iget-object v1, p1, Lcom/anythink/basead/c/a;->c:Lcom/anythink/core/common/d/h;

    const-string v2, "extra_offer_ad"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 65
    iget-object v1, p1, Lcom/anythink/basead/c/a;->d:Ljava/lang/String;

    const-string v2, "extra_event_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    iget-object p1, p1, Lcom/anythink/basead/c/a;->g:Lcom/anythink/core/common/d/i;

    const-string v1, "extra_request_info"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    .line 68
    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "extra_is_show_end_card"

    .line 163
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->k:Z

    const-string v0, "extra_show_feedback_button"

    .line 164
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->l:Z

    :cond_0
    return-void
.end method

.method private b()Lcom/anythink/basead/ui/FullScreenAdView;
    .locals 8

    .line 103
    new-instance v7, Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v2, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdActivity;->d:Lcom/anythink/core/common/d/h;

    iget-object v4, p0, Lcom/anythink/basead/ui/BaseAdActivity;->g:Ljava/lang/String;

    iget v5, p0, Lcom/anythink/basead/ui/BaseAdActivity;->h:I

    iget v6, p0, Lcom/anythink/basead/ui/BaseAdActivity;->j:I

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/anythink/basead/ui/FullScreenAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;II)V

    return-object v7
.end method

.method private c()V
    .locals 3

    .line 170
    new-instance v0, Lcom/anythink/basead/ui/BaseAdActivity$1;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/BaseAdActivity$1;-><init>(Lcom/anythink/basead/ui/BaseAdActivity;)V

    .line 231
    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v1, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->setListener(Lcom/anythink/basead/f/b$b;)V

    .line 232
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-wide v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->i:J

    invoke-virtual {v0, v1, v2}, Lcom/anythink/basead/ui/FullScreenAdView;->setShowBannerTime(J)V

    .line 233
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-boolean v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->k:Z

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/FullScreenAdView;->setIsShowEndCard(Z)V

    .line 234
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-boolean v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->l:Z

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/FullScreenAdView;->setHideFeedbackButton(Z)V

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->init()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 239
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 241
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 110
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 114
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x1

    .line 117
    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/BaseAdActivity;->requestWindowFeature(I)Z

    .line 118
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 120
    instance-of v1, p0, Lcom/anythink/basead/ui/AdLandscapeActivity;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    .line 121
    iput v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->j:I

    goto :goto_0

    .line 123
    :cond_1
    iput v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->j:I

    .line 1073
    :goto_0
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_0
    const-string v2, "extra_scenario"

    .line 1076
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/anythink/basead/ui/BaseAdActivity;->g:Ljava/lang/String;

    const-string v2, "extra_ad_format"

    .line 1077
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->h:I

    const-string v0, "extra_offer_ad"

    .line 1078
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/d/h;

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->d:Lcom/anythink/core/common/d/h;

    const-string v0, "extra_request_info"

    .line 1079
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/d/i;

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    const-string v0, "extra_event_id"

    .line 1080
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->e:Ljava/lang/String;

    .line 1082
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz v0, :cond_4

    .line 1083
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_1
    int-to-long v0, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/j;->l()I

    move-result v0

    goto :goto_1

    :goto_2
    iput-wide v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->i:J

    goto :goto_3

    .line 1086
    :cond_3
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Intent is null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 1089
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    :cond_4
    :goto_3
    invoke-static {}, Lcom/anythink/basead/f/b;->a()Lcom/anythink/basead/f/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/f/b;->a(Ljava/lang/String;)Lcom/anythink/basead/f/b$b;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    .line 128
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    const-string v1, "40002"

    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-nez v0, :cond_5

    goto/16 :goto_5

    .line 141
    :cond_5
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->d:Lcom/anythink/core/common/d/h;

    if-nez v0, :cond_7

    .line 142
    sget-object p1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " onCreate: OfferAd = null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :try_start_1
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    if-eqz p1, :cond_6

    .line 145
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/b$b;->a(Lcom/anythink/basead/c/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception p1

    .line 148
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 150
    :cond_6
    :goto_4
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->finish()V

    return-void

    :cond_7
    if-eqz p1, :cond_8

    const-string v0, "extra_is_show_end_card"

    .line 1163
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->k:Z

    const-string v0, "extra_show_feedback_button"

    .line 1164
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->l:Z

    .line 2103
    :cond_8
    new-instance p1, Lcom/anythink/basead/ui/FullScreenAdView;

    iget-object v2, p0, Lcom/anythink/basead/ui/BaseAdActivity;->c:Lcom/anythink/core/common/d/i;

    iget-object v3, p0, Lcom/anythink/basead/ui/BaseAdActivity;->d:Lcom/anythink/core/common/d/h;

    iget-object v4, p0, Lcom/anythink/basead/ui/BaseAdActivity;->g:Ljava/lang/String;

    iget v5, p0, Lcom/anythink/basead/ui/BaseAdActivity;->h:I

    iget v6, p0, Lcom/anythink/basead/ui/BaseAdActivity;->j:I

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/anythink/basead/ui/FullScreenAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Ljava/lang/String;II)V

    .line 155
    iput-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    .line 157
    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/BaseAdActivity;->setContentView(Landroid/view/View;)V

    .line 2170
    new-instance p1, Lcom/anythink/basead/ui/BaseAdActivity$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BaseAdActivity$1;-><init>(Lcom/anythink/basead/ui/BaseAdActivity;)V

    .line 2231
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0, p1}, Lcom/anythink/basead/ui/FullScreenAdView;->setListener(Lcom/anythink/basead/f/b$b;)V

    .line 2232
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-wide v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->i:J

    invoke-virtual {p1, v0, v1}, Lcom/anythink/basead/ui/FullScreenAdView;->setShowBannerTime(J)V

    .line 2233
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->k:Z

    invoke-virtual {p1, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->setIsShowEndCard(Z)V

    .line 2234
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    iget-boolean v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->l:Z

    invoke-virtual {p1, v0}, Lcom/anythink/basead/ui/FullScreenAdView;->setHideFeedbackButton(Z)V

    .line 2237
    :try_start_2
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {p1}, Lcom/anythink/basead/ui/FullScreenAdView;->init()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_1
    move-exception p1

    .line 2239
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 2241
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->finish()V

    return-void

    .line 129
    :cond_9
    :goto_5
    sget-object p1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Start FullScreen Ad Error."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :try_start_3
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    if-eqz p1, :cond_a

    .line 132
    iget-object p1, p0, Lcom/anythink/basead/ui/BaseAdActivity;->f:Lcom/anythink/basead/f/b$b;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/b$b;->a(Lcom/anythink/basead/c/f;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_6

    :catchall_2
    move-exception p1

    .line 135
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 137
    :cond_a
    :goto_6
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BaseAdActivity;->finish()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->m()V

    .line 287
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 295
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected onPause()V
    .locals 1

    .line 273
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 275
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->l()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 264
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 266
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->k()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .line 248
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 249
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    if-eqz v0, :cond_1

    .line 250
    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->isShowEndCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    const-string v1, "onSaveInstanceState... mFullScreenAdView.isShowEndCard() - true"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    const-string v1, "extra_is_show_end_card"

    .line 252
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BaseAdActivity;->b:Lcom/anythink/basead/ui/FullScreenAdView;

    invoke-virtual {v0}, Lcom/anythink/basead/ui/FullScreenAdView;->needHideFeedbackButton()Z

    move-result v0

    .line 257
    sget-object v1, Lcom/anythink/basead/ui/BaseAdActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onSaveInstanceState... mFullScreenAdView.needShowFeedbackButton() - "

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "extra_show_feedback_button"

    .line 258
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method
