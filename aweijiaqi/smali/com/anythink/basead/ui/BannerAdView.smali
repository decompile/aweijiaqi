.class public Lcom/anythink/basead/ui/BannerAdView;
.super Lcom/anythink/basead/ui/BaseAdView;


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static final x:I = 0x1

.field private static final y:I = 0x2


# instance fields
.field private final A:Landroid/view/View$OnClickListener;

.field a:Lcom/anythink/basead/f/a;

.field b:Z

.field c:Ljava/lang/String;

.field d:I

.field e:I

.field private v:Landroid/view/View;

.field private w:I

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    const-class v0, Lcom/anythink/basead/ui/BannerAdView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/ui/BannerAdView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x2

    .line 52
    iput p1, p0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    .line 56
    new-instance p1, Lcom/anythink/basead/ui/BannerAdView$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BannerAdView$1;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->z:Landroid/view/View$OnClickListener;

    .line 69
    new-instance p1, Lcom/anythink/basead/ui/BannerAdView$2;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BannerAdView$2;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->A:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/basead/ui/BaseAdView;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Lcom/anythink/core/common/d/h;)V

    const/4 p1, 0x2

    .line 52
    iput p1, p0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    .line 56
    new-instance p1, Lcom/anythink/basead/ui/BannerAdView$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BannerAdView$1;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->z:Landroid/view/View$OnClickListener;

    .line 69
    new-instance p1, Lcom/anythink/basead/ui/BannerAdView$2;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BannerAdView$2;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    iput-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->A:Landroid/view/View$OnClickListener;

    .line 83
    iput-object p4, p0, Lcom/anythink/basead/ui/BannerAdView;->a:Lcom/anythink/basead/f/a;

    .line 1166
    new-instance p1, Lcom/anythink/basead/ui/BannerAdView$3;

    invoke-direct {p1, p0}, Lcom/anythink/basead/ui/BannerAdView$3;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/BannerAdView;->a(Ljava/lang/Runnable;)V

    .line 1471
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x0

    :goto_0
    if-ge p2, p1, :cond_1

    .line 1474
    iget-object p3, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {p3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    if-eqz p3, :cond_0

    .line 1476
    iget-object p4, p0, Lcom/anythink/basead/ui/BannerAdView;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1480
    :cond_1
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/BannerAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1482
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    new-instance p2, Lcom/anythink/basead/ui/BannerAdView$9;

    invoke-direct {p2, p0}, Lcom/anythink/basead/ui/BannerAdView$9;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/BannerAdView;)I
    .locals 0

    .line 39
    iget p0, p0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    return p0
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/u;

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 177
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast p1, Lcom/anythink/core/common/d/u;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/u;->z()I

    move-result p1

    if-eq p1, v2, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    goto :goto_0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v0, v0, Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_1

    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/anythink/basead/a/a/b;->b(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 193
    :cond_2
    iput v1, p0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    return v1
.end method

.method static synthetic b(Lcom/anythink/basead/ui/BannerAdView;)V
    .locals 0

    .line 39
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 11

    .line 200
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "myoffer_banner_root"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 201
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_banner_close"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 202
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "myoffer_banner_ad_text"

    invoke-static {v2, v3, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 204
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/j;->i()I

    move-result v3

    const/16 v4, 0x8

    const/4 v5, 0x0

    if-nez v3, :cond_0

    .line 205
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    const-string v6, "728x90"

    invoke-static {v6, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 209
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41b80000    # 23.0f

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 210
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 211
    iget-object v6, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 214
    :cond_0
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 221
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 222
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 223
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 224
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    new-instance v0, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 228
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 229
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v5, v3}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 232
    new-instance v3, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 233
    sget-object v7, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v7}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 234
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v7

    new-instance v8, Lcom/anythink/core/common/res/e;

    const/4 v9, 0x1

    invoke-direct {v8, v9, p1}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v10, Lcom/anythink/basead/ui/BannerAdView$4;

    invoke-direct {v10, p0, p1, v3, v0}, Lcom/anythink/basead/ui/BannerAdView$4;-><init>(Lcom/anythink/basead/ui/BannerAdView;Ljava/lang/String;Lcom/anythink/core/common/res/image/RecycleImageView;Lcom/anythink/core/common/res/image/RecycleImageView;)V

    invoke-virtual {v7, v8, v10}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 270
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xd

    .line 272
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 273
    invoke-virtual {p0, v3, v9, p1}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 276
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 277
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "myoffer_banner_self_ad_logo"

    invoke-static {p1, v0, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 278
    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/res/e;

    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v9, v3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v3, Lcom/anythink/basead/ui/BannerAdView$5;

    invoke-direct {v3, p0, p1}, Lcom/anythink/basead/ui/BannerAdView$5;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1, v3}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 292
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 297
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 299
    :cond_3
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 301
    :goto_1
    iget-object p1, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic c(Lcom/anythink/basead/ui/BannerAdView;)V
    .locals 0

    .line 39
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method static synthetic d(Lcom/anythink/basead/ui/BannerAdView;)V
    .locals 0

    .line 39
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->g()V

    return-void
.end method

.method static synthetic e(Lcom/anythink/basead/ui/BannerAdView;)V
    .locals 0

    .line 39
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->f()V

    return-void
.end method

.method private static synthetic f(Lcom/anythink/basead/ui/BannerAdView;)V
    .locals 0

    .line 39
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    return-void
.end method

.method private static k()V
    .locals 0

    return-void
.end method

.method private l()V
    .locals 18

    move-object/from16 v0, p0

    .line 104
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v1

    .line 106
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v2, v2, Lcom/anythink/core/common/d/u;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 108
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    const-string v5, "300x250"

    const-string v6, "320x90"

    const-string v7, "320x50"

    const/4 v8, 0x3

    const-string v9, "728x90"

    const/4 v10, 0x2

    const/4 v11, 0x0

    const/4 v13, 0x1

    sparse-switch v4, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_1
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_2
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_3
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v1, -0x1

    :goto_2
    const/16 v4, 0x5a

    const/16 v14, 0x140

    const-string v15, "myoffer_banner_ad_layout_320x50"

    if-eqz v1, :cond_6

    if-eq v1, v13, :cond_4

    if-eq v1, v10, :cond_2

    .line 144
    iput-object v7, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    .line 145
    iput v14, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    const/16 v1, 0x32

    .line 146
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_8

    .line 148
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_8

    .line 149
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 132
    :cond_2
    iput-object v9, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    const/16 v1, 0x2d8

    .line 133
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    .line 134
    iput v4, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_3

    .line 136
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_3

    .line 137
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->x()Ljava/lang/String;

    move-result-object v2

    .line 139
    :cond_3
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_728x90"

    goto :goto_3

    .line 121
    :cond_4
    iput-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    const/16 v1, 0x12c

    .line 122
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    const/16 v1, 0xfa

    .line 123
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_5

    .line 125
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_5

    .line 126
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->w()Ljava/lang/String;

    move-result-object v2

    .line 128
    :cond_5
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_300x250"

    goto :goto_3

    .line 110
    :cond_6
    iput-object v6, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    .line 111
    iput v14, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    .line 112
    iput v4, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_7

    .line 114
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_7

    .line 115
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->v()Ljava/lang/String;

    move-result-object v2

    .line 117
    :cond_7
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_320x90"

    .line 3176
    :cond_8
    :goto_3
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/u;

    if-eqz v1, :cond_9

    .line 3177
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/u;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/u;->z()I

    move-result v1

    if-eq v1, v13, :cond_b

    if-eq v1, v8, :cond_a

    goto :goto_4

    .line 3186
    :cond_9
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_b

    .line 3187
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {v2}, Lcom/anythink/basead/a/a/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_4

    :cond_a
    const/4 v1, 0x2

    goto :goto_5

    :cond_b
    :goto_4
    const/4 v1, 0x1

    .line 3193
    :goto_5
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    const-string v4, "myoffer_banner_self_ad_logo"

    const-string v14, "myoffer_banner_ad_text"

    const-string v3, "myoffer_banner_close"

    const-string v8, "layout"

    const/16 v10, 0x8

    const-string v12, "id"

    if-ne v13, v1, :cond_10

    .line 155
    sget-object v1, Lcom/anythink/basead/ui/BannerAdView;->TAG:Ljava/lang/String;

    const-string v5, "mode: pure picture"

    invoke-static {v1, v5}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "myoffer_banner_ad_layout_pure_picture"

    invoke-static {v5, v6, v8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3200
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v5, "myoffer_banner_root"

    invoke-static {v1, v5, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 3201
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 3202
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v14, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3204
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v5, v5, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/j;->i()I

    move-result v5

    if-nez v5, :cond_c

    .line 3205
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    .line 3207
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-static {v9, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 3208
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 3209
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41b80000    # 23.0f

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 3210
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 3211
    iget-object v6, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 3214
    :cond_c
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3221
    :cond_d
    :goto_6
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 3222
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 3223
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 3224
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3227
    new-instance v1, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 3228
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 3229
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v11, v5}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 3232
    new-instance v5, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 3233
    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 3234
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v6

    new-instance v7, Lcom/anythink/core/common/res/e;

    invoke-direct {v7, v13, v2}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v8, Lcom/anythink/basead/ui/BannerAdView$4;

    invoke-direct {v8, v0, v2, v5, v1}, Lcom/anythink/basead/ui/BannerAdView$4;-><init>(Lcom/anythink/basead/ui/BannerAdView;Ljava/lang/String;Lcom/anythink/core/common/res/image/RecycleImageView;Lcom/anythink/core/common/res/image/RecycleImageView;)V

    invoke-virtual {v6, v7, v8}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 3270
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3271
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    .line 3272
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3273
    invoke-virtual {v0, v5, v13, v1}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 3276
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 3277
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3278
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v2

    new-instance v4, Lcom/anythink/core/common/res/e;

    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v13, v5}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v5, Lcom/anythink/basead/ui/BannerAdView$5;

    invoke-direct {v5, v0, v1}, Lcom/anythink/basead/ui/BannerAdView$5;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v4, v5}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 3292
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3296
    :cond_e
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 3297
    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 3299
    :cond_f
    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3301
    :goto_7
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    .line 159
    :cond_10
    sget-object v1, Lcom/anythink/basead/ui/BannerAdView;->TAG:Ljava/lang/String;

    const-string v2, "mode: assemble banner"

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v15, v8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3306
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_banner_icon"

    invoke-static {v1, v2, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 3307
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v8, "myoffer_banner_ad_title"

    invoke-static {v2, v8, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 3308
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v15, "myoffer_banner_desc"

    invoke-static {v8, v15, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 3309
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v13, "myoffer_banner_ad_install_btn"

    invoke-static {v15, v13, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v0, v13}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 3310
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v3, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 3311
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v14, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3314
    iget-object v14, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v14, v14, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v14}, Lcom/anythink/core/common/d/j;->i()I

    move-result v14

    if-nez v14, :cond_11

    const/4 v14, 0x1

    goto :goto_8

    :cond_11
    const/4 v14, 0x0

    :goto_8
    if-eqz v14, :cond_12

    .line 3316
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v15, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 3318
    :cond_12
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v15, v10}, Landroid/view/View;->setVisibility(I)V

    .line 3321
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->hashCode()I

    move-result v10

    const v11, 0x59df59c2

    if-eq v10, v11, :cond_15

    const v11, 0x59df5a3e

    if-eq v10, v11, :cond_14

    const v11, 0x60b65fb2

    if-eq v10, v11, :cond_13

    goto :goto_9

    :cond_13
    invoke-virtual {v15, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x2

    goto :goto_a

    :cond_14
    invoke-virtual {v15, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x1

    goto :goto_a

    :cond_15
    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x0

    goto :goto_a

    :cond_16
    :goto_9
    const/4 v10, -0x1

    :goto_a
    if-eqz v10, :cond_19

    const/4 v11, 0x1

    if-eq v10, v11, :cond_18

    const/4 v11, 0x2

    if-eq v10, v11, :cond_17

    goto :goto_b

    .line 3333
    :cond_17
    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3334
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    const/high16 v15, 0x42380000    # 46.0f

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3335
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b

    .line 3328
    :cond_18
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3329
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    const/high16 v15, 0x41200000    # 10.0f

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3330
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b

    :cond_19
    const/high16 v15, 0x41200000    # 10.0f

    .line 3323
    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3324
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3325
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3341
    :goto_b
    iget-object v10, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v10}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1a

    .line 3342
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    const/4 v11, 0x3

    .line 3343
    invoke-virtual {v1, v11}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    const/4 v11, 0x1

    .line 3344
    invoke-virtual {v1, v11}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 3345
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v15

    new-instance v11, Lcom/anythink/core/common/res/e;

    move-object/from16 v16, v3

    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v3

    move/from16 v17, v14

    const/4 v14, 0x1

    invoke-direct {v11, v14, v3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    iget v3, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v10, v10, Landroid/view/ViewGroup$LayoutParams;->height:I

    new-instance v14, Lcom/anythink/basead/ui/BannerAdView$6;

    invoke-direct {v14, v0, v1}, Lcom/anythink/basead/ui/BannerAdView$6;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v15, v11, v3, v10, v14}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    goto :goto_c

    :cond_1a
    move-object/from16 v16, v3

    move/from16 v17, v14

    .line 3360
    :goto_c
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3363
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3364
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3365
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3366
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3367
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3368
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3373
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 3374
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 3376
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v4

    new-instance v8, Lcom/anythink/core/common/res/e;

    iget-object v10, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v10}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v8, v11, v10}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v10, Lcom/anythink/basead/ui/BannerAdView$7;

    invoke-direct {v10, v0, v3}, Lcom/anythink/basead/ui/BannerAdView$7;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v4, v8, v10}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    goto :goto_d

    :cond_1b
    const/4 v3, 0x0

    .line 3390
    :goto_d
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3394
    iget-boolean v3, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    if-eqz v3, :cond_1d

    .line 3395
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "myoffer_banner_main_image"

    invoke-static {v3, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 3397
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 3398
    invoke-virtual {v3}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, 0x3

    .line 3399
    invoke-virtual {v3, v4}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    const/4 v8, 0x1

    .line 3400
    invoke-virtual {v3, v8}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 3401
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v10

    new-instance v11, Lcom/anythink/core/common/res/e;

    iget-object v12, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v12}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v8, v12}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v8, Lcom/anythink/basead/ui/BannerAdView$8;

    invoke-direct {v8, v0, v3}, Lcom/anythink/basead/ui/BannerAdView$8;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v10, v11, v8}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 3415
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    :cond_1c
    const/4 v4, 0x3

    goto :goto_e

    :cond_1d
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 3418
    :goto_e
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3421
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    const/4 v3, 0x0

    .line 3422
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    :cond_1e
    const/16 v3, 0x8

    .line 3424
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3428
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    goto :goto_f

    :sswitch_4
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x2

    goto :goto_10

    :sswitch_5
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x0

    goto :goto_10

    :sswitch_6
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x3

    goto :goto_10

    :sswitch_7
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x1

    goto :goto_10

    :cond_1f
    :goto_f
    const/4 v8, -0x1

    :goto_10
    if-eqz v8, :cond_23

    const/4 v3, 0x1

    if-eq v8, v3, :cond_22

    const/4 v3, 0x2

    if-eq v8, v3, :cond_21

    .line 3447
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xb

    .line 3448
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz v17, :cond_20

    .line 3450
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_11

    .line 3452
    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_11

    .line 3441
    :cond_21
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3442
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3454
    :goto_11
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_12

    .line 3436
    :cond_22
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3437
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41c80000    # 25.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3438
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_12

    .line 3430
    :cond_23
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xf

    .line 3431
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v3, 0x6

    const/4 v4, -0x1

    .line 3432
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3433
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3460
    :goto_12
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_24

    move-object/from16 v1, v16

    const/4 v2, 0x0

    .line 3461
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13

    :cond_24
    move-object/from16 v1, v16

    const/16 v2, 0x8

    .line 3463
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3465
    :goto_13
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :sswitch_data_0
    .sparse-switch
        -0x215ddd38 -> :sswitch_3
        0x59df59c2 -> :sswitch_2
        0x59df5a3e -> :sswitch_1
        0x60b65fb2 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x215ddd38 -> :sswitch_7
        0x59df59c2 -> :sswitch_6
        0x59df5a3e -> :sswitch_5
        0x60b65fb2 -> :sswitch_4
    .end sparse-switch
.end method

.method private m()V
    .locals 1

    .line 166
    new-instance v0, Lcom/anythink/basead/ui/BannerAdView$3;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/BannerAdView$3;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/BannerAdView;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private n()V
    .locals 16

    move-object/from16 v0, p0

    .line 306
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "id"

    const-string v3, "myoffer_banner_icon"

    invoke-static {v1, v3, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 307
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "myoffer_banner_ad_title"

    invoke-static {v3, v4, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "myoffer_banner_desc"

    invoke-static {v4, v5, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "myoffer_banner_ad_install_btn"

    invoke-static {v5, v6, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 310
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "myoffer_banner_close"

    invoke-static {v6, v7, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 311
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "myoffer_banner_ad_text"

    invoke-static {v6, v7, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 314
    iget-object v7, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v7, v7, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v7}, Lcom/anythink/core/common/d/j;->i()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-nez v7, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    const-string v10, "728x90"

    const-string v11, "320x90"

    const-string v12, "320x50"

    const/16 v15, 0x8

    const/4 v14, 0x2

    if-eqz v7, :cond_1

    .line 316
    iget-object v13, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v13, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 318
    :cond_1
    iget-object v13, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v13, v15}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v13, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->hashCode()I

    move-result v15

    const v8, 0x59df59c2

    if-eq v15, v8, :cond_4

    const v8, 0x59df5a3e

    if-eq v15, v8, :cond_3

    const v8, 0x60b65fb2

    if-eq v15, v8, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x2

    goto :goto_2

    :cond_3
    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x0

    goto :goto_2

    :cond_5
    :goto_1
    const/4 v8, -0x1

    :goto_2
    if-eqz v8, :cond_8

    if-eq v8, v9, :cond_7

    if-eq v8, v14, :cond_6

    goto :goto_3

    .line 333
    :cond_6
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v13

    const/high16 v15, 0x42380000    # 46.0f

    invoke-static {v13, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 335
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 328
    :cond_7
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 329
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v13

    const/high16 v15, 0x41200000    # 10.0f

    invoke-static {v13, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 330
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_8
    const/high16 v15, 0x41200000    # 10.0f

    .line 323
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout$LayoutParams;

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v13

    iput v13, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 325
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 341
    :goto_3
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    const/4 v13, 0x3

    if-nez v8, :cond_9

    .line 342
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 343
    invoke-virtual {v1, v13}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    .line 344
    invoke-virtual {v1, v9}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 345
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v15

    new-instance v14, Lcom/anythink/core/common/res/e;

    iget-object v13, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v13}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v14, v9, v13}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    iget v13, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    new-instance v9, Lcom/anythink/basead/ui/BannerAdView$6;

    invoke-direct {v9, v0, v1}, Lcom/anythink/basead/ui/BannerAdView$6;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v15, v14, v13, v8, v9}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 360
    :cond_9
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v8}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v8, 0x0

    if-nez v4, :cond_a

    .line 374
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v9, "myoffer_banner_self_ad_logo"

    invoke-static {v4, v9, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v9

    new-instance v13, Lcom/anythink/core/common/res/e;

    iget-object v14, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v14}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-direct {v13, v15, v14}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v14, Lcom/anythink/basead/ui/BannerAdView$7;

    invoke-direct {v14, v0, v4}, Lcom/anythink/basead/ui/BannerAdView$7;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v9, v13, v14}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    goto :goto_4

    :cond_a
    move-object v4, v8

    .line 390
    :goto_4
    iget-object v9, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    iget-boolean v4, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    if-eqz v4, :cond_b

    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v8, "myoffer_banner_main_image"

    invoke-static {v4, v8, v2}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 397
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 398
    invoke-virtual {v8}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, 0x3

    .line 399
    invoke-virtual {v8, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    const/4 v4, 0x1

    .line 400
    invoke-virtual {v8, v4}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v9

    new-instance v13, Lcom/anythink/core/common/res/e;

    iget-object v14, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v14}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v4, v14}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v4, Lcom/anythink/basead/ui/BannerAdView$8;

    invoke-direct {v4, v0, v8}, Lcom/anythink/basead/ui/BannerAdView$8;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v9, v13, v4}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 415
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_b
    const/4 v2, 0x3

    .line 418
    :goto_5
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    const/4 v4, 0x0

    .line 422
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9

    :cond_c
    const/16 v4, 0x8

    .line 424
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 428
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto :goto_6

    :sswitch_0
    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v13, 0x2

    goto :goto_7

    :sswitch_1
    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v13, 0x0

    goto :goto_7

    :sswitch_2
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v13, 0x3

    goto :goto_7

    :sswitch_3
    const-string v2, "300x250"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v13, 0x1

    goto :goto_7

    :cond_d
    :goto_6
    const/4 v13, -0x1

    :goto_7
    if-eqz v13, :cond_11

    const/4 v2, 0x1

    if-eq v13, v2, :cond_10

    const/4 v2, 0x2

    if-eq v13, v2, :cond_f

    .line 447
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v2, 0xb

    .line 448
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz v7, :cond_e

    .line 450
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-static {v2, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_8

    .line 452
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v2, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_8

    .line 441
    :cond_f
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 442
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-static {v2, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 454
    :goto_8
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_9

    .line 436
    :cond_10
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41c80000    # 25.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 438
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_9

    .line 430
    :cond_11
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xf

    .line 431
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v3, 0x6

    const/4 v4, -0x1

    .line 432
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 433
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 460
    :goto_9
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x0

    .line 461
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a

    :cond_12
    const/16 v1, 0x8

    .line 463
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    .line 465
    :goto_a
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x215ddd38 -> :sswitch_3
        0x59df59c2 -> :sswitch_2
        0x59df5a3e -> :sswitch_1
        0x60b65fb2 -> :sswitch_0
    .end sparse-switch
.end method

.method private o()V
    .locals 4

    .line 471
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 474
    iget-object v2, p0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 476
    iget-object v3, p0, Lcom/anythink/basead/ui/BannerAdView;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/BannerAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    new-instance v1, Lcom/anythink/basead/ui/BannerAdView$9;

    invoke-direct {v1, p0}, Lcom/anythink/basead/ui/BannerAdView$9;-><init>(Lcom/anythink/basead/ui/BannerAdView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 18

    move-object/from16 v0, p0

    .line 2104
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->h()Ljava/lang/String;

    move-result-object v1

    .line 2106
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v2, v2, Lcom/anythink/core/common/d/u;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 2108
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    const-string v5, "300x250"

    const-string v6, "320x90"

    const-string v7, "320x50"

    const/4 v8, 0x3

    const-string v9, "728x90"

    const/4 v10, 0x2

    const/4 v11, 0x0

    const/4 v13, 0x1

    sparse-switch v4, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_1
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_2
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_3
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v1, -0x1

    :goto_2
    const/16 v4, 0x5a

    const/16 v14, 0x140

    const-string v15, "myoffer_banner_ad_layout_320x50"

    if-eqz v1, :cond_6

    if-eq v1, v13, :cond_4

    if-eq v1, v10, :cond_2

    .line 2144
    iput-object v7, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    .line 2145
    iput v14, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    const/16 v1, 0x32

    .line 2146
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_8

    .line 2148
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_8

    .line 2149
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2132
    :cond_2
    iput-object v9, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    const/16 v1, 0x2d8

    .line 2133
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    .line 2134
    iput v4, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_3

    .line 2136
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_3

    .line 2137
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->x()Ljava/lang/String;

    move-result-object v2

    .line 2139
    :cond_3
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_728x90"

    goto :goto_3

    .line 2121
    :cond_4
    iput-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    const/16 v1, 0x12c

    .line 2122
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    const/16 v1, 0xfa

    .line 2123
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_5

    .line 2125
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_5

    .line 2126
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->w()Ljava/lang/String;

    move-result-object v2

    .line 2128
    :cond_5
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_300x250"

    goto :goto_3

    .line 2110
    :cond_6
    iput-object v6, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    .line 2111
    iput v14, v0, Lcom/anythink/basead/ui/BannerAdView;->d:I

    .line 2112
    iput v4, v0, Lcom/anythink/basead/ui/BannerAdView;->e:I

    if-nez v2, :cond_7

    .line 2114
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_7

    .line 2115
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/p;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->v()Ljava/lang/String;

    move-result-object v2

    .line 2117
    :cond_7
    iput-boolean v13, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    const-string v15, "myoffer_banner_ad_layout_320x90"

    .line 2176
    :cond_8
    :goto_3
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/u;

    if-eqz v1, :cond_9

    .line 2177
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    check-cast v1, Lcom/anythink/core/common/d/u;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/u;->z()I

    move-result v1

    if-eq v1, v13, :cond_b

    if-eq v1, v8, :cond_a

    goto :goto_4

    .line 2186
    :cond_9
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    instance-of v1, v1, Lcom/anythink/core/common/d/p;

    if-eqz v1, :cond_b

    .line 2187
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {v2}, Lcom/anythink/basead/a/a/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_4

    :cond_a
    const/4 v1, 0x2

    goto :goto_5

    :cond_b
    :goto_4
    const/4 v1, 0x1

    .line 2193
    :goto_5
    iput v1, v0, Lcom/anythink/basead/ui/BannerAdView;->w:I

    const-string v4, "myoffer_banner_self_ad_logo"

    const-string v14, "myoffer_banner_ad_text"

    const-string v3, "myoffer_banner_close"

    const-string v8, "layout"

    const/16 v10, 0x8

    const-string v12, "id"

    if-ne v13, v1, :cond_10

    .line 2155
    sget-object v1, Lcom/anythink/basead/ui/BannerAdView;->TAG:Ljava/lang/String;

    const-string v5, "mode: pure picture"

    invoke-static {v1, v5}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "myoffer_banner_ad_layout_pure_picture"

    invoke-static {v5, v6, v8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2200
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v5, "myoffer_banner_root"

    invoke-static {v1, v5, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 2201
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 2202
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v14, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2204
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v5, v5, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/j;->i()I

    move-result v5

    if-nez v5, :cond_c

    .line 2205
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2207
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-static {v9, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2208
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 2209
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41b80000    # 23.0f

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2210
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2211
    iget-object v6, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 2214
    :cond_c
    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2221
    :cond_d
    :goto_6
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    .line 2222
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2223
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2224
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2227
    new-instance v1, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 2228
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2229
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v11, v5}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2232
    new-instance v5, Lcom/anythink/core/common/res/image/RecycleImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/anythink/core/common/res/image/RecycleImageView;-><init>(Landroid/content/Context;)V

    .line 2233
    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Lcom/anythink/core/common/res/image/RecycleImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2234
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v6

    new-instance v7, Lcom/anythink/core/common/res/e;

    invoke-direct {v7, v13, v2}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v8, Lcom/anythink/basead/ui/BannerAdView$4;

    invoke-direct {v8, v0, v2, v5, v1}, Lcom/anythink/basead/ui/BannerAdView$4;-><init>(Lcom/anythink/basead/ui/BannerAdView;Ljava/lang/String;Lcom/anythink/core/common/res/image/RecycleImageView;Lcom/anythink/core/common/res/image/RecycleImageView;)V

    invoke-virtual {v6, v7, v8}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 2270
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2271
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    .line 2272
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2273
    invoke-virtual {v0, v5, v13, v1}, Lcom/anythink/basead/ui/BannerAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2276
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 2277
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2278
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v2

    new-instance v4, Lcom/anythink/core/common/res/e;

    iget-object v5, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v13, v5}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v5, Lcom/anythink/basead/ui/BannerAdView$5;

    invoke-direct {v5, v0, v1}, Lcom/anythink/basead/ui/BannerAdView$5;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v4, v5}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 2292
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2296
    :cond_e
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2297
    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 2299
    :cond_f
    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2301
    :goto_7
    iget-object v1, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    .line 2159
    :cond_10
    sget-object v1, Lcom/anythink/basead/ui/BannerAdView;->TAG:Ljava/lang/String;

    const-string v2, "mode: assemble banner"

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2160
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v15, v8}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2306
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_banner_icon"

    invoke-static {v1, v2, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 2307
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v8, "myoffer_banner_ad_title"

    invoke-static {v2, v8, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2308
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v15, "myoffer_banner_desc"

    invoke-static {v8, v15, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 2309
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    const-string v13, "myoffer_banner_ad_install_btn"

    invoke-static {v15, v13, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v0, v13}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 2310
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v3, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    .line 2311
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v14, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2314
    iget-object v14, v0, Lcom/anythink/basead/ui/BannerAdView;->f:Lcom/anythink/core/common/d/i;

    iget-object v14, v14, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    invoke-virtual {v14}, Lcom/anythink/core/common/d/j;->i()I

    move-result v14

    if-nez v14, :cond_11

    const/4 v14, 0x1

    goto :goto_8

    :cond_11
    const/4 v14, 0x0

    :goto_8
    if-eqz v14, :cond_12

    .line 2316
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v15, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 2318
    :cond_12
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->v:Landroid/view/View;

    invoke-virtual {v15, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2321
    iget-object v15, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->hashCode()I

    move-result v10

    const v11, 0x59df59c2

    if-eq v10, v11, :cond_15

    const v11, 0x59df5a3e

    if-eq v10, v11, :cond_14

    const v11, 0x60b65fb2

    if-eq v10, v11, :cond_13

    goto :goto_9

    :cond_13
    invoke-virtual {v15, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x2

    goto :goto_a

    :cond_14
    invoke-virtual {v15, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x1

    goto :goto_a

    :cond_15
    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    const/4 v10, 0x0

    goto :goto_a

    :cond_16
    :goto_9
    const/4 v10, -0x1

    :goto_a
    if-eqz v10, :cond_19

    const/4 v11, 0x1

    if-eq v10, v11, :cond_18

    const/4 v11, 0x2

    if-eq v10, v11, :cond_17

    goto :goto_b

    .line 2333
    :cond_17
    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2334
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    const/high16 v15, 0x42380000    # 46.0f

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2335
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b

    .line 2328
    :cond_18
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2329
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    const/high16 v15, 0x41200000    # 10.0f

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2330
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b

    :cond_19
    const/high16 v15, 0x41200000    # 10.0f

    .line 2323
    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2324
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v15}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v11

    iput v11, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2325
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2341
    :goto_b
    iget-object v10, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v10}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1a

    .line 2342
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    const/4 v11, 0x3

    .line 2343
    invoke-virtual {v1, v11}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    const/4 v11, 0x1

    .line 2344
    invoke-virtual {v1, v11}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 2345
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v15

    new-instance v11, Lcom/anythink/core/common/res/e;

    move-object/from16 v16, v3

    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object v3

    move/from16 v17, v14

    const/4 v14, 0x1

    invoke-direct {v11, v14, v3}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    iget v3, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v10, v10, Landroid/view/ViewGroup$LayoutParams;->height:I

    new-instance v14, Lcom/anythink/basead/ui/BannerAdView$6;

    invoke-direct {v14, v0, v1}, Lcom/anythink/basead/ui/BannerAdView$6;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v15, v11, v3, v10, v14}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    goto :goto_c

    :cond_1a
    move-object/from16 v16, v3

    move/from16 v17, v14

    .line 2360
    :goto_c
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2363
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2364
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2365
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2366
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2367
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2368
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2373
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 2374
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 2376
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v4

    new-instance v8, Lcom/anythink/core/common/res/e;

    iget-object v10, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v10}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v8, v11, v10}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v10, Lcom/anythink/basead/ui/BannerAdView$7;

    invoke-direct {v10, v0, v3}, Lcom/anythink/basead/ui/BannerAdView$7;-><init>(Lcom/anythink/basead/ui/BannerAdView;Landroid/widget/ImageView;)V

    invoke-virtual {v4, v8, v10}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    goto :goto_d

    :cond_1b
    const/4 v3, 0x0

    .line 2390
    :goto_d
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2394
    iget-boolean v3, v0, Lcom/anythink/basead/ui/BannerAdView;->b:Z

    if-eqz v3, :cond_1d

    .line 2395
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "myoffer_banner_main_image"

    invoke-static {v3, v4, v12}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/anythink/basead/ui/BannerAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/anythink/basead/ui/component/RoundImageView;

    .line 2397
    iget-object v4, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 2398
    invoke-virtual {v3}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, 0x3

    .line 2399
    invoke-virtual {v3, v4}, Lcom/anythink/basead/ui/component/RoundImageView;->setRadiusInDip(I)V

    const/4 v8, 0x1

    .line 2400
    invoke-virtual {v3, v8}, Lcom/anythink/basead/ui/component/RoundImageView;->setNeedRadiu(Z)V

    .line 2401
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v10

    new-instance v11, Lcom/anythink/core/common/res/e;

    iget-object v12, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v12}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v8, v12}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v8, Lcom/anythink/basead/ui/BannerAdView$8;

    invoke-direct {v8, v0, v3}, Lcom/anythink/basead/ui/BannerAdView$8;-><init>(Lcom/anythink/basead/ui/BannerAdView;Lcom/anythink/basead/ui/component/RoundImageView;)V

    invoke-virtual {v10, v11, v8}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 2415
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    :cond_1c
    const/4 v4, 0x3

    goto :goto_e

    :cond_1d
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 2418
    :goto_e
    iget-object v8, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2421
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    const/4 v3, 0x0

    .line 2422
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_12

    :cond_1e
    const/16 v3, 0x8

    .line 2424
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2428
    iget-object v3, v0, Lcom/anythink/basead/ui/BannerAdView;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    goto :goto_f

    :sswitch_4
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x2

    goto :goto_10

    :sswitch_5
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x0

    goto :goto_10

    :sswitch_6
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x3

    goto :goto_10

    :sswitch_7
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const/4 v8, 0x1

    goto :goto_10

    :cond_1f
    :goto_f
    const/4 v8, -0x1

    :goto_10
    if-eqz v8, :cond_23

    const/4 v3, 0x1

    if-eq v8, v3, :cond_22

    const/4 v3, 0x2

    if-eq v8, v3, :cond_21

    .line 2447
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xb

    .line 2448
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz v17, :cond_20

    .line 2450
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_11

    .line 2452
    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto :goto_11

    .line 2441
    :cond_21
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2442
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2454
    :goto_11
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_12

    .line 2436
    :cond_22
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2437
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/basead/ui/BannerAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41c80000    # 25.0f

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2438
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_12

    .line 2430
    :cond_23
    invoke-virtual {v1}, Lcom/anythink/basead/ui/component/RoundImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xf

    .line 2431
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v3, 0x6

    const/4 v4, -0x1

    .line 2432
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2433
    invoke-virtual {v1, v2}, Lcom/anythink/basead/ui/component/RoundImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2460
    :goto_12
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->s()Z

    move-result v1

    if-eqz v1, :cond_24

    move-object/from16 v1, v16

    const/4 v2, 0x0

    .line 2461
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13

    :cond_24
    move-object/from16 v1, v16

    const/16 v2, 0x8

    .line 2463
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2465
    :goto_13
    iget-object v2, v0, Lcom/anythink/basead/ui/BannerAdView;->u:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :sswitch_data_0
    .sparse-switch
        -0x215ddd38 -> :sswitch_3
        0x59df59c2 -> :sswitch_2
        0x59df5a3e -> :sswitch_1
        0x60b65fb2 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x215ddd38 -> :sswitch_7
        0x59df59c2 -> :sswitch_6
        0x59df5a3e -> :sswitch_5
        0x60b65fb2 -> :sswitch_4
    .end sparse-switch
.end method

.method protected final a(Z)V
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->a:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 522
    invoke-interface {v0, p1}, Lcom/anythink/basead/f/a;->onDeeplinkCallback(Z)V

    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 3

    .line 501
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->g:Lcom/anythink/core/common/d/h;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/BannerAdView;->i()Lcom/anythink/basead/c/h;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2, v0, v1}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 503
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->a:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 504
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/anythink/basead/ui/BannerAdView;->a:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_0

    .line 515
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdClick()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 494
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->onAttachedToWindow()V

    .line 4039
    invoke-super {p0}, Lcom/anythink/basead/ui/BaseAdView;->h()V

    return-void
.end method
