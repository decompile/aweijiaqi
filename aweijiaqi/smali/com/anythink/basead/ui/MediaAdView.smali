.class public Lcom/anythink/basead/ui/MediaAdView;
.super Landroid/widget/RelativeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/basead/ui/MediaAdView$a;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field a:Lcom/anythink/core/common/d/h;

.field b:Lcom/anythink/core/common/d/j;

.field c:Lcom/anythink/basead/ui/MediaAdView$a;

.field d:Z

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lcom/anythink/basead/ui/MediaAdView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/basead/ui/MediaAdView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/j;ZLcom/anythink/basead/ui/MediaAdView$a;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    iput-object p2, p0, Lcom/anythink/basead/ui/MediaAdView;->a:Lcom/anythink/core/common/d/h;

    .line 49
    iput-object p3, p0, Lcom/anythink/basead/ui/MediaAdView;->b:Lcom/anythink/core/common/d/j;

    .line 50
    iput-boolean p4, p0, Lcom/anythink/basead/ui/MediaAdView;->d:Z

    .line 51
    iput-object p5, p0, Lcom/anythink/basead/ui/MediaAdView;->c:Lcom/anythink/basead/ui/MediaAdView$a;

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/ui/MediaAdView;)Landroid/widget/ImageView;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/basead/ui/MediaAdView;->i:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic b(Lcom/anythink/basead/ui/MediaAdView;)Landroid/widget/ImageView;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/basead/ui/MediaAdView;->h:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/basead/ui/MediaAdView;)Landroid/widget/ImageView;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/basead/ui/MediaAdView;->j:Landroid/widget/ImageView;

    return-object p0
.end method


# virtual methods
.method public getClickViews()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 160
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->b:Lcom/anythink/core/common/d/j;

    if-eqz v1, :cond_0

    .line 165
    invoke-virtual {v1}, Lcom/anythink/core/common/d/j;->m()I

    move-result v1

    if-nez v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public init(II)V
    .locals 4

    .line 57
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_media_ad_view"

    const-string v3, "layout"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 59
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "myoffer_banner_ad_title"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->e:Landroid/widget/TextView;

    .line 60
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_media_ad_cta"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->f:Landroid/widget/TextView;

    .line 61
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_media_ad_close"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->g:Landroid/widget/ImageView;

    .line 62
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_media_ad_bg_blur"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->h:Landroid/widget/ImageView;

    .line 63
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_media_ad_main_image"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->i:Landroid/widget/ImageView;

    .line 64
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "myoffer_media_ad_logo"

    invoke-static {v0, v2, v1}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/basead/ui/MediaAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->j:Landroid/widget/ImageView;

    .line 67
    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->a:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object v0

    .line 68
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->e:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    :goto_0
    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->a:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->l()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/anythink/basead/ui/MediaAdView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "myoffer_cta_learn_more"

    const-string v3, "string"

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/g/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 82
    :goto_1
    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->i:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 83
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/res/e;

    iget-object v2, p0, Lcom/anythink/basead/ui/MediaAdView;->a:Lcom/anythink/core/common/d/h;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v2, Lcom/anythink/basead/ui/MediaAdView$1;

    invoke-direct {v2, p0}, Lcom/anythink/basead/ui/MediaAdView$1;-><init>(Lcom/anythink/basead/ui/MediaAdView;)V

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;IILcom/anythink/core/common/res/b$a;)V

    .line 124
    invoke-virtual {p0}, Lcom/anythink/basead/ui/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/common/res/b;->a(Landroid/content/Context;)Lcom/anythink/core/common/res/b;

    move-result-object p1

    new-instance p2, Lcom/anythink/core/common/res/e;

    iget-object v0, p0, Lcom/anythink/basead/ui/MediaAdView;->a:Lcom/anythink/core/common/d/h;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/h;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v3, v0}, Lcom/anythink/core/common/res/e;-><init>(ILjava/lang/String;)V

    new-instance v0, Lcom/anythink/basead/ui/MediaAdView$2;

    invoke-direct {v0, p0}, Lcom/anythink/basead/ui/MediaAdView$2;-><init>(Lcom/anythink/basead/ui/MediaAdView;)V

    invoke-virtual {p1, p2, v0}, Lcom/anythink/core/common/res/b;->a(Lcom/anythink/core/common/res/e;Lcom/anythink/core/common/res/b$a;)V

    .line 138
    iget-boolean p1, p0, Lcom/anythink/basead/ui/MediaAdView;->d:Z

    if-eqz p1, :cond_2

    .line 139
    iget-object p1, p0, Lcom/anythink/basead/ui/MediaAdView;->g:Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 141
    :cond_2
    iget-object p1, p0, Lcom/anythink/basead/ui/MediaAdView;->g:Landroid/widget/ImageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    :goto_2
    iget-object p1, p0, Lcom/anythink/basead/ui/MediaAdView;->g:Landroid/widget/ImageView;

    new-instance p2, Lcom/anythink/basead/ui/MediaAdView$3;

    invoke-direct {p2, p0}, Lcom/anythink/basead/ui/MediaAdView$3;-><init>(Lcom/anythink/basead/ui/MediaAdView;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
