.class final Lcom/anythink/basead/ui/PlayerView$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/basead/ui/PlayerView;-><init>(Landroid/view/ViewGroup;Lcom/anythink/basead/ui/PlayerView$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/basead/ui/PlayerView;


# direct methods
.method constructor <init>(Lcom/anythink/basead/ui/PlayerView;Landroid/os/Looper;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .line 117
    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    iget p1, p1, Landroid/os/Message;->what:I

    invoke-static {v0, p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;I)I

    .line 119
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result p1

    if-gtz p1, :cond_0

    return-void

    .line 124
    :cond_0
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->b(Lcom/anythink/basead/ui/PlayerView;)Landroid/widget/ImageView;

    move-result-object p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->c(Lcom/anythink/basead/ui/PlayerView;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-ltz p1, :cond_1

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result p1

    int-to-long v0, p1

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->c(Lcom/anythink/basead/ui/PlayerView;)J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-ltz p1, :cond_1

    .line 125
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-virtual {p1}, Lcom/anythink/basead/ui/PlayerView;->showCloseButton()V

    .line 128
    :cond_1
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->d(Lcom/anythink/basead/ui/PlayerView;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->e(Lcom/anythink/basead/ui/PlayerView;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 129
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->f(Lcom/anythink/basead/ui/PlayerView;)Z

    .line 130
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 131
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/basead/ui/PlayerView$a;->a()V

    .line 135
    :cond_2
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 136
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {v0}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/ui/PlayerView$a;->a(I)V

    .line 139
    :cond_3
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->h(Lcom/anythink/basead/ui/PlayerView;)Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result p1

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {v0}, Lcom/anythink/basead/ui/PlayerView;->i(Lcom/anythink/basead/ui/PlayerView;)I

    move-result v0

    if-lt p1, v0, :cond_4

    .line 140
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->j(Lcom/anythink/basead/ui/PlayerView;)Z

    .line 141
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 142
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    const/16 v0, 0x19

    invoke-interface {p1, v0}, Lcom/anythink/basead/ui/PlayerView$a;->b(I)V

    goto :goto_0

    .line 144
    :cond_4
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->k(Lcom/anythink/basead/ui/PlayerView;)Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result p1

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {v0}, Lcom/anythink/basead/ui/PlayerView;->l(Lcom/anythink/basead/ui/PlayerView;)I

    move-result v0

    if-lt p1, v0, :cond_5

    .line 145
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->m(Lcom/anythink/basead/ui/PlayerView;)Z

    .line 146
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 147
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    const/16 v0, 0x32

    invoke-interface {p1, v0}, Lcom/anythink/basead/ui/PlayerView$a;->b(I)V

    goto :goto_0

    .line 149
    :cond_5
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->n(Lcom/anythink/basead/ui/PlayerView;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result p1

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {v0}, Lcom/anythink/basead/ui/PlayerView;->o(Lcom/anythink/basead/ui/PlayerView;)I

    move-result v0

    if-lt p1, v0, :cond_6

    .line 150
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->p(Lcom/anythink/basead/ui/PlayerView;)Z

    .line 151
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 152
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->g(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/PlayerView$a;

    move-result-object p1

    const/16 v0, 0x4b

    invoke-interface {p1, v0}, Lcom/anythink/basead/ui/PlayerView$a;->b(I)V

    .line 156
    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->q(Lcom/anythink/basead/ui/PlayerView;)V

    .line 157
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->r(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/CountDownView;

    move-result-object p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->r(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/CountDownView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/basead/ui/CountDownView;->isShown()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 158
    iget-object p1, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {p1}, Lcom/anythink/basead/ui/PlayerView;->r(Lcom/anythink/basead/ui/PlayerView;)Lcom/anythink/basead/ui/CountDownView;

    move-result-object p1

    iget-object v0, p0, Lcom/anythink/basead/ui/PlayerView$1;->a:Lcom/anythink/basead/ui/PlayerView;

    invoke-static {v0}, Lcom/anythink/basead/ui/PlayerView;->a(Lcom/anythink/basead/ui/PlayerView;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/anythink/basead/ui/CountDownView;->refresh(I)V

    :cond_7
    return-void
.end method
