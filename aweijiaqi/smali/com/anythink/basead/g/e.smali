.class public final Lcom/anythink/basead/g/e;
.super Lcom/anythink/basead/g/c;


# instance fields
.field a:Lcom/anythink/basead/f/a;

.field k:Lcom/anythink/basead/d/c;

.field l:Lcom/anythink/basead/a/b;

.field m:Landroid/view/View;

.field n:Z

.field o:Landroid/view/View$OnClickListener;

.field private final p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Ljava/lang/String;Z)V
    .locals 0

    .line 92
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/anythink/basead/g/c;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Ljava/lang/String;Z)V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/g/e;->p:Ljava/lang/String;

    .line 45
    new-instance p1, Lcom/anythink/basead/g/e$1;

    invoke-direct {p1, p0}, Lcom/anythink/basead/g/e$1;-><init>(Lcom/anythink/basead/g/e;)V

    iput-object p1, p0, Lcom/anythink/basead/g/e;->o:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 170
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 171
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 172
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 173
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 174
    invoke-direct {p0, v1, p2}, Lcom/anythink/basead/g/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 177
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/basead/g/e;)V
    .locals 5

    .line 1205
    iget-boolean v0, p0, Lcom/anythink/basead/g/e;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1209
    iput-boolean v0, p0, Lcom/anythink/basead/g/e;->n:Z

    .line 1210
    iget-object v0, p0, Lcom/anythink/basead/g/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/b;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/g/a/b;->a(Lcom/anythink/core/common/d/p;)V

    const/16 v0, 0x8

    .line 1211
    iget-object v1, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    new-instance v2, Lcom/anythink/basead/c/h;

    iget-object v3, p0, Lcom/anythink/basead/g/e;->d:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 1213
    iget-object p0, p0, Lcom/anythink/basead/g/e;->a:Lcom/anythink/basead/f/a;

    if-eqz p0, :cond_0

    .line 1214
    invoke-interface {p0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_0
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    .line 188
    iput-object p1, p0, Lcom/anythink/basead/g/e;->m:Landroid/view/View;

    .line 189
    new-instance v0, Lcom/anythink/basead/g/e$2;

    invoke-direct {v0, p0}, Lcom/anythink/basead/g/e$2;-><init>(Lcom/anythink/basead/g/e;)V

    .line 196
    iget-object v1, p0, Lcom/anythink/basead/g/e;->k:Lcom/anythink/basead/d/c;

    if-nez v1, :cond_0

    .line 197
    new-instance v1, Lcom/anythink/basead/d/c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/anythink/basead/d/c;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/anythink/basead/g/e;->k:Lcom/anythink/basead/d/c;

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/anythink/basead/g/e;->k:Lcom/anythink/basead/d/c;

    invoke-virtual {v1, p1, v0}, Lcom/anythink/basead/d/c;->a(Landroid/view/View;Lcom/anythink/basead/d/b;)V

    return-void
.end method

.method private static l()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private m()V
    .locals 5

    .line 205
    iget-boolean v0, p0, Lcom/anythink/basead/g/e;->n:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 209
    iput-boolean v0, p0, Lcom/anythink/basead/g/e;->n:Z

    .line 210
    iget-object v0, p0, Lcom/anythink/basead/g/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/b;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0, v1}, Lcom/anythink/basead/g/a/b;->a(Lcom/anythink/core/common/d/p;)V

    const/16 v0, 0x8

    .line 211
    iget-object v1, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    new-instance v2, Lcom/anythink/basead/c/h;

    iget-object v3, p0, Lcom/anythink/basead/g/e;->d:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/anythink/basead/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/anythink/basead/a/a;->a(ILcom/anythink/core/common/d/h;Lcom/anythink/basead/c/h;)V

    .line 213
    iget-object v0, p0, Lcom/anythink/basead/g/e;->a:Lcom/anythink/basead/f/a;

    if-eqz v0, :cond_1

    .line 214
    invoke-interface {v0}, Lcom/anythink/basead/f/a;->onAdShow()V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .line 164
    invoke-direct {p0, p1}, Lcom/anythink/basead/g/e;->b(Landroid/view/View;)V

    .line 166
    iget-object v0, p0, Lcom/anythink/basead/g/e;->o:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1, v0}, Lcom/anythink/basead/g/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Landroid/view/View;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 153
    invoke-direct {p0, p1}, Lcom/anythink/basead/g/e;->b(Landroid/view/View;)V

    if-eqz p2, :cond_1

    .line 155
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 156
    iget-object v0, p0, Lcom/anythink/basead/g/e;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    return-void

    .line 159
    :cond_1
    iget-object p2, p0, Lcom/anythink/basead/g/e;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/anythink/basead/g/e;->a:Lcom/anythink/basead/f/a;

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/anythink/basead/g/e;->g:Lcom/anythink/core/common/d/p;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public final j()V
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/anythink/basead/g/e;->k:Lcom/anythink/basead/d/c;

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0}, Lcom/anythink/basead/d/c;->a()V

    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .line 219
    invoke-virtual {p0}, Lcom/anythink/basead/g/e;->j()V

    const/4 v0, 0x0

    .line 220
    iput-object v0, p0, Lcom/anythink/basead/g/e;->a:Lcom/anythink/basead/f/a;

    .line 221
    iput-object v0, p0, Lcom/anythink/basead/g/e;->l:Lcom/anythink/basead/a/b;

    .line 222
    iput-object v0, p0, Lcom/anythink/basead/g/e;->k:Lcom/anythink/basead/d/c;

    return-void
.end method
