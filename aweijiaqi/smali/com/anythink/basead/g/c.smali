.class public abstract Lcom/anythink/basead/g/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/g/a;


# static fields
.field public static final h:Ljava/lang/String; = "extra_request_id"

.field public static final i:Ljava/lang/String; = "extra_scenario"

.field public static final j:Ljava/lang/String; = "extra_orientation"


# instance fields
.field public b:Ljava/lang/String;

.field protected c:Landroid/content/Context;

.field protected d:Lcom/anythink/core/common/d/i;

.field protected e:Ljava/lang/String;

.field protected f:Z

.field protected g:Lcom/anythink/core/common/d/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Ljava/lang/String;Z)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/g/c;->b:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    .line 41
    iput-object p3, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    .line 42
    iput-boolean p4, p0, Lcom/anythink/basead/g/c;->f:Z

    return-void
.end method

.method private b()Lcom/anythink/basead/c/f;
    .locals 4

    .line 93
    iget-object v0, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "30001"

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/anythink/basead/g/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/p;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    if-nez v0, :cond_1

    const-string v0, "No fill, offer = null!"

    .line 99
    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    return-object v0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-nez v0, :cond_2

    const-string v0, "30002"

    const-string v1, "No fill, setting = null!"

    .line 102
    invoke-static {v0, v1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    :goto_0
    const-string v0, "offerid\u3001placementid can not be null!"

    .line 94
    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/anythink/basead/f/c;)V
    .locals 5

    .line 1093
    :try_start_0
    iget-object v0, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "30001"

    if-nez v0, :cond_3

    :try_start_1
    iget-object v0, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1096
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/anythink/basead/g/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/p;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    if-nez v0, :cond_1

    const-string v0, "No fill, offer = null!"

    .line 1099
    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    goto :goto_1

    .line 1101
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-nez v0, :cond_2

    const-string v0, "30002"

    const-string v1, "No fill, setting = null!"

    .line 1102
    invoke-static {v0, v1}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const-string v0, "offerid\u3001placementid can not be null!"

    .line 1094
    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    .line 51
    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    return-void

    .line 56
    :cond_4
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v1, v1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    iget-object v3, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v3, v3, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    new-instance v4, Lcom/anythink/basead/g/c$1;

    invoke-direct {v4, p0, p1}, Lcom/anythink/basead/g/c$1;-><init>(Lcom/anythink/basead/g/c;Lcom/anythink/basead/f/c;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/anythink/basead/g/a/a;->a(Ljava/lang/String;Lcom/anythink/core/common/d/p;Lcom/anythink/core/common/d/j;Lcom/anythink/basead/a/a/a$a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 72
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 74
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-9999"

    invoke-static {v1, v0}, Lcom/anythink/basead/c/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/basead/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/anythink/basead/f/c;->onAdLoadFailed(Lcom/anythink/basead/c/f;)V

    return-void
.end method

.method public a()Z
    .locals 4

    .line 82
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/g/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    iget-object v2, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    iget-boolean v3, p0, Lcom/anythink/basead/g/c;->f:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/anythink/basead/g/a/a;->a(Lcom/anythink/core/common/d/p;Lcom/anythink/core/common/d/j;Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected final d()Z
    .locals 4

    .line 108
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/anythink/basead/g/c;->b:Ljava/lang/String;

    const-string v2, "isReady() context = null!"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v0, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/anythink/basead/g/c;->b:Ljava/lang/String;

    const-string v2, "isReady() mPlacementId = null!"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/anythink/basead/g/c;->b:Ljava/lang/String;

    const-string v2, "isReady() mOfferId = null!"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    if-nez v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/anythink/basead/g/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/basead/g/c;->d:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/basead/g/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/anythink/basead/g/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/p;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/basead/g/c;->g:Lcom/anythink/core/common/d/p;

    if-nez v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/anythink/basead/g/c;->b:Ljava/lang/String;

    const-string v2, "isReady() MyOffer no exist!"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_3
    const/4 v0, 0x1

    return v0
.end method
