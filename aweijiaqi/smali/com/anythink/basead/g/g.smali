.class public final Lcom/anythink/basead/g/g;
.super Lcom/anythink/basead/g/c;


# instance fields
.field a:Lcom/anythink/basead/f/a;

.field k:Lcom/anythink/basead/ui/SplashAdView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/anythink/basead/g/c;-><init>(Landroid/content/Context;Lcom/anythink/core/common/d/i;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .locals 2

    .line 37
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/basead/g/g$1;

    invoke-direct {v1, p0, p1}, Lcom/anythink/basead/g/g$1;-><init>(Lcom/anythink/basead/g/g;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/anythink/basead/f/a;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/anythink/basead/g/g;->a:Lcom/anythink/basead/f/a;

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final a()Z
    .locals 4

    .line 59
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/basead/g/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/anythink/basead/g/g;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/basead/g/a/a;->a(Landroid/content/Context;)Lcom/anythink/basead/g/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/basead/g/g;->g:Lcom/anythink/core/common/d/p;

    iget-object v2, p0, Lcom/anythink/basead/g/g;->d:Lcom/anythink/core/common/d/i;

    iget-object v2, v2, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    iget-boolean v3, p0, Lcom/anythink/basead/g/g;->f:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/anythink/basead/g/a/a;->a(Lcom/anythink/core/common/d/p;Lcom/anythink/core/common/d/j;Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 63
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    .line 70
    iput-object v0, p0, Lcom/anythink/basead/g/g;->a:Lcom/anythink/basead/f/a;

    .line 71
    iget-object v1, p0, Lcom/anythink/basead/g/g;->k:Lcom/anythink/basead/ui/SplashAdView;

    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {v1}, Lcom/anythink/basead/ui/SplashAdView;->destroy()V

    .line 73
    iput-object v0, p0, Lcom/anythink/basead/g/g;->k:Lcom/anythink/basead/ui/SplashAdView;

    :cond_0
    return-void
.end method
