.class public abstract Lcom/anythink/basead/d/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/d/b;


# static fields
.field private static final a:I = 0x3e8

.field private static final b:I = 0x32


# instance fields
.field private c:Z

.field private d:I

.field private e:I

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x3e8

    .line 29
    iput v0, p0, Lcom/anythink/basead/d/a;->d:I

    const/16 v0, 0x32

    .line 30
    iput v0, p0, Lcom/anythink/basead/d/a;->e:I

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/anythink/basead/d/a;->f:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final b()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/anythink/basead/d/a;->e:I

    return v0
.end method

.method public final c()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/anythink/basead/d/a;->d:I

    return v0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/anythink/basead/d/a;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/anythink/basead/d/a;->c:Z

    return v0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    .line 59
    iput-boolean v0, p0, Lcom/anythink/basead/d/a;->c:Z

    return-void
.end method
