.class public final Lcom/anythink/banner/a/e;
.super Lcom/anythink/core/common/f;


# instance fields
.field private N:Lcom/anythink/banner/api/ATBannerView;

.field a:Lcom/anythink/banner/a/d;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/anythink/banner/a/d;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/anythink/banner/a/e;->a:Lcom/anythink/banner/a/d;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/anythink/banner/a/e;->a:Lcom/anythink/banner/a/d;

    if-eqz v0, :cond_0

    .line 41
    iget-boolean v1, p0, Lcom/anythink/banner/a/e;->s:Z

    invoke-interface {v0, v1}, Lcom/anythink/banner/a/d;->onBannerLoaded(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/banner/api/ATBannerView;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/anythink/banner/a/e;->N:Lcom/anythink/banner/api/ATBannerView;

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 1

    .line 54
    instance-of v0, p1, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    if-eqz v0, :cond_0

    .line 55
    check-cast p1, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    iget-object v0, p0, Lcom/anythink/banner/a/e;->N:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {p1, v0}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->setATBannerView(Lcom/anythink/banner/api/ATBannerView;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/anythink/banner/a/e;->a:Lcom/anythink/banner/a/d;

    if-eqz v0, :cond_0

    .line 48
    iget-boolean v1, p0, Lcom/anythink/banner/a/e;->s:Z

    invoke-interface {v0, v1, p1}, Lcom/anythink/banner/a/d;->onBannerFailed(ZLcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/anythink/banner/a/e;->a:Lcom/anythink/banner/a/d;

    return-void
.end method
