.class public Lcom/anythink/banner/api/ATBannerView;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/banner/api/ATBannerView$a;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field hasCallbackShow:Z

.field hasTouchWindow:Z

.field private mAdLoadManager:Lcom/anythink/banner/a/a;

.field mCountDownTimer:Landroid/os/CountDownTimer;

.field mCustomBannerAd:Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

.field private mInnerBannerListener:Lcom/anythink/banner/a/d;

.field mIsRefresh:Z

.field private mListener:Lcom/anythink/banner/api/ATBannerListener;

.field private mPlacementId:Ljava/lang/String;

.field mRefreshRunnable:Ljava/lang/Runnable;

.field mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

.field private mScenario:Ljava/lang/String;

.field visibility:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 252
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 55
    const-class p1, Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string p1, ""

    .line 59
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    const/4 p1, 0x0

    .line 63
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    .line 64
    iput p1, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    .line 66
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasCallbackShow:Z

    .line 70
    sget-object v0, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    iput-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    .line 73
    new-instance v0, Lcom/anythink/banner/api/ATBannerView$1;

    invoke-direct {v0, p0}, Lcom/anythink/banner/api/ATBannerView$1;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 86
    new-instance v0, Lcom/anythink/banner/api/ATBannerView$2;

    invoke-direct {v0, p0}, Lcom/anythink/banner/api/ATBannerView$2;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    .line 312
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 256
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-class p1, Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string p1, ""

    .line 59
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    const/4 p1, 0x0

    .line 63
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    .line 64
    iput p1, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    .line 66
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasCallbackShow:Z

    .line 70
    sget-object p2, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    .line 73
    new-instance p2, Lcom/anythink/banner/api/ATBannerView$1;

    invoke-direct {p2, p0}, Lcom/anythink/banner/api/ATBannerView$1;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 86
    new-instance p2, Lcom/anythink/banner/api/ATBannerView$2;

    invoke-direct {p2, p0}, Lcom/anythink/banner/api/ATBannerView$2;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    .line 312
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 260
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const-class p1, Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string p1, ""

    .line 59
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    const/4 p1, 0x0

    .line 63
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    .line 64
    iput p1, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    .line 66
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasCallbackShow:Z

    .line 70
    sget-object p2, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    .line 73
    new-instance p2, Lcom/anythink/banner/api/ATBannerView$1;

    invoke-direct {p2, p0}, Lcom/anythink/banner/api/ATBannerView$1;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 86
    new-instance p2, Lcom/anythink/banner/api/ATBannerView$2;

    invoke-direct {p2, p0}, Lcom/anythink/banner/api/ATBannerView$2;-><init>(Lcom/anythink/banner/api/ATBannerView;)V

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    .line 312
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    return-void
.end method

.method static synthetic access$000(Lcom/anythink/banner/api/ATBannerView;Z)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->loadAd(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/a/a;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/anythink/banner/api/ATBannerView;Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;Z)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/banner/api/ATBannerView;->notifyBannerImpression(Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/anythink/banner/api/ATBannerView;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/anythink/banner/api/ATBannerView;)Z
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/anythink/banner/api/ATBannerView;->isInView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/api/ATBannerListener;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->mListener:Lcom/anythink/banner/api/ATBannerListener;

    return-object p0
.end method

.method static synthetic access$500(Lcom/anythink/banner/api/ATBannerView;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/a/d;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    return-object p0
.end method

.method static synthetic access$700(Lcom/anythink/banner/api/ATBannerView;Landroid/content/Context;Lcom/anythink/core/common/d/b;Z)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/banner/api/ATBannerView;->notifyBannerShow(Landroid/content/Context;Lcom/anythink/core/common/d/b;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/anythink/banner/api/ATBannerView;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/anythink/banner/api/ATBannerView;Ljava/lang/Runnable;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->startAutoRefresh(Ljava/lang/Runnable;)V

    return-void
.end method

.method private controlShow(I)V
    .locals 7

    .line 369
    iput p1, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    .line 370
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-nez v0, :cond_0

    return-void

    .line 374
    :cond_0
    monitor-enter v0

    if-nez p1, :cond_8

    .line 376
    :try_start_0
    iget-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    if-eqz p1, :cond_8

    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_2

    .line 381
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object p1

    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 384
    invoke-virtual {p1}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    instance-of v2, v2, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    if-eqz v2, :cond_2

    .line 385
    invoke-virtual {p1}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    check-cast v1, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    :cond_2
    if-nez v1, :cond_3

    .line 389
    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mCustomBannerAd:Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    if-eqz v2, :cond_4

    .line 391
    :cond_3
    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    invoke-virtual {v2}, Lcom/anythink/banner/a/a;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 392
    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v3, "first add in window to countDown refresh!"

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, v2}, Lcom/anythink/banner/api/ATBannerView;->startAutoRefresh(Ljava/lang/Runnable;)V

    .line 397
    :cond_4
    iget-boolean v2, p0, Lcom/anythink/banner/api/ATBannerView;->hasCallbackShow:Z

    if-nez v2, :cond_9

    invoke-direct {p0}, Lcom/anythink/banner/api/ATBannerView;->isInView()Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_9

    .line 399
    invoke-virtual {p1}, Lcom/anythink/core/common/d/b;->e()I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/anythink/core/common/d/b;->a(I)V

    .line 402
    invoke-virtual {v1}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->getBannerView()Landroid/view/View;

    move-result-object v2

    .line 403
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eq v4, p0, :cond_5

    .line 404
    iget-object v4, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v5, "Banner View already add in other parent!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 408
    :cond_5
    iput-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mCustomBannerAd:Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    .line 411
    invoke-virtual {v1}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v4

    .line 412
    iget-object v5, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    .line 1269
    iput-object v5, v4, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    .line 415
    new-instance v4, Lcom/anythink/banner/a/b;

    iget-object v5, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    iget-boolean v6, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    invoke-direct {v4, v5, v1, v6}, Lcom/anythink/banner/a/b;-><init>(Lcom/anythink/banner/a/d;Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;Z)V

    invoke-virtual {v1, v4}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->setAdEventListener(Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;)V

    .line 417
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v4, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    invoke-direct {p0, v1, p1, v4}, Lcom/anythink/banner/api/ATBannerView;->notifyBannerShow(Landroid/content/Context;Lcom/anythink/core/common/d/b;Z)V

    .line 419
    invoke-virtual {p0, v2}, Lcom/anythink/banner/api/ATBannerView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_6

    .line 421
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->removeAllViews()V

    .line 422
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0x11

    .line 423
    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 424
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    invoke-virtual {p0, v2, v1}, Lcom/anythink/banner/api/ATBannerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_6
    sub-int/2addr v1, v3

    :goto_0
    if-ltz v1, :cond_7

    .line 428
    invoke-virtual {p0, v1}, Lcom/anythink/banner/api/ATBannerView;->removeViewAt(I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 433
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    invoke-virtual {v1, p1}, Lcom/anythink/banner/a/a;->a(Lcom/anythink/core/common/d/b;)V

    .line 435
    iput-boolean v3, p0, Lcom/anythink/banner/api/ATBannerView;->hasCallbackShow:Z

    goto :goto_3

    .line 378
    :cond_8
    :goto_2
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v1, "no in window to stop refresh!"

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_9
    :goto_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private isInView()Z
    .locals 1

    .line 462
    iget-boolean v0, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private loadAd(Z)V
    .locals 3

    .line 316
    iput-boolean p1, p0, Lcom/anythink/banner/api/ATBannerView;->mIsRefresh:Z

    .line 317
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v1, "start to load to stop countdown refresh!"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/anythink/banner/api/ATBannerView;->stopAutoRefresh(Ljava/lang/Runnable;)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-eqz v0, :cond_1

    .line 323
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    invoke-virtual {v0, v1, p0, p1, v2}, Lcom/anythink/banner/a/a;->a(Landroid/content/Context;Lcom/anythink/banner/api/ATBannerView;ZLcom/anythink/banner/a/d;)V

    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mInnerBannerListener:Lcom/anythink/banner/a/d;

    const-string v1, ""

    const-string v2, "3001"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/anythink/banner/a/d;->onBannerFailed(ZLcom/anythink/core/api/AdError;)V

    return-void
.end method

.method private notifyBannerImpression(Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;Z)V
    .locals 8

    .line 526
    invoke-virtual {p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    .line 527
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v6

    new-instance v7, Lcom/anythink/banner/api/ATBannerView$4;

    move-object v0, v7

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/anythink/banner/api/ATBannerView$4;-><init>(Lcom/anythink/banner/api/ATBannerView;Lcom/anythink/core/common/d/d;Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;Z)V

    invoke-virtual {v6, v7}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private notifyBannerShow(Landroid/content/Context;Lcom/anythink/core/common/d/b;Z)V
    .locals 11

    .line 491
    invoke-virtual {p2}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    .line 492
    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    .line 494
    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v0

    .line 495
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/anythink/core/common/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1429
    iput-object v0, v2, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    .line 499
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    if-eqz v2, :cond_0

    .line 500
    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v6}, Lcom/anythink/core/common/g/g;->a(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/anythink/core/common/d/d;->f(Ljava/lang/String;)V

    .line 504
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v9

    new-instance v10, Lcom/anythink/banner/api/ATBannerView$3;

    move-object v0, v10

    move-object v1, p0

    move-object v3, p1

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/anythink/banner/api/ATBannerView$3;-><init>(Lcom/anythink/banner/api/ATBannerView;Lcom/anythink/core/common/d/d;Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;JLcom/anythink/core/common/d/b;Z)V

    invoke-virtual {v9, v10}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private startAutoRefresh(Ljava/lang/Runnable;)V
    .locals 5

    .line 469
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    sget-object v1, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 470
    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->stopAutoRefresh(Ljava/lang/Runnable;)V

    .line 471
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 472
    invoke-virtual {v0}, Lcom/anythink/core/c/d;->A()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 473
    sget-object v1, Lcom/anythink/banner/api/ATBannerView$a;->COUNTDOWN_ING:Lcom/anythink/banner/api/ATBannerView$a;

    iput-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    .line 474
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v0}, Lcom/anythink/core/c/d;->B()J

    move-result-wide v3

    invoke-virtual {v1, p1, v3, v4}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;J)V

    .line 478
    :cond_0
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    sget-object v0, Lcom/anythink/banner/api/ATBannerView$a;->COUNTDOWN_FINISH:Lcom/anythink/banner/api/ATBannerView$a;

    if-ne p1, v0, :cond_1

    .line 479
    invoke-direct {p0, v2}, Lcom/anythink/banner/api/ATBannerView;->loadAd(Z)V

    :cond_1
    return-void
.end method

.method private stopAutoRefresh(Ljava/lang/Runnable;)V
    .locals 1

    .line 485
    sget-object v0, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    iput-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshStatus:Lcom/anythink/banner/api/ATBannerView$a;

    .line 486
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public checkAdStatus()Lcom/anythink/core/api/ATAdStatusInfo;
    .locals 6

    .line 275
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 276
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 277
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-nez v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v3, "PlacementId is empty!"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    new-instance v0, Lcom/anythink/core/api/ATAdStatusInfo;

    invoke-direct {v0, v2, v2, v1}, Lcom/anythink/core/api/ATAdStatusInfo;-><init>(ZZLcom/anythink/core/api/ATAdInfo;)V

    return-object v0

    .line 287
    :cond_1
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/banner/a/a;->b(Landroid/content/Context;)Lcom/anythink/core/api/ATAdStatusInfo;

    move-result-object v0

    .line 288
    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->i:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->r:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-static {v1, v2, v3, v4, v5}, Lcom/anythink/core/api/ATSDK;->apiLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 278
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v3, "SDK init error!"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    new-instance v0, Lcom/anythink/core/api/ATAdStatusInfo;

    invoke-direct {v0, v2, v2, v1}, Lcom/anythink/core/api/ATAdStatusInfo;-><init>(ZZLcom/anythink/core/api/ATAdInfo;)V

    return-object v0
.end method

.method public destroy()V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mCustomBannerAd:Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->destory()V

    :cond_0
    return-void
.end method

.method public loadAd()V
    .locals 5

    .line 307
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    sget-object v1, Lcom/anythink/core/common/b/e$e;->i:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->n:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->h:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v0, v1, v2, v3, v4}, Lcom/anythink/core/api/ATSDK;->apiLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 308
    invoke-direct {p0, v0}, Lcom/anythink/banner/api/ATBannerView;->loadAd(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 345
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const/4 v0, 0x1

    .line 346
    iput-boolean v0, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 351
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    const/4 v0, 0x0

    .line 352
    iput-boolean v0, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 445
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 447
    iget v0, p0, Lcom/anythink/banner/api/ATBannerView;->visibility:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/anythink/banner/api/ATBannerView;->hasTouchWindow:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 454
    :cond_0
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/anythink/banner/a/a;->c()Z

    move-result p1

    if-nez p1, :cond_2

    .line 455
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v0, "onWindowFocusChanged first add in window to countDown refresh!"

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->startAutoRefresh(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 448
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    if-eqz p1, :cond_2

    .line 449
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v0, "onWindowFocusChanged no in window to stop refresh!"

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0

    .line 364
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 365
    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->controlShow(I)V

    return-void
.end method

.method public setBannerAdListener(Lcom/anythink/banner/api/ATBannerListener;)V
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mListener:Lcom/anythink/banner/api/ATBannerListener;

    return-void
.end method

.method public setLocalExtra(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->TAG:Ljava/lang/String;

    const-string v0, "You must set unit Id first."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 303
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public setPlacementId(Ljava/lang/String;)V
    .locals 1

    .line 264
    invoke-virtual {p0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/anythink/banner/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/banner/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/banner/api/ATBannerView;->mAdLoadManager:Lcom/anythink/banner/a/a;

    .line 265
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mPlacementId:Ljava/lang/String;

    return-void
.end method

.method public setScenario(Ljava/lang/String;)V
    .locals 1

    .line 269
    invoke-static {p1}, Lcom/anythink/core/common/g/g;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView;->mScenario:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .line 358
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 359
    invoke-direct {p0, p1}, Lcom/anythink/banner/api/ATBannerView;->controlShow(I)V

    return-void
.end method
