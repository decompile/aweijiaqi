.class final Lcom/anythink/banner/api/ATBannerView$4$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/banner/api/ATBannerView$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/banner/api/ATBannerView$4;


# direct methods
.method constructor <init>(Lcom/anythink/banner/api/ATBannerView$4;)V
    .locals 0

    .line 535
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v0, v0, Lcom/anythink/banner/api/ATBannerView$4;->this$0:Lcom/anythink/banner/api/ATBannerView;

    invoke-static {v0}, Lcom/anythink/banner/api/ATBannerView;->access$400(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/api/ATBannerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v0, v0, Lcom/anythink/banner/api/ATBannerView$4;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-boolean v0, v0, Lcom/anythink/banner/api/ATBannerView$4;->val$isRefresh:Z

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v0, v0, Lcom/anythink/banner/api/ATBannerView$4;->this$0:Lcom/anythink/banner/api/ATBannerView;

    invoke-static {v0}, Lcom/anythink/banner/api/ATBannerView;->access$400(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/api/ATBannerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v1, v1, Lcom/anythink/banner/api/ATBannerView$4;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/anythink/banner/api/ATBannerListener;->onBannerAutoRefreshed(Lcom/anythink/core/api/ATAdInfo;)V

    return-void

    .line 542
    :cond_0
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v0, v0, Lcom/anythink/banner/api/ATBannerView$4;->this$0:Lcom/anythink/banner/api/ATBannerView;

    invoke-static {v0}, Lcom/anythink/banner/api/ATBannerView;->access$400(Lcom/anythink/banner/api/ATBannerView;)Lcom/anythink/banner/api/ATBannerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView$4$1;->this$1:Lcom/anythink/banner/api/ATBannerView$4;

    iget-object v1, v1, Lcom/anythink/banner/api/ATBannerView$4;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/anythink/banner/api/ATBannerListener;->onBannerShow(Lcom/anythink/core/api/ATAdInfo;)V

    :cond_1
    return-void
.end method
