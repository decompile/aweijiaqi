.class final Lcom/anythink/banner/api/ATBannerView$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/banner/api/ATBannerView;->notifyBannerShow(Landroid/content/Context;Lcom/anythink/core/common/d/b;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/banner/api/ATBannerView;

.field final synthetic val$adCacheInfo:Lcom/anythink/core/common/d/b;

.field final synthetic val$adTrackingInfo:Lcom/anythink/core/common/d/d;

.field final synthetic val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$isRefresh:Z

.field final synthetic val$timestamp:J


# direct methods
.method constructor <init>(Lcom/anythink/banner/api/ATBannerView;Lcom/anythink/core/common/d/d;Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;JLcom/anythink/core/common/d/b;Z)V
    .locals 0

    .line 504
    iput-object p1, p0, Lcom/anythink/banner/api/ATBannerView$3;->this$0:Lcom/anythink/banner/api/ATBannerView;

    iput-object p2, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adTrackingInfo:Lcom/anythink/core/common/d/d;

    iput-object p3, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    iput-wide p5, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$timestamp:J

    iput-object p7, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adCacheInfo:Lcom/anythink/core/common/d/b;

    iput-boolean p8, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$isRefresh:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .line 507
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adTrackingInfo:Lcom/anythink/core/common/d/d;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->this$0:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adTrackingInfo:Lcom/anythink/core/common/d/d;

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/n;->a(Landroid/content/Context;Lcom/anythink/core/common/d/d;)V

    .line 513
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adTrackingInfo:Lcom/anythink/core/common/d/d;

    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    iget-wide v5, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$timestamp:J

    invoke-virtual/range {v1 .. v6}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V

    .line 515
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$adCacheInfo:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Lcom/anythink/core/common/d/b;)V

    .line 517
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->supportImpressionCallback()Z

    move-result v0

    if-nez v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/anythink/banner/api/ATBannerView$3;->this$0:Lcom/anythink/banner/api/ATBannerView;

    iget-object v1, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$baseAdAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    iget-boolean v3, p0, Lcom/anythink/banner/api/ATBannerView$3;->val$isRefresh:Z

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/banner/api/ATBannerView;->access$1000(Lcom/anythink/banner/api/ATBannerView;Landroid/content/Context;Lcom/anythink/core/api/ATBaseAdAdapter;Z)V

    :cond_0
    return-void
.end method
