.class final enum Lcom/anythink/banner/api/ATBannerView$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/banner/api/ATBannerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/anythink/banner/api/ATBannerView$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/anythink/banner/api/ATBannerView$a;

.field public static final enum COUNTDOWN_FINISH:Lcom/anythink/banner/api/ATBannerView$a;

.field public static final enum COUNTDOWN_ING:Lcom/anythink/banner/api/ATBannerView$a;

.field public static final enum NORMAL:Lcom/anythink/banner/api/ATBannerView$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 50
    new-instance v0, Lcom/anythink/banner/api/ATBannerView$a;

    const/4 v1, 0x0

    const-string v2, "NORMAL"

    invoke-direct {v0, v2, v1}, Lcom/anythink/banner/api/ATBannerView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    .line 51
    new-instance v0, Lcom/anythink/banner/api/ATBannerView$a;

    const/4 v2, 0x1

    const-string v3, "COUNTDOWN_ING"

    invoke-direct {v0, v3, v2}, Lcom/anythink/banner/api/ATBannerView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/anythink/banner/api/ATBannerView$a;->COUNTDOWN_ING:Lcom/anythink/banner/api/ATBannerView$a;

    .line 52
    new-instance v0, Lcom/anythink/banner/api/ATBannerView$a;

    const/4 v3, 0x2

    const-string v4, "COUNTDOWN_FINISH"

    invoke-direct {v0, v4, v3}, Lcom/anythink/banner/api/ATBannerView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/anythink/banner/api/ATBannerView$a;->COUNTDOWN_FINISH:Lcom/anythink/banner/api/ATBannerView$a;

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/anythink/banner/api/ATBannerView$a;

    .line 49
    sget-object v5, Lcom/anythink/banner/api/ATBannerView$a;->NORMAL:Lcom/anythink/banner/api/ATBannerView$a;

    aput-object v5, v4, v1

    sget-object v1, Lcom/anythink/banner/api/ATBannerView$a;->COUNTDOWN_ING:Lcom/anythink/banner/api/ATBannerView$a;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    sput-object v4, Lcom/anythink/banner/api/ATBannerView$a;->$VALUES:[Lcom/anythink/banner/api/ATBannerView$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/anythink/banner/api/ATBannerView$a;
    .locals 1

    .line 49
    const-class v0, Lcom/anythink/banner/api/ATBannerView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/anythink/banner/api/ATBannerView$a;

    return-object p0
.end method

.method public static values()[Lcom/anythink/banner/api/ATBannerView$a;
    .locals 1

    .line 49
    sget-object v0, Lcom/anythink/banner/api/ATBannerView$a;->$VALUES:[Lcom/anythink/banner/api/ATBannerView$a;

    invoke-virtual {v0}, [Lcom/anythink/banner/api/ATBannerView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/anythink/banner/api/ATBannerView$a;

    return-object v0
.end method
