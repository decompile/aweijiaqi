.class public abstract Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;
.super Lcom/anythink/core/api/ATBaseAdAdapter;


# instance fields
.field protected mATBannerView:Lcom/anythink/banner/api/ATBannerView;

.field protected mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getBannerView()Landroid/view/View;
.end method

.method public final isAdReady()Z
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->getBannerView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final releaseLoadResource()V
    .locals 1

    .line 36
    invoke-super {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->releaseLoadResource()V

    const/4 v0, 0x0

    .line 37
    iput-object v0, p0, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-void
.end method

.method public final setATBannerView(Lcom/anythink/banner/api/ATBannerView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-void
.end method

.method public setAdEventListener(Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-void
.end method
