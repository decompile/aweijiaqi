.class public final Lcom/anythink/sdk/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final anythink_gdpr_btn_area:I = 0x7f08001d

.field public static final anythink_myoffer_banner_ad_install_btn:I = 0x7f08001e

.field public static final anythink_myoffer_banner_ad_text:I = 0x7f08001f

.field public static final anythink_myoffer_banner_ad_title:I = 0x7f080020

.field public static final anythink_myoffer_banner_close:I = 0x7f080021

.field public static final anythink_myoffer_banner_desc:I = 0x7f080022

.field public static final anythink_myoffer_banner_icon:I = 0x7f080023

.field public static final anythink_myoffer_banner_main_image:I = 0x7f080024

.field public static final anythink_myoffer_banner_root:I = 0x7f080025

.field public static final anythink_myoffer_banner_self_ad_logo:I = 0x7f080026

.field public static final anythink_myoffer_banner_view_id:I = 0x7f080027

.field public static final anythink_myoffer_bg_blur_id:I = 0x7f080028

.field public static final anythink_myoffer_btn_banner_cta:I = 0x7f080029

.field public static final anythink_myoffer_btn_close_id:I = 0x7f08002a

.field public static final anythink_myoffer_btn_mute_id:I = 0x7f08002b

.field public static final anythink_myoffer_count_down_view_id:I = 0x7f08002c

.field public static final anythink_myoffer_end_card_id:I = 0x7f08002d

.field public static final anythink_myoffer_feedback_et:I = 0x7f08002e

.field public static final anythink_myoffer_feedback_iv_close:I = 0x7f08002f

.field public static final anythink_myoffer_feedback_ll_abnormal:I = 0x7f080030

.field public static final anythink_myoffer_feedback_ll_report_ad_1:I = 0x7f080031

.field public static final anythink_myoffer_feedback_ll_report_ad_2:I = 0x7f080032

.field public static final anythink_myoffer_feedback_tv_1:I = 0x7f080033

.field public static final anythink_myoffer_feedback_tv_2:I = 0x7f080034

.field public static final anythink_myoffer_feedback_tv_3:I = 0x7f080035

.field public static final anythink_myoffer_feedback_tv_4:I = 0x7f080036

.field public static final anythink_myoffer_feedback_tv_5:I = 0x7f080037

.field public static final anythink_myoffer_feedback_tv_6:I = 0x7f080038

.field public static final anythink_myoffer_feedback_tv_7:I = 0x7f080039

.field public static final anythink_myoffer_feedback_tv_8:I = 0x7f08003a

.field public static final anythink_myoffer_feedback_tv_9:I = 0x7f08003b

.field public static final anythink_myoffer_feedback_tv_abnormal:I = 0x7f08003c

.field public static final anythink_myoffer_feedback_tv_commit:I = 0x7f08003d

.field public static final anythink_myoffer_feedback_tv_other_suggestion:I = 0x7f08003e

.field public static final anythink_myoffer_feedback_tv_report_ad:I = 0x7f08003f

.field public static final anythink_myoffer_full_screen_view_id:I = 0x7f080040

.field public static final anythink_myoffer_iv_banner_icon:I = 0x7f080041

.field public static final anythink_myoffer_iv_logo:I = 0x7f080042

.field public static final anythink_myoffer_loading_id:I = 0x7f080043

.field public static final anythink_myoffer_main_image_id:I = 0x7f080044

.field public static final anythink_myoffer_media_ad_bg_blur:I = 0x7f080045

.field public static final anythink_myoffer_media_ad_close:I = 0x7f080046

.field public static final anythink_myoffer_media_ad_cta:I = 0x7f080047

.field public static final anythink_myoffer_media_ad_logo:I = 0x7f080048

.field public static final anythink_myoffer_media_ad_main_image:I = 0x7f080049

.field public static final anythink_myoffer_player_view_id:I = 0x7f08004a

.field public static final anythink_myoffer_rating_view:I = 0x7f08004b

.field public static final anythink_myoffer_rl_root:I = 0x7f08004c

.field public static final anythink_myoffer_splash_ad_content_image_area:I = 0x7f08004d

.field public static final anythink_myoffer_splash_ad_install_btn:I = 0x7f08004e

.field public static final anythink_myoffer_splash_ad_logo:I = 0x7f08004f

.field public static final anythink_myoffer_splash_ad_title:I = 0x7f080050

.field public static final anythink_myoffer_splash_bg:I = 0x7f080051

.field public static final anythink_myoffer_splash_bottom_area:I = 0x7f080052

.field public static final anythink_myoffer_splash_desc:I = 0x7f080053

.field public static final anythink_myoffer_splash_icon:I = 0x7f080054

.field public static final anythink_myoffer_splash_root:I = 0x7f080055

.field public static final anythink_myoffer_splash_self_ad_logo:I = 0x7f080056

.field public static final anythink_myoffer_splash_skip:I = 0x7f080057

.field public static final anythink_myoffer_tv_banner_desc:I = 0x7f080058

.field public static final anythink_myoffer_tv_banner_title:I = 0x7f080059

.field public static final anythink_policy_agree_view:I = 0x7f08007a

.field public static final anythink_policy_content_view:I = 0x7f08007b

.field public static final anythink_policy_loading_view:I = 0x7f08007c

.field public static final anythink_policy_reject_view:I = 0x7f08007d

.field public static final anythink_policy_webview_area:I = 0x7f08007e

.field public static final anythink_tips:I = 0x7f08007f

.field public static final anythink_tips_area:I = 0x7f080080


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
