.class public final Lcom/anythink/sdk/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final anythink_browser_close_icon:I = 0x7f07005a

.field public static final anythink_browser_left_icon:I = 0x7f07005b

.field public static final anythink_browser_refresh_icon:I = 0x7f07005c

.field public static final anythink_browser_right_icon:I = 0x7f07005d

.field public static final anythink_browser_unleft_icon:I = 0x7f07005e

.field public static final anythink_browser_unright_icon:I = 0x7f07005f

.field public static final anythink_core_icon_close:I = 0x7f070063

.field public static final anythink_core_loading:I = 0x7f070064

.field public static final anythink_myoffer_bg_banner:I = 0x7f070065

.field public static final anythink_myoffer_bg_banner_ad_choice:I = 0x7f070066

.field public static final anythink_myoffer_bg_bottom_banner:I = 0x7f070067

.field public static final anythink_myoffer_bg_btn_cta:I = 0x7f070068

.field public static final anythink_myoffer_bg_btn_cta_banner:I = 0x7f070069

.field public static final anythink_myoffer_bg_feedback_button:I = 0x7f07006a

.field public static final anythink_myoffer_bg_feedback_button_normal:I = 0x7f07006b

.field public static final anythink_myoffer_bg_feedback_button_pressed:I = 0x7f07006c

.field public static final anythink_myoffer_bg_feedback_dialog:I = 0x7f07006d

.field public static final anythink_myoffer_bg_feedback_submit:I = 0x7f07006e

.field public static final anythink_myoffer_bg_feedback_submit_normal:I = 0x7f07006f

.field public static final anythink_myoffer_bg_feedback_submit_pressed:I = 0x7f070070

.field public static final anythink_myoffer_bg_feedback_textview:I = 0x7f070071

.field public static final anythink_myoffer_bg_feedback_textview_color:I = 0x7f070072

.field public static final anythink_myoffer_bg_feedback_textview_normal:I = 0x7f070073

.field public static final anythink_myoffer_bg_feedback_textview_pressed:I = 0x7f070074

.field public static final anythink_myoffer_btn_close:I = 0x7f070075

.field public static final anythink_myoffer_btn_close_pressed:I = 0x7f070076

.field public static final anythink_myoffer_feedback_dialog_close:I = 0x7f070077

.field public static final anythink_myoffer_loading:I = 0x7f070078

.field public static final anythink_myoffer_splash_ad_bg:I = 0x7f070079

.field public static final anythink_myoffer_splash_btn:I = 0x7f07007a

.field public static final anythink_myoffer_splash_land_bottom_bg:I = 0x7f07007b

.field public static final anythink_myoffer_splash_skip_bg:I = 0x7f07007c

.field public static final anythink_myoffer_splash_star:I = 0x7f07007d

.field public static final anythink_myoffer_splash_star_gray:I = 0x7f07007e

.field public static final anythink_myoffer_video_close:I = 0x7f07007f

.field public static final anythink_myoffer_video_mute:I = 0x7f070080

.field public static final anythink_myoffer_video_no_mute:I = 0x7f070081


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
