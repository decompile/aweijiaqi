.class public final Lcom/anythink/china/common/service/ApkDownloadService$a;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/anythink/china/common/service/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/china/common/service/ApkDownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/china/common/service/ApkDownloadService;


# direct methods
.method public constructor <init>(Lcom/anythink/china/common/service/ApkDownloadService;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-static {v0}, Lcom/anythink/china/common/service/ApkDownloadService;->a(Lcom/anythink/china/common/service/ApkDownloadService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/china/common/a/d;

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {v0}, Lcom/anythink/china/common/a/d;->b()V

    .line 40
    iget-object v0, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-static {v0}, Lcom/anythink/china/common/service/ApkDownloadService;->a(Lcom/anythink/china/common/service/ApkDownloadService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-static {v0}, Lcom/anythink/china/common/service/ApkDownloadService;->a(Lcom/anythink/china/common/service/ApkDownloadService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-static {v0}, Lcom/anythink/china/common/service/ApkDownloadService;->a(Lcom/anythink/china/common/service/ApkDownloadService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/china/common/a/d;

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0}, Lcom/anythink/china/common/a/d;->a()V

    .line 49
    iget-object v0, p0, Lcom/anythink/china/common/service/ApkDownloadService$a;->a:Lcom/anythink/china/common/service/ApkDownloadService;

    invoke-static {v0}, Lcom/anythink/china/common/service/ApkDownloadService;->a(Lcom/anythink/china/common/service/ApkDownloadService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
