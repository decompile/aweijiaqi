.class Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;
.super Ljava/lang/Object;
.source "SDKInitHelper.java"

# interfaces
.implements Lcom/anythink/core/api/NetTrafficeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->checkIsEuTraffic(Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

.field final synthetic val$callbackListener:Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;->val$callbackListener:Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorCallback(Ljava/lang/String;)V
    .locals 2

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "check EU error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;->val$callbackListener:Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;

    if-eqz v0, :cond_0

    .line 165
    invoke-interface {v0, p1}, Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;->onErrorCallback(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onResultCallback(Z)V
    .locals 2

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "check EU:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;->val$callbackListener:Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;

    if-eqz v0, :cond_0

    .line 157
    invoke-interface {v0, p1}, Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;->onResultCallback(Z)V

    :cond_0
    return-void
.end method
