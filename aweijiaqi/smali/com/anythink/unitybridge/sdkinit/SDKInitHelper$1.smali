.class Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;
.super Ljava/lang/Object;
.source "SDKInitHelper.java"

# interfaces
.implements Lcom/anythink/core/api/ATSDKInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->initAppliction(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

.field final synthetic val$appid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;Ljava/lang/String;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->val$appid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFail(Ljava/lang/String;)V
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    iget-object v1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->val$appid:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init faild ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "] "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/unitybridge/sdkinit/SDKInitListener;->initSDKError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init--> done :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->val$appid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->this$0:Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    iget-object v1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;->val$appid:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/anythink/unitybridge/sdkinit/SDKInitListener;->initSDKSuccess(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
