.class public Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;
.super Ljava/lang/Object;
.source "SDKInitHelper.java"


# static fields
.field private static final SP_KEY_FIRST_RUN:Ljava/lang/String; = "SP_KEY_FIRST_RUN"

.field public static final TAG:Ljava/lang/String; = "AT_android_unity3d"


# instance fields
.field mActivity:Landroid/app/Activity;

.field mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;


# direct methods
.method public constructor <init>(Lcom/anythink/unitybridge/sdkinit/SDKInitListener;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string v0, "pSDKInitListener == null .."

    .line 27
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    const-string p1, "SDKInitHelper"

    .line 30
    invoke-static {p1}, Lcom/anythink/unitybridge/UnityPluginUtils;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public addNetworkGDPRInfo(ILjava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public checkIsEuTraffic(Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;)V
    .locals 2

    const-string v0, "checkIsEuTraffic"

    .line 151
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$2;-><init>(Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;Lcom/anythink/unitybridge/sdkinit/SDKEUCallbackListener;)V

    invoke-static {v0, v1}, Lcom/anythink/core/api/ATSDK;->checkIsEuTraffic(Landroid/content/Context;Lcom/anythink/core/api/NetTrafficeCallback;)V

    return-void
.end method

.method public deniedUploadDeviceInfo(Ljava/lang/String;)V
    .locals 2

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deniedUploadDeviceInfo "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 208
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ","

    .line 209
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 210
    invoke-static {p1}, Lcom/anythink/core/api/ATSDK;->deniedUploadDeviceInfo([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getGDPRLevel()I
    .locals 1

    const-string v0, "getGDPRLevel"

    .line 134
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/anythink/core/api/ATSDK;->getGDPRDataLevel(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public initAppliction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initAppliction--> appid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_1

    const-string p2, "initAppliction--> sActivity == null"

    .line 37
    invoke-static {p2}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 38
    iget-object p2, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mSDKInitListener:Lcom/anythink/unitybridge/sdkinit/SDKInitListener;

    if-eqz p2, :cond_0

    const-string v0, "activity can not be null "

    .line 39
    invoke-interface {p2, p1, v0}, Lcom/anythink/unitybridge/sdkinit/SDKInitListener;->initSDKError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    .line 61
    :cond_1
    new-instance v1, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$1;-><init>(Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;Ljava/lang/String;)V

    invoke-static {v0, p1, p2, v1}, Lcom/anythink/core/api/ATSDK;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/api/ATSDKInitListener;)V

    return-void
.end method

.method public initCustomMap(Ljava/lang/String;)V
    .locals 4

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initCustomMap--> :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 98
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 100
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object p1

    .line 103
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 105
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 108
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    :cond_1
    invoke-static {v0}, Lcom/anythink/core/api/ATSDK;->initCustomMap(Ljava/util/Map;)V

    return-void
.end method

.method public initPlacementCustomMap(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initPlacementCustomMap-->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p2

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 117
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object p2

    .line 122
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 124
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p2

    .line 127
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    .line 130
    :cond_1
    invoke-static {p1, v0}, Lcom/anythink/core/api/ATSDK;->initPlacementCustomMap(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public isEUTraffic()Z
    .locals 1

    const-string v0, "isEUTraffic"

    .line 139
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/anythink/core/api/ATSDK;->isEUTraffic(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public setChannel(Ljava/lang/String;)V
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setChannel--> :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 87
    invoke-static {p1}, Lcom/anythink/core/api/ATSDK;->setChannel(Ljava/lang/String;)V

    return-void
.end method

.method public setDebugLogOpen(Z)V
    .locals 2

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setDebugLogOpen--> :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 82
    invoke-static {p1}, Lcom/anythink/core/api/ATSDK;->setNetworkLogDebug(Z)V

    return-void
.end method

.method public setGDPRLevel(I)V
    .locals 2

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setGDPRLevel--> level:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/anythink/core/api/ATSDK;->setGDPRUploadDataLevel(Landroid/content/Context;I)V

    return-void
.end method

.method public setSubChannel(Ljava/lang/String;)V
    .locals 2

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setSubChannel--> :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 92
    invoke-static {p1}, Lcom/anythink/core/api/ATSDK;->setSubChannel(Ljava/lang/String;)V

    return-void
.end method

.method public showGDPRAuth()V
    .locals 2

    const-string v0, "showGDPRAuth "

    .line 172
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$3;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/sdkinit/SDKInitHelper$3;-><init>(Lcom/anythink/unitybridge/sdkinit/SDKInitHelper;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
