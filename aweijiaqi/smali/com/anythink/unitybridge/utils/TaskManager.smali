.class public Lcom/anythink/unitybridge/utils/TaskManager;
.super Ljava/lang/Object;
.source "TaskManager.java"


# static fields
.field private static sSelf:Lcom/anythink/unitybridge/utils/TaskManager;


# instance fields
.field private mAPKDownPool:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mSinglePool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mSinglePool:Ljava/util/concurrent/ExecutorService;

    .line 16
    iput-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mAPKDownPool:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 19
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mSinglePool:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static getInstance()Lcom/anythink/unitybridge/utils/TaskManager;
    .locals 1

    .line 23
    sget-object v0, Lcom/anythink/unitybridge/utils/TaskManager;->sSelf:Lcom/anythink/unitybridge/utils/TaskManager;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/anythink/unitybridge/utils/TaskManager;

    invoke-direct {v0}, Lcom/anythink/unitybridge/utils/TaskManager;-><init>()V

    sput-object v0, Lcom/anythink/unitybridge/utils/TaskManager;->sSelf:Lcom/anythink/unitybridge/utils/TaskManager;

    .line 26
    :cond_0
    sget-object v0, Lcom/anythink/unitybridge/utils/TaskManager;->sSelf:Lcom/anythink/unitybridge/utils/TaskManager;

    return-object v0
.end method

.method private run(Lcom/anythink/unitybridge/utils/Worker;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mSinglePool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public release()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mSinglePool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 61
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager;->mAPKDownPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    return-void
.end method

.method public run_proxy(Ljava/lang/Runnable;)V
    .locals 2

    const-wide/16 v0, 0x0

    .line 34
    invoke-virtual {p0, p1, v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxyDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public run_proxyDelayed(Ljava/lang/Runnable;J)V
    .locals 3

    if-eqz p1, :cond_0

    .line 39
    new-instance v0, Lcom/anythink/unitybridge/utils/TaskManager$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/anythink/unitybridge/utils/TaskManager$1;-><init>(Lcom/anythink/unitybridge/utils/TaskManager;JLjava/lang/Runnable;)V

    .line 54
    new-instance p1, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    const-wide/16 v1, 0x3e8

    div-long/2addr p2, v1

    invoke-direct {p1, p2, p3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/anythink/unitybridge/utils/Worker;->setID(I)V

    .line 55
    invoke-direct {p0, v0}, Lcom/anythink/unitybridge/utils/TaskManager;->run(Lcom/anythink/unitybridge/utils/Worker;)V

    :cond_0
    return-void
.end method
