.class public abstract Lcom/anythink/unitybridge/utils/Worker;
.super Ljava/lang/Object;
.source "Worker.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final TYPE_NORMAL:I = 0x1

.field public static final TYPE_PHOTO:I = 0x2

.field public static final TYPE_PRECLICK:I = 0x3


# instance fields
.field protected mRunning:Z

.field protected mType:I

.field private mWorkID:I

.field protected mWorkerStatus:Lcom/anythink/unitybridge/utils/WorkerListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mRunning:Z

    .line 12
    iput v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mType:I

    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkID:I

    return-void
.end method


# virtual methods
.method public getID()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkID:I

    return v0
.end method

.method public run()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkerStatus:Lcom/anythink/unitybridge/utils/WorkerListener;

    if-eqz v0, :cond_0

    .line 31
    invoke-interface {v0, p0}, Lcom/anythink/unitybridge/utils/WorkerListener;->onWorkStart(Lcom/anythink/unitybridge/utils/Worker;)V

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/unitybridge/utils/Worker;->work()V

    .line 36
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkerStatus:Lcom/anythink/unitybridge/utils/WorkerListener;

    if-eqz v0, :cond_1

    .line 37
    invoke-interface {v0, p0}, Lcom/anythink/unitybridge/utils/WorkerListener;->onWorkFinished(Lcom/anythink/unitybridge/utils/Worker;)V

    :cond_1
    return-void
.end method

.method setID(I)V
    .locals 0

    .line 16
    iput p1, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkID:I

    return-void
.end method

.method public setStatusListener(Lcom/anythink/unitybridge/utils/WorkerListener;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/anythink/unitybridge/utils/Worker;->mWorkerStatus:Lcom/anythink/unitybridge/utils/WorkerListener;

    return-void
.end method

.method public abstract work()V
.end method
