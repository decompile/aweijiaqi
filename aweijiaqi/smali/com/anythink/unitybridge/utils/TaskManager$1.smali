.class Lcom/anythink/unitybridge/utils/TaskManager$1;
.super Lcom/anythink/unitybridge/utils/Worker;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/utils/TaskManager;->run_proxyDelayed(Ljava/lang/Runnable;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/utils/TaskManager;

.field final synthetic val$delayed:J

.field final synthetic val$runnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/utils/TaskManager;JLjava/lang/Runnable;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/anythink/unitybridge/utils/TaskManager$1;->this$0:Lcom/anythink/unitybridge/utils/TaskManager;

    iput-wide p2, p0, Lcom/anythink/unitybridge/utils/TaskManager$1;->val$delayed:J

    iput-object p4, p0, Lcom/anythink/unitybridge/utils/TaskManager$1;->val$runnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/anythink/unitybridge/utils/Worker;-><init>()V

    return-void
.end method


# virtual methods
.method public work()V
    .locals 2

    .line 43
    :try_start_0
    iget-wide v0, p0, Lcom/anythink/unitybridge/utils/TaskManager$1;->val$delayed:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 46
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 49
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "thread-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/anythink/unitybridge/utils/TaskManager$1;->getID()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "t"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/anythink/unitybridge/utils/TaskManager$1;->val$runnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method
