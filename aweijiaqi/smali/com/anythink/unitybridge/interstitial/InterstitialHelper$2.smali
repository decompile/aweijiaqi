.class Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;
.super Ljava/lang/Object;
.source "InterstitialHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->loadInterstitialAd(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/interstitial/InterstitialHelper;)V
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->mInterstitialAd:Lcom/anythink/interstitial/api/ATInterstitial;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->mInterstitialAd:Lcom/anythink/interstitial/api/ATInterstitial;

    invoke-virtual {v0}, Lcom/anythink/interstitial/api/ATInterstitial;->load()V

    goto :goto_0

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadInterstitialAd error, you must call initInterstitial first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AT_android_unity3d"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;-><init>(Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
