.class public interface abstract Lcom/anythink/unitybridge/interstitial/InterstitialListener;
.super Ljava/lang/Object;
.source "InterstitialListener.java"


# virtual methods
.method public abstract onInterstitialAdClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdClose(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdLoadFail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdLoaded(Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdShow(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdVideoEnd(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdVideoError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdVideoStart(Ljava/lang/String;Ljava/lang/String;)V
.end method
