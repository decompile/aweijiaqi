.class Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;
.super Ljava/lang/Object;
.source "InterstitialHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;)V
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;->this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 212
    iget-object v0, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;->this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;

    iget-object v0, v0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->mListener:Lcom/anythink/unitybridge/interstitial/InterstitialListener;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;->this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;

    iget-object v0, v0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    monitor-enter v0

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;->this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;

    iget-object v1, v1, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->mListener:Lcom/anythink/unitybridge/interstitial/InterstitialListener;

    iget-object v2, p0, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2$1;->this$1:Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;

    iget-object v2, v2, Lcom/anythink/unitybridge/interstitial/InterstitialHelper$2;->this$0:Lcom/anythink/unitybridge/interstitial/InterstitialHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/interstitial/InterstitialHelper;->mPlacementId:Ljava/lang/String;

    const-string v3, "-1"

    const-string v4, "you must call initInterstitial first .."

    invoke-interface {v1, v2, v3, v4}, Lcom/anythink/unitybridge/interstitial/InterstitialListener;->onInterstitialAdLoadFail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method
