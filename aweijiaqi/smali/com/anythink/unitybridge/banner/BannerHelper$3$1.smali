.class Lcom/anythink/unitybridge/banner/BannerHelper$3$1;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper$3;)V
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$3$1;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 248
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$3$1;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper$3;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mListener:Lcom/anythink/unitybridge/banner/BannerListener;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$3$1;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper$3;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    monitor-enter v0

    .line 250
    :try_start_0
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$3$1;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper$3;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mListener:Lcom/anythink/unitybridge/banner/BannerListener;

    iget-object v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$3$1;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$3;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper$3;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    const-string v3, "-1"

    const-string v4, "you must call initBanner first"

    invoke-interface {v1, v2, v3, v4}, Lcom/anythink/unitybridge/banner/BannerListener;->onBannerFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method
