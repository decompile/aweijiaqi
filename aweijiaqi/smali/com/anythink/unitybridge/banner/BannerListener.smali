.class public interface abstract Lcom/anythink/unitybridge/banner/BannerListener;
.super Ljava/lang/Object;
.source "BannerListener.java"


# virtual methods
.method public abstract onBannerAutoRefreshFail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerAutoRefreshed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerClose(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerLoaded(Ljava/lang/String;)V
.end method

.method public abstract onBannerShow(Ljava/lang/String;Ljava/lang/String;)V
.end method
