.class public Lcom/anythink/unitybridge/banner/BannerHelper;
.super Ljava/lang/Object;
.source "BannerHelper.java"


# static fields
.field private static final kATBannerAdShowingPisitionBottom:Ljava/lang/String; = "bottom"

.field private static final kATBannerAdShowingPisitionTop:Ljava/lang/String; = "top"


# instance fields
.field private final TAG:Ljava/lang/String;

.field mActivity:Landroid/app/Activity;

.field mBannerView:Lcom/anythink/banner/api/ATBannerView;

.field mListener:Lcom/anythink/unitybridge/banner/BannerListener;

.field mPlacementId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/anythink/unitybridge/banner/BannerListener;)V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->TAG:Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BannerHelper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "Listener == null"

    .line 42
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 44
    :cond_0
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mListener:Lcom/anythink/unitybridge/banner/BannerListener;

    const-string p1, "BannerHelper"

    .line 45
    invoke-static {p1}, Lcom/anythink/unitybridge/UnityPluginUtils;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mActivity:Landroid/app/Activity;

    const-string p1, ""

    .line 46
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper;->setConfig(Ljava/lang/String;)V

    return-void
.end method

.method private setConfig(Ljava/lang/String;)V
    .locals 3

    const-string v0, "Scenario"

    .line 321
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_0

    .line 323
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 325
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 329
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 334
    :cond_0
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "showBanner: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", scenario: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 335
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 336
    iget-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {p1, v2}, Lcom/anythink/banner/api/ATBannerView;->setScenario(Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public checkAdStatus()Ljava/lang/String;
    .locals 5

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkAdStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    if-eqz v0, :cond_0

    .line 390
    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->checkAdStatus()Lcom/anythink/core/api/ATAdStatusInfo;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->isLoading()Z

    move-result v1

    .line 392
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->isReady()Z

    move-result v2

    .line 393
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->getATTopAdInfo()Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    .line 396
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "isLoading"

    .line 397
    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "isReady"

    .line 398
    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "adInfo"

    .line 399
    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 401
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    .line 403
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public cleanBannerAd()V
    .locals 2

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clean: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 373
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$8;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$8;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public hideBannerAd()V
    .locals 2

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hideBannerAd: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 358
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$7;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$7;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initBanner(Ljava/lang/String;)V
    .locals 2

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initBanner 1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    .line 53
    new-instance p1, Lcom/anythink/banner/api/ATBannerView;

    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mActivity:Landroid/app/Activity;

    invoke-direct {p1, v0}, Lcom/anythink/banner/api/ATBannerView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    .line 54
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/anythink/banner/api/ATBannerView;->setPlacementId(Ljava/lang/String;)V

    .line 55
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "initBanner 2: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 56
    iget-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$1;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$1;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V

    invoke-virtual {p1, v0}, Lcom/anythink/banner/api/ATBannerView;->setBannerAdListener(Lcom/anythink/banner/api/ATBannerListener;)V

    .line 163
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "initBanner 3: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    return-void
.end method

.method public loadBannerAd()V
    .locals 1

    .line 238
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$3;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$3;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public loadBannerAd(Ljava/lang/String;)V
    .locals 2

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadBanner: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", jsonMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$2;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public showBannerAd()V
    .locals 2

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showBanner without ATRect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 343
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$6;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$6;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public showBannerAd(IIIILjava/lang/String;)V
    .locals 8

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showBanner: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", jsonMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 263
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$4;

    move-object v1, v0

    move-object v2, p0

    move v3, p3

    move v4, p4

    move v5, p1

    move v6, p2

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/anythink/unitybridge/banner/BannerHelper$4;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;IIIILjava/lang/String;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public showBannerAd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showBanner by position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", jsonMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 288
    new-instance v0, Lcom/anythink/unitybridge/banner/BannerHelper$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/anythink/unitybridge/banner/BannerHelper$5;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
