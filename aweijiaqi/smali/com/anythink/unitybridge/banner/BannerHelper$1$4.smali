.class Lcom/anythink/unitybridge/banner/BannerHelper$1$4;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper$1;->onBannerShow(Lcom/anythink/core/api/ATAdInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

.field final synthetic val$adInfo:Lcom/anythink/core/api/ATAdInfo;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/ATAdInfo;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

    iput-object p2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->val$adInfo:Lcom/anythink/core/api/ATAdInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 109
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mListener:Lcom/anythink/unitybridge/banner/BannerListener;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    monitor-enter v0

    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mListener:Lcom/anythink/unitybridge/banner/BannerListener;

    iget-object v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->this$1:Lcom/anythink/unitybridge/banner/BannerHelper$1;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;->val$adInfo:Lcom/anythink/core/api/ATAdInfo;

    invoke-virtual {v3}, Lcom/anythink/core/api/ATAdInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/anythink/unitybridge/banner/BannerListener;->onBannerShow(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method
