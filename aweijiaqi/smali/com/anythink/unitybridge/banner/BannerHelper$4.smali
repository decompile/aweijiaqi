.class Lcom/anythink/unitybridge/banner/BannerHelper$4;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper;->showBannerAd(IIIILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

.field final synthetic val$height:I

.field final synthetic val$jsonMap:Ljava/lang/String;

.field final synthetic val$width:I

.field final synthetic val$x:I

.field final synthetic val$y:I


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper;IIIILjava/lang/String;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iput p2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$width:I

    iput p3, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$height:I

    iput p4, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$x:I

    iput p5, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$y:I

    iput-object p6, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$jsonMap:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 266
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    if-eqz v0, :cond_1

    .line 267
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$width:I

    iget v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$height:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 268
    iget v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$x:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 269
    iget v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$y:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 270
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v1}, Lcom/anythink/banner/api/ATBannerView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v1}, Lcom/anythink/banner/api/ATBannerView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->val$jsonMap:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/anythink/unitybridge/banner/BannerHelper;->access$000(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 278
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "show error, you must call initBanner first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$4;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
