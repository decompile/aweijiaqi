.class Lcom/anythink/unitybridge/banner/BannerHelper$1;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Lcom/anythink/banner/api/ATBannerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper;->initBanner(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/banner/BannerHelper;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBannerAutoRefreshFail(Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerAutoRefreshFail: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 151
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$7;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$7;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerAutoRefreshed(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerAutoRefreshed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$6;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$6;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerClicked(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerClicked: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$3;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$3;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerClose(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerClose: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 121
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$5;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$5;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerFailed(Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$2;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerLoaded()V
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerLoaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/banner/BannerHelper$1$1;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBannerShow(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBannerShow: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$1;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/banner/BannerHelper$1$4;-><init>(Lcom/anythink/unitybridge/banner/BannerHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method
