.class Lcom/anythink/unitybridge/banner/BannerHelper$5;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper;->showBannerAd(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

.field final synthetic val$jsonMap:Ljava/lang/String;

.field final synthetic val$position:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->val$position:Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->val$jsonMap:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 291
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    if-eqz v0, :cond_3

    .line 294
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 296
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 298
    :goto_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 299
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->val$position:Ljava/lang/String;

    const-string v1, "top"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x31

    .line 300
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1

    :cond_1
    const/16 v0, 0x51

    .line 302
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 304
    :goto_1
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0}, Lcom/anythink/banner/api/ATBannerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 308
    :cond_2
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->val$jsonMap:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/anythink/unitybridge/banner/BannerHelper;->access$000(Lcom/anythink/unitybridge/banner/BannerHelper;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 312
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "show error, you must call initBanner first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$5;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    :goto_2
    return-void
.end method
