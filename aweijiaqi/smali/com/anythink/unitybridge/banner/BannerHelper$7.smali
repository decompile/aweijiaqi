.class Lcom/anythink/unitybridge/banner/BannerHelper$7;
.super Ljava/lang/Object;
.source "BannerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/banner/BannerHelper;->hideBannerAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/banner/BannerHelper;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/banner/BannerHelper;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$7;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$7;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/anythink/unitybridge/banner/BannerHelper$7;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/banner/BannerHelper;->mBannerView:Lcom/anythink/banner/api/ATBannerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/anythink/banner/api/ATBannerView;->setVisibility(I)V

    goto :goto_0

    .line 364
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hideBannerAd error, you must call initBanner first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/banner/BannerHelper$7;->this$0:Lcom/anythink/unitybridge/banner/BannerHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/banner/BannerHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
