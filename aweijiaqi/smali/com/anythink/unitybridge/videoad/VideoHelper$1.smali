.class Lcom/anythink/unitybridge/videoad/VideoHelper$1;
.super Ljava/lang/Object;
.source "VideoHelper.java"

# interfaces
.implements Lcom/anythink/rewardvideo/api/ATRewardVideoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/videoad/VideoHelper;->initVideo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/videoad/VideoHelper;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReward(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReward: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->isReward:Z

    .line 164
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$8;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$8;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdClosed(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdClosed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 133
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$6;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$6;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdFailed(Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$2;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdLoaded()V
    .locals 2

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdLoaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$1;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdPlayClicked(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdPlayClicked: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 148
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$7;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$7;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdPlayEnd(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdPlayEnd: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$4;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$4;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdPlayFailed(Lcom/anythink/core/api/AdError;Lcom/anythink/core/api/ATAdInfo;)V
    .locals 1

    .line 116
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onRewardedVideoAdPlayFailed: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object p2

    new-instance v0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;

    invoke-direct {v0, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {p2, v0}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRewardedVideoAdPlayStart(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardedVideoAdPlayStart: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1$3;

    invoke-direct {v1, p0, p1}, Lcom/anythink/unitybridge/videoad/VideoHelper$1$3;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void
.end method
