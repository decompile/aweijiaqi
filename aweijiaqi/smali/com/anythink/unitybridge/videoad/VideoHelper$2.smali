.class Lcom/anythink/unitybridge/videoad/VideoHelper$2;
.super Ljava/lang/Object;
.source "VideoHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/videoad/VideoHelper;->fillVideo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

.field final synthetic val$jsonMap:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/videoad/VideoHelper;Ljava/lang/String;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->val$jsonMap:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const-string v0, "UserExtraData"

    const-string v1, "UserId"

    const-string v2, ""

    .line 187
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 190
    :try_start_0
    iget-object v4, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->val$jsonMap:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 191
    new-instance v4, Lorg/json/JSONObject;

    iget-object v5, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->val$jsonMap:Ljava/lang/String;

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-static {v3, v4}, Lcom/anythink/unitybridge/utils/Const;->fillMapFromJsonObject(Ljava/util/Map;Lorg/json/JSONObject;)V

    .line 195
    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 199
    :goto_0
    :try_start_1
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 200
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_2

    :catch_0
    move-object v1, v2

    :catch_1
    :cond_2
    move-object v0, v2

    :goto_1
    move-object v2, v1

    .line 207
    :goto_2
    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    if-eqz v1, :cond_5

    .line 209
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    const-string v1, "user_id"

    .line 211
    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "user_custom_data"

    .line 212
    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fillVideo: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v4, v4, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", userId:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", userExtraData:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 217
    :cond_4
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    invoke-virtual {v0, v3}, Lcom/anythink/rewardvideo/api/ATRewardVideoAd;->setLocalExtra(Ljava/util/Map;)V

    .line 218
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    invoke-virtual {v0}, Lcom/anythink/rewardvideo/api/ATRewardVideoAd;->load()V

    goto :goto_3

    .line 220
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fillVideo error, you must call initVideo first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$2;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 221
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$2$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/videoad/VideoHelper$2$1;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$2;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    :goto_3
    return-void
.end method
