.class public interface abstract Lcom/anythink/unitybridge/videoad/VideoListener;
.super Ljava/lang/Object;
.source "VideoListener.java"


# virtual methods
.method public abstract onReward(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdClosed(Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdLoaded(Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdPlayClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdPlayEnd(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdPlayFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdPlayStart(Ljava/lang/String;Ljava/lang/String;)V
.end method
