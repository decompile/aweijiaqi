.class Lcom/anythink/unitybridge/videoad/VideoHelper$3;
.super Ljava/lang/Object;
.source "VideoHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/videoad/VideoHelper;->showVideo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

.field final synthetic val$jsonMap:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/videoad/VideoHelper;Ljava/lang/String;)V
    .locals 0

    .line 241
    iput-object p1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->val$jsonMap:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "Scenario"

    .line 244
    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    if-eqz v1, :cond_2

    .line 245
    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->isReady:Z

    const-string v1, ""

    .line 248
    iget-object v2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->val$jsonMap:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 250
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->val$jsonMap:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 252
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 256
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 260
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showVideo: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", scenario: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 261
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    iget-object v2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/videoad/VideoHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2, v1}, Lcom/anythink/rewardvideo/api/ATRewardVideoAd;->show(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mRewardVideoAd:Lcom/anythink/rewardvideo/api/ATRewardVideoAd;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/anythink/rewardvideo/api/ATRewardVideoAd;->show(Landroid/app/Activity;)V

    goto :goto_1

    .line 267
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showVideo error, you must call initVideo first "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$3;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 268
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/videoad/VideoHelper$3$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/videoad/VideoHelper$3$1;-><init>(Lcom/anythink/unitybridge/videoad/VideoHelper$3;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    :goto_1
    return-void
.end method
