.class Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;
.super Ljava/lang/Object;
.source "VideoHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/videoad/VideoHelper$1;->onRewardedVideoAdPlayFailed(Lcom/anythink/core/api/AdError;Lcom/anythink/core/api/ATAdInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

.field final synthetic val$pAdError:Lcom/anythink/core/api/AdError;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/videoad/VideoHelper$1;Lcom/anythink/core/api/AdError;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

    iput-object p2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 120
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper;->mListener:Lcom/anythink/unitybridge/videoad/VideoListener;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    monitor-enter v0

    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/videoad/VideoHelper;->mListener:Lcom/anythink/unitybridge/videoad/VideoListener;

    iget-object v2, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->this$1:Lcom/anythink/unitybridge/videoad/VideoHelper$1;

    iget-object v2, v2, Lcom/anythink/unitybridge/videoad/VideoHelper$1;->this$0:Lcom/anythink/unitybridge/videoad/VideoHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/videoad/VideoHelper;->mPlacementId:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-virtual {v3}, Lcom/anythink/core/api/AdError;->getCode()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/unitybridge/videoad/VideoHelper$1$5;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-virtual {v4}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/anythink/unitybridge/videoad/VideoListener;->onRewardedVideoAdPlayFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method
