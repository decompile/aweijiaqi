.class Lcom/anythink/unitybridge/nativead/NativeHelper$3;
.super Ljava/lang/Object;
.source "NativeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/nativead/NativeHelper;->show(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

.field final synthetic val$jsonMap:Ljava/lang/String;

.field final synthetic val$showConfig:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->val$jsonMap:Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->val$showConfig:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "Scenario"

    .line 226
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->val$jsonMap:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_0

    .line 228
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->val$jsonMap:Ljava/lang/String;

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 230
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 239
    :cond_0
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v0, v2}, Lcom/anythink/nativead/api/ATNative;->getNativeAd(Ljava/lang/String;)Lcom/anythink/nativead/api/NativeAd;

    move-result-object v0

    goto :goto_1

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v0}, Lcom/anythink/nativead/api/ATNative;->getNativeAd()Lcom/anythink/nativead/api/NativeAd;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_6

    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nativeAd:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", scenario: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->val$showConfig:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/anythink/unitybridge/nativead/NativeHelper;->access$000(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;)Lcom/anythink/unitybridge/nativead/ViewInfo;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    .line 248
    sget-object v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->currViewInfo:Ljava/util/List;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iput-object v0, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mNativeAd:Lcom/anythink/nativead/api/NativeAd;

    .line 250
    new-instance v1, Lcom/anythink/unitybridge/nativead/NativeHelper$3$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/nativead/NativeHelper$3$1;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper$3;)V

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/api/NativeAd;->setNativeEventListener(Lcom/anythink/nativead/api/ATNativeEventListener;)V

    .line 329
    new-instance v1, Lcom/anythink/unitybridge/nativead/NativeHelper$3$2;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/nativead/NativeHelper$3$2;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper$3;)V

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/api/NativeAd;->setDislikeCallbackListener(Lcom/anythink/nativead/api/ATNativeDislikeListener;)V

    .line 346
    new-instance v1, Lcom/anythink/unitybridge/nativead/ATUnityRender;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-direct {v1, v2, v3}, Lcom/anythink/unitybridge/nativead/ATUnityRender;-><init>(Landroid/app/Activity;Lcom/anythink/unitybridge/nativead/ViewInfo;)V

    .line 348
    :try_start_1
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v2, :cond_2

    .line 349
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v2, v3}, Lcom/anythink/unitybridge/nativead/NativeHelper;->access$100(Lcom/anythink/unitybridge/nativead/NativeHelper;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;)V

    .line 351
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->setDislikeView(Landroid/view/View;)V

    .line 354
    :cond_2
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v0, v2, v1}, Lcom/anythink/nativead/api/NativeAd;->renderAdView(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/nativead/api/ATNativeAdRenderer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v2

    .line 356
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 360
    :goto_2
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 361
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 362
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 365
    :cond_3
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/anythink/nativead/api/ATNativeAdView;->addView(Landroid/view/View;)V

    .line 368
    :cond_4
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v2, :cond_5

    .line 369
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    iget-object v4, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 370
    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 371
    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 372
    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->getClickViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v3, v1, v2}, Lcom/anythink/nativead/api/NativeAd;->prepare(Lcom/anythink/nativead/api/ATNativeAdView;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "prepare native ad with logo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    goto :goto_3

    .line 375
    :cond_5
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->getClickViews()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lcom/anythink/nativead/api/NativeAd;->prepare(Lcom/anythink/nativead/api/ATNativeAdView;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "prepare native ad:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 379
    :goto_3
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-static {v0, v1, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->addNativeAdView2Activity(Landroid/app/Activity;Lcom/anythink/unitybridge/nativead/ViewInfo;Lcom/anythink/nativead/api/ATNativeAdView;)V

    goto :goto_4

    .line 381
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    :goto_4
    return-void
.end method
