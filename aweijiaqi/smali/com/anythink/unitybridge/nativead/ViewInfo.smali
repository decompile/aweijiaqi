.class public Lcom/anythink/unitybridge/nativead/ViewInfo;
.super Ljava/lang/Object;
.source "ViewInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;
    }
.end annotation


# instance fields
.field public IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

.field public titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V
    .locals 3

    if-eqz p0, :cond_6

    if-nez p2, :cond_0

    goto/16 :goto_3

    :cond_0
    if-eqz p1, :cond_5

    .line 45
    iget v0, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    if-ltz v0, :cond_5

    iget v0, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    if-gez v0, :cond_1

    goto :goto_2

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]   add 2 activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "add2activity"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    iget v2, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 53
    iget v1, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 54
    iget v1, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    if-lez p3, :cond_2

    .line 56
    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    :cond_2
    const/16 p3, 0x33

    .line 58
    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 62
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    :try_start_0
    iget-object p3, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_3

    .line 67
    iget-object p2, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p2

    .line 71
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    .line 74
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    .line 75
    instance-of p3, p2, Landroid/view/ViewGroup;

    if-eqz p3, :cond_4

    .line 76
    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 79
    :cond_4
    invoke-virtual {p0, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 46
    :cond_5
    :goto_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "add2activity--["

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, " config error ,show error !"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_3
    return-void
.end method

.method public static addNativeAdView2Activity(Landroid/app/Activity;Lcom/anythink/unitybridge/nativead/ViewInfo;Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 1

    if-eqz p0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 101
    :cond_0
    new-instance v0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;

    invoke-direct {v0, p2, p1, p0}, Lcom/anythink/unitybridge/nativead/ViewInfo$1;-><init>(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/unitybridge/nativead/ViewInfo;Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    :goto_0
    const-string p0, "pActivity or native ad view is null"

    .line 96
    invoke-static {p0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    return-void
.end method

.method public static createDefualtView(Landroid/app/Activity;)Lcom/anythink/unitybridge/nativead/ViewInfo;
    .locals 7

    .line 142
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    .line 143
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 144
    iget p0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 146
    new-instance v1, Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-direct {v1}, Lcom/anythink/unitybridge/nativead/ViewInfo;-><init>()V

    .line 147
    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const/16 v3, 0xc

    iput v3, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 148
    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v4, "0X000000"

    iput-object v4, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 149
    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v5, "0XFFFFFF"

    iput-object v5, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 150
    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput p0, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 151
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 152
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const/4 v0, 0x0

    iput v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 153
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 154
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v2, "rootView_def"

    iput-object v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 157
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 158
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 159
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 160
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const/16 v2, 0x19

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 161
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 162
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 163
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 164
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v6, "imgMainView_def"

    iput-object v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 167
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 168
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 169
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 170
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x5

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 171
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    div-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 172
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/lit8 v6, v6, 0x64

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 173
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/lit8 v6, v6, 0xa

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 174
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v6, "adlogo_def"

    iput-object v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 177
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 178
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 179
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 180
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 181
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 182
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 183
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 184
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v6, "appicon_def"

    iput-object v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 187
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 188
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 189
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 190
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 191
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 192
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 193
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 196
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 197
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 198
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 199
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 200
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 201
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 202
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v6, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v6, v6, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 203
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v6, "desc_def"

    iput-object v6, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 205
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 206
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 207
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 208
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 209
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 210
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v5, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v5, v5, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v5, v0

    iput v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 211
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v5, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v5, v5, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v5, v0

    iput v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 212
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v5, "cta_def"

    iput-object v5, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    .line 214
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    .line 215
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    .line 216
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput-object v4, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    .line 217
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 218
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 219
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 220
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v2, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 221
    iget-object p0, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const-string v0, "dislike_def"

    iput-object v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 227
    new-instance v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;-><init>(Lcom/anythink/unitybridge/nativead/ViewInfo;)V

    .line 228
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "x"

    .line 229
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    add-int/2addr p1, p3

    iput p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    :cond_0
    const-string p1, "y"

    .line 233
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 234
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    add-int/2addr p1, p4

    iput p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    :cond_1
    const-string p1, "width"

    .line 236
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 237
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    :cond_2
    const-string p1, "height"

    .line 239
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 240
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    :cond_3
    const-string p1, "backgroundColor"

    .line 242
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 243
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    :cond_4
    const-string p1, "textColor"

    .line 245
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 246
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    :cond_5
    const-string p1, "textSize"

    .line 248
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_6

    .line 249
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    :cond_6
    const-string p1, "isCustomClick"

    .line 251
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_7

    .line 252
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    .line 255
    :cond_7
    iput-object p2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->name:Ljava/lang/String;

    return-object v0
.end method
