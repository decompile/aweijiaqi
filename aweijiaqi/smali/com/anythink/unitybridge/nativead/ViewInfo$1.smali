.class final Lcom/anythink/unitybridge/nativead/ViewInfo$1;
.super Ljava/lang/Object;
.source "ViewInfo.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/nativead/ViewInfo;->addNativeAdView2Activity(Landroid/app/Activity;Lcom/anythink/unitybridge/nativead/ViewInfo;Lcom/anythink/nativead/api/ATNativeAdView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

.field final synthetic val$pActivity:Landroid/app/Activity;

.field final synthetic val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;


# direct methods
.method constructor <init>(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/unitybridge/nativead/ViewInfo;Landroid/app/Activity;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iput-object p3, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pActivity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v0}, Lcom/anythink/nativead/api/ATNativeAdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 110
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 114
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_2

    .line 115
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 116
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 117
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 119
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/anythink/nativead/api/ATNativeAdView;->setBackgroundColor(I)V

    :cond_1
    const-string v1, "Add native view to content start...."

    .line 123
    invoke-static {v1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 124
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$pActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/ViewInfo$1;->val$mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "Add native view to content end...."

    .line 125
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v0, "pViewInfo.rootView is null"

    .line 127
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    :goto_1
    return-void
.end method
