.class Lcom/anythink/unitybridge/nativead/NativeHelper$4;
.super Ljava/lang/Object;
.source "NativeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/nativead/NativeHelper;->cleanView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/nativead/NativeHelper;)V
    .locals 0

    .line 424
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$4;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 429
    :try_start_0
    sget-object v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->currViewInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anythink/unitybridge/nativead/ViewInfo;

    .line 430
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$4;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$4;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v1}, Lcom/anythink/nativead/api/ATNativeAdView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 433
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$4;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 441
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-void
.end method
