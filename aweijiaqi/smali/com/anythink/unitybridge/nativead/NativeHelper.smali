.class public Lcom/anythink/unitybridge/nativead/NativeHelper;
.super Ljava/lang/Object;
.source "NativeHelper.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "AT_android_unity3d"

.field static currViewInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/unitybridge/nativead/ViewInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mATNative:Lcom/anythink/nativead/api/ATNative;

.field mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

.field mActivity:Landroid/app/Activity;

.field mDislikeView:Landroid/widget/ImageView;

.field mListener:Lcom/anythink/unitybridge/nativead/NativeListener;

.field mNativeAd:Lcom/anythink/nativead/api/NativeAd;

.field mPlacementId:Ljava/lang/String;

.field pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->currViewInfo:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/anythink/unitybridge/nativead/NativeListener;)V
    .locals 2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string v0, "AT_android_unity3d"

    const-string v1, "Listener == null"

    .line 50
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mListener:Lcom/anythink/unitybridge/nativead/NativeListener;

    const-string p1, "NativeHelper"

    .line 53
    invoke-static {p1}, Lcom/anythink/unitybridge/UnityPluginUtils;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    const-string p1, ""

    .line 54
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;)Lcom/anythink/unitybridge/nativead/ViewInfo;
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/anythink/unitybridge/nativead/NativeHelper;->parseViewInfo(Ljava/lang/String;)Lcom/anythink/unitybridge/nativead/ViewInfo;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/anythink/unitybridge/nativead/NativeHelper;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/anythink/unitybridge/nativead/NativeHelper;->initDislikeView(Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;)V

    return-void
.end method

.method private initDislikeView(Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;)V
    .locals 4

    .line 390
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 391
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    .line 392
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    const-string v2, "btn_close"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/anythink/unitybridge/utils/CommonUtil;->getResId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 395
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    iget v2, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 396
    iget v1, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 397
    iget v1, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 399
    iget-object v1, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    iget-object p1, p1, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->bgcolor:Ljava/lang/String;

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 403
    :cond_1
    iget-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mDislikeView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private parseViewInfo(Ljava/lang/String;)Lcom/anythink/unitybridge/nativead/ViewInfo;
    .locals 12

    const-string v0, "dislike"

    const-string v1, "cta"

    const-string v2, "adLogo"

    const-string v3, "desc"

    const-string v4, "title"

    const-string v5, "mainImage"

    const-string v6, "appIcon"

    const-string v7, "parent"

    .line 152
    new-instance v8, Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-direct {v8}, Lcom/anythink/unitybridge/nativead/ViewInfo;-><init>()V

    iput-object v8, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    .line 154
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "AT_android_unity3d"

    const-string v9, "showConfig is null ,user defult"

    .line 155
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v8, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v8}, Lcom/anythink/unitybridge/nativead/ViewInfo;->createDefualtView(Landroid/app/Activity;)Lcom/anythink/unitybridge/nativead/ViewInfo;

    move-result-object v8

    iput-object v8, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    .line 160
    :cond_0
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 162
    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    const/4 v9, 0x0

    if-eqz p1, :cond_1

    .line 163
    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 164
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parent----> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 165
    iget-object v10, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v11, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v11, p1, v7, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v10, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 168
    :cond_1
    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 169
    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 170
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "appIcon----> "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 171
    iget-object v7, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v10, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v10, p1, v6, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v7, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 174
    :cond_2
    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 175
    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 176
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mainImage----> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 177
    iget-object v6, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v7, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v7, p1, v5, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v6, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 181
    :cond_3
    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 182
    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 183
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "title----> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 184
    iget-object v5, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v6, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v6, p1, v4, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v5, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 187
    :cond_4
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 188
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 189
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "desc----> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 190
    iget-object v4, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v5, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v5, p1, v3, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 193
    :cond_5
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 194
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 195
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adLogo----> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 196
    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v4, p1, v2, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 199
    :cond_6
    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 200
    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 201
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cta----> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 202
    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v3, p1, v1, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v2, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    .line 205
    :cond_7
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 206
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dislike----> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    invoke-virtual {v2, p1, v0, v9, v9}, Lcom/anythink/unitybridge/nativead/ViewInfo;->parseINFO(Ljava/lang/String;Ljava/lang/String;II)Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    move-result-object p1

    iput-object p1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->dislikeView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 213
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 215
    :cond_8
    :goto_0
    iget-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->pViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    return-object p1
.end method


# virtual methods
.method public checkAdStatus()Ljava/lang/String;
    .locals 5

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkAdStatus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 464
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {v0}, Lcom/anythink/nativead/api/ATNative;->checkAdStatus()Lcom/anythink/core/api/ATAdStatusInfo;

    move-result-object v0

    .line 466
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->isLoading()Z

    move-result v1

    .line 467
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->isReady()Z

    move-result v2

    .line 468
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->getATTopAdInfo()Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    .line 471
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "isLoading"

    .line 472
    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "isReady"

    .line 473
    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "adInfo"

    .line 474
    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 476
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    .line 478
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public clean()V
    .locals 0

    return-void
.end method

.method public cleanView()V
    .locals 2

    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cleanView:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 424
    new-instance v0, Lcom/anythink/unitybridge/nativead/NativeHelper$4;

    invoke-direct {v0, p0}, Lcom/anythink/unitybridge/nativead/NativeHelper$4;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initNative(Ljava/lang/String;)V
    .locals 3

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initNative "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 59
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/anythink/nativead/api/ATNative;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    new-instance v2, Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    invoke-direct {v2, p0}, Lcom/anythink/unitybridge/nativead/NativeHelper$1;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper;)V

    invoke-direct {v0, v1, p1, v2}, Lcom/anythink/nativead/api/ATNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/nativead/api/ATNativeNetworkListener;)V

    iput-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    .line 92
    iget-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    if-nez p1, :cond_0

    .line 93
    new-instance p1, Lcom/anythink/nativead/api/ATNativeAdView;

    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mActivity:Landroid/app/Activity;

    invoke-direct {p1, v0}, Lcom/anythink/nativead/api/ATNativeAdView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNativeAdView:Lcom/anythink/nativead/api/ATNativeAdView;

    .line 96
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "initNative 2 "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    return-void
.end method

.method public isAdReady()Z
    .locals 3

    .line 407
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isAdReady:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v0}, Lcom/anythink/nativead/api/ATNative;->checkAdStatus()Lcom/anythink/core/api/ATAdStatusInfo;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->isReady()Z

    move-result v0

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    return v0
.end method

.method public loadNative(Ljava/lang/String;)V
    .locals 2

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadNative: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".localExtra: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/unitybridge/nativead/NativeHelper$2;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mNativeAd:Lcom/anythink/nativead/api/NativeAd;

    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {v0}, Lcom/anythink/nativead/api/NativeAd;->onPause()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mNativeAd:Lcom/anythink/nativead/api/NativeAd;

    if-eqz v0, :cond_0

    .line 458
    invoke-virtual {v0}, Lcom/anythink/nativead/api/NativeAd;->onResume()V

    :cond_0
    return-void
.end method

.method public show(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "show: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", showConfig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", jsonMap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 221
    new-instance v0, Lcom/anythink/unitybridge/nativead/NativeHelper$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/anythink/unitybridge/nativead/NativeHelper$3;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/anythink/unitybridge/UnityPluginUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
