.class Lcom/anythink/unitybridge/nativead/NativeHelper$2;
.super Ljava/lang/Object;
.source "NativeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/nativead/NativeHelper;->loadNative(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

.field final synthetic val$localExtra:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/nativead/NativeHelper;Ljava/lang/String;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->val$localExtra:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 104
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    if-nez v0, :cond_0

    const-string v0, "AT_android_unity3d"

    const-string v1, "you must call initNative first"

    .line 105
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-static {}, Lcom/anythink/unitybridge/utils/TaskManager;->getInstance()Lcom/anythink/unitybridge/utils/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/unitybridge/nativead/NativeHelper$2$1;

    invoke-direct {v1, p0}, Lcom/anythink/unitybridge/nativead/NativeHelper$2$1;-><init>(Lcom/anythink/unitybridge/nativead/NativeHelper$2;)V

    invoke-virtual {v0, v1}, Lcom/anythink/unitybridge/utils/TaskManager;->run_proxy(Ljava/lang/Runnable;)V

    return-void

    .line 119
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 121
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->val$localExtra:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 124
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 125
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 126
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "native_ad_size"

    .line 128
    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "x"

    .line 132
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string v4, "key_width"

    const/4 v5, 0x0

    .line 133
    aget-object v5, v3, v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "key_height"

    const/4 v5, 0x1

    .line 134
    aget-object v3, v3, v5

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 138
    :cond_2
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v1, v0}, Lcom/anythink/nativead/api/ATNative;->setLocalExtra(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 143
    :goto_1
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$2;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mATNative:Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v0}, Lcom/anythink/nativead/api/ATNative;->makeAdRequest()V

    return-void
.end method
