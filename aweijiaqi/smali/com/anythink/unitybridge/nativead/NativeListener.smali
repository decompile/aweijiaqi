.class public interface abstract Lcom/anythink/unitybridge/nativead/NativeListener;
.super Ljava/lang/Object;
.source "NativeListener.java"


# virtual methods
.method public abstract onAdClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAdCloseButtonClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAdImpressed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onAdVideoEnd(Ljava/lang/String;)V
.end method

.method public abstract onAdVideoProgress(Ljava/lang/String;I)V
.end method

.method public abstract onAdVideoStart(Ljava/lang/String;)V
.end method

.method public abstract onNativeAdLoadFail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onNativeAdLoaded(Ljava/lang/String;)V
.end method
