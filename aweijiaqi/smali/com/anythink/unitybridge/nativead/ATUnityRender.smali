.class public Lcom/anythink/unitybridge/nativead/ATUnityRender;
.super Ljava/lang/Object;
.source "ATUnityRender.java"

# interfaces
.implements Lcom/anythink/nativead/api/ATNativeAdRenderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/anythink/nativead/api/ATNativeAdRenderer<",
        "Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;",
        ">;"
    }
.end annotation


# instance fields
.field mActivity:Landroid/app/Activity;

.field mClickViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mDislikeView:Landroid/view/View;

.field mFrameLayout:Landroid/widget/FrameLayout;

.field mNetworkType:I

.field mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/anythink/unitybridge/nativead/ViewInfo;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    .line 34
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    .line 35
    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    return-void
.end method

.method private dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Z",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 271
    iget v0, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_2

    :cond_0
    if-eqz p2, :cond_2

    if-eqz p1, :cond_1

    .line 274
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "add customClick ----> "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 275
    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    .line 280
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "add click ----> "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 281
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public createView(Landroid/content/Context;I)Landroid/view/View;
    .locals 0

    .line 44
    iput p2, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    .line 45
    new-instance p2, Landroid/widget/FrameLayout;

    invoke-direct {p2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    return-object p2
.end method

.method public getClickViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic renderAdView(Landroid/view/View;Lcom/anythink/nativead/unitgroup/a;)V
    .locals 0

    .line 24
    check-cast p2, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    invoke-virtual {p0, p1, p2}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->renderAdView(Landroid/view/View;Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;)V

    return-void
.end method

.method public renderAdView(Landroid/view/View;Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;)V
    .locals 16

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    .line 52
    new-instance v8, Landroid/widget/TextView;

    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v8, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 53
    new-instance v9, Landroid/widget/TextView;

    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v9, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 54
    new-instance v10, Landroid/widget/TextView;

    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v10, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 56
    iget-object v2, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    const/4 v11, 0x0

    aput-object v2, v1, v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v7, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdMediaView([Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    const/4 v2, -0x1

    if-eqz v1, :cond_1

    .line 57
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->isNativeExpress()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 59
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mDislikeView:Landroid/view/View;

    if-eqz v4, :cond_0

    const/16 v5, 0x8

    .line 60
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 63
    :cond_0
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_1

    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_1

    .line 64
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v11, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mX:I

    .line 65
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iput v11, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mY:I

    .line 66
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    iput v3, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mWidth:I

    .line 67
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    iput v3, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->mHeight:I

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ExpressNative, NetworkType ----> "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 70
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v3, v3, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v0, v1, v3, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    return-void

    .line 75
    :cond_1
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const/16 v5, 0xf

    if-eqz v4, :cond_4

    .line 77
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 78
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 81
    :cond_2
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    if-lez v4, :cond_3

    .line 82
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 84
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "title---->"

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 85
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {v8}, Landroid/widget/TextView;->setSingleLine()V

    .line 88
    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 89
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 92
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v12, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v12, v12, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v4, v8, v12, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    .line 97
    :cond_4
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_7

    .line 99
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 102
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 105
    :cond_5
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    if-lez v4, :cond_6

    .line 106
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    int-to-float v4, v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_6
    const/16 v4, 0x11

    .line 110
    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 111
    invoke-virtual {v10}, Landroid/widget/TextView;->setSingleLine()V

    .line 112
    invoke-virtual {v10, v5}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 113
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cat---->"

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getCallToActionText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 116
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getCallToActionText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v12, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v12, v12, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v4, v10, v12, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    .line 121
    :cond_7
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_a

    .line 123
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 124
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textcolor:Ljava/lang/String;

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 127
    :cond_8
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    if-lez v4, :cond_9

    .line 128
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->textSize:I

    int-to-float v4, v4

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 130
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "desc---->"

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getDescriptionText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 131
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getDescriptionText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x3

    .line 134
    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 135
    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 136
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 138
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v5, v5, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v4, v9, v5, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    .line 142
    :cond_a
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    const/4 v5, 0x0

    if-eqz v4, :cond_c

    .line 144
    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v12, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v4, v12}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 147
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdIconView()Landroid/view/View;

    move-result-object v12

    if-nez v12, :cond_b

    .line 148
    new-instance v12, Lcom/anythink/nativead/api/ATNativeImageView;

    iget-object v13, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v12, v13}, Lcom/anythink/nativead/api/ATNativeImageView;-><init>(Landroid/content/Context;)V

    .line 149
    new-instance v13, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v13, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v12, v13}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getIconImageUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/anythink/nativead/api/ATNativeImageView;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_b
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdIconView()Landroid/view/View;

    move-result-object v12

    new-instance v13, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v13, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v12, v13}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object v12, v5

    .line 156
    :goto_0
    iget-object v13, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v14, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v14, v14, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v13, v4, v14, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    goto :goto_1

    :cond_c
    move-object v12, v5

    .line 160
    :goto_1
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_d

    .line 161
    new-instance v4, Lcom/anythink/nativead/api/ATNativeImageView;

    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v4, v5}, Lcom/anythink/nativead/api/ATNativeImageView;-><init>(Landroid/content/Context;)V

    .line 162
    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v13, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v13, v13, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v5, v4, v13, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    .line 163
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdChoiceIconUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/anythink/nativead/api/ATNativeImageView;->setImage(Ljava/lang/String;)V

    move-object v13, v4

    goto :goto_2

    :cond_d
    move-object v13, v5

    .line 166
    :goto_2
    iget v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_e

    if-eqz v1, :cond_e

    .line 168
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mediaView ---> \u89c6\u5c4f\u64ad\u653e "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getVideoUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 169
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v4, v4, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v4, :cond_f

    .line 170
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v5, v5, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v4, v1, v5, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    goto :goto_3

    :cond_e
    const-string v1, "mediaView ---> \u5927\u56fe\u64ad\u653e"

    .line 174
    invoke-static {v1}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 175
    iget-object v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v1, :cond_f

    .line 176
    new-instance v1, Lcom/anythink/nativead/api/ATNativeImageView;

    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v4}, Lcom/anythink/nativead/api/ATNativeImageView;-><init>(Landroid/content/Context;)V

    .line 177
    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v5, v5, Lcom/anythink/unitybridge/nativead/ViewInfo;->imgMainView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    invoke-static {v4, v1, v5, v2}, Lcom/anythink/unitybridge/nativead/ViewInfo;->add2ParentView(Landroid/widget/FrameLayout;Landroid/view/View;Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;I)V

    .line 178
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getMainImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/anythink/nativead/api/ATNativeImageView;->setImage(Ljava/lang/String;)V

    .line 183
    :cond_f
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdFrom()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v4, -0x2

    const/high16 v5, 0x40400000    # 3.0f

    if-nez v1, :cond_10

    iget v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    const/16 v14, 0x17

    if-ne v1, v14, :cond_10

    .line 184
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 185
    iget-object v14, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v14, v5}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v14

    iput v14, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 186
    iget-object v14, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v14, v5}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v14

    iput v14, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    const/16 v14, 0x50

    .line 187
    iput v14, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 188
    new-instance v14, Landroid/widget/TextView;

    iget-object v15, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v14, v15}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 v15, 0x40c00000    # 6.0f

    .line 189
    invoke-virtual {v14, v3, v15}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 190
    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    const/high16 v15, 0x40a00000    # 5.0f

    invoke-static {v3, v15}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v3

    iget-object v4, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v4, v11}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v4

    iget-object v5, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v5, v15}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v5

    iget-object v15, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v15, v11}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v11

    invoke-virtual {v14, v3, v4, v5, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    const v3, -0x777778

    .line 191
    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 192
    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 193
    invoke-virtual/range {p2 .. p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdFrom()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v14, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 198
    :cond_10
    iget v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    if-ne v1, v0, :cond_11

    const-string v0, "start to add admob ad textview "

    .line 199
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 200
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 201
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v1, "AD"

    .line 202
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v1, 0x41300000    # 11.0f

    .line 203
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 204
    iget-object v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-static {v1, v2}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v1

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v2}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    const-string v1, "#66000000"

    .line 205
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 206
    iget-object v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_11

    .line 207
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 208
    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v2}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 209
    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v2}, Lcom/anythink/unitybridge/utils/CommonUtil;->dip2px(Landroid/content/Context;F)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 210
    iget-object v2, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "add admob ad textview 2 activity"

    .line 212
    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 217
    :cond_11
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 219
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_12

    .line 220
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->rootView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "root"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 222
    :cond_12
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_13

    .line 223
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->titleView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "title"

    move-object/from16 v0, p0

    move-object v1, v8

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 225
    :cond_13
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_14

    .line 226
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->descView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "desc"

    move-object/from16 v0, p0

    move-object v1, v9

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 228
    :cond_14
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_15

    .line 229
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->IconView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "icon"

    move-object/from16 v0, p0

    move-object v1, v12

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 231
    :cond_15
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_16

    .line 232
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->adLogoView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "adLogo"

    move-object/from16 v0, p0

    move-object v1, v13

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 234
    :cond_16
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    if-eqz v0, :cond_17

    .line 235
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mViewInfo:Lcom/anythink/unitybridge/nativead/ViewInfo;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/ViewInfo;->ctaView:Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;

    iget-boolean v2, v0, Lcom/anythink/unitybridge/nativead/ViewInfo$INFO;->isCustomClick:Z

    iget-object v3, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mClickViews:Ljava/util/List;

    const-string v5, "cta"

    move-object/from16 v0, p0

    move-object v1, v10

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/anythink/unitybridge/nativead/ATUnityRender;->dealWithClick(Landroid/view/View;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 241
    :cond_17
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mDislikeView:Landroid/view/View;

    if-eqz v0, :cond_19

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bind dislike ----> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mNetworkType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/unitybridge/MsgTools;->pirntMsg(Ljava/lang/String;)V

    .line 244
    iget-object v0, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mDislikeView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 246
    new-instance v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;

    invoke-direct {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;-><init>()V

    .line 247
    iget-object v1, v6, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mDislikeView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->setCloseView(Landroid/view/View;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;

    .line 249
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_18

    .line 250
    invoke-virtual {v0, v11}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->setCustomViewList(Ljava/util/List;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;

    .line 253
    :cond_18
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->build()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->setExtraInfo(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;)V

    goto :goto_4

    .line 255
    :cond_19
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1a

    .line 256
    new-instance v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;

    invoke-direct {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;-><init>()V

    .line 257
    invoke-virtual {v0, v11}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->setCustomViewList(Ljava/util/List;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->build()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->setExtraInfo(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;)V

    :cond_1a
    :goto_4
    return-void
.end method

.method public setDislikeView(Landroid/view/View;)V
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/ATUnityRender;->mDislikeView:Landroid/view/View;

    return-void
.end method
