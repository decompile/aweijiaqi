.class Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;
.super Ljava/lang/Object;
.source "NativeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/unitybridge/nativead/NativeHelper$1;->onNativeAdLoadFail(Lcom/anythink/core/api/AdError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

.field final synthetic val$pAdError:Lcom/anythink/core/api/AdError;


# direct methods
.method constructor <init>(Lcom/anythink/unitybridge/nativead/NativeHelper$1;Lcom/anythink/core/api/AdError;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    iput-object p2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 82
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper$1;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper;->mListener:Lcom/anythink/unitybridge/nativead/NativeListener;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    iget-object v0, v0, Lcom/anythink/unitybridge/nativead/NativeHelper$1;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    monitor-enter v0

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper$1;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v1, v1, Lcom/anythink/unitybridge/nativead/NativeHelper;->mListener:Lcom/anythink/unitybridge/nativead/NativeListener;

    iget-object v2, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->this$1:Lcom/anythink/unitybridge/nativead/NativeHelper$1;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper$1;->this$0:Lcom/anythink/unitybridge/nativead/NativeHelper;

    iget-object v2, v2, Lcom/anythink/unitybridge/nativead/NativeHelper;->mPlacementId:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-virtual {v3}, Lcom/anythink/core/api/AdError;->getCode()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/unitybridge/nativead/NativeHelper$1$2;->val$pAdError:Lcom/anythink/core/api/AdError;

    invoke-virtual {v4}, Lcom/anythink/core/api/AdError;->getFullErrorInfo()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/anythink/unitybridge/nativead/NativeListener;->onNativeAdLoadFail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :goto_0
    return-void
.end method
