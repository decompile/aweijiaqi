.class public Lcom/anythink/unitybridge/MsgTools;
.super Ljava/lang/Object;
.source "MsgTools.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AT_android_unity3d"

.field static isDebug:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static pirntMsg(Ljava/lang/String;)V
    .locals 1

    .line 11
    sget-boolean v0, Lcom/anythink/unitybridge/MsgTools;->isDebug:Z

    if-eqz v0, :cond_0

    const-string v0, "AT_android_unity3d"

    .line 12
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
