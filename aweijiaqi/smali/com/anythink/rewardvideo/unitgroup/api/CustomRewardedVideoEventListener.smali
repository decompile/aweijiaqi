.class public interface abstract Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onDeeplinkCallback(Z)V
.end method

.method public abstract onReward()V
.end method

.method public abstract onRewardedVideoAdClosed()V
.end method

.method public abstract onRewardedVideoAdPlayClicked()V
.end method

.method public abstract onRewardedVideoAdPlayEnd()V
.end method

.method public abstract onRewardedVideoAdPlayFailed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onRewardedVideoAdPlayStart()V
.end method
