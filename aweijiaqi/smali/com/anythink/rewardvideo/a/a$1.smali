.class final Lcom/anythink/rewardvideo/a/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/rewardvideo/a/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/anythink/rewardvideo/api/ATRewardVideoListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/d/b;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/app/Activity;

.field final synthetic d:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

.field final synthetic e:Lcom/anythink/rewardvideo/a/a;


# direct methods
.method constructor <init>(Lcom/anythink/rewardvideo/a/a;Lcom/anythink/core/common/d/b;Ljava/lang/String;Landroid/app/Activity;Lcom/anythink/rewardvideo/api/ATRewardVideoListener;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/anythink/rewardvideo/a/a$1;->e:Lcom/anythink/rewardvideo/a/a;

    iput-object p2, p0, Lcom/anythink/rewardvideo/a/a$1;->a:Lcom/anythink/core/common/d/b;

    iput-object p3, p0, Lcom/anythink/rewardvideo/a/a$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/anythink/rewardvideo/a/a$1;->c:Landroid/app/Activity;

    iput-object p5, p0, Lcom/anythink/rewardvideo/a/a$1;->d:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .line 87
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v3

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    if-eqz v3, :cond_0

    .line 91
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->e:Lcom/anythink/rewardvideo/a/a;

    invoke-static {v0}, Lcom/anythink/rewardvideo/a/a;->a(Lcom/anythink/rewardvideo/a/a;)Ljava/lang/String;

    move-result-object v0

    .line 1429
    iput-object v0, v3, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->b:Ljava/lang/String;

    .line 2269
    iput-object v0, v3, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    .line 93
    invoke-virtual {v3}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v6}, Lcom/anythink/core/common/g/g;->a(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/anythink/core/common/d/d;->f(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->e:Lcom/anythink/rewardvideo/a/a;

    invoke-static {v0}, Lcom/anythink/rewardvideo/a/a;->b(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/anythink/core/common/g/n;->a(Landroid/content/Context;Lcom/anythink/core/common/d/d;)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->e:Lcom/anythink/rewardvideo/a/a;

    invoke-static {v0}, Lcom/anythink/rewardvideo/a/a;->c(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0xd

    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    invoke-virtual/range {v1 .. v6}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V

    .line 101
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/rewardvideo/a/a$1;->e:Lcom/anythink/rewardvideo/a/a;

    invoke-static {v1}, Lcom/anythink/rewardvideo/a/a;->d(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/rewardvideo/a/a$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Lcom/anythink/core/common/d/b;)V

    .line 104
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/a$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    check-cast v0, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;

    .line 105
    iget-object v1, p0, Lcom/anythink/rewardvideo/a/a$1;->c:Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 106
    invoke-virtual {v0, v1}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;->refreshActivityContext(Landroid/app/Activity;)V

    .line 109
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    new-instance v2, Lcom/anythink/rewardvideo/a/a$1$1;

    invoke-direct {v2, p0, v0}, Lcom/anythink/rewardvideo/a/a$1$1;-><init>(Lcom/anythink/rewardvideo/a/a$1;Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;)V

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method
