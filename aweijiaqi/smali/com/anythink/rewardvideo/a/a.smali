.class public Lcom/anythink/rewardvideo/a/a;
.super Lcom/anythink/core/common/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/d<",
        "Lcom/anythink/rewardvideo/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field k:Ljava/lang/String;

.field l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    const-class v0, Lcom/anythink/rewardvideo/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/rewardvideo/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/anythink/core/common/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/anythink/rewardvideo/a/d;)Lcom/anythink/core/common/f;
    .locals 3

    .line 142
    new-instance v0, Lcom/anythink/rewardvideo/a/b;

    iget-object v1, p1, Lcom/anythink/rewardvideo/a/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/rewardvideo/a/b;-><init>(Landroid/content/Context;)V

    .line 143
    iget-object v1, p0, Lcom/anythink/rewardvideo/a/a;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/rewardvideo/a/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/rewardvideo/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v1, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    .line 1033
    iput-object v1, v0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    .line 145
    iget-boolean p1, p1, Lcom/anythink/rewardvideo/a/d;->e:Z

    invoke-virtual {v0, p1}, Lcom/anythink/rewardvideo/a/b;->a(Z)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/rewardvideo/a/a;
    .locals 2

    .line 47
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    instance-of v1, v0, Lcom/anythink/rewardvideo/a/a;

    if-nez v1, :cond_1

    .line 49
    :cond_0
    new-instance v0, Lcom/anythink/rewardvideo/a/a;

    invoke-direct {v0, p0, p1}, Lcom/anythink/rewardvideo/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;Lcom/anythink/core/common/d;)V

    .line 52
    :cond_1
    check-cast v0, Lcom/anythink/rewardvideo/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/anythink/rewardvideo/a/a;)Ljava/lang/String;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/a;->g:Ljava/lang/String;

    return-object p0
.end method

.method private static a(Lcom/anythink/rewardvideo/a/d;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_0

    .line 159
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    invoke-interface {p0, p1}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdFailed(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/anythink/rewardvideo/a/a;->k:Ljava/lang/String;

    .line 165
    iput-object p2, p0, Lcom/anythink/rewardvideo/a/a;->l:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/a;->b:Landroid/content/Context;

    return-object p0
.end method

.method private static b(Lcom/anythink/rewardvideo/a/d;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_0

    .line 152
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    invoke-interface {p0}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdLoaded()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/a;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/rewardvideo/a/a;)Landroid/content/Context;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/anythink/rewardvideo/a/a;->b:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/app/Activity;Ljava/lang/String;Lcom/anythink/rewardvideo/api/ATRewardVideoListener;)V
    .locals 8

    monitor-enter p0

    const/4 v0, 0x1

    .line 61
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/anythink/rewardvideo/a/a;->a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;

    move-result-object v3

    if-nez v3, :cond_1

    const-string p1, "4001"

    const-string p2, ""

    const-string v0, "No Cache."

    .line 63
    invoke-static {p1, p2, v0}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 66
    invoke-static {p2}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object p2

    invoke-interface {p3, p1, p2}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdPlayFailed(Lcom/anythink/core/api/AdError;Lcom/anythink/core/api/ATAdInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    if-eqz v3, :cond_2

    .line 73
    :try_start_1
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;

    if-eqz v1, :cond_2

    .line 74
    invoke-virtual {p0, v3}, Lcom/anythink/rewardvideo/a/a;->a(Lcom/anythink/core/common/d/b;)V

    .line 78
    invoke-virtual {p0}, Lcom/anythink/rewardvideo/a/a;->d()V

    .line 81
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->e()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {v3, v1}, Lcom/anythink/core/common/d/b;->a(I)V

    .line 84
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v7, Lcom/anythink/rewardvideo/a/a$1;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p2

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/anythink/rewardvideo/a/a$1;-><init>(Lcom/anythink/rewardvideo/a/a;Lcom/anythink/core/common/d/b;Ljava/lang/String;Landroid/app/Activity;Lcom/anythink/rewardvideo/api/ATRewardVideoListener;)V

    invoke-virtual {v0, v7}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Landroid/content/Context;ZLcom/anythink/rewardvideo/api/ATRewardVideoListener;)V
    .locals 1

    .line 129
    new-instance v0, Lcom/anythink/rewardvideo/a/d;

    invoke-direct {v0}, Lcom/anythink/rewardvideo/a/d;-><init>()V

    .line 130
    iput-boolean p2, v0, Lcom/anythink/rewardvideo/a/d;->e:Z

    .line 131
    iget-object p2, p0, Lcom/anythink/rewardvideo/a/a;->k:Ljava/lang/String;

    iput-object p2, v0, Lcom/anythink/rewardvideo/a/d;->c:Ljava/lang/String;

    .line 132
    iget-object p2, p0, Lcom/anythink/rewardvideo/a/a;->l:Ljava/lang/String;

    iput-object p2, v0, Lcom/anythink/rewardvideo/a/d;->g:Ljava/lang/String;

    .line 133
    iput-object p3, v0, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    .line 134
    iput-object p1, v0, Lcom/anythink/rewardvideo/a/d;->b:Landroid/content/Context;

    .line 136
    iget-object p1, p0, Lcom/anythink/rewardvideo/a/a;->b:Landroid/content/Context;

    iget-object p2, p0, Lcom/anythink/rewardvideo/a/a;->c:Ljava/lang/String;

    const-string p3, "1"

    invoke-super {p0, p1, p3, p2, v0}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;)V
    .locals 1

    .line 36
    check-cast p1, Lcom/anythink/rewardvideo/a/d;

    .line 2151
    iget-object v0, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_0

    .line 2152
    iget-object p1, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    invoke-interface {p1}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdLoaded()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 36
    check-cast p1, Lcom/anythink/rewardvideo/a/d;

    .line 1158
    iget-object v0, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_0

    .line 1159
    iget-object p1, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    invoke-interface {p1, p2}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdFailed(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method public final synthetic b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;
    .locals 3

    .line 36
    check-cast p1, Lcom/anythink/rewardvideo/a/d;

    .line 3142
    new-instance v0, Lcom/anythink/rewardvideo/a/b;

    iget-object v1, p1, Lcom/anythink/rewardvideo/a/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/rewardvideo/a/b;-><init>(Landroid/content/Context;)V

    .line 3143
    iget-object v1, p0, Lcom/anythink/rewardvideo/a/a;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/rewardvideo/a/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/rewardvideo/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3144
    iget-object v1, p1, Lcom/anythink/rewardvideo/a/d;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    .line 4033
    iput-object v1, v0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    .line 3145
    iget-boolean p1, p1, Lcom/anythink/rewardvideo/a/d;->e:Z

    invoke-virtual {v0, p1}, Lcom/anythink/rewardvideo/a/b;->a(Z)V

    return-object v0
.end method
