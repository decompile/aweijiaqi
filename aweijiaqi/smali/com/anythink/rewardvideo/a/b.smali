.class public final Lcom/anythink/rewardvideo/a/b;
.super Lcom/anythink/core/common/f;


# instance fields
.field a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/anythink/rewardvideo/api/ATRewardVideoListener;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/anythink/rewardvideo/a/b;->s:Z

    if-eqz v0, :cond_0

    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_1

    .line 43
    invoke-interface {v0}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdLoaded()V

    :cond_1
    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/anythink/rewardvideo/a/b;->s:Z

    if-eqz v0, :cond_0

    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    if-eqz v0, :cond_1

    .line 55
    invoke-interface {v0, p1}, Lcom/anythink/rewardvideo/api/ATRewardVideoListener;->onRewardedVideoAdFailed(Lcom/anythink/core/api/AdError;)V

    :cond_1
    const/4 p1, 0x0

    .line 58
    iput-object p1, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/anythink/rewardvideo/a/b;->a:Lcom/anythink/rewardvideo/api/ATRewardVideoListener;

    return-void
.end method
