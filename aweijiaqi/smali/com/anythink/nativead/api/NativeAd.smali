.class public Lcom/anythink/nativead/api/NativeAd;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/nativead/api/NativeAd$ImpressionEventListener;,
        Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;,
        Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;
    }
.end annotation


# instance fields
.field private hasSetShowTkDetail:Z

.field private mAdCacheInfo:Lcom/anythink/core/common/d/b;

.field private mAdRender:Lcom/anythink/nativead/api/ATNativeAdRenderer;

.field protected mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

.field mConfirmListener:Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;

.field private mContext:Landroid/content/Context;

.field mDefaultCloseViewListener:Landroid/view/View$OnClickListener;

.field private mDislikeListener:Lcom/anythink/nativead/api/ATNativeDislikeListener;

.field public mDownLoadProgressListener:Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;

.field private mIsDestroyed:Z

.field private mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

.field mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

.field private mPlacementId:Ljava/lang/String;

.field private mRecordedImpression:Z

.field private mRecordedShow:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    new-instance v0, Lcom/anythink/nativead/api/NativeAd$3;

    invoke-direct {v0, p0}, Lcom/anythink/nativead/api/NativeAd$3;-><init>(Lcom/anythink/nativead/api/NativeAd;)V

    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mDefaultCloseViewListener:Landroid/view/View$OnClickListener;

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mPlacementId:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    .line 65
    invoke-virtual {p3}, Lcom/anythink/core/common/d/b;->h()Lcom/anythink/core/api/BaseAd;

    move-result-object p1

    check-cast p1, Lcom/anythink/nativead/unitgroup/a;

    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    .line 66
    new-instance p2, Lcom/anythink/nativead/api/NativeAd$1;

    invoke-direct {p2, p0}, Lcom/anythink/nativead/api/NativeAd$1;-><init>(Lcom/anythink/nativead/api/NativeAd;)V

    invoke-virtual {p1, p2}, Lcom/anythink/nativead/unitgroup/a;->setNativeEventListener(Lcom/anythink/nativead/unitgroup/a$a;)V

    return-void
.end method

.method static synthetic access$000(Lcom/anythink/nativead/api/NativeAd;)Z
    .locals 0

    .line 40
    iget-boolean p0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    return p0
.end method

.method static synthetic access$100(Lcom/anythink/nativead/api/NativeAd;)Lcom/anythink/core/common/d/b;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    return-object p0
.end method

.method static synthetic access$200(Lcom/anythink/nativead/api/NativeAd;Lcom/anythink/core/common/d/d;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/anythink/nativead/api/NativeAd;->fillShowTrackingInfo(Lcom/anythink/core/common/d/d;)V

    return-void
.end method

.method static synthetic access$300(Lcom/anythink/nativead/api/NativeAd;)Landroid/content/Context;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$400(Lcom/anythink/nativead/api/NativeAd;)Lcom/anythink/nativead/api/ATNativeEventListener;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    return-object p0
.end method

.method private bindListener()V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    instance-of v1, v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    if-eqz v1, :cond_1

    .line 231
    check-cast v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 233
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->checkHasCloseViewListener()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 237
    :cond_0
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 239
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCloseView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mDefaultCloseViewListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method private declared-synchronized fillShowTrackingInfo(Lcom/anythink/core/common/d/d;)V
    .locals 4

    monitor-enter p0

    .line 307
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    if-eqz p1, :cond_0

    .line 309
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lcom/anythink/core/common/g/g;->a(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/anythink/core/common/d/d;->f(Ljava/lang/String;)V

    .line 313
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->hasSetShowTkDetail:Z

    if-nez v0, :cond_1

    .line 314
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 315
    iput-boolean v1, p0, Lcom/anythink/nativead/api/NativeAd;->hasSetShowTkDetail:Z

    if-eqz p1, :cond_1

    .line 1429
    iput-object v0, p1, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    .line 319
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/anythink/core/common/g/n;->a(Landroid/content/Context;Lcom/anythink/core/common/d/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private renderViewToWindow(Landroid/view/View;)V
    .locals 5

    .line 158
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mPlacementId:Ljava/lang/String;

    sget-object v1, Lcom/anythink/core/common/b/e$e;->l:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->p:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->h:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v0, v1, v2, v3, v4}, Lcom/anythink/core/api/ATSDK;->apiLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->getCustomAdContainer()Landroid/view/ViewGroup;

    move-result-object v0

    .line 172
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    if-nez v0, :cond_1

    move-object v0, p1

    .line 181
    :cond_1
    iget-object v2, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    new-instance v3, Lcom/anythink/nativead/api/NativeAd$2;

    invoke-direct {v3, p0}, Lcom/anythink/nativead/api/NativeAd$2;-><init>(Lcom/anythink/nativead/api/NativeAd;)V

    invoke-virtual {v2, v1, v0, v3}, Lcom/anythink/nativead/api/ATNativeAdView;->renderView(ILandroid/view/View;Lcom/anythink/nativead/api/NativeAd$ImpressionEventListener;)V

    .line 188
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mAdRender:Lcom/anythink/nativead/api/ATNativeAdRenderer;

    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-interface {v0, p1, v1}, Lcom/anythink/nativead/api/ATNativeAdRenderer;->renderAdView(Landroid/view/View;Lcom/anythink/nativead/unitgroup/a;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized clear(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 2

    monitor-enter p0

    .line 278
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 279
    monitor-exit p0

    return-void

    .line 281
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/api/ATNativeAdView;->clearImpressionListener(I)V

    const/4 v0, 0x0

    .line 283
    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0, p1}, Lcom/anythink/nativead/unitgroup/a;->clear(Landroid/view/View;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized destory()V
    .locals 1

    monitor-enter p0

    .line 290
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 291
    monitor-exit p0

    return-void

    .line 293
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {p0, v0}, Lcom/anythink/nativead/api/NativeAd;->clear(Lcom/anythink/nativead/api/ATNativeAdView;)V

    const/4 v0, 0x1

    .line 294
    iput-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    const/4 v0, 0x0

    .line 295
    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    .line 296
    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mDislikeListener:Lcom/anythink/nativead/api/ATNativeDislikeListener;

    .line 297
    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mDefaultCloseViewListener:Landroid/view/View$OnClickListener;

    .line 298
    iput-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    .line 300
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->destroy()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAdInfo()Lcom/anythink/core/api/ATAdInfo;
    .locals 1

    .line 569
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-static {v0}, Lcom/anythink/core/api/ATAdInfo;->fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    return-object v0
.end method

.method public getAdInteractionType()I
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    if-eqz v1, :cond_0

    .line 152
    check-cast v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getNativeAdInteractionType()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method declared-synchronized handleAdDislikeButtonClick(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 2

    monitor-enter p0

    .line 474
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 475
    monitor-exit p0

    return-void

    .line 479
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mDislikeListener:Lcom/anythink/nativead/api/ATNativeDislikeListener;

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mDislikeListener:Lcom/anythink/nativead/api/ATNativeDislikeListener;

    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/anythink/nativead/api/ATNativeDislikeListener;->onAdCloseButtonClick(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/core/api/ATAdInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleClick(Lcom/anythink/nativead/api/ATNativeAdView;Landroid/view/View;)V
    .locals 3

    monitor-enter p0

    .line 437
    :try_start_0
    iget-boolean p2, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    .line 438
    monitor-exit p0

    return-void

    .line 441
    :cond_0
    :try_start_1
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz p2, :cond_1

    .line 442
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {p2}, Lcom/anythink/nativead/unitgroup/a;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object p2

    .line 444
    sget-object v0, Lcom/anythink/core/common/b/e$e;->d:Ljava/lang/String;

    sget-object v1, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-static {p2, v0, v1, v2}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 449
    :cond_1
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    if-eqz p2, :cond_2

    .line 450
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-static {v0}, Lcom/anythink/core/api/ATAdInfo;->fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/anythink/nativead/api/ATNativeEventListener;->onAdClicked(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/core/api/ATAdInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleDeeplinkCallback(Lcom/anythink/nativead/api/ATNativeAdView;Z)V
    .locals 2

    monitor-enter p0

    .line 425
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 426
    monitor-exit p0

    return-void

    .line 429
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    instance-of v0, v0, Lcom/anythink/nativead/api/ATNativeEventExListener;

    if-eqz v0, :cond_1

    .line 430
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    check-cast v0, Lcom/anythink/nativead/api/ATNativeEventExListener;

    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcom/anythink/nativead/api/ATNativeEventExListener;->onDeeplinkCallback(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/core/api/ATAdInfo;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 433
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleDownloadConfirm(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V
    .locals 2

    monitor-enter p0

    .line 515
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 516
    monitor-exit p0

    return-void

    .line 519
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mConfirmListener:Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_2

    .line 520
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mConfirmListener:Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    :goto_0
    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2, p3}, Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;->onDownloadConfirm(Landroid/content/Context;Lcom/anythink/core/api/ATAdInfo;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleImpression(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 2

    monitor-enter p0

    .line 381
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mRecordedImpression:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 385
    iput-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mRecordedImpression:Z

    .line 387
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/nativead/api/NativeAd$5;

    invoke-direct {v1, p0, p1}, Lcom/anythink/nativead/api/NativeAd$5;-><init>(Lcom/anythink/nativead/api/NativeAd;Lcom/anythink/nativead/api/ATNativeAdView;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    monitor-exit p0

    return-void

    .line 382
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleVideoEnd(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 3

    monitor-enter p0

    .line 486
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 487
    monitor-exit p0

    return-void

    .line 490
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object v0

    const/16 v1, 0x64

    .line 2488
    iput v1, v0, Lcom/anythink/core/common/d/d;->q:I

    .line 493
    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    if-eqz v0, :cond_2

    .line 497
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    invoke-interface {v0, p1}, Lcom/anythink/nativead/api/ATNativeEventListener;->onAdVideoEnd(Lcom/anythink/nativead/api/ATNativeAdView;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleVideoProgress(Lcom/anythink/nativead/api/ATNativeAdView;I)V
    .locals 1

    monitor-enter p0

    .line 503
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 504
    monitor-exit p0

    return-void

    .line 507
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    invoke-interface {v0, p1, p2}, Lcom/anythink/nativead/api/ATNativeEventListener;->onAdVideoProgress(Lcom/anythink/nativead/api/ATNativeAdView;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 511
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized handleVideoStart(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 3

    monitor-enter p0

    .line 456
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 457
    monitor-exit p0

    return-void

    .line 460
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object v0

    const/4 v1, 0x0

    .line 1488
    iput v1, v0, Lcom/anythink/core/common/d/d;->q:I

    .line 463
    iget-object v1, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    if-eqz v0, :cond_2

    .line 468
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    invoke-interface {v0, p1}, Lcom/anythink/nativead/api/ATNativeEventListener;->onAdVideoStart(Lcom/anythink/nativead/api/ATNativeAdView;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public onPause()V
    .locals 1

    .line 551
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    return-void

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_1

    .line 555
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->onPause()V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 560
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz v0, :cond_1

    .line 564
    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->onResume()V

    :cond_1
    return-void
.end method

.method public declared-synchronized prepare(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 193
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/anythink/nativead/api/NativeAd;->prepare(Lcom/anythink/nativead/api/ATNativeAdView;Landroid/widget/FrameLayout$LayoutParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized prepare(Lcom/anythink/nativead/api/ATNativeAdView;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1

    monitor-enter p0

    .line 197
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 198
    monitor-exit p0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 201
    :try_start_1
    invoke-virtual {p0, p1, v0, p2}, Lcom/anythink/nativead/api/NativeAd;->prepare(Lcom/anythink/nativead/api/ATNativeAdView;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized prepare(Lcom/anythink/nativead/api/ATNativeAdView;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/nativead/api/ATNativeAdView;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    .line 206
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 207
    monitor-exit p0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 210
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/anythink/nativead/unitgroup/a;->prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 212
    invoke-direct {p0}, Lcom/anythink/nativead/api/NativeAd;->bindListener()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 214
    :cond_1
    :try_start_2
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {p2, p1, p3}, Lcom/anythink/nativead/unitgroup/a;->prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 215
    invoke-direct {p0}, Lcom/anythink/nativead/api/NativeAd;->bindListener()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 218
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized recordShow(Lcom/anythink/nativead/api/ATNativeAdView;)V
    .locals 4

    monitor-enter p0

    .line 327
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mRecordedShow:Z

    if-nez v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/a;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object v0

    const/4 v1, 0x1

    .line 329
    iput-boolean v1, p0, Lcom/anythink/nativead/api/NativeAd;->mRecordedShow:Z

    .line 330
    iget-object v2, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    if-eqz v2, :cond_0

    .line 332
    iget-object v2, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    iget-object v3, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->e()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/d/b;->a(I)V

    .line 334
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/nativead/api/NativeAd;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 336
    iget-object v2, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d/b;)V

    .line 337
    invoke-virtual {v1}, Lcom/anythink/core/common/d;->d()V

    .line 342
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v1

    new-instance v2, Lcom/anythink/nativead/api/NativeAd$4;

    invoke-direct {v2, p0, v0}, Lcom/anythink/nativead/api/NativeAd$4;-><init>(Lcom/anythink/nativead/api/NativeAd;Lcom/anythink/core/common/d/d;)V

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    .line 369
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mAdCacheInfo:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 370
    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->supportImpressionCallback()Z

    move-result v0

    if-nez v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    instance-of v0, v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    check-cast v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    invoke-virtual {v0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->impressionTrack(Landroid/view/View;)V

    .line 374
    :cond_1
    invoke-virtual {p0, p1}, Lcom/anythink/nativead/api/NativeAd;->handleImpression(Lcom/anythink/nativead/api/ATNativeAdView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized renderAdView(Lcom/anythink/nativead/api/ATNativeAdView;Lcom/anythink/nativead/api/ATNativeAdRenderer;)V
    .locals 1

    monitor-enter p0

    .line 114
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 115
    monitor-exit p0

    return-void

    .line 117
    :cond_0
    :try_start_1
    iput-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mAdRender:Lcom/anythink/nativead/api/ATNativeAdRenderer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_4

    .line 124
    :try_start_2
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    if-eqz p2, :cond_1

    .line 125
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    invoke-virtual {p2, v0}, Lcom/anythink/nativead/unitgroup/a;->clear(Landroid/view/View;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 133
    :catch_0
    :cond_1
    :try_start_3
    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeView:Lcom/anythink/nativead/api/ATNativeAdView;

    .line 134
    iget-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {p1}, Lcom/anythink/nativead/unitgroup/a;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object p1

    .line 135
    iget-object p2, p0, Lcom/anythink/nativead/api/NativeAd;->mAdRender:Lcom/anythink/nativead/api/ATNativeAdRenderer;

    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->B()I

    move-result p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-interface {p2, v0, p1}, Lcom/anythink/nativead/api/ATNativeAdRenderer;->createView(Landroid/content/Context;I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 141
    invoke-direct {p0, p1}, Lcom/anythink/nativead/api/NativeAd;->renderViewToWindow(Landroid/view/View;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    monitor-exit p0

    return-void

    .line 138
    :cond_3
    :try_start_4
    new-instance p1, Ljava/lang/Exception;

    const-string p2, "not set render view!"

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    .line 119
    :cond_4
    new-instance p1, Ljava/lang/Exception;

    const-string p2, "Render cannot be null!"

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setDislikeCallbackListener(Lcom/anythink/nativead/api/ATNativeDislikeListener;)V
    .locals 1

    .line 255
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    return-void

    .line 258
    :cond_0
    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mDislikeListener:Lcom/anythink/nativead/api/ATNativeDislikeListener;

    return-void
.end method

.method public final setDownLoadProgressListener(Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;)V
    .locals 1

    .line 526
    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mDownLoadProgressListener:Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;

    .line 527
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0, p1}, Lcom/anythink/nativead/unitgroup/a;->setDownLoadProgressListener(Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;)V

    return-void
.end method

.method public setDownloadConfirmListener(Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    instance-of v1, v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    if-eqz v1, :cond_1

    .line 267
    check-cast v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->registerDownloadConfirmListener()V

    goto :goto_0

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/anythink/nativead/api/NativeAd;->mBaseNativeAd:Lcom/anythink/nativead/unitgroup/a;

    instance-of v1, v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    if-eqz v1, :cond_1

    .line 271
    check-cast v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->unregeisterDownloadConfirmListener()V

    .line 274
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mConfirmListener:Lcom/anythink/nativead/api/NativeAd$DownloadConfirmListener;

    return-void
.end method

.method public setNativeEventListener(Lcom/anythink/nativead/api/ATNativeEventListener;)V
    .locals 1

    .line 248
    iget-boolean v0, p0, Lcom/anythink/nativead/api/NativeAd;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    return-void

    .line 251
    :cond_0
    iput-object p1, p0, Lcom/anythink/nativead/api/NativeAd;->mNativeEventListener:Lcom/anythink/nativead/api/ATNativeEventListener;

    return-void
.end method
