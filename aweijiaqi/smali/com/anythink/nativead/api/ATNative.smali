.class public Lcom/anythink/nativead/api/ATNative;
.super Ljava/lang/Object;


# instance fields
.field private final TAG:Ljava/lang/String;

.field mAdLoadManager:Lcom/anythink/nativead/a/a;

.field mContext:Landroid/content/Context;

.field mListener:Lcom/anythink/nativead/api/ATNativeNetworkListener;

.field mOpenSetting:Lcom/anythink/nativead/api/ATNativeOpenSetting;

.field mPlacementId:Ljava/lang/String;

.field mSelfListener:Lcom/anythink/nativead/api/ATNativeNetworkListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/nativead/api/ATNativeNetworkListener;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-class v0, Lcom/anythink/nativead/api/ATNative;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/nativead/api/ATNative;->TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/anythink/nativead/api/ATNativeOpenSetting;

    invoke-direct {v0}, Lcom/anythink/nativead/api/ATNativeOpenSetting;-><init>()V

    iput-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mOpenSetting:Lcom/anythink/nativead/api/ATNativeOpenSetting;

    .line 56
    new-instance v0, Lcom/anythink/nativead/api/ATNative$1;

    invoke-direct {v0, p0}, Lcom/anythink/nativead/api/ATNative$1;-><init>(Lcom/anythink/nativead/api/ATNative;)V

    iput-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mSelfListener:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    .line 48
    iput-object p1, p0, Lcom/anythink/nativead/api/ATNative;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/anythink/nativead/api/ATNative;->mListener:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    .line 52
    invoke-static {p1, p2}, Lcom/anythink/nativead/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/nativead/a/a;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    return-void
.end method


# virtual methods
.method public checkAdStatus()Lcom/anythink/core/api/ATAdStatusInfo;
    .locals 6

    .line 133
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 134
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/a/a;->b(Landroid/content/Context;)Lcom/anythink/core/api/ATAdStatusInfo;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->l:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->r:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATAdStatusInfo;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-static {v1, v2, v3, v4, v5}, Lcom/anythink/core/api/ATSDK;->apiLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 136
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->TAG:Ljava/lang/String;

    const-string v1, "SDK init error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v0, Lcom/anythink/core/api/ATAdStatusInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/anythink/core/api/ATAdStatusInfo;-><init>(ZZLcom/anythink/core/api/ATAdInfo;)V

    return-object v0
.end method

.method public getNativeAd()Lcom/anythink/nativead/api/NativeAd;
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/anythink/nativead/a/a;->c(Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    new-instance v1, Lcom/anythink/nativead/api/NativeAd;

    iget-object v2, p0, Lcom/anythink/nativead/api/ATNative;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/anythink/nativead/api/NativeAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNativeAd(Ljava/lang/String;)Lcom/anythink/nativead/api/NativeAd;
    .locals 3

    .line 121
    invoke-static {p1}, Lcom/anythink/core/common/g/g;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    invoke-virtual {v0, p1}, Lcom/anythink/nativead/a/a;->c(Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 126
    new-instance v0, Lcom/anythink/nativead/api/NativeAd;

    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/anythink/nativead/api/NativeAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getOpenSetting()Lcom/anythink/nativead/api/ATNativeOpenSetting;
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    if-eqz v0, :cond_0

    .line 149
    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mOpenSetting:Lcom/anythink/nativead/api/ATNativeOpenSetting;

    iget-object v2, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/nativead/a/a;->a(Lcom/anythink/nativead/api/ATNativeOpenSetting;Ljava/lang/String;)V

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mOpenSetting:Lcom/anythink/nativead/api/ATNativeOpenSetting;

    return-object v0
.end method

.method public makeAdRequest()V
    .locals 5

    .line 90
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    sget-object v1, Lcom/anythink/core/common/b/e$e;->l:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->n:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->h:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v0, v1, v2, v3, v4}, Lcom/anythink/core/api/ATSDK;->apiLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/anythink/nativead/api/ATNative;->mAdLoadManager:Lcom/anythink/nativead/a/a;

    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/nativead/api/ATNative;->mSelfListener:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/nativead/a/a;->a(Landroid/content/Context;Lcom/anythink/nativead/api/ATNativeNetworkListener;)V

    return-void
.end method

.method public setLocalExtra(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 101
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/nativead/api/ATNative;->mPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
