.class public Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtraInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    }
.end annotation


# instance fields
.field adLogoViewId:I

.field calltoActionViewId:I

.field closeView:Landroid/view/View;

.field creativeViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field customViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field descriptionViewId:I

.field iconViewId:I

.field mainImageViewId:I

.field parentViewId:I

.field sourceViewId:I

.field titleViewId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setParentViewId(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Landroid/view/View;)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setCloseView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Ljava/util/List;)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setCustomViews(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setAdLogoViewId(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setCalltoActionViewId(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Ljava/util/List;)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setCreativeViews(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setDescriptionViewId(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setIconViewId(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setTitleViewId(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setMainImageViewId(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V
    .locals 0

    .line 342
    invoke-direct {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->setSourceViewId(I)V

    return-void
.end method

.method private setAdLogoViewId(I)V
    .locals 0

    .line 408
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->adLogoViewId:I

    return-void
.end method

.method private setCalltoActionViewId(I)V
    .locals 0

    .line 416
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->calltoActionViewId:I

    return-void
.end method

.method private setCloseView(Landroid/view/View;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->closeView:Landroid/view/View;

    return-void
.end method

.method private setCreativeViews(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 432
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->creativeViews:Ljava/util/List;

    return-void
.end method

.method private setCustomViews(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 440
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->customViews:Ljava/util/List;

    return-void
.end method

.method private setDescriptionViewId(I)V
    .locals 0

    .line 392
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->descriptionViewId:I

    return-void
.end method

.method private setIconViewId(I)V
    .locals 0

    .line 424
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->iconViewId:I

    return-void
.end method

.method private setMainImageViewId(I)V
    .locals 0

    .line 400
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->mainImageViewId:I

    return-void
.end method

.method private setParentViewId(I)V
    .locals 0

    .line 368
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->parentViewId:I

    return-void
.end method

.method private setSourceViewId(I)V
    .locals 0

    .line 384
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->sourceViewId:I

    return-void
.end method

.method private setTitleViewId(I)V
    .locals 0

    .line 376
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->titleViewId:I

    return-void
.end method


# virtual methods
.method public getAdLogoViewId()I
    .locals 1

    .line 404
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->adLogoViewId:I

    return v0
.end method

.method public getCalltoActionViewId()I
    .locals 1

    .line 412
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->calltoActionViewId:I

    return v0
.end method

.method public getCloseView()Landroid/view/View;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->closeView:Landroid/view/View;

    return-object v0
.end method

.method public getCreativeViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 428
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->creativeViews:Ljava/util/List;

    return-object v0
.end method

.method public getCustomViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 436
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->customViews:Ljava/util/List;

    return-object v0
.end method

.method public getDescriptionViewId()I
    .locals 1

    .line 388
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->descriptionViewId:I

    return v0
.end method

.method public getIconViewId()I
    .locals 1

    .line 420
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->iconViewId:I

    return v0
.end method

.method public getMainImageViewId()I
    .locals 1

    .line 396
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->mainImageViewId:I

    return v0
.end method

.method public getParentViewId()I
    .locals 1

    .line 364
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->parentViewId:I

    return v0
.end method

.method public getSourceViewId()I
    .locals 1

    .line 380
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->sourceViewId:I

    return v0
.end method

.method public getTitleViewId()I
    .locals 1

    .line 372
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->titleViewId:I

    return v0
.end method
