.class public Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field adLogoViewId:I

.field calltoActionViewId:I

.field closeView:Landroid/view/View;

.field creativeViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field customViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field descriptionViewId:I

.field iconViewId:I

.field mainImageViewId:I

.field parentViewId:I

.field sourceViewId:I

.field titleViewId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;
    .locals 2

    .line 513
    new-instance v0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    invoke-direct {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;-><init>()V

    .line 515
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->parentViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$000(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 516
    iget-object v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->closeView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$100(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Landroid/view/View;)V

    .line 517
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->adLogoViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$200(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 518
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->calltoActionViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$300(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 519
    iget-object v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->creativeViews:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$400(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Ljava/util/List;)V

    .line 520
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->descriptionViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$500(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 521
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->iconViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$600(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 522
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->titleViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$700(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 523
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->mainImageViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$800(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 524
    iget v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->sourceViewId:I

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$900(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;I)V

    .line 525
    iget-object v1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->customViews:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->access$1000(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;Ljava/util/List;)V

    return-object v0
.end method

.method public setAdLogoViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 487
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->adLogoViewId:I

    return-object p0
.end method

.method public setCalltoActionViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 492
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->calltoActionViewId:I

    return-object p0
.end method

.method public setCloseView(Landroid/view/View;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 462
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->closeView:Landroid/view/View;

    return-object p0
.end method

.method public setCreativeViewList(Ljava/util/List;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)",
            "Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;"
        }
    .end annotation

    .line 502
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->creativeViews:Ljava/util/List;

    return-object p0
.end method

.method public setCustomViewList(Ljava/util/List;)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)",
            "Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;"
        }
    .end annotation

    .line 507
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->customViews:Ljava/util/List;

    return-object p0
.end method

.method public setDescriptionViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 477
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->descriptionViewId:I

    return-object p0
.end method

.method public setIconViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 497
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->iconViewId:I

    return-object p0
.end method

.method public setMainImageViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 482
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->mainImageViewId:I

    return-object p0
.end method

.method public setParentId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 457
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->parentViewId:I

    return-object p0
.end method

.method public setSourceViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 472
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->sourceViewId:I

    return-object p0
.end method

.method public setTitleViewId(I)Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;
    .locals 0

    .line 467
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo$Builder;->titleViewId:I

    return-object p0
.end method
