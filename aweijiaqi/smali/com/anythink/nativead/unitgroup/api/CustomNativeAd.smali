.class public Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;
.super Lcom/anythink/nativead/unitgroup/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$NativeAdConst;,
        Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;
    }
.end annotation


# static fields
.field public static IS_AUTO_PLAY_KEY:Ljava/lang/String; = "is_auto_play"

.field static final MAX_STAR_RATING:D = 5.0

.field static final MIN_STAR_RATING:D


# instance fields
.field private adLogoView:Landroid/view/View;

.field private extraInfo:Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

.field private mAdChoiceIconUrl:Ljava/lang/String;

.field private mAdFrom:Ljava/lang/String;

.field private mCallToAction:Ljava/lang/String;

.field private mClickDestinationUrl:Ljava/lang/String;

.field private mCloseViewListener:Landroid/view/View$OnClickListener;

.field private mIconImageUrl:Ljava/lang/String;

.field private mImageUrlList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMainImageUrl:Ljava/lang/String;

.field private mNetworkInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mStarRating:Ljava/lang/Double;

.field private mText:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mVideoUrl:Ljava/lang/String;

.field private nInteractionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 54
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/a;-><init>()V

    const-wide/16 v0, 0x0

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mStarRating:Ljava/lang/Double;

    const/4 v0, 0x0

    .line 52
    iput v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->nInteractionType:I

    return-void
.end method


# virtual methods
.method public final bindDislikeListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 286
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCloseViewListener:Landroid/view/View$OnClickListener;

    .line 288
    invoke-virtual {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 290
    invoke-virtual {p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCloseView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCloseViewListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public final checkHasCloseViewListener()Z
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCloseViewListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public clear(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public destroy()V
    .locals 1

    const/4 v0, 0x0

    .line 327
    iput-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCloseViewListener:Landroid/view/View$OnClickListener;

    .line 328
    iput-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->extraInfo:Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    return-void
.end method

.method public final getAdChoiceIconUrl()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mAdChoiceIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getAdFrom()Ljava/lang/String;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mAdFrom:Ljava/lang/String;

    return-object v0
.end method

.method public getAdIconView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAdLogo()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAdLogoView()Landroid/view/View;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->adLogoView:Landroid/view/View;

    return-object v0
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getCallToActionText()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCallToAction:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomAdContainer()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDescriptionText()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->extraInfo:Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    return-object v0
.end method

.method public getIconImageUrl()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mIconImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getImageUrlList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mImageUrlList:Ljava/util/List;

    return-object v0
.end method

.method public getMainImageUrl()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mMainImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getNativeAdInteractionType()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->nInteractionType:I

    return v0
.end method

.method public final getNetworkInfoMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 259
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mNetworkInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public final getStarRating()Ljava/lang/Double;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mStarRating:Ljava/lang/Double;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mVideoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public impressionTrack(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public isNativeExpress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public registerDownloadConfirmListener()V
    .locals 0

    return-void
.end method

.method public final setAdChoiceIconUrl(Ljava/lang/String;)V
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mAdChoiceIconUrl:Ljava/lang/String;

    return-void
.end method

.method public final setAdFrom(Ljava/lang/String;)V
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mAdFrom:Ljava/lang/String;

    return-void
.end method

.method public final setAdLogoView(Landroid/view/View;)V
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->adLogoView:Landroid/view/View;

    return-void
.end method

.method public final setCallToActionText(Ljava/lang/String;)V
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mCallToAction:Ljava/lang/String;

    return-void
.end method

.method public final setDescriptionText(Ljava/lang/String;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mText:Ljava/lang/String;

    return-void
.end method

.method public setExtraInfo(Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->extraInfo:Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    return-void
.end method

.method public final setIconImageUrl(Ljava/lang/String;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mIconImageUrl:Ljava/lang/String;

    return-void
.end method

.method public final setImageUrlList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 235
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mImageUrlList:Ljava/util/List;

    return-void
.end method

.method public final setMainImageUrl(Ljava/lang/String;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mMainImageUrl:Ljava/lang/String;

    return-void
.end method

.method public final setNativeInteractionType(I)V
    .locals 0

    .line 186
    iput p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->nInteractionType:I

    return-void
.end method

.method public final setNetworkInfoMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 255
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mNetworkInfoMap:Ljava/util/Map;

    return-void
.end method

.method public final setStarRating(Ljava/lang/Double;)V
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 215
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mStarRating:Ljava/lang/Double;

    return-void

    .line 216
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-ltz v4, :cond_1

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    cmpg-double v4, v0, v2

    if-gtz v4, :cond_1

    .line 217
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mStarRating:Ljava/lang/Double;

    :cond_1
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public final setVideoUrl(Ljava/lang/String;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->mVideoUrl:Ljava/lang/String;

    return-void
.end method

.method public unregeisterDownloadConfirmListener()V
    .locals 0

    return-void
.end method
