.class public abstract Lcom/anythink/nativead/unitgroup/a;
.super Lcom/anythink/core/api/BaseAd;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/nativead/unitgroup/a$a;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final NETWORK_UNKNOW:I

.field protected mAdSourceType:Ljava/lang/String;

.field protected mAdTrackingInfo:Lcom/anythink/core/common/d/d;

.field public mDownLoadProgressListener:Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;

.field private mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

.field protected mNetworkType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    const-class v0, Lcom/anythink/nativead/unitgroup/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .line 59
    invoke-direct {p0}, Lcom/anythink/core/api/BaseAd;-><init>()V

    const/4 v0, -0x1

    .line 29
    iput v0, p0, Lcom/anythink/nativead/unitgroup/a;->NETWORK_UNKNOW:I

    const-string v1, "0"

    .line 55
    iput-object v1, p0, Lcom/anythink/nativead/unitgroup/a;->mAdSourceType:Ljava/lang/String;

    .line 57
    iput v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNetworkType:I

    return-void
.end method


# virtual methods
.method public abstract bindDislikeListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract clear(Landroid/view/View;)V
.end method

.method public abstract getAdIconView()Landroid/view/View;
.end method

.method public varargs abstract getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
.end method

.method public final getAdType()Ljava/lang/String;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mAdSourceType:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getCustomAdContainer()Landroid/view/ViewGroup;
.end method

.method public final getDetail()Lcom/anythink/core/common/d/d;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mAdTrackingInfo:Lcom/anythink/core/common/d/d;

    return-object v0
.end method

.method public abstract isNativeExpress()Z
.end method

.method public final notifyAdClicked()V
    .locals 2

    .line 135
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdClicked..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 137
    invoke-interface {v0, v1}, Lcom/anythink/nativead/unitgroup/a$a;->onAdClicked(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final notifyAdDislikeClick()V
    .locals 2

    .line 197
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdDislikeClick..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {v0}, Lcom/anythink/nativead/unitgroup/a$a;->onAdDislikeButtonClick()V

    :cond_0
    return-void
.end method

.method public final notifyAdImpression()V
    .locals 2

    .line 145
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdImpression..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 147
    invoke-interface {v0}, Lcom/anythink/nativead/unitgroup/a$a;->onAdImpressed()V

    :cond_0
    return-void
.end method

.method public final notifyAdVideoEnd()V
    .locals 2

    .line 165
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdVideoEnd..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 167
    invoke-interface {v0}, Lcom/anythink/nativead/unitgroup/a$a;->onAdVideoEnd()V

    :cond_0
    return-void
.end method

.method public final notifyAdVideoPlayProgress(I)V
    .locals 2

    .line 185
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdVideoPlayProgress..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 187
    invoke-interface {v0, p1}, Lcom/anythink/nativead/unitgroup/a$a;->onAdVideoProgress(I)V

    :cond_0
    return-void
.end method

.method public final notifyAdVideoStart()V
    .locals 2

    .line 155
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyAdVideoStart..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 157
    invoke-interface {v0}, Lcom/anythink/nativead/unitgroup/a$a;->onAdVideoStart()V

    :cond_0
    return-void
.end method

.method public final notifyDeeplinkCallback(Z)V
    .locals 2

    .line 175
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyDeeplinkCallback..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 177
    invoke-interface {v0, p1}, Lcom/anythink/nativead/unitgroup/a$a;->onDeeplinkCallback(Z)V

    :cond_0
    return-void
.end method

.method public final notifyDownloadConfirm(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V
    .locals 2

    .line 204
    sget-object v0, Lcom/anythink/nativead/unitgroup/a;->TAG:Ljava/lang/String;

    const-string v1, "notifyDownloadConfirm..."

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    if-eqz v0, :cond_0

    .line 206
    invoke-interface {v0, p1, p2, p3}, Lcom/anythink/nativead/unitgroup/a$a;->onDownloadConfirmCallback(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    :cond_0
    return-void
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
.end method

.method public abstract prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation
.end method

.method public final setDownLoadProgressListener(Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/a;->mDownLoadProgressListener:Lcom/anythink/nativead/api/NativeAd$DownLoadProgressListener;

    return-void
.end method

.method public setNativeEventListener(Lcom/anythink/nativead/unitgroup/a$a;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/a;->mNativeEventListener:Lcom/anythink/nativead/unitgroup/a$a;

    return-void
.end method

.method public final setTrackingInfo(Lcom/anythink/core/common/d/d;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/anythink/nativead/unitgroup/a;->mAdTrackingInfo:Lcom/anythink/core/common/d/d;

    return-void
.end method
