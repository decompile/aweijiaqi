.class public Lcom/anythink/nativead/a/a;
.super Lcom/anythink/core/common/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/d<",
        "Lcom/anythink/nativead/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    const-class v0, Lcom/anythink/nativead/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/nativead/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/anythink/core/common/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lcom/anythink/nativead/a/c;)Lcom/anythink/core/common/f;
    .locals 2

    .line 89
    new-instance v0, Lcom/anythink/nativead/a/b;

    iget-object v1, p0, Lcom/anythink/nativead/a/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/nativead/a/b;-><init>(Landroid/content/Context;)V

    .line 90
    iget-object p0, p0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    .line 2057
    iput-object p0, v0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/nativead/a/a;
    .locals 2

    .line 40
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    instance-of v1, v0, Lcom/anythink/nativead/a/a;

    if-nez v1, :cond_1

    .line 42
    :cond_0
    new-instance v0, Lcom/anythink/nativead/a/a;

    invoke-direct {v0, p0, p1}, Lcom/anythink/nativead/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;Lcom/anythink/core/common/d;)V

    .line 45
    :cond_1
    check-cast v0, Lcom/anythink/nativead/a/a;

    return-object v0
.end method

.method private static a(Lcom/anythink/nativead/a/c;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 104
    iget-object p0, p0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    invoke-interface {p0, p1}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoadFail(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method private static b(Lcom/anythink/nativead/a/c;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 97
    iget-object p0, p0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    invoke-interface {p0}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoaded()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/anythink/nativead/api/ATNativeNetworkListener;)V
    .locals 2

    .line 80
    new-instance v0, Lcom/anythink/nativead/a/c;

    invoke-direct {v0}, Lcom/anythink/nativead/a/c;-><init>()V

    .line 81
    iput-object p1, v0, Lcom/anythink/nativead/a/c;->a:Landroid/content/Context;

    .line 82
    iput-object p2, v0, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    .line 83
    iget-object p1, p0, Lcom/anythink/nativead/a/a;->b:Landroid/content/Context;

    iget-object p2, p0, Lcom/anythink/nativead/a/a;->c:Ljava/lang/String;

    const-string v1, "0"

    invoke-super {p0, p1, v1, p2, v0}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;)V
    .locals 1

    .line 32
    check-cast p1, Lcom/anythink/nativead/a/c;

    .line 3096
    iget-object v0, p1, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 3097
    iget-object p1, p1, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    invoke-interface {p1}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoaded()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 32
    check-cast p1, Lcom/anythink/nativead/a/c;

    .line 2103
    iget-object v0, p1, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 2104
    iget-object p1, p1, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    invoke-interface {p1, p2}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoadFail(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/nativead/api/ATNativeOpenSetting;Ljava/lang/String;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/anythink/nativead/a/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 111
    invoke-virtual {p2}, Lcom/anythink/core/c/d;->A()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p1, Lcom/anythink/nativead/api/ATNativeOpenSetting;->isAutoRefresh:Z

    .line 112
    invoke-virtual {p2}, Lcom/anythink/core/c/d;->B()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/anythink/nativead/api/ATNativeOpenSetting;->autoRefreshTime:J

    :cond_1
    return-void
.end method

.method public final synthetic b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;
    .locals 2

    .line 32
    check-cast p1, Lcom/anythink/nativead/a/c;

    .line 4089
    new-instance v0, Lcom/anythink/nativead/a/b;

    iget-object v1, p1, Lcom/anythink/nativead/a/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/nativead/a/b;-><init>(Landroid/content/Context;)V

    .line 4090
    iget-object p1, p1, Lcom/anythink/nativead/a/c;->b:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    .line 5057
    iput-object p1, v0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/anythink/core/common/d/b;
    .locals 3

    .line 56
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/nativead/a/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/nativead/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->h()Lcom/anythink/core/api/BaseAd;

    move-result-object v1

    instance-of v1, v1, Lcom/anythink/nativead/unitgroup/a;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/anythink/nativead/unitgroup/api/CustomNativeAdapter;

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    .line 1269
    iput-object p1, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    .line 61
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->h()Lcom/anythink/core/api/BaseAd;

    move-result-object p1

    check-cast p1, Lcom/anythink/nativead/unitgroup/a;

    .line 62
    invoke-virtual {p1, v1}, Lcom/anythink/nativead/unitgroup/a;->setTrackingInfo(Lcom/anythink/core/common/d/d;)V

    .line 64
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object p1

    iget-object v2, p0, Lcom/anythink/nativead/a/a;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1, v0}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
