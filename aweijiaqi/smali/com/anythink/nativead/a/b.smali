.class public final Lcom/anythink/nativead/a/b;
.super Lcom/anythink/core/common/f;


# instance fields
.field a:Lcom/anythink/nativead/api/ATNativeNetworkListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/anythink/nativead/api/ATNativeNetworkListener;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v0}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoaded()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    if-eqz v0, :cond_0

    .line 71
    invoke-interface {v0, p1}, Lcom/anythink/nativead/api/ATNativeNetworkListener;->onNativeAdLoadFail(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/anythink/nativead/a/b;->a:Lcom/anythink/nativead/api/ATNativeNetworkListener;

    return-void
.end method
