.class public final Lcom/anythink/interstitial/a/d;
.super Lcom/anythink/core/common/f;


# instance fields
.field a:Lcom/anythink/interstitial/api/ATInterstitialListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/anythink/interstitial/api/ATInterstitialListener;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/anythink/interstitial/a/d;->s:Z

    if-eqz v0, :cond_0

    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    if-eqz v0, :cond_1

    .line 43
    invoke-interface {v0}, Lcom/anythink/interstitial/api/ATInterstitialListener;->onInterstitialAdLoaded()V

    :cond_1
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/anythink/interstitial/a/d;->s:Z

    if-eqz v0, :cond_0

    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    if-eqz v0, :cond_1

    .line 54
    invoke-interface {v0, p1}, Lcom/anythink/interstitial/api/ATInterstitialListener;->onInterstitialAdLoadFail(Lcom/anythink/core/api/AdError;)V

    :cond_1
    const/4 p1, 0x0

    .line 56
    iput-object p1, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 61
    iput-object v0, p0, Lcom/anythink/interstitial/a/d;->a:Lcom/anythink/interstitial/api/ATInterstitialListener;

    return-void
.end method
