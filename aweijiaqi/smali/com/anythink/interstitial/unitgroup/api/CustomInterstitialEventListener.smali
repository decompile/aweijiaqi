.class public interface abstract Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onDeeplinkCallback(Z)V
.end method

.method public abstract onInterstitialAdClicked()V
.end method

.method public abstract onInterstitialAdClose()V
.end method

.method public abstract onInterstitialAdShow()V
.end method

.method public abstract onInterstitialAdVideoEnd()V
.end method

.method public abstract onInterstitialAdVideoError(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInterstitialAdVideoStart()V
.end method
