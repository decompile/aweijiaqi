.class final Lcom/anythink/network/toutiao/TTATBannerAdapter$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATBannerAdapter;->a(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/anythink/network/toutiao/TTATBannerAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/util/Map;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .line 190
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    const-string v2, "size"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 198
    :goto_0
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    const-string v3, "layout_type"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 199
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 203
    :goto_1
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    const-string v5, "media_size"

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 204
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->a:Ljava/util/Map;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    const/16 v5, 0x64

    const/4 v6, 0x1

    const/16 v7, 0x258

    if-ne v2, v6, :cond_4

    packed-switch v3, :pswitch_data_0

    :cond_3
    const/4 v1, 0x0

    const/4 v7, 0x0

    goto :goto_4

    :pswitch_0
    const/16 v1, 0x1f4

    goto :goto_4

    :pswitch_1
    const/16 v1, 0x190

    goto :goto_4

    :pswitch_2
    const/16 v1, 0x184

    goto :goto_4

    :pswitch_3
    const/16 v1, 0xc8

    goto :goto_4

    :pswitch_4
    const/16 v1, 0x11e

    goto :goto_4

    :pswitch_5
    const/16 v1, 0xfa

    goto :goto_4

    :pswitch_6
    const/16 v1, 0x96

    goto :goto_4

    :pswitch_7
    const/16 v1, 0x64

    goto :goto_4

    :pswitch_8
    const/16 v1, 0x5a

    goto :goto_4

    .line 253
    :cond_4
    :try_start_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "x"

    .line 254
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 255
    aget-object v3, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 256
    :try_start_1
    aget-object v1, v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v7, v3

    goto :goto_4

    :catch_0
    move-exception v1

    move v7, v3

    goto :goto_3

    :catch_1
    move-exception v1

    const/4 v7, 0x0

    .line 259
    :goto_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v1, 0x0

    :goto_4
    if-eqz v7, :cond_6

    if-nez v1, :cond_5

    goto :goto_5

    :cond_5
    move v5, v1

    goto :goto_6

    :cond_6
    :goto_5
    const/16 v7, 0x280

    .line 269
    :goto_6
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iput v7, v1, Lcom/anythink/network/toutiao/TTATBannerAdapter;->d:I

    .line 270
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iput v5, v1, Lcom/anythink/network/toutiao/TTATBannerAdapter;->e:I

    .line 273
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->z(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->A(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->B(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v1, v1

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    .line 274
    :goto_7
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v3}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->C(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v3}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->D(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v3}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->E(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/banner/api/ATBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    float-to-int v4, v3

    .line 277
    :cond_8
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->c:Ljava/util/Map;

    if-eqz v3, :cond_c

    const-string v8, "key_width"

    .line 278
    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 279
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->c:Ljava/util/Map;

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 281
    :try_start_2
    instance-of v8, v3, Ljava/lang/Integer;

    if-nez v8, :cond_9

    instance-of v8, v3, Ljava/lang/String;

    if-eqz v8, :cond_a

    .line 282
    :cond_9
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v1, v8, Landroid/util/DisplayMetrics;->density:F
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    div-float/2addr v3, v1

    float-to-int v1, v3

    goto :goto_8

    :catch_2
    move-exception v3

    .line 285
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 289
    :cond_a
    :goto_8
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->c:Ljava/util/Map;

    const-string v8, "key_height"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 290
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->c:Ljava/util/Map;

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 292
    :try_start_3
    instance-of v8, v3, Ljava/lang/Integer;

    if-nez v8, :cond_b

    instance-of v8, v3, Ljava/lang/String;

    if-eqz v8, :cond_c

    .line 293
    :cond_b
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-float v3, v3

    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v4, v8, Landroid/util/DisplayMetrics;->density:F
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    div-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v3

    goto :goto_9

    :catch_3
    move-exception v3

    .line 296
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 301
    :cond_c
    :goto_9
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->b:Landroid/content/Context;

    invoke-interface {v0, v3}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v0

    .line 302
    new-instance v3, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iget-object v8, v8, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v3

    .line 303
    invoke-virtual {v3, v7, v5}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 304
    invoke-virtual {v3, v6}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    if-ne v2, v6, :cond_f

    if-gtz v1, :cond_d

    .line 308
    div-int/lit8 v7, v7, 0x2

    int-to-float v1, v7

    goto :goto_a

    :cond_d
    int-to-float v1, v1

    :goto_a
    if-gtz v4, :cond_e

    const/4 v2, 0x0

    goto :goto_b

    :cond_e
    int-to-float v2, v4

    :goto_b
    invoke-virtual {v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 309
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 310
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATBannerAdapter;->i:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadBannerExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    return-void

    .line 312
    :cond_f
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 313
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATBannerAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadBannerAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
