.class public Lcom/anythink/network/toutiao/TTATInterstitialAdapter;
.super Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;

.field d:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

.field e:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

.field f:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

.field g:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

.field h:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;

.field private final i:Ljava/lang/String;

.field private j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

.field private k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

.field private l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->i:Ljava/lang/String;

    const-string v0, ""

    .line 32
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->b:I

    .line 41
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$1;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;

    .line 60
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$2;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$2;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

    .line 86
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$3;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$3;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->e:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    .line 117
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$4;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$4;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    .line 165
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$5;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$5;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    .line 189
    new-instance v0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$6;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$6;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->h:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic C(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic D(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    return-object p0
.end method

.method static synthetic E(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic F(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic G(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic H(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic I(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic J(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic K(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic L(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic M(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Ljava/lang/String;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->i:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic N(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic O(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic P(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic Q(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;F)I
    .locals 1

    .line 1414
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/high16 p0, 0x3f800000    # 1.0f

    :cond_0
    div-float/2addr p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;)Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    return-object p1
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Lcom/bytedance/sdk/openadsdk/TTInteractionAd;)Lcom/bytedance/sdk/openadsdk/TTInteractionAd;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    return-object p1
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;)Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    return-object p1
.end method

.method private a(Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 230
    new-instance v6, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p4

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;)V
    .locals 7

    .line 2230
    new-instance v6, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p4

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static b(Landroid/content/Context;F)I
    .locals 1

    .line 414
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/high16 p0, 0x3f800000    # 1.0f

    :cond_0
    div-float/2addr p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method static synthetic b(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 379
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;->setFullScreenVideoAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;)V

    .line 380
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    if-eqz v0, :cond_1

    .line 384
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTInteractionAd;->setAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;)V

    .line 385
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTInteractionAd;->setDownloadListener(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    .line 386
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_2

    .line 390
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;)V

    .line 391
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->destroy()V

    .line 392
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 395
    :cond_2
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

    .line 396
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;

    .line 397
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    .line 398
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->e:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    .line 399
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    .line 400
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->h:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;

    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 322
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 405
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 410
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "app_id"

    .line 327
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "slot_id"

    .line 328
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a:Ljava/lang/String;

    .line 330
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const-string v0, "is_video"

    .line 337
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 338
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->b:I

    :cond_1
    const/4 v0, 0x0

    const-string v1, "layout_type"

    .line 342
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v5, v0

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    :goto_0
    const-string v0, "size"

    .line 347
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 348
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v0, "1:1"

    :goto_1
    move-object v7, v0

    const-string v0, "personalized_template"

    .line 351
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 355
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    new-instance v8, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;-><init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v8}, Lcom/anythink/network/toutiao/TTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V

    return-void

    .line 331
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 332
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, ""

    const-string p3, "app_id or slot_id is empty!"

    invoke-interface {p1, p2, p3}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 2

    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 300
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTInteractionAd;->setAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;)V

    .line 301
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTInteractionAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTInteractionAd;->showInteractionAd(Landroid/app/Activity;)V

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 305
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;->setFullScreenVideoAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd$FullScreenVideoAdInteractionListener;)V

    .line 306
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->k:Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTFullScreenVideoAd;->showFullScreenVideoAd(Landroid/app/Activity;)V

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 310
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->h:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;)V

    .line 311
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->showInteractionExpressAd(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    .line 315
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method
