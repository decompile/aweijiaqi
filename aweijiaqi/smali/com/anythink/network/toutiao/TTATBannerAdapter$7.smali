.class final Lcom/anythink/network/toutiao/TTATBannerAdapter$7;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/network/toutiao/TTATInitManager$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATBannerAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/anythink/network/toutiao/TTATBannerAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .line 403
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->b:Ljava/util/Map;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->L(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->M(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 4

    .line 407
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->b:Ljava/util/Map;

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->c:Ljava/util/Map;

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a(Lcom/anythink/network/toutiao/TTATBannerAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 409
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->J(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;->d:Lcom/anythink/network/toutiao/TTATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->K(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
