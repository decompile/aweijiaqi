.class public Lcom/anythink/network/toutiao/TTATInitManager;
.super Lcom/anythink/core/api/ATInitMediation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/network/toutiao/TTATInitManager$a;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static a:Lcom/anythink/network/toutiao/TTATInitManager;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Z

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/network/toutiao/TTATInitManager$a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    const-class v0, Lcom/anythink/network/toutiao/TTATInitManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/toutiao/TTATInitManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 40
    invoke-direct {p0}, Lcom/anythink/core/api/ATInitMediation;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->d:Ljava/util/Map;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->h:Ljava/lang/Object;

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->b:Landroid/os/Handler;

    const/4 v0, 0x1

    .line 42
    iput-boolean v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->c:Z

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInitManager;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 1142
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->h:Ljava/lang/Object;

    monitor-enter v0

    .line 1143
    :try_start_0
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 1146
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/network/toutiao/TTATInitManager$a;

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 1149
    invoke-interface {v4}, Lcom/anythink/network/toutiao/TTATInitManager$a;->onSuccess()V

    goto :goto_1

    .line 1151
    :cond_0
    invoke-interface {v4, p2, p3}, Lcom/anythink/network/toutiao/TTATInitManager$a;->onError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1155
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 1157
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1158
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 142
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->h:Ljava/lang/Object;

    monitor-enter v0

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 146
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/network/toutiao/TTATInitManager$a;

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 149
    invoke-interface {v4}, Lcom/anythink/network/toutiao/TTATInitManager$a;->onSuccess()V

    goto :goto_1

    .line 151
    :cond_0
    invoke-interface {v4, p2, p3}, Lcom/anythink/network/toutiao/TTATInitManager$a;->onError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 155
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 157
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 158
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATInitManager;)Z
    .locals 1

    const/4 v0, 0x1

    .line 26
    iput-boolean v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->e:Z

    return v0
.end method

.method public static declared-synchronized getInstance()Lcom/anythink/network/toutiao/TTATInitManager;
    .locals 2

    const-class v0, Lcom/anythink/network/toutiao/TTATInitManager;

    monitor-enter v0

    .line 47
    :try_start_0
    sget-object v1, Lcom/anythink/network/toutiao/TTATInitManager;->a:Lcom/anythink/network/toutiao/TTATInitManager;

    if-nez v1, :cond_0

    .line 48
    new-instance v1, Lcom/anythink/network/toutiao/TTATInitManager;

    invoke-direct {v1}, Lcom/anythink/network/toutiao/TTATInitManager;-><init>()V

    sput-object v1, Lcom/anythink/network/toutiao/TTATInitManager;->a:Lcom/anythink/network/toutiao/TTATInitManager;

    .line 50
    :cond_0
    sget-object v1, Lcom/anythink/network/toutiao/TTATInitManager;->a:Lcom/anythink/network/toutiao/TTATInitManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    const-string v0, "Pangle(Tiktok)"

    return-object v0
.end method

.method public getNetworkSDKClass()Ljava/lang/String;
    .locals 1

    const-string v0, "com.bytedance.sdk.openadsdk.TTAdSdk"

    return-object v0
.end method

.method public getNetworkVersion()Ljava/lang/String;
    .locals 1

    .line 180
    invoke-static {}, Lcom/anythink/network/toutiao/TTATConst;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviderStatus()Ljava/util/List;
    .locals 2

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "com.bytedance.sdk.openadsdk.multipro.TTMultiProvider"

    .line 216
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "com.bytedance.sdk.openadsdk.TTFileProvider"

    .line 217
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public initSDK(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 63
    invoke-virtual {p0, p1, p2, v0}, Lcom/anythink/network/toutiao/TTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V

    return-void
.end method

.method public initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/anythink/network/toutiao/TTATInitManager$a;",
            ")V"
        }
    .end annotation

    .line 68
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->isInitSuccess()Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->e:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->h:Ljava/lang/Object;

    monitor-enter v0

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p3, :cond_1

    .line 79
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_1
    monitor-exit v0

    return-void

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    if-nez v1, :cond_3

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    .line 88
    :cond_3
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 89
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "app_id"

    .line 91
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    if-eqz p3, :cond_4

    .line 94
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_4
    iget-boolean p3, p0, Lcom/anythink/network/toutiao/TTATInitManager;->c:Z

    if-eqz p3, :cond_5

    const/4 p3, 0x5

    new-array p3, p3, [I

    .line 99
    fill-array-data p3, :array_0

    goto :goto_0

    :cond_5
    new-array p3, v2, [I

    const/4 v0, 0x0

    const/4 v1, 0x2

    aput v1, p3, v0

    .line 109
    :goto_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInitManager;->b:Landroid/os/Handler;

    new-instance v1, Lcom/anythink/network/toutiao/TTATInitManager$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/anythink/network/toutiao/TTATInitManager$1;-><init>(Lcom/anythink/network/toutiao/TTATInitManager;Landroid/content/Context;Ljava/lang/String;[I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catchall_0
    move-exception p1

    .line 89
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_6
    :goto_1
    if-eqz p3, :cond_7

    .line 70
    invoke-interface {p3}, Lcom/anythink/network/toutiao/TTATInitManager$a;->onSuccess()V

    :cond_7
    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
    .end array-data
.end method

.method public setIsOpenDirectDownload(Z)V
    .locals 0

    .line 163
    iput-boolean p1, p0, Lcom/anythink/network/toutiao/TTATInitManager;->c:Z

    return-void
.end method
