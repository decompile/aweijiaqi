.class public Lcom/anythink/network/toutiao/TTATBannerAdapter;
.super Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/content/Context;

.field c:Landroid/view/View;

.field d:I

.field e:I

.field f:I

.field g:Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;

.field h:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

.field i:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

.field j:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

.field private final k:Ljava/lang/String;

.field private l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->k:Ljava/lang/String;

    const-string v0, ""

    .line 33
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a:Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$1;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;

    .line 102
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$2;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$2;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->h:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    .line 121
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$3;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$3;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->i:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    .line 155
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$4;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$4;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic C(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic D(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic E(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic F(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic G(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic H(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic I(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic J(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic K(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic L(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic M(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATBannerAdapter;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;)Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    return-object p1
.end method

.method private a(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;)V
    .locals 1

    .line 321
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$6;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$6;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    invoke-interface {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 187
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATBannerAdapter;Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;)V
    .locals 1

    .line 1321
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$6;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATBannerAdapter$6;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;)V

    invoke-interface {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATBannerAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1

    .line 2187
    new-instance v0, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/anythink/network/toutiao/TTATBannerAdapter$5;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/anythink/network/toutiao/TTATBannerAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/toutiao/TTATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    const/4 v0, 0x0

    .line 427
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->c:Landroid/view/View;

    .line 429
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v1, :cond_0

    .line 430
    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;)V

    .line 431
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->destroy()V

    .line 432
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->l:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 435
    :cond_0
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->h:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    .line 436
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$BannerAdListener;

    .line 437
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->j:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    .line 438
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->i:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    .line 439
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->b:Landroid/content/Context;

    return-void
.end method

.method public getBannerView()Landroid/view/View;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->c:Landroid/view/View;

    return-object v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 370
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 449
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "nw_rft"

    const-string v1, "app_id"

    .line 375
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "slot_id"

    .line 376
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a:Ljava/lang/String;

    .line 378
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 385
    :cond_0
    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_2

    .line 386
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_1

    .line 387
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "Context must be activity."

    invoke-interface {p1, v2, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    .line 392
    :cond_2
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->b:Landroid/content/Context;

    const/4 v1, 0x0

    .line 394
    iput v1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->f:I

    .line 396
    :try_start_0
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 397
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 400
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 403
    :cond_3
    :goto_0
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    new-instance v1, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/anythink/network/toutiao/TTATBannerAdapter$7;-><init>(Lcom/anythink/network/toutiao/TTATBannerAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/anythink/network/toutiao/TTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V

    return-void

    .line 379
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 380
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "app_id or slot_id is empty!"

    invoke-interface {p1, v2, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method
