.class final Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 128
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 133
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 134
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 137
    :try_start_0
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "1"

    iget-object v5, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 138
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    int-to-float v2, v2

    invoke-static {v4, v2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->a:Landroid/content/Context;

    int-to-float v3, v3

    invoke-static {v4, v3}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    .line 141
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 145
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->c:Ljava/util/Map;

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    :try_start_1
    const-string v4, "ad_is_support_deep_link"

    .line 147
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 152
    :catch_0
    :try_start_2
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->c:Ljava/util/Map;

    const-string v4, "ad_orientation"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_2

    const/4 v4, 0x2

    if-eq v2, v4, :cond_1

    goto :goto_1

    .line 155
    :cond_1
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_1

    .line 158
    :cond_2
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    nop

    .line 165
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    invoke-static {v2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->u(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 166
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    invoke-static {v2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->v(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setUserID(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 169
    :cond_4
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    invoke-static {v2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->w(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 170
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    invoke-static {v2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->x(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setMediaExtra(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 173
    :cond_5
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 175
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 176
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;->d:Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadRewardVideoAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;)V

    return-void
.end method
