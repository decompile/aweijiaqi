.class public Lcom/anythink/network/toutiao/TTATNativeExpressAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

.field b:Landroid/content/Context;

.field c:Ljava/lang/String;

.field d:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

.field e:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    const-class v0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;ZZ)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->b:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->c:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 38
    invoke-virtual {p0, p4, p5}, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->setAdData(ZZ)V

    .line 1173
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz p1, :cond_0

    .line 1177
    new-instance p2, Lcom/anythink/network/toutiao/TTATNativeExpressAd$3;

    invoke-direct {p2, p0}, Lcom/anythink/network/toutiao/TTATNativeExpressAd$3;-><init>(Lcom/anythink/network/toutiao/TTATNativeExpressAd;)V

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    :cond_0
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .line 25
    sget-object v0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->f:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-nez v0, :cond_0

    return-void

    .line 145
    :cond_0
    new-instance v1, Lcom/anythink/network/toutiao/TTATNativeExpressAd$2;

    invoke-direct {v1, p0}, Lcom/anythink/network/toutiao/TTATNativeExpressAd$2;-><init>(Lcom/anythink/network/toutiao/TTATNativeExpressAd;)V

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->d:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    .line 44
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz p1, :cond_0

    .line 45
    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->render()V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 173
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-nez v0, :cond_0

    return-void

    .line 177
    :cond_0
    new-instance v1, Lcom/anythink/network/toutiao/TTATNativeExpressAd$3;

    invoke-direct {v1, p0}, Lcom/anythink/network/toutiao/TTATNativeExpressAd$3;-><init>(Lcom/anythink/network/toutiao/TTATNativeExpressAd;)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    return-void
.end method


# virtual methods
.method public clear(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public destroy()V
    .locals 2

    .line 244
    sget-object v0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->f:Ljava/lang/String;

    const-string v1, "destroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 245
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->e:Landroid/view/View;

    .line 246
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v1, :cond_0

    .line 247
    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;)V

    .line 248
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->destroy()V

    .line 249
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 251
    :cond_0
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->b:Landroid/content/Context;

    .line 252
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->d:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    return-void
.end method

.method public getAdLogo()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 0

    .line 227
    :try_start_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->e:Landroid/view/View;

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz p1, :cond_0

    .line 228
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getExpressAdView()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->e:Landroid/view/View;

    .line 230
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->e:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public isNativeExpress()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_0

    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_0

    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-direct {p0, p1}, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public setAdData(ZZ)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setCanInterruptVideoPlay(Z)V

    .line 52
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getInteractionType()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->setNativeInteractionType(I)V

    if-eqz p2, :cond_1

    .line 56
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    new-instance p2, Lcom/anythink/network/toutiao/TTATNativeExpressAd$1;

    invoke-direct {p2, p0}, Lcom/anythink/network/toutiao/TTATNativeExpressAd$1;-><init>(Lcom/anythink/network/toutiao/TTATNativeExpressAd;)V

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setVideoAdListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressVideoAdListener;)V

    .line 100
    :cond_1
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getImageMode()I

    move-result p1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_3

    const/4 p2, 0x3

    if-eq p1, p2, :cond_3

    if-eq p1, v0, :cond_3

    const/4 p2, 0x5

    if-eq p1, p2, :cond_2

    const/16 p2, 0xf

    if-eq p1, p2, :cond_2

    const/16 p2, 0x10

    if-eq p1, p2, :cond_3

    goto :goto_1

    :cond_2
    const-string p1, "1"

    .line 103
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->mAdSourceType:Ljava/lang/String;

    return-void

    :cond_3
    const-string p1, "2"

    .line 110
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATNativeExpressAd;->mAdSourceType:Ljava/lang/String;

    :goto_1
    return-void
.end method
