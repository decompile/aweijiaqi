.class final Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/network/toutiao/TTATInitManager$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:I

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->b:Ljava/util/Map;

    iput p4, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->c:I

    iput-object p5, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->P(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->Q(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 5

    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->b:Ljava/util/Map;

    iget v3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->c:I

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 361
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->N(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$8;->f:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    invoke-static {v1}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->O(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
