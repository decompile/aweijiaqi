.class final Lcom/anythink/network/toutiao/TTATInitManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:[I

.field final synthetic d:Lcom/anythink/network/toutiao/TTATInitManager;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATInitManager;Landroid/content/Context;Ljava/lang/String;[I)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->d:Lcom/anythink/network/toutiao/TTATInitManager;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->c:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v0, 0x0

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;-><init>()V

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->b:Ljava/lang/String;

    .line 114
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->appId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 115
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->useTextureView(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->a:Landroid/content/Context;

    .line 116
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->appName(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    .line 117
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->titleBarTheme(I)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->c:[I

    .line 118
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->directDownloadNetworkType([I)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    .line 119
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->supportMultiProcess(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object v2

    .line 120
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->build()Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    move-result-object v2

    new-instance v3, Lcom/anythink/network/toutiao/TTATInitManager$1$1;

    invoke-direct {v3, p0}, Lcom/anythink/network/toutiao/TTATInitManager$1$1;-><init>(Lcom/anythink/network/toutiao/TTATInitManager$1;)V

    .line 113
    invoke-static {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->init(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;Lcom/bytedance/sdk/openadsdk/TTAdSdk$InitCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    .line 135
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInitManager$1;->d:Lcom/anythink/network/toutiao/TTATInitManager;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-static {v2, v0, v3, v1}, Lcom/anythink/network/toutiao/TTATInitManager;->a(Lcom/anythink/network/toutiao/TTATInitManager;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
