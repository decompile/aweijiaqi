.class public Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;
.super Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;


# instance fields
.field a:Ljava/lang/String;

.field b:Z

.field c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

.field d:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

.field private final e:Ljava/lang/String;

.field private f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;-><init>()V

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->e:Ljava/lang/String;

    const-string v0, ""

    .line 29
    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a:Ljava/lang/String;

    .line 35
    new-instance v0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$1;-><init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    .line 67
    new-instance v0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$2;

    invoke-direct {v0, p0}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$2;-><init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;F)I
    .locals 1

    .line 1255
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/high16 p0, 0x3f800000    # 1.0f

    :cond_0
    div-float/2addr p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;)Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    return-object p1
.end method

.method private a(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 125
    new-instance v0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;-><init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1

    .line 2125
    new-instance v0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$3;-><init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0, v0}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->runOnNetworkRequestThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static b(Landroid/content/Context;F)I
    .locals 1

    .line 255
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/high16 p0, 0x3f800000    # 1.0f

    :cond_0
    div-float/2addr p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method static synthetic b(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 236
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->setRewardAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;)V

    .line 237
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    .line 240
    :cond_0
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    .line 241
    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 196
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 251
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/toutiao/TTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "app_id"

    .line 201
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "slot_id"

    .line 202
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a:Ljava/lang/String;

    const-string v1, "personalized_template"

    .line 203
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 205
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 212
    :cond_0
    invoke-static {}, Lcom/anythink/network/toutiao/TTATInitManager;->getInstance()Lcom/anythink/network/toutiao/TTATInitManager;

    move-result-object v0

    new-instance v2, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$4;

    invoke-direct {v2, p0, p1, p3, v1}, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter$4;-><init>(Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v2}, Lcom/anythink/network/toutiao/TTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/toutiao/TTATInitManager$a;)V

    return-void

    .line 206
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_2

    .line 207
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, ""

    const-string p3, "app_id or slot_id is empty!"

    invoke-interface {p1, p2, p3}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 188
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    .line 189
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->d:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->setRewardAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;)V

    .line 190
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATRewardedVideoAdapter;->f:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->showRewardVideoAd(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
