.class final Lcom/anythink/network/toutiao/TTATAdapter$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATAdapter;->a(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:[I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/anythink/network/toutiao/TTATNativeExpressAd;

.field final synthetic d:Lcom/anythink/network/toutiao/TTATAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATAdapter;[ILjava/util/List;Lcom/anythink/network/toutiao/TTATNativeExpressAd;)V
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->a:[I

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->c:Lcom/anythink/network/toutiao/TTATNativeExpressAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClicked(Landroid/view/View;I)V
    .locals 0

    return-void
.end method

.method public final onAdShow(Landroid/view/View;I)V
    .locals 0

    return-void
.end method

.method public final onRenderFail(Landroid/view/View;Ljava/lang/String;I)V
    .locals 2

    .line 378
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->a:[I

    const/4 v0, 0x0

    aget v1, p1, v0

    add-int/lit8 v1, v1, -0x1

    aput v1, p1, v0

    .line 379
    aget p1, p1, v0

    if-nez p1, :cond_1

    .line 380
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_0

    .line 381
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1}, Lcom/anythink/network/toutiao/TTATAdapter;->z(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 382
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1}, Lcom/anythink/network/toutiao/TTATAdapter;->A(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 385
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1}, Lcom/anythink/network/toutiao/TTATAdapter;->B(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 386
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 387
    iget-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 388
    iget-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p2}, Lcom/anythink/network/toutiao/TTATAdapter;->C(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method

.method public final onRenderSuccess(Landroid/view/View;FF)V
    .locals 0

    .line 396
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    iget-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->c:Lcom/anythink/network/toutiao/TTATNativeExpressAd;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->a:[I

    const/4 p2, 0x0

    aget p3, p1, p2

    add-int/lit8 p3, p3, -0x1

    aput p3, p1, p2

    .line 398
    aget p1, p1, p2

    if-nez p1, :cond_0

    .line 399
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1}, Lcom/anythink/network/toutiao/TTATAdapter;->D(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 400
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 401
    iget-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->b:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 402
    iget-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$3;->d:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p2}, Lcom/anythink/network/toutiao/TTATAdapter;->E(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method
