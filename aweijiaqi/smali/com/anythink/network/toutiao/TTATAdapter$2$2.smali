.class final Lcom/anythink/network/toutiao/TTATAdapter$2$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATAdapter$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/anythink/network/toutiao/TTATAdapter$2;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATAdapter$2;Z)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iput-boolean p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(ILjava/lang/String;)V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v0, v0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->f(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v0, v0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->g(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onNativeExpressAdLoad(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;",
            ">;)V"
        }
    .end annotation

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 230
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 231
    new-instance v1, Lcom/anythink/network/toutiao/TTATNativeExpressAd;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v3, v2, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v4, v2, Lcom/anythink/network/toutiao/TTATAdapter;->a:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->a:Z

    const/4 v7, 0x1

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/anythink/network/toutiao/TTATNativeExpressAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;ZZ)V

    .line 232
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 235
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$2;->b:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object p1, p1, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1, v0}, Lcom/anythink/network/toutiao/TTATAdapter;->a(Lcom/anythink/network/toutiao/TTATAdapter;Ljava/util/List;)V

    return-void
.end method
