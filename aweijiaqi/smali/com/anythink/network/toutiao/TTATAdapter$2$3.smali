.class final Lcom/anythink/network/toutiao/TTATAdapter$2$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATAdapter$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:I

.field final synthetic d:Lcom/anythink/network/toutiao/TTATAdapter$2;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATAdapter$2;ZLandroid/graphics/Bitmap;I)V
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iput-boolean p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->a:Z

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->b:Landroid/graphics/Bitmap;

    iput p4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(ILjava/lang/String;)V
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v0, v0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->h(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v0, v0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->i(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onFeedAdLoad(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/TTFeedAd;",
            ">;)V"
        }
    .end annotation

    .line 256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 257
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/bytedance/sdk/openadsdk/TTFeedAd;

    .line 258
    new-instance v1, Lcom/anythink/network/toutiao/TTATNativeAd;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v3, v2, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v4, v2, Lcom/anythink/network/toutiao/TTATAdapter;->a:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->a:Z

    iget-object v7, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->b:Landroid/graphics/Bitmap;

    iget v8, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->c:I

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/anythink/network/toutiao/TTATNativeAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/TTNativeAd;ZLandroid/graphics/Bitmap;I)V

    .line 259
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object p1, p1, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {p1}, Lcom/anythink/network/toutiao/TTATAdapter;->j(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 263
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 264
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 265
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2$3;->d:Lcom/anythink/network/toutiao/TTATAdapter$2;

    iget-object v0, v0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->k(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method
