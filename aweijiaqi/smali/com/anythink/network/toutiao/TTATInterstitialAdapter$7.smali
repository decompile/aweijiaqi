.class final Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a(Landroid/content/Context;Ljava/util/Map;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;Ljava/util/Map;Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->a:Ljava/util/Map;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->c:Ljava/lang/String;

    iput p5, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .line 233
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    const/4 v1, 0x0

    .line 238
    :try_start_0
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->a:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 239
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->a:Ljava/util/Map;

    const-string v3, "key_width"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    const/4 v2, 0x0

    .line 245
    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->a:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 246
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->a:Ljava/util/Map;

    const-string v4, "key_height"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    :cond_1
    const/4 v3, 0x0

    .line 251
    :goto_1
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v0

    .line 252
    new-instance v4, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v5, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget-object v5, v5, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v4

    .line 253
    iget-object v5, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 254
    iget-object v6, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 255
    invoke-virtual {v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    const/4 v7, 0x1

    .line 256
    invoke-virtual {v4, v7}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 258
    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget v8, v8, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->b:I

    if-eqz v8, :cond_4

    .line 261
    :try_start_2
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget v1, v1, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "1"

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 262
    :cond_2
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    int-to-float v2, v5

    invoke-static {v1, v2}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    int-to-float v3, v6

    invoke-static {v2, v3}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->a(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v4, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    .line 265
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 268
    :cond_3
    :goto_2
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 269
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->e:Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadFullScreenVideoAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$FullScreenVideoAdListener;)V

    return-void

    .line 271
    :cond_4
    iget v8, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->d:I

    if-ne v8, v7, :cond_7

    .line 272
    iget-object v7, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    if-gtz v2, :cond_5

    .line 274
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v5, 0x41f00000    # 30.0f

    mul-float v5, v5, v7

    sub-float/2addr v2, v5

    goto :goto_3

    :cond_5
    int-to-float v2, v2

    :goto_3
    div-float/2addr v2, v7

    float-to-int v2, v2

    if-gtz v3, :cond_6

    goto :goto_4

    :cond_6
    int-to-float v1, v3

    div-float/2addr v1, v7

    float-to-int v1, v1

    .line 276
    :goto_4
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    invoke-static {v3}, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->M(Lcom/anythink/network/toutiao/TTATInterstitialAdapter;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "expressWidth: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ", expressHeight: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    int-to-float v2, v2

    int-to-float v1, v1

    .line 278
    invoke-virtual {v4, v2, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 279
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 280
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->g:Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadInteractionExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    return-void

    .line 282
    :cond_7
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    .line 283
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATInterstitialAdapter$7;->e:Lcom/anythink/network/toutiao/TTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/toutiao/TTATInterstitialAdapter;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadInteractionAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$InteractionAdListener;)V

    return-void
.end method
