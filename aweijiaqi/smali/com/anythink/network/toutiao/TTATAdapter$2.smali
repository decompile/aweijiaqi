.class final Lcom/anythink/network/toutiao/TTATAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/toutiao/TTATAdapter;->a(Landroid/content/Context;Ljava/util/Map;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/anythink/network/toutiao/TTATAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/toutiao/TTATAdapter;Landroid/content/Context;Ljava/util/Map;II)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iput-object p2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    iput p4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->c:I

    iput p5, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .line 107
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 110
    iget-object v2, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 114
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v3, :cond_b

    const-string v8, "key_width"

    .line 117
    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    iget-object v3, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v4

    .line 122
    :goto_0
    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const-string v9, "tt_image_height"

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 123
    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    goto :goto_1

    .line 124
    :cond_1
    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const-string v9, "key_height"

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 125
    iget-object v8, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    goto :goto_1

    :cond_2
    move-object v8, v4

    .line 128
    :goto_1
    iget-object v9, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const-string v10, "tt_can_interrupt_video"

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 129
    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const-string v11, "tt_video_play_btn_bitmap"

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .line 130
    iget-object v11, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->b:Ljava/util/Map;

    const-string v12, "tt_video_play_btn_SIZE"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    if-eqz v3, :cond_6

    if-eqz v8, :cond_6

    .line 134
    :try_start_0
    instance-of v12, v3, Ljava/lang/Integer;

    if-nez v12, :cond_3

    instance-of v12, v3, Ljava/lang/String;

    if-eqz v12, :cond_4

    .line 135
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    .line 138
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 141
    :cond_4
    :goto_2
    :try_start_1
    instance-of v3, v8, Ljava/lang/Integer;

    if-nez v3, :cond_5

    instance-of v3, v8, Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 142
    :cond_5
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v3

    .line 145
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 148
    :cond_6
    iget v3, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->c:I

    if-ne v3, v7, :cond_7

    const/16 v1, 0x2b2

    const/16 v2, 0x184

    goto :goto_3

    :cond_7
    if-ne v3, v6, :cond_8

    const/16 v1, 0xe4

    const/16 v2, 0x96

    .line 157
    :cond_8
    :goto_3
    instance-of v3, v9, Ljava/lang/Boolean;

    if-eqz v3, :cond_9

    .line 158
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    goto :goto_4

    :cond_9
    const/4 v3, 0x0

    .line 161
    :goto_4
    instance-of v8, v10, Landroid/graphics/Bitmap;

    if-eqz v8, :cond_a

    .line 162
    check-cast v10, Landroid/graphics/Bitmap;

    move-object v4, v10

    .line 165
    :cond_a
    instance-of v8, v11, Ljava/lang/Integer;

    if-eqz v8, :cond_c

    .line 166
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    goto :goto_5

    :cond_b
    const/4 v3, 0x0

    :cond_c
    const/4 v8, 0x0

    .line 175
    :goto_5
    iget-object v9, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    invoke-interface {v0, v9}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v0

    .line 176
    new-instance v9, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v10, v10, Lcom/anythink/network/toutiao/TTATAdapter;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v9

    if-lez v1, :cond_d

    if-lez v2, :cond_d

    .line 179
    invoke-virtual {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_6

    :cond_d
    const/16 v10, 0x280

    const/16 v11, 0x140

    .line 181
    invoke-virtual {v9, v10, v11}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 183
    :goto_6
    iget v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->d:I

    invoke-virtual {v9, v10}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 184
    invoke-virtual {v9, v7}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 188
    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v10, v10, Lcom/anythink/network/toutiao/TTATAdapter;->c:Ljava/lang/String;

    const-string v11, "0"

    invoke-static {v11, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v10, v10, Lcom/anythink/network/toutiao/TTATAdapter;->b:Ljava/lang/String;

    invoke-static {v11, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 189
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v4}, Lcom/anythink/network/toutiao/TTATAdapter;->c(Lcom/anythink/network/toutiao/TTATAdapter;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "load Native Express Ad"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    int-to-float v1, v1

    invoke-static {v4, v1}, Lcom/anythink/network/toutiao/TTATAdapter;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    int-to-float v2, v2

    invoke-static {v4, v2}, Lcom/anythink/network/toutiao/TTATAdapter;->a(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 192
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$1;

    invoke-direct {v2, p0, v3}, Lcom/anythink/network/toutiao/TTATAdapter$2$1;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;Z)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadNativeExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    return-void

    .line 215
    :cond_e
    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v10, v10, Lcom/anythink/network/toutiao/TTATAdapter;->c:Ljava/lang/String;

    const-string v12, "1"

    invoke-static {v12, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v10, v10, Lcom/anythink/network/toutiao/TTATAdapter;->b:Ljava/lang/String;

    invoke-static {v11, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 216
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v4}, Lcom/anythink/network/toutiao/TTATAdapter;->c(Lcom/anythink/network/toutiao/TTATAdapter;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "load Native Express Video"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    int-to-float v1, v1

    invoke-static {v4, v1}, Lcom/anythink/network/toutiao/TTATAdapter;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    iget-object v4, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->a:Landroid/content/Context;

    int-to-float v2, v2

    invoke-static {v4, v2}, Lcom/anythink/network/toutiao/TTATAdapter;->a(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v9, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 219
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$2;

    invoke-direct {v2, p0, v3}, Lcom/anythink/network/toutiao/TTATAdapter$2$2;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;Z)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadExpressDrawFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    return-void

    .line 243
    :cond_f
    iget-object v1, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    iget-object v1, v1, Lcom/anythink/network/toutiao/TTATAdapter;->c:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v10

    const/4 v13, 0x3

    packed-switch v10, :pswitch_data_0

    goto :goto_7

    :pswitch_0
    const-string v5, "3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v5, 0x3

    goto :goto_8

    :pswitch_1
    const-string v5, "2"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v5, 0x2

    goto :goto_8

    :pswitch_2
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v5, 0x1

    goto :goto_8

    :pswitch_3
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    goto :goto_8

    :cond_10
    :goto_7
    const/4 v5, -0x1

    :goto_8
    if-eqz v5, :cond_15

    if-eq v5, v7, :cond_14

    if-eq v5, v6, :cond_13

    if-eq v5, v13, :cond_12

    .line 349
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->x(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 350
    iget-object v0, p0, Lcom/anythink/network/toutiao/TTATAdapter$2;->e:Lcom/anythink/network/toutiao/TTATAdapter;

    invoke-static {v0}, Lcom/anythink/network/toutiao/TTATAdapter;->y(Lcom/anythink/network/toutiao/TTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    const-string v2, "The Native type is not exit."

    invoke-interface {v0, v1, v2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    return-void

    .line 323
    :cond_12
    invoke-virtual {v9, v6}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setNativeAdType(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 324
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$6;

    invoke-direct {v2, p0, v3, v4, v8}, Lcom/anythink/network/toutiao/TTATAdapter$2$6;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;ZLandroid/graphics/Bitmap;I)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadNativeAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeAdListener;)V

    return-void

    .line 297
    :cond_13
    invoke-virtual {v9, v7}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setNativeAdType(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 298
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$5;

    invoke-direct {v2, p0, v3, v4, v8}, Lcom/anythink/network/toutiao/TTATAdapter$2$5;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;ZLandroid/graphics/Bitmap;I)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadNativeAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeAdListener;)V

    return-void

    .line 271
    :cond_14
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$4;

    invoke-direct {v2, p0, v3, v4, v8}, Lcom/anythink/network/toutiao/TTATAdapter$2$4;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;ZLandroid/graphics/Bitmap;I)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadDrawFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$DrawFeedAdListener;)V

    return-void

    .line 245
    :cond_15
    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    new-instance v2, Lcom/anythink/network/toutiao/TTATAdapter$2$3;

    invoke-direct {v2, p0, v3, v4, v8}, Lcom/anythink/network/toutiao/TTATAdapter$2$3;-><init>(Lcom/anythink/network/toutiao/TTATAdapter$2;ZLandroid/graphics/Bitmap;I)V

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadFeedAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
