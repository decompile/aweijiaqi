.class public Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Lcom/anythink/basead/e/h;

.field b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/basead/e/h;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->b:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    .line 30
    new-instance p1, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd$1;

    invoke-direct {p1, p0}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd$1;-><init>(Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;)V

    invoke-virtual {p2, p1}, Lcom/anythink/basead/e/h;->a(Lcom/anythink/basead/f/a;)V

    .line 52
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->a()Lcom/anythink/core/common/d/h;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/basead/b;->a(Lcom/anythink/core/common/d/h;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setNetworkInfoMap(Ljava/util/Map;)V

    .line 53
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setAdChoiceIconUrl(Ljava/lang/String;)V

    .line 54
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 55
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 56
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 57
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 58
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public clear(Landroid/view/View;)V
    .locals 0

    .line 90
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->h()V

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 98
    invoke-virtual {v0, v1}, Lcom/anythink/basead/e/h;->a(Lcom/anythink/basead/f/a;)V

    .line 99
    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {v0}, Lcom/anythink/basead/e/h;->i()V

    :cond_0
    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .line 71
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->b:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v1, v2}, Lcom/anythink/basead/e/h;->a(Landroid/content/Context;ZZLcom/anythink/basead/ui/MediaAdView$a;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getCustomAdContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lcom/anythink/basead/ui/OwnNativeAdView;

    iget-object v1, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/OwnNativeAdView;-><init>(Landroid/content/Context;)V

    return-object v0

    .line 66
    :cond_0
    invoke-super {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getCustomAdContainer()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    .line 83
    iget-object p2, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p2, :cond_0

    .line 84
    invoke-virtual {p2, p1}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    .line 76
    iget-object p3, p0, Lcom/anythink/network/onlineapi/OnlineApiATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p3, :cond_0

    .line 77
    invoke-virtual {p3, p1, p2}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;Ljava/util/List;)V

    :cond_0
    return-void
.end method
