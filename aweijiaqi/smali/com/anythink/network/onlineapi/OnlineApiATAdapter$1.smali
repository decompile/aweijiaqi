.class final Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/f/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/onlineapi/OnlineApiATAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/onlineapi/OnlineApiATAdapter;Landroid/content/Context;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;

    iput-object p2, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNativeAdLoadError(Lcom/anythink/basead/c/f;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;

    invoke-static {v0}, Lcom/anythink/network/onlineapi/OnlineApiATAdapter;->c(Lcom/anythink/network/onlineapi/OnlineApiATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;

    invoke-static {v0}, Lcom/anythink/network/onlineapi/OnlineApiATAdapter;->d(Lcom/anythink/network/onlineapi/OnlineApiATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/anythink/basead/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/anythink/basead/c/f;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final varargs onNativeAdLoaded([Lcom/anythink/basead/e/h;)V
    .locals 6

    .line 39
    array-length v0, p1

    new-array v0, v0, [Lcom/anythink/network/adx/AdxATNativeAd;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 40
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 41
    new-instance v3, Lcom/anythink/network/adx/AdxATNativeAd;

    iget-object v4, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->a:Landroid/content/Context;

    aget-object v5, p1, v2

    invoke-direct {v3, v4, v5, v1, v1}, Lcom/anythink/network/adx/AdxATNativeAd;-><init>(Landroid/content/Context;Lcom/anythink/basead/e/h;ZZ)V

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;

    invoke-static {p1}, Lcom/anythink/network/onlineapi/OnlineApiATAdapter;->a(Lcom/anythink/network/onlineapi/OnlineApiATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 44
    iget-object p1, p0, Lcom/anythink/network/onlineapi/OnlineApiATAdapter$1;->b:Lcom/anythink/network/onlineapi/OnlineApiATAdapter;

    invoke-static {p1}, Lcom/anythink/network/onlineapi/OnlineApiATAdapter;->b(Lcom/anythink/network/onlineapi/OnlineApiATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method
