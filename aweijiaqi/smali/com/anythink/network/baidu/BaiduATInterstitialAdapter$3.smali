.class final Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;Landroid/content/Context;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->D(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->E(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 4

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 222
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 224
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->B(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->C(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Baidu: init error, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
