.class public Lcom/anythink/network/baidu/BaiduATInitManager;
.super Lcom/anythink/core/api/ATInitMediation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;
    }
.end annotation


# static fields
.field private static a:Lcom/anythink/network/baidu/BaiduATInitManager;


# instance fields
.field private b:Z

.field private c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 27
    invoke-direct {p0}, Lcom/anythink/core/api/ATInitMediation;-><init>()V

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->e:Ljava/lang/Object;

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private a(ZLjava/lang/Throwable;)V
    .locals 5

    .line 120
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 124
    iget-object v4, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 127
    invoke-interface {v4}, Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;->onSuccess()V

    goto :goto_1

    .line 129
    :cond_0
    invoke-interface {v4, p2}, Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;->onError(Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 133
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 135
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 136
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATInitManager;)Z
    .locals 1

    const/4 v0, 0x1

    .line 23
    iput-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/anythink/network/baidu/BaiduATInitManager;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0, v0, v1}, Lcom/anythink/network/baidu/BaiduATInitManager;->a(ZLjava/lang/Throwable;)V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;
    .locals 2

    const-class v0, Lcom/anythink/network/baidu/BaiduATInitManager;

    monitor-enter v0

    .line 32
    :try_start_0
    sget-object v1, Lcom/anythink/network/baidu/BaiduATInitManager;->a:Lcom/anythink/network/baidu/BaiduATInitManager;

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Lcom/anythink/network/baidu/BaiduATInitManager;

    invoke-direct {v1}, Lcom/anythink/network/baidu/BaiduATInitManager;-><init>()V

    sput-object v1, Lcom/anythink/network/baidu/BaiduATInitManager;->a:Lcom/anythink/network/baidu/BaiduATInitManager;

    .line 35
    :cond_0
    sget-object v1, Lcom/anythink/network/baidu/BaiduATInitManager;->a:Lcom/anythink/network/baidu/BaiduATInitManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getActivityStatus()Ljava/util/List;
    .locals 2

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "com.baidu.mobads.sdk.api.AppActivity"

    .line 165
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "com.baidu.mobads.sdk.api.MobRewardVideoActivity"

    .line 166
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    const-string v0, "Baidu"

    return-object v0
.end method

.method public getNetworkSDKClass()Ljava/lang/String;
    .locals 1

    const-string v0, "com.baidu.mobads.sdk.api.BDAdConfig"

    return-object v0
.end method

.method public getNetworkVersion()Ljava/lang/String;
    .locals 1

    .line 154
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATConst;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviderStatus()Ljava/util/List;
    .locals 2

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "com.baidu.mobads.sdk.api.BdFileProvider"

    .line 173
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public declared-synchronized initSDK(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    .line 49
    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    .line 53
    :try_start_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/anythink/network/baidu/BaiduATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;ZLcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized initSDK(Landroid/content/Context;Ljava/util/Map;ZLcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z",
            "Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    .line 59
    :try_start_0
    iput-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->b:Z

    .line 62
    :cond_0
    iget-boolean p3, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->b:Z

    if-eqz p3, :cond_2

    if-eqz p4, :cond_1

    .line 64
    invoke-interface {p4}, Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;->onSuccess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 66
    :cond_1
    monitor-exit p0

    return-void

    .line 69
    :cond_2
    :try_start_1
    iget-object p3, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->e:Ljava/lang/Object;

    monitor-enter p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 71
    :try_start_2
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p4, :cond_3

    .line 73
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    invoke-interface {p1, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_3
    monitor-exit p3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    .line 78
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    if-nez v1, :cond_5

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    .line 82
    :cond_5
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 83
    monitor-exit p3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    const-string p3, "app_id"

    .line 86
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    if-eqz p4, :cond_6

    .line 89
    iget-object p3, p0, Lcom/anythink/network/baidu/BaiduATInitManager;->d:Ljava/util/List;

    invoke-interface {p3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 93
    :cond_6
    :try_start_5
    new-instance p3, Lcom/baidu/mobads/sdk/api/BDAdConfig$Builder;

    invoke-direct {p3}, Lcom/baidu/mobads/sdk/api/BDAdConfig$Builder;-><init>()V

    .line 94
    invoke-virtual {p3, p2}, Lcom/baidu/mobads/sdk/api/BDAdConfig$Builder;->setAppsid(Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/BDAdConfig$Builder;

    move-result-object p2

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/sdk/api/BDAdConfig$Builder;->build(Landroid/content/Context;)Lcom/baidu/mobads/sdk/api/BDAdConfig;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/BDAdConfig;->init()V

    .line 98
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p2, Lcom/anythink/network/baidu/BaiduATInitManager$1;

    invoke-direct {p2, p0}, Lcom/anythink/network/baidu/BaiduATInitManager$1;-><init>(Lcom/anythink/network/baidu/BaiduATInitManager;)V

    const-wide/16 p3, 0x5dc

    invoke-virtual {p1, p2, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 111
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 108
    :try_start_6
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 110
    invoke-direct {p0, v0, p1}, Lcom/anythink/network/baidu/BaiduATInitManager;->a(ZLjava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 112
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    .line 83
    :try_start_7
    monitor-exit p3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setPermissionAppList(Z)V
    .locals 0

    .line 43
    invoke-static {p1}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->setPermissionAppList(Z)V

    return-void
.end method

.method public setPermissionReadDeviceId(Z)V
    .locals 0

    .line 39
    invoke-static {p1}, Lcom/baidu/mobads/sdk/api/MobadsPermissionSettings;->setPermissionReadDeviceID(Z)V

    return-void
.end method
