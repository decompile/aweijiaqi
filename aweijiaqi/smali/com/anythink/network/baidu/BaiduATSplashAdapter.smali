.class public Lcom/anythink/network/baidu/BaiduATSplashAdapter;
.super Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/baidu/mobads/sdk/api/SplashAd;

.field c:Z

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;-><init>()V

    .line 24
    const-class v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->d:Ljava/lang/String;

    const-string v0, ""

    .line 25
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 4

    .line 32
    new-instance v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)V

    .line 72
    new-instance v1, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    invoke-direct {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;-><init>()V

    .line 73
    iget v2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mFetchAdTimeout:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "timeout"

    invoke-virtual {v1, v3, v2}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    const-string v2, "displayDownloadInfo"

    const-string v3, "true"

    .line 74
    invoke-virtual {v1, v2, v3}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    .line 75
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    const-string v2, "use_dialog_frame"

    invoke-virtual {v1, v2, p2}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    .line 76
    new-instance p2, Lcom/baidu/mobads/sdk/api/SplashAd;

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->build()Lcom/baidu/mobads/sdk/api/RequestParameters;

    move-result-object v1

    invoke-direct {p2, p1, v2, v1, v0}, Lcom/baidu/mobads/sdk/api/SplashAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RequestParameters;Lcom/baidu/mobads/sdk/api/SplashAdListener;)V

    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->b:Lcom/baidu/mobads/sdk/api/SplashAd;

    .line 77
    invoke-virtual {p2}, Lcom/baidu/mobads/sdk/api/SplashAd;->load()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATSplashAdapter;Landroid/content/Context;Z)V
    .locals 4

    .line 1032
    new-instance v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)V

    .line 1072
    new-instance v1, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    invoke-direct {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;-><init>()V

    .line 1073
    iget v2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mFetchAdTimeout:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "timeout"

    invoke-virtual {v1, v3, v2}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    const-string v2, "displayDownloadInfo"

    const-string v3, "true"

    .line 1074
    invoke-virtual {v1, v2, v3}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    .line 1075
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    const-string v2, "use_dialog_frame"

    invoke-virtual {v1, v2, p2}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;

    .line 1076
    new-instance p2, Lcom/baidu/mobads/sdk/api/SplashAd;

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/baidu/mobads/sdk/api/RequestParameters$Builder;->build()Lcom/baidu/mobads/sdk/api/RequestParameters;

    move-result-object v1

    invoke-direct {p2, p1, v2, v1, v0}, Lcom/baidu/mobads/sdk/api/SplashAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/RequestParameters;Lcom/baidu/mobads/sdk/api/SplashAdListener;)V

    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->b:Lcom/baidu/mobads/sdk/api/SplashAd;

    .line 1077
    invoke-virtual {p2}, Lcom/baidu/mobads/sdk/api/SplashAd;->load()V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->b:Lcom/baidu/mobads/sdk/api/SplashAd;

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/SplashAd;->destroy()V

    :cond_0
    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 82
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 147
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c:Z

    return v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ad_click_confirm_status"

    const/4 v1, 0x0

    .line 92
    iput-boolean v1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c:Z

    .line 93
    instance-of v2, p1, Landroid/app/Activity;

    const-string v3, ""

    if-nez v2, :cond_1

    .line 94
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_0

    .line 95
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "Baidu: context must be activity"

    invoke-interface {p1, v3, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v2, "app_id"

    .line 100
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "ad_place_id"

    .line 101
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    .line 102
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    new-array v2, v2, [Z

    aput-boolean v1, v2, v1

    if-eqz p3, :cond_3

    .line 111
    :try_start_0
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 112
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p3

    aput-boolean p3, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :catch_0
    :cond_3
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$2;

    invoke-direct {v0, p0, p1, v2}, Lcom/anythink/network/baidu/BaiduATSplashAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATSplashAdapter;Landroid/content/Context;[Z)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V

    return-void

    .line 103
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 104
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, " app_id ,ad_place_id is empty."

    invoke-interface {p1, v3, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public show(Landroid/app/Activity;Landroid/view/ViewGroup;)V
    .locals 0

    .line 152
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->b:Lcom/baidu/mobads/sdk/api/SplashAd;

    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/sdk/api/SplashAd;->show(Landroid/view/ViewGroup;)V

    return-void

    .line 155
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    if-eqz p1, :cond_1

    .line 156
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    invoke-interface {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdDismiss()V

    :cond_1
    return-void
.end method
