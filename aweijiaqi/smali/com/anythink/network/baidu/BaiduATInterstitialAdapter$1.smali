.class final Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClick()V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->d(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClicked()V

    :cond_0
    return-void
.end method

.method public final onAdClose(F)V
    .locals 0

    .line 59
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 60
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->g(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClose()V

    :cond_0
    return-void
.end method

.method public final onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->h(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->i(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "Baidu: "

    invoke-virtual {v1, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAdLoaded()V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->p(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->q(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdDataLoaded()V

    :cond_0
    return-void
.end method

.method public final onAdShow()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdShow()V

    .line 46
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoStart()V

    :cond_0
    return-void
.end method

.method public final onAdSkip(F)V
    .locals 0

    return-void
.end method

.method public final onVideoDownloadFailed()V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->l(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->m(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    const-string v2, "Baidu: onVideoDownloadFailed()"

    invoke-interface {v0, v1, v2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onVideoDownloadSuccess()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->j(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->k(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method

.method public final playCompletion()V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->n(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->o(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoEnd()V

    :cond_0
    return-void
.end method
