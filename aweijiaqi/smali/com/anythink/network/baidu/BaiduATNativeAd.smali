.class public Lcom/anythink/network/baidu/BaiduATNativeAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Lcom/baidu/mobads/sdk/api/XNativeView;

.field private b:Lcom/baidu/mobads/sdk/api/NativeResponse;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->c:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    .line 33
    invoke-virtual {p0, p2}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setData(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATNativeAd;)Lcom/baidu/mobads/sdk/api/NativeResponse;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    return-object p0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 158
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eq p1, v0, :cond_2

    .line 159
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 160
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 161
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 162
    invoke-direct {p0, v1}, Lcom/anythink/network/baidu/BaiduATNativeAd;->a(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 143
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eq p1, v0, :cond_1

    .line 144
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 145
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 146
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 147
    invoke-direct {p0, v1, p2}, Lcom/anythink/network/baidu/BaiduATNativeAd;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public clear(Landroid/view/View;)V
    .locals 1

    .line 172
    invoke-direct {p0, p1}, Lcom/anythink/network/baidu/BaiduATNativeAd;->a(Landroid/view/View;)V

    .line 173
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 174
    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeItem(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    .line 175
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeViewClickListener(Lcom/baidu/mobads/sdk/api/XNativeView$INativeViewClickListener;)V

    .line 176
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    const/4 v0, 0x0

    .line 202
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    .line 204
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeItem(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    .line 206
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeViewClickListener(Lcom/baidu/mobads/sdk/api/XNativeView$INativeViewClickListener;)V

    .line 207
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    .line 209
    :cond_0
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->c:Landroid/content/Context;

    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .line 186
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getMaterialType()Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    if-ne p1, v0, :cond_0

    .line 187
    new-instance p1, Lcom/baidu/mobads/sdk/api/XNativeView;

    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->c:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    .line 188
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeItem(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    .line 189
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    new-instance v0, Lcom/anythink/network/baidu/BaiduATNativeAd$5;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATNativeAd$5;-><init>(Lcom/anythink/network/baidu/BaiduATNativeAd;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/sdk/api/XNativeView;->setNativeViewClickListener(Lcom/baidu/mobads/sdk/api/XNativeView$INativeViewClickListener;)V

    .line 195
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCustomAdContainer()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDownloadAppInfo()Lcom/anythink/network/baidu/BaiduDownloadAppInfo;
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 230
    :cond_0
    new-instance v1, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;

    invoke-direct {v1, v0}, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;-><init>(Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    return-object v1
.end method

.method public handleClick(Landroid/view/View;Z)V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    if-eqz v0, :cond_0

    .line 221
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/sdk/api/NativeResponse;->handleClick(Landroid/view/View;Z)V

    .line 222
    invoke-virtual {p0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->notifyAdClicked()V

    :cond_0
    return-void
.end method

.method public impressionTrack(Landroid/view/View;)V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->b:Lcom/baidu/mobads/sdk/api/NativeResponse;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 215
    invoke-interface {v0, p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->recordImpression(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 66
    :cond_0
    new-instance p2, Lcom/anythink/network/baidu/BaiduATNativeAd$1;

    invoke-direct {p2, p0}, Lcom/anythink/network/baidu/BaiduATNativeAd$1;-><init>(Lcom/anythink/network/baidu/BaiduATNativeAd;)V

    invoke-direct {p0, p1, p2}, Lcom/anythink/network/baidu/BaiduATNativeAd;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 77
    invoke-virtual {p0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 78
    invoke-virtual {p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 79
    invoke-virtual {p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    .line 81
    new-instance v1, Lcom/anythink/network/baidu/BaiduATNativeAd$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/network/baidu/BaiduATNativeAd$2;-><init>(Lcom/anythink/network/baidu/BaiduATNativeAd;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 93
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eqz p1, :cond_3

    .line 94
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/XNativeView;->render()V

    :cond_3
    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 104
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    if-eqz p2, :cond_1

    .line 105
    iget-object p3, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eq p2, p3, :cond_1

    .line 106
    new-instance p3, Lcom/anythink/network/baidu/BaiduATNativeAd$3;

    invoke-direct {p3, p0}, Lcom/anythink/network/baidu/BaiduATNativeAd$3;-><init>(Lcom/anythink/network/baidu/BaiduATNativeAd;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 120
    :cond_2
    invoke-virtual {p0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 121
    invoke-virtual {p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 122
    invoke-virtual {p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    if-eqz p2, :cond_3

    .line 124
    new-instance p3, Lcom/anythink/network/baidu/BaiduATNativeAd$4;

    invoke-direct {p3, p0}, Lcom/anythink/network/baidu/BaiduATNativeAd$4;-><init>(Lcom/anythink/network/baidu/BaiduATNativeAd;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 136
    :cond_4
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->a:Lcom/baidu/mobads/sdk/api/XNativeView;

    if-eqz p1, :cond_5

    .line 137
    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/XNativeView;->render()V

    :cond_5
    return-void
.end method

.method public setData(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 1

    .line 38
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 39
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 40
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getBaiduLogoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setAdChoiceIconUrl(Ljava/lang/String;)V

    .line 41
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 42
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getDesc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 43
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->isNeedDownloadApp()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\u4e0b\u8f7d"

    goto :goto_0

    :cond_0
    const-string v0, "\u67e5\u770b"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    .line 44
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getBrandName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setAdFrom(Ljava/lang/String;)V

    .line 45
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getMultiPicUrls()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setImageUrlList(Ljava/util/List;)V

    .line 46
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->isNeedDownloadApp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/baidu/BaiduATNativeAd;->setNativeInteractionType(I)V

    .line 48
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAdMaterialType()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "1"

    .line 49
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->mAdSourceType:Ljava/lang/String;

    return-void

    :cond_1
    const-string p1, "2"

    .line 51
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATNativeAd;->mAdSourceType:Ljava/lang/String;

    return-void
.end method
