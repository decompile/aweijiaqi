.class final Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATBannerAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;Landroid/content/Context;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->q(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->r(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 4

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a(Lcom/anythink/network/baidu/BaiduATBannerAdapter;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    .line 125
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 127
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->o(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;->b:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->p(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Baidu: init error, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
