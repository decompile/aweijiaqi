.class public Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;
.super Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

.field b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

.field c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    const-class v0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;-><init>()V

    const-string v0, ""

    .line 27
    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic C(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic D(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic E(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .line 33
    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    if-eqz v0, :cond_0

    .line 1041
    new-instance v0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    .line 1105
    new-instance v0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    .line 1106
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->load()V

    return-void

    .line 1110
    :cond_0
    new-instance v0, Lcom/baidu/mobads/sdk/api/InterstitialAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    .line 1111
    new-instance p1, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->setListener(Lcom/baidu/mobads/sdk/api/InterstitialAdListener;)V

    .line 1152
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->loadAd()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;Landroid/content/Context;)V
    .locals 4

    .line 2033
    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    if-eqz v0, :cond_0

    .line 2041
    new-instance v0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    .line 2105
    new-instance v0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    .line 2106
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->load()V

    return-void

    .line 2110
    :cond_0
    new-instance v0, Lcom/baidu/mobads/sdk/api/InterstitialAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    .line 2111
    new-instance p1, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->setListener(Lcom/baidu/mobads/sdk/api/InterstitialAdListener;)V

    .line 2152
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    invoke-virtual {p0}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->loadAd()V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    .line 41
    new-instance v0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    .line 105
    new-instance v0, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    .line 106
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->load()V

    return-void
.end method

.method static synthetic c(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method private c(Landroid/content/Context;)V
    .locals 2

    .line 110
    new-instance v0, Lcom/baidu/mobads/sdk/api/InterstitialAd;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    .line 111
    new-instance p1, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->setListener(Lcom/baidu/mobads/sdk/api/InterstitialAdListener;)V

    .line 152
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->loadAd()V

    return-void
.end method

.method static synthetic d(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 243
    iput-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    .line 244
    iput-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd$FullScreenVideoAdListener;

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    if-eqz v0, :cond_1

    .line 247
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->destroy()V

    .line 248
    iput-object v1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    :cond_1
    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 189
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 259
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 1

    .line 157
    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->isReady()Z

    move-result v0

    return v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->isAdReady()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p3, "app_id"

    .line 194
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    const-string v0, "ad_place_id"

    .line 195
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    .line 197
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string v0, ""

    if-nez p3, :cond_4

    iget-object p3, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->e:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 204
    :cond_0
    instance-of p3, p1, Landroid/app/Activity;

    if-nez p3, :cond_2

    .line 205
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_1

    .line 206
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "Baidu context must be activity."

    invoke-interface {p1, v0, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string p3, "unit_type"

    .line 211
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_3

    .line 213
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "1"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    .line 216
    :cond_3
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object p3

    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    xor-int/lit8 v0, v0, 0x1

    new-instance v1, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;

    invoke-direct {v1, p0, p1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$3;-><init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;Landroid/content/Context;)V

    invoke-virtual {p3, p1, p2, v0, v1}, Lcom/anythink/network/baidu/BaiduATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;ZLcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V

    return-void

    .line 198
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 199
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, " app_id ,ad_place_id is empty."

    invoke-interface {p1, v0, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 1

    .line 172
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->f:Z

    if-eqz v0, :cond_0

    .line 173
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    if-eqz p1, :cond_1

    .line 174
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->b:Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;

    invoke-virtual {p1}, Lcom/baidu/mobads/sdk/api/FullScreenVideoAd;->show()V

    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->a:Lcom/baidu/mobads/sdk/api/InterstitialAd;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/InterstitialAd;->showAd(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception p1

    .line 182
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method
