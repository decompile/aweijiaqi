.class final Lcom/anythink/network/baidu/BaiduATAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/BaiduNativeManager$FeedAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATAdapter;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/anythink/network/baidu/BaiduATAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATAdapter;Landroid/content/Context;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->b:Lcom/anythink/network/baidu/BaiduATAdapter;

    iput-object p2, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLpClosed()V
    .locals 0

    return-void
.end method

.method public final onNativeFail(ILjava/lang/String;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->b:Lcom/anythink/network/baidu/BaiduATAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATAdapter;->c(Lcom/anythink/network/baidu/BaiduATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->b:Lcom/anythink/network/baidu/BaiduATAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATAdapter;->d(Lcom/anythink/network/baidu/BaiduATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onNativeLoad(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobads/sdk/api/NativeResponse;",
            ">;)V"
        }
    .end annotation

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/sdk/api/NativeResponse;

    .line 44
    new-instance v2, Lcom/anythink/network/baidu/BaiduATNativeAd;

    iget-object v3, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/anythink/network/baidu/BaiduATNativeAd;-><init>(Landroid/content/Context;Lcom/baidu/mobads/sdk/api/NativeResponse;)V

    .line 45
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 49
    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    .line 50
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->b:Lcom/anythink/network/baidu/BaiduATAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATAdapter;->a(Lcom/anythink/network/baidu/BaiduATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATAdapter$1;->b:Lcom/anythink/network/baidu/BaiduATAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATAdapter;->b(Lcom/anythink/network/baidu/BaiduATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method

.method public final onNoAd(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final onVideoDownloadFailed()V
    .locals 0

    return-void
.end method

.method public final onVideoDownloadSuccess()V
    .locals 0

    return-void
.end method
