.class public Lcom/anythink/network/baidu/BaiduATBannerAdapter;
.super Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/baidu/mobads/sdk/api/AdView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/anythink/banner/unitgroup/api/CustomBannerAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .line 31
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdView;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/sdk/api/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    .line 32
    new-instance p1, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)V

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/AdView;->setListener(Lcom/baidu/mobads/sdk/api/AdViewListener;)V

    .line 80
    new-instance p1, Lcom/anythink/network/baidu/BaiduATBannerAdapter$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)V

    invoke-virtual {p0, p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/baidu/BaiduATBannerAdapter;Landroid/content/Context;)V
    .locals 2

    .line 1031
    new-instance v0, Lcom/baidu/mobads/sdk/api/AdView;

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/sdk/api/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    .line 1032
    new-instance p1, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;-><init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)V

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/sdk/api/AdView;->setListener(Lcom/baidu/mobads/sdk/api/AdViewListener;)V

    .line 1080
    new-instance p1, Lcom/anythink/network/baidu/BaiduATBannerAdapter$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter$2;-><init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)V

    invoke-virtual {p0, p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic c(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mImpressionEventListener:Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mATBannerView:Lcom/anythink/banner/api/ATBannerView;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 145
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/sdk/api/AdView;->setListener(Lcom/baidu/mobads/sdk/api/AdViewListener;)V

    .line 146
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/sdk/api/AdView;->destroy()V

    .line 147
    iput-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    :cond_0
    return-void
.end method

.method public getBannerView()Landroid/view/View;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    return-object v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 100
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 158
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p3, "app_id"

    .line 106
    invoke-interface {p2, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 107
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v1

    :goto_0
    const-string v0, "ad_place_id"

    .line 109
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a:Ljava/lang/String;

    .line 113
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_3

    iget-object p3, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_1

    .line 119
    :cond_2
    invoke-static {}, Lcom/anythink/network/baidu/BaiduATInitManager;->getInstance()Lcom/anythink/network/baidu/BaiduATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter$3;-><init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;Landroid/content/Context;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/baidu/BaiduATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/baidu/BaiduATInitManager$InitCallback;)V

    return-void

    .line 114
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_4

    .line 115
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "app_id or ad_place_id is empty."

    invoke-interface {p1, v1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method
