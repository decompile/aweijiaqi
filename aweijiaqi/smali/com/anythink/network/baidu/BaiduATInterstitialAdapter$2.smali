.class final Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/InterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->c(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClick(Lcom/baidu/mobads/sdk/api/InterstitialAd;)V
    .locals 0

    .line 131
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->v(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 132
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->w(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClicked()V

    :cond_0
    return-void
.end method

.method public final onAdDismissed()V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->x(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->y(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClose()V

    :cond_0
    return-void
.end method

.method public final onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->z(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->A(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAdPresent()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->t(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->u(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdShow()V

    :cond_0
    return-void
.end method

.method public final onAdReady()V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->r(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter$2;->a:Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;->s(Lcom/anythink/network/baidu/BaiduATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method
