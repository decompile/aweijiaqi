.class final Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/RewardVideoAd$RewardVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClick()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->c(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->d(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;->onRewardedVideoAdPlayClicked()V

    :cond_0
    return-void
.end method

.method public final onAdClose(F)V
    .locals 0

    .line 44
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->e(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 45
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->f(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;->onRewardedVideoAdClosed()V

    :cond_0
    return-void
.end method

.method public final onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->g(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->h(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAdLoaded()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->o(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->p(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdDataLoaded()V

    :cond_0
    return-void
.end method

.method public final onAdShow()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->a(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->b(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;->onRewardedVideoAdPlayStart()V

    :cond_0
    return-void
.end method

.method public final onAdSkip(F)V
    .locals 0

    return-void
.end method

.method public final onVideoDownloadFailed()V
    .locals 0

    return-void
.end method

.method public final onVideoDownloadSuccess()V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->i(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->j(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method

.method public final playCompletion()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->k(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->l(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;->onRewardedVideoAdPlayEnd()V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->m(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;->n(Lcom/anythink/network/baidu/BaiduATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;->onReward()V

    :cond_1
    return-void
.end method
