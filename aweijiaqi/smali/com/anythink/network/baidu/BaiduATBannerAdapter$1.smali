.class final Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/AdViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClick(Lorg/json/JSONObject;)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->e(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->f(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;->onBannerAdClicked()V

    :cond_0
    return-void
.end method

.method public final onAdClose(Lorg/json/JSONObject;)V
    .locals 0

    .line 74
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->k(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->l(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;->onBannerAdClose()V

    :cond_0
    return-void
.end method

.method public final onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->g(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->h(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/api/ATBannerView;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    iget-object v1, v1, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b:Lcom/baidu/mobads/sdk/api/AdView;

    invoke-virtual {v0, v1}, Lcom/anythink/banner/api/ATBannerView;->removeView(Landroid/view/View;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->i(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->j(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final onAdReady(Lcom/baidu/mobads/sdk/api/AdView;)V
    .locals 0

    return-void
.end method

.method public final onAdShow(Lorg/json/JSONObject;)V
    .locals 1

    .line 40
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->a(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 41
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->b(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {p1, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->c(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 44
    iget-object p1, p0, Lcom/anythink/network/baidu/BaiduATBannerAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATBannerAdapter;

    invoke-static {p1}, Lcom/anythink/network/baidu/BaiduATBannerAdapter;->d(Lcom/anythink/network/baidu/BaiduATBannerAdapter;)Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/banner/unitgroup/api/CustomBannerEventListener;->onBannerAdShow()V

    :cond_1
    return-void
.end method

.method public final onAdSwitch()V
    .locals 0

    return-void
.end method
