.class final Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/baidu/mobads/sdk/api/SplashAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onADLoaded()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c:Z

    .line 43
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->d(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method

.method public final onAdClick()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->i(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->j(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdClicked()V

    :cond_0
    return-void
.end method

.method public final onAdDismissed()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->a(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->b(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdDismiss()V

    :cond_0
    return-void
.end method

.method public final onAdFailed(Ljava/lang/String;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->e(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->f(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onAdPresent()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->c:Z

    .line 58
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->g(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/anythink/network/baidu/BaiduATSplashAdapter$1;->a:Lcom/anythink/network/baidu/BaiduATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/baidu/BaiduATSplashAdapter;->h(Lcom/anythink/network/baidu/BaiduATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdShow()V

    :cond_0
    return-void
.end method
