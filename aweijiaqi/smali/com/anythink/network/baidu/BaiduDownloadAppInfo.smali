.class public Lcom/anythink/network/baidu/BaiduDownloadAppInfo;
.super Ljava/lang/Object;


# instance fields
.field public appPrivacyLink:Ljava/lang/String;

.field public appSize:J

.field public appVersion:Ljava/lang/String;

.field public apppermissionLink:Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public publisher:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/sdk/api/NativeResponse;)V
    .locals 2

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getPublisher()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->publisher:Ljava/lang/String;

    .line 22
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->appVersion:Ljava/lang/String;

    .line 23
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAppPrivacyLink()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->appPrivacyLink:Ljava/lang/String;

    .line 24
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAppPermissionLink()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->apppermissionLink:Ljava/lang/String;

    .line 25
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->packageName:Ljava/lang/String;

    .line 26
    invoke-interface {p1}, Lcom/baidu/mobads/sdk/api/NativeResponse;->getAppSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/network/baidu/BaiduDownloadAppInfo;->appSize:J

    return-void
.end method
