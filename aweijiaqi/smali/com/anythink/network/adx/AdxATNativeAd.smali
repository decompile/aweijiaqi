.class public Lcom/anythink/network/adx/AdxATNativeAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Lcom/anythink/basead/e/h;

.field b:Landroid/content/Context;

.field c:Z

.field d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/anythink/basead/e/h;ZZ)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->b:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    .line 33
    new-instance p1, Lcom/anythink/network/adx/AdxATNativeAd$1;

    invoke-direct {p1, p0}, Lcom/anythink/network/adx/AdxATNativeAd$1;-><init>(Lcom/anythink/network/adx/AdxATNativeAd;)V

    invoke-virtual {p2, p1}, Lcom/anythink/basead/e/h;->a(Lcom/anythink/basead/f/a;)V

    .line 55
    iput-boolean p3, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    .line 56
    iput-boolean p4, p0, Lcom/anythink/network/adx/AdxATNativeAd;->d:Z

    if-eqz p3, :cond_0

    return-void

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->a()Lcom/anythink/core/common/d/h;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/basead/b;->a(Lcom/anythink/core/common/d/h;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setNetworkInfoMap(Ljava/util/Map;)V

    .line 63
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setAdChoiceIconUrl(Ljava/lang/String;)V

    .line 64
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 65
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 66
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 67
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 68
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/adx/AdxATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public clear(Landroid/view/View;)V
    .locals 0

    .line 110
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p1, :cond_0

    .line 111
    invoke-virtual {p1}, Lcom/anythink/basead/e/h;->h()V

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 118
    invoke-virtual {v0, v1}, Lcom/anythink/basead/e/h;->a(Lcom/anythink/basead/f/a;)V

    .line 119
    iget-object v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    invoke-virtual {v0}, Lcom/anythink/basead/e/h;->i()V

    :cond_0
    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    .line 78
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    iget-object v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->b:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    iget-boolean v2, p0, Lcom/anythink/network/adx/AdxATNativeAd;->d:Z

    new-instance v3, Lcom/anythink/network/adx/AdxATNativeAd$2;

    invoke-direct {v3, p0}, Lcom/anythink/network/adx/AdxATNativeAd$2;-><init>(Lcom/anythink/network/adx/AdxATNativeAd;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/anythink/basead/e/h;->a(Landroid/content/Context;ZZLcom/anythink/basead/ui/MediaAdView$a;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getCustomAdContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/anythink/basead/ui/OwnNativeAdView;

    iget-object v1, p0, Lcom/anythink/network/adx/AdxATNativeAd;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/basead/ui/OwnNativeAdView;-><init>(Landroid/content/Context;)V

    return-object v0

    .line 91
    :cond_0
    invoke-super {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getCustomAdContainer()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public isNativeExpress()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    return v0
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    .line 103
    iget-boolean p2, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p2, :cond_0

    .line 104
    invoke-virtual {p2, p1}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    .line 96
    iget-boolean p3, p0, Lcom/anythink/network/adx/AdxATNativeAd;->c:Z

    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/anythink/network/adx/AdxATNativeAd;->a:Lcom/anythink/basead/e/h;

    if-eqz p3, :cond_0

    .line 97
    invoke-virtual {p3, p1, p2}, Lcom/anythink/basead/e/h;->a(Landroid/view/View;Ljava/util/List;)V

    :cond_0
    return-void
.end method
