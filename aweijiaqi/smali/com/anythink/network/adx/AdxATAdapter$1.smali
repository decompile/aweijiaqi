.class final Lcom/anythink/network/adx/AdxATAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/basead/f/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/adx/AdxATAdapter;->loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Z

.field final synthetic e:Z

.field final synthetic f:Lcom/anythink/network/adx/AdxATAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/adx/AdxATAdapter;IILandroid/content/Context;ZZ)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->f:Lcom/anythink/network/adx/AdxATAdapter;

    iput p2, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->a:I

    iput p3, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->b:I

    iput-object p4, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->c:Landroid/content/Context;

    iput-boolean p5, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->d:Z

    iput-boolean p6, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNativeAdLoadError(Lcom/anythink/basead/c/f;)V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->f:Lcom/anythink/network/adx/AdxATAdapter;

    invoke-static {v0}, Lcom/anythink/network/adx/AdxATAdapter;->c(Lcom/anythink/network/adx/AdxATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->f:Lcom/anythink/network/adx/AdxATAdapter;

    invoke-static {v0}, Lcom/anythink/network/adx/AdxATAdapter;->d(Lcom/anythink/network/adx/AdxATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/anythink/basead/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/anythink/basead/c/f;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final varargs onNativeAdLoaded([Lcom/anythink/basead/e/h;)V
    .locals 7

    .line 70
    array-length v0, p1

    new-array v0, v0, [Lcom/anythink/network/adx/AdxATNativeAd;

    const/4 v1, 0x0

    .line 71
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 72
    aget-object v2, p1, v1

    .line 73
    iget v3, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->a:I

    iget v4, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->b:I

    invoke-virtual {v2, v3, v4}, Lcom/anythink/basead/e/h;->a(II)V

    .line 74
    new-instance v3, Lcom/anythink/network/adx/AdxATNativeAd;

    iget-object v4, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->c:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->d:Z

    iget-boolean v6, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->e:Z

    invoke-direct {v3, v4, v2, v5, v6}, Lcom/anythink/network/adx/AdxATNativeAd;-><init>(Landroid/content/Context;Lcom/anythink/basead/e/h;ZZ)V

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->f:Lcom/anythink/network/adx/AdxATAdapter;

    invoke-static {p1}, Lcom/anythink/network/adx/AdxATAdapter;->a(Lcom/anythink/network/adx/AdxATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 77
    iget-object p1, p0, Lcom/anythink/network/adx/AdxATAdapter$1;->f:Lcom/anythink/network/adx/AdxATAdapter;

    invoke-static {p1}, Lcom/anythink/network/adx/AdxATAdapter;->b(Lcom/anythink/network/adx/AdxATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method
