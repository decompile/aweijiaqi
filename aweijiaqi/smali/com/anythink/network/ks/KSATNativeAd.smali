.class public Lcom/anythink/network/ks/KSATNativeAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/kwad/sdk/api/KsNativeAd;

.field c:Z

.field d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/kwad/sdk/api/KsNativeAd;Z)V
    .locals 2

    .line 38
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->a:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    .line 41
    iput-boolean p3, p0, Lcom/anythink/network/ks/KSATNativeAd;->c:Z

    .line 1047
    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAppName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 1048
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getAppIconUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 1049
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getAdSource()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setAdFrom(Ljava/lang/String;)V

    .line 1050
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getAppScore()F

    move-result p1

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setStarRating(Ljava/lang/Double;)V

    .line 1051
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getAdDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 1052
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    .line 1053
    iget-object p2, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getInteractionType()I

    move-result p2

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/anythink/network/ks/KSATNativeAd;->setNativeInteractionType(I)V

    .line 1055
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    .line 1057
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1058
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/kwad/sdk/api/KsImage;

    .line 1059
    invoke-interface {v1}, Lcom/kwad/sdk/api/KsImage;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1061
    :cond_1
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 1063
    :cond_2
    invoke-virtual {p0, p2}, Lcom/anythink/network/ks/KSATNativeAd;->setImageUrlList(Ljava/util/List;)V

    .line 1064
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getActionDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    .line 1065
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getVideoUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/ks/KSATNativeAd;->setVideoUrl(Ljava/lang/String;)V

    .line 1067
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    if-ne p1, v0, :cond_3

    const-string p1, "1"

    .line 1068
    iput-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->mAdSourceType:Ljava/lang/String;

    return-void

    .line 1069
    :cond_3
    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    const/4 p2, 0x3

    if-eq p1, p2, :cond_4

    iget-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_5

    :cond_4
    const-string p1, "2"

    .line 1070
    iput-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->mAdSourceType:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method private a()V
    .locals 5

    .line 47
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getAppName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getAppIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getAdSource()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setAdFrom(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getAppScore()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setStarRating(Ljava/lang/Double;)V

    .line 51
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getAdDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v1}, Lcom/kwad/sdk/api/KsNativeAd;->getInteractionType()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, Lcom/anythink/network/ks/KSATNativeAd;->setNativeInteractionType(I)V

    .line 55
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_2

    .line 57
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 58
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/kwad/sdk/api/KsImage;

    .line 59
    invoke-interface {v4}, Lcom/kwad/sdk/api/KsImage;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 61
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 63
    :cond_2
    invoke-virtual {p0, v1}, Lcom/anythink/network/ks/KSATNativeAd;->setImageUrlList(Ljava/util/List;)V

    .line 64
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getActionDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/ks/KSATNativeAd;->setVideoUrl(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result v0

    if-ne v0, v3, :cond_3

    const-string v0, "1"

    .line 68
    iput-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->mAdSourceType:Ljava/lang/String;

    return-void

    .line 69
    :cond_3
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    const-string v0, "2"

    .line 70
    iput-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->mAdSourceType:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    new-instance v1, Lcom/anythink/network/ks/KSATNativeAd$1;

    invoke-direct {v1, p0}, Lcom/anythink/network/ks/KSATNativeAd$1;-><init>(Lcom/anythink/network/ks/KSATNativeAd;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/kwad/sdk/api/KsNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;)V

    return-void
.end method

.method private a(Ljava/util/List;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 81
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->d:Landroid/view/View;

    if-eq p2, v0, :cond_1

    .line 82
    check-cast p2, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 83
    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 84
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 85
    invoke-direct {p0, p1, v1}, Lcom/anythink/network/ks/KSATNativeAd;->a(Ljava/util/List;Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->d:Landroid/view/View;

    if-eq p2, v0, :cond_2

    .line 89
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsNativeAd;->setDownloadListener(Lcom/kwad/sdk/api/KsAppDownloadListener;)V

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0, v1, v1, v1}, Lcom/kwad/sdk/api/KsNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :catch_0
    iput-object v1, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    .line 192
    :cond_0
    iput-object v1, p0, Lcom/anythink/network/ks/KSATNativeAd;->a:Landroid/content/Context;

    return-void
.end method

.method public getAdLogo()Landroid/graphics/Bitmap;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getSdkLogo()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .line 172
    :try_start_0
    new-instance p1, Lcom/kwad/sdk/api/KsAdVideoPlayConfig$Builder;

    invoke-direct {p1}, Lcom/kwad/sdk/api/KsAdVideoPlayConfig$Builder;-><init>()V

    iget-boolean v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->c:Z

    .line 173
    invoke-virtual {p1, v0}, Lcom/kwad/sdk/api/KsAdVideoPlayConfig$Builder;->videoSoundEnable(Z)Lcom/kwad/sdk/api/KsAdVideoPlayConfig$Builder;

    move-result-object p1

    .line 174
    iget-object v0, p0, Lcom/anythink/network/ks/KSATNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd;

    iget-object v1, p0, Lcom/anythink/network/ks/KSATNativeAd;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/kwad/sdk/api/KsAdVideoPlayConfig$Builder;->build()Lcom/kwad/sdk/api/KsAdVideoPlayConfig;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/kwad/sdk/api/KsNativeAd;->getVideoView(Landroid/content/Context;Lcom/kwad/sdk/api/KsAdVideoPlayConfig;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/network/ks/KSATNativeAd;->d:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    .line 97
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 98
    invoke-direct {p0, p2, p1}, Lcom/anythink/network/ks/KSATNativeAd;->a(Ljava/util/List;Landroid/view/View;)V

    .line 100
    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2}, Lcom/anythink/network/ks/KSATNativeAd;->a(Landroid/view/ViewGroup;Ljava/util/List;)V

    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 107
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p3

    if-gtz p3, :cond_1

    .line 110
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 111
    invoke-direct {p0, p2, p1}, Lcom/anythink/network/ks/KSATNativeAd;->a(Ljava/util/List;Landroid/view/View;)V

    .line 114
    :cond_1
    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2}, Lcom/anythink/network/ks/KSATNativeAd;->a(Landroid/view/ViewGroup;Ljava/util/List;)V

    return-void
.end method
