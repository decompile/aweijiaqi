.class final Lcom/anythink/network/ks/KSATInterstitialAdapter$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/ks/KSATInterstitialAdapter;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/ks/KSATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/ks/KSATInterstitialAdapter;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(ILjava/lang/String;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->h(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->i(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onFullScreenVideoAdLoad(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/kwad/sdk/api/KsFullScreenVideoAd;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 130
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    iput-object p1, v0, Lcom/anythink/network/ks/KSATInterstitialAdapter;->b:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    .line 132
    iget-object p1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->l(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 133
    iget-object p1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->m(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    new-array v0, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {p1, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    .line 137
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/anythink/network/ks/KSATInitManager;->getInstance()Lcom/anythink/network/ks/KSATInitManager;

    move-result-object p1

    iget-object v0, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-virtual {v0}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    iget-object v1, v1, Lcom/anythink/network/ks/KSATInterstitialAdapter;->b:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    invoke-virtual {p1, v0, v1}, Lcom/anythink/network/ks/KSATInitManager;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final onRequestResult(I)V
    .locals 0

    .line 123
    iget-object p1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->j(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 124
    iget-object p1, p0, Lcom/anythink/network/ks/KSATInterstitialAdapter$3;->a:Lcom/anythink/network/ks/KSATInterstitialAdapter;

    invoke-static {p1}, Lcom/anythink/network/ks/KSATInterstitialAdapter;->k(Lcom/anythink/network/ks/KSATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdDataLoaded()V

    :cond_0
    return-void
.end method
