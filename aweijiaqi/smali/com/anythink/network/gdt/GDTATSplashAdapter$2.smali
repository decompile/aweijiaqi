.class final Lcom/anythink/network/gdt/GDTATSplashAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATSplashAdapter;->onADLoaded(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATSplashAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATSplashAdapter;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter$2;->a:Lcom/anythink/network/gdt/GDTATSplashAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter$2;->a:Lcom/anythink/network/gdt/GDTATSplashAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATSplashAdapter;->f(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;

    invoke-direct {v0}, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;-><init>()V

    .line 161
    iput-object p3, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->appInfoUrl:Ljava/lang/String;

    .line 162
    iput p2, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->scenes:I

    .line 163
    iput-object p4, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->confirmCallBack:Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;

    .line 164
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter$2;->a:Lcom/anythink/network/gdt/GDTATSplashAdapter;

    invoke-static {p2}, Lcom/anythink/network/gdt/GDTATSplashAdapter;->g(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    move-result-object p2

    invoke-interface {p2, p1, v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onDownloadConfirm(Landroid/content/Context;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    :cond_0
    return-void
.end method
