.class public Lcom/anythink/network/gdt/GDTATNativeExpressAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Lcom/qq/e/ads/nativ/NativeExpressAD;

.field b:Lcom/qq/e/ads/nativ/NativeExpressADView;

.field c:Lcom/anythink/network/gdt/a;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;IIIII)V
    .locals 4

    .line 37
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 39
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd;)V

    const/4 v1, -0x1

    if-lez p3, :cond_0

    .line 173
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    int-to-float p3, p3

    invoke-static {p1, p3}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;F)I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, -0x1

    :goto_0
    if-lez p4, :cond_1

    .line 176
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    int-to-float p4, p4

    invoke-static {p1, p4}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;F)I

    move-result p4

    goto :goto_1

    :cond_1
    const/4 p4, -0x2

    .line 179
    :goto_1
    new-instance v2, Lcom/qq/e/ads/nativ/NativeExpressAD;

    new-instance v3, Lcom/qq/e/ads/nativ/ADSize;

    invoke-direct {v3, p3, p4}, Lcom/qq/e/ads/nativ/ADSize;-><init>(II)V

    invoke-direct {v2, p1, v3, p2, v0}, Lcom/qq/e/ads/nativ/NativeExpressAD;-><init>(Landroid/content/Context;Lcom/qq/e/ads/nativ/ADSize;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;)V

    iput-object v2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    .line 181
    new-instance p2, Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-direct {p2}, Lcom/qq/e/ads/cfg/VideoOption$Builder;-><init>()V

    const/4 p3, 0x0

    const/4 p4, 0x1

    if-ne p5, p4, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    .line 182
    :goto_2
    invoke-virtual {p2, v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p2

    if-ne p5, p4, :cond_3

    const/4 p3, 0x1

    .line 183
    :cond_3
    invoke-virtual {p2, p3}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p2

    .line 184
    invoke-virtual {p2, p6}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayPolicy(I)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p2

    .line 185
    invoke-virtual {p2}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->build()Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object p2

    .line 187
    iget-object p3, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-virtual {p3, p2}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setVideoOption(Lcom/qq/e/ads/cfg/VideoOption;)V

    if-eq p7, v1, :cond_4

    .line 189
    iget-object p3, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-virtual {p3, p7}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setMaxVideoDuration(I)V

    .line 192
    :cond_4
    iget-object p3, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    invoke-virtual {p2}, Lcom/qq/e/ads/cfg/VideoOption;->getAutoPlayPolicy()I

    move-result p2

    invoke-static {p1, p2}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {p3, p1}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setVideoPlayPolicy(I)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATNativeExpressAd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->mAdSourceType:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/anythink/network/gdt/a;)V
    .locals 1

    .line 197
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    .line 198
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/NativeExpressAD;->loadAD(I)V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATNativeExpressAd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->mAdSourceType:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->setMediaListener(Lcom/qq/e/ads/nativ/NativeExpressMediaListener;)V

    .line 247
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    invoke-virtual {v0}, Lcom/qq/e/ads/nativ/NativeExpressADView;->destroy()V

    .line 249
    :cond_0
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    .line 250
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    .line 251
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 0

    .line 208
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    return-object p1
.end method

.method public isNativeExpress()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0

    .line 229
    invoke-super {p0, p1, p2}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V

    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    .line 237
    invoke-super {p0, p1, p2, p3}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V

    return-void
.end method

.method public registerDownloadConfirmListener()V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    if-eqz v0, :cond_0

    .line 214
    new-instance v1, Lcom/anythink/network/gdt/GDTATNativeExpressAd$2;

    invoke-direct {v1, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd$2;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd;)V

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V

    :cond_0
    return-void
.end method
