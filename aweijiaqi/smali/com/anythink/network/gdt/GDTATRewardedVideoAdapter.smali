.class public Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;
.super Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

.field b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

.field c:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const-class v0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardVideoAdapter;-><init>()V

    const-string v0, "0"

    .line 33
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->e:Ljava/lang/String;

    const/4 v0, 0x0

    .line 34
    iput v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic C(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic D(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic E(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic F(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic G(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic H(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic I(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic J(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic K(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic L(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .line 83
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x30

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_1

    const/16 v2, 0x31

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 2096
    new-instance v0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    iget v5, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    if-eq v5, v4, :cond_3

    const/4 v3, 0x1

    :cond_3
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    .line 2181
    :try_start_0
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 2182
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 2183
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 2184
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2188
    :catchall_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->loadAD()V

    return-void

    .line 1192
    :cond_4
    new-instance v0, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    .line 1275
    iget p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    if-eq p1, v4, :cond_5

    const/4 v3, 0x1

    :cond_5
    invoke-virtual {v0, v3}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setVolumeOn(Z)V

    .line 1277
    :try_start_1
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 1278
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 1279
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 1280
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1284
    :catchall_1
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->loadAD()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;Landroid/content/Context;)V
    .locals 6

    .line 3083
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x30

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_1

    const/16 v2, 0x31

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 4096
    new-instance v0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    iget v5, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    if-eq v5, v4, :cond_3

    const/4 v3, 0x1

    :cond_3
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    .line 4181
    :try_start_0
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 4182
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 4183
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 4184
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4188
    :catchall_0
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->loadAD()V

    return-void

    .line 3192
    :cond_4
    new-instance v0, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    .line 3275
    iget p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    if-eq p1, v4, :cond_5

    const/4 v3, 0x1

    :cond_5
    invoke-virtual {v0, v3}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setVolumeOn(Z)V

    .line 3277
    :try_start_1
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 3278
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 3279
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 3280
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3284
    :catchall_1
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->loadAD()V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private b(Landroid/content/Context;)V
    .locals 5

    .line 96
    new-instance v0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    iget v3, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v0, p1, v1, v2, v4}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;Z)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    .line 181
    :try_start_0
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 182
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 183
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 184
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :catchall_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->loadAD()V

    return-void
.end method

.method static synthetic c(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private c(Landroid/content/Context;)V
    .locals 3

    .line 192
    new-instance v0, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$3;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    .line 275
    iget p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setVolumeOn(Z)V

    .line 277
    :try_start_0
    new-instance p1, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    .line 278
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 279
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mUserData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    .line 280
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :catchall_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->loadAD()V

    return-void
.end method

.method static synthetic d(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;)Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mImpressionListener:Lcom/anythink/rewardvideo/unitgroup/api/CustomRewardedVideoEventListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 297
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-eqz v0, :cond_1

    .line 301
    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->destroy()V

    .line 302
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    :cond_1
    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 291
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 313
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 4

    .line 319
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 320
    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->checkValidity()Lcom/qq/e/comm/util/VideoAdValidity;

    move-result-object v0

    sget-object v3, Lcom/qq/e/comm/util/VideoAdValidity;->VALID:Lcom/qq/e/comm/util/VideoAdValidity;

    if-ne v0, v3, :cond_0

    return v1

    :cond_0
    return v2

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-eqz v0, :cond_2

    .line 322
    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->checkValidity()Lcom/qq/e/comm/util/VideoAdValidity;

    move-result-object v0

    sget-object v3, Lcom/qq/e/comm/util/VideoAdValidity;->VALID:Lcom/qq/e/comm/util/VideoAdValidity;

    if-ne v0, v3, :cond_2

    return v1

    :cond_2
    return v2
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p3, "app_id"

    .line 41
    invoke-interface {p2, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v1

    :goto_0
    const-string v0, "unit_id"

    .line 45
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    const-string v2, "personalized_template"

    .line 49
    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 50
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->e:Ljava/lang/String;

    :cond_2
    const-string v2, "video_muted"

    .line 53
    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 54
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->f:I

    .line 57
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    goto :goto_2

    .line 65
    :cond_4
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->c:Ljava/lang/String;

    .line 67
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;Landroid/content/Context;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/gdt/GDTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/gdt/GDTATInitManager$OnInitCallback;)V

    return-void

    .line 58
    :cond_5
    :goto_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_6

    .line 59
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "GTD appid or unitId is empty."

    invoke-interface {p1, v1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 334
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->showAD(Landroid/app/Activity;)V

    return-void

    .line 336
    :cond_0
    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->showAD()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 339
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    return-void

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->b:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    .line 344
    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->showAD(Landroid/app/Activity;)V

    return-void

    .line 346
    :cond_2
    sget-object p1, Lcom/anythink/network/gdt/GDTATRewardedVideoAdapter;->d:Ljava/lang/String;

    const-string v0, "GDT native express reward video, show: activity = null"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method
