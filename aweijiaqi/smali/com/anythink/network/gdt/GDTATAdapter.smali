.class public Lcom/anythink/network/gdt/GDTATAdapter;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAdapter;

# interfaces
.implements Lcom/anythink/network/gdt/a;


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAdapter;-><init>()V

    const/4 v0, -0x1

    .line 32
    iput v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    const/4 v0, 0x2

    .line 34
    iput v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->c:I

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 10

    .line 43
    :try_start_0
    iget v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->d:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 51
    iget v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->c:I

    if-eq v0, v1, :cond_0

    .line 53
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    iget v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    iget v6, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    iget v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    iget v8, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    iget v9, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    move-object v2, v0

    move-object v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V

    .line 1197
    iput-object p0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    .line 1198
    iget-object p1, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/NativeExpressAD;->loadAD(I)V

    return-void

    .line 2115
    :cond_0
    new-instance v8, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    iget v3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    iget v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    iget v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    iget v6, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    iget v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    move-object v0, v8

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V

    .line 2116
    invoke-virtual {v8, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/a;)V

    return-void

    .line 1076
    :cond_1
    new-instance v0, Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/anythink/network/gdt/GDTATAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATAdapter;Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeADUnifiedListener;)V

    .line 1106
    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1107
    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setMaxVideoDuration(I)V

    .line 1109
    :cond_2
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    invoke-static {p1, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setVideoPlayPolicy(I)V

    .line 1110
    iget p1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->b:I

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->loadData(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 64
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz v0, :cond_3

    .line 65
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATAdapter;Landroid/content/Context;)V
    .locals 10

    .line 3043
    :try_start_0
    iget v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->d:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 3051
    iget v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->c:I

    if-eq v0, v1, :cond_0

    .line 3053
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    iget v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    iget v6, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    iget v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    iget v8, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    iget v9, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    move-object v2, v0

    move-object v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V

    .line 3197
    iput-object p0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    .line 3198
    iget-object p1, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressAD;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/NativeExpressAD;->loadAD(I)V

    return-void

    .line 4115
    :cond_0
    new-instance v8, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    iget v3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    iget v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    iget v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    iget v6, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    iget v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    move-object v0, v8

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V

    .line 4116
    invoke-virtual {v8, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/a;)V

    return-void

    .line 3076
    :cond_1
    new-instance v0, Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/anythink/network/gdt/GDTATAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATAdapter;Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeADUnifiedListener;)V

    .line 3106
    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 3107
    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setMaxVideoDuration(I)V

    .line 3109
    :cond_2
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    invoke-static {p1, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setVideoPlayPolicy(I)V

    .line 3110
    iget p1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->b:I

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->loadData(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 3064
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz v0, :cond_3

    .line 3065
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-interface {p0, v0, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    .line 76
    new-instance v0, Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/anythink/network/gdt/GDTATAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATAdapter;Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeADUnifiedListener;)V

    .line 106
    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 107
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setMaxVideoDuration(I)V

    .line 109
    :cond_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    iget v1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    invoke-static {p1, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setVideoPlayPolicy(I)V

    .line 110
    iget p1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->b:I

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->loadData(I)V

    return-void
.end method

.method static synthetic c(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private c(Landroid/content/Context;)V
    .locals 9

    .line 115
    new-instance v8, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    iget v3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    iget v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    iget v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    iget v6, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    iget v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    move-object v0, v8

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V

    .line 116
    invoke-virtual {v8, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/a;)V

    return-void
.end method

.method static synthetic d(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/gdt/GDTATAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 0

    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 121
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 224
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "key_height"

    const-string v1, "gdtad_height"

    const-string v2, "key_width"

    const-string v3, "request_ad_num"

    const-string v4, "app_id"

    .line 129
    invoke-interface {p2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const-string v6, ""

    if-eqz v5, :cond_0

    .line 130
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v6

    :goto_0
    const-string v5, "unit_id"

    .line 132
    invoke-interface {p2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 133
    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_1
    move-object v5, v6

    :goto_1
    const-string v7, "unit_version"

    .line 136
    invoke-interface {p2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 137
    invoke-interface {p2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->c:I

    :cond_2
    const-string v7, "unit_type"

    .line 140
    invoke-interface {p2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 141
    invoke-interface {p2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/anythink/network/gdt/GDTATAdapter;->d:I

    .line 144
    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    goto/16 :goto_4

    :cond_4
    const/4 v4, 0x1

    .line 154
    :try_start_0
    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 155
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    :cond_5
    const/4 v3, 0x1

    .line 160
    :goto_2
    iput v3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->b:I

    .line 162
    iput-object v5, p0, Lcom/anythink/network/gdt/GDTATAdapter;->a:Ljava/lang/String;

    .line 168
    :try_start_1
    invoke-interface {p3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 169
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/anythink/network/gdt/GDTATAdapter;->h:I

    .line 172
    :cond_6
    invoke-interface {p3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 173
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    iput p3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I

    goto :goto_3

    .line 174
    :cond_7
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 175
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    iput p3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->i:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception p3

    .line 178
    invoke-virtual {p3}, Ljava/lang/Exception;->printStackTrace()V

    :cond_8
    :goto_3
    const/4 p3, 0x0

    const/4 v0, -0x1

    const-string v1, "video_muted"

    .line 184
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 185
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    :cond_9
    const-string v1, "video_autoplay"

    .line 187
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 188
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    :cond_a
    const-string v1, "video_duration"

    .line 190
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 191
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 194
    :cond_b
    iput p3, p0, Lcom/anythink/network/gdt/GDTATAdapter;->e:I

    .line 195
    iput v4, p0, Lcom/anythink/network/gdt/GDTATAdapter;->f:I

    .line 196
    iput v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->g:I

    .line 198
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/gdt/GDTATAdapter$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/gdt/GDTATAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATAdapter;Landroid/content/Context;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/gdt/GDTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/gdt/GDTATInitManager$OnInitCallback;)V

    return-void

    .line 145
    :cond_c
    :goto_4
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_d

    .line 146
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "GTD appid or unitId is empty."

    invoke-interface {p1, v6, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    return-void
.end method

.method public notifyError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    invoke-interface {v0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public varargs notifyLoaded([Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;)V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    invoke-interface {v0, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_0
    return-void
.end method
