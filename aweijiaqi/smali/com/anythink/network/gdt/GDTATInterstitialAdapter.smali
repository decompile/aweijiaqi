.class public Lcom/anythink/network/gdt/GDTATInterstitialAdapter;
.super Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;

# interfaces
.implements Lcom/qq/e/ads/interstitial2/UnifiedInterstitialMediaListener;


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

.field b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:I

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialAdapter;-><init>()V

    const/4 v0, 0x0

    .line 36
    iput v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->e:I

    return-void
.end method

.method static synthetic A(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic B(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic C(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic D(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private a(Landroid/content/Context;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "0"

    .line 41
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    const-string v1, "is_fullscreen"

    .line 42
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    :cond_0
    const-string v1, "personalized_template"

    .line 47
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "1"

    .line 51
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1144
    new-instance v0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v3, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;

    invoke-direct {v3, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, p1, v2, v3}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    .line 1235
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 1237
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1238
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 1240
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadHalfScreenAD()V

    return-void

    .line 2061
    :cond_3
    instance-of v0, p1, Landroid/app/Activity;

    if-nez v0, :cond_5

    .line 2062
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_4

    .line 2063
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, ""

    const-string v0, "GDT UnifiedInterstitial\'s context must be activity."

    invoke-interface {p1, p2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void

    .line 2068
    :cond_5
    new-instance v0, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-object v2, p1

    check-cast v2, Landroid/app/Activity;

    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v4, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;

    invoke-direct {v4, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, v2, v3, v4}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    .line 2134
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 2136
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2137
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 2139
    :cond_6
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadAD()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;)V
    .locals 5

    const-string v0, "0"

    .line 3041
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    const-string v1, "is_fullscreen"

    .line 3042
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3043
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    :cond_0
    const-string v1, "personalized_template"

    .line 3047
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3048
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "1"

    .line 3051
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3144
    new-instance v0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v3, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;

    invoke-direct {v3, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, p1, v2, v3}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    .line 3235
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 3237
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 3238
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 3240
    :cond_2
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadHalfScreenAD()V

    return-void

    .line 4061
    :cond_3
    instance-of v0, p1, Landroid/app/Activity;

    if-nez v0, :cond_5

    .line 4062
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_4

    .line 4063
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p1, ""

    const-string p2, "GDT UnifiedInterstitial\'s context must be activity."

    invoke-interface {p0, p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void

    .line 4068
    :cond_5
    new-instance v0, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-object v2, p1

    check-cast v2, Landroid/app/Activity;

    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v4, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;

    invoke-direct {v4, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, v2, v3, v4}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    .line 4134
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 4136
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 4137
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 4139
    :cond_6
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadAD()V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private b(Landroid/content/Context;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 61
    instance-of v0, p1, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 62
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_0

    .line 63
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, ""

    const-string v0, "GDT UnifiedInterstitial\'s context must be activity."

    invoke-interface {p1, p2, v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    .line 68
    :cond_1
    new-instance v0, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v3, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;

    invoke-direct {v3, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    .line 134
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 136
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    const-string p2, "1"

    invoke-static {p2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 137
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 139
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadAD()V

    return-void
.end method

.method static synthetic c(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private c(Landroid/content/Context;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 144
    new-instance v0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    new-instance v2, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;

    invoke-direct {v2, p0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    .line 235
    invoke-direct {p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Landroid/content/Context;Ljava/util/Map;)V

    .line 237
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    const-string p2, "1"

    invoke-static {p2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 238
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadFullScreenAD()V

    return-void

    .line 240
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->loadHalfScreenAD()V

    return-void
.end method

.method static synthetic d(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method private d(Landroid/content/Context;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "video_muted"

    .line 376
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 377
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "video_autoplay"

    .line 379
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    .line 380
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    :goto_1
    const-string v3, "video_duration"

    .line 382
    invoke-interface {p2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, -0x1

    if-eqz v5, :cond_2

    .line 383
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_2

    :cond_2
    const/4 p2, -0x1

    .line 386
    :goto_2
    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-eqz v3, :cond_6

    .line 387
    new-instance v3, Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-direct {v3}, Lcom/qq/e/ads/cfg/VideoOption$Builder;-><init>()V

    if-ne v0, v4, :cond_3

    const/4 v5, 0x1

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    .line 388
    :goto_3
    invoke-virtual {v3, v5}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v3

    if-ne v0, v4, :cond_4

    const/4 v5, 0x1

    goto :goto_4

    :cond_4
    const/4 v5, 0x0

    .line 389
    :goto_4
    invoke-virtual {v3, v5}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v3

    .line 390
    invoke-virtual {v3, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayPolicy(I)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v3

    .line 391
    invoke-virtual {v3}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->build()Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object v3

    .line 392
    iget-object v5, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v5, v3}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoOption(Lcom/qq/e/ads/cfg/VideoOption;)V

    if-eq p2, v6, :cond_5

    .line 394
    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v3, p2}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMaxVideoDuration(I)V

    .line 397
    :cond_5
    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    invoke-static {p1, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v3, p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoPlayPolicy(I)V

    .line 401
    :cond_6
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    if-eqz p1, :cond_d

    .line 402
    sget-object p1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->ALWAYS:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    if-eqz v1, :cond_9

    if-eq v1, v4, :cond_8

    const/4 v3, 0x2

    if-eq v1, v3, :cond_7

    goto :goto_5

    .line 411
    :cond_7
    sget-object p1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->NEVER:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_5

    .line 408
    :cond_8
    sget-object p1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->ALWAYS:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_5

    .line 405
    :cond_9
    sget-object p1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->WIFI:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    .line 416
    :goto_5
    new-instance v1, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-direct {v1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;-><init>()V

    if-ne v0, v4, :cond_a

    const/4 v3, 0x1

    goto :goto_6

    :cond_a
    const/4 v3, 0x0

    .line 417
    :goto_6
    invoke-virtual {v1, v3}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object v1

    if-ne v0, v4, :cond_b

    const/4 v2, 0x1

    .line 418
    :cond_b
    invoke-virtual {v1, v2}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object v0

    .line 419
    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayPolicy(Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object p1

    if-eq p2, v6, :cond_c

    .line 422
    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setMaxVideoDuration(I)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    .line 425
    :cond_c
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->build()Lcom/qq/e/ads/nativ/express2/VideoOption2;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->setVideoOption(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V

    :cond_d
    return-void
.end method

.method static synthetic e(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic h(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic j(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic k(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic l(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic m(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic n(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic o(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic p(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic q(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic r(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic s(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic t(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic u(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic v(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic w(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic x(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic y(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method

.method static synthetic z(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMediaListener(Lcom/qq/e/ads/interstitial2/UnifiedInterstitialMediaListener;)V

    .line 348
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->destroy()V

    .line 349
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    if-eqz v0, :cond_1

    .line 353
    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->destroy()V

    .line 354
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    :cond_1
    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 297
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 365
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 4

    .line 246
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->isValid()Z

    move-result v0

    return v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 249
    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->checkValidity()Lcom/qq/e/comm/util/VideoAdValidity;

    move-result-object v0

    .line 250
    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v2}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->isVideoAd()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    .line 251
    sget-object v2, Lcom/qq/e/comm/util/VideoAdValidity;->VALID:Lcom/qq/e/comm/util/VideoAdValidity;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/qq/e/comm/util/VideoAdValidity;->NONE_CACHE:Lcom/qq/e/comm/util/VideoAdValidity;

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    :goto_0
    return v3

    .line 253
    :cond_3
    sget-object v2, Lcom/qq/e/comm/util/VideoAdValidity;->VALID:Lcom/qq/e/comm/util/VideoAdValidity;

    if-ne v0, v2, :cond_4

    return v3

    :cond_4
    return v1
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p3, "app_id"

    .line 305
    invoke-interface {p2, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 306
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v1

    :goto_0
    const-string v0, "unit_id"

    .line 309
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 310
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    const-string v2, "unit_version"

    .line 313
    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 314
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->e:I

    .line 317
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 325
    :cond_3
    iput-object p3, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->c:Ljava/lang/String;

    .line 326
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d:Ljava/lang/String;

    .line 329
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$3;-><init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;Landroid/content/Context;Ljava/util/Map;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/gdt/GDTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/gdt/GDTATInitManager$OnInitCallback;)V

    return-void

    .line 318
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 319
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "GDT appid or unitId is empty."

    invoke-interface {p1, v1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public onVideoComplete()V
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoEnd()V

    :cond_0
    return-void
.end method

.method public onVideoError(Lcom/qq/e/comm/util/AdError;)V
    .locals 3

    .line 464
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onVideoInit()V
    .locals 0

    return-void
.end method

.method public onVideoLoading()V
    .locals 0

    return-void
.end method

.method public onVideoPageClose()V
    .locals 0

    return-void
.end method

.method public onVideoPageOpen()V
    .locals 0

    return-void
.end method

.method public onVideoPause()V
    .locals 0

    return-void
.end method

.method public onVideoReady(J)V
    .locals 0

    return-void
.end method

.method public onVideoStart()V
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->mImpressListener:Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoStart()V

    :cond_0
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 3

    .line 261
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    const-string v1, "Gdt (Full Screen) show fail: context need be Activity"

    const-string v2, "1"

    if-eqz v0, :cond_3

    .line 263
    invoke-virtual {v0, p0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMediaListener(Lcom/qq/e/ads/interstitial2/UnifiedInterstitialMediaListener;)V

    .line 265
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->showFullScreenAD(Landroid/app/Activity;)V

    return-void

    .line 269
    :cond_0
    sget-object p1, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->TAG:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    if-eqz p1, :cond_2

    .line 273
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->show(Landroid/app/Activity;)V

    return-void

    .line 275
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->show()V

    return-void

    .line 278
    :cond_3
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    if-eqz v0, :cond_7

    .line 279
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_4

    .line 281
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->showFullScreenAD(Landroid/app/Activity;)V

    return-void

    .line 283
    :cond_4
    sget-object p1, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->TAG:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_5
    if-eqz p1, :cond_6

    .line 287
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->showHalfScreenAD(Landroid/app/Activity;)V

    return-void

    .line 289
    :cond_6
    sget-object p1, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->TAG:Ljava/lang/String;

    const-string v0, "Gdt (Half Screen) show fail: context need be Activity"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-void
.end method
