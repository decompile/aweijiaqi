.class final Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeExpressAd2;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLoadSuccess(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 41
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-static {v0, p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V

    return-void

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    if-eqz p1, :cond_1

    .line 45
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    const-string v0, ""

    const-string v1, "GDT Ad request success but no Ad return."

    invoke-interface {p1, v0, v1}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    :cond_1
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public final onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 3

    .line 53
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    return-void
.end method
