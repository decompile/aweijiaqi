.class final Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b(Landroid/content/Context;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onADClicked()V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->g(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->h(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClicked()V

    :cond_0
    return-void
.end method

.method public final onADClosed()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->i(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->j(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClose()V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->destroy()V

    .line 126
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v1}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final onADExposure()V
    .locals 0

    return-void
.end method

.method public final onADLeftApplication()V
    .locals 0

    return-void
.end method

.method public final onADOpened()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->e(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->f(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdShow()V

    :cond_0
    return-void
.end method

.method public final onADReceive()V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    .line 75
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v1}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->c(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$1;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->d(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onVideoCached()V
    .locals 0

    return-void
.end method
