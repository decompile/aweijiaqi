.class public Lcom/anythink/network/gdt/GDTATSplashAdapter;
.super Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

# interfaces
.implements Lcom/qq/e/ads/splash/SplashADListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Lcom/qq/e/ads/splash/SplashAD;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATSplashAdapter;Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/ads/splash/SplashAD;
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->c:Lcom/qq/e/ads/splash/SplashAD;

    return-object p1
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATSplashAdapter;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mFetchAdTimeout:I

    return p0
.end method

.method static synthetic c(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/qq/e/ads/splash/SplashAD;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->c:Lcom/qq/e/ads/splash/SplashAD;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic e(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    return-object p0
.end method

.method static synthetic f(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/network/gdt/GDTATSplashAdapter;)Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    return-object p0
.end method


# virtual methods
.method public destory()V
    .locals 1

    const/4 v0, 0x0

    .line 102
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->c:Lcom/qq/e/ads/splash/SplashAD;

    return-void
.end method

.method public getNetworkName()Ljava/lang/String;
    .locals 1

    .line 35
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkSDKVersion()Ljava/lang/String;
    .locals 1

    .line 112
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATInitManager;->getNetworkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAdReady()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->b:Z

    return v0
.end method

.method public loadCustomNetworkAd(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ad_click_confirm_status"

    const-string v1, "app_id"

    .line 48
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, ""

    if-eqz v2, :cond_0

    .line 49
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    const-string v2, "unit_id"

    .line 51
    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 52
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v3

    .line 55
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_2

    .line 63
    :cond_2
    iput-object v2, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->a:Ljava/lang/String;

    const/4 v1, 0x0

    .line 65
    iput-boolean v1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->b:Z

    .line 67
    iput-boolean v1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->d:Z

    if-eqz p3, :cond_3

    .line 69
    :try_start_0
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 70
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :catch_0
    :cond_3
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/gdt/GDTATSplashAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/gdt/GDTATSplashAdapter$1;-><init>(Lcom/anythink/network/gdt/GDTATSplashAdapter;Landroid/content/Context;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/anythink/network/gdt/GDTATInitManager;->initSDK(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/network/gdt/GDTATInitManager$OnInitCallback;)V

    return-void

    .line 56
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_5

    .line 57
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const-string p2, "GTD appid or unitId is empty."

    invoke-interface {p1, v3, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public onADClicked()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdClicked()V

    :cond_0
    return-void
.end method

.method public onADDismissed()V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdDismiss()V

    :cond_0
    return-void
.end method

.method public onADExposure()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mImpressionListener:Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;

    invoke-interface {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;->onSplashAdShow()V

    :cond_0
    return-void
.end method

.method public onADLoaded(J)V
    .locals 0

    const/4 p1, 0x1

    .line 154
    iput-boolean p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->b:Z

    .line 155
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->c:Lcom/qq/e/ads/splash/SplashAD;

    if-eqz p1, :cond_0

    iget-boolean p2, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->d:Z

    if-eqz p2, :cond_0

    .line 156
    new-instance p2, Lcom/anythink/network/gdt/GDTATSplashAdapter$2;

    invoke-direct {p2, p0}, Lcom/anythink/network/gdt/GDTATSplashAdapter$2;-><init>(Lcom/anythink/network/gdt/GDTATSplashAdapter;)V

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/splash/SplashAD;->setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V

    .line 169
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz p1, :cond_1

    .line 170
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    const/4 p2, 0x0

    new-array p2, p2, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {p1, p2}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    :cond_1
    return-void
.end method

.method public onADPresent()V
    .locals 0

    return-void
.end method

.method public onADTick(J)V
    .locals 0

    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 3

    .line 124
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->mLoadListener:Lcom/anythink/core/api/ATCustomLoadListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public show(Landroid/app/Activity;Landroid/view/ViewGroup;)V
    .locals 0

    .line 95
    iget-boolean p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->b:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATSplashAdapter;->c:Lcom/qq/e/ads/splash/SplashAD;

    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {p1, p2}, Lcom/qq/e/ads/splash/SplashAD;->showAd(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method
