.class public Lcom/anythink/network/gdt/GDTATNativeAd;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/content/Context;

.field c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

.field d:I

.field e:I

.field f:I

.field g:Landroid/view/View;

.field h:Lcom/qq/e/ads/nativ/MediaView;

.field i:Z

.field j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    const-class v0, Lcom/anythink/network/gdt/GDTATNativeAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/network/gdt/GDTATNativeAd;->k:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/qq/e/ads/nativ/NativeUnifiedADData;III)V
    .locals 1

    .line 50
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    const/4 v0, 0x0

    .line 165
    iput-boolean v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->i:Z

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->b:Landroid/content/Context;

    .line 53
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->a:Ljava/lang/ref/WeakReference;

    .line 55
    iput p3, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->d:I

    .line 56
    iput p4, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->e:I

    .line 57
    iput p5, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->f:I

    .line 59
    iput-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    .line 1114
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 1115
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getDesc()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 1117
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getIconUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 1118
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAppScore()I

    move-result p1

    int-to-double p3, p1

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setStarRating(Ljava/lang/Double;)V

    .line 1120
    invoke-virtual {p0, p2}, Lcom/anythink/network/gdt/GDTATNativeAd;->getCallToACtion(Lcom/qq/e/ads/nativ/NativeUnifiedADData;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    .line 1122
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 1124
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgList()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setImageUrlList(Ljava/util/List;)V

    .line 1126
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->isAppAd()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->setNativeInteractionType(I)V

    .line 1128
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result p1

    const/4 p3, 0x2

    if-ne p1, p3, :cond_0

    const-string p1, "1"

    .line 1129
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->mAdSourceType:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p1, "2"

    .line 1131
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->mAdSourceType:Ljava/lang/String;

    .line 1134
    :goto_0
    new-instance p1, Lcom/anythink/network/gdt/GDTATNativeAd$2;

    invoke-direct {p1, p0}, Lcom/anythink/network/gdt/GDTATNativeAd$2;-><init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V

    invoke-interface {p2, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->setNativeAdEventListener(Lcom/qq/e/ads/nativ/NativeADEventListener;)V

    return-void
.end method

.method private a(Landroid/view/View;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 342
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    if-eq p1, v0, :cond_1

    .line 343
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 344
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 345
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 346
    invoke-direct {p0, v1, p2}, Lcom/anythink/network/gdt/GDTATNativeAd;->a(Landroid/view/View;Ljava/util/List;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    .line 349
    :cond_1
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lcom/qq/e/ads/nativ/NativeUnifiedADData;)V
    .locals 2

    .line 114
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setTitle(Ljava/lang/String;)V

    .line 115
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getDesc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setDescriptionText(Ljava/lang/String;)V

    .line 117
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setIconImageUrl(Ljava/lang/String;)V

    .line 118
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAppScore()I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setStarRating(Ljava/lang/Double;)V

    .line 120
    invoke-virtual {p0, p1}, Lcom/anythink/network/gdt/GDTATNativeAd;->getCallToACtion(Lcom/qq/e/ads/nativ/NativeUnifiedADData;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setCallToActionText(Ljava/lang/String;)V

    .line 122
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setMainImageUrl(Ljava/lang/String;)V

    .line 124
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setImageUrlList(Ljava/util/List;)V

    .line 126
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->isAppAd()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/network/gdt/GDTATNativeAd;->setNativeInteractionType(I)V

    .line 128
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "1"

    .line 129
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->mAdSourceType:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, "2"

    .line 131
    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->mAdSourceType:Ljava/lang/String;

    .line 134
    :goto_0
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeAd$2;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeAd$2;-><init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V

    invoke-interface {p1, v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->setNativeAdEventListener(Lcom/qq/e/ads/nativ/NativeADEventListener;)V

    return-void
.end method


# virtual methods
.method public clear(Landroid/view/View;)V
    .locals 0

    .line 355
    invoke-super {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->clear(Landroid/view/View;)V

    .line 356
    invoke-virtual {p0}, Lcom/anythink/network/gdt/GDTATNativeAd;->onPause()V

    const/4 p1, 0x0

    .line 357
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    .line 358
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    return-void
.end method

.method public destroy()V
    .locals 2

    .line 378
    invoke-super {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->destroy()V

    .line 380
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 381
    invoke-interface {v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->setNativeAdEventListener(Lcom/qq/e/ads/nativ/NativeADEventListener;)V

    .line 382
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->destroy()V

    .line 383
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    .line 385
    :cond_0
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    .line 387
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->b:Landroid/content/Context;

    .line 388
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 389
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 390
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->a:Ljava/lang/ref/WeakReference;

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    if-eqz v0, :cond_2

    .line 394
    invoke-virtual {v0}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->removeAllViews()V

    .line 395
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    :cond_2
    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_2

    .line 171
    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 172
    invoke-super {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdMediaView([Ljava/lang/Object;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 175
    :cond_0
    new-instance p1, Lcom/qq/e/ads/nativ/MediaView;

    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->b:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/qq/e/ads/nativ/MediaView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    const/high16 v0, -0x1000000

    .line 176
    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/MediaView;->setBackgroundColor(I)V

    .line 177
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-nez p1, :cond_1

    .line 179
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/nativ/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    return-object p1

    .line 187
    :cond_2
    invoke-super {p0, p1}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;->getAdMediaView([Ljava/lang/Object;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getCallToACtion(Lcom/qq/e/ads/nativ/NativeUnifiedADData;)Ljava/lang/String;
    .locals 2

    .line 82
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getCTAText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getCTAText()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 88
    :cond_0
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->isAppAd()Z

    move-result v0

    .line 89
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAppStatus()I

    move-result p1

    const-string v1, "\u6d4f\u89c8"

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_5

    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    const/16 v0, 0x10

    if-eq p1, v0, :cond_5

    return-object v1

    :cond_2
    const-string p1, "\u5b89\u88c5"

    return-object p1

    :cond_3
    const-string p1, "\u66f4\u65b0"

    return-object p1

    :cond_4
    const-string p1, "\u542f\u52a8"

    return-object p1

    :cond_5
    const-string p1, "\u4e0b\u8f7d"

    return-object p1
.end method

.method public getCustomAdContainer()Landroid/view/ViewGroup;
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    return-object v0
.end method

.method public isNativeExpress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_0

    .line 371
    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->resume()V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 7

    .line 197
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    if-eqz v0, :cond_3

    .line 198
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 199
    invoke-direct {p0, p1, v5}, Lcom/anythink/network/gdt/GDTATNativeAd;->a(Landroid/view/View;Ljava/util/List;)V

    .line 200
    invoke-virtual {p0}, Lcom/anythink/network/gdt/GDTATNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v6, p1

    move-object v4, p2

    invoke-interface/range {v1 .. v6}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindAdToView(Landroid/content/Context;Lcom/qq/e/ads/nativ/widget/NativeAdContainer;Landroid/widget/FrameLayout$LayoutParams;Ljava/util/List;Ljava/util/List;)V

    .line 203
    :try_start_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    new-instance v0, Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-direct {v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;-><init>()V

    iget v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->d:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 204
    :goto_1
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v0

    iget v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->d:I

    if-ne v1, v3, :cond_2

    const/4 v2, 0x1

    .line 205
    :cond_2
    invoke-virtual {v0, v2}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v0

    iget v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->e:I

    .line 206
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayPolicy(I)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->build()Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object v0

    new-instance v1, Lcom/anythink/network/gdt/GDTATNativeAd$3;

    invoke-direct {v1, p0}, Lcom/anythink/network/gdt/GDTATNativeAd$3;-><init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V

    .line 203
    invoke-interface {p1, p2, v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindMediaView(Lcom/qq/e/ads/nativ/MediaView;Lcom/qq/e/ads/cfg/VideoOption;Lcom/qq/e/ads/nativ/NativeADMediaListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 257
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_3
    return-void
.end method

.method public prepare(Landroid/view/View;Ljava/util/List;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/FrameLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    .line 266
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    if-eqz v0, :cond_3

    .line 267
    invoke-virtual {p0}, Lcom/anythink/network/gdt/GDTATNativeAd;->getExtraInfo()Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->j:Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd$ExtraInfo;->getCustomViews()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v6, p1

    move-object v4, p3

    move-object v5, p2

    invoke-interface/range {v1 .. v6}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindAdToView(Landroid/content/Context;Lcom/qq/e/ads/nativ/widget/NativeAdContainer;Landroid/widget/FrameLayout$LayoutParams;Ljava/util/List;Ljava/util/List;)V

    .line 270
    :try_start_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->h:Lcom/qq/e/ads/nativ/MediaView;

    new-instance p3, Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-direct {p3}, Lcom/qq/e/ads/cfg/VideoOption$Builder;-><init>()V

    iget v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->d:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 271
    :goto_1
    invoke-virtual {p3, v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p3

    iget v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->d:I

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    .line 272
    :cond_2
    invoke-virtual {p3, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p3

    iget v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->e:I

    .line 273
    invoke-virtual {p3, v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayPolicy(I)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    move-result-object p3

    .line 274
    invoke-virtual {p3}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->build()Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object p3

    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeAd$4;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeAd$4;-><init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V

    .line 270
    invoke-interface {p1, p2, p3, v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindMediaView(Lcom/qq/e/ads/nativ/MediaView;Lcom/qq/e/ads/cfg/VideoOption;Lcom/qq/e/ads/nativ/NativeADMediaListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 324
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_3
    return-void
.end method

.method public registerDownloadConfirmListener()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd;->c:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    new-instance v1, Lcom/anythink/network/gdt/GDTATNativeAd$1;

    invoke-direct {v1, p0}, Lcom/anythink/network/gdt/GDTATNativeAd$1;-><init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V

    invoke-interface {v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V

    return-void
.end method
