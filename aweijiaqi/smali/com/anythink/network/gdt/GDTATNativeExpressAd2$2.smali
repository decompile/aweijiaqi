.class final Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/ads/nativ/express2/AdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

.field final synthetic b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iput-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClosed()V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->notifyAdDislikeClick()V

    return-void
.end method

.method public final onClick()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->notifyAdClicked()V

    return-void
.end method

.method public final onExposed()V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-virtual {v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->notifyAdImpression()V

    return-void
.end method

.method public final onRenderFail()V
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    const-string v1, ""

    const-string v2, "GDT onRenderFail"

    invoke-interface {v0, v1, v2}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public final onRenderSuccess()V
    .locals 4

    .line 129
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-static {v0, v1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    .line 131
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    move-result-object v0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->isVideoAd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const-string v1, "2"

    invoke-static {v0, v1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Ljava/lang/String;)Ljava/lang/String;

    .line 137
    :goto_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/anythink/network/gdt/a;->notifyLoaded([Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;)V

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;->b:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    return-void
.end method
