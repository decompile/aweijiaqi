.class final Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->c(Landroid/content/Context;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)V
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdLoaded()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->isVideoAd()Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->k(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->l(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    .line 153
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v1}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->m(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->n(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdDataLoaded()V

    :cond_2
    return-void
.end method

.method public final onClick()V
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->w(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->x(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClicked()V

    :cond_0
    return-void
.end method

.method public final onClose()V
    .locals 2

    .line 220
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->A(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->B(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdClose()V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->destroy()V

    .line 227
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v1}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final onError(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->q(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->r(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onExpose()V
    .locals 0

    return-void
.end method

.method public final onShow()V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->s(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->t(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdShow()V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->isVideoAd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->u(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->v(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoStart()V

    :cond_1
    return-void
.end method

.method public final onVideoCached()V
    .locals 3

    .line 167
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->isVideoAd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->o(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->p(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/core/api/ATCustomLoadListener;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/anythink/core/api/BaseAd;

    invoke-interface {v0, v1}, Lcom/anythink/core/api/ATCustomLoadListener;->onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V

    .line 172
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-virtual {v1}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    iget-object v2, v2, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->b:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method public final onVideoComplete()V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->y(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATInterstitialAdapter$2;->a:Lcom/anythink/network/gdt/GDTATInterstitialAdapter;

    invoke-static {v0}, Lcom/anythink/network/gdt/GDTATInterstitialAdapter;->z(Lcom/anythink/network/gdt/GDTATInterstitialAdapter;)Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/anythink/interstitial/unitgroup/api/CustomInterstitialEventListener;->onInterstitialAdVideoEnd()V

    :cond_0
    return-void
.end method
