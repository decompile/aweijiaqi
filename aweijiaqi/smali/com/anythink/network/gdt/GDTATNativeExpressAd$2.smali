.class final Lcom/anythink/network/gdt/GDTATNativeExpressAd$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeExpressAd;->registerDownloadConfirmListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$2;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V
    .locals 1

    .line 217
    new-instance v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;

    invoke-direct {v0}, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;-><init>()V

    .line 218
    iput-object p3, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->appInfoUrl:Ljava/lang/String;

    .line 219
    iput p2, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->scenes:I

    .line 220
    iput-object p4, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->confirmCallBack:Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;

    .line 221
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$2;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3, v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->notifyDownloadConfirm(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    return-void
.end method
