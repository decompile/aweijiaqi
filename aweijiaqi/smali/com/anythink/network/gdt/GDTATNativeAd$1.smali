.class final Lcom/anythink/network/gdt/GDTATNativeAd$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeAd;->registerDownloadConfirmListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATNativeAd;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeAd;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V
    .locals 3

    const-string v0, "GDTATNativeAd"

    const-string v1, "onDownloadConfirm...."

    .line 69
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeAd;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeAd;->g:Landroid/view/View;

    .line 71
    iget-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeAd;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/anythink/network/gdt/GDTATNativeAd;->g:Landroid/view/View;

    .line 72
    new-instance v1, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;

    invoke-direct {v1}, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;-><init>()V

    .line 73
    iput-object p3, v1, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->appInfoUrl:Ljava/lang/String;

    .line 74
    iput p2, v1, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->scenes:I

    .line 75
    iput-object p4, v1, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->confirmCallBack:Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;

    .line 76
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeAd;

    invoke-virtual {p2, p1, v0, v1}, Lcom/anythink/network/gdt/GDTATNativeAd;->notifyDownloadConfirm(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    return-void
.end method
