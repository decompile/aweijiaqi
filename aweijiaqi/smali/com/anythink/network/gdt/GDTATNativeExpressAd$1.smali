.class final Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeExpressAd;-><init>(Landroid/content/Context;Ljava/lang/String;IIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onADClicked(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    .line 150
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    invoke-virtual {p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->notifyAdClicked()V

    return-void
.end method

.method public final onADCloseOverlay(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public final onADClosed(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    .line 155
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    invoke-virtual {p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->notifyAdDislikeClick()V

    return-void
.end method

.method public final onADExposure(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    .line 145
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    invoke-virtual {p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->notifyAdImpression()V

    return-void
.end method

.method public final onADLeftApplication(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public final onADLoaded(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/qq/e/ads/nativ/NativeExpressADView;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 52
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/qq/e/ads/nativ/NativeExpressADView;

    .line 53
    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->render()V

    return-void

    .line 55
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    if-eqz p1, :cond_1

    .line 56
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    const-string v0, ""

    const-string v1, "GDT Ad request success but no Ad return."

    invoke-interface {p1, v0, v1}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_1
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public final onADOpenOverlay(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public final onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object v0, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public final onRenderFail(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 2

    .line 66
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    if-eqz p1, :cond_0

    .line 67
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    const-string v0, ""

    const-string v1, "GDT onRenderFail"

    invoke-interface {p1, v0, v1}, Lcom/anythink/network/gdt/a;->notifyError(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public final onRenderSuccess(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iput-object p1, v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    .line 76
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->getBoundData()Lcom/qq/e/comm/pi/AdData;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 78
    invoke-interface {p1}, Lcom/qq/e/comm/pi/AdData;->getAdPatternType()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 79
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const-string v0, "1"

    invoke-static {p1, v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->a(Lcom/anythink/network/gdt/GDTATNativeExpressAd;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 81
    :cond_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const-string v0, "2"

    invoke-static {p1, v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b(Lcom/anythink/network/gdt/GDTATNativeExpressAd;Ljava/lang/String;)Ljava/lang/String;

    .line 85
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->b:Lcom/qq/e/ads/nativ/NativeExpressADView;

    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1$1;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;)V

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/NativeExpressADView;->setMediaListener(Lcom/qq/e/ads/nativ/NativeExpressMediaListener;)V

    .line 137
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    if-eqz p1, :cond_2

    .line 138
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    iget-object p1, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/anythink/network/gdt/a;->notifyLoaded([Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;)V

    .line 140
    :cond_2
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd$1;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/anythink/network/gdt/GDTATNativeExpressAd;->c:Lcom/anythink/network/gdt/a;

    return-void
.end method
