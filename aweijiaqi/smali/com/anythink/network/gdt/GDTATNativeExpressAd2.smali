.class public Lcom/anythink/network/gdt/GDTATNativeExpressAd2;
.super Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;


# instance fields
.field a:Lcom/anythink/network/gdt/a;

.field private b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

.field private c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

.field private d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

.field private e:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIIII)V
    .locals 3

    .line 37
    invoke-direct {p0}, Lcom/anythink/nativead/unitgroup/api/CustomNativeAd;-><init>()V

    .line 38
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$1;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V

    const/4 v1, -0x1

    if-lez p3, :cond_0

    .line 63
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    int-to-float p3, p3

    invoke-static {p1, p3}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;F)I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, -0x1

    :goto_0
    if-lez p4, :cond_1

    .line 66
    invoke-static {}, Lcom/anythink/network/gdt/GDTATInitManager;->getInstance()Lcom/anythink/network/gdt/GDTATInitManager;

    int-to-float p4, p4

    invoke-static {p1, p4}, Lcom/anythink/network/gdt/GDTATInitManager;->a(Landroid/content/Context;F)I

    move-result p4

    goto :goto_1

    :cond_1
    const/4 p4, -0x2

    .line 69
    :goto_1
    new-instance v2, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    invoke-direct {v2, p1, p2, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;)V

    iput-object v2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->e:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    .line 70
    invoke-virtual {v2, p3, p4}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setAdSize(II)V

    const/4 p1, 0x1

    if-eq p6, p1, :cond_3

    const/4 p2, 0x2

    if-eq p6, p2, :cond_2

    .line 82
    sget-object p2, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->WIFI:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_2

    .line 79
    :cond_2
    sget-object p2, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->NEVER:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_2

    .line 76
    :cond_3
    sget-object p2, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->ALWAYS:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    .line 86
    :goto_2
    new-instance p3, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-direct {p3}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;-><init>()V

    const/4 p4, 0x0

    if-ne p5, p1, :cond_4

    const/4 p6, 0x1

    goto :goto_3

    :cond_4
    const/4 p6, 0x0

    .line 87
    :goto_3
    invoke-virtual {p3, p6}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object p3

    if-ne p5, p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 p1, 0x0

    .line 88
    :goto_4
    invoke-virtual {p3, p1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayPolicy(Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    move-result-object p1

    if-eq p7, v1, :cond_6

    .line 92
    invoke-virtual {p1, p7}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setMaxVideoDuration(I)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    .line 95
    :cond_6
    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->build()Lcom/qq/e/ads/nativ/express2/VideoOption2;

    move-result-object p1

    .line 96
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->e:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setVideoOption2(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->mAdSourceType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V
    .locals 1

    .line 1103
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

    .line 1144
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$3;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$3;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    .line 1176
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

    invoke-interface {p1, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setAdEventListener(Lcom/qq/e/ads/nativ/express2/AdEventListener;)V

    .line 1177
    iget-object p0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    invoke-interface {p1, p0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setMediaListener(Lcom/qq/e/ads/nativ/express2/MediaEventListener;)V

    .line 1179
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->render()V

    return-void
.end method

.method private a(Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V
    .locals 1

    .line 103
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$2;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

    .line 144
    new-instance v0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$3;

    invoke-direct {v0, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$3;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V

    iput-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    .line 176
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

    invoke-interface {p1, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setAdEventListener(Lcom/qq/e/ads/nativ/express2/AdEventListener;)V

    .line 177
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    invoke-interface {p1, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setMediaListener(Lcom/qq/e/ads/nativ/express2/MediaEventListener;)V

    .line 179
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->render()V

    return-void
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    return-object p1
.end method

.method static synthetic b(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->mAdSourceType:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method protected final a(Lcom/anythink/network/gdt/a;)V
    .locals 1

    .line 183
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    .line 184
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->e:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 185
    invoke-virtual {p1, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->loadAd(I)V

    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 222
    invoke-interface {v0, v1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setMediaListener(Lcom/qq/e/ads/nativ/express2/MediaEventListener;)V

    .line 223
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-interface {v0, v1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setAdEventListener(Lcom/qq/e/ads/nativ/express2/AdEventListener;)V

    .line 224
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    .line 226
    :cond_0
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->c:Lcom/qq/e/ads/nativ/express2/AdEventListener;

    .line 227
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    .line 228
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->e:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    .line 230
    iput-object v1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->a:Lcom/anythink/network/gdt/a;

    return-void
.end method

.method public varargs getAdMediaView([Ljava/lang/Object;)Landroid/view/View;
    .locals 0

    .line 213
    iget-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz p1, :cond_0

    .line 214
    invoke-interface {p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->getAdView()Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public isNativeExpress()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public registerDownloadConfirmListener()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->b:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v0, :cond_0

    .line 192
    new-instance v1, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$4;

    invoke-direct {v1, p0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$4;-><init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V

    invoke-interface {v0, v1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V

    :cond_0
    return-void
.end method
