.class final Lcom/anythink/network/gdt/GDTATNativeExpressAd2$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->registerDownloadConfirmListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;


# direct methods
.method constructor <init>(Lcom/anythink/network/gdt/GDTATNativeExpressAd2;)V
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$4;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V
    .locals 1

    .line 195
    new-instance v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;

    invoke-direct {v0}, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;-><init>()V

    .line 196
    iput-object p3, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->appInfoUrl:Ljava/lang/String;

    .line 197
    iput p2, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->scenes:I

    .line 198
    iput-object p4, v0, Lcom/anythink/network/gdt/GDTDownloadFirmInfo;->confirmCallBack:Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;

    .line 199
    iget-object p2, p0, Lcom/anythink/network/gdt/GDTATNativeExpressAd2$4;->a:Lcom/anythink/network/gdt/GDTATNativeExpressAd2;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3, v0}, Lcom/anythink/network/gdt/GDTATNativeExpressAd2;->notifyDownloadConfirm(Landroid/content/Context;Landroid/view/View;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    return-void
.end method
