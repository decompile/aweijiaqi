.class final Lcom/anythink/splashad/a/c$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/splashad/a/c;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/anythink/splashad/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/d/b;

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Landroid/view/ViewGroup;

.field final synthetic d:Lcom/anythink/splashad/a/a;

.field final synthetic e:Lcom/anythink/splashad/a/c;


# direct methods
.method constructor <init>(Lcom/anythink/splashad/a/c;Lcom/anythink/core/common/d/b;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/anythink/splashad/a/a;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/anythink/splashad/a/c$1;->e:Lcom/anythink/splashad/a/c;

    iput-object p2, p0, Lcom/anythink/splashad/a/c$1;->a:Lcom/anythink/core/common/d/b;

    iput-object p3, p0, Lcom/anythink/splashad/a/c$1;->b:Landroid/app/Activity;

    iput-object p4, p0, Lcom/anythink/splashad/a/c$1;->c:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/anythink/splashad/a/c$1;->d:Lcom/anythink/splashad/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 121
    iget-object v0, p0, Lcom/anythink/splashad/a/c$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    check-cast v0, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    .line 122
    iget-object v1, p0, Lcom/anythink/splashad/a/c$1;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 123
    invoke-virtual {v0, v1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->refreshActivityContext(Landroid/app/Activity;)V

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/anythink/splashad/a/c$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v4

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    if-eqz v4, :cond_1

    .line 131
    iget-object v1, p0, Lcom/anythink/splashad/a/c$1;->e:Lcom/anythink/splashad/a/c;

    invoke-static {v1}, Lcom/anythink/splashad/a/c;->a(Lcom/anythink/splashad/a/c;)Ljava/lang/String;

    move-result-object v1

    .line 1429
    iput-object v1, v4, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    .line 132
    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6, v7}, Lcom/anythink/core/common/g/g;->a(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/anythink/core/common/d/d;->f(Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/anythink/splashad/a/c$1;->e:Lcom/anythink/splashad/a/c;

    invoke-static {v1}, Lcom/anythink/splashad/a/c;->b(Lcom/anythink/splashad/a/c;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/anythink/core/common/g/n;->a(Landroid/content/Context;Lcom/anythink/core/common/d/d;)V

    .line 138
    :cond_1
    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 139
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/splashad/a/c$1;->e:Lcom/anythink/splashad/a/c;

    invoke-static {v2}, Lcom/anythink/splashad/a/c;->c(Lcom/anythink/splashad/a/c;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/splashad/a/c$1;->a:Lcom/anythink/core/common/d/b;

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Lcom/anythink/core/common/d/b;)V

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/anythink/splashad/a/c$1;->e:Lcom/anythink/splashad/a/c;

    invoke-static {v1}, Lcom/anythink/splashad/a/c;->d(Lcom/anythink/splashad/a/c;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v5

    invoke-virtual/range {v2 .. v7}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V

    .line 144
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    new-instance v2, Lcom/anythink/splashad/a/c$1$1;

    invoke-direct {v2, p0, v0}, Lcom/anythink/splashad/a/c$1$1;-><init>(Lcom/anythink/splashad/a/c$1;Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;)V

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method
