.class public final Lcom/anythink/splashad/a/c;
.super Lcom/anythink/core/common/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/d<",
        "Lcom/anythink/splashad/a/g;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/anythink/splashad/a/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/anythink/core/common/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lcom/anythink/splashad/a/g;)Lcom/anythink/core/common/f;
    .locals 2

    .line 81
    new-instance v0, Lcom/anythink/splashad/a/e;

    iget-object v1, p0, Lcom/anythink/splashad/a/g;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/splashad/a/e;-><init>(Landroid/content/Context;)V

    .line 82
    iget v1, p0, Lcom/anythink/splashad/a/g;->c:I

    .line 1109
    iput v1, v0, Lcom/anythink/splashad/a/e;->a:I

    .line 83
    iget-object p0, p0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    .line 1165
    iput-object p0, v0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/splashad/a/c;
    .locals 2

    .line 50
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    instance-of v1, v0, Lcom/anythink/splashad/a/c;

    if-nez v1, :cond_1

    .line 52
    :cond_0
    new-instance v0, Lcom/anythink/splashad/a/c;

    invoke-direct {v0, p0, p1}, Lcom/anythink/splashad/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;Lcom/anythink/core/common/d;)V

    .line 55
    :cond_1
    check-cast v0, Lcom/anythink/splashad/a/c;

    return-object v0
.end method

.method static synthetic a(Lcom/anythink/splashad/a/c;)Ljava/lang/String;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/anythink/splashad/a/c;->g:Ljava/lang/String;

    return-object p0
.end method

.method private static a(Lcom/anythink/splashad/a/g;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 179
    iget-object p0, p0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p0, p1}, Lcom/anythink/splashad/a/b;->onCallbackNoAdError(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/anythink/splashad/a/g;)V
    .locals 0

    .line 76
    iget-object p1, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p1, p0}, Lcom/anythink/splashad/a/b;->setRequestId(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/splashad/a/g;)Z
    .locals 7

    .line 164
    new-instance v0, Lcom/anythink/splashad/a/d;

    iget-object v1, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/splashad/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    .line 165
    iget-object v1, p3, Lcom/anythink/splashad/a/g;->a:Landroid/content/Context;

    iget-object v4, p3, Lcom/anythink/splashad/a/g;->f:Lcom/anythink/core/api/ATMediationRequestInfo;

    iget-object v5, p3, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    iget v6, p3, Lcom/anythink/splashad/a/g;->c:I

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/anythink/splashad/a/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/api/ATMediationRequestInfo;Lcom/anythink/splashad/a/b;I)V

    const/4 p1, 0x1

    return p1
.end method

.method static synthetic b(Lcom/anythink/splashad/a/c;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method private static b(Lcom/anythink/splashad/a/g;)V
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 172
    iget-object p0, p0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p0}, Lcom/anythink/splashad/a/b;->onCallbackAdLoaded()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/anythink/splashad/a/c;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/anythink/splashad/a/c;)Landroid/content/Context;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/splashad/a/d;->b()Lcom/anythink/core/common/d/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-object v0

    .line 93
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;

    move-result-object p1

    return-object p1
.end method

.method public final declared-synchronized a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/anythink/splashad/a/a;)V
    .locals 8

    monitor-enter p0

    const/4 v0, 0x1

    .line 97
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/anythink/splashad/a/c;->a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;

    move-result-object v3

    if-nez v3, :cond_0

    .line 99
    sget-object p1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string p2, "Splash No Cache."

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    :cond_0
    if-eqz v3, :cond_2

    .line 103
    :try_start_1
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v1, :cond_2

    .line 104
    invoke-virtual {p0, v3}, Lcom/anythink/splashad/a/c;->a(Lcom/anythink/core/common/d/b;)V

    .line 108
    invoke-virtual {p0}, Lcom/anythink/splashad/a/c;->d()V

    .line 111
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->e()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {v3, v1}, Lcom/anythink/core/common/d/b;->a(I)V

    .line 114
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    invoke-virtual {v0}, Lcom/anythink/splashad/a/d;->b()Lcom/anythink/core/common/d/b;

    move-result-object v0

    if-ne v0, v3, :cond_1

    .line 115
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    const/4 v1, 0x0

    .line 1268
    iput-object v1, v0, Lcom/anythink/splashad/a/d;->d:Lcom/anythink/core/common/d/b;

    .line 118
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v7, Lcom/anythink/splashad/a/c$1;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/anythink/splashad/a/c$1;-><init>(Lcom/anythink/splashad/a/c;Lcom/anythink/core/common/d/b;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/anythink/splashad/a/a;)V

    invoke-virtual {v0, v7}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Landroid/content/Context;Lcom/anythink/core/api/ATMediationRequestInfo;Lcom/anythink/splashad/a/b;I)V
    .locals 1

    .line 64
    new-instance v0, Lcom/anythink/splashad/a/g;

    invoke-direct {v0}, Lcom/anythink/splashad/a/g;-><init>()V

    .line 65
    iput-object p1, v0, Lcom/anythink/splashad/a/g;->a:Landroid/content/Context;

    .line 66
    iput-object p3, v0, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    .line 67
    iput-object p2, v0, Lcom/anythink/splashad/a/g;->f:Lcom/anythink/core/api/ATMediationRequestInfo;

    .line 68
    iput p4, v0, Lcom/anythink/splashad/a/g;->c:I

    .line 70
    iget-object p1, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    iget-object p2, p0, Lcom/anythink/splashad/a/c;->c:Ljava/lang/String;

    const-string p3, "4"

    invoke-super {p0, p1, p3, p2, v0}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;)V
    .locals 1

    .line 39
    check-cast p1, Lcom/anythink/splashad/a/g;

    .line 4171
    iget-object v0, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 4172
    iget-object p1, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p1}, Lcom/anythink/splashad/a/b;->onCallbackAdLoaded()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Lcom/anythink/core/common/g;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 39
    check-cast p1, Lcom/anythink/splashad/a/g;

    .line 3178
    iget-object v0, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 3179
    iget-object p1, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p1, p2}, Lcom/anythink/splashad/a/b;->onCallbackNoAdError(Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/String;Lcom/anythink/core/common/g;)V
    .locals 0

    .line 39
    check-cast p2, Lcom/anythink/splashad/a/g;

    .line 7076
    iget-object p2, p2, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p2, p1}, Lcom/anythink/splashad/a/b;->setRequestId(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)Z
    .locals 7

    .line 39
    check-cast p3, Lcom/anythink/splashad/a/g;

    .line 6164
    new-instance v0, Lcom/anythink/splashad/a/d;

    iget-object v1, p0, Lcom/anythink/splashad/a/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/splashad/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    .line 6165
    iget-object v1, p3, Lcom/anythink/splashad/a/g;->a:Landroid/content/Context;

    iget-object v4, p3, Lcom/anythink/splashad/a/g;->f:Lcom/anythink/core/api/ATMediationRequestInfo;

    iget-object v5, p3, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    iget v6, p3, Lcom/anythink/splashad/a/g;->c:I

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/anythink/splashad/a/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/api/ATMediationRequestInfo;Lcom/anythink/splashad/a/b;I)V

    const/4 p1, 0x1

    return p1
.end method

.method public final synthetic b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;
    .locals 2

    .line 39
    check-cast p1, Lcom/anythink/splashad/a/g;

    .line 5081
    new-instance v0, Lcom/anythink/splashad/a/e;

    iget-object v1, p1, Lcom/anythink/splashad/a/g;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/anythink/splashad/a/e;-><init>(Landroid/content/Context;)V

    .line 5082
    iget v1, p1, Lcom/anythink/splashad/a/g;->c:I

    .line 5109
    iput v1, v0, Lcom/anythink/splashad/a/e;->a:I

    .line 5083
    iget-object p1, p1, Lcom/anythink/splashad/a/g;->b:Lcom/anythink/splashad/a/b;

    .line 5165
    iput-object p1, v0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .line 207
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    const-string v1, "Splash FetchAd Timeout."

    const-string v2, ""

    const-string v3, "2001"

    const/4 v4, 0x1

    const-string v5, "0"

    const-string v6, "4"

    if-eqz v0, :cond_0

    .line 1312
    new-instance v7, Lcom/anythink/core/common/d/d;

    invoke-direct {v7}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 1313
    iget-object v8, v0, Lcom/anythink/splashad/a/d;->f:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 1314
    iget-object v0, v0, Lcom/anythink/splashad/a/d;->e:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 1315
    invoke-virtual {v7, v6}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 1316
    invoke-virtual {v7, v5}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    .line 1317
    invoke-virtual {v7, v4}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 1318
    invoke-static {v3, v2, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    .line 209
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    const/4 v7, 0x0

    .line 2264
    iput-object v7, v0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    .line 210
    iput-object v7, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    .line 213
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f;

    .line 215
    iget-object v7, p0, Lcom/anythink/splashad/a/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 217
    instance-of p1, v0, Lcom/anythink/splashad/a/e;

    if-eqz p1, :cond_2

    .line 218
    check-cast v0, Lcom/anythink/splashad/a/e;

    invoke-virtual {v0}, Lcom/anythink/splashad/a/e;->g()V

    return-void

    .line 221
    :cond_1
    new-instance v0, Lcom/anythink/core/common/d/d;

    invoke-direct {v0}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 222
    iget-object v7, p0, Lcom/anythink/splashad/a/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v0, p1}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v0, v6}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v0, v5}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v0, v4}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 227
    invoke-static {v3, v2, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    :cond_2
    return-void
.end method

.method public final e()Z
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/anythink/splashad/a/c;->a:Lcom/anythink/splashad/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/splashad/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
