.class public final Lcom/anythink/splashad/a/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/splashad/a/d$a;
    }
.end annotation


# instance fields
.field a:Z

.field b:Lcom/anythink/splashad/a/b;

.field c:J

.field d:Lcom/anythink/core/common/d/b;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field private g:Landroid/content/Context;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/splashad/a/d;->g:Landroid/content/Context;

    return-void
.end method

.method private b(Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 228
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/a/d$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/splashad/a/d$2;-><init>(Lcom/anythink/splashad/a/d;Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 156
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/a/d$1;

    invoke-direct {v1, p0}, Lcom/anythink/splashad/a/d$1;-><init>(Lcom/anythink/splashad/a/d;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private d()V
    .locals 1

    const/4 v0, 0x0

    .line 264
    iput-object v0, p0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    return-void
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    .line 268
    iput-object v0, p0, Lcom/anythink/splashad/a/d;->d:Lcom/anythink/core/common/d/b;

    return-void
.end method

.method private f()V
    .locals 4

    .line 312
    new-instance v0, Lcom/anythink/core/common/d/d;

    invoke-direct {v0}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 313
    iget-object v1, p0, Lcom/anythink/splashad/a/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 314
    iget-object v1, p0, Lcom/anythink/splashad/a/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    const-string v1, "4"

    .line 315
    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    const-string v1, "0"

    .line 316
    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 317
    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->b(Z)V

    const-string v1, "2001"

    const-string v2, ""

    const-string v3, "Splash FetchAd Timeout."

    .line 318
    invoke-static {v1, v2, v3}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/api/ATMediationRequestInfo;Lcom/anythink/splashad/a/b;I)V
    .locals 4

    const-string v0, ""

    .line 176
    iput-object p5, p0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    .line 178
    iput-object p3, p0, Lcom/anythink/splashad/a/d;->e:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Lcom/anythink/splashad/a/d;->f:Ljava/lang/String;

    .line 181
    new-instance p5, Lcom/anythink/core/common/d/d;

    invoke-direct {p5}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 182
    invoke-virtual {p5, p2}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p5, p3}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p4}, Lcom/anythink/core/api/ATMediationRequestInfo;->getNetworkFirmId()I

    move-result p3

    invoke-virtual {p5, p3}, Lcom/anythink/core/common/d/d;->o(I)V

    const-string p3, "4"

    .line 185
    invoke-virtual {p5, p3}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p4}, Lcom/anythink/core/api/ATMediationRequestInfo;->getAdSourceId()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    const-string v1, "0"

    if-eqz p3, :cond_0

    move-object p3, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p4}, Lcom/anythink/core/api/ATMediationRequestInfo;->getAdSourceId()Ljava/lang/String;

    move-result-object p3

    :goto_0
    invoke-virtual {p5, p3}, Lcom/anythink/core/common/d/d;->j(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p5, v1}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    const/4 p3, 0x1

    .line 188
    invoke-virtual {p5, p3}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 192
    :try_start_0
    invoke-virtual {p4}, Lcom/anythink/core/api/ATMediationRequestInfo;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/i;->a(Ljava/lang/String;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    .line 193
    instance-of v2, v1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v2, :cond_1

    .line 194
    move-object v2, v1

    check-cast v2, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    .line 195
    invoke-virtual {v2, p6}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->setFetchAdTimeout(I)V

    .line 196
    iput-boolean p3, p0, Lcom/anythink/splashad/a/d;->h:Z

    const/4 p6, 0x0

    .line 197
    iput-boolean p6, p0, Lcom/anythink/splashad/a/d;->a:Z

    .line 199
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/anythink/splashad/a/d;->c:J

    .line 201
    invoke-virtual {v1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkName()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p5, p6}, Lcom/anythink/core/common/d/d;->r(Ljava/lang/String;)V

    const/4 p6, 0x2

    .line 1445
    iput p6, p5, Lcom/anythink/core/common/d/d;->n:I

    .line 204
    invoke-virtual {v1, p5}, Lcom/anythink/core/api/ATBaseAdAdapter;->setTrackingInfo(Lcom/anythink/core/common/d/d;)V

    .line 205
    sget-object p6, Lcom/anythink/core/common/b/e$e;->a:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->h:Ljava/lang/String;

    invoke-static {p5, p6, v2, v0}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object p6, p0, Lcom/anythink/splashad/a/d;->g:Landroid/content/Context;

    invoke-static {p6}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p6

    const/16 v2, 0xa

    invoke-virtual {p6, v2, p5}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 208
    iget-object p6, p0, Lcom/anythink/splashad/a/d;->g:Landroid/content/Context;

    invoke-static {p6}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p6

    invoke-virtual {p6, p3, p5}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 213
    invoke-virtual {p4}, Lcom/anythink/core/api/ATMediationRequestInfo;->getRequestParamMap()Ljava/util/Map;

    move-result-object p3

    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p4

    invoke-virtual {p4, p2}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p2

    new-instance p4, Lcom/anythink/splashad/a/d$a;

    move-object p5, v1

    check-cast p5, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-direct {p4, p0, p5}, Lcom/anythink/splashad/a/d$a;-><init>(Lcom/anythink/splashad/a/d;Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;)V

    invoke-virtual {v1, p1, p3, p2, p4}, Lcom/anythink/core/api/ATBaseAdAdapter;->internalLoad(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Lcom/anythink/core/api/ATCustomLoadListener;)V

    return-void

    .line 215
    :cond_1
    new-instance p1, Ljava/lang/Exception;

    const-string p2, "The class isn\'t instanceof CustomSplashAdapter"

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 218
    iget-object p2, p0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    if-eqz p2, :cond_2

    .line 219
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string p2, "2002"

    invoke-static {p2, v0, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    .line 220
    iget-object p2, p0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    invoke-virtual {p2, p1}, Lcom/anythink/splashad/a/b;->onCallbackNoAdError(Lcom/anythink/core/api/AdError;)V

    :cond_2
    const/4 p1, 0x0

    .line 222
    iput-object p1, p0, Lcom/anythink/splashad/a/d;->b:Lcom/anythink/splashad/a/b;

    return-void
.end method

.method public final a(Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;)V
    .locals 6

    .line 48
    iget-boolean v0, p0, Lcom/anythink/splashad/a/d;->a:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 56
    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/anythink/splashad/a/d;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/d;->d(J)V

    .line 57
    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getNetworkPlacementId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->e(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    sget-object v2, Lcom/anythink/core/common/b/e$e;->b:Ljava/lang/String;

    sget-object v3, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v1, v2, v3, v4}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/anythink/splashad/a/d;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 62
    iget-object v1, p0, Lcom/anythink/splashad/a/d;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 64
    new-instance v1, Lcom/anythink/core/common/d/b;

    invoke-direct {v1}, Lcom/anythink/core/common/d/b;-><init>()V

    .line 65
    invoke-virtual {v1, v0}, Lcom/anythink/core/common/d/b;->b(I)V

    .line 66
    invoke-virtual {v1, p1}, Lcom/anythink/core/common/d/b;->a(Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/b;->c(J)V

    const-wide/32 v2, 0x927c0

    .line 68
    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/b;->b(J)V

    .line 69
    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/anythink/core/common/d/b;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/b;->a(J)V

    .line 72
    iput-object v1, p0, Lcom/anythink/splashad/a/d;->d:Lcom/anythink/core/common/d/b;

    :cond_1
    const/4 p1, 0x1

    .line 76
    iput-boolean p1, p0, Lcom/anythink/splashad/a/d;->a:Z

    .line 77
    iput-boolean v0, p0, Lcom/anythink/splashad/a/d;->h:Z

    .line 1156
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    new-instance v0, Lcom/anythink/splashad/a/d$1;

    invoke-direct {v0, p0}, Lcom/anythink/splashad/a/d$1;-><init>(Lcom/anythink/splashad/a/d;)V

    invoke-virtual {p1, v0}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;Lcom/anythink/core/api/AdError;)V
    .locals 4

    .line 84
    iget-boolean v0, p0, Lcom/anythink/splashad/a/d;->a:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    sget-object v1, Lcom/anythink/core/common/b/e$e;->b:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    .line 93
    iput-boolean v0, p0, Lcom/anythink/splashad/a/d;->a:Z

    const/4 v0, 0x0

    .line 94
    iput-boolean v0, p0, Lcom/anythink/splashad/a/d;->h:Z

    .line 1228
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/a/d$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/splashad/a/d$2;-><init>(Lcom/anythink/splashad/a/d;Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected final a()Z
    .locals 1

    .line 169
    iget-boolean v0, p0, Lcom/anythink/splashad/a/d;->h:Z

    return v0
.end method

.method public final b()Lcom/anythink/core/common/d/b;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/anythink/splashad/a/d;->d:Lcom/anythink/core/common/d/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->e()I

    move-result v0

    if-gtz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/anythink/splashad/a/d;->d:Lcom/anythink/core/common/d/b;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
