.class public final Lcom/anythink/splashad/a/e;
.super Lcom/anythink/core/common/f;


# instance fields
.field N:Lcom/anythink/splashad/a/b;

.field a:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 105
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a(I)V
    .locals 0

    .line 109
    iput p1, p0, Lcom/anythink/splashad/a/e;->a:I

    return-void
.end method

.method private a(Lcom/anythink/splashad/a/b;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/anythink/splashad/a/b;->onCallbackAdLoaded()V

    :cond_0
    const/4 v0, 0x0

    .line 125
    iput-object v0, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 1

    .line 157
    instance-of v0, p1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_0

    .line 158
    check-cast p1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    iget v0, p0, Lcom/anythink/splashad/a/e;->a:I

    invoke-virtual {p1, v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->setFetchAdTimeout(I)V

    :cond_0
    return-void
.end method

.method protected final a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V
    .locals 0

    .line 114
    invoke-super {p0, p1, p2}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    .line 115
    instance-of p2, p1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz p2, :cond_0

    .line 116
    check-cast p1, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-virtual {p1}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->cleanImpressionListener()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0, p1}, Lcom/anythink/splashad/a/b;->onCallbackNoAdError(Lcom/anythink/core/api/AdError;)V

    :cond_0
    const/4 p1, 0x0

    .line 133
    iput-object p1, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    .line 179
    iput-object v0, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    .line 172
    iput-object v0, p0, Lcom/anythink/splashad/a/e;->N:Lcom/anythink/splashad/a/b;

    .line 173
    invoke-super {p0}, Lcom/anythink/core/common/f;->f()V

    return-void
.end method

.method public final g()V
    .locals 0

    .line 190
    invoke-virtual {p0}, Lcom/anythink/splashad/a/e;->f()V

    .line 191
    invoke-virtual {p0}, Lcom/anythink/splashad/a/e;->c()V

    return-void
.end method
