.class public final Lcom/anythink/splashad/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/splashad/unitgroup/api/CustomSplashEventListener;


# instance fields
.field a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

.field b:Lcom/anythink/splashad/a/a;


# direct methods
.method public constructor <init>(Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;Lcom/anythink/splashad/a/a;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    .line 30
    iput-object p2, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    return-void
.end method


# virtual methods
.method public final onDeeplinkCallback(Z)V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/anythink/splashad/a/a;->onDeeplinkCallback(Lcom/anythink/core/api/ATAdInfo;Z)V

    :cond_0
    return-void
.end method

.method public final onDownloadConfirm(Landroid/content/Context;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/anythink/splashad/a/a;->onDownloadConfirm(Landroid/content/Context;Lcom/anythink/core/api/ATAdInfo;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    :cond_0
    return-void
.end method

.method public final onSplashAdClicked()V
    .locals 4

    .line 52
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 57
    sget-object v1, Lcom/anythink/core/common/b/e$e;->d:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    if-eqz v0, :cond_1

    .line 62
    iget-object v1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/splashad/a/a;->onAdClick(Lcom/anythink/core/api/ATAdInfo;)V

    :cond_1
    return-void
.end method

.method public final onSplashAdDismiss()V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    sget-object v1, Lcom/anythink/core/common/b/e$e;->e:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    if-eqz v0, :cond_1

    .line 73
    iget-object v1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/splashad/a/a;->onCallbackAdDismiss(Lcom/anythink/core/api/ATAdInfo;)V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_2

    .line 77
    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->cleanImpressionListener()V

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_3

    .line 81
    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->destory()V

    :cond_3
    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    return-void
.end method

.method public final onSplashAdShow()V
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    .line 38
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-virtual {v2}, Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/anythink/core/common/f/a;->a(Lcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;)V

    .line 40
    sget-object v1, Lcom/anythink/core/common/b/e$e;->c:Ljava/lang/String;

    sget-object v2, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/anythink/splashad/a/f;->b:Lcom/anythink/splashad/a/a;

    if-eqz v0, :cond_1

    .line 45
    iget-object v1, p0, Lcom/anythink/splashad/a/f;->a:Lcom/anythink/splashad/unitgroup/api/CustomSplashAdapter;

    invoke-static {v1}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/splashad/a/a;->onAdShow(Lcom/anythink/core/api/ATAdInfo;)V

    :cond_1
    return-void
.end method
