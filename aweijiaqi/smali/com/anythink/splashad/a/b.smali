.class public abstract Lcom/anythink/splashad/a/b;
.super Ljava/lang/Object;


# instance fields
.field mHasReturn:Z

.field mRequestId:Ljava/lang/String;

.field mTimer:Ljava/util/Timer;

.field mTimerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/anythink/splashad/a/b;->mHasReturn:Z

    .line 21
    new-instance v0, Lcom/anythink/splashad/a/b$1;

    invoke-direct {v0, p0}, Lcom/anythink/splashad/a/b$1;-><init>(Lcom/anythink/splashad/a/b;)V

    iput-object v0, p0, Lcom/anythink/splashad/a/b;->mTimerTask:Ljava/util/TimerTask;

    return-void
.end method


# virtual methods
.method public abstract onAdLoaded(Ljava/lang/String;)V
.end method

.method public onCallbackAdLoaded()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 50
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/splashad/a/b;->mHasReturn:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 51
    iput-boolean v0, p0, Lcom/anythink/splashad/a/b;->mHasReturn:Z

    .line 53
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mRequestId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/anythink/splashad/a/b;->onAdLoaded(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onCallbackNoAdError(Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 63
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/splashad/a/b;->mHasReturn:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 64
    iput-boolean v0, p0, Lcom/anythink/splashad/a/b;->mHasReturn:Z

    .line 66
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mRequestId:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/anythink/splashad/a/b;->onNoAdError(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V

    :cond_1
    return-void
.end method

.method public abstract onNoAdError(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
.end method

.method public abstract onTimeout(Ljava/lang/String;)V
.end method

.method public setRequestId(Ljava/lang/String;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/anythink/splashad/a/b;->mRequestId:Ljava/lang/String;

    return-void
.end method

.method public startCountDown(I)V
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/anythink/splashad/a/b;->mTimer:Ljava/util/Timer;

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/anythink/splashad/a/b;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/anythink/splashad/a/b;->mTimerTask:Ljava/util/TimerTask;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method
