.class final Lcom/anythink/splashad/api/ATSplashAd$2;
.super Lcom/anythink/splashad/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/splashad/api/ATSplashAd;->show(Landroid/app/Activity;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/anythink/splashad/api/ATSplashAd;


# direct methods
.method constructor <init>(Lcom/anythink/splashad/api/ATSplashAd;)V
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/anythink/splashad/api/ATSplashAd$2;->this$0:Lcom/anythink/splashad/api/ATSplashAd;

    invoke-direct {p0}, Lcom/anythink/splashad/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdClick(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 248
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd$2$4;

    invoke-direct {v1, p0, p1}, Lcom/anythink/splashad/api/ATSplashAd$2$4;-><init>(Lcom/anythink/splashad/api/ATSplashAd$2;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAdDismiss(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 260
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd$2$5;

    invoke-direct {v1, p0, p1}, Lcom/anythink/splashad/api/ATSplashAd$2$5;-><init>(Lcom/anythink/splashad/api/ATSplashAd$2;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAdShow(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 236
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd$2$3;

    invoke-direct {v1, p0, p1}, Lcom/anythink/splashad/api/ATSplashAd$2$3;-><init>(Lcom/anythink/splashad/api/ATSplashAd$2;Lcom/anythink/core/api/ATAdInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onDeeplinkCallback(Lcom/anythink/core/api/ATAdInfo;Z)V
    .locals 2

    .line 212
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd$2$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/splashad/api/ATSplashAd$2$1;-><init>(Lcom/anythink/splashad/api/ATSplashAd$2;Lcom/anythink/core/api/ATAdInfo;Z)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onDownloadConfirm(Landroid/content/Context;Lcom/anythink/core/api/ATAdInfo;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V
    .locals 2

    .line 224
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd$2$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/anythink/splashad/api/ATSplashAd$2$2;-><init>(Lcom/anythink/splashad/api/ATSplashAd$2;Landroid/content/Context;Lcom/anythink/core/api/ATAdInfo;Lcom/anythink/core/api/ATNetworkConfirmInfo;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method
