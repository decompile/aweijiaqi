.class public final Lcom/anythink/core/common/f$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/api/ATCustomLoadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/core/common/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field a:Lcom/anythink/core/api/ATBaseAdAdapter;

.field final synthetic b:Lcom/anythink/core/common/f;


# direct methods
.method private constructor <init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    .line 1264
    iput-object p1, p0, Lcom/anythink/core/common/f$a;->b:Lcom/anythink/core/common/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1265
    iput-object p2, p0, Lcom/anythink/core/common/f$a;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    return-void
.end method

.method synthetic constructor <init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;B)V
    .locals 0

    .line 1261
    invoke-direct {p0, p1, p2}, Lcom/anythink/core/common/f$a;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    return-void
.end method


# virtual methods
.method public final varargs onAdCacheLoaded([Lcom/anythink/core/api/BaseAd;)V
    .locals 2

    .line 1280
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/f$a$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/core/common/f$a$2;-><init>(Lcom/anythink/core/common/f$a;[Lcom/anythink/core/api/BaseAd;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAdDataLoaded()V
    .locals 2

    .line 1270
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/f$a$1;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/f$a$1;-><init>(Lcom/anythink/core/common/f$a;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onAdLoadError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1294
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/f$a$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/anythink/core/common/f$a$3;-><init>(Lcom/anythink/core/common/f$a;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method
