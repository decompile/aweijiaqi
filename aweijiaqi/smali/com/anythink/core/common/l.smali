.class public Lcom/anythink/core/common/l;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/anythink/core/common/l;


# instance fields
.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    const-class v0, Lcom/anythink/core/common/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/common/l;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/l;)Landroid/content/Context;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    return-object p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/core/common/l;
    .locals 2

    const-class v0, Lcom/anythink/core/common/l;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/l;->b:Lcom/anythink/core/common/l;

    if-nez v1, :cond_1

    .line 41
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 42
    :try_start_1
    sget-object v1, Lcom/anythink/core/common/l;->b:Lcom/anythink/core/common/l;

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/anythink/core/common/l;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/l;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/core/common/l;->b:Lcom/anythink/core/common/l;

    .line 45
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p0

    .line 47
    :cond_1
    :goto_0
    sget-object p0, Lcom/anythink/core/common/l;->b:Lcom/anythink/core/common/l;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    return-object p0

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/core/common/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/d/d;)V
    .locals 1

    .line 1141
    iget-object v0, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1146
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p1, "common"

    .line 1147
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "data"

    .line 1148
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "adsourceId"

    .line 1149
    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "networkType"

    .line 1150
    invoke-virtual {p5}, Lcom/anythink/core/common/d/d;->B()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "format"

    .line 1151
    invoke-virtual {p5}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    iget-object p1, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1154
    iget-object p0, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/d/d;)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 146
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p1, "common"

    .line 147
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "data"

    .line 148
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "adsourceId"

    .line 149
    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "networkType"

    .line 150
    invoke-virtual {p5}, Lcom/anythink/core/common/d/d;->B()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "format"

    .line 151
    invoke-virtual {p5}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    iget-object p1, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    iget-object p1, p0, Lcom/anythink/core/common/l;->c:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method


# virtual methods
.method public final a(ILcom/anythink/core/common/d/e;Lcom/anythink/core/c/a;)V
    .locals 1

    .line 52
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    new-instance v0, Lcom/anythink/core/common/l$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/anythink/core/common/l$1;-><init>(Lcom/anythink/core/common/l;Lcom/anythink/core/common/d/e;ILcom/anythink/core/c/a;)V

    invoke-static {v0}, Lcom/anythink/core/common/b/g;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public final a(Lcom/anythink/core/c/a;)V
    .locals 1

    .line 113
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    new-instance v0, Lcom/anythink/core/common/l$2;

    invoke-direct {v0, p0, p1}, Lcom/anythink/core/common/l$2;-><init>(Lcom/anythink/core/common/l;Lcom/anythink/core/c/a;)V

    invoke-static {v0}, Lcom/anythink/core/common/b/g;->b(Ljava/lang/Runnable;)V

    return-void
.end method
