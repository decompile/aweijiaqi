.class public final Lcom/anythink/core/common/f/b;
.super Ljava/lang/Object;


# static fields
.field private static b:Lcom/anythink/core/common/f/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Landroid/content/Context;

.field private i:Ljava/io/File;

.field private j:Ljava/util/concurrent/atomic/AtomicInteger;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Lcom/anythink/core/common/e/g;

.field private n:Lcom/anythink/core/common/e/a/b$a;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Agent"

    .line 42
    iput-object v0, p0, Lcom/anythink/core/common/f/b;->a:Ljava/lang/String;

    const/4 v0, 0x5

    .line 46
    iput v0, p0, Lcom/anythink/core/common/f/b;->c:I

    const/16 v0, 0xa

    .line 47
    iput v0, p0, Lcom/anythink/core/common/f/b;->d:I

    const-wide/32 v0, 0x1b7740

    .line 49
    iput-wide v0, p0, Lcom/anythink/core/common/f/b;->e:J

    const-string v0, ""

    .line 51
    iput-object v0, p0, Lcom/anythink/core/common/f/b;->f:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/anythink/core/common/f/b;->g:Ljava/lang/String;

    const/4 v1, 0x0

    .line 59
    iput-boolean v1, p0, Lcom/anythink/core/common/f/b;->k:Z

    .line 61
    iput-object v0, p0, Lcom/anythink/core/common/f/b;->l:Ljava/lang/String;

    .line 63
    new-instance v0, Lcom/anythink/core/common/f/b$1;

    invoke-direct {v0, p0}, Lcom/anythink/core/common/f/b$1;-><init>(Lcom/anythink/core/common/f/b;)V

    iput-object v0, p0, Lcom/anythink/core/common/f/b;->m:Lcom/anythink/core/common/e/g;

    .line 88
    new-instance v0, Lcom/anythink/core/common/f/b$2;

    invoke-direct {v0, p0}, Lcom/anythink/core/common/f/b$2;-><init>(Lcom/anythink/core/common/f/b;)V

    iput-object v0, p0, Lcom/anythink/core/common/f/b;->n:Lcom/anythink/core/common/e/a/b$a;

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f/b;)Landroid/content/Context;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    return-object p0
.end method

.method public static declared-synchronized a()Lcom/anythink/core/common/f/b;
    .locals 2

    const-class v0, Lcom/anythink/core/common/f/b;

    monitor-enter v0

    .line 105
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/f/b;->b:Lcom/anythink/core/common/f/b;

    if-nez v1, :cond_0

    .line 106
    new-instance v1, Lcom/anythink/core/common/f/b;

    invoke-direct {v1}, Lcom/anythink/core/common/f/b;-><init>()V

    sput-object v1, Lcom/anythink/core/common/f/b;->b:Lcom/anythink/core/common/f/b;

    .line 108
    :cond_0
    sget-object v1, Lcom/anythink/core/common/f/b;->b:Lcom/anythink/core/common/f/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private declared-synchronized a(I)V
    .locals 8

    monitor-enter p0

    const/4 v0, 0x0

    .line 324
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/anythink/core/common/f/b;->g:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 325
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 326
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 328
    :cond_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    iget-object v4, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 329
    :try_start_1
    new-instance v0, Ljava/io/FileWriter;

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 333
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    if-le v4, p1, :cond_1

    .line 337
    invoke-virtual {v0, v5}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    const-string v5, "\n"

    .line 338
    invoke-virtual {v0, v5}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_0

    :cond_1
    const-string v6, "Agent"

    const-string v7, "Remove log:"

    .line 340
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_2
    invoke-virtual {v0}, Ljava/io/FileWriter;->flush()V

    .line 344
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    .line 345
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 347
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v4, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    sub-int/2addr v4, p1

    if-gez v4, :cond_3

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    sub-int/2addr v3, p1

    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 348
    iget-object p1, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 349
    iget-object p1, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-virtual {v1, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 374
    monitor-exit p0

    return-void

    .line 375
    :catch_0
    monitor-exit p0

    return-void

    :catchall_0
    move-object v0, v2

    goto :goto_2

    :catch_1
    move-object v0, v2

    goto :goto_4

    :catch_2
    move-object v0, v2

    goto :goto_6

    :catch_3
    move-object v0, v2

    goto :goto_8

    :catchall_1
    :goto_2
    if-eqz v0, :cond_4

    .line 368
    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_3

    .line 375
    :catch_4
    monitor-exit p0

    return-void

    .line 374
    :cond_4
    :goto_3
    monitor-exit p0

    return-void

    :catch_5
    :goto_4
    if-eqz v0, :cond_5

    .line 368
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_5

    .line 375
    :catch_6
    monitor-exit p0

    return-void

    .line 374
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 358
    :catch_7
    :goto_6
    :try_start_5
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_6

    .line 368
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_7

    .line 375
    :catch_8
    monitor-exit p0

    return-void

    .line 374
    :cond_6
    :goto_7
    monitor-exit p0

    return-void

    :catchall_2
    move-exception p1

    if-eqz v0, :cond_7

    .line 368
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 375
    :catch_9
    :cond_7
    :try_start_8
    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catch_a
    :goto_8
    if-eqz v0, :cond_8

    .line 368
    :try_start_9
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_9

    :catchall_3
    move-exception p1

    monitor-exit p0

    throw p1

    .line 375
    :catch_b
    monitor-exit p0

    return-void

    .line 374
    :cond_8
    :goto_9
    monitor-exit p0

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f/b;I)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f/b;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f/b;Z)Z
    .locals 0

    .line 40
    iput-boolean p1, p0, Lcom/anythink/core/common/f/b;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/anythink/core/common/f/b;)J
    .locals 2

    .line 40
    iget-wide v0, p0, Lcom/anythink/core/common/f/b;->e:J

    return-wide v0
.end method

.method static synthetic c(Lcom/anythink/core/common/f/b;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method private declared-synchronized c()V
    .locals 8

    monitor-enter p0

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-nez v0, :cond_0

    .line 259
    monitor-exit p0

    return-void

    .line 262
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/anythink/core/common/f/b;->k:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget v1, p0, Lcom/anythink/core/common/f/b;->c:I

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    .line 264
    iput-boolean v0, p0, Lcom/anythink/core/common/f/b;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 268
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    iget-object v5, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/StackOverflowError; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 271
    :try_start_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    .line 272
    :goto_0
    iget v5, p0, Lcom/anythink/core/common/f/b;->d:I

    if-ge v4, v5, :cond_1

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 274
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v6, "Agent"

    const-string v7, "Try to send:"

    .line 275
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 280
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v4

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 282
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->g()I

    move-result v5

    if-eq v5, v0, :cond_2

    .line 289
    new-instance v0, Lcom/anythink/core/common/e/b;

    iget-object v5, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/anythink/core/c/a;->g()I

    move-result v4

    invoke-direct {v0, v5, v4, v1}, Lcom/anythink/core/common/e/b;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 290
    iget-object v1, p0, Lcom/anythink/core/common/f/b;->m:Lcom/anythink/core/common/e/g;

    invoke-virtual {v0, v2, v1}, Lcom/anythink/core/common/e/b;->a(ILcom/anythink/core/common/e/g;)V

    goto :goto_1

    .line 284
    :cond_2
    new-instance v5, Lcom/anythink/core/common/e/a/a;

    invoke-direct {v5, v1}, Lcom/anythink/core/common/e/a/a;-><init>(Ljava/util/List;)V

    .line 285
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/anythink/core/common/e/a/a;->a(ILjava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->n:Lcom/anythink/core/common/e/a/b$a;

    invoke-virtual {v5, v0}, Lcom/anythink/core/common/e/a/a;->a(Lcom/anythink/core/common/e/a/b$a;)V

    goto :goto_1

    .line 294
    :cond_3
    new-instance v0, Lcom/anythink/core/common/e/b;

    iget-object v4, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-direct {v0, v4, v2, v1}, Lcom/anythink/core/common/e/b;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 295
    iget-object v1, p0, Lcom/anythink/core/common/f/b;->m:Lcom/anythink/core/common/e/g;

    invoke-virtual {v0, v2, v1}, Lcom/anythink/core/common/e/b;->a(ILcom/anythink/core/common/e/g;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/StackOverflowError; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 311
    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 315
    monitor-exit p0

    return-void

    .line 316
    :catch_0
    monitor-exit p0

    return-void

    :catchall_0
    move-object v1, v3

    goto :goto_2

    :catch_1
    move-object v1, v3

    goto :goto_4

    :catch_2
    move-object v1, v3

    goto :goto_6

    .line 307
    :catchall_1
    :goto_2
    :try_start_5
    iput-boolean v2, p0, Lcom/anythink/core/common/f/b;->k:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v1, :cond_4

    .line 311
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_3

    .line 316
    :catch_3
    monitor-exit p0

    return-void

    .line 315
    :cond_4
    :goto_3
    monitor-exit p0

    return-void

    .line 304
    :catch_4
    :goto_4
    :try_start_7
    iput-boolean v2, p0, Lcom/anythink/core/common/f/b;->k:Z

    .line 305
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v1, :cond_5

    .line 311
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_5

    .line 316
    :catch_5
    monitor-exit p0

    return-void

    .line 315
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 299
    :catch_6
    :goto_6
    :try_start_9
    iput-boolean v2, p0, Lcom/anythink/core/common/f/b;->k:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    if-eqz v1, :cond_6

    .line 311
    :try_start_a
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_7

    .line 316
    :catch_7
    monitor-exit p0

    return-void

    .line 315
    :cond_6
    :goto_7
    monitor-exit p0

    return-void

    :catchall_2
    move-exception v0

    if-eqz v1, :cond_7

    .line 311
    :try_start_b
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 316
    :catch_8
    :cond_7
    :try_start_c
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 318
    :cond_8
    monitor-exit p0

    return-void

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/anythink/core/common/f/b;)I
    .locals 0

    .line 40
    iget p0, p0, Lcom/anythink/core/common/f/b;->c:I

    return p0
.end method

.method static synthetic e(Lcom/anythink/core/common/f/b;)Z
    .locals 0

    .line 40
    iget-boolean p0, p0, Lcom/anythink/core/common/f/b;->k:Z

    return p0
.end method

.method static synthetic f(Lcom/anythink/core/common/f/b;)Ljava/io/File;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    return-object p0
.end method

.method static synthetic g(Lcom/anythink/core/common/f/b;)I
    .locals 0

    .line 40
    iget p0, p0, Lcom/anythink/core/common/f/b;->d:I

    return p0
.end method

.method static synthetic h(Lcom/anythink/core/common/f/b;)Lcom/anythink/core/common/e/a/b$a;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/core/common/f/b;->n:Lcom/anythink/core/common/e/a/b$a;

    return-object p0
.end method

.method static synthetic i(Lcom/anythink/core/common/f/b;)Lcom/anythink/core/common/e/g;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/anythink/core/common/f/b;->m:Lcom/anythink/core/common/e/g;

    return-object p0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 7

    const-string v0, "log"

    .line 116
    iget-object v1, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    if-eqz v1, :cond_0

    return-void

    .line 120
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/f/b;->l:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    const/4 p1, 0x0

    .line 124
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_agent_log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/f/b;->f:Ljava/lang/String;

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_temp_log"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/f/b;->g:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    if-nez v0, :cond_2

    .line 128
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/anythink/core/common/f/b;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    .line 129
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :cond_2
    const/4 v0, 0x0

    .line 140
    :try_start_1
    new-instance v1, Ljava/io/LineNumberReader;

    new-instance v2, Ljava/io/FileReader;

    iget-object v3, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-wide v2, 0x7fffffffffffffffL

    .line 141
    :try_start_2
    invoke-virtual {v1, v2, v3}, Ljava/io/LineNumberReader;->skip(J)J

    .line 142
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v0

    .line 143
    iget-object v2, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v2, :cond_3

    .line 144
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 146
    :cond_3
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V

    const-string v0, "Agent"

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "init file log count:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    :try_start_3
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-object v0, v1

    goto :goto_1

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 162
    :catch_1
    :cond_4
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Error; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_2
    :goto_1
    if-eqz v0, :cond_5

    .line 155
    :try_start_6
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Error; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 164
    :catch_3
    :cond_5
    :goto_2
    :try_start_7
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_6

    .line 165
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 168
    :cond_6
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f/b;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->Q()I

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->Q()I

    move-result v1

    goto :goto_3

    :cond_7
    iget v1, p0, Lcom/anythink/core/common/f/b;->c:I

    :goto_3
    iput v1, p0, Lcom/anythink/core/common/f/b;->c:I

    mul-int/lit8 v1, v1, 0x2

    .line 170
    iput v1, p0, Lcom/anythink/core/common/f/b;->d:I

    .line 171
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->S()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->S()J

    move-result-wide v0

    goto :goto_4

    :cond_8
    iget-wide v0, p0, Lcom/anythink/core/common/f/b;->e:J

    :goto_4
    iput-wide v0, p0, Lcom/anythink/core/common/f/b;->e:J

    .line 173
    invoke-virtual {p0}, Lcom/anythink/core/common/f/b;->b()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Error; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 187
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_a

    .line 188
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    :goto_5
    iput-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void

    :catchall_2
    move-exception v0

    goto :goto_6

    :catch_4
    nop

    .line 187
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_a

    .line 188
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    goto :goto_5

    .line 181
    :catch_5
    :try_start_8
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 187
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_a

    .line 188
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    goto :goto_5

    .line 187
    :goto_6
    iget-object v1, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v1, :cond_9

    .line 188
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 190
    :cond_9
    throw v0

    :catch_6
    nop

    .line 187
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_a

    .line 188
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    goto :goto_5

    :cond_a
    return-void
.end method

.method protected final declared-synchronized a(Lcom/anythink/core/common/d/g;)V
    .locals 4

    monitor-enter p0

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_1

    .line 200
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/f/b;->a(Landroid/content/Context;)V

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/common/f/b;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f/b;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->Q()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->Q()I

    move-result v1

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/anythink/core/common/f/b;->c:I

    :goto_0
    iput v1, p0, Lcom/anythink/core/common/f/b;->c:I

    mul-int/lit8 v1, v1, 0x2

    .line 205
    iput v1, p0, Lcom/anythink/core/common/f/b;->d:I

    .line 206
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->S()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/core/common/f/b;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v0, 0x0

    .line 210
    :try_start_1
    invoke-virtual {p1}, Lcom/anythink/core/common/d/g;->a()Lorg/json/JSONObject;

    move-result-object p1

    .line 212
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 218
    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Lcom/anythink/core/common/f/b;->i:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 220
    :try_start_2
    invoke-virtual {v1, p1}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    const-string p1, "\n"

    .line 221
    invoke-virtual {v1, p1}, Ljava/io/FileWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 222
    invoke-virtual {v1}, Ljava/io/FileWriter;->flush()V

    .line 224
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    .line 227
    iget-object p1, p0, Lcom/anythink/core/common/f/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/StackOverflowError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 242
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    goto :goto_6

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_4

    :catch_0
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-object v0, v1

    goto :goto_3

    :catch_2
    move-object v0, v1

    goto :goto_5

    :catchall_1
    move-exception p1

    goto :goto_4

    :catch_3
    :goto_1
    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_6

    .line 234
    :catch_4
    :goto_3
    :try_start_4
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_4

    goto :goto_2

    :goto_4
    if-eqz v0, :cond_3

    .line 242
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 249
    :catch_5
    :cond_3
    :try_start_6
    throw p1

    :catch_6
    :goto_5
    if-eqz v0, :cond_4

    goto :goto_2

    .line 251
    :catch_7
    :cond_4
    :goto_6
    invoke-direct {p0}, Lcom/anythink/core/common/f/b;->c()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 253
    monitor-exit p0

    return-void

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()V
    .locals 2

    .line 383
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/f/b$3;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/f/b$3;-><init>(Lcom/anythink/core/common/f/b;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method
