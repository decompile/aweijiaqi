.class final Lcom/anythink/core/common/f/b$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/f/b;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/f/b;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/f/b;)V
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 386
    monitor-enter p0

    .line 388
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;)Landroid/content/Context;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v0, :cond_0

    .line 389
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    return-void

    .line 391
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    const-string v2, "LOG_SEND_TIME"

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 392
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->b(Lcom/anythink/core/common/f/b;)J

    move-result-wide v0

    cmp-long v4, v2, v0

    if-gtz v4, :cond_1

    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->c(Lcom/anythink/core/common/f/b;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->c(Lcom/anythink/core/common/f/b;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v1}, Lcom/anythink/core/common/f/b;->d(Lcom/anythink/core/common/f/b;)I

    move-result v1

    if-lt v0, v1, :cond_7

    :cond_1
    const-string v0, "Agent"

    const-string v1, "sendLogByTime:30 minites"

    .line 393
    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->e(Lcom/anythink/core/common/f/b;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->c(Lcom/anythink/core/common/f/b;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->c(Lcom/anythink/core/common/f/b;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_7

    .line 396
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 400
    :try_start_3
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    iget-object v5, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v5}, Lcom/anythink/core/common/f/b;->f(Lcom/anythink/core/common/f/b;)Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/StackOverflowError; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 403
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    .line 404
    :goto_0
    iget-object v5, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v5}, Lcom/anythink/core/common/f/b;->g(Lcom/anythink/core/common/f/b;)I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 406
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v6, "Agent"

    const-string v7, "SendLogByTime:"

    .line 407
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 410
    :cond_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    if-nez v4, :cond_3

    .line 413
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0, v2}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;Z)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 455
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 461
    :catch_0
    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    return-void

    .line 418
    :cond_3
    :try_start_7
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v4

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 420
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->g()I

    move-result v5

    if-eq v5, v1, :cond_4

    .line 427
    new-instance v1, Lcom/anythink/core/common/e/b;

    iget-object v5, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v5}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4}, Lcom/anythink/core/c/a;->g()I

    move-result v4

    invoke-direct {v1, v5, v4, v0}, Lcom/anythink/core/common/e/b;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 428
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->i(Lcom/anythink/core/common/f/b;)Lcom/anythink/core/common/e/g;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/e/b;->a(ILcom/anythink/core/common/e/g;)V

    goto :goto_1

    .line 422
    :cond_4
    new-instance v5, Lcom/anythink/core/common/e/a/a;

    invoke-direct {v5, v0}, Lcom/anythink/core/common/e/a/a;-><init>(Ljava/util/List;)V

    .line 423
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Lcom/anythink/core/common/e/a/a;->a(ILjava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->h(Lcom/anythink/core/common/f/b;)Lcom/anythink/core/common/e/a/b$a;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/anythink/core/common/e/a/a;->a(Lcom/anythink/core/common/e/a/b$a;)V

    goto :goto_1

    .line 432
    :cond_5
    new-instance v1, Lcom/anythink/core/common/e/b;

    iget-object v4, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v4}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4, v2, v0}, Lcom/anythink/core/common/e/b;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 433
    iget-object v0, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v0}, Lcom/anythink/core/common/f/b;->i(Lcom/anythink/core/common/f/b;)Lcom/anythink/core/common/e/g;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/e/b;->a(ILcom/anythink/core/common/e/g;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Error; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 455
    :goto_1
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_7

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-object v0, v3

    goto :goto_2

    :catch_2
    move-object v0, v3

    goto :goto_4

    :catch_3
    move-object v0, v3

    goto :goto_5

    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_6

    .line 448
    :catch_4
    :goto_2
    :try_start_9
    iget-object v1, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v1, v2}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;Z)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_7

    .line 455
    :goto_3
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto :goto_7

    .line 442
    :catch_5
    :goto_4
    :try_start_b
    iget-object v1, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v1, v2}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;Z)Z

    .line 446
    invoke-static {}, Ljava/lang/System;->gc()V

    if-eqz v0, :cond_7

    goto :goto_3

    .line 437
    :catch_6
    :goto_5
    iget-object v1, p0, Lcom/anythink/core/common/f/b$3;->a:Lcom/anythink/core/common/f/b;

    invoke-static {v1, v2}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/f/b;Z)Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v0, :cond_7

    goto :goto_3

    :goto_6
    if-eqz v3, :cond_6

    .line 455
    :try_start_c
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 463
    :catch_7
    :cond_6
    :try_start_d
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 471
    :catch_8
    :catchall_2
    :cond_7
    :goto_7
    :try_start_e
    monitor-exit p0

    return-void

    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    throw v0
.end method
