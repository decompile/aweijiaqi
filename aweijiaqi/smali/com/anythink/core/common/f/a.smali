.class public final Lcom/anythink/core/common/f/a;
.super Lcom/anythink/core/common/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/j<",
        "Lcom/anythink/core/common/d/e;",
        ">;"
    }
.end annotation


# static fields
.field private static e:Lcom/anythink/core/common/f/a;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/anythink/core/common/j;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;
    .locals 2

    const-class v0, Lcom/anythink/core/common/f/a;

    monitor-enter v0

    .line 48
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/f/a;->e:Lcom/anythink/core/common/f/a;

    if-nez v1, :cond_0

    .line 49
    new-instance v1, Lcom/anythink/core/common/f/a;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/f/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/core/common/f/a;->e:Lcom/anythink/core/common/f/a;

    .line 51
    :cond_0
    sget-object p0, Lcom/anythink/core/common/f/a;->e:Lcom/anythink/core/common/f/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;)V
    .locals 6

    .line 178
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object p1

    .line 179
    iget-object v0, p0, Lcom/anythink/core/common/f/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 185
    :cond_0
    invoke-virtual {v0}, Lcom/anythink/core/c/d;->a()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 186
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_2

    .line 190
    :cond_1
    invoke-virtual {v0}, Lcom/anythink/core/c/d;->k()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 191
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    goto :goto_2

    .line 195
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 198
    :try_start_0
    iget v3, v2, Lcom/anythink/core/c/d$b;->b:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 200
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 201
    invoke-virtual {v2}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_3

    .line 202
    invoke-virtual {v2}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 203
    invoke-virtual {v2}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v3, 0x0

    .line 205
    iget-wide v4, p2, Lcom/anythink/core/c/d$b;->m:D

    invoke-virtual {v2, v3, v4, v5}, Lcom/anythink/core/common/d/l;->a(ZD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    .line 210
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f/a;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;)V
    .locals 5

    .line 1178
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object p1

    .line 1179
    iget-object p0, p0, Lcom/anythink/core/common/f/a;->d:Landroid/content/Context;

    invoke-static {p0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 1185
    invoke-virtual {p0}, Lcom/anythink/core/c/d;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1186
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 1190
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/core/c/d;->k()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 1191
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_2

    .line 1195
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/c/d$b;

    .line 1198
    :try_start_0
    iget v2, v1, Lcom/anythink/core/c/d$b;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1200
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1201
    invoke-virtual {v1}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    .line 1202
    invoke-virtual {v1}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1203
    invoke-virtual {v1}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 1205
    iget-wide v3, p2, Lcom/anythink/core/c/d$b;->m:D

    invoke-virtual {v1, v2, v3, v4}, Lcom/anythink/core/common/d/l;->a(ZD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 1210
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_4
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f/a;Lcom/anythink/core/common/d/o;)V
    .locals 0

    .line 38
    invoke-super {p0, p1}, Lcom/anythink/core/common/j;->a(Lcom/anythink/core/common/d/o;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(ILcom/anythink/core/common/d/aa;)V
    .locals 6

    monitor-enter p0

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    .line 55
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V
    .locals 9

    monitor-enter p0

    .line 64
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v8, Lcom/anythink/core/common/f/a$1;

    move-object v1, v8

    move-object v2, p0

    move v3, p1

    move-object v4, p3

    move-object v5, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/anythink/core/common/f/a$1;-><init>(Lcom/anythink/core/common/f/a;ILcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/aa;J)V

    invoke-virtual {v0, v8}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(Lcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;)V
    .locals 6

    monitor-enter p0

    const/4 v1, 0x4

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    .line 59
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected final a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/e;",
            ">;)V"
        }
    .end annotation

    .line 139
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->g()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 155
    new-instance v3, Lcom/anythink/core/common/e/j;

    iget-object v4, p0, Lcom/anythink/core/common/f/a;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->g()I

    move-result v0

    invoke-direct {v3, v4, v0, p1}, Lcom/anythink/core/common/e/j;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v1, v2}, Lcom/anythink/core/common/e/j;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    .line 148
    :cond_0
    new-instance v3, Lcom/anythink/core/common/e/j;

    iget-object v5, p0, Lcom/anythink/core/common/f/a;->d:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->g()I

    move-result v6

    invoke-direct {v3, v5, v6, p1}, Lcom/anythink/core/common/e/j;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v1, v2}, Lcom/anythink/core/common/e/j;->a(ILcom/anythink/core/common/e/g;)V

    .line 150
    new-instance v1, Lcom/anythink/core/common/e/a/d;

    invoke-direct {v1, p1}, Lcom/anythink/core/common/e/a/d;-><init>(Ljava/util/List;)V

    .line 151
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v4, p1}, Lcom/anythink/core/common/e/a/d;->a(ILjava/lang/String;)V

    .line 152
    invoke-virtual {v1, v2}, Lcom/anythink/core/common/e/a/d;->a(Lcom/anythink/core/common/e/a/b$a;)V

    return-void

    .line 143
    :cond_1
    new-instance v1, Lcom/anythink/core/common/e/a/d;

    invoke-direct {v1, p1}, Lcom/anythink/core/common/e/a/d;-><init>(Ljava/util/List;)V

    .line 144
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v4, p1}, Lcom/anythink/core/common/e/a/d;->a(ILjava/lang/String;)V

    .line 145
    invoke-virtual {v1, v2}, Lcom/anythink/core/common/e/a/d;->a(Lcom/anythink/core/common/e/a/b$a;)V

    return-void

    .line 159
    :cond_2
    new-instance v0, Lcom/anythink/core/common/e/j;

    iget-object v3, p0, Lcom/anythink/core/common/f/a;->d:Landroid/content/Context;

    invoke-direct {v0, v3, v1, p1}, Lcom/anythink/core/common/e/j;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/e/j;->a(ILcom/anythink/core/common/e/g;)V

    return-void
.end method
