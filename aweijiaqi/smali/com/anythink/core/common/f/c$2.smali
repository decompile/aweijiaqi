.class final Lcom/anythink/core/common/f/c$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/d/g;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d/g;)V
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 672
    iget-object v0, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v0, v0, Lcom/anythink/core/common/d/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->e:Ljava/lang/String;

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v0, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 677
    iget-object v0, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v2, v2, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->f:Ljava/lang/String;

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->i:Ljava/lang/String;

    .line 681
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 685
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->Y()Ljava/lang/String;

    move-result-object v1

    .line 686
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 687
    iget-object v2, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-static {v2, v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 693
    :cond_2
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->W()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_5

    .line 696
    iget-object v4, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v4, v4, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 697
    iget-object v4, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v4, v4, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 699
    :cond_3
    iget-object v4, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v4, v4, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 700
    iget-object v4, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v4, v4, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 701
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v4, v4, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_5

    return-void

    .line 712
    :cond_5
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->U()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 715
    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v1, v1, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 716
    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v1, v1, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_1

    .line 718
    :cond_6
    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v1, v1, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 719
    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v1, v1, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 720
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    iget-object v1, v1, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_8

    .line 727
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/f/d;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/d;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/f/d;->a(Lcom/anythink/core/common/d/o;)V

    return-void

    .line 733
    :cond_8
    invoke-static {}, Lcom/anythink/core/common/f/b;->a()Lcom/anythink/core/common/f/b;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f/c$2;->a:Lcom/anythink/core/common/d/g;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/f/b;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method
