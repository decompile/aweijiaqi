.class public Lcom/anythink/core/common/f/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field public static final e:I = 0x0

.field public static final f:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(IIII)V
    .locals 2

    .line 412
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004641"

    .line 413
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 414
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 415
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 416
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 417
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 418
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(IJJLjava/lang/String;)V
    .locals 2

    .line 451
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004644"

    .line 452
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 453
    iput-object p5, v0, Lcom/anythink/core/common/d/g;->e:Ljava/lang/String;

    .line 454
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 455
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 456
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    sub-long/2addr p3, p1

    .line 457
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 459
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(IJJLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 463
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004651"

    .line 464
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 465
    iput-object p5, v0, Lcom/anythink/core/common/d/g;->e:Ljava/lang/String;

    .line 466
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    sub-long/2addr p3, p1

    .line 467
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 468
    iput-object p6, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 470
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;)V
    .locals 3

    .line 363
    :try_start_0
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004640"

    .line 364
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 365
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 366
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 367
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 368
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 369
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 370
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 371
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->t()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 372
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->D()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 373
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->E()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 375
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 377
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V
    .locals 16

    .line 107
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v2

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v3

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v4

    .line 112
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v5

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v6

    const/4 v7, -0x1

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->p()I

    move-result v10

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v11

    const-wide/16 v13, 0x0

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v15

    move/from16 v8, p1

    move-object/from16 v9, p2

    .line 106
    invoke-static/range {v0 .. v15}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;IILcom/anythink/core/api/AdError;IDJI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;J)V
    .locals 16

    .line 132
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v2

    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v3

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v4

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v5

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v6

    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->t()I

    move-result v7

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->p()I

    move-result v10

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v11

    .line 145
    invoke-virtual/range {p0 .. p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v15

    move/from16 v8, p1

    move-object/from16 v9, p2

    move-wide/from16 v13, p3

    .line 131
    invoke-static/range {v0 .. v15}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;IILcom/anythink/core/api/AdError;IDJI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 157
    :try_start_0
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004633"

    .line 158
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 159
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 160
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 163
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 164
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 165
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 166
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 167
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 169
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "0"

    .line 172
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p1, "1"

    .line 174
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 177
    :goto_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object p1

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 178
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    .line 180
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 182
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V
    .locals 3

    .line 46
    :try_start_0
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004630"

    .line 47
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 48
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 49
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 56
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {v1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 59
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getCode()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 65
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;Ljava/lang/String;)V
    .locals 2

    .line 387
    :try_start_0
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004639"

    .line 388
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 389
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 390
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 391
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 394
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 396
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 397
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 398
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->t()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 399
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 401
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 403
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;Z)V
    .locals 2

    .line 193
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004634"

    .line 194
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 195
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 196
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 200
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 201
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 202
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 203
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 204
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->u()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p1, "1"

    goto :goto_0

    :cond_0
    const-string p1, "0"

    .line 205
    :goto_0
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 1289
    iget p1, p0, Lcom/anythink/core/common/d/d;->w:I

    .line 206
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 208
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object p1

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 209
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 213
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 14

    .line 220
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v13, Lcom/anythink/core/common/f/c$1;

    move-object v1, v13

    move-object v2, p0

    move v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/anythink/core/common/f/c$1;-><init>(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v0, v13}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;ZJJJ)V
    .locals 2

    .line 475
    :try_start_0
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004643"

    .line 476
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 477
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 478
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 481
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 482
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 483
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 484
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 485
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 486
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 487
    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 488
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 489
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 490
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->u()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->s:Ljava/lang/String;

    .line 2289
    iget p2, p0, Lcom/anythink/core/common/d/d;->w:I

    .line 491
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lcom/anythink/core/common/d/g;->t:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p1, "1"

    goto :goto_0

    :cond_0
    const-string p1, "0"

    .line 492
    :goto_0
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->u:Ljava/lang/String;

    .line 494
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object p1

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 495
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    .line 497
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 499
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method protected static a(Lcom/anythink/core/common/d/g;)V
    .locals 2

    .line 669
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/f/c$2;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/f/c$2;-><init>(Lcom/anythink/core/common/d/g;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/h;Lcom/anythink/core/common/d/i;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 610
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004652"

    .line 611
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 612
    iget-object v1, p1, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 613
    iget v1, p1, Lcom/anythink/core/common/d/i;->f:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 614
    iget-object p1, p1, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 616
    instance-of p1, p0, Lcom/anythink/core/common/d/p;

    if-eqz p1, :cond_0

    const-string p1, "1"

    .line 617
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    goto :goto_0

    .line 618
    :cond_0
    instance-of p1, p0, Lcom/anythink/core/common/d/f;

    if-eqz p1, :cond_1

    const-string p1, "2"

    .line 619
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    goto :goto_0

    .line 620
    :cond_1
    instance-of p1, p0, Lcom/anythink/core/common/d/t;

    if-eqz p1, :cond_2

    const-string p1, "3"

    .line 621
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 624
    :cond_2
    :goto_0
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    const-string p1, "0"

    .line 625
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 626
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 629
    :cond_3
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->e()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 630
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->f()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->s:Ljava/lang/String;

    .line 631
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->q()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->t:Ljava/lang/String;

    .line 632
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->g()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->u:Ljava/lang/String;

    .line 633
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->h()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->v:Ljava/lang/String;

    .line 634
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->i()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->w:Ljava/lang/String;

    .line 635
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->j()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->x:Ljava/lang/String;

    .line 636
    invoke-virtual {p0}, Lcom/anythink/core/common/d/h;->m()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->y:Ljava/lang/String;

    .line 639
    :try_start_0
    instance-of p1, p0, Lcom/anythink/core/common/d/u;

    if-eqz p1, :cond_6

    .line 640
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 641
    check-cast p0, Lcom/anythink/core/common/d/u;

    invoke-virtual {p0}, Lcom/anythink/core/common/d/u;->A()Ljava/lang/String;

    move-result-object p0

    .line 642
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 643
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result p0

    const/4 p3, 0x0

    :goto_1
    if-ge p3, p0, :cond_4

    .line 647
    invoke-virtual {p2, p3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    .line 648
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 651
    :cond_4
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    const/4 p2, 0x1

    if-le p0, p2, :cond_5

    .line 652
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    sub-int/2addr p0, p2

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 655
    :cond_5
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->z:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    :catchall_0
    :cond_6
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;IILcom/anythink/core/api/AdError;IDJI)V
    .locals 4

    move-object v0, p1

    .line 75
    new-instance v1, Lcom/anythink/core/common/d/g;

    invoke-direct {v1}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v2, "1004631"

    .line 76
    iput-object v2, v1, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    move-object v2, p0

    .line 77
    iput-object v2, v1, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 78
    iput-object v0, v1, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 81
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 82
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 83
    invoke-static/range {p15 .. p15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 84
    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    move-object v2, p5

    .line 85
    iput-object v2, v1, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 86
    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 87
    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    const-string v2, ""

    if-eqz p9, :cond_0

    .line 88
    invoke-virtual {p9}, Lcom/anythink/core/api/AdError;->getPlatformCode()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    iput-object v3, v1, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    if-eqz p9, :cond_1

    .line 89
    invoke-virtual {p9}, Lcom/anythink/core/api/AdError;->getPlatformMSG()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    iput-object v3, v1, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 90
    invoke-static {p10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/anythink/core/common/d/g;->s:Ljava/lang/String;

    .line 91
    invoke-static/range {p11 .. p12}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/anythink/core/common/d/g;->t:Ljava/lang/String;

    if-nez p8, :cond_2

    .line 93
    invoke-static/range {p13 .. p14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/anythink/core/common/d/g;->u:Ljava/lang/String;

    .line 96
    :cond_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 97
    invoke-virtual {v0}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v2

    :cond_3
    iput-object v2, v1, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    move-object v0, p6

    .line 99
    iput-object v0, v1, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 101
    invoke-static {v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .line 597
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004650"

    .line 598
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 599
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 600
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 601
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 602
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 603
    iput-object p4, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 604
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 606
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 580
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004648"

    .line 581
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 582
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 583
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 584
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 585
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 586
    iput-object p4, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 587
    iput-object p5, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 588
    iput-object p6, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 590
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 2

    .line 316
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004635"

    .line 317
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 318
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 319
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 321
    :cond_0
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 322
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 323
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 324
    invoke-static {p6, p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 327
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;)V
    .locals 2

    .line 550
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004646"

    .line 551
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 552
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 553
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 554
    invoke-virtual {p2}, Lcom/anythink/core/c/d;->J()I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 555
    invoke-virtual {p2}, Lcom/anythink/core/c/d;->t()I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 556
    invoke-virtual {p2}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    .line 558
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->t:Ljava/lang/String;

    .line 560
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 567
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004647"

    .line 568
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 569
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->e:Ljava/lang/String;

    .line 570
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 571
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 573
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
    .locals 2

    .line 428
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004642"

    .line 429
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 430
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 431
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 432
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 433
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    const/4 p0, 0x3

    if-ne p3, p0, :cond_0

    .line 435
    iput-object p4, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x2

    if-ne p3, p0, :cond_1

    .line 437
    invoke-static {p5, p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    long-to-float p0, p7

    const/high16 p1, 0x44800000    # 1024.0f

    div-float/2addr p0, p1

    .line 438
    invoke-static {p0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 441
    :cond_1
    :goto_0
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 332
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004637"

    .line 333
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 334
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 337
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 338
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 339
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 341
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJIJ)V
    .locals 2

    .line 345
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004638"

    .line 346
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 347
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 348
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 349
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 350
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 351
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 352
    iput-object p6, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 353
    invoke-static {p7, p8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 354
    invoke-static {p9, p10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->s:Ljava/lang/String;

    const-string p0, "1"

    .line 355
    invoke-virtual {p0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p12, p13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->t:Ljava/lang/String;

    .line 356
    invoke-static {p11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->u:Ljava/lang/String;

    .line 358
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 299
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004616"

    .line 300
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 301
    iput-object p4, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 302
    iput-object p0, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 303
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 304
    iput-object p2, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 305
    iput-object p3, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 306
    iput-object p5, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 307
    iput-object p6, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 310
    iput-object p4, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 312
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d/g;Ljava/lang/String;)Z
    .locals 0

    .line 33
    invoke-static {p0, p1}, Lcom/anythink/core/common/f/c;->b(Lcom/anythink/core/common/d/g;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static b(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 273
    new-instance v0, Lcom/anythink/core/common/d/g;

    invoke-direct {v0}, Lcom/anythink/core/common/d/g;-><init>()V

    const-string v1, "1004636"

    .line 274
    iput-object v1, v0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    .line 275
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->b:Ljava/lang/String;

    .line 276
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->d:Ljava/lang/String;

    .line 279
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->g:Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->k:Ljava/lang/String;

    .line 281
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->I()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->l:Ljava/lang/String;

    .line 282
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 283
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->n:Ljava/lang/String;

    .line 284
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->u()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->o:Ljava/lang/String;

    .line 285
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->p:Ljava/lang/String;

    .line 286
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getPlatformCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    .line 287
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getPlatformMSG()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/g;->r:Ljava/lang/String;

    .line 289
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object p1

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 290
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    iput-object p1, v0, Lcom/anythink/core/common/d/g;->j:Ljava/lang/String;

    .line 292
    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/common/d/g;->A:Ljava/lang/String;

    .line 294
    invoke-static {v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/g;)V

    return-void
.end method

.method private static b(Lcom/anythink/core/common/d/g;Ljava/lang/String;)Z
    .locals 5

    .line 740
    iget-object v0, p0, Lcom/anythink/core/common/d/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "1004646"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_1
    const-string v1, "1004643"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_2
    const-string v1, "1004640"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_3
    const-string v1, "1004639"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "1004636"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_5
    const-string v1, "1004634"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_6
    const-string v1, "1004631"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    const/4 p0, 0x0

    goto :goto_2

    .line 750
    :pswitch_0
    iget-object p0, p0, Lcom/anythink/core/common/d/g;->q:Ljava/lang/String;

    goto :goto_2

    .line 747
    :pswitch_1
    iget-object p0, p0, Lcom/anythink/core/common/d/g;->m:Ljava/lang/String;

    .line 754
    :goto_2
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 756
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 757
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result p1

    const/4 v1, 0x0

    :goto_3
    if-ge v1, p1, :cond_2

    .line 759
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :catchall_0
    :cond_2
    return v3

    :sswitch_data_0
    .sparse-switch
        0x74b6d1d1 -> :sswitch_6
        0x74b6d1d4 -> :sswitch_5
        0x74b6d1d6 -> :sswitch_4
        0x74b6d1d9 -> :sswitch_3
        0x74b6d1ef -> :sswitch_2
        0x74b6d1f2 -> :sswitch_1
        0x74b6d1f5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
