.class final Lcom/anythink/core/common/f/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;Lcom/anythink/core/c/d$b;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/anythink/core/c/d$b;

.field final synthetic c:Lcom/anythink/core/common/d/aa;

.field final synthetic d:J

.field final synthetic e:Lcom/anythink/core/common/f/a;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/f/a;ILcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/aa;J)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/anythink/core/common/f/a$1;->e:Lcom/anythink/core/common/f/a;

    iput p2, p0, Lcom/anythink/core/common/f/a$1;->a:I

    iput-object p3, p0, Lcom/anythink/core/common/f/a$1;->b:Lcom/anythink/core/c/d$b;

    iput-object p4, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    iput-wide p5, p0, Lcom/anythink/core/common/f/a$1;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .line 68
    iget v0, p0, Lcom/anythink/core/common/f/a$1;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 69
    iget-object v0, p0, Lcom/anythink/core/common/f/a$1;->b:Lcom/anythink/core/c/d$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 71
    iget-wide v2, v0, Lcom/anythink/core/common/d/l;->price:D

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3}, Lcom/anythink/core/common/d/l;->a(ZD)V

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    instance-of v2, v0, Lcom/anythink/core/common/d/d;

    if-eqz v2, :cond_2

    .line 74
    iget-object v2, p0, Lcom/anythink/core/common/f/a$1;->e:Lcom/anythink/core/common/f/a;

    check-cast v0, Lcom/anythink/core/common/d/d;

    iget-object v3, p0, Lcom/anythink/core/common/f/a$1;->b:Lcom/anythink/core/c/d$b;

    invoke-static {v2, v0, v3}, Lcom/anythink/core/common/f/a;->a(Lcom/anythink/core/common/f/a;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;)V

    .line 79
    :cond_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    .line 80
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    .line 82
    new-instance v2, Lcom/anythink/core/common/d/e;

    invoke-direct {v2}, Lcom/anythink/core/common/d/e;-><init>()V

    .line 83
    iget v3, p0, Lcom/anythink/core/common/f/a$1;->a:I

    iput v3, v2, Lcom/anythink/core/common/d/e;->a:I

    .line 84
    iget-object v3, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    iput-object v3, v2, Lcom/anythink/core/common/d/e;->b:Lcom/anythink/core/common/d/aa;

    .line 85
    iget-wide v3, p0, Lcom/anythink/core/common/f/a$1;->d:J

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_3

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :goto_1
    iput-wide v3, v2, Lcom/anythink/core/common/d/e;->c:J

    .line 87
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/core/common/l;->a(Landroid/content/Context;)Lcom/anythink/core/common/l;

    move-result-object v3

    iget v4, p0, Lcom/anythink/core/common/f/a$1;->a:I

    invoke-virtual {v3, v4, v2, v0}, Lcom/anythink/core/common/l;->a(ILcom/anythink/core/common/d/e;Lcom/anythink/core/c/a;)V

    .line 90
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->X()Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 92
    iget-object v4, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    instance-of v4, v4, Lcom/anythink/core/common/d/d;

    if-eqz v4, :cond_5

    .line 94
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 96
    iget-object v5, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    check-cast v5, Lcom/anythink/core/common/d/d;

    invoke-virtual {v5}, Lcom/anythink/core/common/d/d;->B()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v3, :cond_5

    .line 98
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_4

    return-void

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :catchall_0
    nop

    .line 110
    :cond_5
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->V()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 111
    iget v3, p0, Lcom/anythink/core/common/f/a$1;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 112
    iget v3, p0, Lcom/anythink/core/common/f/a$1;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/aa;->L()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    return-void

    .line 121
    :cond_6
    iget v0, p0, Lcom/anythink/core/common/f/a$1;->a:I

    if-ne v1, v0, :cond_9

    iget-object v0, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    instance-of v0, v0, Lcom/anythink/core/common/d/d;

    if-eqz v0, :cond_9

    .line 122
    invoke-static {}, Lcom/anythink/core/common/k;->a()Lcom/anythink/core/common/k;

    iget-object v0, p0, Lcom/anythink/core/common/f/a$1;->c:Lcom/anythink/core/common/d/aa;

    check-cast v0, Lcom/anythink/core/common/d/d;

    if-eqz v0, :cond_9

    .line 1055
    :try_start_1
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v1

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1058
    invoke-virtual {v1}, Lcom/anythink/core/c/d;->f()Ljava/lang/String;

    move-result-object v1

    .line 1059
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "1"

    .line 1062
    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-wide v4, 0x408f400000000000L    # 1000.0

    if-eqz v1, :cond_7

    const-string v6, "token"

    .line 1064
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1065
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1066
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->j()Ljava/lang/String;

    move-result-object v6

    .line 1067
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v7

    div-double/2addr v7, v4

    .line 1068
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->f()Ljava/lang/String;

    move-result-object v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1114
    :try_start_2
    new-instance v10, Lcom/adjust/sdk/AdjustEvent;

    invoke-direct {v10, v1}, Lcom/adjust/sdk/AdjustEvent;-><init>(Ljava/lang/String;)V

    .line 1115
    invoke-virtual {v10, v7, v8, v6}, Lcom/adjust/sdk/AdjustEvent;->setRevenue(DLjava/lang/String;)V

    .line 1116
    invoke-virtual {v10, v9}, Lcom/adjust/sdk/AdjustEvent;->setOrderId(Ljava/lang/String;)V

    .line 1117
    invoke-static {v10}, Lcom/adjust/sdk/Adjust;->trackEvent(Lcom/adjust/sdk/AdjustEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    :cond_7
    :try_start_3
    const-string v1, "2"

    .line 1073
    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_9

    const-string v3, "rtye"

    .line 1075
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 1077
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v6, "af_order_id"

    .line 1078
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->f()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "af_content_id"

    .line 1079
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "af_content_type"

    .line 1080
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "af_revenue"

    const/4 v7, 0x2

    if-ne v1, v7, :cond_8

    .line 1081
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v0

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v0

    div-double/2addr v0, v4

    :goto_3
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "af_currency"

    const-string v1, "USD"

    .line 1082
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v0

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    const-string v4, "af_ad_view"

    invoke-virtual {v0, v1, v4, v3}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 130
    :catchall_2
    :cond_9
    iget-object v0, p0, Lcom/anythink/core/common/f/a$1;->e:Lcom/anythink/core/common/f/a;

    invoke-static {v0, v2}, Lcom/anythink/core/common/f/a;->a(Lcom/anythink/core/common/f/a;Lcom/anythink/core/common/d/o;)V

    return-void
.end method
