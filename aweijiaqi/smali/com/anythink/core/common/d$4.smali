.class final Lcom/anythink/core/common/d$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/anythink/core/c/d;

.field final synthetic c:I

.field final synthetic d:J

.field final synthetic e:I

.field final synthetic f:Ljava/util/List;

.field final synthetic g:Lcom/anythink/core/common/d;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d;Ljava/lang/String;Lcom/anythink/core/c/d;IJILjava/util/List;)V
    .locals 0

    .line 1067
    iput-object p1, p0, Lcom/anythink/core/common/d$4;->g:Lcom/anythink/core/common/d;

    iput-object p2, p0, Lcom/anythink/core/common/d$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/core/common/d$4;->b:Lcom/anythink/core/c/d;

    iput p4, p0, Lcom/anythink/core/common/d$4;->c:I

    iput-wide p5, p0, Lcom/anythink/core/common/d$4;->d:J

    iput p7, p0, Lcom/anythink/core/common/d$4;->e:I

    iput-object p8, p0, Lcom/anythink/core/common/d$4;->f:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .line 1070
    iget-object v0, p0, Lcom/anythink/core/common/d$4;->g:Lcom/anythink/core/common/d;

    invoke-static {v0}, Lcom/anythink/core/common/d;->f(Lcom/anythink/core/common/d;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UnitGroupInfo Finish HeadBidding Tracking"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1073
    new-instance v1, Lcom/anythink/core/common/d/d;

    invoke-direct {v1}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 1074
    iget-object v2, p0, Lcom/anythink/core/common/d$4;->g:Lcom/anythink/core/common/d;

    iget-object v2, v2, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 1075
    iget-object v2, p0, Lcom/anythink/core/common/d$4;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 1076
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/anythink/core/common/d$4;->b:Lcom/anythink/core/c/d;

    invoke-virtual {v3}, Lcom/anythink/core/c/d;->C()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 1077
    iget-object v2, p0, Lcom/anythink/core/common/d$4;->b:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    .line 1078
    iget v2, p0, Lcom/anythink/core/common/d$4;->c:I

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->n(I)V

    .line 1079
    iget-wide v2, p0, Lcom/anythink/core/common/d$4;->d:J

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/d;->e(J)V

    .line 1080
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/d;->f(J)V

    .line 1081
    iget-object v2, p0, Lcom/anythink/core/common/d$4;->b:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->t()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->w(I)V

    .line 1082
    iget-object v2, p0, Lcom/anythink/core/common/d$4;->b:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->J()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->p(I)V

    .line 1083
    iget v2, p0, Lcom/anythink/core/common/d$4;->e:I

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->v(I)V

    const/4 v2, 0x0

    .line 1086
    :goto_0
    iget-object v3, p0, Lcom/anythink/core/common/d$4;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1088
    iget-object v3, p0, Lcom/anythink/core/common/d$4;->f:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 1089
    iget-object v4, p0, Lcom/anythink/core/common/d$4;->g:Lcom/anythink/core/common/d;

    invoke-static {v4}, Lcom/anythink/core/common/d;->f(Lcom/anythink/core/common/d;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "UnitGroupInfo requestLevel:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " || layer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2155
    iget v6, v3, Lcom/anythink/core/c/d$b;->A:I

    .line 1089
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "sortpriority"

    .line 1094
    iget v6, v3, Lcom/anythink/core/c/d$b;->a:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    move v7, v2

    :cond_0
    invoke-virtual {v4, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "sorttype"

    .line 1095
    iget v6, v3, Lcom/anythink/core/c/d$b;->q:I

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "unit_id"

    .line 3059
    iget-object v6, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    .line 1096
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "bidresult"

    .line 1097
    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->g()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "bidprice"

    .line 1098
    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->e()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3091
    iget-wide v6, v3, Lcom/anythink/core/c/d$b;->m:D

    .line 1098
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_1
    const-string v6, "0"

    :goto_1
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "nw_firm_id"

    .line 1099
    iget v6, v3, Lcom/anythink/core/c/d$b;->b:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "tp_bid_id"

    .line 1100
    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v6

    iget-object v6, v6, Lcom/anythink/core/common/d/l;->g:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "rl_bid_status"

    .line 1101
    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->f()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "errormsg"

    .line 3115
    iget-object v3, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    .line 1102
    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1103
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1109
    :cond_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/anythink/core/common/d/d;->q(Ljava/lang/String;)V

    .line 1111
    iget-object v0, p0, Lcom/anythink/core/common/d$4;->g:Lcom/anythink/core/common/d;

    iget-object v0, v0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v0

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    return-void
.end method
