.class final Lcom/anythink/core/common/f$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/c/d$b;Ljava/util/Map;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/api/ATBaseAdAdapter;

.field final synthetic b:Lcom/anythink/core/c/d$b;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/anythink/core/common/f;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/c/d$b;Ljava/util/Map;)V
    .locals 0

    .line 725
    iput-object p1, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iput-object p2, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    iput-object p3, p0, Lcom/anythink/core/common/f$3;->b:Lcom/anythink/core/c/d$b;

    iput-object p4, p0, Lcom/anythink/core/common/f$3;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .line 728
    iget-object v0, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v1, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 730
    iget-object v0, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v0, v0, Lcom/anythink/core/common/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v1, ""

    const-string v2, "2006"

    if-nez v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v3, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    const-string v4, "Request Context is null! Please check the Ad init Context."

    invoke-static {v2, v1, v4}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    return-void

    .line 737
    :cond_0
    invoke-static {}, Lcom/anythink/core/c/a;->Z()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 738
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/core/common/b/h;->a(Landroid/content/Context;)Lcom/anythink/core/common/b/h;

    move-result-object v3

    .line 741
    :try_start_0
    iget-object v4, p0, Lcom/anythink/core/common/f$3;->b:Lcom/anythink/core/c/d$b;

    iget v4, v4, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v3, v4}, Lcom/anythink/core/common/b/h;->c(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    .line 742
    invoke-virtual {v3}, Lcom/anythink/core/common/b/h;->c()Z

    move-result v5

    iget-object v6, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v6, v6, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/anythink/core/api/ATSDK;->isEUTraffic(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v4, v0, v5, v6}, Lcom/anythink/core/api/ATBaseAdAdapter;->setUserDataConsent(Landroid/content/Context;ZZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 743
    iget-object v4, p0, Lcom/anythink/core/common/f$3;->b:Lcom/anythink/core/c/d$b;

    iget v4, v4, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v3, v4}, Lcom/anythink/core/common/b/h;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    .line 746
    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    .line 750
    :cond_1
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v4, v4, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    const-string v4, "4"

    .line 752
    iget-object v5, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v5, v5, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v5}, Lcom/anythink/core/c/d;->C()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 753
    iget-object v4, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v4, v4, Lcom/anythink/core/common/f;->E:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v5, p0, Lcom/anythink/core/common/f$3;->b:Lcom/anythink/core/c/d$b;

    iget-object v5, v5, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget-object v6, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    :cond_2
    iget-object v4, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    iget-object v5, p0, Lcom/anythink/core/common/f$3;->c:Ljava/util/Map;

    new-instance v6, Lcom/anythink/core/common/f$a;

    iget-object v7, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v8, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    const/4 v9, 0x0

    invoke-direct {v6, v7, v8, v9}, Lcom/anythink/core/common/f$a;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;B)V

    invoke-virtual {v4, v0, v5, v3, v6}, Lcom/anythink/core/api/ATBaseAdAdapter;->internalLoad(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Lcom/anythink/core/api/ATCustomLoadListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_1
    move-exception v0

    .line 758
    iget-object v3, p0, Lcom/anythink/core/common/f$3;->d:Lcom/anythink/core/common/f;

    iget-object v4, p0, Lcom/anythink/core/common/f$3;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    return-void
.end method
