.class public final Lcom/anythink/core/common/res/d;
.super Ljava/lang/Object;


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static e:Lcom/anythink/core/common/res/d;


# instance fields
.field a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/anythink/core/common/res/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private f:Landroid/content/Context;

.field private g:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_internal_resouce"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/common/res/d;->c:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_custom_resouce"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/common/res/d;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/res/d;->b:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/common/res/d;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/res/d;->f:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Lcom/anythink/core/common/g/j;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/res/d;->g:Ljava/io/File;

    return-void
.end method

.method private declared-synchronized a(ILjava/io/File;)Lcom/anythink/core/common/res/a;
    .locals 5

    monitor-enter p0

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/res/d;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/res/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const-wide/32 v1, 0x1900000

    goto :goto_0

    .line 1210
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/anythink/core/common/res/d;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    invoke-static {}, Lcom/anythink/core/c/b;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x400

    mul-long v1, v1, v3

    .line 170
    :goto_0
    invoke-static {p2, v1, v2}, Lcom/anythink/core/common/res/a;->a(Ljava/io/File;J)Lcom/anythink/core/common/res/a;

    move-result-object v0

    .line 171
    iget-object p2, p0, Lcom/anythink/core/common/res/d;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 173
    :try_start_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/anythink/core/common/b/g;->r()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 174
    iget-object p2, p0, Lcom/anythink/core/common/res/d;->b:Ljava/lang/String;

    const-string v1, "Create DiskCache error."

    invoke-static {p2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v0

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/core/common/res/d;
    .locals 2

    const-class v0, Lcom/anythink/core/common/res/d;

    monitor-enter v0

    .line 46
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/res/d;->e:Lcom/anythink/core/common/res/d;

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Lcom/anythink/core/common/res/d;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/res/d;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/core/common/res/d;->e:Lcom/anythink/core/common/res/d;

    .line 50
    :cond_0
    sget-object p0, Lcom/anythink/core/common/res/d;->e:Lcom/anythink/core/common/res/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .line 217
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 220
    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/anythink/core/common/res/d;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".0"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    return p1
.end method

.method private b(I)J
    .locals 4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const-wide/32 v0, 0x1900000

    return-wide v0

    .line 210
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/common/res/d;->f:Landroid/content/Context;

    invoke-static {p1}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    invoke-static {}, Lcom/anythink/core/c/b;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    mul-long v0, v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Ljava/io/FileInputStream;
    .locals 3

    .line 132
    invoke-virtual {p0, p1}, Lcom/anythink/core/common/res/d;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 137
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 142
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/anythink/core/common/res/d;->a(ILjava/io/File;)Lcom/anythink/core/common/res/a;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 148
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/res/a;->a(Ljava/lang/String;)Lcom/anythink/core/common/res/a$c;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 151
    invoke-virtual {p1}, Lcom/anythink/core/common/res/a$c;->a()Ljava/io/InputStream;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 153
    check-cast p1, Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 159
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    return-object v2
.end method

.method public final a(I)Ljava/lang/String;
    .locals 2

    .line 188
    sget-object v0, Lcom/anythink/core/common/res/d;->d:Ljava/lang/String;

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 191
    :cond_0
    sget-object v0, Lcom/anythink/core/common/res/d;->c:Ljava/lang/String;

    .line 195
    :goto_0
    new-instance p1, Ljava/io/File;

    iget-object v1, p0, Lcom/anythink/core/common/res/d;->g:Ljava/io/File;

    invoke-direct {p1, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final a(ILjava/lang/String;Ljava/io/InputStream;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_8

    if-nez p3, :cond_0

    goto/16 :goto_5

    .line 65
    :cond_0
    invoke-virtual {p0, p1}, Lcom/anythink/core/common/res/d;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v0

    .line 70
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 72
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 77
    :cond_2
    invoke-direct {p0, p1, v2}, Lcom/anythink/core/common/res/d;->a(ILjava/io/File;)Lcom/anythink/core/common/res/a;

    move-result-object p1

    if-eqz p1, :cond_8

    const/4 v1, 0x0

    .line 85
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/res/a;->a(Ljava/lang/String;)Lcom/anythink/core/common/res/a$c;

    move-result-object v2

    if-nez v2, :cond_4

    .line 87
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/res/a;->b(Ljava/lang/String;)Lcom/anythink/core/common/res/a$a;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_5

    .line 89
    :try_start_1
    invoke-virtual {p1, v0}, Lcom/anythink/core/common/res/a$a;->a(I)Ljava/io/OutputStream;

    move-result-object v1

    const/16 p2, 0x800

    new-array p2, p2, [B

    .line 93
    :goto_0
    invoke-virtual {p3, p2}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 94
    invoke-virtual {v1, p2, v0, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 96
    :cond_3
    invoke-virtual {p1}, Lcom/anythink/core/common/res/a$a;->a()V

    .line 97
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p2

    move-object v4, v1

    move-object v1, p1

    move-object p1, v4

    goto :goto_2

    .line 100
    :cond_4
    :try_start_2
    invoke-virtual {v2}, Lcom/anythink/core/common/res/a$c;->a()Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    :goto_1
    const/4 v0, 0x1

    if-eqz v1, :cond_8

    .line 115
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_5

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p2

    move-object p1, v1

    .line 104
    :goto_2
    :try_start_4
    iget-object p3, p0, Lcom/anythink/core/common/res/d;->b:Ljava/lang/String;

    const-string v2, "writeToDiskLruCache - "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_6

    .line 107
    :try_start_5
    invoke-virtual {v1}, Lcom/anythink/core/common/res/a$a;->b()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    :catch_2
    move-exception p2

    .line 110
    :try_start_6
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_6
    :goto_3
    if-eqz p1, :cond_8

    .line 115
    :try_start_7
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_5

    :catchall_1
    move-exception p2

    move-object v1, p1

    move-object p1, p2

    :goto_4
    if-eqz v1, :cond_7

    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 119
    :catch_3
    :cond_7
    throw p1

    :catch_4
    :cond_8
    :goto_5
    return v0
.end method
