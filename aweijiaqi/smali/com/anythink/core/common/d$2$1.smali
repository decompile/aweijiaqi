.class final Lcom/anythink/core/common/d$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/common/h$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:[J

.field final synthetic b:Lcom/anythink/core/common/d$2;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d$2;[J)V
    .locals 0

    .line 601
    iput-object p1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iput-object p2, p0, Lcom/anythink/core/common/d$2$1;->a:[J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 11

    const-string v0, "HeadBidding"

    const-string v1, "onFinished: ----------------------------------------------"

    .line 664
    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v0, v0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f;

    if-eqz v0, :cond_0

    .line 669
    invoke-virtual {v0}, Lcom/anythink/core/common/f;->e()V

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v2, v0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v4, v0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->g:Lcom/anythink/core/common/g;

    iget-boolean v5, v0, Lcom/anythink/core/common/g;->e:Z

    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->a:[J

    const/4 v1, 0x0

    aget-wide v6, v0, v1

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->h:Lcom/anythink/core/common/d/d;

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->G()I

    move-result v8

    .line 3067
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v10, Lcom/anythink/core/common/d$4;

    move-object v1, v10

    move-object v3, p1

    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d$4;-><init>(Lcom/anythink/core/common/d;Ljava/lang/String;Lcom/anythink/core/c/d;IJILjava/util/List;)V

    invoke-virtual {v0, v10}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "HeadBidding"

    const-string v1, "onSuccess: ----------------------------------------------"

    .line 605
    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v0, v0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f;

    .line 609
    iget-object v1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v1, v1, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v2, v2, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v2, v2, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v1

    .line 611
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 612
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 613
    iget-object v4, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 614
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    iget-object v4, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v4, v4, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    const/4 v5, 0x1

    invoke-static {v4, v3, v5}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d$b;Z)V

    goto :goto_0

    .line 621
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p2

    iget-object v1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    invoke-virtual {p2, v1, p1, v2}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    if-eqz v0, :cond_2

    .line 623
    invoke-virtual {v0, v2}, Lcom/anythink/core/common/f;->a(Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    .line 630
    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v0, v0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v1, v1, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    .line 632
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 633
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 634
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 635
    iget-object v4, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 636
    iget-object v4, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v4, v4, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    const/4 v5, 0x0

    invoke-static {v4, v3, v5}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d$b;Z)V

    .line 637
    iget-wide v4, v3, Lcom/anythink/core/c/d$b;->m:D

    const-wide/16 v6, 0x0

    cmpl-double v8, v4, v6

    if-lez v8, :cond_1

    iget-object v4, v3, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 638
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 640
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 641
    iget-wide v4, v3, Lcom/anythink/core/c/d$b;->s:J

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-eqz v8, :cond_0

    .line 642
    invoke-static {}, Lcom/anythink/core/common/b;->a()Lcom/anythink/core/common/b;

    move-result-object v4

    iget-object v3, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 2069
    iget-object v4, v4, Lcom/anythink/core/common/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const-string p2, "HeadBidding"

    const-string v0, "onFailed: ----------------------------------------------"

    .line 648
    invoke-static {p2, v0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p2

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, p1, v2}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 651
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p2

    iget-object v0, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, p1, v1}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 655
    iget-object p2, p0, Lcom/anythink/core/common/d$2$1;->b:Lcom/anythink/core/common/d$2;

    iget-object p2, p2, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object p2, p2, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/f;

    if-eqz p1, :cond_3

    .line 656
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p2

    if-lez p2, :cond_3

    .line 657
    invoke-virtual {p1, v2}, Lcom/anythink/core/common/f;->a(Ljava/util/List;)V

    :cond_3
    return-void
.end method
