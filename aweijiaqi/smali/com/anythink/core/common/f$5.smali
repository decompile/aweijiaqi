.class final Lcom/anythink/core/common/f$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;JZ)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/c/d$b;

.field final synthetic b:Z

.field final synthetic c:Lcom/anythink/core/common/f;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/f;Lcom/anythink/core/c/d$b;Z)V
    .locals 0

    .line 1107
    iput-object p1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iput-object p2, p0, Lcom/anythink/core/common/f$5;->a:Lcom/anythink/core/c/d$b;

    iput-boolean p3, p0, Lcom/anythink/core/common/f$5;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 1110
    iget-object v0, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    monitor-enter v0

    .line 1112
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-boolean v1, v1, Lcom/anythink/core/common/f;->i:Z

    if-eqz v1, :cond_0

    .line 1113
    monitor-exit v0

    return-void

    .line 1117
    :cond_0
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v1, v1, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/anythink/core/common/f$5;->a:Lcom/anythink/core/c/d$b;

    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 1118
    iget-object v2, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v2, v2, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/anythink/core/common/f$5;->a:Lcom/anythink/core/c/d$b;

    iget-object v3, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 1121
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    :cond_1
    if-eqz v2, :cond_2

    .line 1125
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 1129
    :cond_2
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v1, v1, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/anythink/core/common/f$5;->a:Lcom/anythink/core/c/d$b;

    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v1, v1, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/anythink/core/common/f$5;->a:Lcom/anythink/core/c/d$b;

    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1133
    iget-boolean v1, p0, Lcom/anythink/core/common/f$5;->b:Z

    if-eqz v1, :cond_3

    .line 1134
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v2, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v2, v2, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    iget-object v3, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v3, v3, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/common/f;Ljava/util/List;Ljava/util/List;Z)V

    goto :goto_0

    .line 1136
    :cond_3
    iget-object v1, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v2, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v2, v2, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    iget-object v3, p0, Lcom/anythink/core/common/f$5;->c:Lcom/anythink/core/common/f;

    iget-object v3, v3, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/common/f;Ljava/util/List;Ljava/util/List;Z)V

    .line 1139
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
