.class public final Lcom/anythink/core/common/g/n;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)Lcom/anythink/core/common/d/d;
    .locals 1

    .line 81
    invoke-static {p1, p2, p3}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)V

    .line 83
    invoke-virtual {p0, p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->setmUnitgroupInfo(Lcom/anythink/core/c/d$b;)V

    .line 84
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->z()I

    move-result p3

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->setRefresh(Z)V

    .line 87
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object p3

    .line 3317
    iput-object p3, p1, Lcom/anythink/core/common/d/d;->r:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    nop

    .line 92
    :goto_1
    iget-object p3, p2, Lcom/anythink/core/c/d$b;->c:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkName()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_1
    iget-object p2, p2, Lcom/anythink/core/c/d$b;->c:Ljava/lang/String;

    :goto_2
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/d;->r(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/d;->c(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->setTrackingInfo(Lcom/anythink/core/common/d/d;)V

    return-object p1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;
    .locals 1

    .line 47
    new-instance v0, Lcom/anythink/core/common/d/d;

    invoke-direct {v0}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 48
    invoke-virtual {v0, p1}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, p2}, Lcom/anythink/core/common/d/d;->n(Ljava/lang/String;)V

    .line 53
    invoke-virtual {v0, p4}, Lcom/anythink/core/common/d/d;->k(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0, p5}, Lcom/anythink/core/common/d/d;->m(I)V

    .line 55
    invoke-virtual {v0, p6}, Lcom/anythink/core/common/d/d;->n(I)V

    const/4 p0, 0x0

    .line 1454
    iput p0, v0, Lcom/anythink/core/common/d/d;->o:I

    const/4 p1, 0x2

    .line 2445
    iput p1, v0, Lcom/anythink/core/common/d/d;->n:I

    .line 2463
    iput p0, v0, Lcom/anythink/core/common/d/d;->p:I

    .line 61
    invoke-static {v0, p3}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d;)V

    const/4 p0, 0x1

    .line 63
    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/d;->u(I)V

    .line 64
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/anythink/core/common/b/g;->f()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/d;->t(I)V

    .line 66
    invoke-virtual {v0, p7}, Lcom/anythink/core/common/d/d;->v(I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/anythink/core/common/d/d;)V
    .locals 8

    .line 193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 194
    invoke-static {p0}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object p0

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/anythink/core/a/a;->a(I)Ljava/util/Map;

    move-result-object p0

    const/4 v2, 0x0

    if-eqz p0, :cond_1

    .line 200
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/anythink/core/common/d/x;

    .line 201
    iget v7, v6, Lcom/anythink/core/common/d/x;->c:I

    add-int/2addr v4, v7

    .line 202
    iget v6, v6, Lcom/anythink/core/common/d/x;->d:I

    add-int/2addr v5, v6

    goto :goto_0

    .line 205
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/anythink/core/common/d/x;

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 208
    invoke-virtual {p1, v4}, Lcom/anythink/core/common/d/d;->a(I)V

    add-int/lit8 v5, v5, 0x1

    .line 209
    invoke-virtual {p1, v5}, Lcom/anythink/core/common/d/d;->b(I)V

    if-eqz p0, :cond_2

    .line 210
    iget v3, p0, Lcom/anythink/core/common/d/x;->c:I

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1, v3}, Lcom/anythink/core/common/d/d;->c(I)V

    if-eqz p0, :cond_3

    .line 211
    iget v2, p0, Lcom/anythink/core/common/d/x;->d:I

    :cond_3
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Lcom/anythink/core/common/d/d;->d(I)V

    .line 212
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Check cap waite time:"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "anythink"

    invoke-static {p1, p0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)V
    .locals 3

    .line 101
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x$a;

    move-result-object v0

    .line 103
    iget v1, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->o(I)V

    .line 104
    iget-object v1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->j(Ljava/lang/String;)V

    .line 105
    iget v1, p1, Lcom/anythink/core/c/d$b;->y:I

    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->f(I)V

    .line 106
    iget v1, p1, Lcom/anythink/core/c/d$b;->z:I

    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->g(I)V

    .line 107
    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->i(I)V

    .line 108
    iget-object p2, p1, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->l(Ljava/lang/String;)V

    const/4 p2, 0x0

    if-eqz v0, :cond_0

    .line 109
    iget v1, v0, Lcom/anythink/core/common/d/x$a;->e:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->k(I)V

    if-eqz v0, :cond_1

    .line 110
    iget p2, v0, Lcom/anythink/core/common/d/x$a;->d:I

    :cond_1
    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->l(I)V

    .line 112
    iget p2, p1, Lcom/anythink/core/c/d$b;->I:I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 4091
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->m:D

    .line 113
    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->c(D)V

    .line 4195
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->F:D

    .line 114
    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->a(D)V

    goto :goto_1

    :cond_2
    const-wide/16 v0, 0x0

    .line 116
    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->c(D)V

    .line 117
    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->a(D)V

    .line 119
    :goto_1
    iget p2, p1, Lcom/anythink/core/c/d$b;->n:I

    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->h(I)V

    .line 120
    iget-object p2, p1, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->d(Ljava/lang/String;)V

    .line 5067
    iget-object p2, p1, Lcom/anythink/core/c/d$b;->j:Ljava/lang/String;

    .line 5099
    iput-object p2, p0, Lcom/anythink/core/common/d/aa;->ai:Ljava/lang/String;

    .line 6075
    iget p2, p1, Lcom/anythink/core/c/d$b;->k:I

    .line 6107
    iput p2, p0, Lcom/anythink/core/common/d/aa;->aj:I

    .line 7083
    iget p2, p1, Lcom/anythink/core/c/d$b;->l:I

    .line 7115
    iput p2, p0, Lcom/anythink/core/common/d/aa;->ak:I

    .line 7163
    iget p2, p1, Lcom/anythink/core/c/d$b;->B:I

    .line 124
    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->e(I)V

    .line 7171
    iget-object p2, p1, Lcom/anythink/core/c/d$b;->C:Ljava/lang/String;

    .line 125
    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->g(Ljava/lang/String;)V

    const/16 p2, 0x23

    .line 128
    iget v0, p1, Lcom/anythink/core/c/d$b;->b:I

    if-ne p2, v0, :cond_3

    .line 131
    :try_start_0
    new-instance p2, Lorg/json/JSONObject;

    iget-object v0, p1, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-direct {p2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "my_oid"

    .line 132
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 134
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 136
    invoke-virtual {v0, p2}, Lcom/anythink/core/c/d;->c(Ljava/lang/String;)Lcom/anythink/core/common/d/p;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 139
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "o_id"

    .line 140
    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "c_id"

    .line 141
    invoke-virtual {v0}, Lcom/anythink/core/common/d/p;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/anythink/core/common/d/d;->o(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    nop

    .line 150
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 151
    iget-object p1, p1, Lcom/anythink/core/common/d/l;->g:Ljava/lang/String;

    goto :goto_3

    :cond_4
    const-string p1, ""

    :goto_3
    invoke-virtual {p0, p1}, Lcom/anythink/core/common/d/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d;)V
    .locals 3

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 158
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->C()I

    move-result v0

    const-string v1, "1"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 159
    invoke-virtual {p0, v1}, Lcom/anythink/core/common/d/d;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "0"

    .line 161
    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->m(Ljava/lang/String;)V

    .line 164
    :goto_0
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->w(I)V

    .line 165
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->J()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->p(I)V

    .line 167
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->C()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->h(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->i(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->b()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->b(D)V

    .line 172
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->b(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->n()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->a(Ljava/util/Map;)V

    .line 175
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->q()Lcom/anythink/core/api/ATRewardInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->a(Lcom/anythink/core/api/ATRewardInfo;)V

    .line 176
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->r()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/d/d;->b(Ljava/util/Map;)V

    .line 178
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->a(J)V

    .line 179
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->m()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/anythink/core/common/d/d;->b(J)V

    :cond_1
    return-void
.end method
