.class final Lcom/anythink/core/common/g/d$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/g/d;->s(Landroid/content/Context;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .line 853
    iput-object p1, p0, Lcom/anythink/core/common/g/d$1;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/core/common/g/d$1;->c:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "com.google.android.gms.ads.identifier.AdvertisingIdClient"

    .line 857
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "com.google.android.gms.ads.identifier.AdvertisingIdClient$Info"

    .line 858
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getAdvertisingIdInfo"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    .line 859
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v0

    invoke-virtual {v1, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 860
    iget-object v5, p0, Lcom/anythink/core/common/g/d$1;->a:Landroid/content/Context;

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v3, "getId"

    new-array v4, v0, [Ljava/lang/Class;

    .line 863
    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 864
    iget-object v3, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 869
    :catch_0
    :try_start_1
    new-instance v1, Lcom/anythink/core/common/b/a;

    invoke-direct {v1}, Lcom/anythink/core/common/b/a;-><init>()V

    iget-object v2, p0, Lcom/anythink/core/common/g/d$1;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/b/a$a;

    move-result-object v1

    .line 870
    iget-object v2, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    invoke-virtual {v1}, Lcom/anythink/core/common/b/a$a;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    nop

    .line 876
    :goto_0
    iget-object v1, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/anythink/core/common/g/d;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 877
    iget-object v1, p0, Lcom/anythink/core/common/g/d$1;->b:[Ljava/lang/String;

    aget-object v0, v1, v0

    invoke-static {v0}, Lcom/anythink/core/common/g/d;->a(Ljava/lang/String;)V

    .line 881
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/anythink/core/common/g/d$1;->c:Ljava/util/concurrent/ExecutorService;

    monitor-enter v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 882
    :try_start_3
    iget-object v1, p0, Lcom/anythink/core/common/g/d$1;->c:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 883
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    return-void
.end method
