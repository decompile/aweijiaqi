.class final Lcom/anythink/core/common/d$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/anythink/core/common/g;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:[I

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/anythink/core/common/d;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d;Landroid/content/Context;Lcom/anythink/core/common/g;Ljava/lang/String;[ILjava/lang/String;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iput-object p2, p0, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    iput-object p4, p0, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/anythink/core/common/d$1;->d:[I

    iput-object p6, p0, Lcom/anythink/core/common/d$1;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 19

    move-object/from16 v7, p0

    .line 196
    iget-object v8, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    monitor-enter v8

    .line 197
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    iget-object v1, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/anythink/core/common/b/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/common/g/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 202
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    iput-object v4, v0, Lcom/anythink/core/common/g;->d:Ljava/lang/String;

    .line 204
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v1, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    iget-object v1, v1, Lcom/anythink/core/common/g;->d:Ljava/lang/String;

    iget-object v2, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/d;->a(Ljava/lang/String;Lcom/anythink/core/common/g;)V

    .line 208
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_d

    .line 209
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 210
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 2038
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto/16 :goto_5

    .line 229
    :cond_2
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-virtual {v0}, Lcom/anythink/core/common/d;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "2005"

    const-string v1, ""

    const-string v2, ""

    .line 230
    invoke-static {v0, v1, v2}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    .line 231
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v2, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-virtual {v1, v2, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/g;Lcom/anythink/core/api/AdError;)V

    .line 232
    monitor-exit v8

    return-void

    .line 235
    :cond_3
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 236
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v17

    .line 239
    invoke-static {v5}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v3

    iget-object v6, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 241
    invoke-virtual {v6}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    const-string v3, ""

    :goto_2
    move-object/from16 v18, v3

    .line 244
    iget-object v10, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    const-string v11, ""

    const-string v13, ""

    if-eqz v6, :cond_5

    .line 245
    invoke-virtual {v6}, Lcom/anythink/core/c/d;->F()I

    move-result v3

    move v14, v3

    goto :goto_3

    :cond_5
    const/4 v3, -0x1

    const/4 v14, -0x1

    :goto_3
    iget-object v3, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    iget-boolean v15, v3, Lcom/anythink/core/common/g;->e:Z

    iget-object v3, v7, Lcom/anythink/core/common/d$1;->d:[I

    aget v16, v3, v1

    move-object v9, v4

    move-object v12, v6

    .line 244
    invoke-static/range {v9 .. v16}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v3

    if-nez v6, :cond_6

    .line 249
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    iget-object v9, v9, Lcom/anythink/core/common/g;->f:Lcom/anythink/core/api/ATMediationRequestInfo;

    if-eqz v9, :cond_6

    .line 250
    sget-object v9, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    const-string v10, "request default adsource for splash."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v10, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    iget-object v11, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-virtual {v9, v10, v4, v11}, Lcom/anythink/core/common/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 252
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v13, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    const/4 v14, 0x0

    move-object v11, v0

    move-object/from16 v12, v17

    invoke-virtual/range {v9 .. v14}, Lcom/anythink/core/c/e;->a(Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/e$a;)V

    .line 253
    monitor-exit v8

    return-void

    .line 259
    :cond_6
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget v9, v9, Lcom/anythink/core/common/d;->e:I

    if-ne v9, v2, :cond_7

    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-virtual {v9}, Lcom/anythink/core/common/d;->b()Z

    move-result v9

    if-nez v9, :cond_7

    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v9

    iget-object v10, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    iget-object v11, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 260
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v0

    iget-object v2, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v2, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-virtual {v0, v2}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/g;)V

    .line 262
    invoke-virtual {v3, v1}, Lcom/anythink/core/common/d/d;->b(Z)V

    const/4 v0, 0x4

    .line 263
    invoke-virtual {v3, v0}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 264
    invoke-static {v5}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 265
    invoke-static {v5}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v0

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 266
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iput-boolean v1, v0, Lcom/anythink/core/common/d;->f:Z

    .line 267
    monitor-exit v8

    return-void

    :cond_7
    if-eqz v6, :cond_9

    .line 271
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v9}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 272
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iget-object v11, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v11}, Lcom/anythink/core/common/d;->b(Lcom/anythink/core/common/d;)J

    move-result-wide v11

    sub-long/2addr v9, v11

    const-wide/16 v11, 0x0

    cmp-long v13, v9, v11

    if-lez v13, :cond_9

    .line 273
    invoke-virtual {v6}, Lcom/anythink/core/c/d;->P()J

    move-result-wide v11

    cmp-long v13, v9, v11

    if-gez v13, :cond_9

    const-string v0, "2008"

    const-string v4, ""

    const-string v5, ""

    .line 274
    invoke-static {v0, v4, v5}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    const/4 v4, 0x7

    .line 275
    invoke-virtual {v3, v4}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 276
    iget-object v4, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v4}, Lcom/anythink/core/common/d;->c(Lcom/anythink/core/common/d;)Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v1, 0x1

    .line 277
    :cond_8
    iget-object v4, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    new-instance v5, Lcom/anythink/core/common/c;

    invoke-virtual {v0}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v0, v6}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    iget-object v0, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-static {v4, v1, v3, v5, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    .line 278
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v0, v2}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Z)Z

    .line 279
    monitor-exit v8

    return-void

    .line 284
    :cond_9
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v9}, Lcom/anythink/core/common/d;->d(Lcom/anythink/core/common/d;)Z

    .line 285
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v9}, Lcom/anythink/core/common/d;->e(Lcom/anythink/core/common/d;)J

    .line 286
    iget-object v9, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-static {v9, v1}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Z)Z

    if-eqz v6, :cond_a

    .line 289
    invoke-static {}, Lcom/anythink/core/a/b;->a()Lcom/anythink/core/a/b;

    move-result-object v1

    iget-object v9, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v5, v9, v6}, Lcom/anythink/core/a/b;->a(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v0, "2009"

    const-string v1, ""

    const-string v4, ""

    .line 290
    invoke-static {v0, v1, v4}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    const/16 v1, 0x8

    .line 291
    invoke-virtual {v3, v1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 292
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    new-instance v4, Lcom/anythink/core/common/c;

    invoke-virtual {v0}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    iget-object v0, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    .line 293
    monitor-exit v8

    return-void

    .line 296
    :cond_a
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    invoke-virtual {v1}, Lcom/anythink/core/common/d;->c()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v0, "2005"

    const-string v1, ""

    const-string v4, ""

    .line 297
    invoke-static {v0, v1, v4}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    const/4 v1, 0x3

    .line 298
    invoke-virtual {v3, v1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 300
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    new-instance v4, Lcom/anythink/core/common/c;

    invoke-virtual {v0}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    iget-object v0, v7, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    .line 301
    monitor-exit v8

    return-void

    .line 304
    :cond_b
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iput-boolean v2, v1, Lcom/anythink/core/common/d;->f:Z

    .line 307
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v1, v1, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/anythink/core/common/f;

    .line 2241
    iput-boolean v2, v9, Lcom/anythink/core/common/f;->M:Z

    goto :goto_4

    .line 311
    :cond_c
    iget-object v1, v7, Lcom/anythink/core/common/d$1;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v9

    iget-object v13, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    new-instance v14, Lcom/anythink/core/common/d$1$2;

    move-object v1, v14

    move-object/from16 v2, p0

    move-object v10, v6

    move-object/from16 v6, v18

    invoke-direct/range {v1 .. v6}, Lcom/anythink/core/common/d$1$2;-><init>(Lcom/anythink/core/common/d$1;Lcom/anythink/core/common/d/d;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    move-object v11, v0

    move-object/from16 v12, v17

    invoke-virtual/range {v9 .. v14}, Lcom/anythink/core/c/e;->a(Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/e$a;)V

    .line 385
    monitor-exit v8

    return-void

    .line 212
    :cond_d
    :goto_5
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v2, Lcom/anythink/core/common/d$1$1;

    invoke-direct {v2, v7, v4}, Lcom/anythink/core/common/d$1$1;-><init>(Lcom/anythink/core/common/d$1;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 221
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->r()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 222
    sget-object v0, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Please check these params in your code (AppId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", AppKey: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", PlacementId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v7, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_e
    iget-object v0, v7, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iput-boolean v1, v0, Lcom/anythink/core/common/d;->f:Z

    .line 225
    monitor-exit v8

    return-void

    :catchall_0
    move-exception v0

    .line 385
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
