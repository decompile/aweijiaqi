.class final Lcom/anythink/core/common/d$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/anythink/core/c/d;

.field final synthetic e:Ljava/util/List;

.field final synthetic f:Ljava/util/List;

.field final synthetic g:Lcom/anythink/core/common/g;

.field final synthetic h:Lcom/anythink/core/common/d/d;

.field final synthetic i:Lcom/anythink/core/common/d;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/g;Lcom/anythink/core/common/d/d;)V
    .locals 0

    .line 572
    iput-object p1, p0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iput-object p2, p0, Lcom/anythink/core/common/d$2;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/anythink/core/common/d$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    iput-object p6, p0, Lcom/anythink/core/common/d$2;->e:Ljava/util/List;

    iput-object p7, p0, Lcom/anythink/core/common/d$2;->f:Ljava/util/List;

    iput-object p8, p0, Lcom/anythink/core/common/d$2;->g:Lcom/anythink/core/common/g;

    iput-object p9, p0, Lcom/anythink/core/common/d$2;->h:Lcom/anythink/core/common/d/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [J

    const/4 v1, 0x0

    .line 577
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 582
    new-instance v1, Lcom/anythink/core/common/d/a;

    invoke-direct {v1}, Lcom/anythink/core/common/d/a;-><init>()V

    .line 583
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->a:Landroid/content/Context;

    iput-object v2, v1, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    .line 584
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    .line 585
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    .line 586
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->C()I

    move-result v2

    iput v2, v1, Lcom/anythink/core/common/d/a;->d:I

    .line 587
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->l()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/anythink/core/common/d/a;->i:J

    .line 588
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->m()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/anythink/core/common/d/a;->e:J

    .line 589
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->d()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/anythink/core/common/d/a;->f:J

    .line 590
    invoke-static {}, Lcom/anythink/core/common/a/b;->a()Lcom/anythink/core/common/a/b;

    .line 1034
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v2

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v2

    .line 1035
    invoke-virtual {v2}, Lcom/anythink/core/c/a;->d()Lcom/anythink/core/common/d/m;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1036
    invoke-virtual {v2}, Lcom/anythink/core/common/d/m;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 1040
    :cond_0
    invoke-virtual {v2}, Lcom/anythink/core/common/d/m;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    :goto_0
    const-string v2, "https://adx.anythinktech.com/bid"

    .line 590
    :goto_1
    iput-object v2, v1, Lcom/anythink/core/common/d/a;->j:Ljava/lang/String;

    .line 591
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->e:Ljava/util/List;

    iput-object v2, v1, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 592
    iget-object v2, p0, Lcom/anythink/core/common/d$2;->d:Lcom/anythink/core/c/d;

    iput-object v2, v1, Lcom/anythink/core/common/d/a;->k:Lcom/anythink/core/c/d;

    .line 593
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 594
    iget-object v3, p0, Lcom/anythink/core/common/d$2;->f:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 595
    iget-object v3, p0, Lcom/anythink/core/common/d$2;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 597
    :cond_2
    iput-object v2, v1, Lcom/anythink/core/common/d/a;->h:Ljava/util/List;

    .line 599
    new-instance v2, Lcom/anythink/core/b/b;

    invoke-direct {v2, v1}, Lcom/anythink/core/b/b;-><init>(Lcom/anythink/core/common/d/a;)V

    .line 600
    invoke-static {}, Lcom/anythink/core/api/ATSDK;->isNetworkLogDebug()Z

    move-result v1

    invoke-interface {v2, v1}, Lcom/anythink/core/common/h$b;->a(Z)V

    .line 601
    new-instance v1, Lcom/anythink/core/common/d$2$1;

    invoke-direct {v1, p0, v0}, Lcom/anythink/core/common/d$2$1;-><init>(Lcom/anythink/core/common/d$2;[J)V

    invoke-interface {v2, v1}, Lcom/anythink/core/common/h$b;->a(Lcom/anythink/core/common/h$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    nop

    .line 677
    iget-object v0, p0, Lcom/anythink/core/common/d$2;->i:Lcom/anythink/core/common/d;

    iget-object v0, v0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/anythink/core/common/d$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f;

    if-eqz v0, :cond_3

    .line 679
    invoke-virtual {v0}, Lcom/anythink/core/common/f;->e()V

    :cond_3
    return-void
.end method
