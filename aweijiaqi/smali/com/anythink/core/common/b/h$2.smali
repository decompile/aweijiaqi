.class final Lcom/anythink/core/common/b/h$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/common/e/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/b/h;->a(Lcom/anythink/core/api/NetTrafficeCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/api/NetTrafficeCallback;

.field final synthetic b:Lcom/anythink/core/common/b/h;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/b/h;Lcom/anythink/core/api/NetTrafficeCallback;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/anythink/core/common/b/h$2;->b:Lcom/anythink/core/common/b/h;

    iput-object p2, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    const-string v0, "is_eu"

    const-string v1, "There is no result."

    if-nez p1, :cond_1

    .line 192
    :try_start_0
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_0

    .line 193
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    invoke-interface {p1, v1}, Lcom/anythink/core/api/NetTrafficeCallback;->onErrorCallback(Ljava/lang/String;)V

    :cond_0
    return-void

    .line 199
    :cond_1
    move-object v2, p1

    check-cast v2, Lorg/json/JSONObject;

    .line 201
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 202
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_2

    .line 203
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    invoke-interface {p1, v1}, Lcom/anythink/core/api/NetTrafficeCallback;->onErrorCallback(Ljava/lang/String;)V

    :cond_2
    return-void

    .line 208
    :cond_3
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 211
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_5

    .line 212
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    invoke-interface {p1, v0}, Lcom/anythink/core/api/NetTrafficeCallback;->onResultCallback(Z)V

    return-void

    .line 215
    :cond_4
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_5

    .line 216
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/anythink/core/api/NetTrafficeCallback;->onResultCallback(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    return-void

    :catchall_0
    nop

    .line 220
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_6

    const-string v0, "Internal error"

    .line 221
    invoke-interface {p1, v0}, Lcom/anythink/core/api/NetTrafficeCallback;->onErrorCallback(Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    .locals 0

    .line 229
    iget-object p1, p0, Lcom/anythink/core/common/b/h$2;->a:Lcom/anythink/core/api/NetTrafficeCallback;

    if-eqz p1, :cond_0

    .line 230
    invoke-virtual {p2}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/anythink/core/api/NetTrafficeCallback;->onErrorCallback(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method
