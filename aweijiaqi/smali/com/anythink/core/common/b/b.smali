.class public abstract Lcom/anythink/core/common/b/b;
.super Ljava/lang/Object;


# instance fields
.field isRefresh:Z

.field protected mActivityRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mTrackingInfo:Lcom/anythink/core/common/d/d;

.field private mUnitgroupInfo:Lcom/anythink/core/c/d$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getTrackingInfo()Lcom/anythink/core/common/d/d;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/anythink/core/common/b/b;->mTrackingInfo:Lcom/anythink/core/common/d/d;

    return-object v0
.end method

.method public final getmUnitgroupInfo()Lcom/anythink/core/c/d$b;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/anythink/core/common/b/b;->mUnitgroupInfo:Lcom/anythink/core/c/d$b;

    return-object v0
.end method

.method public final isRefresh()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/anythink/core/common/b/b;->isRefresh:Z

    return v0
.end method

.method public final postOnMainThread(Ljava/lang/Runnable;)V
    .locals 1

    .line 58
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final refreshActivityContext(Landroid/app/Activity;)V
    .locals 1

    .line 53
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/anythink/core/common/b/b;->mActivityRef:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public final runOnNetworkRequestThread(Ljava/lang/Runnable;)V
    .locals 2

    .line 62
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 63
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/g/a/a;->b(Ljava/lang/Runnable;)V

    return-void

    .line 65
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public final setRefresh(Z)V
    .locals 0

    .line 45
    iput-boolean p1, p0, Lcom/anythink/core/common/b/b;->isRefresh:Z

    return-void
.end method

.method public final setTrackingInfo(Lcom/anythink/core/common/d/d;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/anythink/core/common/b/b;->mTrackingInfo:Lcom/anythink/core/common/d/d;

    return-void
.end method

.method public final setmUnitgroupInfo(Lcom/anythink/core/c/d$b;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/anythink/core/common/b/b;->mUnitgroupInfo:Lcom/anythink/core/c/d$b;

    return-void
.end method
