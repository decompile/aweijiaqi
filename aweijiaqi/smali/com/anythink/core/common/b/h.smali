.class public final Lcom/anythink/core/common/b/h;
.super Ljava/lang/Object;


# static fields
.field private static d:Lcom/anythink/core/common/b/h;


# instance fields
.field final a:I

.field b:Landroid/content/Context;

.field c:I

.field private e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, -0x64

    .line 34
    iput v0, p0, Lcom/anythink/core/common/b/h;->a:I

    const/4 v0, 0x2

    .line 39
    iput v0, p0, Lcom/anythink/core/common/b/h;->c:I

    .line 41
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v1, p0, Lcom/anythink/core/common/b/h;->e:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    .line 47
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    sget-object v1, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    const-string v2, "UPLOAD_DATA_LEVEL"

    invoke-static {p1, v1, v2, v0}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/anythink/core/common/b/h;->c:I

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/core/common/b/h;
    .locals 2

    const-class v0, Lcom/anythink/core/common/b/h;

    monitor-enter v0

    .line 52
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/b/h;->d:Lcom/anythink/core/common/b/h;

    if-nez v1, :cond_0

    .line 53
    new-instance v1, Lcom/anythink/core/common/b/h;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/b/h;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/core/common/b/h;->d:Lcom/anythink/core/common/b/h;

    .line 55
    :cond_0
    sget-object p0, Lcom/anythink/core/common/b/h;->d:Lcom/anythink/core/common/b/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/core/common/b/h;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/anythink/core/common/b/h;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/anythink/core/common/b/h;->c:I

    return v0
.end method

.method public final a(I)V
    .locals 3

    .line 59
    iput p1, p0, Lcom/anythink/core/common/b/h;->c:I

    .line 60
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    sget-object v1, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    const-string v2, "UPLOAD_DATA_LEVEL"

    invoke-static {v0, v1, v2, p1}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/anythink/core/api/ATGDPRAuthCallback;)V
    .locals 2

    .line 164
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/b/h$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/anythink/core/common/b/h$1;-><init>(Lcom/anythink/core/common/b/h;Lcom/anythink/core/api/ATGDPRAuthCallback;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/anythink/core/api/NetTrafficeCallback;)V
    .locals 4

    .line 181
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    sget-object v1, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    const/16 v2, -0x64

    const-string v3, "EU_INFO"

    invoke-static {v0, v1, v3, v2}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, v2, :cond_0

    .line 183
    new-instance v0, Lcom/anythink/core/common/e/f;

    invoke-direct {v0}, Lcom/anythink/core/common/e/f;-><init>()V

    new-instance v2, Lcom/anythink/core/common/b/h$2;

    invoke-direct {v2, p0, p1}, Lcom/anythink/core/common/b/h$2;-><init>(Lcom/anythink/core/common/b/h;Lcom/anythink/core/api/NetTrafficeCallback;)V

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/e/f;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    if-eqz p1, :cond_2

    .line 241
    invoke-interface {p1, v2}, Lcom/anythink/core/api/NetTrafficeCallback;->onResultCallback(Z)V

    return-void

    :cond_1
    if-eqz p1, :cond_2

    .line 245
    invoke-interface {p1, v1}, Lcom/anythink/core/api/NetTrafficeCallback;->onResultCallback(Z)V

    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 2

    .line 252
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/b/h$3;

    invoke-direct {v1, p0, p1}, Lcom/anythink/core/common/b/h$3;-><init>(Lcom/anythink/core/common/b/h;I)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b()Z
    .locals 5

    .line 74
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 77
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->s()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->H()I

    move-result v3

    if-nez v3, :cond_1

    return v2

    .line 90
    :cond_1
    iget v3, p0, Lcom/anythink/core/common/b/h;->c:I

    .line 93
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->F()I

    move-result v4

    if-ne v4, v2, :cond_2

    .line 94
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->E()I

    move-result v3

    :cond_2
    if-nez v3, :cond_3

    return v2

    :cond_3
    return v1

    .line 78
    :cond_4
    :goto_0
    iget v0, p0, Lcom/anythink/core/common/b/h;->c:I

    if-ne v0, v2, :cond_5

    return v1

    :cond_5
    return v2
.end method

.method public final c()Z
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v0

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_7

    .line 114
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->s()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 122
    :cond_0
    iget v3, p0, Lcom/anythink/core/common/b/h;->c:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 124
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->H()I

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    return v1

    .line 130
    :cond_2
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->F()I

    move-result v3

    if-ne v3, v2, :cond_4

    .line 132
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->E()I

    move-result v0

    if-nez v0, :cond_3

    return v2

    :cond_3
    return v1

    .line 139
    :cond_4
    iget v3, p0, Lcom/anythink/core/common/b/h;->c:I

    if-nez v3, :cond_5

    return v2

    .line 143
    :cond_5
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->H()I

    move-result v0

    if-nez v0, :cond_6

    return v2

    :cond_6
    return v1

    .line 115
    :cond_7
    :goto_0
    iget v0, p0, Lcom/anythink/core/common/b/h;->c:I

    if-ne v0, v2, :cond_8

    return v1

    :cond_8
    return v2
.end method

.method public final c(I)Z
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/anythink/core/common/b/h;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public final d()Z
    .locals 4

    .line 158
    iget-object v0, p0, Lcom/anythink/core/common/b/h;->b:Landroid/content/Context;

    sget-object v1, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    const-string v2, "EU_INFO"

    const/16 v3, -0x64

    invoke-static {v0, v1, v2, v3}, Lcom/anythink/core/common/g/m;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
