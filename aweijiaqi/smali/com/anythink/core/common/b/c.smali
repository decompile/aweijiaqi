.class public Lcom/anythink/core/common/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field public static final a:Ljava/lang/String; = "start_time"

.field public static final b:Ljava/lang/String; = "end_time"

.field public static final c:Ljava/lang/String; = "psid"

.field public static final d:Ljava/lang/String; = "launch_mode"

.field public static final e:I = 0x0

.field public static final f:I = 0x1


# instance fields
.field g:J

.field h:I

.field i:Lorg/json/JSONObject;

.field j:Landroid/os/Handler;

.field k:Ljava/lang/Runnable;

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 2

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-class v0, Lcom/anythink/core/common/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/anythink/core/common/b/c;->j:Landroid/os/Handler;

    .line 47
    new-instance v0, Lcom/anythink/core/common/b/c$1;

    invoke-direct {v0, p0}, Lcom/anythink/core/common/b/c$1;-><init>(Lcom/anythink/core/common/b/c;)V

    iput-object v0, p0, Lcom/anythink/core/common/b/c;->k:Ljava/lang/Runnable;

    const/4 v0, 0x0

    .line 71
    iput v0, p0, Lcom/anythink/core/common/b/c;->h:I

    .line 72
    iput-wide p1, p0, Lcom/anythink/core/common/b/c;->g:J

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/b/c;)Ljava/lang/String;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 8

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 144
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v2

    .line 146
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "psid"

    .line 147
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/common/b/g;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "start_time"

    .line 148
    iget-wide v5, p0, Lcom/anythink/core/common/b/c;->g:J

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v4, "end_time"

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v4, "launch_mode"

    .line 150
    iget v5, p0, Lcom/anythink/core/common/b/c;->h:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 151
    iput-object v3, p0, Lcom/anythink/core/common/b/c;->i:Lorg/json/JSONObject;

    .line 152
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "playRecord"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v4, p0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onActivityPaused: record leave time:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 160
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object p1

    .line 161
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->p()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 162
    iget-object v2, p0, Lcom/anythink/core/common/b/c;->j:Landroid/os/Handler;

    iget-object v3, p0, Lcom/anythink/core/common/b/c;->k:Ljava/lang/Runnable;

    invoke-virtual {p1}, Lcom/anythink/core/c/a;->n()I

    move-result p1

    int-to-long v4, p1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 163
    iget-object p1, p0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v2, "onActivityPaused : Start to leave application countdown."

    invoke-static {p1, v2}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onActivityPaused: Method use time:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 19

    move-object/from16 v0, p0

    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 89
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->j:Landroid/os/Handler;

    iget-object v4, v0, Lcom/anythink/core/common/b/c;->k:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 91
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v3

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v3

    .line 92
    iget-object v4, v0, Lcom/anythink/core/common/b/c;->i:Lorg/json/JSONObject;

    const-string v5, ""

    const-string v6, "playRecord"

    const/4 v9, 0x1

    if-eqz v4, :cond_2

    .line 93
    iget-object v4, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v10, "onActivityResumed : Time countdown is closed, check the time is up?"

    invoke-static {v4, v10}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v4, v0, Lcom/anythink/core/common/b/c;->i:Lorg/json/JSONObject;

    const-string v10, "start_time"

    .line 95
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    const-string v10, "end_time"

    .line 96
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v14

    const-string v10, "psid"

    .line 97
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v10, "launch_mode"

    .line 98
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v14

    invoke-virtual {v3}, Lcom/anythink/core/c/a;->n()I

    move-result v3

    int-to-long v7, v3

    cmp-long v3, v10, v7

    if-lez v3, :cond_1

    .line 101
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onActivityResumed : Time countdown is closed, time up to send agent and create new psid, playtime:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v10, v14, v12

    const-wide/16 v17, 0x3e8

    div-long v10, v10, v17

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v3

    sget-object v7, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v7, v8, v5}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-ne v4, v9, :cond_0

    const/4 v3, 0x3

    const/4 v11, 0x3

    goto :goto_0

    :cond_0
    const/4 v11, 0x1

    .line 105
    :goto_0
    invoke-static/range {v11 .. v16}, Lcom/anythink/core/common/f/c;->a(IJJLjava/lang/String;)V

    const-wide/16 v3, 0x0

    .line 106
    iput-wide v3, v0, Lcom/anythink/core/common/b/c;->g:J

    goto :goto_1

    .line 108
    :cond_1
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v4, "onActivityResumed : Time countdown is closed, continue to record pervious start time"

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :cond_2
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v4, "onActivityResumed : Time countdown is opened or doesn\'t start countdown"

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v3, 0x0

    .line 113
    iput-object v3, v0, Lcom/anythink/core/common/b/c;->i:Lorg/json/JSONObject;

    .line 115
    iget-wide v3, v0, Lcom/anythink/core/common/b/c;->g:J

    const-wide/16 v7, 0x0

    cmp-long v10, v3, v7

    if-nez v10, :cond_3

    .line 116
    iput v9, v0, Lcom/anythink/core/common/b/c;->h:I

    .line 117
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v4, "onActivityResumed : restart to record starttime"

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v9}, Lcom/anythink/core/common/b/g;->a(Landroid/content/Context;Ljava/lang/String;I)J

    move-result-wide v3

    iput-wide v3, v0, Lcom/anythink/core/common/b/c;->g:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    nop

    goto :goto_2

    .line 128
    :cond_3
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v3

    .line 129
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v7, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v7, v3, v5}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    const-string v4, "onActivityResumed : Continue to record the pervious start time"

    invoke-static {v3, v4}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_2
    iget-wide v3, v0, Lcom/anythink/core/common/b/c;->g:J

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_4

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/anythink/core/common/b/c;->g:J

    .line 137
    :cond_4
    iget-object v3, v0, Lcom/anythink/core/common/b/c;->l:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onActivityResumed: Method use time:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
