.class final Lcom/anythink/core/common/e/a/c$1;
.super Lcom/anythink/core/common/g/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/e/a/c;->a(Lcom/anythink/core/common/e/a/b;Lcom/anythink/core/common/e/a/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/e/a/b;

.field final synthetic b:Lcom/anythink/core/common/e/a/b$a;

.field final synthetic c:Lcom/anythink/core/common/e/a/c;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/e/a/c;Lcom/anythink/core/common/e/a/b;Lcom/anythink/core/common/e/a/b$a;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    iput-object p2, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    iput-object p3, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    invoke-direct {p0}, Lcom/anythink/core/common/g/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    const-string v0, "Response Error Code:"

    const/4 v1, 0x1

    .line 112
    :try_start_0
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    iget-object v3, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    invoke-static {v2, v3}, Lcom/anythink/core/common/e/a/c;->a(Lcom/anythink/core/common/e/a/c;Lcom/anythink/core/common/e/a/b;)V

    .line 113
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    invoke-static {v2}, Lcom/anythink/core/common/e/a/c;->a(Lcom/anythink/core/common/e/a/c;)I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 115
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    if-eqz v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    iget-object v3, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    invoke-interface {v2, v3}, Lcom/anythink/core/common/e/a/b$a;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    .line 125
    :cond_1
    new-instance v3, Ljava/lang/Exception;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_0

    .line 134
    :catch_0
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    invoke-static {v2}, Lcom/anythink/core/common/e/a/c;->b(Lcom/anythink/core/common/e/a/c;)V

    .line 137
    :try_start_1
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    iget-object v3, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    invoke-static {v2, v3}, Lcom/anythink/core/common/e/a/c;->a(Lcom/anythink/core/common/e/a/c;Lcom/anythink/core/common/e/a/b;)V

    .line 138
    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    invoke-static {v2}, Lcom/anythink/core/common/e/a/c;->a(Lcom/anythink/core/common/e/a/c;)I

    move-result v2

    if-ne v2, v1, :cond_3

    .line 140
    iget-object v0, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    iget-object v1, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    invoke-interface {v0, v1}, Lcom/anythink/core/common/e/a/b$a;->a(Ljava/lang/Object;)V

    :cond_2
    return-void

    .line 150
    :cond_3
    new-instance v1, Ljava/lang/Exception;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172
    :goto_0
    iget-object v1, p0, Lcom/anythink/core/common/e/a/c$1;->a:Lcom/anythink/core/common/e/a/b;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    invoke-static {v2}, Lcom/anythink/core/common/e/a/c;->c(Lcom/anythink/core/common/e/a/c;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/e/a/c$1;->c:Lcom/anythink/core/common/e/a/c;

    invoke-static {v3}, Lcom/anythink/core/common/e/a/c;->d(Lcom/anythink/core/common/e/a/c;)I

    move-result v3

    const-string v4, ""

    invoke-virtual {v1, v4, v0, v2, v3}, Lcom/anythink/core/common/e/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 173
    iget-object v0, p0, Lcom/anythink/core/common/e/a/c$1;->b:Lcom/anythink/core/common/e/a/b$a;

    if-eqz v0, :cond_4

    .line 174
    invoke-interface {v0}, Lcom/anythink/core/common/e/a/b$a;->a()V

    :cond_4
    return-void
.end method
