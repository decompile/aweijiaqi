.class public final Lcom/anythink/core/common/e/i;
.super Lcom/anythink/core/common/e/a;


# instance fields
.field a:Lcom/anythink/core/common/d/n;


# direct methods
.method public constructor <init>(Lcom/anythink/core/common/d/n;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/anythink/core/common/e/a;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget v0, v0, Lcom/anythink/core/common/d/n;->b:I

    return v0
.end method

.method protected final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method protected final a(Lcom/anythink/core/api/AdError;)V
    .locals 0

    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget-object v0, v0, Lcom/anythink/core/common/d/n;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected final b(Lcom/anythink/core/api/AdError;)V
    .locals 0

    return-void
.end method

.method protected final c()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget-object v0, v0, Lcom/anythink/core/common/d/n;->c:Ljava/lang/String;

    .line 51
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 53
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 55
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 57
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    return-object v1
.end method

.method protected final d()[B
    .locals 6

    .line 68
    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget-object v0, v0, Lcom/anythink/core/common/d/n;->c:Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget-object v0, v0, Lcom/anythink/core/common/d/n;->c:Ljava/lang/String;

    const-string v3, "gzip"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x0

    .line 71
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    iget-object v5, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    iget-object v5, v5, Lcom/anythink/core/common/d/n;->e:Ljava/lang/String;

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v3, "ofl"

    .line 72
    invoke-virtual {v4, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    move-object v3, v4

    goto :goto_1

    :catch_1
    move-exception v1

    .line 75
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v4, v3

    :goto_2
    if-eqz v0, :cond_2

    if-eqz v4, :cond_1

    .line 79
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/e/i;->c(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0

    :cond_1
    new-array v0, v2, [B

    return-object v0

    :cond_2
    if-eqz v4, :cond_3

    .line 82
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0

    :cond_3
    new-array v0, v2, [B

    return-object v0
.end method

.method protected final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final j()Landroid/content/Context;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final l()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final m()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()Lcom/anythink/core/common/d/n;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/anythink/core/common/e/i;->a:Lcom/anythink/core/common/d/n;

    return-object v0
.end method
