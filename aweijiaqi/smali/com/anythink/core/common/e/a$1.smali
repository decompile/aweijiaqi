.class final Lcom/anythink/core/common/e/a$1;
.super Lcom/anythink/core/common/g/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/e/a;->a(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/anythink/core/common/e/a;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/e/a;I)V
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iput p2, p0, Lcom/anythink/core/common/e/a$1;->a:I

    invoke-direct {p0}, Lcom/anythink/core/common/g/a/b;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    const-string v0, "http.loader"

    const-string v1, "9999"

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "REQUEST URL: "

    .line 301
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 305
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 308
    :try_start_1
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v2}, Lcom/anythink/core/common/e/a;->a()I

    move-result v2

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v2, v5, :cond_0

    if-eq v2, v4, :cond_0

    const/4 v2, 0x2

    :cond_0
    const/4 v6, 0x0

    if-ne v2, v5, :cond_1

    .line 315
    invoke-virtual {v3, v5}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 316
    invoke-virtual {v3, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const-string v7, "POST"

    .line 317
    invoke-virtual {v3, v7}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 318
    invoke-virtual {v3, v6}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    :cond_1
    if-ne v2, v4, :cond_2

    .line 322
    invoke-virtual {v3, v6}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 326
    :cond_2
    iget-object v4, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v4}, Lcom/anythink/core/common/e/a;->c()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 327
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 328
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 329
    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v3, v7, v8}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_3
    iget-object v4, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-boolean v4, v4, Lcom/anythink/core/common/e/a;->k:Z

    if-eqz v4, :cond_5

    .line 335
    iget-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Lcom/anythink/core/common/e/a;->n()V
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_4

    .line 492
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    return-void

    .line 339
    :cond_5
    :try_start_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    const-string v6, "ua"

    invoke-virtual {v4, v6}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "User-Agent"

    const-string v6, "UA_5.7.45"

    .line 340
    invoke-virtual {v3, v4, v6}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const/16 v4, 0x4e20

    .line 343
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const v4, 0xea60

    .line 344
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 345
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    if-ne v2, v5, :cond_7

    .line 349
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v2}, Lcom/anythink/core/common/e/a;->d()[B

    move-result-object v2

    if-eqz v2, :cond_7

    .line 351
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 352
    invoke-virtual {v4, v2}, Ljava/io/OutputStream;->write([B)V

    .line 353
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 354
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 357
    :cond_7
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v4, 0xc8

    if-eq v2, v4, :cond_e

    const/16 v4, 0x12e

    if-ne v2, v4, :cond_c

    .line 365
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-boolean v2, v2, Lcom/anythink/core/common/e/a;->k:Z

    if-nez v2, :cond_9

    const-string v2, "Location"

    .line 367
    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v4, "http"

    .line 369
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 370
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 372
    :cond_8
    invoke-direct {p0, v2}, Lcom/anythink/core/common/e/a$1;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 376
    :cond_9
    iget-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Lcom/anythink/core/common/e/a;->n()V
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/net/ConnectException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/StackOverflowError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    :goto_1
    if-eqz v3, :cond_b

    .line 492
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_b
    return-void

    .line 380
    :cond_c
    :try_start_3
    iget-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    const-string v4, "Http respond status code is "

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "9990"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v2, v6}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/net/ConnectException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/StackOverflowError; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v3, :cond_d

    .line 492
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_d
    return-void

    .line 387
    :cond_e
    :try_start_4
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-boolean v2, v2, Lcom/anythink/core/common/e/a;->k:Z

    if-eqz v2, :cond_10

    .line 388
    iget-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Lcom/anythink/core/common/e/a;->n()V
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/net/ConnectException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/StackOverflowError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v3, :cond_f

    .line 492
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_f
    return-void

    .line 394
    :cond_10
    :try_start_5
    invoke-static {v3}, Lcom/anythink/core/common/e/a;->a(Ljava/net/HttpURLConnection;)Ljava/io/InputStream;

    move-result-object v2

    .line 395
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 396
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 398
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 399
    :goto_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 400
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 404
    :cond_11
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 407
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    if-eqz v2, :cond_12

    .line 410
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_12
    const-string v2, "https://api.anythinktech.com/v1/open/app"

    .line 413
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    const-string v2, "https://api.anythinktech.com/v1/open/placement"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    const-string v2, "https://api.anythinktech.com/v1/open/eu"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_13

    goto :goto_3

    .line 436
    :cond_13
    iget-object p1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .line 437
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget v4, p0, Lcom/anythink/core/common/e/a$1;->a:I

    invoke-virtual {v2, v4, p1}, Lcom/anythink/core/common/e/a;->a(ILjava/lang/Object;)V

    goto :goto_4

    .line 415
    :cond_14
    :goto_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/common/g/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 417
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 419
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "code"

    .line 420
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_16

    .line 422
    sget-object p1, Lcom/anythink/core/common/b/e$a;->d:Ljava/lang/String;

    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_15

    .line 424
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 426
    :cond_15
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 427
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    invoke-virtual {v2, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .line 428
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget v4, p0, Lcom/anythink/core/common/e/a$1;->a:I

    invoke-virtual {v2, v4, p1}, Lcom/anythink/core/common/e/a;->a(ILjava/lang/Object;)V

    goto :goto_4

    :cond_16
    const-string v2, "ATSdk"

    .line 431
    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v2, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    const-string v5, "9991"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v4

    invoke-virtual {v2, p1, v4}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_5
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/net/ConnectException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/StackOverflowError; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Error; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_4
    if-eqz v3, :cond_1b

    .line 492
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    :catchall_0
    move-exception p1

    move-object v2, v3

    goto/16 :goto_d

    :catch_0
    move-exception p1

    move-object v2, v3

    goto :goto_5

    :catch_1
    move-exception p1

    move-object v2, v3

    goto :goto_6

    :catch_2
    move-exception p1

    move-object v2, v3

    goto/16 :goto_7

    :catch_3
    move-exception p1

    move-object v2, v3

    goto/16 :goto_8

    :catch_4
    move-exception p1

    move-object v2, v3

    goto/16 :goto_9

    :catch_5
    move-exception p1

    move-object v2, v3

    goto/16 :goto_a

    :catch_6
    move-exception p1

    move-object v2, v3

    goto/16 :goto_b

    :catch_7
    move-exception p1

    move-object v2, v3

    goto/16 :goto_c

    :catchall_1
    move-exception p1

    goto/16 :goto_d

    :catch_8
    move-exception p1

    goto :goto_5

    :catch_9
    move-exception p1

    goto :goto_6

    :catch_a
    move-exception p1

    goto :goto_7

    :catch_b
    move-exception p1

    goto/16 :goto_8

    :catch_c
    move-exception p1

    goto/16 :goto_9

    :catch_d
    move-exception p1

    goto/16 :goto_a

    :catch_e
    move-exception p1

    goto/16 :goto_b

    :catch_f
    move-exception p1

    goto/16 :goto_c

    .line 478
    :goto_5
    :try_start_6
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 479
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    .line 480
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 486
    :cond_17
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 469
    :goto_6
    :try_start_7
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 471
    invoke-virtual {p1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 472
    invoke-virtual {p1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_18

    .line 473
    invoke-virtual {p1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 475
    :cond_18
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 461
    :goto_7
    :try_start_8
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 463
    invoke-virtual {p1}, Ljava/lang/StackOverflowError;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 464
    invoke-virtual {p1}, Ljava/lang/StackOverflowError;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_19

    .line 465
    invoke-virtual {p1}, Ljava/lang/StackOverflowError;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 467
    :cond_19
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/lang/StackOverflowError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 454
    :goto_8
    :try_start_9
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 455
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 456
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1a

    .line 457
    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 459
    :cond_1a
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {v3, v0, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 452
    :goto_9
    :try_start_a
    iget-object v0, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v0, p1}, Lcom/anythink/core/common/e/a;->a(Lorg/apache/http/conn/ConnectTimeoutException;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 450
    :goto_a
    :try_start_b
    iget-object v0, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    const-string v3, "Connect timeout."

    invoke-virtual {p1}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {v0, v3, p1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 446
    :goto_b
    :try_start_c
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/net/ConnectException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v1, v4}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/anythink/core/common/e/a;->a(Lcom/anythink/core/api/AdError;)V

    .line 447
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    const-string v4, "Connect error."

    invoke-virtual {p1}, Ljava/net/ConnectException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v1, v5}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V

    .line 448
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "http connect error! "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/ConnectException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void

    .line 442
    :goto_c
    :try_start_d
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {p1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v1, v4}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/anythink/core/common/e/a;->a(Lcom/anythink/core/api/AdError;)V

    .line 443
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    const-string v4, "UnknownHostException"

    invoke-virtual {p1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v1, v5}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lcom/anythink/core/common/e/a;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V

    .line 444
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "UnknownHostException "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    if-eqz v2, :cond_1b

    .line 492
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1b
    return-void

    :goto_d
    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 494
    :cond_1c
    throw p1
.end method


# virtual methods
.method public final a()V
    .locals 4

    const-string v0, "9999"

    .line 270
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    invoke-virtual {v1}, Lcom/anythink/core/common/e/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 271
    invoke-direct {p0, v1}, Lcom/anythink/core/common/e/a$1;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    .line 283
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 284
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 285
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 287
    :cond_0
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-object v3, v3, Lcom/anythink/core/common/e/a;->j:Lcom/anythink/core/common/e/g;

    if-eqz v3, :cond_1

    .line 288
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-object v3, v3, Lcom/anythink/core/common/e/a;->j:Lcom/anythink/core/common/e/g;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v0, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/anythink/core/common/e/g;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V

    :cond_1
    return-void

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    .line 274
    :goto_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 275
    invoke-virtual {v1}, Ljava/lang/VirtualMachineError;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 276
    invoke-virtual {v1}, Ljava/lang/VirtualMachineError;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 277
    invoke-virtual {v1}, Ljava/lang/VirtualMachineError;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 279
    :cond_2
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-object v3, v3, Lcom/anythink/core/common/e/a;->j:Lcom/anythink/core/common/e/g;

    if-eqz v3, :cond_3

    .line 280
    iget-object v3, p0, Lcom/anythink/core/common/e/a$1;->b:Lcom/anythink/core/common/e/a;

    iget-object v3, v3, Lcom/anythink/core/common/e/a;->j:Lcom/anythink/core/common/e/g;

    invoke-virtual {v1}, Ljava/lang/VirtualMachineError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v0, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/anythink/core/common/e/g;->a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V

    :cond_3
    return-void
.end method
