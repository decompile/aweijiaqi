.class final Lcom/anythink/core/common/d$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/lang/String;D)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/api/ATBaseAdAdapter;

.field final synthetic b:D

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/anythink/core/common/d;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d;Lcom/anythink/core/api/ATBaseAdAdapter;DLjava/lang/String;)V
    .locals 0

    .line 1131
    iput-object p1, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iput-object p2, p0, Lcom/anythink/core/common/d$5;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    iput-wide p3, p0, Lcom/anythink/core/common/d$5;->b:D

    iput-object p5, p0, Lcom/anythink/core/common/d$5;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .line 1134
    iget-object v0, p0, Lcom/anythink/core/common/d$5;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    .line 1135
    iget-object v1, p0, Lcom/anythink/core/common/d$5;->a:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v1

    if-eqz v0, :cond_6

    if-nez v1, :cond_0

    goto :goto_0

    .line 1141
    :cond_0
    iget v2, v1, Lcom/anythink/core/c/d$b;->n:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    return-void

    .line 1146
    :cond_1
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v2

    const-string v3, "2"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v2

    const-string v3, "4"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 1151
    :cond_2
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-wide v2, v2, Lcom/anythink/core/common/d;->i:D

    iget-wide v4, p0, Lcom/anythink/core/common/d$5;->b:D

    cmpl-double v6, v2, v4

    if-lez v6, :cond_3

    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-object v2, v2, Lcom/anythink/core/common/d;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/core/common/d$5;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    return-void

    .line 1155
    :cond_3
    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->B()I

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_4

    return-void

    .line 1159
    :cond_4
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-wide v3, p0, Lcom/anythink/core/common/d$5;->b:D

    iput-wide v3, v2, Lcom/anythink/core/common/d;->i:D

    .line 1160
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-object v3, p0, Lcom/anythink/core/common/d$5;->c:Ljava/lang/String;

    iput-object v3, v2, Lcom/anythink/core/common/d;->j:Ljava/lang/String;

    .line 1162
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-object v2, v2, Lcom/anythink/core/common/d;->h:Lcom/anythink/core/common/e;

    if-eqz v2, :cond_5

    .line 1163
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    iget-object v2, v2, Lcom/anythink/core/common/d;->h:Lcom/anythink/core/common/e;

    invoke-virtual {v2}, Lcom/anythink/core/common/e;->cancel()V

    .line 1164
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/anythink/core/common/d;->h:Lcom/anythink/core/common/e;

    .line 1167
    :cond_5
    iget-object v2, p0, Lcom/anythink/core/common/d$5;->d:Lcom/anythink/core/common/d;

    invoke-static {v2, v1, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/d;)V

    :cond_6
    :goto_0
    return-void
.end method
