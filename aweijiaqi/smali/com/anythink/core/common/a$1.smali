.class final Lcom/anythink/core/common/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/a;->a(Landroid/content/Context;Lcom/anythink/core/common/d/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/d/d;

.field final synthetic b:Lcom/anythink/core/c/d$b;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Lcom/anythink/core/common/d/b;

.field final synthetic e:Lcom/anythink/core/api/ATBaseAdAdapter;

.field final synthetic f:Lcom/anythink/core/common/a;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/a;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;Landroid/content/Context;Lcom/anythink/core/common/d/b;Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/anythink/core/common/a$1;->f:Lcom/anythink/core/common/a;

    iput-object p2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    iput-object p3, p0, Lcom/anythink/core/common/a$1;->b:Lcom/anythink/core/c/d$b;

    iput-object p4, p0, Lcom/anythink/core/common/a$1;->c:Landroid/content/Context;

    iput-object p5, p0, Lcom/anythink/core/common/a$1;->d:Lcom/anythink/core/common/d/b;

    iput-object p6, p0, Lcom/anythink/core/common/a$1;->e:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 600
    iget-object v0, p0, Lcom/anythink/core/common/a$1;->f:Lcom/anythink/core/common/a;

    monitor-enter v0

    .line 602
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v1

    .line 603
    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->b:Lcom/anythink/core/c/d$b;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->b:Lcom/anythink/core/c/d$b;

    .line 2091
    iget-wide v3, v3, Lcom/anythink/core/c/d$b;->m:D

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0x0

    .line 603
    :goto_0
    invoke-virtual {v1, v2, v3, v4}, Lcom/anythink/core/common/d;->a(Ljava/lang/String;D)V

    .line 605
    iget-object v1, p0, Lcom/anythink/core/common/a$1;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/anythink/core/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    iget-object v1, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;)V

    .line 608
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    iget-object v1, p0, Lcom/anythink/core/common/a$1;->f:Lcom/anythink/core/common/a;

    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v3}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/core/common/a$1;->d:Lcom/anythink/core/common/d/b;

    invoke-virtual {v1, v2, v3, v4}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V

    .line 616
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/b/e;->b()Lcom/anythink/core/api/MediationBidManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 618
    iget-object v2, p0, Lcom/anythink/core/common/a$1;->a:Lcom/anythink/core/common/d/d;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/a$1;->e:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/api/MediationBidManager;->notifyWinnerDisplay(Ljava/lang/String;Lcom/anythink/core/c/d$b;)V

    .line 620
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
