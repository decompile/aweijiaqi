.class final Lcom/anythink/core/common/d$1$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d$1$2;->a(Lcom/anythink/core/c/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/c/d;

.field final synthetic b:Lcom/anythink/core/common/d$1$2;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d$1$2;Lcom/anythink/core/c/d;)V
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iput-object p2, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .line 319
    iget-object v0, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v0, v0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v0, v0, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    monitor-enter v0

    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v1, v1, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v2, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    invoke-virtual {v2}, Lcom/anythink/core/c/d;->x()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;J)J

    .line 322
    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    iget-object v2, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d;)V

    .line 325
    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    invoke-virtual {v1}, Lcom/anythink/core/c/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v2, v2, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v2, v2, Lcom/anythink/core/common/d$1;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "3003"

    const-string v2, ""

    .line 326
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Format corresponding to API: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v4, v4, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v4, v4, Lcom/anythink/core/common/d$1;->e:Ljava/lang/String;

    .line 327
    invoke-static {v4}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", Format corresponding to placement strategy: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    .line 328
    invoke-virtual {v4}, Lcom/anythink/core/c/d;->C()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 326
    invoke-static {v1, v2, v3}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    .line 329
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    new-instance v3, Lcom/anythink/core/common/d$1$2$1$1;

    invoke-direct {v3, p0, v1}, Lcom/anythink/core/common/d$1$2$1$1;-><init>(Lcom/anythink/core/common/d$1$2$1;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 336
    iget-object v2, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v2, v2, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 339
    iget-object v2, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v2, v2, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    invoke-static {v2, v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    .line 340
    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v1, v1, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iput-boolean v3, v1, Lcom/anythink/core/common/d;->f:Z

    .line 341
    monitor-exit v0

    return-void

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v2, v1, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v3, v1, Lcom/anythink/core/common/d$1$2;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v4, v1, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v5, v1, Lcom/anythink/core/common/d$1$2;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/anythink/core/common/d$1$2$1;->a:Lcom/anythink/core/c/d;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v7, v1, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2$1;->b:Lcom/anythink/core/common/d$1$2;

    iget-object v1, v1, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v8, v1, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-static/range {v2 .. v8}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/g;)V

    .line 345
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
