.class final Lcom/anythink/core/common/p$a$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/core/common/p$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field a:Lcom/anythink/core/c/d;

.field b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field final synthetic d:Lcom/anythink/core/common/p$a;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/p$a;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/anythink/core/common/p$a$a;->d:Lcom/anythink/core/common/p$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 216
    iget-object v0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private declared-synchronized a(Lcom/anythink/core/c/d$b;)V
    .locals 7

    monitor-enter p0

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 221
    monitor-exit p0

    return-void

    .line 224
    :cond_0
    :try_start_1
    iget v0, p1, Lcom/anythink/core/c/d$b;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    .line 227
    :goto_0
    iget-object v2, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 228
    iget-object v2, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 229
    iget v3, v2, Lcom/anythink/core/c/d$b;->a:I

    if-eq v3, v1, :cond_4

    iget-wide v3, p1, Lcom/anythink/core/c/d$b;->m:D

    iget-wide v5, v2, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double v2, v3, v5

    if-ltz v2, :cond_2

    goto :goto_1

    .line 233
    :cond_2
    iget-object v2, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_3

    .line 234
    iget-object v0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    monitor-exit p0

    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_4
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 231
    monitor-exit p0

    return-void

    .line 242
    :cond_5
    monitor-exit p0

    return-void

    .line 225
    :cond_6
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static synthetic a(Lcom/anythink/core/common/p$a$a;)V
    .locals 0

    .line 210
    invoke-direct {p0}, Lcom/anythink/core/common/p$a$a;->b()V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/p$a$a;Lcom/anythink/core/c/d$b;)V
    .locals 0

    .line 210
    invoke-direct {p0, p1}, Lcom/anythink/core/common/p$a$a;->a(Lcom/anythink/core/c/d$b;)V

    return-void
.end method

.method private static synthetic b(Lcom/anythink/core/common/p$a$a;)Ljava/util/List;
    .locals 0

    .line 1216
    iget-object p0, p0, Lcom/anythink/core/common/p$a$a;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object p0
.end method

.method private declared-synchronized b()V
    .locals 1

    monitor-enter p0

    .line 245
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/common/p$a$a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 246
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 248
    :try_start_1
    iput-boolean v0, p0, Lcom/anythink/core/common/p$a$a;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()Z
    .locals 1

    .line 252
    iget-boolean v0, p0, Lcom/anythink/core/common/p$a$a;->c:Z

    return v0
.end method

.method private static synthetic c(Lcom/anythink/core/common/p$a$a;)Z
    .locals 0

    .line 1252
    iget-boolean p0, p0, Lcom/anythink/core/common/p$a$a;->c:Z

    return p0
.end method
