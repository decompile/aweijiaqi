.class public final Lcom/anythink/core/common/d/a;
.super Ljava/lang/Object;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:J

.field public f:J

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;

.field public k:Lcom/anythink/core/c/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/anythink/core/common/d/a;
    .locals 6

    .line 31
    new-instance v0, Lcom/anythink/core/common/d/a;

    invoke-direct {v0}, Lcom/anythink/core/common/d/a;-><init>()V

    .line 32
    iget-object v1, p0, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iput-object v1, v0, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    .line 33
    iget-object v1, p0, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    .line 34
    iget-object v1, p0, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    .line 35
    iget v1, p0, Lcom/anythink/core/common/d/a;->d:I

    iput v1, v0, Lcom/anythink/core/common/d/a;->d:I

    .line 36
    iget-wide v1, p0, Lcom/anythink/core/common/d/a;->f:J

    iput-wide v1, v0, Lcom/anythink/core/common/d/a;->f:J

    .line 37
    iget-object v1, p0, Lcom/anythink/core/common/d/a;->h:Ljava/util/List;

    iput-object v1, v0, Lcom/anythink/core/common/d/a;->h:Ljava/util/List;

    .line 38
    iget-object v1, p0, Lcom/anythink/core/common/d/a;->k:Lcom/anythink/core/c/d;

    iput-object v1, v0, Lcom/anythink/core/common/d/a;->k:Lcom/anythink/core/c/d;

    .line 39
    iget-wide v1, p0, Lcom/anythink/core/common/d/a;->e:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    const-wide/16 v1, 0x2710

    .line 40
    iput-wide v1, v0, Lcom/anythink/core/common/d/a;->e:J

    goto :goto_0

    .line 42
    :cond_0
    iput-wide v1, v0, Lcom/anythink/core/common/d/a;->e:J

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/anythink/core/common/d/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)",
            "Lcom/anythink/core/common/d/a;"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Lcom/anythink/core/common/d/a;->a()Lcom/anythink/core/common/d/a;

    move-result-object v0

    .line 50
    iput-object p1, v0, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 52
    iget-object p1, p0, Lcom/anythink/core/common/d/a;->j:Ljava/lang/String;

    iput-object p1, v0, Lcom/anythink/core/common/d/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/util/List;)Lcom/anythink/core/common/d/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)",
            "Lcom/anythink/core/common/d/a;"
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Lcom/anythink/core/common/d/a;->a()Lcom/anythink/core/common/d/a;

    move-result-object v0

    .line 59
    iput-object p1, v0, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    return-object v0
.end method
