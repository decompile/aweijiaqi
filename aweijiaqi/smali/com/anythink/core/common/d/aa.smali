.class public abstract Lcom/anythink/core/common/d/aa;
.super Ljava/lang/Object;


# static fields
.field public static final K:Ljava/lang/String; = "ofm_tid_key"

.field public static final L:I = 0x1

.field public static final M:I = 0x2

.field public static final N:I = 0x3

.field public static final O:I = 0x4

.field public static final P:I = 0x5

.field public static final Q:I = 0x6

.field public static final R:I = 0x7

.field public static final S:I = 0x8

.field public static final T:I = 0x0

.field public static final U:I = 0x1

.field public static final V:I = 0x2

.field public static final W:I = 0x3

.field public static final X:I = 0x4

.field public static final Y:I = 0x5

.field public static final Z:Ljava/lang/String; = "0"

.field public static final aa:Ljava/lang/String; = "1"

.field public static final ab:Ljava/lang/String; = "2"

.field public static final ac:Ljava/lang/String; = "3"

.field public static final ad:Ljava/lang/String; = "4"


# instance fields
.field protected ae:Ljava/lang/String;

.field protected af:Ljava/lang/String;

.field protected ag:Ljava/lang/String;

.field protected ah:Ljava/lang/String;

.field public ai:Ljava/lang/String;

.field public aj:I

.field public ak:I

.field protected al:Ljava/lang/String;

.field protected am:I

.field protected an:I

.field protected ao:I

.field protected ap:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 46
    iput-object v0, p0, Lcom/anythink/core/common/d/aa;->ah:Ljava/lang/String;

    return-void
.end method

.method private a()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/anythink/core/common/d/aa;->ao:I

    return v0
.end method

.method private a(I)V
    .locals 0

    .line 107
    iput p1, p0, Lcom/anythink/core/common/d/aa;->aj:I

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->ai:Ljava/lang/String;

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->ai:Ljava/lang/String;

    return-object v0
.end method

.method private b(I)V
    .locals 0

    .line 115
    iput p1, p0, Lcom/anythink/core/common/d/aa;->ak:I

    return-void
.end method

.method private c()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/anythink/core/common/d/aa;->aj:I

    return v0
.end method

.method private d()I
    .locals 1

    .line 111
    iget v0, p0, Lcom/anythink/core/common/d/aa;->ak:I

    return v0
.end method


# virtual methods
.method public final F()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/anythink/core/common/d/aa;->am:I

    return v0
.end method

.method public final G()I
    .locals 1

    .line 79
    iget v0, p0, Lcom/anythink/core/common/d/aa;->an:I

    return v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->al:Ljava/lang/String;

    return-object v0
.end method

.method public final I()I
    .locals 1

    .line 121
    iget v0, p0, Lcom/anythink/core/common/d/aa;->ap:I

    return v0
.end method

.method public final J()Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->af:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->ag:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 6

    .line 169
    iget-object v0, p0, Lcom/anythink/core/common/d/aa;->ag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :pswitch_1
    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :pswitch_2
    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :pswitch_3
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_4
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_5

    if-eq v0, v5, :cond_4

    if-eq v0, v4, :cond_3

    if-eq v0, v3, :cond_2

    if-eq v0, v2, :cond_1

    const-string v0, "none"

    return-object v0

    :cond_1
    const-string v0, "splash"

    return-object v0

    :cond_2
    const-string v0, "inter"

    return-object v0

    :cond_3
    const-string v0, "banner"

    return-object v0

    :cond_4
    const-string v0, "reward"

    return-object v0

    :cond_5
    const-string v0, "native"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public r(I)Lorg/json/JSONObject;
    .locals 3

    .line 189
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "type"

    .line 191
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "pl_id"

    .line 192
    iget-object v1, p0, Lcom/anythink/core/common/d/aa;->ae:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "req_id"

    .line 193
    iget-object v1, p0, Lcom/anythink/core/common/d/aa;->af:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "format"

    .line 194
    iget-object v1, p0, Lcom/anythink/core/common/d/aa;->ag:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "ps_id"

    .line 195
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "sessionid"

    .line 196
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/d/aa;->ae:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "traffic_group_id"

    .line 197
    iget v1, p0, Lcom/anythink/core/common/d/aa;->ap:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 198
    iget p1, p0, Lcom/anythink/core/common/d/aa;->ao:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const-string p1, "ofm_tid"

    .line 199
    iget v1, p0, Lcom/anythink/core/common/d/aa;->an:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "ofm_system"

    .line 200
    iget v1, p0, Lcom/anythink/core/common/d/aa;->am:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :cond_0
    const-string p1, "is_ofm"

    .line 202
    iget v1, p0, Lcom/anythink/core/common/d/aa;->ao:I

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "asid"

    .line 203
    iget-object v1, p0, Lcom/anythink/core/common/d/aa;->ah:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 206
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->al:Ljava/lang/String;

    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->ah:Ljava/lang/String;

    return-void
.end method

.method public final t(I)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/anythink/core/common/d/aa;->ao:I

    return-void
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->ae:Ljava/lang/String;

    return-void
.end method

.method public final u(I)V
    .locals 0

    .line 75
    iput p1, p0, Lcom/anythink/core/common/d/aa;->am:I

    return-void
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->af:Ljava/lang/String;

    return-void
.end method

.method public final v(I)V
    .locals 0

    .line 83
    iput p1, p0, Lcom/anythink/core/common/d/aa;->an:I

    return-void
.end method

.method public final v(Ljava/lang/String;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/anythink/core/common/d/aa;->ag:Ljava/lang/String;

    return-void
.end method

.method public final w(I)V
    .locals 0

    .line 125
    iput p1, p0, Lcom/anythink/core/common/d/aa;->ap:I

    return-void
.end method
