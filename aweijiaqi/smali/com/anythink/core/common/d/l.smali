.class public final Lcom/anythink/core/common/d/l;
.super Lcom/anythink/core/common/d/k;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct/range {p0 .. p8}, Lcom/anythink/core/common/d/k;-><init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "${AUCTION_PRICE}"

    .line 30
    iput-object p1, p0, Lcom/anythink/core/common/d/l;->j:Ljava/lang/String;

    const-string p1, "${AUCTION_LOSS}"

    .line 31
    iput-object p1, p0, Lcom/anythink/core/common/d/l;->k:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/anythink/core/common/d/l;
    .locals 11

    .line 111
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "is_success"

    .line 112
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    const-string p0, "bid_id"

    .line 113
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string p0, "price"

    .line 114
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-string p0, "nurl"

    .line 115
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string p0, "lurl"

    .line 116
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string p0, "burl"

    .line 117
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string p0, "err_msg"

    .line 118
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 120
    new-instance p0, Lcom/anythink/core/common/d/l;

    move-object v2, p0

    invoke-direct/range {v2 .. v10}, Lcom/anythink/core/common/d/l;-><init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "cur"

    .line 121
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/l;->b:Ljava/lang/String;

    const-string v1, "unit_id"

    .line 122
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/l;->c:Ljava/lang/String;

    const-string v1, "nw_firm_id"

    .line 123
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/anythink/core/common/d/l;->d:I

    const-string v1, "err_code"

    .line 124
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/anythink/core/common/d/l;->a:I

    const-string v1, "expire"

    .line 125
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/anythink/core/common/d/l;->e:J

    const-string v1, "out_data_time"

    .line 126
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/anythink/core/common/d/l;->f:J

    const-string v1, "is_send_winurl"

    .line 127
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/anythink/core/common/d/l;->h:Z

    const-string v1, "offer_data"

    .line 128
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    const-string v1, "tp_bid_id"

    .line 129
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/d/l;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p0

    :catchall_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public final a(D)V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/anythink/core/common/d/l;->loseNoticeUrl:Ljava/lang/String;

    .line 75
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    const-string p2, "${AUCTION_PRICE}"

    invoke-virtual {v0, p2, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 77
    new-instance p2, Lcom/anythink/core/common/e/e;

    invoke-direct {p2, p1}, Lcom/anythink/core/common/e/e;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/anythink/core/common/e/e;->a(ILcom/anythink/core/common/e/g;)V

    :cond_0
    return-void
.end method

.method public final a(ZD)V
    .locals 5

    .line 52
    iget-object v0, p0, Lcom/anythink/core/common/d/l;->displayNoticeUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 55
    :cond_0
    iget v0, p0, Lcom/anythink/core/common/d/l;->d:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "${AUCTION_PRICE}"

    if-eq v0, v1, :cond_1

    .line 68
    new-instance p1, Lcom/anythink/core/common/e/e;

    iget-object v0, p0, Lcom/anythink/core/common/d/l;->displayNoticeUrl:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v4, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/anythink/core/common/e/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3, v2}, Lcom/anythink/core/common/e/e;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    :cond_1
    const-string v0, "${AUCTION_LOSS}"

    if-eqz p1, :cond_2

    .line 59
    new-instance p1, Lcom/anythink/core/common/e/e;

    iget-object v1, p0, Lcom/anythink/core/common/d/l;->displayNoticeUrl:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v4, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "0"

    .line 60
    invoke-virtual {p2, v0, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/anythink/core/common/e/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3, v2}, Lcom/anythink/core/common/e/e;->a(ILcom/anythink/core/common/e/g;)V

    return-void

    .line 62
    :cond_2
    new-instance p1, Lcom/anythink/core/common/e/e;

    iget-object v1, p0, Lcom/anythink/core/common/d/l;->displayNoticeUrl:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v4, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "103"

    .line 63
    invoke-virtual {p2, v0, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/anythink/core/common/e/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3, v2}, Lcom/anythink/core/common/e/e;->a(ILcom/anythink/core/common/e/g;)V

    return-void
.end method

.method public final a()Z
    .locals 5

    .line 38
    iget-wide v0, p0, Lcom/anythink/core/common/d/l;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 3

    .line 42
    iget-boolean v0, p0, Lcom/anythink/core/common/d/l;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 45
    iput-boolean v0, p0, Lcom/anythink/core/common/d/l;->h:Z

    .line 46
    iget-object v0, p0, Lcom/anythink/core/common/d/l;->winNoticeUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lcom/anythink/core/common/e/e;

    iget-object v1, p0, Lcom/anythink/core/common/d/l;->winNoticeUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/anythink/core/common/e/e;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/e/e;->a(ILcom/anythink/core/common/e/g;)V

    :cond_1
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    .line 83
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "bid_id"

    .line 85
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "cur"

    .line 86
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "price"

    .line 87
    iget-wide v2, p0, Lcom/anythink/core/common/d/l;->price:D

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v1, "nurl"

    .line 88
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->winNoticeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "lurl"

    .line 89
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->loseNoticeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "unit_id"

    .line 90
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "nw_firm_id"

    .line 91
    iget v2, p0, Lcom/anythink/core/common/d/l;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "is_success"

    .line 92
    iget-boolean v2, p0, Lcom/anythink/core/common/d/l;->isSuccess:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "err_code"

    .line 93
    iget v2, p0, Lcom/anythink/core/common/d/l;->a:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "err_msg"

    .line 94
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->errorMsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "expire"

    .line 95
    iget-wide v2, p0, Lcom/anythink/core/common/d/l;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "out_data_time"

    .line 96
    iget-wide v2, p0, Lcom/anythink/core/common/d/l;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "is_send_winurl"

    .line 97
    iget-boolean v2, p0, Lcom/anythink/core/common/d/l;->h:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "offer_data"

    .line 98
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "tp_bid_id"

    .line 99
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "burl"

    .line 100
    iget-object v2, p0, Lcom/anythink/core/common/d/l;->displayNoticeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :catchall_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
