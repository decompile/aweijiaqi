.class public abstract Lcom/anythink/core/common/d/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ljava/lang/String; = "320x50"

.field public static final b:Ljava/lang/String; = "320x90"

.field public static final c:Ljava/lang/String; = "300x250"

.field public static final d:Ljava/lang/String; = "728x90"


# instance fields
.field protected e:I

.field protected f:I

.field protected g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:J

.field private q:I

.field private r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:J

.field private v:I

.field private w:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/anythink/core/common/d/j;->w:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final a(I)V
    .locals 0

    .line 67
    iput p1, p0, Lcom/anythink/core/common/d/j;->w:I

    return-void
.end method

.method public final a(J)V
    .locals 0

    .line 95
    iput-wide p1, p0, Lcom/anythink/core/common/d/j;->p:J

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/anythink/core/common/d/j;->r:Ljava/lang/String;

    return-void
.end method

.method public final b()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/anythink/core/common/d/j;->v:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(I)V
    .locals 0

    .line 79
    iput p1, p0, Lcom/anythink/core/common/d/j;->v:I

    return-void
.end method

.method public final b(J)V
    .locals 0

    .line 204
    iput-wide p1, p0, Lcom/anythink/core/common/d/j;->u:J

    return-void
.end method

.method public final c()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/anythink/core/common/d/j;->t:I

    return v0
.end method

.method public final c(I)V
    .locals 0

    .line 87
    iput p1, p0, Lcom/anythink/core/common/d/j;->t:I

    return-void
.end method

.method public final d()J
    .locals 2

    .line 91
    iget-wide v0, p0, Lcom/anythink/core/common/d/j;->p:J

    return-wide v0
.end method

.method public final d(I)V
    .locals 0

    .line 103
    iput p1, p0, Lcom/anythink/core/common/d/j;->n:I

    return-void
.end method

.method public final e()I
    .locals 1

    .line 99
    iget v0, p0, Lcom/anythink/core/common/d/j;->n:I

    return v0
.end method

.method public final e(I)V
    .locals 0

    .line 115
    iput p1, p0, Lcom/anythink/core/common/d/j;->o:I

    return-void
.end method

.method public final f()I
    .locals 1

    .line 111
    iget v0, p0, Lcom/anythink/core/common/d/j;->o:I

    return v0
.end method

.method public final f(I)V
    .locals 0

    .line 127
    iput p1, p0, Lcom/anythink/core/common/d/j;->q:I

    return-void
.end method

.method public final g()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/anythink/core/common/d/j;->q:I

    return v0
.end method

.method public final g(I)V
    .locals 0

    .line 147
    iput p1, p0, Lcom/anythink/core/common/d/j;->s:I

    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/anythink/core/common/d/j;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final h(I)V
    .locals 0

    .line 155
    iput p1, p0, Lcom/anythink/core/common/d/j;->h:I

    return-void
.end method

.method public final i()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/anythink/core/common/d/j;->s:I

    return v0
.end method

.method public final i(I)V
    .locals 0

    .line 163
    iput p1, p0, Lcom/anythink/core/common/d/j;->i:I

    return-void
.end method

.method public final j()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/anythink/core/common/d/j;->h:I

    return v0
.end method

.method public final j(I)V
    .locals 0

    .line 171
    iput p1, p0, Lcom/anythink/core/common/d/j;->j:I

    return-void
.end method

.method public final k()I
    .locals 1

    .line 159
    iget v0, p0, Lcom/anythink/core/common/d/j;->i:I

    return v0
.end method

.method public final k(I)V
    .locals 0

    .line 179
    iput p1, p0, Lcom/anythink/core/common/d/j;->k:I

    return-void
.end method

.method public final l()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/anythink/core/common/d/j;->j:I

    return v0
.end method

.method public final l(I)V
    .locals 0

    .line 187
    iput p1, p0, Lcom/anythink/core/common/d/j;->l:I

    return-void
.end method

.method public final m()I
    .locals 1

    .line 175
    iget v0, p0, Lcom/anythink/core/common/d/j;->k:I

    return v0
.end method

.method public final m(I)V
    .locals 0

    .line 195
    iput p1, p0, Lcom/anythink/core/common/d/j;->m:I

    return-void
.end method

.method public final n()I
    .locals 1

    .line 183
    iget v0, p0, Lcom/anythink/core/common/d/j;->l:I

    return v0
.end method

.method public final n(I)V
    .locals 0

    .line 212
    iput p1, p0, Lcom/anythink/core/common/d/j;->e:I

    return-void
.end method

.method public final o()I
    .locals 1

    .line 191
    iget v0, p0, Lcom/anythink/core/common/d/j;->m:I

    return v0
.end method

.method public final o(I)V
    .locals 0

    .line 220
    iput p1, p0, Lcom/anythink/core/common/d/j;->f:I

    return-void
.end method

.method public final p()J
    .locals 2

    .line 200
    iget-wide v0, p0, Lcom/anythink/core/common/d/j;->u:J

    return-wide v0
.end method

.method public final p(I)V
    .locals 0

    .line 228
    iput p1, p0, Lcom/anythink/core/common/d/j;->g:I

    return-void
.end method

.method public final q()I
    .locals 1

    .line 208
    iget v0, p0, Lcom/anythink/core/common/d/j;->e:I

    return v0
.end method

.method public final r()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/anythink/core/common/d/j;->f:I

    return v0
.end method

.method public final s()I
    .locals 1

    .line 224
    iget v0, p0, Lcom/anythink/core/common/d/j;->g:I

    return v0
.end method
