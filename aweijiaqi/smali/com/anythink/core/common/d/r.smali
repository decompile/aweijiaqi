.class public Lcom/anythink/core/common/d/r;
.super Lcom/anythink/core/common/d/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/anythink/core/common/d/j;-><init>()V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/anythink/core/common/d/r;
    .locals 4

    .line 23
    new-instance v0, Lcom/anythink/core/common/d/r;

    invoke-direct {v0}, Lcom/anythink/core/common/d/r;-><init>()V

    .line 24
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 26
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "f_t"

    .line 28
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->h(I)V

    const-string p0, "v_c"

    .line 29
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->i(I)V

    const-string p0, "s_b_t"

    .line 30
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->j(I)V

    const-string p0, "e_c_a"

    .line 31
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->k(I)V

    const-string p0, "v_m"

    .line 32
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->l(I)V

    const-string p0, "s_c_t"

    .line 33
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->m(I)V

    const-string p0, "m_t"

    .line 34
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->c(I)V

    const-string p0, "o_c_t"

    .line 35
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/anythink/core/common/d/r;->b(J)V

    const-string p0, "ak_cfm"

    .line 37
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->d(I)V

    const-string p0, "ctdown_time"

    .line 39
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/anythink/core/common/d/r;->a(J)V

    const-string p0, "sk_able"

    .line 40
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->e(I)V

    const-string p0, "orient"

    .line 41
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->f(I)V

    const-string p0, "size"

    .line 42
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->a(Ljava/lang/String;)V

    const-string p0, "cl_btn"

    .line 43
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->g(I)V

    const-string p0, "ec_r"

    .line 46
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->n(I)V

    const-string p0, "ec_s_t"

    .line 47
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->o(I)V

    const-string p0, "ec_l_t"

    .line 48
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/r;->p(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 51
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    :goto_0
    return-object v0
.end method
