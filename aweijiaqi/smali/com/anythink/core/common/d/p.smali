.class public final Lcom/anythink/core/common/d/p;
.super Lcom/anythink/core/common/d/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/d/h<",
        "Lcom/anythink/core/common/d/r;",
        ">;"
    }
.end annotation


# instance fields
.field public A:J

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:J

.field private M:I

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/anythink/core/common/d/h;-><init>()V

    return-void
.end method

.method private J()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->R:Ljava/lang/String;

    return-object v0
.end method

.method private K()I
    .locals 1

    .line 178
    iget v0, p0, Lcom/anythink/core/common/d/p;->z:I

    return v0
.end method

.method private L()J
    .locals 2

    .line 186
    iget-wide v0, p0, Lcom/anythink/core/common/d/p;->A:J

    return-wide v0
.end method

.method private M()J
    .locals 2

    .line 195
    iget-wide v0, p0, Lcom/anythink/core/common/d/p;->L:J

    return-wide v0
.end method

.method private b(Lcom/anythink/core/common/d/r;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/common/d/r;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 225
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 229
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_b

    .line 230
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v4, -0x1

    .line 232
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/4 v6, 0x2

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v5, "728x90"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x2

    goto :goto_0

    :sswitch_1
    const-string v5, "320x90"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :sswitch_2
    const-string v5, "320x50"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x3

    goto :goto_0

    :sswitch_3
    const-string v5, "300x250"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x1

    :cond_0
    :goto_0
    if-eqz v4, :cond_5

    if-eq v4, v2, :cond_3

    if-eq v4, v6, :cond_1

    .line 268
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 270
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 256
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 257
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_2
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 260
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 245
    :cond_3
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 246
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 248
    :cond_4
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 249
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 234
    :cond_5
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 235
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 v1, 0x1

    goto :goto_3

    .line 237
    :cond_6
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 238
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_2
    const/4 v1, 0x0

    :goto_3
    const/4 v4, 0x1

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_4
    if-nez v1, :cond_a

    .line 276
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 277
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    .line 283
    :cond_a
    :goto_5
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 284
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    const/4 v4, 0x1

    .line 289
    :cond_c
    :goto_6
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "1"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 290
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 291
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_d
    const/4 v4, 0x0

    .line 296
    :goto_7
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 297
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    :cond_e
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 301
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_f
    const/4 v4, 0x0

    .line 306
    :goto_8
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 307
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_10
    const/4 v4, 0x0

    .line 313
    :cond_11
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "3"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 314
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 315
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_12
    const/4 v4, 0x0

    .line 320
    :goto_a
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 321
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_13
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 325
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_14
    const/4 v4, 0x0

    .line 330
    :goto_b
    iget v1, p0, Lcom/anythink/core/common/d/p;->r:I

    if-ne v1, v2, :cond_16

    .line 331
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 332
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_15
    const/4 v4, 0x0

    .line 341
    :cond_16
    :goto_c
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "4"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 342
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_17

    .line 343
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    :cond_17
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_19

    .line 347
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_18
    move v3, v4

    :cond_19
    if-eqz v3, :cond_1a

    return-object v0

    :cond_1a
    const/4 p1, 0x0

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x215ddd38 -> :sswitch_3
        0x59df59c2 -> :sswitch_2
        0x59df5a3e -> :sswitch_1
        0x60b65fb2 -> :sswitch_0
    .end sparse-switch
.end method

.method private b(J)V
    .locals 0

    .line 190
    iput-wide p1, p0, Lcom/anythink/core/common/d/p;->A:J

    return-void
.end method

.method private f(I)V
    .locals 0

    .line 182
    iput p1, p0, Lcom/anythink/core/common/d/p;->z:I

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final A(Ljava/lang/String;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->G:Ljava/lang/String;

    return-void
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final B(Ljava/lang/String;)V
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->H:Ljava/lang/String;

    return-void
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final C(Ljava/lang/String;)V
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->I:Ljava/lang/String;

    return-void
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final D(Ljava/lang/String;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->J:Ljava/lang/String;

    return-void
.end method

.method public final E()Ljava/lang/String;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final E(Ljava/lang/String;)V
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->K:Ljava/lang/String;

    return-void
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final F(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 375
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/anythink/core/common/d/p;->R:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 379
    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 380
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 382
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\\{"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\\}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    :cond_0
    return-object p1
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Lcom/anythink/core/common/d/j;)Ljava/util/List;
    .locals 7

    .line 20
    check-cast p1, Lcom/anythink/core/common/d/r;

    .line 1222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1225
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 1229
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_b

    .line 1230
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v4, -0x1

    .line 1232
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/4 v6, 0x2

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v5, "728x90"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x2

    goto :goto_0

    :sswitch_1
    const-string v5, "320x90"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :sswitch_2
    const-string v5, "320x50"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x3

    goto :goto_0

    :sswitch_3
    const-string v5, "300x250"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x1

    :cond_0
    :goto_0
    if-eqz v4, :cond_5

    if-eq v4, v2, :cond_3

    if-eq v4, v6, :cond_1

    .line 1268
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1270
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1256
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1257
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1259
    :cond_2
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1260
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1245
    :cond_3
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1246
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1248
    :cond_4
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1249
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1234
    :cond_5
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1235
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 v1, 0x1

    goto :goto_3

    .line 1237
    :cond_6
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1238
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_2
    const/4 v1, 0x0

    :goto_3
    const/4 v4, 0x1

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_4
    if-nez v1, :cond_a

    .line 1276
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1277
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    .line 1283
    :cond_a
    :goto_5
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1284
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    const/4 v4, 0x1

    .line 1289
    :cond_c
    :goto_6
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "1"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1290
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1291
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_d
    const/4 v4, 0x0

    .line 1296
    :goto_7
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1297
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1300
    :cond_e
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1301
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_f
    const/4 v4, 0x0

    .line 1306
    :goto_8
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1307
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_10
    const/4 v4, 0x0

    .line 1313
    :cond_11
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "3"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 1314
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 1315
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_12
    const/4 v4, 0x0

    .line 1320
    :goto_a
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 1321
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1324
    :cond_13
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1325
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_14
    const/4 v4, 0x0

    .line 1330
    :goto_b
    iget v1, p0, Lcom/anythink/core/common/d/p;->r:I

    if-ne v1, v2, :cond_16

    .line 1331
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1332
    iget-object v1, p0, Lcom/anythink/core/common/d/p;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_15
    const/4 v4, 0x0

    .line 1341
    :cond_16
    :goto_c
    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->j()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "4"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 1342
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_17

    .line 1343
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1346
    :cond_17
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_19

    .line 1347
    iget-object p1, p0, Lcom/anythink/core/common/d/p;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_18
    move v3, v4

    :cond_19
    if-eqz v3, :cond_1a

    return-object v0

    :cond_1a
    const/4 p1, 0x0

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x215ddd38 -> :sswitch_3
        0x59df59c2 -> :sswitch_2
        0x59df5a3e -> :sswitch_1
        0x60b65fb2 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(J)V
    .locals 0

    .line 199
    iput-wide p1, p0, Lcom/anythink/core/common/d/p;->L:J

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->R:Ljava/lang/String;

    return-void
.end method

.method public final a(Lcom/anythink/core/common/d/r;)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 211
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/anythink/core/common/d/p;->L:J

    sub-long/2addr v1, v3

    invoke-virtual {p1}, Lcom/anythink/core/common/d/r;->p()J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-lez p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e(I)V
    .locals 0

    .line 93
    iput p1, p0, Lcom/anythink/core/common/d/p;->M:I

    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->N:Ljava/lang/String;

    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    return-void
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    return-void
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    return-void
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final v(Ljava/lang/String;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->B:Ljava/lang/String;

    return-void
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final w(Ljava/lang/String;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->C:Ljava/lang/String;

    return-void
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final x(Ljava/lang/String;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->D:Ljava/lang/String;

    return-void
.end method

.method public final y()I
    .locals 1

    .line 89
    iget v0, p0, Lcom/anythink/core/common/d/p;->M:I

    return v0
.end method

.method public final y(Ljava/lang/String;)V
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->E:Ljava/lang/String;

    return-void
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/anythink/core/common/d/p;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final z(Ljava/lang/String;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/anythink/core/common/d/p;->F:Ljava/lang/String;

    return-void
.end method
