.class public final Lcom/anythink/core/common/d/v;
.super Lcom/anythink/core/common/d/j;


# instance fields
.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/anythink/core/common/d/j;-><init>()V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/anythink/core/common/d/v;
    .locals 6

    .line 61
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 64
    :cond_0
    new-instance v0, Lcom/anythink/core/common/d/v;

    invoke-direct {v0}, Lcom/anythink/core/common/d/v;-><init>()V

    .line 65
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 67
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "f_t"

    .line 69
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->h(I)V

    const-string p0, "v_c"

    .line 71
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq p0, v4, :cond_2

    if-eq p0, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    .line 80
    :goto_0
    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->i(I)V

    const-string p0, "s_b_t"

    .line 82
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->j(I)V

    const-string p0, "e_c_a"

    .line 85
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    if-eq p0, v4, :cond_5

    if-eq p0, v3, :cond_4

    const/4 v5, 0x3

    if-eq p0, v5, :cond_3

    goto :goto_1

    :cond_3
    const/4 p0, 0x2

    goto :goto_1

    :cond_4
    const/4 p0, 0x1

    goto :goto_1

    :cond_5
    const/4 p0, 0x0

    .line 97
    :goto_1
    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->k(I)V

    const-string p0, "ak_cfm"

    .line 100
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    if-eq p0, v4, :cond_7

    if-eq p0, v3, :cond_6

    goto :goto_2

    :cond_6
    const/4 p0, 0x1

    goto :goto_2

    :cond_7
    const/4 p0, 0x0

    .line 109
    :goto_2
    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->d(I)V

    const-string p0, "m_t"

    .line 111
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->c(I)V

    const-string p0, "cm"

    .line 120
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    if-eq p0, v4, :cond_9

    if-eq p0, v3, :cond_8

    move v2, p0

    goto :goto_3

    :cond_8
    const/4 v2, 0x1

    .line 1041
    :cond_9
    :goto_3
    iput v2, v0, Lcom/anythink/core/common/d/v;->h:I

    const-string p0, "ipua"

    .line 131
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    .line 1049
    iput p0, v0, Lcom/anythink/core/common/d/v;->i:I

    const-string p0, "clua"

    .line 132
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    .line 1057
    iput p0, v0, Lcom/anythink/core/common/d/v;->j:I

    const-string p0, "dp_cm"

    .line 133
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->b(I)V

    const-string p0, "l_o_num"

    .line 134
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    .line 2033
    iput p0, v0, Lcom/anythink/core/common/d/v;->k:I

    const-string p0, "ld_t"

    .line 135
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->a(I)V

    const-string p0, "ec_r"

    .line 138
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->n(I)V

    const-string p0, "ec_s_t"

    .line 139
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->o(I)V

    const-string p0, "ec_l_t"

    .line 140
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/anythink/core/common/d/v;->p(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p0

    .line 143
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_a
    :goto_4
    return-object v0
.end method

.method private q(I)V
    .locals 0

    .line 33
    iput p1, p0, Lcom/anythink/core/common/d/v;->k:I

    return-void
.end method

.method private r(I)V
    .locals 0

    .line 41
    iput p1, p0, Lcom/anythink/core/common/d/v;->h:I

    return-void
.end method

.method private s(I)V
    .locals 0

    .line 49
    iput p1, p0, Lcom/anythink/core/common/d/v;->i:I

    return-void
.end method

.method private t(I)V
    .locals 0

    .line 57
    iput p1, p0, Lcom/anythink/core/common/d/v;->j:I

    return-void
.end method


# virtual methods
.method public final t()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/anythink/core/common/d/v;->k:I

    return v0
.end method

.method public final u()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/anythink/core/common/d/v;->h:I

    return v0
.end method

.method public final v()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/anythink/core/common/d/v;->i:I

    return v0
.end method

.method public final w()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/anythink/core/common/d/v;->j:I

    return v0
.end method
