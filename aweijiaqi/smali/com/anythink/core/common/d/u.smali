.class public abstract Lcom/anythink/core/common/d/u;
.super Lcom/anythink/core/common/d/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/d/h<",
        "Lcom/anythink/core/common/d/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final J:I = 0x1

.field public static final K:I = 0x2

.field public static final L:I = 0x3

.field public static final M:I = 0x4

.field public static final N:I = 0x5

.field public static final O:I = 0x6


# instance fields
.field F:J

.field G:Ljava/lang/String;

.field H:Ljava/lang/String;

.field I:I

.field P:Ljava/lang/String;

.field Q:Ljava/lang/String;

.field R:Lcom/anythink/core/common/d/v;

.field S:Lcom/anythink/core/common/d/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/anythink/core/common/d/h;-><init>()V

    return-void
.end method

.method private B()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->Q:Ljava/lang/String;

    return-object v0
.end method

.method private a()J
    .locals 2

    .line 54
    iget-wide v0, p0, Lcom/anythink/core/common/d/u;->F:J

    return-wide v0
.end method

.method private b(Lcom/anythink/core/common/d/v;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/common/d/v;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 112
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 113
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 122
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 127
    :goto_0
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 128
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    .line 134
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "3"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 135
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 136
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_5
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 140
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_6
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 144
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    .line 149
    :goto_2
    invoke-virtual {p0}, Lcom/anythink/core/common/d/u;->t()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 150
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1078
    iget v4, p0, Lcom/anythink/core/common/d/u;->I:I

    if-eq v4, v2, :cond_c

    const/4 v5, 0x2

    if-eq v4, v5, :cond_b

    const/4 v5, 0x3

    if-eq v4, v5, :cond_9

    const/4 v5, 0x4

    if-eq v4, v5, :cond_b

    goto :goto_4

    .line 166
    :cond_9
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->h()Ljava/lang/String;

    move-result-object v4

    const-string v5, "320x50"

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 167
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 168
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 173
    :cond_a
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 174
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 182
    :cond_b
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->P:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    goto :goto_3

    .line 159
    :cond_c
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 160
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    :goto_3
    const/4 v1, 0x0

    .line 192
    :cond_e
    :goto_4
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 193
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_f
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v4, "4"

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_12

    .line 2078
    iget p1, p0, Lcom/anythink/core/common/d/u;->I:I

    if-eq v2, p1, :cond_10

    .line 205
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_10

    .line 206
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_10
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_11

    .line 211
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_11
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_13

    .line 215
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    move v3, v1

    :cond_13
    if-eqz v3, :cond_14

    return-object v0

    :cond_14
    const/4 p1, 0x0

    return-object p1
.end method

.method private v()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->G:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->H:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Lcom/anythink/core/common/d/j;)Ljava/util/List;
    .locals 6

    .line 17
    check-cast p1, Lcom/anythink/core/common/d/v;

    .line 2109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2112
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_4

    .line 2113
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2114
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2117
    :cond_0
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2118
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2121
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2122
    iget-object v1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 2127
    :goto_0
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2128
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    .line 2134
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "3"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2135
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2136
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2139
    :cond_5
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2140
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2143
    :cond_6
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2144
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    .line 2149
    :goto_2
    invoke-virtual {p0}, Lcom/anythink/core/common/d/u;->t()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2150
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->m:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2155
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "2"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 3078
    iget v4, p0, Lcom/anythink/core/common/d/u;->I:I

    if-eq v4, v2, :cond_c

    const/4 v5, 0x2

    if-eq v4, v5, :cond_b

    const/4 v5, 0x3

    if-eq v4, v5, :cond_9

    const/4 v5, 0x4

    if-eq v4, v5, :cond_b

    goto :goto_4

    .line 2166
    :cond_9
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->h()Ljava/lang/String;

    move-result-object v4

    const-string v5, "320x50"

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2167
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2168
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2173
    :cond_a
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2174
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2182
    :cond_b
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->P:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    goto :goto_3

    .line 2159
    :cond_c
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2160
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    :goto_3
    const/4 v1, 0x0

    .line 2192
    :cond_e
    :goto_4
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 2193
    iget-object v4, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2202
    :cond_f
    invoke-virtual {p1}, Lcom/anythink/core/common/d/v;->j()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string v4, "4"

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_12

    .line 4078
    iget p1, p0, Lcom/anythink/core/common/d/u;->I:I

    if-eq v2, p1, :cond_10

    .line 2205
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_10

    .line 2206
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2210
    :cond_10
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_11

    .line 2211
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2214
    :cond_11
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_13

    .line 2215
    iget-object p1, p0, Lcom/anythink/core/common/d/u;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    move v3, v1

    :cond_13
    if-eqz v3, :cond_14

    return-object v0

    :cond_14
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Lcom/anythink/core/common/d/v;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->R:Lcom/anythink/core/common/d/v;

    return-void
.end method

.method public final a(Lcom/anythink/core/common/d/w;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->S:Lcom/anythink/core/common/d/w;

    return-void
.end method

.method public final c(J)V
    .locals 0

    .line 58
    iput-wide p1, p0, Lcom/anythink/core/common/d/u;->F:J

    return-void
.end method

.method public final g(I)V
    .locals 0

    .line 82
    iput p1, p0, Lcom/anythink/core/common/d/u;->I:I

    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->G:Ljava/lang/String;

    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->H:Ljava/lang/String;

    return-void
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->P:Ljava/lang/String;

    return-void
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/anythink/core/common/d/u;->Q:Ljava/lang/String;

    return-void
.end method

.method public final x()Lcom/anythink/core/common/d/v;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->R:Lcom/anythink/core/common/d/v;

    return-object v0
.end method

.method public final y()Lcom/anythink/core/common/d/w;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/anythink/core/common/d/u;->S:Lcom/anythink/core/common/d/w;

    return-object v0
.end method

.method public final z()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/anythink/core/common/d/u;->I:I

    return v0
.end method
