.class public final Lcom/anythink/core/common/d/b;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:J

.field private c:Lcom/anythink/core/api/ATBaseAdAdapter;

.field private d:Lcom/anythink/core/api/BaseAd;

.field private e:I

.field private f:Z

.field private g:J

.field private h:Ljava/lang/String;

.field private i:I

.field private j:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private j()I
    .locals 1

    .line 82
    iget v0, p0, Lcom/anythink/core/common/d/b;->a:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .line 74
    iput p1, p0, Lcom/anythink/core/common/d/b;->e:I

    if-lez p1, :cond_0

    const/4 p1, 0x0

    .line 77
    iput p1, p0, Lcom/anythink/core/common/d/b;->i:I

    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 0

    .line 42
    iput-wide p1, p0, Lcom/anythink/core/common/d/b;->j:J

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/anythink/core/common/d/b;->c:Lcom/anythink/core/api/ATBaseAdAdapter;

    return-void
.end method

.method public final a(Lcom/anythink/core/api/BaseAd;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/anythink/core/common/d/b;->d:Lcom/anythink/core/api/BaseAd;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/anythink/core/common/d/b;->h:Ljava/lang/String;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 66
    iput-boolean p1, p0, Lcom/anythink/core/common/d/b;->f:Z

    return-void
.end method

.method public final a()Z
    .locals 6

    .line 35
    iget v0, p0, Lcom/anythink/core/common/d/b;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/anythink/core/common/d/b;->b:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/anythink/core/common/d/b;->j:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .line 86
    iput p1, p0, Lcom/anythink/core/common/d/b;->a:I

    return-void
.end method

.method public final b(J)V
    .locals 0

    .line 58
    iput-wide p1, p0, Lcom/anythink/core/common/d/b;->g:J

    return-void
.end method

.method public final c()J
    .locals 2

    .line 54
    iget-wide v0, p0, Lcom/anythink/core/common/d/b;->g:J

    return-wide v0
.end method

.method public final c(J)V
    .locals 1

    const/4 v0, 0x1

    .line 94
    iput v0, p0, Lcom/anythink/core/common/d/b;->i:I

    .line 95
    iput-wide p1, p0, Lcom/anythink/core/common/d/b;->b:J

    return-void
.end method

.method public final d()Z
    .locals 1

    .line 62
    iget-boolean v0, p0, Lcom/anythink/core/common/d/b;->f:Z

    return v0
.end method

.method public final e()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/anythink/core/common/d/b;->e:I

    return v0
.end method

.method public final f()J
    .locals 2

    .line 90
    iget-wide v0, p0, Lcom/anythink/core/common/d/b;->b:J

    return-wide v0
.end method

.method public final g()Lcom/anythink/core/api/ATBaseAdAdapter;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->c:Lcom/anythink/core/api/ATBaseAdAdapter;

    return-object v0
.end method

.method public final h()Lcom/anythink/core/api/BaseAd;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->d:Lcom/anythink/core/api/BaseAd;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->c:Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/d/b;->d:Lcom/anythink/core/api/BaseAd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->c:Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/anythink/core/common/d/b;->c:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->isAdReady()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
