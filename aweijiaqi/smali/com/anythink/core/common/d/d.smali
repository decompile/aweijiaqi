.class public Lcom/anythink/core/common/d/d;
.super Lcom/anythink/core/common/d/aa;


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field public static final e:I = 0x5

.field public static final f:I = 0x0

.field public static final g:I = 0x1

.field public static final h:I = 0x2

.field public static final i:I = 0x0

.field public static final j:I = 0x1

.field public static final k:I = 0x2


# instance fields
.field protected A:I

.field protected B:I

.field protected C:I

.field protected D:I

.field E:Z

.field F:I

.field G:Ljava/lang/String;

.field H:J

.field I:J

.field J:Ljava/lang/String;

.field private aA:Z

.field private aB:J

.field private aC:J

.field private aD:Ljava/lang/String;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:I

.field private aH:Ljava/lang/String;

.field private aI:Ljava/lang/String;

.field private aJ:Ljava/lang/String;

.field private aK:D

.field private aL:Ljava/lang/String;

.field private aM:D

.field private aN:Lcom/anythink/core/api/ATRewardInfo;

.field private aO:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private aP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private aQ:J

.field private aR:J

.field private aS:Ljava/lang/String;

.field private aT:J

.field private aU:Ljava/lang/String;

.field private aV:Ljava/lang/String;

.field private aq:I

.field private ar:Ljava/lang/String;

.field private as:Ljava/lang/String;

.field private at:I

.field private au:I

.field private av:Ljava/lang/String;

.field private aw:I

.field private ax:I

.field private ay:I

.field private az:I

.field protected l:I

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field t:I

.field protected u:D

.field v:Ljava/lang/String;

.field public w:I

.field x:I

.field y:I

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/anythink/core/common/d/aa;-><init>()V

    const-string v0, ""

    .line 56
    iput-object v0, p0, Lcom/anythink/core/common/d/d;->m:Ljava/lang/String;

    const/4 v0, 0x0

    .line 57
    iput v0, p0, Lcom/anythink/core/common/d/d;->n:I

    .line 58
    iput v0, p0, Lcom/anythink/core/common/d/d;->o:I

    .line 59
    iput v0, p0, Lcom/anythink/core/common/d/d;->p:I

    return-void
.end method

.method private A(I)V
    .locals 0

    .line 488
    iput p1, p0, Lcom/anythink/core/common/d/d;->q:I

    return-void
.end method

.method private N()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->v:Ljava/lang/String;

    return-object v0
.end method

.method private O()J
    .locals 2

    .line 159
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aQ:J

    return-wide v0
.end method

.method private P()J
    .locals 2

    .line 167
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aR:J

    return-wide v0
.end method

.method private Q()Ljava/lang/String;
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    return-object v0
.end method

.method private R()I
    .locals 1

    .line 289
    iget v0, p0, Lcom/anythink/core/common/d/d;->w:I

    return v0
.end method

.method private S()V
    .locals 1

    const/4 v0, 0x1

    .line 293
    iput v0, p0, Lcom/anythink/core/common/d/d;->w:I

    return-void
.end method

.method private T()Ljava/lang/String;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->r:Ljava/lang/String;

    return-object v0
.end method

.method private U()Ljava/lang/String;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->ar:Ljava/lang/String;

    return-object v0
.end method

.method private V()Ljava/lang/String;
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->m:Ljava/lang/String;

    return-object v0
.end method

.method private W()I
    .locals 1

    .line 449
    iget v0, p0, Lcom/anythink/core/common/d/d;->n:I

    return v0
.end method

.method private X()I
    .locals 1

    .line 458
    iget v0, p0, Lcom/anythink/core/common/d/d;->o:I

    return v0
.end method

.method private Y()I
    .locals 1

    .line 484
    iget v0, p0, Lcom/anythink/core/common/d/d;->q:I

    return v0
.end method

.method private Z()Z
    .locals 1

    .line 501
    iget-boolean v0, p0, Lcom/anythink/core/common/d/d;->E:Z

    return v0
.end method

.method private aa()I
    .locals 1

    .line 509
    iget v0, p0, Lcom/anythink/core/common/d/d;->F:I

    return v0
.end method

.method private ab()Ljava/lang/String;
    .locals 1

    .line 517
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->G:Ljava/lang/String;

    return-object v0
.end method

.method private ac()J
    .locals 2

    .line 531
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->H:J

    return-wide v0
.end method

.method private ad()J
    .locals 2

    .line 539
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->I:J

    return-wide v0
.end method

.method private ae()Ljava/lang/String;
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    return-object v0
.end method

.method private w(Ljava/lang/String;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    return-void
.end method

.method private x(I)V
    .locals 0

    .line 445
    iput p1, p0, Lcom/anythink/core/common/d/d;->n:I

    return-void
.end method

.method private x(Ljava/lang/String;)V
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->r:Ljava/lang/String;

    return-void
.end method

.method private y(I)V
    .locals 0

    .line 454
    iput p1, p0, Lcom/anythink/core/common/d/d;->o:I

    return-void
.end method

.method private y(Ljava/lang/String;)V
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    return-void
.end method

.method private z(I)V
    .locals 0

    .line 463
    iput p1, p0, Lcom/anythink/core/common/d/d;->p:I

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->as:Ljava/lang/String;

    return-object v0
.end method

.method public B()I
    .locals 1

    .line 403
    iget v0, p0, Lcom/anythink/core/common/d/d;->l:I

    return v0
.end method

.method public final C()I
    .locals 1

    .line 421
    iget v0, p0, Lcom/anythink/core/common/d/d;->aq:I

    return v0
.end method

.method public final D()J
    .locals 2

    .line 467
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aB:J

    return-wide v0
.end method

.method public final E()J
    .locals 2

    .line 479
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aC:J

    return-wide v0
.end method

.method public final a()D
    .locals 2

    .line 118
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aM:D

    return-wide v0
.end method

.method public final a(D)V
    .locals 0

    .line 122
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aM:D

    return-void
.end method

.method public final a(I)V
    .locals 0

    .line 175
    iput p1, p0, Lcom/anythink/core/common/d/d;->A:I

    return-void
.end method

.method public final a(J)V
    .locals 0

    .line 163
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aQ:J

    return-void
.end method

.method public final a(Lcom/anythink/core/api/ATRewardInfo;)V
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aN:Lcom/anythink/core/api/ATRewardInfo;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aV:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;)V"
        }
    .end annotation

    .line 245
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aO:Ljava/util/Map;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 333
    iput-boolean p1, p0, Lcom/anythink/core/common/d/d;->aA:Z

    return-void
.end method

.method public final b()D
    .locals 2

    .line 126
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->aK:D

    return-wide v0
.end method

.method public final b(D)V
    .locals 0

    .line 130
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aK:D

    return-void
.end method

.method public final b(I)V
    .locals 0

    .line 179
    iput p1, p0, Lcom/anythink/core/common/d/d;->B:I

    return-void
.end method

.method public final b(J)V
    .locals 0

    .line 171
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aR:J

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aL:Ljava/lang/String;

    return-void
.end method

.method public final b(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 261
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aP:Ljava/util/Map;

    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 505
    iput-boolean p1, p0, Lcom/anythink/core/common/d/d;->E:Z

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aL:Ljava/lang/String;

    return-object v0
.end method

.method public final c(D)V
    .locals 0

    .line 309
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->u:D

    return-void
.end method

.method public final c(I)V
    .locals 0

    .line 183
    iput p1, p0, Lcom/anythink/core/common/d/d;->C:I

    return-void
.end method

.method public final c(J)V
    .locals 0

    .line 471
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aB:J

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aS:Ljava/lang/String;

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aS:Ljava/lang/String;

    return-object v0
.end method

.method public final d(I)V
    .locals 0

    .line 187
    iput p1, p0, Lcom/anythink/core/common/d/d;->D:I

    return-void
.end method

.method public final d(J)V
    .locals 0

    .line 475
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->aC:J

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->v:Ljava/lang/String;

    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aE:Ljava/lang/String;

    return-object v0
.end method

.method public final e(I)V
    .locals 0

    .line 213
    iput p1, p0, Lcom/anythink/core/common/d/d;->aG:I

    return-void
.end method

.method public final e(J)V
    .locals 0

    .line 535
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->H:J

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aE:Ljava/lang/String;

    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public final f(I)V
    .locals 0

    .line 277
    iput p1, p0, Lcom/anythink/core/common/d/d;->x:I

    return-void
.end method

.method public final f(J)V
    .locals 0

    .line 543
    iput-wide p1, p0, Lcom/anythink/core/common/d/d;->I:J

    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aF:Ljava/lang/String;

    return-void
.end method

.method public final g()I
    .locals 1

    .line 209
    iget v0, p0, Lcom/anythink/core/common/d/d;->aG:I

    return v0
.end method

.method public final g(I)V
    .locals 0

    .line 285
    iput p1, p0, Lcom/anythink/core/common/d/d;->y:I

    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aH:Ljava/lang/String;

    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aH:Ljava/lang/String;

    return-object v0
.end method

.method public final h(I)V
    .locals 0

    .line 301
    iput p1, p0, Lcom/anythink/core/common/d/d;->t:I

    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aI:Ljava/lang/String;

    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public final i(I)V
    .locals 0

    .line 341
    iput p1, p0, Lcom/anythink/core/common/d/d;->ay:I

    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aJ:Ljava/lang/String;

    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aJ:Ljava/lang/String;

    return-object v0
.end method

.method public final j(I)V
    .locals 0

    .line 349
    iput p1, p0, Lcom/anythink/core/common/d/d;->az:I

    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    return-void
.end method

.method public final k()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;"
        }
    .end annotation

    .line 241
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aO:Ljava/util/Map;

    return-object v0
.end method

.method public final k(I)V
    .locals 0

    .line 357
    iput p1, p0, Lcom/anythink/core/common/d/d;->at:I

    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->av:Ljava/lang/String;

    return-void
.end method

.method public final l()Lcom/anythink/core/api/ATRewardInfo;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aN:Lcom/anythink/core/api/ATRewardInfo;

    return-object v0
.end method

.method public final l(I)V
    .locals 0

    .line 365
    iput p1, p0, Lcom/anythink/core/common/d/d;->au:I

    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->as:Ljava/lang/String;

    return-void
.end method

.method public final m()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 257
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aP:Ljava/util/Map;

    return-object v0
.end method

.method public final m(I)V
    .locals 0

    .line 381
    iput p1, p0, Lcom/anythink/core/common/d/d;->aw:I

    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->ar:Ljava/lang/String;

    return-void
.end method

.method public final n()I
    .locals 1

    .line 273
    iget v0, p0, Lcom/anythink/core/common/d/d;->x:I

    return v0
.end method

.method public final n(I)V
    .locals 0

    .line 389
    iput p1, p0, Lcom/anythink/core/common/d/d;->ax:I

    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 1

    .line 435
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final o()I
    .locals 1

    .line 281
    iget v0, p0, Lcom/anythink/core/common/d/d;->y:I

    return v0
.end method

.method public final o(I)V
    .locals 0

    .line 408
    iput p1, p0, Lcom/anythink/core/common/d/d;->l:I

    return-void
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->aU:Ljava/lang/String;

    return-void
.end method

.method public final p()I
    .locals 1

    .line 297
    iget v0, p0, Lcom/anythink/core/common/d/d;->t:I

    return v0
.end method

.method public final p(I)V
    .locals 0

    .line 425
    iput p1, p0, Lcom/anythink/core/common/d/d;->aq:I

    return-void
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0

    .line 521
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->G:Ljava/lang/String;

    return-void
.end method

.method public final q()D
    .locals 2

    .line 305
    iget-wide v0, p0, Lcom/anythink/core/common/d/d;->u:D

    return-wide v0
.end method

.method public final q(I)V
    .locals 0

    .line 513
    iput p1, p0, Lcom/anythink/core/common/d/d;->F:I

    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    return-void
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    return-object v0
.end method

.method public r(I)Lorg/json/JSONObject;
    .locals 19

    move-object/from16 v1, p0

    .line 559
    invoke-super/range {p0 .. p1}, Lcom/anythink/core/common/d/aa;->r(I)Lorg/json/JSONObject;

    move-result-object v2

    :try_start_0
    const-string v0, "nw_ver"

    .line 561
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "refresh"

    .line 562
    iget v3, v1, Lcom/anythink/core/common/d/d;->ax:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 565
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aV:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tp_bid_id"

    .line 566
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->aV:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const-string v0, "myoffer_showtype"

    const-string v3, "reason"

    const-string v4, "bidresponselist"

    const-string v5, "scenario"

    const-string v6, "phs"

    const-string v7, "[]"

    const-string v8, "pds"

    const-string v9, "ahs"

    const-string v10, "ads"

    const-string v11, "extra"

    const-string v13, "aprn_auto_req"

    const-string v14, "bidprice"

    const-string v15, "bidtype"

    const-string v12, "auto_req"

    move-object/from16 v16, v7

    const-string v7, "nw_firm_id"

    move-object/from16 v17, v13

    const-string v13, "unit_id"

    move-object/from16 v18, v3

    const-string v3, "gro_id"

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_a

    .line 739
    :pswitch_1
    :try_start_1
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 740
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object/from16 v7, v16

    goto :goto_0

    :cond_1
    new-instance v7, Lorg/json/JSONArray;

    iget-object v0, v1, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    .line 688
    :pswitch_2
    iget-object v4, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 689
    iget v4, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 690
    iget v4, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 691
    iget v3, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 692
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "as_result"

    .line 694
    iget-object v4, v1, Lcom/anythink/core/common/d/d;->G:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v7, v16

    goto :goto_1

    :cond_2
    new-instance v7, Lorg/json/JSONArray;

    iget-object v4, v1, Lcom/anythink/core/common/d/d;->G:Ljava/lang/String;

    invoke-direct {v7, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "new_req_id"

    .line 695
    iget-object v4, v1, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 696
    iget v3, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 698
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v4, "req_id_match"

    if-nez v3, :cond_3

    :try_start_2
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->af:Ljava/lang/String;

    if-nez v3, :cond_3

    const/4 v3, 0x0

    .line 699
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 702
    :cond_3
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/anythink/core/common/d/d;->af:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 703
    iget-object v3, v1, Lcom/anythink/core/common/d/d;->s:Ljava/lang/String;

    iget-object v7, v1, Lcom/anythink/core/common/d/d;->af:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v7, 0x0

    .line 704
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_2

    :cond_4
    const/4 v3, 0x1

    .line 706
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_2

    :cond_5
    const/4 v12, 0x1

    .line 709
    invoke-virtual {v2, v4, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 713
    :goto_2
    iget v3, v1, Lcom/anythink/core/common/d/d;->w:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 715
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 716
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 720
    :cond_6
    iget v0, v1, Lcom/anythink/core/common/d/d;->A:I

    invoke-virtual {v2, v10, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 721
    iget v0, v1, Lcom/anythink/core/common/d/d;->B:I

    invoke-virtual {v2, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 722
    iget v0, v1, Lcom/anythink/core/common/d/d;->C:I

    invoke-virtual {v2, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 723
    iget v0, v1, Lcom/anythink/core/common/d/d;->D:I

    invoke-virtual {v2, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 726
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    const/16 v3, 0x23

    if-ne v3, v0, :cond_f

    .line 727
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aU:Ljava/lang/String;

    invoke-virtual {v2, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_3
    const-string v0, "loadtime"

    .line 680
    iget-wide v4, v1, Lcom/anythink/core/common/d/d;->aC:J

    invoke-virtual {v2, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 681
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 682
    iget v0, v1, Lcom/anythink/core/common/d/d;->F:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_f

    .line 683
    iget v0, v1, Lcom/anythink/core/common/d/d;->F:I

    move-object/from16 v4, v18

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto/16 :goto_a

    .line 732
    :pswitch_4
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "bidrequesttime"

    .line 733
    iget-wide v5, v1, Lcom/anythink/core/common/d/d;->H:J

    invoke-virtual {v2, v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "bidresponsetime"

    .line 734
    iget-wide v5, v1, Lcom/anythink/core/common/d/d;->I:J

    invoke-virtual {v2, v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 735
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object/from16 v7, v16

    goto :goto_3

    :cond_7
    new-instance v7, Lorg/json/JSONArray;

    iget-object v0, v1, Lcom/anythink/core/common/d/d;->J:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_5
    move-object/from16 v4, v18

    const/4 v7, 0x0

    const/4 v12, 0x1

    const-string v0, "isload"

    .line 675
    iget-boolean v5, v1, Lcom/anythink/core/common/d/d;->E:Z

    if-eqz v5, :cond_8

    const/4 v7, 0x1

    :cond_8
    invoke-virtual {v2, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 676
    iget v0, v1, Lcom/anythink/core/common/d/d;->F:I

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 677
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_6
    const/4 v4, 0x1

    const/16 v16, 0x0

    .line 660
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 661
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 662
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 663
    iget v0, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 5329
    iget-boolean v0, v1, Lcom/anythink/core/common/d/d;->aA:Z

    if-eqz v0, :cond_9

    move-object/from16 v0, v17

    const/4 v3, 0x1

    goto :goto_4

    :cond_9
    move-object/from16 v0, v17

    const/4 v3, 0x0

    .line 664
    :goto_4
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 665
    iget v0, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 666
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v14, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 669
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 670
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_7
    move-object/from16 v0, v17

    const/4 v4, 0x1

    const/16 v16, 0x0

    .line 647
    iget-object v5, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 648
    iget v5, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 649
    iget v5, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 650
    iget v3, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 4329
    iget-boolean v3, v1, Lcom/anythink/core/common/d/d;->aA:Z

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    goto :goto_5

    :cond_a
    const/4 v3, 0x0

    .line 651
    :goto_5
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "progress"

    .line 652
    iget v3, v1, Lcom/anythink/core/common/d/d;->q:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 653
    iget v0, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 654
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v14, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_8
    move-object/from16 v4, v17

    const/16 v16, 0x0

    move-object/from16 v17, v11

    goto :goto_6

    :pswitch_9
    move-object/from16 v4, v17

    const/16 v16, 0x0

    move-object/from16 v17, v11

    .line 609
    iget v11, v1, Lcom/anythink/core/common/d/d;->A:I

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 610
    iget v11, v1, Lcom/anythink/core/common/d/d;->B:I

    invoke-virtual {v2, v9, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 611
    iget v11, v1, Lcom/anythink/core/common/d/d;->C:I

    invoke-virtual {v2, v8, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 612
    iget v11, v1, Lcom/anythink/core/common/d/d;->D:I

    invoke-virtual {v2, v6, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 615
    :goto_6
    iget-object v11, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 616
    iget v11, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 617
    iget v7, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 618
    iget v3, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 3329
    iget-boolean v3, v1, Lcom/anythink/core/common/d/d;->aA:Z

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    goto :goto_7

    :cond_b
    const/4 v3, 0x0

    .line 619
    :goto_7
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 621
    iget v3, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 622
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v14, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 626
    iget v3, v1, Lcom/anythink/core/common/d/d;->w:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 628
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 629
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 633
    :cond_c
    iget v0, v1, Lcom/anythink/core/common/d/d;->A:I

    invoke-virtual {v2, v10, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 634
    iget v0, v1, Lcom/anythink/core/common/d/d;->B:I

    invoke-virtual {v2, v9, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 635
    iget v0, v1, Lcom/anythink/core/common/d/d;->C:I

    invoke-virtual {v2, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 636
    iget v0, v1, Lcom/anythink/core/common/d/d;->D:I

    invoke-virtual {v2, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 639
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    const/16 v3, 0x23

    if-ne v3, v0, :cond_f

    .line 640
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aU:Ljava/lang/String;

    move-object/from16 v5, v17

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_a

    :pswitch_a
    move-object v5, v11

    move-object/from16 v4, v17

    const/16 v16, 0x0

    .line 588
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 589
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 590
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 591
    iget v0, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2329
    iget-boolean v0, v1, Lcom/anythink/core/common/d/d;->aA:Z

    if-eqz v0, :cond_d

    const/4 v3, 0x1

    goto :goto_8

    :cond_d
    const/4 v3, 0x0

    .line 592
    :goto_8
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "status"

    .line 593
    iget v3, v1, Lcom/anythink/core/common/d/d;->o:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "filledtime"

    .line 594
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->aC:J

    invoke-virtual {v2, v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "flag"

    .line 595
    iget v3, v1, Lcom/anythink/core/common/d/d;->p:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 596
    iget v0, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 597
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v14, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 601
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    const/16 v3, 0x23

    if-ne v3, v0, :cond_f

    .line 602
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aU:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_a

    :pswitch_b
    move-object v5, v11

    move-object/from16 v4, v17

    const/16 v16, 0x0

    .line 571
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aD:Ljava/lang/String;

    invoke-virtual {v2, v13, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 572
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 573
    iget v0, v1, Lcom/anythink/core/common/d/d;->aq:I

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 574
    iget v0, v1, Lcom/anythink/core/common/d/d;->n:I

    invoke-virtual {v2, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1329
    iget-boolean v0, v1, Lcom/anythink/core/common/d/d;->aA:Z

    if-eqz v0, :cond_e

    const/4 v3, 0x1

    goto :goto_9

    :cond_e
    const/4 v3, 0x0

    .line 575
    :goto_9
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 576
    iget v0, v1, Lcom/anythink/core/common/d/d;->t:I

    invoke-virtual {v2, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 577
    iget-wide v3, v1, Lcom/anythink/core/common/d/d;->u:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v14, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 581
    iget v0, v1, Lcom/anythink/core/common/d/d;->l:I

    const/16 v3, 0x23

    if-ne v3, v0, :cond_f

    .line 582
    iget-object v0, v1, Lcom/anythink/core/common/d/d;->aU:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_a

    :catch_0
    move-exception v0

    .line 744
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_f
    :goto_a
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final s(I)Lorg/json/JSONObject;
    .locals 0

    .line 751
    invoke-super {p0, p1}, Lcom/anythink/core/common/d/aa;->r(I)Lorg/json/JSONObject;

    move-result-object p1

    return-object p1
.end method

.method public final s()Z
    .locals 1

    .line 329
    iget-boolean v0, p0, Lcom/anythink/core/common/d/d;->aA:Z

    return v0
.end method

.method public final t()I
    .locals 1

    .line 337
    iget v0, p0, Lcom/anythink/core/common/d/d;->ay:I

    return v0
.end method

.method public final u()I
    .locals 1

    .line 345
    iget v0, p0, Lcom/anythink/core/common/d/d;->az:I

    return v0
.end method

.method public final v()I
    .locals 1

    .line 353
    iget v0, p0, Lcom/anythink/core/common/d/d;->at:I

    return v0
.end method

.method public final w()I
    .locals 1

    .line 361
    iget v0, p0, Lcom/anythink/core/common/d/d;->au:I

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/anythink/core/common/d/d;->av:Ljava/lang/String;

    return-object v0
.end method

.method public final y()I
    .locals 1

    .line 377
    iget v0, p0, Lcom/anythink/core/common/d/d;->aw:I

    return v0
.end method

.method public final z()I
    .locals 1

    .line 385
    iget v0, p0, Lcom/anythink/core/common/d/d;->ax:I

    return v0
.end method
