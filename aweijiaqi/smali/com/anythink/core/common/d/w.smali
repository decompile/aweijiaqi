.class public final Lcom/anythink/core/common/d/w;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field A:[Ljava/lang/String;

.field B:[Ljava/lang/String;

.field C:[Ljava/lang/String;

.field D:[Ljava/lang/String;

.field E:[Ljava/lang/String;

.field F:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field G:[Ljava/lang/String;

.field H:[Ljava/lang/String;

.field I:Ljava/lang/String;

.field J:Ljava/lang/String;

.field K:Ljava/lang/String;

.field L:Ljava/lang/String;

.field M:Ljava/lang/String;

.field N:Ljava/lang/String;

.field O:Ljava/lang/String;

.field P:Ljava/lang/String;

.field Q:Ljava/lang/String;

.field R:Ljava/lang/String;

.field S:Ljava/lang/String;

.field T:Ljava/lang/String;

.field U:Ljava/lang/String;

.field V:Ljava/lang/String;

.field W:Ljava/lang/String;

.field X:Ljava/lang/String;

.field Y:Ljava/lang/String;

.field Z:Ljava/lang/String;

.field a:Ljava/lang/String;

.field aa:Ljava/lang/String;

.field ab:Ljava/lang/String;

.field ac:Ljava/lang/String;

.field ad:Ljava/lang/String;

.field ae:Ljava/lang/String;

.field af:Ljava/lang/String;

.field ag:Ljava/lang/String;

.field ah:Ljava/lang/String;

.field ai:Ljava/lang/String;

.field aj:Ljava/lang/String;

.field ak:Ljava/lang/String;

.field al:Ljava/lang/String;

.field am:Ljava/lang/String;

.field an:Ljava/lang/String;

.field b:[Ljava/lang/String;

.field c:[Ljava/lang/String;

.field d:[Ljava/lang/String;

.field e:[Ljava/lang/String;

.field f:[Ljava/lang/String;

.field g:[Ljava/lang/String;

.field h:[Ljava/lang/String;

.field i:[Ljava/lang/String;

.field j:[Ljava/lang/String;

.field k:[Ljava/lang/String;

.field l:[Ljava/lang/String;

.field m:[Ljava/lang/String;

.field n:[Ljava/lang/String;

.field o:[Ljava/lang/String;

.field p:[Ljava/lang/String;

.field q:[Ljava/lang/String;

.field r:[Ljava/lang/String;

.field s:[Ljava/lang/String;

.field t:[Ljava/lang/String;

.field u:[Ljava/lang/String;

.field v:[Ljava/lang/String;

.field w:[Ljava/lang/String;

.field x:[Ljava/lang/String;

.field y:[Ljava/lang/String;

.field z:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lcom/anythink/core/common/d/w;
    .locals 6

    .line 117
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 118
    new-instance p0, Lcom/anythink/core/common/d/w;

    invoke-direct {p0}, Lcom/anythink/core/common/d/w;-><init>()V

    const-string v1, "ks"

    .line 119
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->a:Ljava/lang/String;

    const-string v1, "nurl"

    .line 120
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->b:[Ljava/lang/String;

    const-string v1, "imp"

    .line 121
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->c:[Ljava/lang/String;

    const-string v1, "click"

    .line 122
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->d:[Ljava/lang/String;

    const-string v1, "vstart"

    .line 123
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->e:[Ljava/lang/String;

    const-string v1, "v25"

    .line 124
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->f:[Ljava/lang/String;

    const-string v1, "v50"

    .line 125
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->g:[Ljava/lang/String;

    const-string v1, "v75"

    .line 126
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->h:[Ljava/lang/String;

    const-string v1, "v100"

    .line 127
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->i:[Ljava/lang/String;

    const-string v1, "vpaused"

    .line 128
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->j:[Ljava/lang/String;

    const-string v1, "vclick"

    .line 129
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->k:[Ljava/lang/String;

    const-string v1, "vmute"

    .line 130
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->l:[Ljava/lang/String;

    const-string v1, "vunmute"

    .line 131
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->m:[Ljava/lang/String;

    const-string v1, "ec_show"

    .line 132
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->n:[Ljava/lang/String;

    const-string v1, "ec_close"

    .line 133
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->o:[Ljava/lang/String;

    const-string v1, "apk_dl_star"

    .line 134
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->p:[Ljava/lang/String;

    const-string v1, "apk_dl_end"

    .line 135
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->q:[Ljava/lang/String;

    const-string v1, "apk_install"

    .line 136
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->r:[Ljava/lang/String;

    const-string v1, "vresumed"

    .line 141
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->s:[Ljava/lang/String;

    const-string v1, "vskip"

    .line 142
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->t:[Ljava/lang/String;

    const-string v1, "vfail"

    .line 143
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->u:[Ljava/lang/String;

    const-string v1, "apk_start_install"

    .line 144
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->v:[Ljava/lang/String;

    const-string v1, "dp_start"

    .line 145
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->w:[Ljava/lang/String;

    const-string v1, "dp_succ"

    .line 146
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->x:[Ljava/lang/String;

    const-string v1, "app_install"

    .line 147
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->y:[Ljava/lang/String;

    const-string v1, "app_uninstall"

    .line 148
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->z:[Ljava/lang/String;

    const-string v1, "app_unknow"

    .line 149
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->A:[Ljava/lang/String;

    const-string v1, "dp_inst_fail"

    .line 154
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->C:[Ljava/lang/String;

    const-string v1, "dp_uninst_fail"

    .line 155
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->B:[Ljava/lang/String;

    const-string v1, "vd_succ"

    .line 156
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->D:[Ljava/lang/String;

    const-string v1, "vrewarded"

    .line 157
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->E:[Ljava/lang/String;

    const-string v1, "v_p_tracking"

    .line 158
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 160
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/anythink/core/common/d/w;->F:Ljava/util/Map;

    const/4 v2, 0x0

    .line 161
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 162
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "play_sec"

    .line 163
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "list"

    .line 164
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 165
    invoke-static {v3}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v3

    .line 166
    iget-object v5, p0, Lcom/anythink/core/common/d/w;->F:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v1, "load_success"

    .line 173
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->G:[Ljava/lang/String;

    const-string v1, "load_fail"

    .line 174
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->H:[Ljava/lang/String;

    const-string v1, "tp_nurl"

    .line 177
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->I:Ljava/lang/String;

    const-string v1, "tp_imp"

    .line 178
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->J:Ljava/lang/String;

    const-string v1, "tp_click"

    .line 179
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->K:Ljava/lang/String;

    const-string v1, "tp_vstart"

    .line 180
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->L:Ljava/lang/String;

    const-string v1, "tp_v25"

    .line 181
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->M:Ljava/lang/String;

    const-string v1, "tp_v50"

    .line 182
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->N:Ljava/lang/String;

    const-string v1, "tp_v75"

    .line 183
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->O:Ljava/lang/String;

    const-string v1, "tp_v100"

    .line 184
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->P:Ljava/lang/String;

    const-string v1, "tp_vpaused"

    .line 185
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->Q:Ljava/lang/String;

    const-string v1, "tp_vclick"

    .line 186
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->R:Ljava/lang/String;

    const-string v1, "tp_vmute"

    .line 187
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->S:Ljava/lang/String;

    const-string v1, "tp_vunmute"

    .line 188
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->T:Ljava/lang/String;

    const-string v1, "tp_ec_show"

    .line 189
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->U:Ljava/lang/String;

    const-string v1, "tp_ec_close"

    .line 190
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->V:Ljava/lang/String;

    const-string v1, "tp_apk_dl_star"

    .line 191
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->W:Ljava/lang/String;

    const-string v1, "tp_apk_dl_end"

    .line 192
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->X:Ljava/lang/String;

    const-string v1, "tp_apk_install"

    .line 193
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->Y:Ljava/lang/String;

    const-string v1, "tp_vresumed"

    .line 198
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->Z:Ljava/lang/String;

    const-string v1, "tp_vskip"

    .line 199
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->aa:Ljava/lang/String;

    const-string v1, "tp_vfail"

    .line 200
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ab:Ljava/lang/String;

    const-string v1, "tp_apk_start_install"

    .line 201
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ac:Ljava/lang/String;

    const-string v1, "tp_dp_start"

    .line 202
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ad:Ljava/lang/String;

    const-string v1, "tp_dp_succ"

    .line 203
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ae:Ljava/lang/String;

    const-string v1, "tp_app_install"

    .line 204
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->af:Ljava/lang/String;

    const-string v1, "tp_app_uninstall"

    .line 205
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ag:Ljava/lang/String;

    const-string v1, "tp_app_unknow"

    .line 206
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ah:Ljava/lang/String;

    const-string v1, "tp_dp_inst_fail"

    .line 211
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->aj:Ljava/lang/String;

    const-string v1, "tp_dp_uninst_fail"

    .line 212
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ai:Ljava/lang/String;

    const-string v1, "tp_vd_succ"

    .line 213
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->ak:Ljava/lang/String;

    const-string v1, "tp_vrewarded"

    .line 214
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->al:Ljava/lang/String;

    const-string v1, "tp_load_success"

    .line 219
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anythink/core/common/d/w;->am:Ljava/lang/String;

    const-string v1, "tp_load_fail"

    .line 220
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/d/w;->an:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p0

    :catchall_0
    move-exception p0

    .line 226
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Ljava/lang/String;
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->R:Ljava/lang/String;

    return-object v0
.end method

.method public final J()Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->S:Ljava/lang/String;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->X:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 1

    .line 397
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()[Ljava/lang/String;
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->s:[Ljava/lang/String;

    return-object v0
.end method

.method public final R()[Ljava/lang/String;
    .locals 1

    .line 405
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->t:[Ljava/lang/String;

    return-object v0
.end method

.method public final S()[Ljava/lang/String;
    .locals 1

    .line 409
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->u:[Ljava/lang/String;

    return-object v0
.end method

.method public final T()[Ljava/lang/String;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->v:[Ljava/lang/String;

    return-object v0
.end method

.method public final U()[Ljava/lang/String;
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->w:[Ljava/lang/String;

    return-object v0
.end method

.method public final V()[Ljava/lang/String;
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->x:[Ljava/lang/String;

    return-object v0
.end method

.method public final W()[Ljava/lang/String;
    .locals 1

    .line 426
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->y:[Ljava/lang/String;

    return-object v0
.end method

.method public final X()[Ljava/lang/String;
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->z:[Ljava/lang/String;

    return-object v0
.end method

.method public final Y()[Ljava/lang/String;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->A:[Ljava/lang/String;

    return-object v0
.end method

.method public final Z()Ljava/lang/String;
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->G:[Ljava/lang/String;

    return-object v0
.end method

.method public final aa()Ljava/lang/String;
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 1

    .line 453
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ac:Ljava/lang/String;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ad:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()Ljava/lang/String;
    .locals 1

    .line 461
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public final af()Ljava/lang/String;
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->af:Ljava/lang/String;

    return-object v0
.end method

.method public final ag()Ljava/lang/String;
    .locals 1

    .line 469
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ag:Ljava/lang/String;

    return-object v0
.end method

.method public final ah()Ljava/lang/String;
    .locals 1

    .line 473
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public final ai()Ljava/lang/String;
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public final aj()Ljava/lang/String;
    .locals 1

    .line 481
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public final ak()Ljava/lang/String;
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->ak:Ljava/lang/String;

    return-object v0
.end method

.method public final al()Ljava/lang/String;
    .locals 1

    .line 489
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->al:Ljava/lang/String;

    return-object v0
.end method

.method public final am()Ljava/lang/String;
    .locals 1

    .line 493
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->am:Ljava/lang/String;

    return-object v0
.end method

.method public final an()Ljava/lang/String;
    .locals 1

    .line 497
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->an:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Ljava/lang/String;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->H:[Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()[Ljava/lang/String;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final e()[Ljava/lang/String;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public final g()[Ljava/lang/String;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public final h()[Ljava/lang/String;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final i()[Ljava/lang/String;
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->g:[Ljava/lang/String;

    return-object v0
.end method

.method public final j()[Ljava/lang/String;
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public final k()[Ljava/lang/String;
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public final l()[Ljava/lang/String;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public final m()[Ljava/lang/String;
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public final n()[Ljava/lang/String;
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public final o()[Ljava/lang/String;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->m:[Ljava/lang/String;

    return-object v0
.end method

.method public final p()[Ljava/lang/String;
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->n:[Ljava/lang/String;

    return-object v0
.end method

.method public final q()[Ljava/lang/String;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->o:[Ljava/lang/String;

    return-object v0
.end method

.method public final r()[Ljava/lang/String;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->p:[Ljava/lang/String;

    return-object v0
.end method

.method public final s()[Ljava/lang/String;
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->q:[Ljava/lang/String;

    return-object v0
.end method

.method public final t()[Ljava/lang/String;
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->r:[Ljava/lang/String;

    return-object v0
.end method

.method public final u()[Ljava/lang/String;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->B:[Ljava/lang/String;

    return-object v0
.end method

.method public final v()[Ljava/lang/String;
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->C:[Ljava/lang/String;

    return-object v0
.end method

.method public final w()[Ljava/lang/String;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->D:[Ljava/lang/String;

    return-object v0
.end method

.method public final x()[Ljava/lang/String;
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->E:[Ljava/lang/String;

    return-object v0
.end method

.method public final y()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 329
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->F:Ljava/util/Map;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/anythink/core/common/d/w;->I:Ljava/lang/String;

    return-object v0
.end method
