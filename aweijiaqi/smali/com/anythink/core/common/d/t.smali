.class public final Lcom/anythink/core/common/d/t;
.super Lcom/anythink/core/common/d/u;


# static fields
.field public static final A:I = 0x1

.field public static final B:I = 0x2


# instance fields
.field C:I

.field D:J

.field E:J

.field z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/anythink/core/common/d/u;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/anythink/core/common/d/t;->z:I

    return v0
.end method

.method public final a(J)V
    .locals 0

    .line 34
    iput-wide p1, p0, Lcom/anythink/core/common/d/t;->D:J

    return-void
.end method

.method public final b()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final b(J)V
    .locals 0

    .line 38
    iput-wide p1, p0, Lcom/anythink/core/common/d/t;->E:J

    return-void
.end method

.method public final e(I)V
    .locals 0

    .line 46
    iput p1, p0, Lcom/anythink/core/common/d/t;->z:I

    return-void
.end method

.method public final f(I)V
    .locals 0

    .line 54
    iput p1, p0, Lcom/anythink/core/common/d/t;->C:I

    return-void
.end method

.method public final v()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/anythink/core/common/d/t;->C:I

    return v0
.end method

.method public final w()Z
    .locals 5

    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/anythink/core/common/d/t;->E:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/anythink/core/common/d/t;->D:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
