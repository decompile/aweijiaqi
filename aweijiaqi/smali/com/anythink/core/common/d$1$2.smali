.class final Lcom/anythink/core/common/d$1$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/c/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/common/d$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/common/d/d;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/anythink/core/common/d$1;


# direct methods
.method constructor <init>(Lcom/anythink/core/common/d$1;Lcom/anythink/core/common/d/d;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iput-object p2, p0, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    iput-object p3, p0, Lcom/anythink/core/common/d$1$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/anythink/core/common/d$1$2;->c:Landroid/content/Context;

    iput-object p5, p0, Lcom/anythink/core/common/d$1$2;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/anythink/core/api/AdError;)V
    .locals 7

    .line 354
    sget-object v0, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v1, v1, Lcom/anythink/core/common/d$1;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v2, v2, Lcom/anythink/core/common/d$1;->e:Ljava/lang/String;

    invoke-static {v2}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v3

    .line 1072
    invoke-static {}, Lcom/anythink/core/api/ATSDK;->isNetworkLogDebug()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1074
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "action"

    .line 1075
    sget-object v6, Lcom/anythink/core/common/b/e$e;->t:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "result"

    .line 1076
    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "placementId"

    .line 1077
    invoke-virtual {v4, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "adtype"

    .line 1078
    invoke-virtual {v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "errorMsg"

    .line 1079
    invoke-virtual {v4, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1080
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/anythink/core/common/b/e;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_network"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    :catchall_0
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getPlatformCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->getPlatformMSG()Ljava/lang/String;

    move-result-object p1

    const-string v1, "3001"

    invoke-static {v1, v0, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    .line 358
    iget-object v0, p0, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 360
    iget-object v0, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v0, v0, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    iget-object v1, p0, Lcom/anythink/core/common/d$1$2;->a:Lcom/anythink/core/common/d/d;

    iget-object v2, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v2, v2, Lcom/anythink/core/common/d$1;->b:Lcom/anythink/core/common/g;

    invoke-static {v0, v1, p1, v2}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/common/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method public final a(Lcom/anythink/core/c/d;)V
    .locals 2

    .line 315
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/d$1$2$1;

    invoke-direct {v1, p0, p1}, Lcom/anythink/core/common/d$1$2$1;-><init>(Lcom/anythink/core/common/d$1$2;Lcom/anythink/core/c/d;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Lcom/anythink/core/c/d;)V
    .locals 2

    .line 365
    iget-object v0, p0, Lcom/anythink/core/common/d$1$2;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/anythink/core/common/d$1$2;->e:Lcom/anythink/core/common/d$1;

    iget-object v0, v0, Lcom/anythink/core/common/d$1;->f:Lcom/anythink/core/common/d;

    const/4 v1, 0x0

    iput v1, v0, Lcom/anythink/core/common/d;->e:I

    .line 369
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/d$1$2$2;

    invoke-direct {v1, p0, p1}, Lcom/anythink/core/common/d$1$2$2;-><init>(Lcom/anythink/core/common/d$1$2;Lcom/anythink/core/c/d;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method
