.class public abstract Lcom/anythink/core/common/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/anythink/core/common/g;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field protected b:Landroid/content/Context;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/common/f;",
            ">;"
        }
    .end annotation
.end field

.field protected e:I

.field protected f:Z

.field protected g:Ljava/lang/String;

.field protected h:Lcom/anythink/core/common/e;

.field i:D

.field j:Ljava/lang/String;

.field private k:J

.field private l:J

.field private m:Z

.field private n:J

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 65
    iput v0, p0, Lcom/anythink/core/common/d;->e:I

    const-string v1, ""

    .line 73
    iput-object v1, p0, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    .line 81
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->o:Z

    const-wide/16 v2, 0x0

    .line 1120
    iput-wide v2, p0, Lcom/anythink/core/common/d;->i:D

    .line 1121
    iput-object v1, p0, Lcom/anythink/core/common/d;->j:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    .line 88
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object p1, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 90
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    if-nez p1, :cond_0

    .line 91
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    iget-object p2, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/b/g;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;J)J
    .locals 0

    .line 57
    iput-wide p1, p0, Lcom/anythink/core/common/d;->l:J

    return-wide p1
.end method

.method private a(Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/c/d;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 1005
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1007
    new-instance v1, Lcom/anythink/core/common/d/d;

    invoke-direct {v1}, Lcom/anythink/core/common/d/d;-><init>()V

    .line 1008
    iget-object v2, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->t(Ljava/lang/String;)V

    .line 1009
    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->u(Ljava/lang/String;)V

    .line 1010
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->C()I

    move-result v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->v(Ljava/lang/String;)V

    .line 1011
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->s(Ljava/lang/String;)V

    .line 1012
    invoke-virtual {v1, p3}, Lcom/anythink/core/common/d/d;->n(I)V

    .line 1013
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->t()I

    move-result p2

    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->w(I)V

    .line 1014
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->J()I

    move-result p2

    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->p(I)V

    .line 1016
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 1017
    new-instance p3, Ljava/util/LinkedHashMap;

    invoke-direct {p3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1018
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 9091
    iget-wide v3, v2, Lcom/anythink/core/c/d$b;->m:D

    .line 1020
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-nez v5, :cond_0

    .line 1022
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1023
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1025
    :cond_0
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1028
    :cond_1
    invoke-virtual {p3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_1
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1029
    invoke-static {v2}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    goto :goto_1

    .line 1033
    :cond_2
    iget-object p4, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Request UnitGroup\'s Number:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->F()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p4, v2}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p4, 0x0

    .line 1035
    invoke-virtual {p3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1036
    invoke-virtual {p3, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 1037
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/core/c/d$b;

    .line 1039
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->F()I

    move-result v5

    div-int v5, p4, v5

    const/4 v6, 0x1

    add-int/2addr v5, v6

    .line 9159
    iput v5, v4, Lcom/anythink/core/c/d$b;->A:I

    .line 1040
    iget-object v5, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UnitGroupInfo requestLevel:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, " || layer:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10155
    iget v8, v4, Lcom/anythink/core/c/d$b;->A:I

    .line 1040
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/anythink/core/common/g/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v7, "sortpriority"

    .line 1045
    invoke-virtual {v5, v7, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v7, "sorttype"

    .line 1046
    iget v8, v4, Lcom/anythink/core/c/d$b;->q:I

    invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v7, "unit_id"

    .line 11059
    iget-object v8, v4, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    .line 1047
    invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "bidresult"

    .line 1048
    invoke-virtual {v5, v7, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v6, "bidprice"

    .line 1049
    invoke-virtual {v4}, Lcom/anythink/core/c/d$b;->e()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 11091
    iget-wide v7, v4, Lcom/anythink/core/c/d$b;->m:D

    .line 1049
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_4
    const-string v4, "0"

    :goto_3
    invoke-virtual {v5, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1050
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 p4, p4, 0x1

    goto :goto_2

    .line 1058
    :cond_5
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/anythink/core/common/d/d;->q(Ljava/lang/String;)V

    .line 1060
    iget-object p1, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p1

    const/16 p3, 0xf

    invoke-virtual {p1, p3, v1}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    return-object p2
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/g;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d;",
            "Lcom/anythink/core/common/d/d;",
            "TT;)V"
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v0, p2

    move-object/from16 v10, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    .line 483
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->L()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->M()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 484
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->N()Ljava/lang/String;

    move-result-object v1

    .line 485
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->g()Ljava/lang/String;

    move-result-object v2

    .line 486
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->O()Ljava/lang/String;

    move-result-object v4

    .line 487
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->e()Ljava/lang/String;

    move-result-object v5

    .line 484
    invoke-static {v1, v2, v4, v5}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 490
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v0, v10, v12, v3}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    .line 2694
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->T()Z

    move-result v1
    :try_end_0
    .catch Lcom/anythink/core/common/c; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v2, 0x5

    const-string v9, ""

    if-eqz v1, :cond_c

    if-eqz v3, :cond_0

    .line 2705
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v4, :cond_b

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_b

    .line 503
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/anythink/core/a/a;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/x;

    move-result-object v8

    .line 2717
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->G()J

    move-result-wide v1

    .line 2718
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->H()J

    move-result-wide v5

    if-eqz v8, :cond_2

    .line 2720
    iget v7, v8, Lcom/anythink/core/common/d/x;->d:I

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    :goto_0
    if-eqz v8, :cond_3

    .line 2721
    iget v15, v8, Lcom/anythink/core/common/d/x;->c:I
    :try_end_1
    .catch Lcom/anythink/core/common/c; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_1

    :cond_3
    const/4 v15, 0x0

    :goto_1
    const-wide/16 v17, -0x1

    cmp-long v19, v1, v17

    if-eqz v19, :cond_4

    int-to-long v14, v15

    cmp-long v19, v14, v1

    if-gez v19, :cond_5

    :cond_4
    cmp-long v1, v5, v17

    if-eqz v1, :cond_6

    int-to-long v1, v7

    cmp-long v7, v1, v5

    if-gez v7, :cond_5

    goto :goto_2

    .line 2727
    :cond_5
    :try_start_2
    iget-object v0, v11, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v1, "placement capping error"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/anythink/core/common/c; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v1, 0x1

    .line 2729
    :try_start_3
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->q(I)V
    :try_end_3
    .catch Lcom/anythink/core/common/c; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2731
    :try_start_4
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v1, "2003"

    invoke-static {v1, v9, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Capping."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    move-object/from16 v14, p6

    goto/16 :goto_7

    :catch_0
    move-exception v0

    move-object/from16 v14, p6

    goto/16 :goto_9

    .line 2739
    :cond_6
    :goto_2
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v12}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 507
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 508
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 509
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_4
    .catch Lcom/anythink/core/common/c; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move-object v5, v15

    move-object v6, v7

    move-object v13, v7

    move-object v7, v14

    move-object/from16 v20, v9

    move-object/from16 v9, p5

    .line 511
    :try_start_5
    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/d/x;Lcom/anythink/core/common/d/d;)V
    :try_end_5
    .catch Lcom/anythink/core/common/c; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 524
    invoke-static {v15, v13, v0}, Lcom/anythink/core/common/d;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 528
    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-direct {v11, v12, v10, v1, v15}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 531
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 532
    invoke-interface {v1, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 533
    invoke-interface {v1, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 535
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v2

    invoke-virtual {v2, v0, v10, v12, v1}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    .line 539
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_7

    .line 540
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v0, v10}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x1

    goto :goto_3

    :cond_7
    const/4 v9, 0x0

    :goto_3
    if-eqz v9, :cond_8

    .line 545
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_8

    const-string v0, "4005"

    move-object/from16 v1, v20

    .line 546
    invoke-static {v0, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    const/4 v1, 0x6

    move-object/from16 v13, p5

    .line 547
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->q(I)V

    move-object/from16 v14, p6

    const/4 v1, 0x1

    .line 549
    invoke-direct {v11, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V

    const/4 v15, 0x0

    .line 550
    iput-boolean v15, v11, Lcom/anythink/core/common/d;->f:Z

    return-void

    :cond_8
    move-object/from16 v14, p6

    move-object/from16 v16, v13

    const/4 v1, 0x1

    const/4 v15, 0x0

    move-object/from16 v13, p5

    .line 554
    invoke-static {}, Lcom/anythink/core/a/b;->a()Lcom/anythink/core/a/b;

    move-result-object v2

    iget-object v3, v11, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v12}, Lcom/anythink/core/a/b;->b(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d;)V

    .line 555
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 556
    iget-object v1, v11, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v13}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 559
    invoke-virtual {v11, v14}, Lcom/anythink/core/common/d;->b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;

    move-result-object v1

    .line 560
    iput-object v10, v11, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    .line 561
    iget-object v2, v11, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v10, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->G()I

    move-result v7

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, v8

    move v6, v9

    invoke-virtual/range {v1 .. v7}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;ZI)V

    .line 565
    iput-boolean v15, v11, Lcom/anythink/core/common/d;->f:Z

    if-nez v9, :cond_9

    .line 572
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v15

    new-instance v9, Lcom/anythink/core/common/d$2;

    move-object v1, v9

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, v16

    move-object v0, v9

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/anythink/core/common/d$2;-><init>(Lcom/anythink/core/common/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/g;Lcom/anythink/core/common/d/d;)V

    invoke-virtual {v15, v0}, Lcom/anythink/core/common/g/a/a;->b(Ljava/lang/Runnable;)V

    :cond_9
    return-void

    :catchall_1
    move-exception v0

    move-object/from16 v13, p5

    goto :goto_4

    :catch_1
    move-exception v0

    move-object/from16 v13, p5

    goto :goto_5

    :cond_a
    move-object/from16 v14, p6

    move-object v1, v9

    .line 2740
    :try_start_6
    iget-object v0, v11, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v2, "placement pacing error"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 2741
    invoke-virtual {v13, v0}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 2743
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "2004"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Pacing."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    :goto_4
    move-object/from16 v14, p6

    goto :goto_6

    :catch_2
    move-exception v0

    :goto_5
    move-object/from16 v14, p6

    goto :goto_8

    :cond_b
    move-object v1, v9

    .line 2706
    iget-object v0, v11, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v3, "unitgroup list is null"

    invoke-static {v0, v3}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2707
    invoke-virtual {v13, v2}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 2709
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "4004"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "No Adsource."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v1, v9

    .line 2695
    invoke-virtual {v13, v2}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 2697
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "4003"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Strategy is close."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Lcom/anythink/core/common/c; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception v0

    :goto_6
    const/4 v1, 0x1

    .line 520
    :goto_7
    invoke-direct {v11, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    return-void

    :catch_3
    move-exception v0

    :goto_8
    const/4 v1, 0x1

    .line 517
    :goto_9
    invoke-direct {v11, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method private a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/lang/String;D)V
    .locals 8

    .line 1131
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v7, Lcom/anythink/core/common/d$5;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p3

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/anythink/core/common/d$5;-><init>(Lcom/anythink/core/common/d;Lcom/anythink/core/api/ATBaseAdAdapter;DLjava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/d;)V
    .locals 8

    .line 1189
    new-instance v7, Lcom/anythink/core/common/e;

    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v3

    move-object v0, v7

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/anythink/core/common/e;-><init>(JJLcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/d;)V

    .line 1190
    iput-object v7, p0, Lcom/anythink/core/common/d;->h:Lcom/anythink/core/common/e;

    .line 1191
    invoke-virtual {v7}, Lcom/anythink/core/common/e;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method private a(Lcom/anythink/core/c/d$b;Z)V
    .locals 9

    .line 3003
    iget v0, p1, Lcom/anythink/core/c/d$b;->J:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 793
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 794
    invoke-virtual {v0}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 796
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 798
    :try_start_0
    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    .line 799
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    .line 3091
    iget-wide v4, v4, Lcom/anythink/core/c/d$b;->m:D

    const/4 v6, 0x1

    const/4 v7, 0x0

    cmpl-double v8, v2, v4

    if-lez v8, :cond_1

    .line 802
    invoke-virtual {p1, p1, v1, v7, v6}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 804
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object p2

    iget-object v0, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p2, v0, p1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 807
    :cond_1
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v0

    if-eqz p2, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    :goto_1
    if-eqz p2, :cond_3

    const/4 v7, 0x1

    :cond_3
    invoke-virtual {p1, v0, v6, v1, v7}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method

.method private static a(Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;)V
    .locals 1

    .line 694
    invoke-virtual {p0}, Lcom/anythink/core/c/d;->T()Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    const/4 p0, 0x5

    .line 695
    invoke-virtual {p1, p0}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 697
    new-instance p0, Lcom/anythink/core/common/c;

    const-string p1, ""

    const-string v0, "4003"

    invoke-static {v0, p1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    const-string v0, "Strategy is close."

    invoke-direct {p0, p1, v0}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw p0
.end method

.method private a(Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/x;Lcom/anythink/core/common/d/d;)V
    .locals 8

    .line 717
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->G()J

    move-result-wide v0

    .line 718
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->H()J

    move-result-wide v2

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    .line 720
    iget v4, p2, Lcom/anythink/core/common/d/x;->d:I

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 721
    iget p1, p2, Lcom/anythink/core/common/d/x;->c:I

    :cond_1
    const-wide/16 v5, -0x1

    cmp-long p2, v0, v5

    if-eqz p2, :cond_2

    int-to-long p1, p1

    cmp-long v7, p1, v0

    if-gez v7, :cond_3

    :cond_2
    cmp-long p1, v2, v5

    if-eqz p1, :cond_4

    int-to-long p1, v4

    cmp-long v0, p1, v2

    if-gez v0, :cond_3

    goto :goto_1

    .line 727
    :cond_3
    iget-object p1, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string p2, "placement capping error"

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 729
    invoke-virtual {p3, p1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 731
    new-instance p1, Lcom/anythink/core/common/c;

    const-string p2, ""

    const-string p3, "2003"

    invoke-static {p3, p2, p2}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p2

    const-string p3, "Capping."

    invoke-direct {p1, p2, p3}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_1
    return-void
.end method

.method private a(Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;JI)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/c/d;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;JI)V"
        }
    .end annotation

    .line 1067
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v10, Lcom/anythink/core/common/d$4;

    move-object v1, v10

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d$4;-><init>(Lcom/anythink/core/common/d;Ljava/lang/String;Lcom/anythink/core/c/d;IJILjava/util/List;)V

    invoke-virtual {v0, v10}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 399
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->M()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 400
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->N()Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->g()Ljava/lang/String;

    move-result-object v2

    .line 402
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->O()Ljava/lang/String;

    move-result-object v3

    .line 403
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->e()Ljava/lang/String;

    move-result-object v4

    .line 400
    invoke-static {v1, v2, v3, v4}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 405
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-lez v3, :cond_6

    .line 407
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/core/c/d$b;

    .line 409
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v5

    invoke-virtual {v5, p3, v4}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    .line 410
    invoke-virtual {v5}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v5

    goto :goto_2

    :cond_2
    move-object v5, v6

    :goto_2
    const/4 v7, 0x3

    const/4 v8, 0x1

    if-eqz v5, :cond_3

    .line 413
    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 415
    :try_start_0
    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v5

    .line 417
    invoke-virtual {v4, v5, v2, v7, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 422
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    nop

    .line 430
    :cond_3
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v5

    iget-object v9, v4, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v10, v4, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v5, v9, v10}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v5

    if-eqz v5, :cond_4

    const/4 v6, 0x2

    .line 432
    invoke-virtual {v4, v5, v2, v6, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    .line 436
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    :goto_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 442
    :cond_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 443
    iget-object v5, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/anythink/core/common/f;

    if-eqz v5, :cond_5

    .line 444
    iget-object v9, v4, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    .line 2345
    iget-object v10, v5, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v10, :cond_5

    .line 2346
    iget-object v5, v5, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/anythink/core/c/d$b;

    :cond_5
    if-eqz v6, :cond_1

    .line 446
    invoke-virtual {v4, v6, v2, v7, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 450
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    goto :goto_3

    .line 459
    :cond_6
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, p3, p2, p1, v0}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    if-nez v3, :cond_7

    .line 462
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p1

    invoke-virtual {p1, p3, p2}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method private a(Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/d/x;Lcom/anythink/core/common/d/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/c/d;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Lcom/anythink/core/common/d/x;",
            "Lcom/anythink/core/common/d/d;",
            ")V"
        }
    .end annotation

    .line 756
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/c/d$b;

    .line 757
    invoke-virtual {p8}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, p8, p7, v0}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/lang/String;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/d/x;Lcom/anythink/core/c/d$b;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 758
    iput v1, v0, Lcom/anythink/core/c/d$b;->a:I

    .line 759
    invoke-interface {p6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 763
    :cond_0
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 768
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/anythink/core/c/d$b;

    .line 770
    invoke-virtual {p8}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p8, p7, p3}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/lang/String;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/d/x;Lcom/anythink/core/c/d$b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 771
    iput v1, p3, Lcom/anythink/core/c/d$b;->a:I

    .line 772
    invoke-interface {p6, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 775
    :cond_2
    invoke-interface {p5, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 778
    :cond_3
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p1

    if-gtz p1, :cond_5

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_4

    goto :goto_2

    .line 779
    :cond_4
    iget-object p1, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string p2, "no vail adsource"

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x6

    .line 781
    invoke-virtual {p8, p1}, Lcom/anythink/core/common/d/d;->q(I)V

    const-string p1, ""

    const-string p2, "4005"

    .line 783
    invoke-static {p2, p1, p1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    .line 784
    new-instance p2, Lcom/anythink/core/common/c;

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p1, p3}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw p2

    :cond_5
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/g;)V
    .locals 21

    move-object/from16 v10, p0

    move-object/from16 v0, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    .line 11483
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->L()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->M()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 11484
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->N()Ljava/lang/String;

    move-result-object v1

    .line 11485
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->g()Ljava/lang/String;

    move-result-object v2

    .line 11486
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->O()Ljava/lang/String;

    move-result-object v4

    .line 11487
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->e()Ljava/lang/String;

    move-result-object v5

    .line 11484
    invoke-static {v1, v2, v4, v5}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 11490
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v0, v11, v12, v3}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    .line 11694
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->T()Z

    move-result v1
    :try_end_0
    .catch Lcom/anythink/core/common/c; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v2, 0x5

    const-string v9, ""

    if-eqz v1, :cond_c

    if-eqz v3, :cond_0

    .line 11705
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v4, :cond_b

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_b

    .line 11503
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/anythink/core/a/a;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/x;

    move-result-object v8

    .line 11717
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->G()J

    move-result-wide v1

    .line 11718
    invoke-virtual/range {p4 .. p4}, Lcom/anythink/core/c/d;->H()J

    move-result-wide v5

    if-eqz v8, :cond_2

    .line 11720
    iget v7, v8, Lcom/anythink/core/common/d/x;->d:I

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    :goto_0
    if-eqz v8, :cond_3

    .line 11721
    iget v15, v8, Lcom/anythink/core/common/d/x;->c:I
    :try_end_1
    .catch Lcom/anythink/core/common/c; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_1

    :cond_3
    const/4 v15, 0x0

    :goto_1
    const-wide/16 v17, -0x1

    cmp-long v19, v1, v17

    if-eqz v19, :cond_4

    int-to-long v14, v15

    cmp-long v19, v14, v1

    if-gez v19, :cond_5

    :cond_4
    cmp-long v1, v5, v17

    if-eqz v1, :cond_6

    int-to-long v1, v7

    cmp-long v7, v1, v5

    if-gez v7, :cond_5

    goto :goto_2

    .line 11727
    :cond_5
    :try_start_2
    iget-object v0, v10, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v1, "placement capping error"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/anythink/core/common/c; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v1, 0x1

    .line 11729
    :try_start_3
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->q(I)V
    :try_end_3
    .catch Lcom/anythink/core/common/c; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 11731
    :try_start_4
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v1, "2003"

    invoke-static {v1, v9, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Capping."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    move-object/from16 v14, p6

    goto/16 :goto_7

    :catch_0
    move-exception v0

    move-object/from16 v14, p6

    goto/16 :goto_9

    .line 11739
    :cond_6
    :goto_2
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v12}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 11507
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 11508
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 11509
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_4
    .catch Lcom/anythink/core/common/c; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move-object v5, v15

    move-object v6, v7

    move-object v13, v7

    move-object v7, v14

    move-object/from16 v20, v9

    move-object/from16 v9, p5

    .line 11511
    :try_start_5
    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/d/x;Lcom/anythink/core/common/d/d;)V
    :try_end_5
    .catch Lcom/anythink/core/common/c; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 11524
    invoke-static {v15, v13, v0}, Lcom/anythink/core/common/d;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 11528
    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    invoke-direct {v10, v12, v11, v1, v15}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 11531
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 11532
    invoke-interface {v1, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 11533
    invoke-interface {v1, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 11535
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v2

    invoke-virtual {v2, v0, v11, v12, v1}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    .line 11539
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_7

    .line 11540
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v0, v11}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x1

    goto :goto_3

    :cond_7
    const/4 v9, 0x0

    :goto_3
    if-eqz v9, :cond_8

    .line 11545
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_8

    const-string v0, "4005"

    move-object/from16 v1, v20

    .line 11546
    invoke-static {v0, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    const/4 v1, 0x6

    move-object/from16 v13, p5

    .line 11547
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->q(I)V

    move-object/from16 v14, p6

    const/4 v1, 0x1

    .line 11549
    invoke-direct {v10, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V

    const/4 v15, 0x0

    .line 11550
    iput-boolean v15, v10, Lcom/anythink/core/common/d;->f:Z

    return-void

    :cond_8
    move-object/from16 v14, p6

    move-object/from16 v16, v13

    const/4 v1, 0x1

    const/4 v15, 0x0

    move-object/from16 v13, p5

    .line 11554
    invoke-static {}, Lcom/anythink/core/a/b;->a()Lcom/anythink/core/a/b;

    move-result-object v2

    iget-object v3, v10, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v12}, Lcom/anythink/core/a/b;->b(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d;)V

    .line 11555
    invoke-virtual {v13, v1}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 11556
    iget-object v1, v10, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v13}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 11559
    invoke-virtual {v10, v14}, Lcom/anythink/core/common/d;->b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;

    move-result-object v1

    .line 11560
    iput-object v11, v10, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    .line 11561
    iget-object v2, v10, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v11, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11563
    invoke-virtual/range {p5 .. p5}, Lcom/anythink/core/common/d/d;->G()I

    move-result v7

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, v8

    move v6, v9

    invoke-virtual/range {v1 .. v7}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;ZI)V

    .line 11565
    iput-boolean v15, v10, Lcom/anythink/core/common/d;->f:Z

    if-nez v9, :cond_9

    .line 11572
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v15

    new-instance v9, Lcom/anythink/core/common/d$2;

    move-object v1, v9

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, v16

    move-object v0, v9

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/anythink/core/common/d$2;-><init>(Lcom/anythink/core/common/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/g;Lcom/anythink/core/common/d/d;)V

    invoke-virtual {v15, v0}, Lcom/anythink/core/common/g/a/a;->b(Ljava/lang/Runnable;)V

    :cond_9
    return-void

    :catchall_1
    move-exception v0

    move-object/from16 v13, p5

    goto :goto_4

    :catch_1
    move-exception v0

    move-object/from16 v13, p5

    goto :goto_5

    :cond_a
    move-object/from16 v14, p6

    move-object v1, v9

    .line 11740
    :try_start_6
    iget-object v0, v10, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v2, "placement pacing error"

    invoke-static {v0, v2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 11741
    invoke-virtual {v13, v0}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 11743
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "2004"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Pacing."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    :goto_4
    move-object/from16 v14, p6

    goto :goto_6

    :catch_2
    move-exception v0

    :goto_5
    move-object/from16 v14, p6

    goto :goto_8

    :cond_b
    move-object v1, v9

    .line 11706
    iget-object v0, v10, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v3, "unitgroup list is null"

    invoke-static {v0, v3}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 11707
    invoke-virtual {v13, v2}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 11709
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "4004"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "No Adsource."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v1, v9

    .line 11695
    invoke-virtual {v13, v2}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 11697
    new-instance v0, Lcom/anythink/core/common/c;

    const-string v2, "4003"

    invoke-static {v2, v1, v1}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    const-string v2, "Strategy is close."

    invoke-direct {v0, v1, v2}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Lcom/anythink/core/common/c; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception v0

    :goto_6
    const/4 v1, 0x1

    .line 11520
    :goto_7
    invoke-direct {v10, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    return-void

    :catch_3
    move-exception v0

    :goto_8
    const/4 v1, 0x1

    .line 11517
    :goto_9
    invoke-direct {v10, v1, v13, v0, v14}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/d;)V
    .locals 8

    .line 15189
    new-instance v7, Lcom/anythink/core/common/e;

    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v3

    move-object v0, v7

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/anythink/core/common/e;-><init>(JJLcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/d;)V

    .line 15190
    iput-object v7, p0, Lcom/anythink/core/common/d;->h:Lcom/anythink/core/common/e;

    .line 15191
    invoke-virtual {v7}, Lcom/anythink/core/common/e;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d$b;Z)V
    .locals 9

    .line 14003
    iget v0, p1, Lcom/anythink/core/c/d$b;->J:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 13793
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v0

    iget-object v2, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 13794
    invoke-virtual {v0}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 13796
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 13798
    :try_start_0
    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    .line 13799
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    .line 14091
    iget-wide v4, v4, Lcom/anythink/core/c/d$b;->m:D

    const/4 v6, 0x1

    const/4 v7, 0x0

    cmpl-double v8, v2, v4

    if-lez v8, :cond_1

    .line 13802
    invoke-virtual {p1, p1, v1, v7, v6}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 13804
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object p2

    iget-object p0, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p2, p0, p1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 13807
    :cond_1
    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object p0

    invoke-virtual {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object p0

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    :goto_1
    if-eqz p2, :cond_3

    const/4 v7, 0x1

    :cond_3
    invoke-virtual {p1, p0, v6, v0, v7}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method

.method private static synthetic a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d;Ljava/lang/String;ILjava/util/List;JI)V
    .locals 11

    .line 15067
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v10, Lcom/anythink/core/common/d$4;

    move-object v1, v10

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d$4;-><init>(Lcom/anythink/core/common/d;Ljava/lang/String;Lcom/anythink/core/c/d;IJILjava/util/List;)V

    invoke-virtual {v0, v10}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 12399
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->M()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 12400
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->N()Ljava/lang/String;

    move-result-object v1

    .line 12401
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->g()Ljava/lang/String;

    move-result-object v2

    .line 12402
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->O()Ljava/lang/String;

    move-result-object v3

    .line 12403
    invoke-virtual {p1}, Lcom/anythink/core/c/d;->e()Ljava/lang/String;

    move-result-object v4

    .line 12400
    invoke-static {v1, v2, v3, v4}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 12405
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-lez v3, :cond_6

    .line 12407
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/anythink/core/c/d$b;

    .line 12409
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v5

    invoke-virtual {v5, p3, v4}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    .line 12410
    invoke-virtual {v5}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v5

    goto :goto_2

    :cond_2
    move-object v5, v6

    :goto_2
    const/4 v7, 0x3

    const/4 v8, 0x1

    if-eqz v5, :cond_3

    .line 12413
    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 12415
    :try_start_0
    invoke-virtual {v5}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v5

    .line 12417
    invoke-virtual {v4, v5, v2, v7, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 12422
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    nop

    .line 12430
    :cond_3
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v5

    iget-object v9, v4, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v10, v4, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v5, v9, v10}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v5

    if-eqz v5, :cond_4

    const/4 v6, 0x2

    .line 12432
    invoke-virtual {v4, v5, v2, v6, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    .line 12436
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    :goto_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 12442
    :cond_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 12443
    iget-object v5, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/anythink/core/common/f;

    if-eqz v5, :cond_5

    .line 12444
    iget-object v9, v4, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    .line 13345
    iget-object v10, v5, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v10, :cond_5

    .line 13346
    iget-object v5, v5, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/anythink/core/c/d$b;

    :cond_5
    if-eqz v6, :cond_1

    .line 12446
    invoke-virtual {v4, v6, v2, v7, v8}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 12450
    invoke-static {v0, v4}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    goto :goto_3

    .line 12459
    :cond_6
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p0

    invoke-virtual {p0, p3, p2, p1, v0}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;)V

    if-nez v3, :cond_7

    .line 12462
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object p0

    invoke-virtual {p0, p3, p2}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V
    .locals 1

    const/4 v0, 0x1

    .line 57
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/d;ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;Lcom/anythink/core/common/d/d;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 705
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    return-void

    .line 706
    :cond_2
    iget-object p1, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string p2, "unitgroup list is null"

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x5

    .line 707
    invoke-virtual {p3, p1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 709
    new-instance p1, Lcom/anythink/core/common/c;

    const-string p2, ""

    const-string p3, "4004"

    invoke-static {p3, p2, p2}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p2

    const-string p3, "No Adsource."

    invoke-direct {p1, p2, p3}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 830
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    if-ltz v0, :cond_3

    .line 831
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 4003
    iget v3, v2, Lcom/anythink/core/c/d$b;->J:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 838
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v3

    invoke-virtual {v3, p2, v2}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 839
    invoke-virtual {v3}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v3

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    .line 841
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 844
    :try_start_0
    invoke-virtual {v3}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/c/d$b;III)V

    .line 850
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 851
    invoke-static {p0, v2}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    nop

    .line 858
    :cond_1
    invoke-static {v2}, Lcom/anythink/core/common/d;->a(Lcom/anythink/core/c/d$b;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 859
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 860
    invoke-static {p0, v2}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private a(ZLcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/anythink/core/common/d/d;",
            "Lcom/anythink/core/api/AdError;",
            "TT;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 918
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->f:Z

    .line 920
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    new-instance v2, Lcom/anythink/core/common/d$3;

    invoke-direct {v2, p0, p4, p2, p3}, Lcom/anythink/core/common/d$3;-><init>(Lcom/anythink/core/common/d;Lcom/anythink/core/common/g;Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 926
    invoke-virtual {p2, v0}, Lcom/anythink/core/common/d/d;->b(Z)V

    if-eqz p1, :cond_0

    .line 928
    iget-object p1, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p1

    const/16 p4, 0xa

    invoke-virtual {p1, p4, p2}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 930
    invoke-static {p2, p3}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    :cond_0
    return-void
.end method

.method private a(ZLcom/anythink/core/common/d/d;Ljava/lang/Throwable;Lcom/anythink/core/common/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/anythink/core/common/d/d;",
            "Ljava/lang/Throwable;",
            "TT;)V"
        }
    .end annotation

    .line 908
    instance-of v0, p3, Lcom/anythink/core/common/c;

    if-eqz v0, :cond_0

    .line 909
    check-cast p3, Lcom/anythink/core/common/c;

    iget-object p3, p3, Lcom/anythink/core/common/c;->a:Lcom/anythink/core/api/AdError;

    goto :goto_0

    .line 911
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p3

    const-string v0, "9999"

    const-string v1, ""

    invoke-static {v0, v1, p3}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p3

    .line 914
    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/anythink/core/common/d;->a(ZLcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;Lcom/anythink/core/common/g;)V

    return-void
.end method

.method private static a(Lcom/anythink/core/c/d$b;)Z
    .locals 4

    const/4 v0, 0x0

    .line 875
    :try_start_0
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v3, p0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 876
    invoke-virtual {v1}, Lcom/anythink/core/common/d/l;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 877
    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    return v3

    :cond_0
    if-eqz v1, :cond_1

    .line 887
    iget-wide v2, v1, Lcom/anythink/core/common/d/l;->price:D

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/common/d/l;->a(D)V

    .line 891
    :cond_1
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget p0, p0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v2, v3, p0}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;I)V

    if-eqz v1, :cond_2

    .line 894
    iget p0, v1, Lcom/anythink/core/common/d/l;->d:I

    const/16 v2, 0x42

    if-ne p0, v2, :cond_2

    .line 895
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p0

    invoke-virtual {p0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p0

    iget-object v1, v1, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_2
    return v0
.end method

.method private static a(Lcom/anythink/core/c/d;Ljava/lang/String;Lcom/anythink/core/common/d/d;Lcom/anythink/core/common/d/x;Lcom/anythink/core/c/d$b;)Z
    .locals 13

    move-object/from16 v0, p3

    move-object/from16 v3, p4

    .line 946
    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->e()Ljava/lang/String;

    move-result-object v5

    .line 947
    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->z()I

    move-result v1

    const/4 v2, 0x0

    const/4 v12, 0x1

    if-ne v1, v12, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->G()I

    move-result v11

    const-string v6, ""

    const-string v8, ""

    const/4 v9, -0x1

    move-object v7, p0

    .line 945
    invoke-static/range {v4 .. v11}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v6

    .line 948
    invoke-static {v6, v3, v2}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)V

    if-eqz v0, :cond_1

    .line 951
    iget-object v1, v3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d/x;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/x$a;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    .line 953
    iget v1, v0, Lcom/anythink/core/common/d/x$a;->e:I

    move v4, v1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v0, :cond_3

    .line 954
    iget v0, v0, Lcom/anythink/core/common/d/x$a;->d:I

    move v5, v0

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    .line 956
    :goto_3
    iget v0, v3, Lcom/anythink/core/c/d$b;->d:I

    const-string v7, "2003"

    const/4 v8, 0x2

    const/4 v1, -0x1

    const-string v9, "Out of Cap"

    const-string v10, ""

    if-eq v0, v1, :cond_4

    iget v0, v3, Lcom/anythink/core/c/d$b;->d:I

    if-lt v5, v0, :cond_4

    .line 4119
    iput-object v9, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const-string v2, "Out of Cap"

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v3, p4

    .line 958
    invoke-static/range {v0 .. v5}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Ljava/lang/String;Lcom/anythink/core/c/d$b;II)V

    .line 959
    invoke-static {v7, v10, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-static {v6, v8, v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V

    return v12

    .line 963
    :cond_4
    iget v0, v3, Lcom/anythink/core/c/d$b;->e:I

    if-eq v0, v1, :cond_5

    iget v0, v3, Lcom/anythink/core/c/d$b;->e:I

    if-lt v4, v0, :cond_5

    .line 5119
    iput-object v9, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const-string v2, "Out of Cap"

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v3, p4

    .line 965
    invoke-static/range {v0 .. v5}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Ljava/lang/String;Lcom/anythink/core/c/d$b;II)V

    .line 966
    invoke-static {v7, v10, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-static {v6, v8, v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V

    return v12

    .line 970
    :cond_5
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    move-result-object v0

    move-object v1, p1

    invoke-virtual {v0, p1, v3}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v7, "Out of Pacing"

    .line 6119
    iput-object v7, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const-string v2, "Out of Pacing"

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v3, p4

    .line 972
    invoke-static/range {v0 .. v5}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Ljava/lang/String;Lcom/anythink/core/c/d$b;II)V

    const/4 v0, 0x3

    const-string v1, "2004"

    .line 973
    invoke-static {v1, v10, v7}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v1

    invoke-static {v6, v0, v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V

    return v12

    .line 977
    :cond_6
    invoke-static {}, Lcom/anythink/core/common/b;->a()Lcom/anythink/core/common/b;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/anythink/core/common/b;->a(Lcom/anythink/core/c/d$b;)Z

    move-result v0

    const-string v7, "2007"

    const/4 v8, 0x4

    if-eqz v0, :cond_7

    const-string v9, "Request fail in pacing"

    .line 7119
    iput-object v9, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const-string v2, "Request fail in pacing"

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v3, p4

    .line 979
    invoke-static/range {v0 .. v5}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Ljava/lang/String;Lcom/anythink/core/c/d$b;II)V

    .line 980
    invoke-static {v7, v10, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-static {v6, v8, v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V

    return v12

    .line 984
    :cond_7
    iget v0, v3, Lcom/anythink/core/c/d$b;->n:I

    if-ne v0, v12, :cond_8

    .line 985
    invoke-static {}, Lcom/anythink/core/common/b;->a()Lcom/anythink/core/common/b;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/anythink/core/common/b;->b(Lcom/anythink/core/c/d$b;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v9, "Bid fail in pacing"

    .line 8119
    iput-object v9, v3, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const-string v2, "Bid fail in pacing"

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v3, p4

    .line 987
    invoke-static/range {v0 .. v5}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Ljava/lang/String;Lcom/anythink/core/c/d$b;II)V

    .line 988
    invoke-static {v7, v10, v9}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v0

    invoke-static {v6, v8, v0}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;)V

    return v12

    :cond_8
    return v2
.end method

.method static synthetic a(Lcom/anythink/core/common/d;)Z
    .locals 0

    .line 57
    iget-boolean p0, p0, Lcom/anythink/core/common/d;->m:Z

    return p0
.end method

.method static synthetic a(Lcom/anythink/core/common/d;Z)Z
    .locals 0

    .line 57
    iput-boolean p1, p0, Lcom/anythink/core/common/d;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/anythink/core/common/d;)J
    .locals 2

    .line 57
    iget-wide v0, p0, Lcom/anythink/core/common/d;->n:J

    return-wide v0
.end method

.method private b(Lcom/anythink/core/c/d;Lcom/anythink/core/common/d/d;)V
    .locals 1

    .line 739
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 740
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    const-string v0, "placement pacing error"

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    .line 741
    invoke-virtual {p2, p1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 743
    new-instance p1, Lcom/anythink/core/common/c;

    const-string p2, ""

    const-string v0, "2004"

    invoke-static {v0, p2, p2}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p2

    const-string v0, "Pacing."

    invoke-direct {p1, p2, v0}, Lcom/anythink/core/common/c;-><init>(Lcom/anythink/core/api/AdError;Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic c(Lcom/anythink/core/common/d;)Z
    .locals 0

    .line 57
    iget-boolean p0, p0, Lcom/anythink/core/common/d;->o:Z

    return p0
.end method

.method static synthetic d(Lcom/anythink/core/common/d;)Z
    .locals 1

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->m:Z

    return v0
.end method

.method static synthetic e(Lcom/anythink/core/common/d;)J
    .locals 2

    const-wide/16 v0, 0x0

    .line 57
    iput-wide v0, p0, Lcom/anythink/core/common/d;->n:J

    return-wide v0
.end method

.method static synthetic f(Lcom/anythink/core/common/d;)Ljava/lang/String;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/anythink/core/common/d;->a:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;
    .locals 22

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 1223
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v2

    iget-object v3, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v2

    .line 1225
    iget-object v3, v0, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, ""

    if-eqz v3, :cond_0

    move-object v13, v4

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    move-object v13, v3

    :goto_0
    if-nez v2, :cond_1

    .line 1228
    iget-object v2, v0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v2

    iget-object v3, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v2

    :cond_1
    move-object v8, v2

    const/4 v2, 0x0

    if-nez v8, :cond_3

    .line 1233
    iget-object v15, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    const/16 v17, 0x0

    const/16 v19, -0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const-string v14, ""

    const-string v16, ""

    const-string v18, ""

    invoke-static/range {v14 .. v21}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v5

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    .line 1235
    invoke-static {v5, v1, v4, v13}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, -0x1

    const/4 v10, -0x1

    const/4 v14, 0x0

    const-string v9, ""

    const-string v11, ""

    const-string v12, ""

    const-string v15, ""

    .line 1237
    invoke-static/range {v5 .. v15}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :goto_1
    return-object v2

    .line 1243
    :cond_3
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    iget-object v3, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1245
    iget-object v6, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/anythink/core/c/d;->F()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v5, ""

    const-string v7, ""

    const-string v9, ""

    invoke-static/range {v5 .. v12}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v5

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    .line 1247
    invoke-static {v5, v1, v4, v13}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    const/4 v7, 0x3

    const/4 v8, -0x1

    const/4 v10, -0x1

    const/4 v14, 0x0

    const-string v9, ""

    const-string v11, ""

    const-string v12, ""

    const-string v15, ""

    .line 1249
    invoke-static/range {v5 .. v15}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :goto_2
    return-object v2

    .line 1255
    :cond_5
    iget-object v3, v0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v3

    iget-object v5, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v3, v8, v5}, Lcom/anythink/core/a/a;->a(Lcom/anythink/core/c/d;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1257
    iget-object v6, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/anythink/core/c/d;->F()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v5, ""

    const-string v7, ""

    const-string v9, ""

    invoke-static/range {v5 .. v12}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v5

    if-eqz v1, :cond_6

    const/4 v1, 0x2

    .line 1259
    invoke-static {v5, v1, v4, v13}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, -0x1

    const/4 v10, -0x1

    const/4 v14, 0x0

    const-string v9, ""

    const-string v11, ""

    const-string v12, ""

    const-string v15, ""

    .line 1261
    invoke-static/range {v5 .. v15}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :goto_3
    return-object v2

    .line 1266
    :cond_7
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v2

    iget-object v3, v0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v5, p1

    invoke-virtual {v2, v5, v3, v4, v1}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/anythink/core/common/d/b;

    move-result-object v1

    return-object v1
.end method

.method public final a()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/e;->b(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    .line 121
    iget-boolean v1, p0, Lcom/anythink/core/common/d;->m:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->m:Z

    .line 123
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/core/common/d;->n:J

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .line 182
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x1

    new-array v7, v1, [I

    const/4 v1, 0x0

    aput v1, v7, v1

    const-string v2, "ofm_tid_key"

    .line 185
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    :try_start_0
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v7, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    :catchall_0
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/d$1;

    move-object v2, v1

    move-object v3, p0

    move-object v4, p1

    move-object v5, p4

    move-object v6, p3

    move-object v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/anythink/core/common/d$1;-><init>(Lcom/anythink/core/common/d;Landroid/content/Context;Lcom/anythink/core/common/g;Ljava/lang/String;[ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/anythink/core/common/d/b;)V
    .locals 0

    .line 1196
    invoke-virtual {p1}, Lcom/anythink/core/common/d/b;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 1197
    iput p1, p0, Lcom/anythink/core/common/d;->e:I

    :cond_0
    return-void
.end method

.method public abstract a(Lcom/anythink/core/common/g;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/anythink/core/common/g;Lcom/anythink/core/api/AdError;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/anythink/core/api/AdError;",
            ")V"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/anythink/core/common/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 102
    iput p1, p0, Lcom/anythink/core/common/d;->e:I

    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/anythink/core/common/d;->k:J

    .line 105
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->m:Z

    const-wide/16 v1, 0x0

    .line 106
    iput-wide v1, p0, Lcom/anythink/core/common/d;->n:J

    .line 107
    iput-boolean v0, p0, Lcom/anythink/core/common/d;->o:Z

    return-void

    .line 109
    :cond_0
    iput v0, p0, Lcom/anythink/core/common/d;->e:I

    return-void
.end method

.method public final a(Ljava/lang/String;D)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/f;

    if-eqz p1, :cond_0

    .line 143
    invoke-virtual {p1, p2, p3}, Lcom/anythink/core/common/f;->a(D)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/anythink/core/common/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    return-void
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    .line 1206
    invoke-virtual {p0, p1, v0}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/g;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TT;)Z"
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public final b(Landroid/content/Context;)Lcom/anythink/core/api/ATAdStatusInfo;
    .locals 4

    .line 1211
    invoke-virtual {p0}, Lcom/anythink/core/common/d;->c()Z

    move-result v0

    const/4 v1, 0x0

    .line 1212
    invoke-virtual {p0, p1, v1}, Lcom/anythink/core/common/d;->a(Landroid/content/Context;Z)Lcom/anythink/core/common/d/b;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1216
    invoke-virtual {p1}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/api/ATAdInfo;->fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 1218
    :goto_0
    new-instance v3, Lcom/anythink/core/api/ATAdStatusInfo;

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-direct {v3, v0, v1, v2}, Lcom/anythink/core/api/ATAdStatusInfo;-><init>(ZZLcom/anythink/core/api/ATAdInfo;)V

    return-object v3
.end method

.method public abstract b(Lcom/anythink/core/common/g;)Lcom/anythink/core/common/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/anythink/core/common/f;"
        }
    .end annotation
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b()Z
    .locals 5

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/anythink/core/common/d;->k:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/anythink/core/common/d;->l:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 3

    .line 163
    iget-boolean v0, p0, Lcom/anythink/core/common/d;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/anythink/core/common/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/anythink/core/common/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f;

    if-eqz v0, :cond_1

    .line 168
    invoke-virtual {v0}, Lcom/anythink/core/common/f;->d()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 2

    .line 1174
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    new-instance v1, Lcom/anythink/core/common/d$6;

    invoke-direct {v1, p0}, Lcom/anythink/core/common/d$6;-><init>(Lcom/anythink/core/common/d;)V

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
