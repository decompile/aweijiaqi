.class public abstract Lcom/anythink/core/common/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/core/common/f$b;,
        Lcom/anythink/core/common/f$a;
    }
.end annotation


# instance fields
.field protected A:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected B:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected C:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/common/f$b;",
            ">;"
        }
    .end annotation
.end field

.field E:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATBaseAdAdapter;",
            ">;"
        }
    .end annotation
.end field

.field F:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field G:Z

.field H:Z

.field I:Z

.field J:D

.field protected K:Ljava/lang/Runnable;

.field L:D

.field M:Z

.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;

.field protected b:Landroid/content/Context;

.field protected c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field protected d:I

.field protected e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field g:Z

.field protected h:Z

.field protected i:Z

.field j:Z

.field k:Z

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field protected p:Lcom/anythink/core/c/d;

.field protected q:Ljava/lang/String;

.field protected r:Ljava/lang/String;

.field protected s:Z

.field t:Ljava/lang/String;

.field u:Lcom/anythink/core/api/AdError;

.field v:J

.field w:I

.field x:I

.field y:I

.field z:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    const-string v0, ""

    .line 61
    iput-object v0, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    .line 62
    iput-object v0, p0, Lcom/anythink/core/common/f;->f:Ljava/lang/String;

    const/4 v1, 0x0

    .line 64
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->g:Z

    .line 65
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->h:Z

    .line 66
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->i:Z

    .line 67
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->j:Z

    .line 69
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->k:Z

    .line 95
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/anythink/core/common/f;->z:Ljava/lang/Object;

    .line 113
    new-instance v2, Lcom/anythink/core/common/f$1;

    invoke-direct {v2, p0}, Lcom/anythink/core/common/f$1;-><init>(Lcom/anythink/core/common/f;)V

    iput-object v2, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    .line 1238
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->M:Z

    .line 168
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/anythink/core/common/f;->c:Ljava/lang/ref/WeakReference;

    .line 169
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    .line 171
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    .line 172
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    .line 174
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    .line 175
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    .line 177
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    .line 179
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    .line 180
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    .line 182
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    .line 184
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x5

    invoke-direct {p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object p1, p0, Lcom/anythink/core/common/f;->E:Ljava/util/concurrent/ConcurrentHashMap;

    const-string p1, "4001"

    .line 186
    invoke-static {p1, v0, v0}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    return-void
.end method

.method private a(Lcom/anythink/core/c/d$b;JZ)Ljava/lang/Runnable;
    .locals 3

    const-wide/16 v0, -0x1

    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1107
    :cond_0
    new-instance v0, Lcom/anythink/core/common/f$5;

    invoke-direct {v0, p0, p1, p4}, Lcom/anythink/core/common/f$5;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/c/d$b;Z)V

    .line 1142
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1, v0, p2, p3}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;J)V

    return-object v0
.end method

.method static synthetic a(Lcom/anythink/core/common/f;)Ljava/lang/String;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    return-object p0
.end method

.method private declared-synchronized a(ILjava/util/List;Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;Z)V"
        }
    .end annotation

    monitor-enter p0

    .line 290
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 291
    monitor-exit p0

    return-void

    .line 294
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 295
    invoke-direct {p0}, Lcom/anythink/core/common/f;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 296
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 297
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-interface {p2, v1, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298
    invoke-interface {p2, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 299
    invoke-interface {p3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 301
    iget-object p1, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addAdSourceToRequestingPool:start to request: waiting size:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "; requesting size:"

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/anythink/core/c/d$b;

    .line 303
    invoke-direct {p0, p2, v1, p4, v1}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;ZZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 305
    :cond_1
    monitor-exit p0

    return-void

    .line 306
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/anythink/core/common/f;->h()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    .line 309
    :cond_3
    :try_start_3
    iget-object p1, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "addAdSourceToRequestingPool(Has been returned):start to request: waiting size:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "; requesting size:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-lez p1, :cond_a

    .line 316
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/c/d$b;

    .line 318
    iget v2, p0, Lcom/anythink/core/common/f;->y:I

    iget-object v3, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v3}, Lcom/anythink/core/c/d;->S()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 319
    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 320
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    invoke-direct {p0, p1, v1, p4, v0}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;ZZZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 322
    monitor-exit p0

    return-void

    :cond_4
    if-eqz p4, :cond_8

    .line 324
    :try_start_4
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v2

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 325
    iget-wide v3, p1, Lcom/anythink/core/c/d$b;->m:D

    invoke-virtual {v2}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v2

    iget-wide v5, v2, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double v2, v3, v5

    if-lez v2, :cond_5

    goto :goto_1

    .line 331
    :cond_5
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz p1, :cond_6

    .line 332
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->G:Z

    .line 333
    invoke-direct {p0}, Lcom/anythink/core/common/f;->g()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 336
    :cond_6
    monitor-exit p0

    return-void

    .line 326
    :cond_7
    :goto_1
    :try_start_5
    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 327
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-direct {p0, p1, v1, p4, v1}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;ZZZ)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 329
    monitor-exit p0

    return-void

    .line 337
    :cond_8
    :try_start_6
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz p1, :cond_9

    .line 338
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->H:Z

    .line 339
    invoke-direct {p0}, Lcom/anythink/core/common/f;->g()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 342
    :cond_9
    monitor-exit p0

    return-void

    .line 343
    :cond_a
    :try_start_7
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz p1, :cond_c

    if-eqz p4, :cond_b

    .line 345
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->G:Z

    goto :goto_2

    .line 347
    :cond_b
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->H:Z

    .line 349
    :goto_2
    invoke-direct {p0}, Lcom/anythink/core/common/f;->g()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 355
    :cond_c
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(J)V
    .locals 2

    .line 1162
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/c/d$b;Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/api/ATBaseAdAdapter;",
            "Lcom/anythink/core/c/d$b;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .line 720
    iget v0, p2, Lcom/anythink/core/c/d$b;->b:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 721
    iget-object v0, p0, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v3}, Lcom/anythink/core/c/d;->C()I

    move-result v3

    if-eqz p4, :cond_0

    iget p4, p0, Lcom/anythink/core/common/f;->x:I

    goto :goto_0

    :cond_0
    iget p4, p0, Lcom/anythink/core/common/f;->w:I

    :goto_0
    invoke-static {v0, v1, v2, v3, p4}, Lcom/anythink/core/common/g/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Lorg/json/JSONObject;

    move-result-object p4

    .line 722
    invoke-virtual {p4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p4

    const-string v0, "tp_info"

    invoke-interface {p3, v0, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    :cond_1
    new-instance p4, Lcom/anythink/core/common/f$3;

    invoke-direct {p4, p0, p1, p2, p3}, Lcom/anythink/core/common/f$3;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/c/d$b;Ljava/util/Map;)V

    .line 764
    iget-object p1, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {p1}, Lcom/anythink/core/c/d;->C()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "2"

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 765
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    return-void

    .line 767
    :cond_2
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/anythink/core/common/g/a/a;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/anythink/core/c/d$b;)V
    .locals 5

    if-eqz p1, :cond_3

    .line 267
    iget v0, p1, Lcom/anythink/core/c/d$b;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2211
    iget v0, p1, Lcom/anythink/core/c/d$b;->H:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_1

    .line 270
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v0

    .line 271
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    iget-object v2, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget p1, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v2, p1}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;I)V

    if-eqz v0, :cond_1

    .line 273
    iget p1, v0, Lcom/anythink/core/common/d/l;->d:I

    const/16 v1, 0x42

    if-ne p1, v1, :cond_1

    .line 274
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object p1

    iget-object v1, v0, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    if-eqz v0, :cond_3

    .line 277
    iget-wide v1, p0, Lcom/anythink/core/common/f;->J:D

    const-wide/16 v3, 0x0

    cmpl-double p1, v1, v3

    if-lez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-wide v1, v0, Lcom/anythink/core/common/d/l;->price:D

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/common/d/l;->a(D)V

    :cond_3
    :goto_1
    return-void
.end method

.method private declared-synchronized a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V
    .locals 8

    monitor-enter p0

    .line 983
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 985
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;->b(Lcom/anythink/core/c/d$b;)V

    .line 987
    iget-object v1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 988
    iget-object v1, p0, Lcom/anythink/core/common/f;->E:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object p1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_5

    .line 993
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v2

    new-instance v3, Lcom/anythink/core/common/f$4;

    invoke-direct {v3, p0, p2}, Lcom/anythink/core/common/f$4;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 1007
    invoke-virtual {p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    .line 1008
    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v3

    .line 1010
    invoke-direct {p0, v3}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    .line 1011
    monitor-exit p0

    return-void

    .line 1015
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->B()I

    move-result v6

    invoke-virtual {p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7, p3}, Lcom/anythink/core/api/AdError;->putNetworkErrorMsg(Ljava/lang/String;ILjava/lang/String;Lcom/anythink/core/api/AdError;)V

    .line 1017
    invoke-static {}, Lcom/anythink/core/common/b;->a()Lcom/anythink/core/common/b;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 10051
    iget-object v4, v4, Lcom/anythink/core/common/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019
    iget-object v4, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1023
    iget-object v6, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Runnable;

    if-eqz v6, :cond_2

    .line 1025
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 1026
    iget-object v6, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    .line 1030
    :goto_0
    iget-object v7, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Runnable;

    if-eqz v7, :cond_3

    .line 1032
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 1033
    iget-object v6, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x1

    :cond_3
    if-eqz v6, :cond_4

    .line 10454
    iput v1, v2, Lcom/anythink/core/common/d/d;->o:I

    .line 1042
    :cond_4
    invoke-direct {p0, v3, v2, v1}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Z)V

    .line 1044
    sget-object v3, Lcom/anythink/core/common/b/e$e;->b:Ljava/lang/String;

    sget-object v6, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v3, v6, v7}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v4

    invoke-static {v2, p1, p3, v6, v7}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;J)V

    :cond_5
    if-eqz p2, :cond_6

    .line 1051
    invoke-virtual {p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object p2

    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->s()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 1052
    invoke-direct {p0}, Lcom/anythink/core/common/f;->i()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-boolean p1, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez p1, :cond_8

    .line 1053
    invoke-direct {p0}, Lcom/anythink/core/common/f;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_6
    if-eqz v0, :cond_7

    .line 1058
    :try_start_2
    iget-object p1, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    iget-object p2, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-direct {p0, v1, p1, p2, v1}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    .line 1060
    :cond_7
    :try_start_3
    iget-object p2, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    iget-object p3, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-direct {p0, v1, p2, p3, p1}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1063
    :cond_8
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Lcom/anythink/core/c/d$b;ZZZ)V
    .locals 10

    .line 605
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startAdSourceRequest: NetworkFirmId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "---content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "----Default:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, "-----fromHBPool:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 608
    :try_start_0
    iget v1, p1, Lcom/anythink/core/c/d$b;->n:I

    if-ne v1, v0, :cond_1

    .line 609
    iget-object v1, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    const-string v2, "hb request send win notice url, remove cache"

    invoke-static {v1, v2}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object v1

    const/16 v2, 0x42

    if-eqz v1, :cond_0

    .line 612
    invoke-virtual {v1}, Lcom/anythink/core/common/d/l;->b()V

    .line 614
    iget v3, p1, Lcom/anythink/core/c/d$b;->b:I

    if-ne v3, v2, :cond_0

    .line 615
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v3

    iget-object v4, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;Lcom/anythink/core/common/d/l;)V

    .line 619
    :cond_0
    iget v1, p1, Lcom/anythink/core/c/d$b;->b:I

    if-eq v1, v2, :cond_1

    .line 620
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    iget-object v2, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v3, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :cond_1
    :goto_0
    if-eqz p3, :cond_2

    .line 631
    iget v1, p0, Lcom/anythink/core/common/f;->x:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/anythink/core/common/f;->x:I

    goto :goto_1

    .line 633
    :cond_2
    iget v1, p0, Lcom/anythink/core/common/f;->w:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/anythink/core/common/f;->w:I

    .line 636
    :goto_1
    iget-object v2, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v4, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    if-eqz p3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    :goto_2
    move-object v6, v1

    iget-object v1, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v1}, Lcom/anythink/core/c/d;->F()I

    move-result v7

    iget-boolean v8, p0, Lcom/anythink/core/common/f;->s:Z

    iget v9, p0, Lcom/anythink/core/common/f;->d:I

    invoke-static/range {v2 .. v9}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v1

    .line 638
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v2

    iget-object v3, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    .line 639
    invoke-virtual {v2}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v2

    goto :goto_3

    :cond_4
    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_5

    .line 640
    invoke-virtual {v2}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 641
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;->d(Lcom/anythink/core/c/d$b;)V

    return-void

    .line 645
    :cond_5
    invoke-static {p1}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    const/4 v4, 0x0

    const-string v5, ""

    if-nez v2, :cond_7

    if-eqz p3, :cond_6

    .line 647
    iget p2, p0, Lcom/anythink/core/common/f;->x:I

    goto :goto_4

    :cond_6
    iget p2, p0, Lcom/anythink/core/common/f;->w:I

    :goto_4
    sub-int/2addr p2, v0

    invoke-static {v1, p1, p2}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)V

    .line 649
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p1, Lcom/anythink/core/c/d$b;->g:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " does not exist!"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "2002"

    invoke-static {p3, v5, p2}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p2

    .line 650
    iget-object p3, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    iget-object p4, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v0, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {p3, p4, v0, v5, p2}, Lcom/anythink/core/api/AdError;->putNetworkErrorMsg(Ljava/lang/String;ILjava/lang/String;Lcom/anythink/core/api/AdError;)V

    .line 651
    invoke-direct {p0, p1, v3, p2}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    const-wide/16 p3, 0x0

    .line 654
    :try_start_1
    invoke-static {v1, v4, p2, p3, p4}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    return-void

    .line 663
    :cond_7
    :try_start_2
    iget v3, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/anythink/core/common/g/d;->a(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_5

    :catchall_2
    nop

    :goto_5
    if-eqz p3, :cond_8

    .line 669
    iget v3, p0, Lcom/anythink/core/common/f;->x:I

    goto :goto_6

    :cond_8
    iget v3, p0, Lcom/anythink/core/common/f;->w:I

    :goto_6
    sub-int/2addr v3, v0

    invoke-static {v2, v1, p1, v3}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)Lcom/anythink/core/common/d/d;

    move-result-object v1

    .line 670
    invoke-virtual {v1, p2}, Lcom/anythink/core/common/d/d;->a(Z)V

    if-eqz p4, :cond_9

    const/4 p4, 0x5

    .line 3445
    iput p4, v1, Lcom/anythink/core/common/d/d;->n:I

    .line 676
    :cond_9
    iget-object p4, p0, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    invoke-static {p4}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p4

    invoke-virtual {p4, v0, v1}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 678
    sget-object p4, Lcom/anythink/core/common/b/e$e;->a:Ljava/lang/String;

    sget-object v0, Lcom/anythink/core/common/b/e$e;->h:Ljava/lang/String;

    invoke-static {v1, p4, v0, v5}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_b

    .line 684
    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->b()J

    move-result-wide v6

    invoke-direct {p0, p1, v6, v7, p3}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;JZ)Ljava/lang/Runnable;

    move-result-object p2

    if-eqz p2, :cond_a

    .line 686
    iget-object p4, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    iget-object v0, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p4, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4135
    :cond_a
    iget-wide v6, p1, Lcom/anythink/core/c/d$b;->w:J

    .line 689
    invoke-direct {p0, p1, v6, v7, p3}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;JZ)Ljava/lang/Runnable;

    move-result-object p2

    if-eqz p2, :cond_b

    .line 691
    iget-object p4, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    iget-object v0, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p4, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_b
    invoke-virtual {v1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2, v1, v4}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Z)V

    .line 697
    iget-object p2, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    iget-object p4, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    iget-object p2, p0, Lcom/anythink/core/common/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_c

    const-string p1, "4002"

    .line 701
    invoke-static {p1, v5, v5}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    return-void

    .line 705
    :cond_c
    iget-object p2, p0, Lcom/anythink/core/common/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_d

    .line 706
    iget-object p2, p0, Lcom/anythink/core/common/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/app/Activity;

    invoke-virtual {v2, p2}, Lcom/anythink/core/api/ATBaseAdAdapter;->refreshActivityContext(Landroid/app/Activity;)V

    .line 709
    :cond_d
    iget-object p2, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    iget-object p4, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    invoke-virtual {p2, p4, v0, p1}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;

    move-result-object p2

    invoke-direct {p0, v2, p1, p2, p3}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/c/d$b;Ljava/util/Map;Z)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f;Lcom/anythink/core/c/d$b;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 55
    invoke-direct {p0, p1, v1, v0, v0}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;ZZZ)V

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/common/f;Ljava/util/List;Ljava/util/List;Z)V
    .locals 1

    const/4 v0, 0x1

    .line 55
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Z)V
    .locals 1

    .line 1330
    iget-object v0, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/f$b;

    if-nez v0, :cond_0

    .line 1332
    new-instance v0, Lcom/anythink/core/common/f$b;

    invoke-direct {v0, p0, p2, p3}, Lcom/anythink/core/common/f$b;-><init>(Lcom/anythink/core/common/f;Lcom/anythink/core/common/d/d;Z)V

    .line 1333
    iget-object p2, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 1335
    :cond_0
    iput-object p2, v0, Lcom/anythink/core/common/f$b;->a:Lcom/anythink/core/common/d/d;

    .line 1336
    iput-boolean p3, v0, Lcom/anythink/core/common/f$b;->b:Z

    return-void
.end method

.method private declared-synchronized a(Ljava/util/List;Lcom/anythink/core/c/d$b;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Lcom/anythink/core/c/d$b;",
            "Z)V"
        }
    .end annotation

    monitor-enter p0

    .line 364
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->I:Z

    if-eqz v0, :cond_0

    .line 365
    invoke-direct {p0, p2}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366
    monitor-exit p0

    return-void

    .line 369
    :cond_0
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 370
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    if-eqz p3, :cond_2

    .line 374
    iget-object p2, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    iget-object p3, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    iget-object v0, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/anythink/core/c/d$b;

    iget-object p3, p3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 375
    :cond_1
    iget-object p2, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    iget-object p3, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-direct {p0, p1, p2, p3, p1}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-void

    .line 378
    :cond_2
    :try_start_2
    iget-object p2, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    iget-object p3, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    iget-object v0, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/anythink/core/c/d$b;

    iget-object p3, p3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 379
    :cond_3
    iget-object p2, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    iget-object p3, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382
    :cond_4
    monitor-exit p0

    return-void

    .line 385
    :cond_5
    :try_start_3
    monitor-enter p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 387
    :try_start_4
    invoke-static {p1, p2}, Lcom/anythink/core/common/g/g;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;)V

    .line 388
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p2

    :try_start_5
    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private static a(Lorg/json/JSONArray;Lcom/anythink/core/c/d$b;DI)V
    .locals 4

    .line 535
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "unit_id"

    .line 536
    iget-object v2, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "nw_firm_id"

    .line 537
    iget v2, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "bidprice"

    .line 538
    invoke-virtual {p1}, Lcom/anythink/core/c/d$b;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string p1, "ctype"

    .line 539
    invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string p1, "result"

    .line 540
    invoke-virtual {v0, p1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 542
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .line 1313
    iget-object v0, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1314
    iget-object v0, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/f$b;

    if-eqz p1, :cond_0

    .line 1315
    iget-boolean p1, p1, Lcom/anythink/core/common/f$b;->b:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private b(Ljava/lang/String;)Lcom/anythink/core/c/d$b;
    .locals 1

    .line 1345
    iget-object v0, p0, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_0

    .line 1346
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/c/d$b;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private declared-synchronized b(Lcom/anythink/core/c/d$b;)V
    .locals 1

    monitor-enter p0

    .line 399
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 400
    iget-object v0, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    .line 196
    iget-object v0, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v0}, Lcom/anythink/core/c/d;->K()Lcom/anythink/core/common/d/z;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 201
    :cond_0
    iget v1, v0, Lcom/anythink/core/common/d/z;->c:I

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/anythink/core/c/d$b;

    .line 203
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 204
    iget v5, v3, Lcom/anythink/core/c/d$b;->b:I

    if-ne v5, v1, :cond_1

    aput-object v3, v2, v4

    .line 210
    :cond_2
    aget-object p1, v2, v4

    if-nez p1, :cond_3

    return-void

    .line 214
    :cond_3
    iget-object p1, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "addDefaultAdSourceToRequestingPool: Default UnitGroupInfo:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, v2, v4

    iget v3, v3, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "--content:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v2, v4

    iget-object v3, v3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object p1, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "addDefaultAdSourceToRequestingPool delay:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Lcom/anythink/core/common/d/z;->b:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p1

    new-instance v1, Lcom/anythink/core/common/f$2;

    invoke-direct {v1, p0, v2}, Lcom/anythink/core/common/f$2;-><init>(Lcom/anythink/core/common/f;[Lcom/anythink/core/c/d$b;)V

    iget-wide v2, v0, Lcom/anythink/core/common/d/z;->b:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private b(Z)V
    .locals 11

    .line 934
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onLoadedCallbackToDeveloper: isCache:"

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 935
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    .line 936
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->k:Z

    .line 938
    iget-object v1, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 940
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 943
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/anythink/core/common/f;->v:J

    sub-long/2addr v1, v3

    .line 944
    iget-object v3, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v4, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v5, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    iget-object v7, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/anythink/core/c/d;->F()I

    move-result v8

    iget-boolean v9, p0, Lcom/anythink/core/common/f;->s:Z

    iget v10, p0, Lcom/anythink/core/common/f;->d:I

    invoke-static/range {v3 .. v10}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v3

    .line 945
    invoke-virtual {v3, v0}, Lcom/anythink/core/common/d/d;->b(Z)V

    .line 946
    invoke-virtual {v3, v1, v2}, Lcom/anythink/core/common/d/d;->d(J)V

    if-eqz p1, :cond_1

    const/4 p1, 0x5

    .line 948
    invoke-virtual {v3, p1}, Lcom/anythink/core/common/d/d;->q(I)V

    .line 952
    :cond_1
    iget-object p1, p0, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object p1

    const/16 v0, 0xc

    invoke-virtual {p1, v0, v3}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 954
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p1

    iget-object v0, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 956
    iget-object v0, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v0}, Lcom/anythink/core/c/d;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/anythink/core/common/d;->a(Ljava/lang/String;)V

    .line 960
    :cond_2
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v1

    iget-object v2, p0, Lcom/anythink/core/common/f;->N:Ljava/util/List;

    iget-object v3, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v4, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    iget-object v5, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v6, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/anythink/core/common/f;->s:Z

    invoke-virtual/range {v1 .. v8}, Lcom/anythink/core/common/a;->a(Ljava/util/List;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 965
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->M:Z

    if-nez p1, :cond_3

    .line 966
    invoke-virtual {p0}, Lcom/anythink/core/common/f;->a()V

    .line 969
    :cond_3
    invoke-virtual {p0}, Lcom/anythink/core/common/f;->b()V

    .line 971
    invoke-direct {p0}, Lcom/anythink/core/common/f;->j()V

    return-void
.end method

.method private c(Lcom/anythink/core/c/d$b;)V
    .locals 2

    .line 526
    iget-object v0, p0, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_0

    .line 527
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/f;->F:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private declared-synchronized d(Lcom/anythink/core/c/d$b;)V
    .locals 5

    monitor-enter p0

    .line 795
    :try_start_0
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->m:D

    iget-wide v2, p0, Lcom/anythink/core/common/f;->J:D

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 796
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->m:D

    iput-wide v0, p0, Lcom/anythink/core/common/f;->J:D

    .line 799
    :cond_0
    invoke-direct {p0, p1}, Lcom/anythink/core/common/f;->b(Lcom/anythink/core/c/d$b;)V

    .line 800
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCacheAdLoaded: NetworkFirmId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "---content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget p1, p0, Lcom/anythink/core/common/f;->y:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/anythink/core/common/f;->y:I

    .line 802
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez p1, :cond_1

    .line 803
    invoke-direct {p0, v0}, Lcom/anythink/core/common/f;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 805
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized g()V
    .locals 5

    monitor-enter p0

    .line 237
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->G:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/anythink/core/common/f;->H:Z

    if-nez v0, :cond_0

    goto :goto_2

    .line 241
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->I:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 242
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->I:Z

    .line 243
    iget-object v1, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 244
    :try_start_1
    iget-object v2, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    if-eqz v3, :cond_1

    .line 245
    iget v4, v3, Lcom/anythink/core/c/d$b;->n:I

    if-ne v4, v0, :cond_1

    .line 246
    invoke-direct {p0, v3}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;)V

    goto :goto_0

    .line 249
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 251
    :try_start_2
    iget-object v1, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 252
    :try_start_3
    iget-object v2, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    if-eqz v3, :cond_3

    .line 253
    iget v4, v3, Lcom/anythink/core/c/d$b;->n:I

    if-ne v4, v0, :cond_3

    .line 254
    invoke-direct {p0, v3}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;)V

    goto :goto_1

    .line 257
    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_1
    move-exception v0

    .line 249
    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 259
    :cond_5
    monitor-exit p0

    return-void

    .line 238
    :cond_6
    :goto_2
    monitor-exit p0

    return-void

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()V
    .locals 10

    const/4 v0, 0x1

    .line 1073
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    const/4 v0, 0x0

    .line 1074
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->k:Z

    .line 1076
    iget-object v0, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1078
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 1081
    :cond_0
    iget-object v2, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v4, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    iget-object v6, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/anythink/core/c/d;->F()I

    move-result v7

    iget-boolean v8, p0, Lcom/anythink/core/common/f;->s:Z

    iget v9, p0, Lcom/anythink/core/common/f;->d:I

    invoke-static/range {v2 .. v9}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v0

    .line 1082
    iget-object v1, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    invoke-static {v0, v1}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Lcom/anythink/core/api/AdError;)V

    .line 1084
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->M:Z

    if-nez v0, :cond_1

    .line 1085
    iget-object v0, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    invoke-virtual {p0, v0}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/api/AdError;)V

    .line 1089
    :cond_1
    invoke-virtual {p0}, Lcom/anythink/core/common/f;->b()V

    .line 1091
    invoke-direct {p0}, Lcom/anythink/core/common/f;->j()V

    return-void
.end method

.method private declared-synchronized i()Z
    .locals 3

    monitor-enter p0

    .line 1147
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFinishAllRequest:isFinishBidding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/anythink/core/common/f;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFinishAllRequest:requestWaitingPool: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFinishAllRequest:requestingPool: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFinishAllRequest:hbRequestWaitingPool: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    iget-object v0, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFinishAllRequest:hbRequestingPool: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private j()V
    .locals 2

    .line 1214
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    if-eqz v0, :cond_2

    .line 1215
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/anythink/core/common/f;->j:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->k:Z

    if-nez v0, :cond_2

    .line 1216
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/d;->b(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private k()V
    .locals 1

    const/4 v0, 0x1

    .line 1241
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->M:Z

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(D)V
    .locals 3

    const/4 v0, 0x1

    .line 1180
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->j:Z

    .line 1181
    iget-wide v0, p0, Lcom/anythink/core/common/f;->L:D

    cmpl-double v2, p1, v0

    if-lez v2, :cond_0

    .line 1182
    iput-wide p1, p0, Lcom/anythink/core/common/f;->L:D

    .line 1184
    :cond_0
    invoke-direct {p0}, Lcom/anythink/core/common/f;->j()V

    return-void
.end method

.method public abstract a(Lcom/anythink/core/api/ATBaseAdAdapter;)V
.end method

.method protected a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V
    .locals 1

    .line 1066
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/anythink/core/common/f;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/api/AdError;)V

    return-void
.end method

.method public final declared-synchronized a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/anythink/core/api/ATBaseAdAdapter;",
            "Ljava/util/List<",
            "+",
            "Lcom/anythink/core/api/BaseAd;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    if-eqz p1, :cond_0

    .line 816
    :try_start_0
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 818
    :goto_0
    iget-object v1, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 820
    invoke-direct {p0, v0}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 821
    monitor-exit p0

    return-void

    .line 824
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/anythink/core/common/f;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onAdLoaded: NetworkFirmId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    iget v4, v4, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "---content:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    iget-object v4, v4, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    .line 826
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    .line 5091
    iget-wide v4, v3, Lcom/anythink/core/c/d$b;->m:D

    .line 829
    iget-wide v6, p0, Lcom/anythink/core/common/f;->J:D

    cmpl-double v8, v4, v6

    if-lez v8, :cond_2

    .line 6091
    iget-wide v3, v3, Lcom/anythink/core/c/d$b;->m:D

    .line 830
    iput-wide v3, p0, Lcom/anythink/core/common/f;->J:D

    .line 833
    :cond_2
    iget-object v3, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 834
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-virtual {v2, v5, v6}, Lcom/anythink/core/common/d/d;->d(J)V

    .line 837
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkPlacementId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/d/d;->e(Ljava/lang/String;)V

    .line 839
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/anythink/core/common/f;->b(Lcom/anythink/core/c/d$b;)V

    .line 850
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    .line 6135
    iget-wide v3, v3, Lcom/anythink/core/c/d$b;->w:J

    const-wide/16 v5, -0x1

    cmp-long v7, v3, v5

    if-eqz v7, :cond_3

    .line 851
    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->D()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_3

    .line 852
    invoke-static {v2}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;)V

    .line 859
    :cond_3
    iget-object v3, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    .line 861
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 862
    iget-object v3, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    .line 865
    :goto_1
    iget-object v6, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Runnable;

    if-eqz v6, :cond_5

    .line 867
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    .line 868
    iget-object v3, p0, Lcom/anythink/core/common/f;->C:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    :cond_5
    if-eqz v3, :cond_6

    .line 6454
    iput v5, v2, Lcom/anythink/core/common/d/d;->o:I

    .line 876
    :cond_6
    invoke-direct {p0, v0, v2, v5}, Lcom/anythink/core/common/f;->a(Ljava/lang/String;Lcom/anythink/core/common/d/d;Z)V

    .line 878
    iget-wide v6, p0, Lcom/anythink/core/common/f;->L:D

    const-wide/16 v8, 0x0

    const/4 v3, 0x2

    cmpl-double v10, v6, v8

    if-lez v10, :cond_8

    .line 880
    iget-wide v6, p0, Lcom/anythink/core/common/f;->L:D

    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v8

    iget-wide v8, v8, Lcom/anythink/core/c/d$b;->m:D

    cmpg-double v10, v6, v8

    if-gez v10, :cond_7

    .line 6463
    iput v3, v2, Lcom/anythink/core/common/d/d;->p:I

    goto :goto_2

    .line 7463
    :cond_7
    iput v5, v2, Lcom/anythink/core/common/d/d;->p:I

    goto :goto_2

    .line 8463
    :cond_8
    iput v4, v2, Lcom/anythink/core/common/d/d;->p:I

    .line 889
    :goto_2
    iget-object v6, p0, Lcom/anythink/core/common/f;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/anythink/core/common/f/a;->a(Landroid/content/Context;)Lcom/anythink/core/common/f/a;

    move-result-object v6

    invoke-virtual {v6, v3, v2}, Lcom/anythink/core/common/f/a;->a(ILcom/anythink/core/common/d/aa;)V

    .line 890
    sget-object v3, Lcom/anythink/core/common/b/e$e;->b:Ljava/lang/String;

    sget-object v6, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    const-string v7, ""

    invoke-static {v2, v3, v6, v7}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v11

    .line 898
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v6

    iget-object v7, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/anythink/core/common/d/d;->t()I

    move-result v8

    move-object v9, p1

    move-object v10, p2

    invoke-virtual/range {v6 .. v12}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;ILcom/anythink/core/api/ATBaseAdAdapter;Ljava/util/List;J)Lcom/anythink/core/common/d/ab;

    .line 901
    iget-boolean p2, p0, Lcom/anythink/core/common/f;->M:Z

    if-nez p2, :cond_9

    iget-boolean p2, p0, Lcom/anythink/core/common/f;->j:Z

    if-nez p2, :cond_9

    .line 902
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object p2

    iget-object v2, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {p2, v2}, Lcom/anythink/core/common/o;->a(Ljava/lang/String;)Lcom/anythink/core/common/d;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 903
    iget-object p2, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {p2}, Lcom/anythink/core/c/d;->y()I

    move-result p2

    if-lez p2, :cond_9

    .line 904
    iget-object v11, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object p2

    iget-wide v9, p2, Lcom/anythink/core/c/d$b;->m:D

    .line 9131
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p2

    new-instance v2, Lcom/anythink/core/common/d$5;

    move-object v6, v2

    move-object v8, p1

    invoke-direct/range {v6 .. v11}, Lcom/anythink/core/common/d$5;-><init>(Lcom/anythink/core/common/d;Lcom/anythink/core/api/ATBaseAdAdapter;DLjava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;)V

    .line 910
    :cond_9
    iget p1, p0, Lcom/anythink/core/common/f;->y:I

    add-int/2addr p1, v5

    iput p1, p0, Lcom/anythink/core/common/f;->y:I

    if-eqz v0, :cond_a

    .line 913
    iget-object p1, p0, Lcom/anythink/core/common/f;->E:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    :cond_a
    iget-boolean p1, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez p1, :cond_b

    .line 917
    invoke-direct {p0, v4}, Lcom/anythink/core/common/f;->b(Z)V

    :cond_b
    if-eqz v1, :cond_c

    .line 923
    iget-object p1, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    iget-object p2, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-direct {p0, v5, p1, p2, v5}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 925
    :cond_c
    :try_start_2
    iget-object p1, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    iget-object p2, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-direct {p0, v5, p1, p2, v4}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 928
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract a(Lcom/anythink/core/api/AdError;)V
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1225
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 1226
    iput-object v1, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    goto :goto_0

    .line 1228
    :cond_0
    iput-object p1, p0, Lcom/anythink/core/common/f;->e:Ljava/lang/String;

    .line 1231
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1232
    iput-object v1, p0, Lcom/anythink/core/common/f;->f:Ljava/lang/String;

    return-void

    .line 1234
    :cond_1
    iput-object p2, p0, Lcom/anythink/core/common/f;->f:Ljava/lang/String;

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/util/List;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;ZI)V"
        }
    .end annotation

    .line 566
    iput-boolean p5, p0, Lcom/anythink/core/common/f;->g:Z

    .line 568
    iput-boolean p5, p0, Lcom/anythink/core/common/f;->G:Z

    .line 569
    iget-object p5, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    invoke-interface {p5, p4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 570
    iput-object p2, p0, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    .line 571
    iput-object p1, p0, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    .line 572
    iput-object p3, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    const-string p1, ""

    .line 573
    iput-object p1, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    .line 574
    iput p6, p0, Lcom/anythink/core/common/f;->d:I

    .line 576
    iput-object p4, p0, Lcom/anythink/core/common/f;->N:Ljava/util/List;

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 578
    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p5

    if-ge p2, p5, :cond_1

    if-lez p2, :cond_0

    .line 580
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p6, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, ","

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    iput-object p5, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    .line 582
    :cond_0
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Lcom/anythink/core/c/d$b;

    iget p6, p6, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 583
    new-instance p6, Ljava/lang/StringBuilder;

    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    iput-object p5, p0, Lcom/anythink/core/common/f;->t:Ljava/lang/String;

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 586
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p5

    iput-wide p5, p0, Lcom/anythink/core/common/f;->v:J

    .line 589
    iget-object p2, p0, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {p2}, Lcom/anythink/core/c/d;->w()J

    move-result-wide p5

    .line 3162
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object p2

    iget-object v0, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    invoke-virtual {p2, v0, p5, p6}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/Runnable;J)V

    .line 591
    invoke-virtual {p3}, Lcom/anythink/core/c/d;->F()I

    move-result p2

    iget-object p3, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    iget-object p5, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-direct {p0, p2, p3, p5, p1}, Lcom/anythink/core/common/f;->a(ILjava/util/List;Ljava/util/List;Z)V

    .line 593
    invoke-direct {p0, p4}, Lcom/anythink/core/common/f;->b(Ljava/util/List;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v1, p0

    .line 410
    iget-object v2, v1, Lcom/anythink/core/common/f;->z:Ljava/lang/Object;

    monitor-enter v2

    .line 411
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 412
    monitor-exit v2

    return-void

    :cond_0
    const-wide/16 v3, 0x0

    .line 417
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 420
    iget-object v5, v1, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-lez v5, :cond_1

    .line 421
    iget-object v5, v1, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    iget-object v8, v1, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v7

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/anythink/core/c/d$b;

    goto :goto_0

    :cond_1
    move-object v5, v6

    .line 424
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 425
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 428
    iget-boolean v10, v1, Lcom/anythink/core/common/f;->h:Z

    if-eqz v10, :cond_7

    .line 432
    iget-boolean v5, v1, Lcom/anythink/core/common/f;->k:Z

    if-eqz v5, :cond_2

    .line 434
    invoke-static {}, Lcom/anythink/core/common/a;->a()Lcom/anythink/core/common/a;

    move-result-object v5

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v6

    iget-object v14, v1, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    invoke-virtual {v5, v6, v14}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;

    move-result-object v6

    const/4 v11, 0x2

    goto :goto_1

    :cond_2
    const/4 v11, 0x3

    .line 439
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 441
    invoke-direct {v1, v3}, Lcom/anythink/core/common/f;->c(Lcom/anythink/core/c/d$b;)V

    .line 443
    iget-wide v14, v3, Lcom/anythink/core/c/d$b;->s:J

    .line 449
    iget-boolean v4, v1, Lcom/anythink/core/common/f;->k:Z

    if-eqz v4, :cond_5

    if-eqz v6, :cond_4

    .line 451
    invoke-virtual {v6}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    move/from16 v17, v11

    iget-wide v10, v4, Lcom/anythink/core/c/d$b;->m:D

    .line 452
    iget-wide v12, v3, Lcom/anythink/core/c/d$b;->m:D

    invoke-virtual {v6}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v4

    move-object/from16 v18, v8

    iget-wide v7, v4, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double v4, v12, v7

    if-lez v4, :cond_3

    .line 453
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v7, v18

    const/4 v4, 0x1

    goto :goto_4

    :cond_3
    move-object/from16 v7, v18

    .line 457
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x3

    goto :goto_4

    :cond_4
    move-object v7, v8

    move/from16 v17, v11

    .line 461
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x1

    goto :goto_3

    :cond_5
    move-object v7, v8

    move/from16 v17, v11

    .line 466
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x3

    :goto_3
    const-wide/16 v10, 0x0

    .line 470
    :goto_4
    invoke-static {v0, v3, v10, v11, v4}, Lcom/anythink/core/common/f;->a(Lorg/json/JSONArray;Lcom/anythink/core/c/d$b;DI)V

    move-object v8, v7

    move-wide v3, v14

    move/from16 v11, v17

    const/4 v7, 0x1

    goto :goto_2

    :cond_6
    move-object v7, v8

    move/from16 v17, v11

    goto :goto_a

    :cond_7
    move-object v7, v8

    .line 478
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 480
    invoke-direct {v1, v3}, Lcom/anythink/core/common/f;->c(Lcom/anythink/core/c/d$b;)V

    .line 482
    iget-wide v12, v3, Lcom/anythink/core/c/d$b;->s:J

    if-eqz v5, :cond_a

    .line 483
    iget-wide v14, v3, Lcom/anythink/core/c/d$b;->m:D

    move-wide/from16 v16, v12

    iget-wide v11, v5, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double v4, v14, v11

    if-lez v4, :cond_8

    goto :goto_7

    .line 489
    :cond_8
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_9

    .line 491
    iget-wide v10, v5, Lcom/anythink/core/c/d$b;->m:D

    goto :goto_6

    :cond_9
    const-wide/16 v10, 0x0

    :goto_6
    const/4 v4, 0x2

    goto :goto_9

    :cond_a
    move-wide/from16 v16, v12

    .line 484
    :goto_7
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_b

    .line 486
    iget-wide v10, v5, Lcom/anythink/core/c/d$b;->m:D

    goto :goto_8

    :cond_b
    const-wide/16 v10, 0x0

    :goto_8
    const/4 v4, 0x1

    .line 495
    :goto_9
    invoke-static {v0, v3, v10, v11, v4}, Lcom/anythink/core/common/f;->a(Lorg/json/JSONArray;Lcom/anythink/core/c/d$b;DI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide/from16 v3, v16

    goto :goto_5

    :cond_c
    const/4 v11, 0x1

    .line 501
    :goto_a
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v6, "load_status"

    .line 502
    invoke-virtual {v5, v6, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v6, "bid_time"

    .line 503
    invoke-virtual {v5, v6, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v3, "result_list"

    .line 504
    invoke-virtual {v5, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 505
    iget-object v0, v1, Lcom/anythink/core/common/f;->q:Ljava/lang/String;

    iget-object v3, v1, Lcom/anythink/core/common/f;->r:Ljava/lang/String;

    iget-object v4, v1, Lcom/anythink/core/common/f;->p:Lcom/anythink/core/c/d;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/anythink/core/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 514
    :catch_0
    :try_start_2
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 515
    iget-object v4, v1, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct {v1, v4, v3, v5}, Lcom/anythink/core/common/f;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;Z)V

    goto :goto_b

    .line 519
    :cond_d
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 520
    iget-object v4, v1, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v3, v5}, Lcom/anythink/core/common/f;->a(Ljava/util/List;Lcom/anythink/core/c/d$b;Z)V

    goto :goto_c

    .line 522
    :cond_e
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public final a(Z)V
    .locals 0

    .line 1166
    iput-boolean p1, p0, Lcom/anythink/core/common/f;->s:Z

    return-void
.end method

.method public abstract b()V
.end method

.method public final declared-synchronized b(Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 4

    monitor-enter p0

    .line 783
    :try_start_0
    invoke-virtual {p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object p1

    .line 784
    iget-object v0, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 785
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/anythink/core/common/d/d;->c(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 786
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected final c()V
    .locals 10

    .line 121
    monitor-enter p0

    const/4 v0, 0x1

    .line 122
    :try_start_0
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->i:Z

    .line 125
    iget-object v1, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 126
    iget-object v3, p0, Lcom/anythink/core/common/f;->E:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v3, p0, Lcom/anythink/core/common/f;->D:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/common/f$b;

    .line 128
    iget-object v4, p0, Lcom/anythink/core/common/f;->A:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 129
    iget-boolean v4, v3, Lcom/anythink/core/common/f$b;->b:Z

    if-nez v4, :cond_0

    .line 130
    iget-object v4, v3, Lcom/anythink/core/common/f$b;->a:Lcom/anythink/core/common/d/d;

    const/4 v5, 0x2

    .line 1454
    iput v5, v4, Lcom/anythink/core/common/d/d;->o:I

    const-string v5, "2001"

    const-string v6, ""

    const-string v7, ""

    .line 132
    invoke-static {v5, v6, v7}, Lcom/anythink/core/api/ErrorCode;->getErrorCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/api/AdError;

    move-result-object v5

    .line 133
    iget-object v6, p0, Lcom/anythink/core/common/f;->u:Lcom/anythink/core/api/AdError;

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->B()I

    move-result v8

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->H()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/anythink/core/api/AdError;->putNetworkErrorMsg(Ljava/lang/String;ILjava/lang/String;Lcom/anythink/core/api/AdError;)V

    .line 135
    iput-boolean v0, v3, Lcom/anythink/core/common/f$b;->b:Z

    .line 137
    iget-object v3, v3, Lcom/anythink/core/common/f$b;->a:Lcom/anythink/core/common/d/d;

    sget-object v6, Lcom/anythink/core/common/b/e$e;->b:Ljava/lang/String;

    sget-object v7, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v7, v8}, Lcom/anythink/core/common/g/g;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_1

    .line 139
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v6, v2

    goto :goto_1

    :cond_1
    const-wide/16 v6, 0x0

    :goto_1
    invoke-static {v4, v0, v5, v6, v7}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILcom/anythink/core/api/AdError;J)V

    goto :goto_0

    .line 144
    :cond_2
    iget-boolean v1, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez v1, :cond_3

    .line 145
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    .line 146
    invoke-direct {p0}, Lcom/anythink/core/common/f;->h()V

    .line 152
    :cond_3
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->G:Z

    .line 153
    iput-boolean v0, p0, Lcom/anythink/core/common/f;->H:Z

    .line 154
    invoke-direct {p0}, Lcom/anythink/core/common/f;->g()V

    .line 159
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .line 1171
    iget-boolean v0, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/anythink/core/common/f;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/anythink/core/common/f;->B:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 3

    .line 1191
    iget-object v0, p0, Lcom/anythink/core/common/f;->z:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    .line 1192
    :try_start_0
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->g:Z

    .line 1194
    iget-boolean v2, p0, Lcom/anythink/core/common/f;->h:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/anythink/core/common/f;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1195
    invoke-direct {p0}, Lcom/anythink/core/common/f;->h()V

    .line 1199
    :cond_0
    iget-object v2, p0, Lcom/anythink/core/common/f;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1200
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->G:Z

    .line 1203
    :cond_1
    iget-object v2, p0, Lcom/anythink/core/common/f;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 1204
    iput-boolean v1, p0, Lcom/anythink/core/common/f;->H:Z

    .line 1207
    :cond_2
    invoke-direct {p0}, Lcom/anythink/core/common/f;->g()V

    .line 1209
    invoke-direct {p0}, Lcom/anythink/core/common/f;->j()V

    .line 1210
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public f()V
    .locals 2

    .line 1245
    iget-object v0, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1247
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/common/f;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/anythink/core/common/b/g;->c(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
