.class public final Lcom/anythink/core/common/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/anythink/core/common/a;


# instance fields
.field private b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/common/d/ab;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public static declared-synchronized a()Lcom/anythink/core/common/a;
    .locals 2

    const-class v0, Lcom/anythink/core/common/a;

    monitor-enter v0

    .line 53
    :try_start_0
    sget-object v1, Lcom/anythink/core/common/a;->a:Lcom/anythink/core/common/a;

    if-nez v1, :cond_0

    .line 54
    new-instance v1, Lcom/anythink/core/common/a;

    invoke-direct {v1}, Lcom/anythink/core/common/a;-><init>()V

    sput-object v1, Lcom/anythink/core/common/a;->a:Lcom/anythink/core/common/a;

    .line 56
    :cond_0
    sget-object v1, Lcom/anythink/core/common/a;->a:Lcom/anythink/core/common/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/c/d$b;I)V
    .locals 12

    move-object v0, p0

    move-object/from16 v1, p4

    .line 456
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v2

    move-object v4, p2

    invoke-virtual {v2, p2}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v11, 0x0

    aput v11, v3, v11

    const-string v5, "ofm_tid_key"

    .line 459
    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 461
    :try_start_0
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    :catchall_0
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    aget v10, v3, v11

    const-string v5, ""

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v3 .. v10}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v2

    move/from16 v3, p5

    .line 467
    invoke-static {p0, v2, v1, v3}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)Lcom/anythink/core/common/d/d;

    const/4 v1, 0x3

    .line 3445
    iput v1, v2, Lcom/anythink/core/common/d/d;->n:I

    .line 470
    invoke-virtual {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/anythink/core/common/d/d;->e(Ljava/lang/String;)V

    .line 471
    invoke-virtual {p0, v11}, Lcom/anythink/core/api/ATBaseAdAdapter;->setRefresh(Z)V

    return-void
.end method

.method private static a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V
    .locals 2

    .line 475
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "priority"

    .line 477
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "unit_id"

    .line 478
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "nw_firm_id"

    .line 479
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "nw_ver"

    .line 480
    invoke-virtual {v0, p1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "result"

    if-eqz p5, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 481
    :goto_0
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const/4 p1, -0x1

    if-eq p6, p1, :cond_1

    const-string p1, "reason"

    .line 483
    invoke-virtual {v0, p1, p6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 487
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 490
    :cond_1
    :goto_1
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILcom/anythink/core/api/ATBaseAdAdapter;Ljava/util/List;J)Lcom/anythink/core/common/d/ab;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/anythink/core/api/ATBaseAdAdapter;",
            "Ljava/util/List<",
            "+",
            "Lcom/anythink/core/api/BaseAd;",
            ">;J)",
            "Lcom/anythink/core/common/d/ab;"
        }
    .end annotation

    .line 76
    monitor-enter p0

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 79
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v1

    .line 81
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v2

    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 84
    iget-object v3, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/common/d/ab;

    if-nez v3, :cond_1

    .line 91
    new-instance v3, Lcom/anythink/core/common/d/ab;

    invoke-direct {v3}, Lcom/anythink/core/common/d/ab;-><init>()V

    .line 92
    iput p2, v3, Lcom/anythink/core/common/d/ab;->a:I

    .line 93
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/anythink/core/common/d/ab;->b:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 96
    :cond_1
    iput p2, v3, Lcom/anythink/core/common/d/ab;->a:I

    .line 97
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/anythink/core/common/d/ab;->b:Ljava/lang/String;

    .line 100
    :goto_0
    invoke-virtual {v3}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 102
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/anythink/core/common/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 103
    monitor-exit p0

    return-object v3

    :cond_2
    if-eqz p4, :cond_4

    .line 106
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_4

    .line 107
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 108
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_1
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/api/BaseAd;

    .line 109
    new-instance v2, Lcom/anythink/core/common/d/b;

    invoke-direct {v2}, Lcom/anythink/core/common/d/b;-><init>()V

    .line 110
    invoke-virtual {v2, p2}, Lcom/anythink/core/common/d/b;->b(I)V

    .line 111
    invoke-virtual {v2, p3}, Lcom/anythink/core/common/d/b;->a(Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 112
    invoke-virtual {v2, v0}, Lcom/anythink/core/common/d/b;->a(Lcom/anythink/core/api/BaseAd;)V

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/anythink/core/common/d/b;->c(J)V

    .line 114
    invoke-virtual {v2, p5, p6}, Lcom/anythink/core/common/d/b;->b(J)V

    .line 115
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/anythink/core/common/d/b;->a(Ljava/lang/String;)V

    .line 116
    iget-wide v4, v1, Lcom/anythink/core/c/d$b;->v:J

    invoke-virtual {v2, v4, v5}, Lcom/anythink/core/common/d/b;->a(J)V

    .line 118
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 120
    :cond_3
    invoke-virtual {v3, p1}, Lcom/anythink/core/common/d/ab;->a(Ljava/util/List;)V

    goto :goto_2

    .line 122
    :cond_4
    new-instance p1, Lcom/anythink/core/common/d/b;

    invoke-direct {p1}, Lcom/anythink/core/common/d/b;-><init>()V

    .line 123
    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/b;->b(I)V

    .line 124
    invoke-virtual {p1, p3}, Lcom/anythink/core/common/d/b;->a(Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lcom/anythink/core/common/d/b;->c(J)V

    .line 126
    invoke-virtual {p1, p5, p6}, Lcom/anythink/core/common/d/b;->b(J)V

    .line 127
    invoke-virtual {p3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object p2

    invoke-virtual {p2}, Lcom/anythink/core/common/d/d;->K()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/b;->a(Ljava/lang/String;)V

    .line 128
    iget-wide p2, v1, Lcom/anythink/core/c/d$b;->v:J

    invoke-virtual {p1, p2, p3}, Lcom/anythink/core/common/d/b;->a(J)V

    .line 130
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 131
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-virtual {v3, p2}, Lcom/anythink/core/common/d/ab;->a(Ljava/util/List;)V

    .line 135
    :goto_2
    monitor-exit p0

    return-object v3

    :catchall_0
    move-exception p1

    .line 138
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Lcom/anythink/core/common/d/ab;
    .locals 6

    .line 631
    iget v0, p2, Lcom/anythink/core/c/d$b;->b:I

    const/4 v1, 0x0

    const/16 v2, 0x42

    if-ne v0, v2, :cond_1

    .line 632
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v0

    iget-object v2, p2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v3, p2, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v0, v2, v3}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 633
    invoke-virtual {v0}, Lcom/anythink/core/common/d/l;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-object v1

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz p1, :cond_3

    .line 639
    iget-object p2, p2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/d/ab;

    if-eqz p1, :cond_2

    .line 640
    invoke-virtual {p1}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object p2

    goto :goto_0

    :cond_2
    move-object p2, v1

    :goto_0
    if-eqz p2, :cond_3

    .line 641
    invoke-virtual {p2}, Lcom/anythink/core/common/d/b;->f()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/anythink/core/common/d/b;->c()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long p2, v2, v4

    if-lez p2, :cond_3

    return-object p1

    :cond_3
    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/common/d/b;
    .locals 1

    .line 149
    monitor-enter p0

    const/4 v0, 0x0

    .line 150
    :try_start_0
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/anythink/core/common/a;->a(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/anythink/core/common/d/b;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 151
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/anythink/core/common/d/b;
    .locals 32

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    move-object/from16 v9, p2

    .line 164
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 165
    invoke-static {}, Landroid/os/Looper;->prepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :catchall_0
    :cond_0
    new-instance v17, Lorg/json/JSONArray;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONArray;-><init>()V

    .line 171
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 173
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 174
    monitor-enter p0

    .line 175
    :try_start_1
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v5

    const/16 v18, 0x0

    if-nez v5, :cond_1

    .line 178
    monitor-exit p0

    return-object v18

    .line 181
    :cond_1
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/anythink/core/common/p;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 182
    invoke-static {}, Lcom/anythink/core/common/p;->a()Lcom/anythink/core/common/p;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/anythink/core/common/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static/range {p1 .. p1}, Lcom/anythink/core/common/g/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    move-object v4, v2

    if-eqz v1, :cond_3

    .line 186
    invoke-interface {v6, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 189
    :cond_3
    iget-object v1, v8, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    .line 194
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/anythink/core/common/b/g;->m()Ljava/lang/String;

    .line 195
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/anythink/core/common/b/g;->e(Ljava/lang/String;)Ljava/lang/String;

    .line 198
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    const/4 v15, 0x1

    const/16 v19, 0x0

    if-lez v1, :cond_1f

    const/4 v1, 0x0

    .line 201
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_1f

    .line 202
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    move-object v14, v10

    check-cast v14, Lcom/anythink/core/c/d$b;

    .line 203
    iget v10, v14, Lcom/anythink/core/c/d$b;->b:I

    const/16 v11, 0x23

    if-ne v10, v11, :cond_4

    .line 204
    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_4
    iget v10, v14, Lcom/anythink/core/c/d$b;->a:I

    if-gez v10, :cond_5

    iget v10, v14, Lcom/anythink/core/c/d$b;->a:I

    move v13, v10

    goto :goto_1

    :cond_5
    move v13, v1

    .line 209
    :goto_1
    invoke-static {}, Lcom/anythink/core/a/c;->a()Lcom/anythink/core/a/c;

    move-result-object v10

    invoke-virtual {v10, v9, v14}, Lcom/anythink/core/a/c;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 210
    iget-object v12, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v14, v14, Lcom/anythink/core/c/d$b;->b:I

    const-string v16, ""

    const/16 v20, 0x0

    const/16 v21, 0x3

    move-object/from16 v10, v17

    move v11, v13

    move v13, v14

    move-object/from16 v14, v16

    move/from16 v15, v20

    move/from16 v16, v21

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    :goto_2
    move/from16 v20, v1

    move-object/from16 v21, v3

    move-object/from16 v30, v5

    move-object/from16 v31, v6

    const/4 v1, 0x2

    move-object v6, v4

    goto/16 :goto_f

    .line 215
    :cond_6
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/anythink/core/a/a;->a(Landroid/content/Context;)Lcom/anythink/core/a/a;

    move-result-object v10

    invoke-virtual {v10, v9, v14}, Lcom/anythink/core/a/a;->a(Ljava/lang/String;Lcom/anythink/core/c/d$b;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 216
    iget-object v12, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v14, v14, Lcom/anythink/core/c/d$b;->b:I

    const-string v15, ""

    const/16 v16, 0x0

    const/16 v20, 0x2

    move-object/from16 v10, v17

    move v11, v13

    move v13, v14

    move-object v14, v15

    move/from16 v15, v16

    move/from16 v16, v20

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    goto :goto_2

    .line 224
    :cond_7
    iget v10, v14, Lcom/anythink/core/c/d$b;->b:I

    const/16 v11, 0x42

    if-ne v10, v11, :cond_9

    .line 225
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v10

    iget-object v11, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v12, v14, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v10, v11, v12}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 226
    invoke-virtual {v10}, Lcom/anythink/core/common/d/l;->a()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 227
    :cond_8
    iget-object v12, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v14, v14, Lcom/anythink/core/c/d$b;->b:I

    const-string v15, ""

    const/16 v16, 0x0

    const/16 v20, 0x5

    move-object/from16 v10, v17

    move v11, v13

    move v13, v14

    move-object v14, v15

    move/from16 v15, v16

    move/from16 v16, v20

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    goto :goto_2

    :cond_9
    if-eqz v3, :cond_a

    .line 233
    iget-object v10, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/anythink/core/common/d/ab;

    goto :goto_3

    :cond_a
    move-object/from16 v10, v18

    :goto_3
    if-eqz v10, :cond_b

    .line 235
    invoke-virtual {v10}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v11

    move-object/from16 v16, v11

    goto :goto_4

    :cond_b
    move-object/from16 v16, v18

    :goto_4
    if-eqz v10, :cond_13

    if-nez v16, :cond_c

    goto/16 :goto_8

    .line 312
    :cond_c
    invoke-virtual {v5}, Lcom/anythink/core/c/d;->C()I

    move-result v10

    const-string v11, "0"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v10, v11, :cond_e

    .line 313
    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v10

    if-eqz v10, :cond_d

    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->h()Lcom/anythink/core/api/BaseAd;

    move-result-object v10

    if-eqz v10, :cond_d

    :goto_5
    const/4 v15, 0x1

    goto :goto_6

    :cond_d
    const/4 v15, 0x0

    goto :goto_6

    .line 315
    :cond_e
    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v10

    if-eqz v10, :cond_d

    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/anythink/core/api/ATBaseAdAdapter;->isAdReady()Z

    move-result v10

    if-eqz v10, :cond_d

    goto :goto_5

    :goto_6
    if-eqz v15, :cond_12

    .line 320
    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->f()J

    move-result-wide v10

    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->c()J

    move-result-wide v20

    add-long v10, v10, v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    cmp-long v12, v10, v20

    if-lez v12, :cond_11

    .line 322
    invoke-virtual/range {v16 .. v16}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    .line 324
    iget-object v11, v14, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v12, v14, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v15, -0x1

    move-object/from16 v9, v17

    move v10, v13

    move v3, v13

    move-object v13, v1

    move-object v1, v14

    move v14, v2

    invoke-static/range {v9 .. v15}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    .line 326
    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    .line 327
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/anythink/core/common/d/d;->p(Ljava/lang/String;)V

    .line 329
    invoke-virtual {v2, v3}, Lcom/anythink/core/common/d/d;->j(I)V

    if-eqz p3, :cond_10

    const/16 v20, 0x1

    const/16 v21, -0x1

    .line 331
    iget-object v5, v1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v1, v1, Lcom/anythink/core/c/d$b;->b:I

    .line 332
    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v26

    .line 2449
    iget v0, v2, Lcom/anythink/core/common/d/d;->n:I

    const/4 v6, 0x3

    if-ne v0, v6, :cond_f

    const/16 v28, 0x1

    goto :goto_7

    :cond_f
    const/16 v28, 0x0

    :goto_7
    const-string v29, ""

    move-object/from16 v19, v2

    move/from16 v22, v3

    move-object/from16 v23, v5

    move/from16 v24, v1

    move-object/from16 v27, v4

    .line 331
    invoke-static/range {v19 .. v29}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 336
    :cond_10
    monitor-exit p0

    return-object v16

    :cond_11
    move v11, v13

    move-object v10, v14

    .line 338
    iget-object v12, v10, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v13, v10, Lcom/anythink/core/c/d$b;->b:I

    const-string v14, ""

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v10, v17

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    goto/16 :goto_2

    :cond_12
    move v11, v13

    move-object v10, v14

    .line 342
    iget-object v12, v10, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v13, v10, Lcom/anythink/core/c/d$b;->b:I

    const-string v14, ""

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, v17

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    goto/16 :goto_2

    :cond_13
    :goto_8
    move v11, v13

    move-object v10, v14

    .line 2203
    iget v12, v10, Lcom/anythink/core/c/d$b;->G:I

    if-ne v12, v2, :cond_14

    const/4 v15, 0x1

    goto :goto_9

    :cond_14
    const/4 v15, 0x0

    :goto_9
    if-eqz v15, :cond_15

    .line 243
    invoke-static {v10}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v12

    goto :goto_a

    :cond_15
    move-object/from16 v12, v18

    :goto_a
    if-eqz v12, :cond_1e

    .line 247
    invoke-virtual {v5, v9, v4, v10}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;

    move-result-object v13

    .line 248
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v14

    invoke-virtual {v14, v9}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 251
    :try_start_2
    invoke-virtual {v12, v0, v13, v14}, Lcom/anythink/core/api/ATBaseAdAdapter;->internalInitNetworkObjectByPlacementId(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v13, :cond_16

    move/from16 v20, v1

    move-object v1, v12

    const/4 v15, 0x2

    move-object v2, v4

    move-object/from16 v21, v3

    move-object/from16 v3, p2

    move-object v14, v4

    move-object v4, v5

    move-object/from16 v30, v5

    move-object v5, v10

    move-object/from16 v31, v6

    move v6, v11

    .line 253
    :try_start_3
    invoke-static/range {v1 .. v6}, Lcom/anythink/core/common/a;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/c/d$b;I)V

    goto :goto_b

    :cond_16
    move/from16 v20, v1

    move-object/from16 v21, v3

    move-object v14, v4

    move-object/from16 v30, v5

    move-object/from16 v31, v6

    const/4 v15, 0x2

    .line 257
    :goto_b
    invoke-virtual/range {v30 .. v30}, Lcom/anythink/core/c/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_17

    if-eqz v13, :cond_19

    .line 258
    invoke-virtual {v12, v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getBaseAdObject(Landroid/content/Context;)Lcom/anythink/core/api/BaseAd;

    move-result-object v1

    if-eqz v1, :cond_1a

    const/4 v2, 0x1

    goto :goto_d

    :cond_17
    if-eqz v13, :cond_18

    .line 260
    invoke-virtual {v12}, Lcom/anythink/core/api/ATBaseAdAdapter;->isAdReady()Z

    move-result v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move v2, v1

    goto :goto_c

    :cond_18
    const/4 v2, 0x0

    :goto_c
    move-object/from16 v1, v18

    goto :goto_d

    :catchall_1
    move/from16 v20, v1

    move-object/from16 v21, v3

    move-object v14, v4

    move-object/from16 v30, v5

    move-object/from16 v31, v6

    const/4 v15, 0x2

    :catchall_2
    :cond_19
    move-object/from16 v1, v18

    :cond_1a
    const/4 v2, 0x0

    :goto_d
    if-eqz v2, :cond_1d

    if-eqz v1, :cond_1b

    .line 283
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 284
    invoke-virtual {v12}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/api/BaseAd;->setTrackingInfo(Lcom/anythink/core/common/d/d;)V

    .line 285
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v0

    goto :goto_e

    :cond_1b
    move-object/from16 v5, v18

    .line 287
    :goto_e
    invoke-virtual {v10}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move v3, v11

    move-object v4, v12

    invoke-virtual/range {v1 .. v7}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;ILcom/anythink/core/api/ATBaseAdAdapter;Ljava/util/List;J)Lcom/anythink/core/common/d/ab;

    move-result-object v0

    .line 289
    invoke-virtual {v12}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    .line 290
    invoke-virtual {v1, v11}, Lcom/anythink/core/common/d/d;->j(I)V

    if-eqz p3, :cond_1c

    const/16 v20, 0x1

    const/16 v21, -0x1

    .line 294
    iget-object v2, v10, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v3, v10, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v12}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x1

    const-string v29, ""

    move-object/from16 v19, v1

    move/from16 v22, v11

    move-object/from16 v23, v2

    move/from16 v24, v3

    move-object/from16 v27, v14

    invoke-static/range {v19 .. v29}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 297
    :cond_1c
    invoke-virtual {v0}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 299
    :cond_1d
    iget-object v12, v10, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v13, v10, Lcom/anythink/core/c/d$b;->b:I

    const-string v1, ""

    const/4 v2, 0x0

    const/16 v16, 0x4

    move-object/from16 v10, v17

    move-object v6, v14

    move-object v14, v1

    const/4 v1, 0x2

    move v15, v2

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    goto :goto_f

    :cond_1e
    move/from16 v20, v1

    move-object/from16 v21, v3

    move-object/from16 v30, v5

    move-object/from16 v31, v6

    const/4 v1, 0x2

    move-object v6, v4

    .line 304
    iget-object v12, v10, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v13, v10, Lcom/anythink/core/c/d$b;->b:I

    const-string v14, ""

    const/4 v15, 0x0

    const/16 v16, 0x4

    move-object/from16 v10, v17

    invoke-static/range {v10 .. v16}, Lcom/anythink/core/common/a;->a(Lorg/json/JSONArray;ILjava/lang/String;ILjava/lang/String;ZI)V

    :goto_f
    add-int/lit8 v2, v20, 0x1

    move v1, v2

    move-object v4, v6

    move-object/from16 v3, v21

    move-object/from16 v5, v30

    move-object/from16 v6, v31

    const/4 v2, 0x2

    const/4 v15, 0x1

    goto/16 :goto_0

    :cond_1f
    move-object/from16 v30, v5

    move-object/from16 v31, v6

    const/4 v1, 0x2

    move-object v6, v4

    .line 350
    invoke-virtual/range {v30 .. v30}, Lcom/anythink/core/c/d;->v()I

    move-result v2

    const/4 v10, 0x1

    if-ne v2, v10, :cond_20

    const/4 v15, 0x1

    goto :goto_10

    .line 352
    :cond_20
    invoke-virtual/range {v30 .. v30}, Lcom/anythink/core/c/d;->v()I

    move-result v2

    if-ne v2, v1, :cond_21

    move/from16 v15, p4

    goto :goto_10

    :cond_21
    const/4 v15, 0x0

    .line 357
    :goto_10
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2a

    if-eqz v15, :cond_2a

    .line 358
    invoke-static {}, Lcom/anythink/core/common/m;->a()Lcom/anythink/core/common/m;

    move-result-object v1

    invoke-virtual {v1, v0, v9}, Lcom/anythink/core/common/m;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 361
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 362
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 363
    iget-object v4, v3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    if-eqz v4, :cond_22

    iget-object v4, v3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_22

    move-object v11, v3

    goto :goto_11

    :cond_23
    move-object/from16 v11, v18

    :goto_11
    if-eqz v11, :cond_2a

    move-object/from16 v12, v30

    .line 371
    invoke-virtual {v12, v9, v6, v11}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "isDefaultOffer"

    .line 372
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v2, v31

    .line 373
    invoke-interface {v2, v11}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 375
    :try_start_5
    invoke-static {v11}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v14

    .line 376
    invoke-static {}, Lcom/anythink/core/common/o;->a()Lcom/anythink/core/common/o;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/anythink/core/common/o;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v14, v0, v1, v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->initNetworkObjectByPlacementId(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    if-eqz v7, :cond_24

    move-object v1, v14

    move-object v2, v6

    move-object/from16 v3, p2

    move-object v4, v12

    move-object v5, v11

    move-object v15, v6

    move v6, v13

    .line 379
    :try_start_6
    invoke-static/range {v1 .. v6}, Lcom/anythink/core/common/a;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Lcom/anythink/core/c/d$b;I)V

    goto :goto_12

    :cond_24
    move-object v15, v6

    .line 384
    :goto_12
    invoke-virtual {v12}, Lcom/anythink/core/c/d;->C()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_25

    if-eqz v7, :cond_26

    .line 385
    invoke-virtual {v14, v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getBaseAdObject(Landroid/content/Context;)Lcom/anythink/core/api/BaseAd;

    move-result-object v0

    if-eqz v0, :cond_27

    const/16 v19, 0x1

    goto :goto_13

    :cond_25
    if-eqz v7, :cond_26

    .line 387
    invoke-virtual {v14}, Lcom/anythink/core/api/ATBaseAdAdapter;->isAdReady()Z

    move-result v19

    :cond_26
    move-object/from16 v0, v18

    :cond_27
    :goto_13
    if-eqz v19, :cond_2b

    if-eqz v0, :cond_28

    .line 394
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 395
    invoke-virtual {v14}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/anythink/core/api/BaseAd;->setTrackingInfo(Lcom/anythink/core/common/d/d;)V

    .line 396
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v1

    goto :goto_14

    :cond_28
    move-object/from16 v5, v18

    .line 399
    :goto_14
    invoke-virtual {v11}, Lcom/anythink/core/c/d$b;->a()J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move v3, v13

    move-object v4, v14

    invoke-virtual/range {v1 .. v7}, Lcom/anythink/core/common/a;->a(Ljava/lang/String;ILcom/anythink/core/api/ATBaseAdAdapter;Ljava/util/List;J)Lcom/anythink/core/common/d/ab;

    move-result-object v0

    .line 401
    invoke-virtual {v14}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    .line 3293
    iput v10, v1, Lcom/anythink/core/common/d/d;->w:I

    .line 404
    invoke-virtual {v1, v13}, Lcom/anythink/core/common/d/d;->j(I)V

    .line 405
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/d;->p(Ljava/lang/String;)V

    if-eqz p3, :cond_29

    const/16 v20, 0x1

    const/16 v21, -0x1

    .line 408
    iget-object v2, v11, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v3, v11, Lcom/anythink/core/c/d$b;->b:I

    .line 409
    invoke-virtual {v14}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x1

    iget-object v4, v11, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    move-object/from16 v19, v1

    move/from16 v22, v13

    move-object/from16 v23, v2

    move/from16 v24, v3

    move-object/from16 v27, v15

    move-object/from16 v29, v4

    .line 408
    invoke-static/range {v19 .. v29}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 412
    :cond_29
    invoke-virtual {v0}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    monitor-exit p0

    return-object v0

    :catchall_3
    nop

    goto :goto_15

    :catchall_4
    move-object v15, v6

    goto :goto_15

    :cond_2a
    move-object v15, v6

    move-object/from16 v12, v30

    :cond_2b
    :goto_15
    if-eqz p3, :cond_2c

    const-string v2, ""

    const-string v4, ""

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v15

    move-object/from16 v1, p2

    move-object v3, v12

    .line 422
    invoke-static/range {v0 .. v7}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v0

    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v22, -0x1

    const-string v23, ""

    const/16 v24, -0x1

    const-string v25, ""

    .line 424
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v26

    const/16 v28, 0x0

    const-string v29, ""

    move-object/from16 v19, v0

    move-object/from16 v27, v15

    .line 423
    invoke-static/range {v19 .. v29}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ZIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    if-eqz p4, :cond_2c

    .line 426
    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v10, v1, v15}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;ILjava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_2c
    monitor-exit p0

    return-object v18

    :catchall_5
    move-exception v0

    .line 432
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/anythink/core/common/d/b;)V
    .locals 9

    .line 589
    monitor-enter p0

    .line 590
    :try_start_0
    invoke-virtual {p2}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v6

    const/4 v0, 0x0

    if-eqz v6, :cond_0

    .line 591
    invoke-virtual {v6}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    :cond_0
    move-object v2, v0

    :goto_0
    if-eqz v6, :cond_1

    .line 592
    invoke-virtual {v6}, Lcom/anythink/core/api/ATBaseAdAdapter;->getmUnitgroupInfo()Lcom/anythink/core/c/d$b;

    move-result-object v0

    :cond_1
    move-object v3, v0

    if-eqz v2, :cond_2

    .line 597
    invoke-static {}, Lcom/anythink/core/common/g/a/a;->a()Lcom/anythink/core/common/g/a/a;

    move-result-object v7

    new-instance v8, Lcom/anythink/core/common/a$1;

    move-object v0, v8

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/anythink/core/common/a$1;-><init>(Lcom/anythink/core/common/a;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;Landroid/content/Context;Lcom/anythink/core/common/d/b;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    invoke-virtual {v7, v8}, Lcom/anythink/core/common/g/a/a;->a(Ljava/lang/Runnable;)V

    .line 624
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 530
    monitor-enter p0

    .line 531
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz p1, :cond_0

    .line 532
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 533
    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/d/ab;

    if-eqz p1, :cond_0

    .line 535
    invoke-virtual {p1}, Lcom/anythink/core/common/d/ab;->b()V

    .line 538
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/common/d/b;)V
    .locals 1

    .line 549
    monitor-enter p0

    if-nez p3, :cond_0

    .line 551
    :try_start_0
    monitor-exit p0

    return-void

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz p1, :cond_1

    .line 554
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 555
    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/d/ab;

    if-eqz v0, :cond_1

    .line 557
    invoke-virtual {v0, p3}, Lcom/anythink/core/common/d/ab;->a(Lcom/anythink/core/common/d/b;)V

    .line 559
    invoke-virtual {v0}, Lcom/anythink/core/common/d/ab;->c()Z

    move-result p3

    if-nez p3, :cond_1

    .line 560
    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final declared-synchronized a(Ljava/util/List;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    .line 654
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 655
    iget-object v3, v1, Lcom/anythink/core/common/a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v12, p2

    invoke-virtual {v3, v12}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v3, :cond_4

    .line 657
    iget-object v4, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Lcom/anythink/core/common/d/ab;

    if-eqz v13, :cond_4

    .line 663
    invoke-virtual {v13}, Lcom/anythink/core/common/d/ab;->a()Lcom/anythink/core/common/d/b;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 666
    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->f()J

    move-result-wide v4

    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->c()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v8, v4, v6

    if-lez v8, :cond_3

    .line 668
    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v15, p4

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 672
    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 673
    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->g()Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v3

    .line 674
    invoke-virtual {v3}, Lcom/anythink/core/api/ATBaseAdAdapter;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v4

    .line 677
    invoke-virtual/range {p3 .. p3}, Lcom/anythink/core/c/d;->F()I

    move-result v9

    if-eqz v4, :cond_0

    .line 678
    invoke-virtual {v4}, Lcom/anythink/core/common/d/d;->G()I

    move-result v4

    move v11, v4

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    const/4 v11, 0x0

    :goto_1
    move-object/from16 v4, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p3

    move-object/from16 v8, p6

    move/from16 v10, p7

    .line 676
    invoke-static/range {v4 .. v11}, Lcom/anythink/core/common/g/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d;Ljava/lang/String;IZI)Lcom/anythink/core/common/d/d;

    move-result-object v4

    move-object/from16 v5, p1

    .line 680
    invoke-interface {v5, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 681
    invoke-static {v3, v4, v2, v6}, Lcom/anythink/core/common/g/n;->a(Lcom/anythink/core/api/ATBaseAdAdapter;Lcom/anythink/core/common/d/d;Lcom/anythink/core/c/d$b;I)Lcom/anythink/core/common/d/d;

    const/4 v2, 0x4

    .line 4445
    iput v2, v4, Lcom/anythink/core/common/d/d;->n:I

    .line 685
    invoke-virtual {v14}, Lcom/anythink/core/common/d/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/anythink/core/common/f/c;->a(Lcom/anythink/core/common/d/d;Ljava/lang/String;)V

    .line 688
    invoke-virtual {v13, v4, v6}, Lcom/anythink/core/common/d/ab;->a(Lcom/anythink/core/common/d/d;I)V

    goto/16 :goto_0

    :cond_1
    move-object/from16 v5, p1

    goto :goto_2

    :cond_2
    move-object/from16 v5, p1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v5, p1

    move-object/from16 v15, p4

    .line 693
    :goto_2
    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :cond_4
    move-object/from16 v5, p1

    move-object/from16 v15, p4

    goto/16 :goto_0

    .line 697
    :cond_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
