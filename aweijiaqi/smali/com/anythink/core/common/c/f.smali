.class public Lcom/anythink/core/common/c/f;
.super Lcom/anythink/core/common/c/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/core/common/c/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/anythink/core/common/c/a<",
        "Lcom/anythink/core/common/d/x;",
        ">;"
    }
.end annotation


# static fields
.field private static c:Lcom/anythink/core/common/c/f;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/anythink/core/common/c/b;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/anythink/core/common/c/a;-><init>(Lcom/anythink/core/common/c/b;)V

    .line 24
    const-class p1, Lcom/anythink/core/common/c/f;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/common/c/f;->b:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/anythink/core/common/c/b;)Lcom/anythink/core/common/c/f;
    .locals 1

    .line 33
    sget-object v0, Lcom/anythink/core/common/c/f;->c:Lcom/anythink/core/common/c/f;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/anythink/core/common/c/f;

    invoke-direct {v0, p0}, Lcom/anythink/core/common/c/f;-><init>(Lcom/anythink/core/common/c/b;)V

    sput-object v0, Lcom/anythink/core/common/c/f;->c:Lcom/anythink/core/common/c/f;

    .line 36
    :cond_0
    sget-object p0, Lcom/anythink/core/common/c/f;->c:Lcom/anythink/core/common/c/f;

    return-object p0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x;
    .locals 7

    if-eqz p0, :cond_4

    .line 219
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 220
    new-instance v0, Lcom/anythink/core/common/d/x;

    invoke-direct {v0}, Lcom/anythink/core/common/d/x;-><init>()V

    .line 221
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, v0, Lcom/anythink/core/common/d/x;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 222
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "format"

    .line 224
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/anythink/core/common/d/x;->a:I

    const-string v1, "placement_id"

    .line 225
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x;->b:Ljava/lang/String;

    .line 227
    new-instance v1, Lcom/anythink/core/common/d/x$a;

    invoke-direct {v1}, Lcom/anythink/core/common/d/x$a;-><init>()V

    const-string v2, "adsource_id"

    .line 228
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    const-string v2, "hour_time"

    .line 229
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    const-string v2, "date_time"

    .line 230
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    .line 233
    iget-object v2, v1, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 234
    iput v3, v1, Lcom/anythink/core/common/d/x$a;->e:I

    goto :goto_1

    :cond_0
    const-string v2, "hour_imp"

    .line 236
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/anythink/core/common/d/x$a;->e:I

    .line 238
    :goto_1
    iget v2, v0, Lcom/anythink/core/common/d/x;->d:I

    iget v4, v1, Lcom/anythink/core/common/d/x$a;->e:I

    add-int/2addr v2, v4

    iput v2, v0, Lcom/anythink/core/common/d/x;->d:I

    .line 241
    iget-object v2, v1, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 242
    iput v3, v1, Lcom/anythink/core/common/d/x$a;->d:I

    goto :goto_2

    :cond_1
    const-string v2, "date_imp"

    .line 244
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/anythink/core/common/d/x$a;->d:I

    .line 246
    :goto_2
    iget v2, v0, Lcom/anythink/core/common/d/x;->c:I

    iget v3, v1, Lcom/anythink/core/common/d/x$a;->d:I

    add-int/2addr v2, v3

    iput v2, v0, Lcom/anythink/core/common/d/x;->c:I

    const-string v2, "show_time"

    .line 249
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/anythink/core/common/d/x$a;->f:J

    .line 250
    iget-wide v2, v1, Lcom/anythink/core/common/d/x$a;->f:J

    iget-wide v4, v0, Lcom/anythink/core/common/d/x;->e:J

    cmp-long v6, v2, v4

    if-ltz v6, :cond_2

    .line 251
    iget-wide v2, v1, Lcom/anythink/core/common/d/x$a;->f:J

    iput-wide v2, v0, Lcom/anythink/core/common/d/x;->e:J

    .line 254
    :cond_2
    iget-object v2, v0, Lcom/anythink/core/common/d/x;->f:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, v1, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 256
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return-object v0

    :cond_4
    const/4 p0, 0x0

    return-object p0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x$a;
    .locals 2

    if-eqz p0, :cond_2

    .line 271
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 272
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 273
    new-instance v0, Lcom/anythink/core/common/d/x$a;

    invoke-direct {v0}, Lcom/anythink/core/common/d/x$a;-><init>()V

    const-string v1, "adsource_id"

    .line 274
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    const-string v1, "hour_time"

    .line 275
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    const-string v1, "date_time"

    .line 276
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    .line 279
    iget-object v1, v0, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 280
    iput v1, v0, Lcom/anythink/core/common/d/x$a;->e:I

    goto :goto_0

    :cond_0
    const-string p2, "hour_imp"

    .line 282
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result p2

    iput p2, v0, Lcom/anythink/core/common/d/x$a;->e:I

    .line 286
    :goto_0
    iget-object p2, v0, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    invoke-static {p2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 287
    iput v1, v0, Lcom/anythink/core/common/d/x$a;->d:I

    goto :goto_1

    :cond_1
    const-string p1, "date_imp"

    .line 289
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    iput p1, v0, Lcom/anythink/core/common/d/x$a;->d:I

    :goto_1
    const-string p1, "show_time"

    .line 293
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p0

    iput-wide p0, v0, Lcom/anythink/core/common/d/x$a;->f:J

    return-object v0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 4

    .line 197
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string p1, "SELECT adsource_id FROM placement_ad_impression WHERE adsource_id=? GROUP BY adsource_id"

    invoke-virtual {v0, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 198
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 199
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return v1

    :cond_0
    if-eqz p1, :cond_1

    .line 203
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_1
    return v3
.end method


# virtual methods
.method public final declared-synchronized a(ILjava/lang/String;Lcom/anythink/core/common/d/x$a;)J
    .locals 7

    monitor-enter p0

    .line 165
    :try_start_0
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_4

    if-nez p3, :cond_0

    goto/16 :goto_1

    .line 169
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "format"

    .line 170
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string p1, "placement_id"

    .line 171
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "adsource_id"

    .line 172
    iget-object p2, p3, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "hour_time"

    .line 173
    iget-object p2, p3, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "hour_imp"

    .line 174
    iget p2, p3, Lcom/anythink/core/common/d/x$a;->e:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string p1, "date_time"

    .line 175
    iget-object p2, p3, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "date_imp"

    .line 176
    iget p2, p3, Lcom/anythink/core/common/d/x$a;->d:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string p1, "show_time"

    .line 177
    iget-wide v3, p3, Lcom/anythink/core/common/d/x$a;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 179
    iget-object p1, p3, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    const-string p2, "SELECT adsource_id FROM placement_ad_impression WHERE adsource_id=? GROUP BY adsource_id"

    .line 2197
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, p2, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 2198
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result p2

    if-lez p2, :cond_1

    .line 2199
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 2203
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    .line 180
    iget-object p1, p0, Lcom/anythink/core/common/c/f;->b:Ljava/lang/String;

    const-string p2, "existsByTime--update"

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "adsource_id = ? "

    .line 182
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p2

    const-string v3, "placement_ad_impression"

    new-array v4, v4, [Ljava/lang/String;

    iget-object p3, p3, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    aput-object p3, v4, v6

    invoke-virtual {p2, v3, v0, p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long p1, p1

    monitor-exit p0

    return-wide p1

    .line 184
    :cond_3
    :try_start_2
    iget-object p1, p0, Lcom/anythink/core/common/c/f;->b:Ljava/lang/String;

    const-string p2, "existsByTime--insert"

    invoke-static {p1, p2}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    const-string p2, "placement_ad_impression"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-wide p1

    .line 192
    :catch_0
    monitor-exit p0

    return-wide v1

    .line 166
    :cond_4
    :goto_1
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x$a;
    .locals 2

    monitor-enter p0

    .line 138
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT * FROM placement_ad_impression WHERE adsource_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\' AND placement_id=\'"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 p2, 0x0

    .line 142
    :try_start_1
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_2

    .line 1271
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1272
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 1273
    new-instance v0, Lcom/anythink/core/common/d/x$a;

    invoke-direct {v0}, Lcom/anythink/core/common/d/x$a;-><init>()V

    const-string v1, "adsource_id"

    .line 1274
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    const-string v1, "hour_time"

    .line 1275
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    const-string v1, "date_time"

    .line 1276
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    .line 1279
    iget-object v1, v0, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    invoke-static {v1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    const/4 v1, 0x0

    if-nez p4, :cond_0

    .line 1280
    iput v1, v0, Lcom/anythink/core/common/d/x$a;->e:I

    goto :goto_0

    :cond_0
    const-string p4, "hour_imp"

    .line 1282
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p4

    invoke-interface {p1, p4}, Landroid/database/Cursor;->getInt(I)I

    move-result p4

    iput p4, v0, Lcom/anythink/core/common/d/x$a;->e:I

    .line 1286
    :goto_0
    iget-object p4, v0, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    invoke-static {p4, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 1287
    iput v1, v0, Lcom/anythink/core/common/d/x$a;->d:I

    goto :goto_1

    :cond_1
    const-string p3, "date_imp"

    .line 1289
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    iput p3, v0, Lcom/anythink/core/common/d/x$a;->d:I

    :goto_1
    const-string p3, "show_time"

    .line 1293
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p3

    iput-wide p3, v0, Lcom/anythink/core/common/d/x$a;->f:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object p2, v0

    goto :goto_2

    :catchall_0
    nop

    goto :goto_3

    :catch_0
    nop

    goto :goto_5

    :cond_2
    :goto_2
    if-eqz p1, :cond_3

    .line 157
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 144
    :cond_3
    monitor-exit p0

    return-object p2

    :catchall_1
    move-object p1, p2

    :goto_3
    if-eqz p1, :cond_5

    .line 157
    :goto_4
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_6

    :catch_1
    move-object p1, p2

    .line 150
    :catch_2
    :try_start_5
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz p1, :cond_5

    goto :goto_4

    :catchall_2
    move-exception p2

    if-eqz p1, :cond_4

    .line 157
    :try_start_6
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 159
    :cond_4
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_3
    move-object p1, p2

    :goto_5
    if-eqz p1, :cond_5

    goto :goto_4

    .line 160
    :cond_5
    :goto_6
    monitor-exit p0

    return-object p2

    :catchall_3
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x;
    .locals 2

    monitor-enter p0

    .line 112
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT * FROM placement_ad_impression WHERE placement_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v0, 0x0

    .line 116
    :try_start_1
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 117
    :try_start_2
    invoke-static {p1, p2, p3}, Lcom/anythink/core/common/c/f;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/common/d/x;

    move-result-object p2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p1, :cond_0

    .line 131
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 118
    :cond_0
    monitor-exit p0

    return-object p2

    :catchall_0
    nop

    goto :goto_0

    :catch_0
    nop

    goto :goto_2

    :catchall_1
    move-object p1, v0

    :goto_0
    if-eqz p1, :cond_2

    .line 131
    :goto_1
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_3

    :catch_1
    move-object p1, v0

    .line 124
    :catch_2
    :try_start_5
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz p1, :cond_2

    goto :goto_1

    :catchall_2
    move-exception p2

    if-eqz p1, :cond_1

    .line 131
    :try_start_6
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_1
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catch_3
    move-object p1, v0

    :goto_2
    if-eqz p1, :cond_2

    goto :goto_1

    .line 134
    :cond_2
    :goto_3
    monitor-exit p0

    return-object v0

    :catchall_3
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized a(ILjava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/common/d/x;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 41
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT * FROM placement_ad_impression WHERE format=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v1, 0x0

    .line 47
    :try_start_1
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 48
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result p1

    if-lez p1, :cond_6

    .line 49
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_4

    const-string p1, "placement_id"

    .line 50
    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/common/d/x;

    if-nez v2, :cond_0

    .line 54
    new-instance v2, Lcom/anythink/core/common/d/x;

    invoke-direct {v2}, Lcom/anythink/core/common/d/x;-><init>()V

    .line 55
    iput-object p1, v2, Lcom/anythink/core/common/d/x;->b:Ljava/lang/String;

    const-string v3, "format"

    .line 56
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/anythink/core/common/d/x;->a:I

    .line 57
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, v2, Lcom/anythink/core/common/d/x;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 58
    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_0
    new-instance p1, Lcom/anythink/core/common/d/x$a;

    invoke-direct {p1}, Lcom/anythink/core/common/d/x$a;-><init>()V

    const-string v3, "adsource_id"

    .line 62
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    const-string v3, "hour_time"

    .line 63
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    const-string v3, "date_time"

    .line 64
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    .line 67
    iget-object v3, p1, Lcom/anythink/core/common/d/x$a;->b:Ljava/lang/String;

    invoke-static {v3, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    .line 68
    iput v4, p1, Lcom/anythink/core/common/d/x$a;->e:I

    goto :goto_1

    :cond_1
    const-string v3, "hour_imp"

    .line 70
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p1, Lcom/anythink/core/common/d/x$a;->e:I

    .line 72
    :goto_1
    iget v3, v2, Lcom/anythink/core/common/d/x;->d:I

    iget v5, p1, Lcom/anythink/core/common/d/x$a;->e:I

    add-int/2addr v3, v5

    iput v3, v2, Lcom/anythink/core/common/d/x;->d:I

    .line 75
    iget-object v3, p1, Lcom/anythink/core/common/d/x$a;->c:Ljava/lang/String;

    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 76
    iput v4, p1, Lcom/anythink/core/common/d/x$a;->d:I

    goto :goto_2

    :cond_2
    const-string v3, "date_imp"

    .line 78
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p1, Lcom/anythink/core/common/d/x$a;->d:I

    .line 80
    :goto_2
    iget v3, v2, Lcom/anythink/core/common/d/x;->c:I

    iget v4, p1, Lcom/anythink/core/common/d/x$a;->d:I

    add-int/2addr v3, v4

    iput v3, v2, Lcom/anythink/core/common/d/x;->c:I

    const-string v3, "show_time"

    .line 83
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p1, Lcom/anythink/core/common/d/x$a;->f:J

    .line 84
    iget-wide v3, p1, Lcom/anythink/core/common/d/x$a;->f:J

    iget-wide v5, v2, Lcom/anythink/core/common/d/x;->e:J

    cmp-long v7, v3, v5

    if-ltz v7, :cond_3

    .line 85
    iget-wide v3, p1, Lcom/anythink/core/common/d/x$a;->f:J

    iput-wide v3, v2, Lcom/anythink/core/common/d/x;->e:J

    .line 88
    :cond_3
    iget-object v2, v2, Lcom/anythink/core/common/d/x;->f:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p1, Lcom/anythink/core/common/d/x$a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 90
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_5

    .line 105
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 91
    :cond_5
    monitor-exit p0

    return-object v0

    :catchall_0
    nop

    goto :goto_4

    :catch_0
    nop

    goto :goto_5

    :cond_6
    if-eqz v1, :cond_8

    .line 105
    :goto_3
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_6

    :goto_4
    if-eqz v1, :cond_8

    goto :goto_3

    .line 98
    :catch_1
    :try_start_4
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_8

    goto :goto_3

    :catchall_1
    move-exception p1

    if-eqz v1, :cond_7

    .line 105
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 107
    :cond_7
    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :goto_5
    if-eqz v1, :cond_8

    goto :goto_3

    .line 108
    :cond_8
    :goto_6
    monitor-exit p0

    return-object v0

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .line 306
    monitor-enter p0

    .line 308
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "date_time!=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 309
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 310
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 312
    :cond_0
    :try_start_2
    invoke-virtual {p0}, Lcom/anythink/core/common/c/f;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "placement_ad_impression"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 315
    :catch_0
    :goto_0
    :try_start_3
    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method
