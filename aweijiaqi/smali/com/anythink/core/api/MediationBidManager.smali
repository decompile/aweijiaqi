.class public abstract Lcom/anythink/core/api/MediationBidManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/core/api/MediationBidManager$BidListener;
    }
.end annotation


# instance fields
.field protected mRequestUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract notifyWinnerDisplay(Ljava/lang/String;Lcom/anythink/core/c/d$b;)V
.end method

.method public setBidRequestUrl(Ljava/lang/String;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/anythink/core/api/MediationBidManager;->mRequestUrl:Ljava/lang/String;

    return-void
.end method

.method public abstract startBid(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/api/MediationBidManager$BidListener;J)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;",
            "Lcom/anythink/core/api/MediationBidManager$BidListener;",
            "J)V"
        }
    .end annotation
.end method
