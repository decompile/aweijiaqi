.class public Lcom/anythink/core/api/ATAdConst$KEY;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/core/api/ATAdConst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KEY"
.end annotation


# static fields
.field public static final AD_CLICK_CONFIRM_STATUS:Ljava/lang/String; = "ad_click_confirm_status"

.field public static final AD_HEIGHT:Ljava/lang/String; = "key_height"

.field public static final AD_IS_SUPPORT_DEEP_LINK:Ljava/lang/String; = "ad_is_support_deep_link"

.field public static final AD_ORIENTATION:Ljava/lang/String; = "ad_orientation"

.field public static final AD_SOUND:Ljava/lang/String; = "ad_sound"

.field public static final AD_WIDTH:Ljava/lang/String; = "key_width"

.field public static final REWARD_AMOUNT:Ljava/lang/String; = "reward_amount"

.field public static final REWARD_NAME:Ljava/lang/String; = "reward_name"

.field public static final USER_CUSTOM_DATA:Ljava/lang/String; = "user_custom_data"

.field public static final USER_ID:Ljava/lang/String; = "user_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
