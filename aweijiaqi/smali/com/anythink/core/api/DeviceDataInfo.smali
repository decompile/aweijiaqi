.class public Lcom/anythink/core/api/DeviceDataInfo;
.super Ljava/lang/Object;


# static fields
.field public static final ANDROID_ID:Ljava/lang/String; = "android_id"

.field public static final APP_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final APP_VERSION_CODE:Ljava/lang/String; = "app_vc"

.field public static final APP_VERSION_NAME:Ljava/lang/String; = "app_vn"

.field public static final BRAND:Ljava/lang/String; = "brand"

.field public static final DEVICE_SCREEN_SIZE:Ljava/lang/String; = "screen"

.field public static final GAID:Ljava/lang/String; = "gaid"

.field public static final INSTALLER:Ljava/lang/String; = "it_src"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final MCC:Ljava/lang/String; = "mcc"

.field public static final MNC:Ljava/lang/String; = "mnc"

.field public static final MODEL:Ljava/lang/String; = "model"

.field public static final NETWORK_TYPE:Ljava/lang/String; = "network_type"

.field public static final ORIENTATION:Ljava/lang/String; = "orient"

.field public static final OS_VERSION_CODE:Ljava/lang/String; = "os_vc"

.field public static final OS_VERSION_NAME:Ljava/lang/String; = "os_vn"

.field public static final TIMEZONE:Ljava/lang/String; = "timezone"

.field public static final USER_AGENT:Ljava/lang/String; = "ua"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
