.class public Lcom/anythink/core/api/ATAdInfo;
.super Ljava/lang/Object;


# instance fields
.field private mAdNetworkType:Ljava/lang/String;

.field private mAdsourceId:Ljava/lang/String;

.field private mAdsourceIndex:I

.field private mBaseAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

.field private mChannel:Ljava/lang/String;

.field private mCountry:Ljava/lang/String;

.field private mCurrency:Ljava/lang/String;

.field private mCustomRule:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mEcpm:D

.field private mEcpmLevel:I

.field private mEcpmPrecision:Ljava/lang/String;

.field private mExtInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mIsHBAdsource:I

.field private mNetworkFirmId:I

.field private mNetworkPlacementId:Ljava/lang/String;

.field private mPublisherRevenue:Ljava/lang/Double;

.field private mScenarioId:Ljava/lang/String;

.field private mScenarioRewardName:Ljava/lang/String;

.field private mScenarioRewardNumber:I

.field private mSegmentId:I

.field private mShowId:Ljava/lang/String;

.field private mSubChannel:Ljava/lang/String;

.field private mTopOnAdFormat:Ljava/lang/String;

.field private mTopOnPlacementId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 60
    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkFirmId:I

    const-string v1, ""

    .line 61
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceId:Ljava/lang/String;

    .line 62
    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceIndex:I

    const-wide/16 v2, 0x0

    .line 63
    iput-wide v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    const/4 v0, 0x0

    .line 64
    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mIsHBAdsource:I

    .line 66
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mShowId:Ljava/lang/String;

    .line 67
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mPublisherRevenue:Ljava/lang/Double;

    .line 68
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mCurrency:Ljava/lang/String;

    .line 69
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mCountry:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnPlacementId:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnAdFormat:Ljava/lang/String;

    const-string v2, "publisher_defined"

    .line 73
    iput-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmPrecision:Ljava/lang/String;

    const-string v2, "Network"

    .line 74
    iput-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkPlacementId:Ljava/lang/String;

    const/4 v2, 0x1

    .line 76
    iput v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmLevel:I

    .line 78
    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mSegmentId:I

    .line 79
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    .line 80
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    .line 81
    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    .line 83
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mSubChannel:Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mChannel:Ljava/lang/String;

    const/4 v0, 0x0

    .line 85
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    return-void
.end method

.method private static fillData(Lcom/anythink/core/api/ATAdInfo;Lcom/anythink/core/common/d/d;)Lcom/anythink/core/api/ATAdInfo;
    .locals 6

    .line 238
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->B()I

    move-result v0

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkFirmId:I

    .line 239
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceId:Ljava/lang/String;

    .line 240
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->u()I

    move-result v0

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceIndex:I

    .line 242
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->p()I

    move-result v0

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mIsHBAdsource:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 245
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->q()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->b()D

    move-result-wide v4

    mul-double v2, v2, v4

    iput-wide v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    goto :goto_0

    .line 247
    :cond_0
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->a()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    .line 250
    :goto_0
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCurrency:Ljava/lang/String;

    .line 253
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mShowId:Ljava/lang/String;

    .line 254
    iget-wide v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mPublisherRevenue:Ljava/lang/Double;

    .line 256
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCountry:Ljava/lang/String;

    .line 258
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->L()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnAdFormat:Ljava/lang/String;

    .line 259
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->J()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnPlacementId:Ljava/lang/String;

    .line 262
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mIsHBAdsource:I

    if-ne v0, v1, :cond_1

    const-string v0, "exact"

    .line 263
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmPrecision:Ljava/lang/String;

    goto :goto_1

    .line 264
    :cond_1
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 265
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmPrecision:Ljava/lang/String;

    .line 269
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->B()I

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_3

    const-string v0, "Cross_Promotion"

    .line 270
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    goto :goto_2

    .line 271
    :cond_3
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->B()I

    move-result v0

    const/16 v1, 0x42

    if-ne v0, v1, :cond_4

    const-string v0, "Adx"

    .line 272
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v0, "Network"

    .line 274
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    .line 277
    :goto_2
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkPlacementId:Ljava/lang/String;

    .line 278
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->g()I

    move-result v0

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmLevel:I

    .line 279
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->C()I

    move-result v0

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mSegmentId:I

    .line 1265
    iget-object v0, p1, Lcom/anythink/core/common/d/d;->z:Ljava/lang/String;

    .line 280
    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    .line 283
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnAdFormat:Ljava/lang/String;

    const-string v1, "RewardedVideo"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 284
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->k()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 285
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 286
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/api/ATRewardInfo;

    if-eqz v0, :cond_5

    .line 288
    iget-object v1, v0, Lcom/anythink/core/api/ATRewardInfo;->rewardName:Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    .line 289
    iget v0, v0, Lcom/anythink/core/api/ATRewardInfo;->rewardNumber:I

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    .line 293
    :cond_5
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    if-nez v0, :cond_7

    .line 294
    :cond_6
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->l()Lcom/anythink/core/api/ATRewardInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 296
    iget-object v1, v0, Lcom/anythink/core/api/ATRewardInfo;->rewardName:Ljava/lang/String;

    iput-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    .line 297
    iget v0, v0, Lcom/anythink/core/api/ATRewardInfo;->rewardNumber:I

    iput v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    .line 303
    :cond_7
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mChannel:Ljava/lang/String;

    .line 304
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mSubChannel:Ljava/lang/String;

    .line 305
    invoke-virtual {p1}, Lcom/anythink/core/common/d/d;->m()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    return-object p0
.end method

.method private static fromAdTrackingInfo(Lcom/anythink/core/common/d/d;)Lcom/anythink/core/api/ATAdInfo;
    .locals 1

    .line 230
    new-instance v0, Lcom/anythink/core/api/ATAdInfo;

    invoke-direct {v0}, Lcom/anythink/core/api/ATAdInfo;-><init>()V

    if-eqz p0, :cond_0

    .line 232
    invoke-static {v0, p0}, Lcom/anythink/core/api/ATAdInfo;->fillData(Lcom/anythink/core/api/ATAdInfo;Lcom/anythink/core/common/d/d;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object p0

    return-object p0

    :cond_0
    return-object v0
.end method

.method public static fromAdapter(Lcom/anythink/core/common/b/b;)Lcom/anythink/core/api/ATAdInfo;
    .locals 2

    if-eqz p0, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/anythink/core/common/b/b;->getTrackingInfo()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/api/ATAdInfo;->fromAdTrackingInfo(Lcom/anythink/core/common/d/d;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    .line 208
    instance-of v1, p0, Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v1, :cond_0

    .line 209
    check-cast p0, Lcom/anythink/core/api/ATBaseAdAdapter;

    iput-object p0, v0, Lcom/anythink/core/api/ATAdInfo;->mBaseAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    .line 210
    invoke-virtual {p0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkInfoMap()Ljava/util/Map;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    :cond_0
    return-object v0

    .line 216
    :cond_1
    new-instance p0, Lcom/anythink/core/api/ATAdInfo;

    invoke-direct {p0}, Lcom/anythink/core/api/ATAdInfo;-><init>()V

    return-object p0
.end method

.method public static fromBaseAd(Lcom/anythink/core/api/BaseAd;)Lcom/anythink/core/api/ATAdInfo;
    .locals 1

    if-eqz p0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/anythink/core/api/BaseAd;->getDetail()Lcom/anythink/core/common/d/d;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/api/ATAdInfo;->fromAdTrackingInfo(Lcom/anythink/core/common/d/d;)Lcom/anythink/core/api/ATAdInfo;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Lcom/anythink/core/api/BaseAd;->getNetworkInfoMap()Ljava/util/Map;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    return-object v0

    .line 225
    :cond_0
    new-instance p0, Lcom/anythink/core/api/ATAdInfo;

    invoke-direct {p0}, Lcom/anythink/core/api/ATAdInfo;-><init>()V

    return-object p0
.end method


# virtual methods
.method public getAdNetworkType()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    return-object v0
.end method

.method public getAdsourceId()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdsourceIndex()I
    .locals 1

    .line 98
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceIndex:I

    return v0
.end method

.method public getChannel()Ljava/lang/String;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomRule()Ljava/lang/String;
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getEcpm()D
    .locals 2

    .line 102
    iget-wide v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    return-wide v0
.end method

.method public getEcpmLevel()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmLevel:I

    return v0
.end method

.method public getEcpmPrecision()Ljava/lang/String;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmPrecision:Ljava/lang/String;

    return-object v0
.end method

.method public getExtInfoMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getNetworkFirmId()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkFirmId:I

    return v0
.end method

.method public getNetworkPlacementId()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkPlacementId:Ljava/lang/String;

    return-object v0
.end method

.method public getPublisherRevenue()Ljava/lang/Double;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mPublisherRevenue:Ljava/lang/Double;

    return-object v0
.end method

.method public getRewardUserCustomData()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mBaseAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getUserCustomData()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getScenarioId()Ljava/lang/String;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    return-object v0
.end method

.method public getScenarioRewardName()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    return-object v0
.end method

.method public getScenarioRewardNumber()I
    .locals 1

    .line 175
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    return v0
.end method

.method public getSegmentId()I
    .locals 1

    .line 155
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mSegmentId:I

    return v0
.end method

.method public getShowId()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mShowId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubChannel()Ljava/lang/String;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mSubChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getTopOnAdFormat()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnAdFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getTopOnPlacementId()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnPlacementId:Ljava/lang/String;

    return-object v0
.end method

.method public isHeaderBiddingAdsource()I
    .locals 1

    .line 111
    iget v0, p0, Lcom/anythink/core/api/ATAdInfo;->mIsHBAdsource:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 314
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "id"

    .line 316
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mShowId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "publisher_revenue"

    .line 317
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mPublisherRevenue:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "currency"

    .line 318
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mCurrency:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "country"

    .line 319
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mCountry:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "adunit_id"

    .line 320
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "adunit_format"

    .line 322
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mTopOnAdFormat:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "precision"

    .line 323
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmPrecision:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "network_type"

    .line 324
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mAdNetworkType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "network_placement_id"

    .line 325
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkPlacementId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ecpm_level"

    .line 326
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpmLevel:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "segment_id"

    .line 328
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mSegmentId:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 329
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "scenario_id"

    .line 330
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    if-eqz v1, :cond_1

    const-string v1, "scenario_reward_name"

    .line 334
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "scenario_reward_number"

    .line 335
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mScenarioRewardNumber:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mChannel:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "channel"

    .line 339
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mChannel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 341
    :cond_2
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mSubChannel:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "sub_channel"

    .line 342
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mSubChannel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 344
    :cond_3
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_4

    const-string v1, "custom_rule"

    .line 345
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/anythink/core/api/ATAdInfo;->mCustomRule:Ljava/util/Map;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    const-string v1, "network_firm_id"

    .line 347
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mNetworkFirmId:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "adsource_id"

    .line 349
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "adsource_index"

    .line 350
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mAdsourceIndex:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "adsource_price"

    .line 351
    iget-wide v2, p0, Lcom/anythink/core/api/ATAdInfo;->mEcpm:D

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v1, "adsource_isheaderbidding"

    .line 352
    iget v2, p0, Lcom/anythink/core/api/ATAdInfo;->mIsHBAdsource:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 354
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_5

    const-string v1, "ext_info"

    .line 355
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/anythink/core/api/ATAdInfo;->mExtInfoMap:Ljava/util/Map;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 358
    :cond_5
    iget-object v1, p0, Lcom/anythink/core/api/ATAdInfo;->mBaseAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    if-eqz v1, :cond_6

    const-string v1, "reward_custom_data"

    .line 359
    iget-object v2, p0, Lcom/anythink/core/api/ATAdInfo;->mBaseAdapter:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getUserCustomData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 363
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 366
    :cond_6
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
