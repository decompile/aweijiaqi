.class public final Lcom/anythink/core/b/c;
.super Lcom/anythink/core/b/d;


# instance fields
.field final a:Ljava/lang/String;

.field b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/b/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Z

.field k:Lcom/anythink/core/b/b/a;

.field l:J


# direct methods
.method public constructor <init>(Lcom/anythink/core/common/d/a;)V
    .locals 1

    .line 60
    invoke-direct {p0, p1}, Lcom/anythink/core/b/d;-><init>(Lcom/anythink/core/common/d/a;)V

    const-string v0, "HeadBidding"

    .line 43
    iput-object v0, p0, Lcom/anythink/core/b/c;->a:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/b/c;->d:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    const/4 v0, 0x0

    .line 61
    iput-boolean v0, p0, Lcom/anythink/core/b/c;->j:Z

    .line 63
    invoke-direct {p0, p1}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/common/d/a;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/l;",
            ">;"
        }
    .end annotation

    .line 254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 255
    instance-of v1, p0, Lorg/json/JSONArray;

    if-eqz v1, :cond_0

    .line 256
    check-cast p0, Lorg/json/JSONArray;

    const/4 v1, 0x0

    .line 257
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 258
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/common/d/l;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/l;

    move-result-object v2

    .line 259
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/anythink/core/b/c;Ljava/util/List;J)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/core/b/c;->a(Ljava/util/List;J)V

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/a;)V
    .locals 9

    .line 118
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/anythink/core/b/c;->f:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/anythink/core/b/c;->g:Ljava/lang/String;

    .line 121
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/anythink/core/b/c;->i:Ljava/lang/String;

    .line 122
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 123
    iget v1, p1, Lcom/anythink/core/common/d/a;->d:I

    .line 125
    iget-object p1, p1, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/anythink/core/b/c;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/anythink/core/b/c;->g:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v1, v4}, Lcom/anythink/core/common/g/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Lorg/json/JSONObject;

    move-result-object p1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/b/c;->h:Ljava/lang/String;

    .line 126
    iget-boolean p1, p0, Lcom/anythink/core/b/c;->n:Z

    if-eqz p1, :cond_0

    .line 127
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "Start HeadBidding List"

    .line 129
    invoke-static {v0}, Lcom/anythink/core/b/c;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :catch_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "HeadBidding"

    invoke-static {v2, p1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/c/d$b;

    .line 137
    invoke-static {v0}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v2

    const-wide/16 v3, 0x0

    if-nez v2, :cond_1

    const-string v2, "There is no Network SDK."

    .line 140
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/anythink/core/b/c;->b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    goto :goto_0

    .line 144
    :cond_1
    iget-object v5, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v5, v5, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iget-object v6, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v6, v6, Lcom/anythink/core/common/d/a;->k:Lcom/anythink/core/c/d;

    iget-object v7, p0, Lcom/anythink/core/b/c;->g:Ljava/lang/String;

    iget-object v8, p0, Lcom/anythink/core/b/c;->f:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/anythink/core/api/ATBaseAdAdapter;->networkSDKInit(Landroid/content/Context;Ljava/util/Map;)V

    .line 145
    iget v5, v0, Lcom/anythink/core/c/d$b;->b:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_7

    const/4 v6, 0x6

    if-eq v5, v6, :cond_6

    const/16 v6, 0xd

    if-eq v5, v6, :cond_4

    const/16 v6, 0x20

    if-eq v5, v6, :cond_3

    const/16 v2, 0x42

    if-eq v5, v2, :cond_2

    const-string v2, "This network don\'t support head bidding in current TopOn\'s version."

    .line 182
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/anythink/core/b/c;->b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    goto :goto_0

    .line 153
    :cond_2
    new-instance v2, Lcom/anythink/core/b/a/a/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->g()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/anythink/core/b/a/a/a;-><init>(Ljava/lang/String;Lcom/anythink/core/c/d$b;Ljava/util/List;)V

    .line 154
    iget-object v3, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/anythink/core/b/a/a/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :cond_3
    new-instance v3, Lcom/anythink/core/b/a/a/e;

    iget-object v4, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v4, v4, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0, v2}, Lcom/anythink/core/b/a/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 161
    iget-object v2, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/anythink/core/b/a/a/e;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 171
    :cond_4
    new-instance v5, Lcom/anythink/core/b/a/a/f;

    iget-object v6, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v6, v6, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v0, v2}, Lcom/anythink/core/b/a/a/f;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 173
    invoke-virtual {v5}, Lcom/anythink/core/b/a/a/f;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 174
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is initializing."

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/anythink/core/b/c;->b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 176
    :cond_5
    iget-object v2, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lcom/anythink/core/b/a/a/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 147
    :cond_6
    new-instance v3, Lcom/anythink/core/b/a/a/d;

    iget-object v4, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v4, v4, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0, v2}, Lcom/anythink/core/b/a/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 148
    iget-object v2, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/anythink/core/b/a/a/d;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 166
    :cond_7
    new-instance v3, Lcom/anythink/core/b/a/a/c;

    iget-object v4, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v4, v4, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0, v2}, Lcom/anythink/core/b/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V

    .line 167
    iget-object v2, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/anythink/core/b/a/a/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method private a(Lcom/anythink/core/common/d/l;)V
    .locals 2

    .line 112
    iget-object v0, p1, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v0, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v0, v0, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/util/List;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/l;",
            ">;J)V"
        }
    .end annotation

    monitor-enter p0

    .line 190
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/b/c;->j:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    .line 192
    iput-boolean v0, p0, Lcom/anythink/core/b/c;->j:Z

    if-eqz p1, :cond_3

    .line 194
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 196
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/common/d/l;

    .line 198
    iget-object v1, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget v1, v1, Lcom/anythink/core/common/d/a;->d:I

    const-string v2, "4"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 199
    iget v1, v0, Lcom/anythink/core/common/d/l;->d:I

    const/16 v2, 0x42

    if-eq v1, v2, :cond_0

    goto :goto_1

    .line 1112
    :cond_0
    iget-object v1, v0, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1113
    invoke-static {}, Lcom/anythink/core/common/a/a;->a()Lcom/anythink/core/common/a/a;

    iget-object v1, p0, Lcom/anythink/core/b/c;->m:Lcom/anythink/core/common/d/a;

    iget-object v1, v1, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    iget-object v3, v0, Lcom/anythink/core/common/d/l;->i:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/anythink/core/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v0, Lcom/anythink/core/common/d/l;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/anythink/core/common/d/l;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/c/d$b;

    .line 208
    invoke-virtual {p0, v1, v0, p2, p3}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V

    goto :goto_0

    .line 211
    :cond_2
    iget-object p1, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_3

    .line 214
    :cond_3
    iget-object p1, p0, Lcom/anythink/core/b/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/c/d$b;

    const-string v1, "Bid request error."

    .line 215
    invoke-static {v0, v1}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    iget-object v1, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const-string v1, "Bid request error."

    .line 219
    invoke-direct {p0, v0, v1, p2, p3}, Lcom/anythink/core/b/c;->b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    goto :goto_2

    .line 223
    :cond_5
    iget-object p1, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x2

    if-lt p1, p2, :cond_6

    .line 224
    iget-object p1, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 230
    :cond_6
    :goto_3
    iget-boolean p1, p0, Lcom/anythink/core/b/c;->n:Z

    if-eqz p1, :cond_7

    .line 231
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string p2, "HeadBidding Success List"

    .line 234
    iget-object p3, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-static {p3}, Lcom/anythink/core/b/c;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "HeadBidding Fail List"

    .line 235
    iget-object p3, p0, Lcom/anythink/core/b/c;->d:Ljava/util/List;

    invoke-static {p3}, Lcom/anythink/core/b/c;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    :catch_0
    :try_start_2
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    const-string p2, "HeadBidding"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_7
    iget-object p1, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_8

    .line 244
    iget-object p1, p0, Lcom/anythink/core/b/c;->k:Lcom/anythink/core/b/b/a;

    iget-object p2, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {p1, p2}, Lcom/anythink/core/b/b/a;->a(Ljava/util/List;)V

    .line 246
    :cond_8
    iget-object p1, p0, Lcom/anythink/core/b/c;->k:Lcom/anythink/core/b/b/a;

    iget-object p2, p0, Lcom/anythink/core/b/c;->d:Ljava/util/List;

    invoke-interface {p1, p2}, Lcom/anythink/core/b/b/a;->b(Ljava/util/List;)V

    .line 247
    iget-object p1, p0, Lcom/anythink/core/b/c;->k:Lcom/anythink/core/b/b/a;

    invoke-interface {p1}, Lcom/anythink/core/b/b/a;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    :cond_9
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private static synthetic b(Ljava/lang/Object;)Ljava/util/List;
    .locals 3

    .line 1254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1255
    instance-of v1, p0, Lorg/json/JSONArray;

    if-eqz v1, :cond_0

    .line 1256
    check-cast p0, Lorg/json/JSONArray;

    const/4 v1, 0x0

    .line 1257
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1258
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/anythink/core/common/d/l;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/l;

    move-result-object v2

    .line 1259
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V
    .locals 0

    .line 300
    invoke-static {p1, p2, p3, p4}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    .line 301
    iget-object p2, p0, Lcom/anythink/core/b/c;->d:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .line 295
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/anythink/core/b/c;->l:J

    sub-long/2addr v0, v2

    const/4 v2, 0x0

    .line 296
    invoke-direct {p0, v2, v0, v1}, Lcom/anythink/core/b/c;->a(Ljava/util/List;J)V

    return-void
.end method

.method protected final a(Lcom/anythink/core/b/b/a;)V
    .locals 8

    .line 73
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/core/b/c;->l:J

    .line 75
    iput-object p1, p0, Lcom/anythink/core/b/c;->k:Lcom/anythink/core/b/b/a;

    .line 76
    iget-object p1, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    const-wide/16 v0, 0x0

    .line 77
    invoke-direct {p0, p1, v0, v1}, Lcom/anythink/core/b/c;->a(Ljava/util/List;J)V

    return-void

    .line 82
    :cond_0
    new-instance p1, Lcom/anythink/core/b/a/a;

    iget-object v3, p0, Lcom/anythink/core/b/c;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/anythink/core/b/c;->g:Ljava/lang/String;

    iget-object v5, p0, Lcom/anythink/core/b/c;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/anythink/core/b/c;->c:Ljava/util/List;

    iget-object v7, p0, Lcom/anythink/core/b/c;->h:Ljava/lang/String;

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lcom/anythink/core/b/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-instance v1, Lcom/anythink/core/b/c$1;

    invoke-direct {v1, p0}, Lcom/anythink/core/b/c$1;-><init>(Lcom/anythink/core/b/c;)V

    invoke-virtual {p1, v0, v1}, Lcom/anythink/core/b/a/a;->a(ILcom/anythink/core/common/e/g;)V

    return-void
.end method

.method protected final a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V
    .locals 4

    .line 269
    instance-of v0, p2, Lcom/anythink/core/common/d/l;

    if-eqz v0, :cond_3

    .line 270
    check-cast p2, Lcom/anythink/core/common/d/l;

    .line 271
    iget-boolean v0, p2, Lcom/anythink/core/common/d/l;->isSuccess:Z

    if-eqz v0, :cond_1

    .line 272
    iput-wide p3, p1, Lcom/anythink/core/c/d$b;->s:J

    .line 273
    iget-object p3, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    iget p3, p2, Lcom/anythink/core/common/d/l;->d:I

    const/16 p4, 0x42

    if-ne p3, p4, :cond_0

    .line 276
    iget-wide p3, p2, Lcom/anythink/core/common/d/l;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr p3, v0

    iput-wide p3, p2, Lcom/anythink/core/common/d/l;->f:J

    goto :goto_0

    .line 278
    :cond_0
    iget-wide p3, p1, Lcom/anythink/core/c/d$b;->x:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr p3, v0

    iput-wide p3, p2, Lcom/anythink/core/common/d/l;->f:J

    .line 281
    :goto_0
    invoke-static {p1, p2}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/l;)V

    return-void

    .line 283
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "errorCode:["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lcom/anythink/core/common/d/l;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "],errorMsg:["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p2, Lcom/anythink/core/common/d/l;->errorMsg:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    iget-object p2, p0, Lcom/anythink/core/b/c;->e:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    .line 287
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p2, Lcom/anythink/core/common/d/l;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/anythink/core/common/d/l;->errorMsg:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/anythink/core/b/c;->b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/anythink/core/b/c;->n:Z

    return-void
.end method
