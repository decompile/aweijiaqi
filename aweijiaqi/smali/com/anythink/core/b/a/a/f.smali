.class public final Lcom/anythink/core/b/a/a/f;
.super Lcom/anythink/core/b/a/a/b;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field e:Ljava/lang/String;

.field h:Lcom/anythink/core/api/ATBaseAdAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 3

    .line 29
    invoke-direct {p0, p2, p3}, Lcom/anythink/core/b/a/a/b;-><init>(Ljava/lang/String;Lcom/anythink/core/c/d$b;)V

    .line 32
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :catchall_0
    :try_start_1
    new-instance p2, Lorg/json/JSONObject;

    iget-object v0, p3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-direct {p2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "account_id"

    .line 37
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "app_id"

    .line 38
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "placement_id"

    .line 39
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 41
    iput-object v0, p0, Lcom/anythink/core/b/a/a/f;->a:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/anythink/core/b/a/a/f;->b:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/anythink/core/b/a/a/f;->c:Ljava/lang/String;

    .line 44
    iget p2, p3, Lcom/anythink/core/c/d$b;->b:I

    iput p2, p0, Lcom/anythink/core/b/a/a/f;->d:I

    .line 45
    iput-object p4, p0, Lcom/anythink/core/b/a/a/f;->h:Lcom/anythink/core/api/ATBaseAdAdapter;

    .line 46
    invoke-virtual {p4, p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getBiddingToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/b/a/a/f;->e:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/anythink/core/b/a/a/f;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/anythink/core/b/a/a/f;->h:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lorg/json/JSONObject;
    .locals 3

    .line 70
    invoke-super {p0}, Lcom/anythink/core/b/a/a/b;->c()Lorg/json/JSONObject;

    move-result-object v0

    :try_start_0
    const-string v1, "app_id"

    .line 72
    iget-object v2, p0, Lcom/anythink/core/b/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "unit_id"

    .line 73
    iget-object v2, p0, Lcom/anythink/core/b/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "nw_firm_id"

    .line 74
    iget v2, p0, Lcom/anythink/core/b/a/a/f;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "bid_token"

    .line 75
    iget-object v2, p0, Lcom/anythink/core/b/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "account_id"

    .line 77
    iget-object v2, p0, Lcom/anythink/core/b/a/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/anythink/core/b/a/a/f;->e:Ljava/lang/String;

    return-object v0
.end method
