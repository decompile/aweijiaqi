.class public final Lcom/anythink/core/b/a/a/d;
.super Lcom/anythink/core/b/a/a/b;


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:I

.field h:Ljava/lang/String;

.field i:Lcom/anythink/core/api/ATBaseAdAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/c/d$b;Lcom/anythink/core/api/ATBaseAdAdapter;)V
    .locals 5

    .line 32
    invoke-direct {p0, p2, p3}, Lcom/anythink/core/b/a/a/b;-><init>(Ljava/lang/String;Lcom/anythink/core/c/d$b;)V

    const/4 v0, 0x0

    .line 22
    iput v0, p0, Lcom/anythink/core/b/a/a/d;->a:I

    .line 23
    iput v0, p0, Lcom/anythink/core/b/a/a/d;->b:I

    .line 35
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :catchall_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "appid"

    .line 40
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "appkey"

    .line 41
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "unitid"

    .line 42
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "size"

    .line 43
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    iput-object v2, p0, Lcom/anythink/core/b/a/a/d;->c:Ljava/lang/String;

    .line 46
    iput-object v3, p0, Lcom/anythink/core/b/a/a/d;->d:Ljava/lang/String;

    .line 47
    iget p3, p3, Lcom/anythink/core/c/d$b;->b:I

    iput p3, p0, Lcom/anythink/core/b/a/a/d;->e:I

    .line 48
    iput-object p4, p0, Lcom/anythink/core/b/a/a/d;->i:Lcom/anythink/core/api/ATBaseAdAdapter;

    .line 49
    invoke-virtual {p4, p1}, Lcom/anythink/core/api/ATBaseAdAdapter;->getBiddingToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/b/a/a/d;->h:Ljava/lang/String;

    const-string p1, "2"

    .line 51
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "x"

    .line 52
    invoke-virtual {v1, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 53
    array-length p2, p1

    const/4 p3, 0x2

    if-ne p2, p3, :cond_0

    .line 54
    aget-object p2, p1, v0

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/anythink/core/b/a/a/d;->a:I

    const/4 p2, 0x1

    .line 55
    aget-object p1, p1, p2

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/anythink/core/b/a/a/d;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/anythink/core/b/a/a/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/anythink/core/b/a/a/d;->i:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lorg/json/JSONObject;
    .locals 3

    .line 79
    :try_start_0
    invoke-super {p0}, Lcom/anythink/core/b/a/a/b;->c()Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "display_manager_ver"

    .line 1072
    iget-object v2, p0, Lcom/anythink/core/b/a/a/d;->i:Lcom/anythink/core/api/ATBaseAdAdapter;

    invoke-virtual {v2}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkSDKVersion()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "unit_id"

    .line 81
    iget-object v2, p0, Lcom/anythink/core/b/a/a/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "app_id"

    .line 82
    iget-object v2, p0, Lcom/anythink/core/b/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "nw_firm_id"

    .line 83
    iget v2, p0, Lcom/anythink/core/b/a/a/d;->e:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "buyeruid"

    .line 84
    iget-object v2, p0, Lcom/anythink/core/b/a/a/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ad_format"

    .line 85
    iget-object v2, p0, Lcom/anythink/core/b/a/a/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    iget-object v1, p0, Lcom/anythink/core/b/a/a/d;->f:Ljava/lang/String;

    const-string v2, "2"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ad_width"

    .line 88
    iget v2, p0, Lcom/anythink/core/b/a/a/d;->a:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "ad_height"

    .line 89
    iget v2, p0, Lcom/anythink/core/b/a/a/d;->b:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    :catchall_1
    :cond_0
    :goto_0
    return-object v0
.end method
