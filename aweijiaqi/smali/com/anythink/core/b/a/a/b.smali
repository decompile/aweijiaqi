.class public abstract Lcom/anythink/core/b/a/a/b;
.super Ljava/lang/Object;


# instance fields
.field protected f:Ljava/lang/String;

.field protected g:Lcom/anythink/core/c/d$b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/anythink/core/c/d$b;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/anythink/core/b/a/a/b;->f:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/anythink/core/b/a/a/b;->g:Lcom/anythink/core/c/d$b;

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public c()Lorg/json/JSONObject;
    .locals 3

    .line 29
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "ad_format"

    .line 31
    iget-object v2, p0, Lcom/anythink/core/b/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ad_source_id"

    .line 32
    iget-object v2, p0, Lcom/anythink/core/b/a/a/b;->g:Lcom/anythink/core/c/d$b;

    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 35
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method
