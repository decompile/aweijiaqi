.class public abstract Lcom/anythink/core/b/d;
.super Ljava/lang/Object;


# instance fields
.field protected m:Lcom/anythink/core/common/d/a;

.field protected n:Z


# direct methods
.method public constructor <init>(Lcom/anythink/core/common/d/a;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/anythink/core/b/d;->m:Lcom/anythink/core/common/d/a;

    return-void
.end method

.method protected static a(Ljava/util/List;)Lorg/json/JSONArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .line 45
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 47
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/c/d$b;

    .line 48
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "network_firm_id"

    .line 49
    iget v4, v1, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "ad_source_id"

    .line 50
    iget-object v4, v1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "content"

    .line 51
    iget-object v4, v1, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    iget-object v3, v1, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "error"

    .line 54
    iget-object v1, v1, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    :cond_0
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_1
    return-object v0
.end method

.method protected static a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/l;)V
    .locals 8

    .line 81
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v2, p0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Lcom/anythink/core/common/d/l;->a()Z

    move-result v3

    if-nez v3, :cond_0

    iget-wide v3, p1, Lcom/anythink/core/common/d/l;->price:D

    iget-wide v5, v0, Lcom/anythink/core/common/d/l;->price:D

    cmpg-double v7, v3, v5

    if-gtz v7, :cond_0

    .line 85
    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    return-void

    :cond_0
    const/4 v0, 0x2

    .line 88
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    .line 3211
    iget v1, p0, Lcom/anythink/core/c/d$b;->H:I

    if-eq v1, v0, :cond_1

    .line 90
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v0

    iget-object p0, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, Lcom/anythink/core/b/e;->a(Ljava/lang/String;Lcom/anythink/core/common/d/l;)V

    :cond_1
    return-void
.end method

.method protected static a(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V
    .locals 0

    .line 66
    iput-wide p2, p0, Lcom/anythink/core/c/d$b;->s:J

    const-wide/16 p2, 0x0

    .line 67
    iput-wide p2, p0, Lcom/anythink/core/c/d$b;->m:D

    const/4 p2, -0x1

    .line 68
    iput p2, p0, Lcom/anythink/core/c/d$b;->q:I

    .line 69
    iput p2, p0, Lcom/anythink/core/c/d$b;->a:I

    .line 71
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p1, "bid error"

    .line 2119
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    return-void

    .line 3119
    :cond_0
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    return-void
.end method

.method protected static a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z
    .locals 3

    .line 103
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v0

    iget-object v1, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget v2, p0, Lcom/anythink/core/c/d$b;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/b/e;->b(Ljava/lang/String;I)Lcom/anythink/core/common/d/l;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {v0}, Lcom/anythink/core/common/d/l;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    .line 107
    invoke-virtual {p0, v0, v1, v2, v1}, Lcom/anythink/core/c/d$b;->a(Lcom/anythink/core/common/d/l;III)V

    .line 4119
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    const/4 p0, 0x1

    return p0

    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Lcom/anythink/core/b/b/a;)V
.end method

.method protected abstract a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V
.end method

.method protected a(Z)V
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/anythink/core/b/d;->n:Z

    return-void
.end method
