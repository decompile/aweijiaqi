.class public Lcom/anythink/core/b/a;
.super Lcom/anythink/core/b/d;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/anythink/core/b/b/a;

.field private e:J

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    const-class v0, Lcom/anythink/core/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/anythink/core/common/d/a;)V
    .locals 1

    .line 46
    invoke-direct {p0, p1}, Lcom/anythink/core/b/d;-><init>(Lcom/anythink/core/common/d/a;)V

    .line 43
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/anythink/core/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v0, v0, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/b/a;->b:Ljava/util/List;

    .line 49
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;
    .locals 0

    .line 152
    invoke-static {p0}, Lcom/anythink/core/api/ATBiddingResult;->fail(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/anythink/core/b/a;ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/anythink/core/b/a;->a(ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V

    return-void
.end method

.method private declared-synchronized a(ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V
    .locals 4

    monitor-enter p0

    .line 126
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/anythink/core/b/a;->e:J

    sub-long/2addr v0, v2

    invoke-virtual {p0, p3, p2, v0, v1}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V

    .line 128
    iget-object v0, p0, Lcom/anythink/core/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    .line 129
    iget-object v0, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/anythink/core/b/a;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 131
    iget-object v0, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    .line 133
    iget-object p1, p2, Lcom/anythink/core/api/ATBiddingResult;->errorMsg:Ljava/lang/String;

    invoke-static {p3, p1}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z

    move-result p1

    :cond_0
    if-eqz p1, :cond_1

    .line 136
    iget-object p1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    iget-object p2, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    invoke-interface {p1, p2}, Lcom/anythink/core/b/b/a;->a(Ljava/util/List;)V

    goto :goto_0

    .line 138
    :cond_1
    iget-object p1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    iget-object p2, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    invoke-interface {p1, p2}, Lcom/anythink/core/b/b/a;->b(Ljava/util/List;)V

    .line 141
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 143
    iget-object p1, p0, Lcom/anythink/core/b/a;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_3

    .line 144
    iget-object p1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    if-eqz p1, :cond_3

    .line 145
    iget-object p1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {p1}, Lcom/anythink/core/b/b/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 9

    monitor-enter p0

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_4

    .line 158
    iget-object v0, p0, Lcom/anythink/core/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 160
    sget-object v0, Lcom/anythink/core/b/a;->a:Ljava/lang/String;

    const-string v1, "c2s bid request timeout"

    invoke-static {v0, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 164
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 166
    iget-object v1, p0, Lcom/anythink/core/b/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    const-string v4, "bid timeout"

    .line 168
    invoke-static {v3, v4}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v4, "bid timeout!"

    .line 5152
    invoke-static {v4}, Lcom/anythink/core/api/ATBiddingResult;->fail(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;

    move-result-object v4

    .line 173
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/anythink/core/b/a;->e:J

    sub-long/2addr v5, v7

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V

    .line 174
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/anythink/core/b/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 179
    iget-object v1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    if-eqz v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v1, v0}, Lcom/anythink/core/b/b/a;->a(Ljava/util/List;)V

    .line 181
    iget-object v0, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v0, v2}, Lcom/anythink/core/b/b/a;->b(Ljava/util/List;)V

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/anythink/core/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 186
    iget-object v0, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v0}, Lcom/anythink/core/b/b/a;->a()V

    :cond_3
    const/4 v0, 0x0

    .line 189
    iput-object v0, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lcom/anythink/core/b/b/a;)V
    .locals 9

    .line 54
    iput-object p1, p0, Lcom/anythink/core/b/a;->d:Lcom/anythink/core/b/b/a;

    .line 56
    iget-object p1, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object p1, p1, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 57
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/anythink/core/b/a;->e:J

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 62
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/anythink/core/c/d$b;

    .line 63
    invoke-static {v3}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v4

    if-nez v4, :cond_0

    .line 66
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v3, Lcom/anythink/core/c/d$b;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "not exist!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1152
    invoke-static {v4}, Lcom/anythink/core/api/ATBiddingResult;->fail(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;

    move-result-object v4

    .line 67
    invoke-direct {p0, v1, v4, v3}, Lcom/anythink/core/b/a;->a(ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V

    goto :goto_1

    .line 72
    :cond_0
    :try_start_0
    new-instance v5, Lcom/anythink/core/b/a$1;

    invoke-direct {v5, p0, v3}, Lcom/anythink/core/b/a$1;-><init>(Lcom/anythink/core/b/a;Lcom/anythink/core/c/d$b;)V

    .line 80
    sget-object v6, Lcom/anythink/core/b/a;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "start c2s bid request: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/anythink/core/api/ATBaseAdAdapter;->getNetworkName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v6, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v6, v6, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/anythink/core/c/e;->a(Landroid/content/Context;)Lcom/anythink/core/c/e;

    move-result-object v6

    iget-object v7, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v7, v7, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/anythink/core/c/e;->a(Ljava/lang/String;)Lcom/anythink/core/c/d;

    move-result-object v6

    .line 82
    iget-object v7, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v7, v7, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iget-object v8, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v8, v8, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v3}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;

    move-result-object v6

    .line 83
    iget-object v7, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object v7, v7, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    invoke-virtual {v4, v7, v6, v5}, Lcom/anythink/core/api/ATBaseAdAdapter;->startBiddingRequest(Landroid/content/Context;Ljava/util/Map;Lcom/anythink/core/api/ATBiddingListener;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "This network don\'t support head bidding in current TopOn\'s version."

    .line 2152
    invoke-static {v4}, Lcom/anythink/core/api/ATBiddingResult;->fail(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;

    move-result-object v4

    .line 85
    invoke-direct {p0, v1, v4, v3}, Lcom/anythink/core/b/a;->a(ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    .line 88
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    .line 89
    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 3152
    invoke-static {v4}, Lcom/anythink/core/api/ATBiddingResult;->fail(Ljava/lang/String;)Lcom/anythink/core/api/ATBiddingResult;

    move-result-object v4

    .line 90
    invoke-direct {p0, v1, v4, v3}, Lcom/anythink/core/b/a;->a(ZLcom/anythink/core/api/ATBiddingResult;Lcom/anythink/core/c/d$b;)V

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    return-void
.end method

.method protected final a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V
    .locals 10

    .line 99
    iget-boolean v0, p2, Lcom/anythink/core/common/d/k;->isSuccess:Z

    if-eqz v0, :cond_0

    .line 100
    iput-wide p3, p1, Lcom/anythink/core/c/d$b;->s:J

    .line 103
    new-instance p3, Lcom/anythink/core/common/d/l;

    const/4 v2, 0x1

    iget-wide v3, p2, Lcom/anythink/core/common/d/k;->price:D

    iget-object v5, p2, Lcom/anythink/core/common/d/k;->token:Ljava/lang/String;

    iget-object v6, p2, Lcom/anythink/core/common/d/k;->winNoticeUrl:Ljava/lang/String;

    iget-object v7, p2, Lcom/anythink/core/common/d/k;->loseNoticeUrl:Ljava/lang/String;

    iget-object v8, p2, Lcom/anythink/core/common/d/k;->displayNoticeUrl:Ljava/lang/String;

    const-string v9, ""

    move-object v1, p3

    invoke-direct/range {v1 .. v9}, Lcom/anythink/core/common/d/l;-><init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4011
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->x:J

    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p3, Lcom/anythink/core/common/d/l;->f:J

    .line 5011
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->x:J

    .line 105
    iput-wide v0, p3, Lcom/anythink/core/common/d/l;->e:J

    .line 108
    invoke-static {p1, p3}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/l;)V

    .line 110
    sget-object p2, Lcom/anythink/core/common/b/e$e;->f:Ljava/lang/String;

    iget-object p3, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object p3, p3, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iget-object p4, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget p4, p4, Lcom/anythink/core/common/d/a;->d:I

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-static {p4}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-static {p2, p3, p4, p1}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)V

    return-void

    .line 118
    :cond_0
    iget-object p2, p2, Lcom/anythink/core/common/d/k;->errorMsg:Ljava/lang/String;

    invoke-static {p1, p2, p3, p4}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    .line 120
    sget-object p2, Lcom/anythink/core/common/b/e$e;->g:Ljava/lang/String;

    iget-object p3, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget-object p3, p3, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iget-object p4, p0, Lcom/anythink/core/b/a;->m:Lcom/anythink/core/common/d/a;

    iget p4, p4, Lcom/anythink/core/common/d/a;->d:I

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-static {p4}, Lcom/anythink/core/common/g/g;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-static {p2, p3, p4, p1}, Lcom/anythink/core/common/g/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)V

    return-void
.end method
