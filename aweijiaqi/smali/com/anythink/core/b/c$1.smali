.class final Lcom/anythink/core/b/c$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/anythink/core/common/e/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/anythink/core/b/c;->a(Lcom/anythink/core/b/b/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/anythink/core/b/c;


# direct methods
.method constructor <init>(Lcom/anythink/core/b/c;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .line 90
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    iget-wide v2, v2, Lcom/anythink/core/b/c;->l:J

    sub-long/2addr v0, v2

    .line 1254
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1255
    instance-of v3, p1, Lorg/json/JSONArray;

    if-eqz v3, :cond_0

    .line 1256
    check-cast p1, Lorg/json/JSONArray;

    const/4 v3, 0x0

    .line 1257
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1258
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/common/d/l;->a(Ljava/lang/String;)Lcom/anythink/core/common/d/l;

    move-result-object v4

    .line 1259
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    invoke-static {p1, v2, v0, v1}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/b/c;Ljava/util/List;J)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/anythink/core/api/AdError;)V
    .locals 2

    .line 98
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iget-object v0, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    iget-wide v0, v0, Lcom/anythink/core/b/c;->l:J

    sub-long/2addr p1, v0

    .line 99
    iget-object v0, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/b/c;Ljava/util/List;J)V

    return-void
.end method

.method public final b()V
    .locals 4

    .line 104
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    iget-wide v2, v2, Lcom/anythink/core/b/c;->l:J

    sub-long/2addr v0, v2

    .line 105
    iget-object v2, p0, Lcom/anythink/core/b/c$1;->a:Lcom/anythink/core/b/c;

    const/4 v3, 0x0

    invoke-static {v2, v3, v0, v1}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/b/c;Ljava/util/List;J)V

    return-void
.end method
