.class public final Lcom/anythink/core/b/f;
.super Lcom/anythink/core/b/d;


# instance fields
.field a:Z

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Lcom/anythink/core/b/b/a;


# direct methods
.method public constructor <init>(Lcom/anythink/core/common/d/a;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/anythink/core/b/d;-><init>(Lcom/anythink/core/common/d/a;)V

    const-string p1, "IH Bidding"

    .line 31
    iput-object p1, p0, Lcom/anythink/core/b/f;->b:Ljava/lang/String;

    const/4 p1, 0x0

    .line 34
    iput-boolean p1, p0, Lcom/anythink/core/b/f;->a:Z

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/b/f;Ljava/util/List;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/anythink/core/b/f;->b(Ljava/util/List;)V

    return-void
.end method

.method private static a(Lcom/anythink/core/c/d$b;)V
    .locals 10

    .line 174
    new-instance v9, Lcom/anythink/core/common/d/l;

    iget-wide v2, p0, Lcom/anythink/core/c/d$b;->m:D

    iget-object v4, p0, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    const/4 v1, 0x1

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/anythink/core/common/d/l;-><init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4011
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->x:J

    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, v9, Lcom/anythink/core/common/d/l;->f:J

    .line 5011
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->x:J

    .line 176
    iput-wide v0, v9, Lcom/anythink/core/common/d/l;->e:J

    .line 179
    invoke-static {p0, v9}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/l;)V

    return-void
.end method

.method private static b(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V
    .locals 0

    .line 170
    invoke-static {p0, p1, p2, p3}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    return-void
.end method

.method private declared-synchronized b(Ljava/util/List;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    .line 96
    :try_start_0
    iget-boolean v0, v1, Lcom/anythink/core/b/f;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 97
    monitor-exit p0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 101
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p1

    .line 104
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/anythink/core/b/f;->c:J

    sub-long/2addr v2, v4

    .line 105
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 106
    iget-object v5, v1, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-object v5, v5, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/anythink/core/c/d$b;

    .line 108
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v10, 0x0

    if-eqz v9, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/anythink/core/c/d$b;

    .line 109
    iget-object v11, v6, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget-object v12, v9, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 110
    iput-wide v2, v9, Lcom/anythink/core/c/d$b;->s:J

    .line 111
    iput v10, v9, Lcom/anythink/core/c/d$b;->q:I

    .line 1174
    new-instance v8, Lcom/anythink/core/common/d/l;

    const/4 v13, 0x1

    iget-wide v14, v9, Lcom/anythink/core/c/d$b;->m:D

    iget-object v10, v9, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    move-object v12, v8

    move-object/from16 v16, v10

    invoke-direct/range {v12 .. v20}, Lcom/anythink/core/common/d/l;-><init>(ZDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2011
    iget-wide v10, v9, Lcom/anythink/core/c/d$b;->x:J

    .line 1175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    add-long/2addr v10, v12

    iput-wide v10, v8, Lcom/anythink/core/common/d/l;->f:J

    .line 3011
    iget-wide v10, v9, Lcom/anythink/core/c/d$b;->x:J

    .line 1176
    iput-wide v10, v8, Lcom/anythink/core/common/d/l;->e:J

    .line 1179
    invoke-static {v9, v8}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/l;)V

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_2
    if-nez v7, :cond_2

    const-string v7, "No Bid Info."

    .line 3170
    invoke-static {v6, v7, v2, v3}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;J)V

    const-string v7, "No Bid Info."

    .line 119
    invoke-static {v6, v7}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/c/d$b;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 121
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 123
    :cond_5
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    :cond_6
    iget-boolean v2, v1, Lcom/anythink/core/b/f;->n:Z

    if-eqz v2, :cond_7

    .line 130
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v3, "IH Bidding Success List"

    .line 133
    invoke-static {v0}, Lcom/anythink/core/b/f;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "IH Bidding Fail List"

    .line 134
    invoke-static {v4}, Lcom/anythink/core/b/f;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    :catch_0
    :try_start_3
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    iget-object v3, v1, Lcom/anythink/core/b/f;->b:Ljava/lang/String;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_7
    iget-object v2, v1, Lcom/anythink/core/b/f;->d:Lcom/anythink/core/b/b/a;

    if-eqz v2, :cond_9

    .line 143
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 144
    iget-object v2, v1, Lcom/anythink/core/b/f;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v2, v0}, Lcom/anythink/core/b/b/a;->a(Ljava/util/List;)V

    .line 147
    :cond_8
    iget-object v0, v1, Lcom/anythink/core/b/f;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v0, v4}, Lcom/anythink/core/b/b/a;->b(Ljava/util/List;)V

    .line 148
    iget-object v0, v1, Lcom/anythink/core/b/f;->d:Lcom/anythink/core/b/b/a;

    invoke-interface {v0}, Lcom/anythink/core/b/b/a;->a()V

    .line 151
    :cond_9
    iput-boolean v7, v1, Lcom/anythink/core/b/f;->a:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    const/4 v0, 0x0

    .line 162
    invoke-direct {p0, v0}, Lcom/anythink/core/b/f;->b(Ljava/util/List;)V

    return-void
.end method

.method protected final a(Lcom/anythink/core/b/b/a;)V
    .locals 9

    .line 42
    iput-object p1, p0, Lcom/anythink/core/b/f;->d:Lcom/anythink/core/b/b/a;

    const/4 p1, 0x0

    .line 43
    iput-boolean p1, p0, Lcom/anythink/core/b/f;->a:Z

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anythink/core/b/f;->c:J

    .line 46
    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-object v4, p1, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 48
    iget-boolean p1, p0, Lcom/anythink/core/b/f;->n:Z

    if-eqz p1, :cond_0

    .line 49
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "Start IH Bidding List"

    .line 51
    invoke-static {v4}, Lcom/anythink/core/b/f;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :catch_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    iget-object v0, p0, Lcom/anythink/core/b/f;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/anythink/core/common/b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/b/e;->b()Lcom/anythink/core/api/MediationBidManager;

    move-result-object p1

    if-nez p1, :cond_2

    .line 59
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anythink/core/c/d$b;

    .line 60
    iget v1, v0, Lcom/anythink/core/c/d$b;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 63
    invoke-static {v0}, Lcom/anythink/core/common/g/i;->a(Lcom/anythink/core/c/d$b;)Lcom/anythink/core/api/ATBaseAdAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 65
    invoke-virtual {v0}, Lcom/anythink/core/api/ATBaseAdAdapter;->getBidManager()Lcom/anythink/core/api/MediationBidManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 67
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/anythink/core/b/e;->a(Lcom/anythink/core/api/MediationBidManager;)V

    goto :goto_0

    .line 73
    :cond_2
    invoke-static {}, Lcom/anythink/core/b/e;->a()Lcom/anythink/core/b/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/anythink/core/b/e;->b()Lcom/anythink/core/api/MediationBidManager;

    move-result-object v0

    if-nez v0, :cond_3

    .line 75
    iget-object p1, p0, Lcom/anythink/core/b/f;->b:Ljava/lang/String;

    const-string v0, "No BidManager."

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    .line 76
    invoke-direct {p0, p1}, Lcom/anythink/core/b/f;->b(Ljava/util/List;)V

    return-void

    :cond_3
    const-string p1, "https://bidding.anythinktech.com"

    .line 80
    invoke-virtual {v0, p1}, Lcom/anythink/core/api/MediationBidManager;->setBidRequestUrl(Ljava/lang/String;)V

    .line 81
    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-object v1, p1, Lcom/anythink/core/common/d/a;->a:Landroid/content/Context;

    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget v2, p1, Lcom/anythink/core/common/d/a;->d:I

    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-object v3, p1, Lcom/anythink/core/common/d/a;->c:Ljava/lang/String;

    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-object v5, p1, Lcom/anythink/core/common/d/a;->h:Ljava/util/List;

    new-instance v6, Lcom/anythink/core/b/f$1;

    invoke-direct {v6, p0}, Lcom/anythink/core/b/f$1;-><init>(Lcom/anythink/core/b/f;)V

    iget-object p1, p0, Lcom/anythink/core/b/f;->m:Lcom/anythink/core/common/d/a;

    iget-wide v7, p1, Lcom/anythink/core/common/d/a;->f:J

    invoke-virtual/range {v0 .. v8}, Lcom/anythink/core/api/MediationBidManager;->startBid(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/anythink/core/api/MediationBidManager$BidListener;J)V

    return-void
.end method

.method protected final a(Lcom/anythink/core/c/d$b;Lcom/anythink/core/common/d/k;J)V
    .locals 0

    return-void
.end method
