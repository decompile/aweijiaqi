.class public Lcom/anythink/core/b/b;
.super Lcom/anythink/core/common/g/a;

# interfaces
.implements Lcom/anythink/core/common/h$b;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/anythink/core/common/h$a;

.field private e:Lcom/anythink/core/b/c;

.field private f:Lcom/anythink/core/b/a;

.field private g:Lcom/anythink/core/b/f;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:J

.field private m:J

.field private n:Z

.field private o:Z

.field private p:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    const-class v0, Lcom/anythink/core/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/b/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/anythink/core/common/d/a;)V
    .locals 9

    .line 45
    invoke-direct {p0}, Lcom/anythink/core/common/g/a;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    .line 47
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/anythink/core/b/b;->k:Ljava/lang/String;

    .line 48
    iget-wide v0, p1, Lcom/anythink/core/common/d/a;->i:J

    iput-wide v0, p0, Lcom/anythink/core/b/b;->l:J

    .line 49
    iget-wide v0, p1, Lcom/anythink/core/common/d/a;->e:J

    iput-wide v0, p0, Lcom/anythink/core/b/b;->m:J

    .line 51
    iget-object v0, p1, Lcom/anythink/core/common/d/a;->g:Ljava/util/List;

    .line 53
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v3, v2

    move-object v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v1, :cond_7

    .line 61
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/anythink/core/c/d$b;

    .line 63
    iget v7, v6, Lcom/anythink/core/c/d$b;->K:I

    sget v8, Lcom/anythink/core/c/d$b;->M:I

    if-eq v7, v8, :cond_4

    iget v7, v6, Lcom/anythink/core/c/d$b;->K:I

    sget v8, Lcom/anythink/core/c/d$b;->O:I

    if-ne v7, v8, :cond_0

    goto :goto_1

    .line 69
    :cond_0
    iget v7, v6, Lcom/anythink/core/c/d$b;->K:I

    sget v8, Lcom/anythink/core/c/d$b;->N:I

    if-ne v7, v8, :cond_2

    if-nez v3, :cond_1

    .line 71
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    :cond_1
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 74
    :cond_2
    iget v7, v6, Lcom/anythink/core/c/d$b;->K:I

    sget v8, Lcom/anythink/core/c/d$b;->Q:I

    if-ne v7, v8, :cond_6

    if-nez v4, :cond_3

    .line 76
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    :cond_3
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    :goto_1
    if-nez v2, :cond_5

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    :cond_5
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_7
    const/4 v0, 0x1

    if-eqz v2, :cond_8

    .line 83
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 84
    new-instance v1, Lcom/anythink/core/b/c;

    invoke-virtual {p1, v2}, Lcom/anythink/core/common/d/a;->a(Ljava/util/List;)Lcom/anythink/core/common/d/a;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/anythink/core/b/c;-><init>(Lcom/anythink/core/common/d/a;)V

    iput-object v1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    goto :goto_3

    .line 86
    :cond_8
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->i:Z

    :goto_3
    if-eqz v3, :cond_9

    .line 89
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 90
    new-instance v1, Lcom/anythink/core/b/a;

    invoke-virtual {p1, v3}, Lcom/anythink/core/common/d/a;->b(Ljava/util/List;)Lcom/anythink/core/common/d/a;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/anythink/core/b/a;-><init>(Lcom/anythink/core/common/d/a;)V

    iput-object v1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    goto :goto_4

    .line 92
    :cond_9
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->h:Z

    :goto_4
    if-eqz v4, :cond_a

    .line 95
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 96
    new-instance v0, Lcom/anythink/core/b/f;

    invoke-virtual {p1, v4}, Lcom/anythink/core/common/d/a;->b(Ljava/util/List;)Lcom/anythink/core/common/d/a;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/anythink/core/b/f;-><init>(Lcom/anythink/core/common/d/a;)V

    iput-object v0, p0, Lcom/anythink/core/b/b;->g:Lcom/anythink/core/b/f;

    return-void

    .line 98
    :cond_a
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->j:Z

    return-void
.end method

.method static synthetic a(Lcom/anythink/core/b/b;ZLjava/util/List;Z)V
    .locals 0

    .line 3241
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 3244
    :try_start_0
    iget-object p1, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 3246
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 3248
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3251
    iget-boolean p1, p0, Lcom/anythink/core/b/b;->o:Z

    if-nez p1, :cond_1

    if-eqz p3, :cond_2

    .line 3252
    :cond_1
    invoke-direct {p0}, Lcom/anythink/core/b/b;->d()V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    .line 3248
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private a(ZLjava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;Z)V"
        }
    .end annotation

    .line 241
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 244
    :try_start_0
    iget-object p1, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 246
    :cond_0
    iget-object p1, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 248
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    iget-boolean p1, p0, Lcom/anythink/core/b/b;->o:Z

    if-nez p1, :cond_1

    if-eqz p3, :cond_2

    .line 252
    :cond_1
    invoke-direct {p0}, Lcom/anythink/core/b/b;->d()V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    .line 248
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/anythink/core/b/b;)Z
    .locals 1

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/anythink/core/b/b;)V
    .locals 3

    .line 3279
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->i:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/anythink/core/b/b;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/anythink/core/b/b;->j:Z

    if-eqz v0, :cond_4

    .line 3281
    iget-object v0, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3282
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 3283
    iput-object v1, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 3286
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/core/b/b;->a()V

    .line 3288
    invoke-direct {p0}, Lcom/anythink/core/b/b;->d()V

    .line 3290
    iget-object v0, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    if-eqz v0, :cond_1

    .line 3291
    iget-object v2, p0, Lcom/anythink/core/b/b;->k:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/anythink/core/common/h$a;->a(Ljava/lang/String;)V

    .line 3299
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    if-eqz v0, :cond_2

    .line 3300
    iput-object v1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    .line 3302
    :cond_2
    iget-object v0, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    if-eqz v0, :cond_3

    .line 3303
    iput-object v1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    .line 3305
    :cond_3
    iput-object v1, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    :cond_4
    return-void
.end method

.method private c()V
    .locals 7

    .line 187
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 188
    new-instance v0, Lcom/anythink/core/b/b$4;

    invoke-direct {v0, p0}, Lcom/anythink/core/b/b$4;-><init>(Lcom/anythink/core/b/b;)V

    .line 195
    iget-object v1, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 2232
    iget-wide v2, p0, Lcom/anythink/core/b/b;->l:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-gtz v6, :cond_0

    const-wide/16 v2, 0x7d0

    .line 195
    :cond_0
    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method static synthetic c(Lcom/anythink/core/b/b;)Z
    .locals 1

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->h:Z

    return v0
.end method

.method private declared-synchronized d()V
    .locals 4

    monitor-enter p0

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v0, :cond_0

    if-lez v1, :cond_4

    .line 204
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 205
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 207
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-lez v0, :cond_1

    .line 209
    :try_start_1
    iget-object v0, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 210
    iget-object v0, p0, Lcom/anythink/core/b/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :cond_1
    :goto_0
    if-lez v1, :cond_2

    .line 214
    iget-object v0, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 215
    iget-object v0, p0, Lcom/anythink/core/b/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 217
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    :try_start_2
    iget-object v0, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    if-eqz v0, :cond_4

    .line 220
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 221
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 222
    iget-object v0, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    iget-object v1, p0, Lcom/anythink/core/b/b;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/anythink/core/common/h$a;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 224
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 225
    iget-object v0, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    iget-object v1, p0, Lcom/anythink/core/b/b;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcom/anythink/core/common/h$a;->b(Ljava/lang/String;Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229
    :cond_4
    monitor-exit p0

    return-void

    .line 217
    :goto_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/anythink/core/b/b;)Z
    .locals 1

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->j:Z

    return v0
.end method

.method private e()J
    .locals 5

    .line 232
    iget-wide v0, p0, Lcom/anythink/core/b/b;->l:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const-wide/16 v0, 0x7d0

    :cond_0
    return-wide v0
.end method

.method static synthetic e(Lcom/anythink/core/b/b;)Z
    .locals 1

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/anythink/core/b/b;->o:Z

    return v0
.end method

.method private f()V
    .locals 3

    .line 279
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->i:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/anythink/core/b/b;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/anythink/core/b/b;->j:Z

    if-eqz v0, :cond_4

    .line 281
    iget-object v0, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 283
    iput-object v1, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/anythink/core/b/b;->a()V

    .line 288
    invoke-direct {p0}, Lcom/anythink/core/b/b;->d()V

    .line 290
    iget-object v0, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    if-eqz v0, :cond_1

    .line 291
    iget-object v2, p0, Lcom/anythink/core/b/b;->k:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/anythink/core/common/h$a;->a(Ljava/lang/String;)V

    .line 2299
    :cond_1
    iget-object v0, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    if-eqz v0, :cond_2

    .line 2300
    iput-object v1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    .line 2302
    :cond_2
    iget-object v0, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    if-eqz v0, :cond_3

    .line 2303
    iput-object v1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    .line 2305
    :cond_3
    iput-object v1, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    :cond_4
    return-void
.end method

.method static synthetic f(Lcom/anythink/core/b/b;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/anythink/core/b/b;->d()V

    return-void
.end method

.method private g()V
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 300
    iput-object v1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    if-eqz v0, :cond_1

    .line 303
    iput-object v1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    .line 305
    :cond_1
    iput-object v1, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/anythink/core/common/h$a;)V
    .locals 6

    .line 109
    iput-object p1, p0, Lcom/anythink/core/b/b;->d:Lcom/anythink/core/common/h$a;

    .line 1187
    new-instance p1, Ljava/util/Timer;

    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 1188
    new-instance p1, Lcom/anythink/core/b/b$4;

    invoke-direct {p1, p0}, Lcom/anythink/core/b/b$4;-><init>(Lcom/anythink/core/b/b;)V

    .line 1195
    iget-object v0, p0, Lcom/anythink/core/b/b;->p:Ljava/util/Timer;

    .line 1232
    iget-wide v1, p0, Lcom/anythink/core/b/b;->l:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-gtz v5, :cond_0

    const-wide/16 v1, 0x7d0

    .line 1195
    :cond_0
    invoke-virtual {v0, p1, v1, v2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 115
    iget-wide v0, p0, Lcom/anythink/core/b/b;->m:J

    invoke-super {p0, v0, v1}, Lcom/anythink/core/common/g/a;->a(J)V

    .line 117
    iget-object p1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    if-eqz p1, :cond_1

    .line 119
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->n:Z

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/c;->a(Z)V

    .line 120
    iget-object p1, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    new-instance v0, Lcom/anythink/core/b/b$1;

    invoke-direct {v0, p0}, Lcom/anythink/core/b/b$1;-><init>(Lcom/anythink/core/b/b;)V

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/c;->a(Lcom/anythink/core/b/b/a;)V

    .line 139
    :cond_1
    iget-object p1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    if-eqz p1, :cond_2

    .line 141
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->n:Z

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/a;->a(Z)V

    .line 142
    iget-object p1, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    new-instance v0, Lcom/anythink/core/b/b$2;

    invoke-direct {v0, p0}, Lcom/anythink/core/b/b$2;-><init>(Lcom/anythink/core/b/b;)V

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/a;->a(Lcom/anythink/core/b/b/a;)V

    .line 161
    :cond_2
    iget-object p1, p0, Lcom/anythink/core/b/b;->g:Lcom/anythink/core/b/f;

    if-eqz p1, :cond_3

    .line 162
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->n:Z

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/f;->a(Z)V

    .line 163
    iget-object p1, p0, Lcom/anythink/core/b/b;->g:Lcom/anythink/core/b/f;

    new-instance v0, Lcom/anythink/core/b/b$3;

    invoke-direct {v0, p0}, Lcom/anythink/core/b/b$3;-><init>(Lcom/anythink/core/b/b;)V

    invoke-virtual {p1, v0}, Lcom/anythink/core/b/f;->a(Lcom/anythink/core/b/b/a;)V

    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 104
    iput-boolean p1, p0, Lcom/anythink/core/b/b;->n:Z

    return-void
.end method

.method protected final b()V
    .locals 1

    .line 258
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->i:Z

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/anythink/core/b/b;->e:Lcom/anythink/core/b/c;

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Lcom/anythink/core/b/c;->a()V

    .line 264
    :cond_0
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->h:Z

    if-nez v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/anythink/core/b/b;->f:Lcom/anythink/core/b/a;

    if-eqz v0, :cond_1

    .line 266
    invoke-virtual {v0}, Lcom/anythink/core/b/a;->a()V

    .line 270
    :cond_1
    iget-boolean v0, p0, Lcom/anythink/core/b/b;->j:Z

    if-nez v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/anythink/core/b/b;->g:Lcom/anythink/core/b/f;

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {v0}, Lcom/anythink/core/b/f;->a()V

    :cond_2
    return-void
.end method
