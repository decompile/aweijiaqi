.class public Lcom/anythink/core/c/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/core/c/b$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static c:Lcom/anythink/core/c/b;

.field private static d:Lcom/anythink/core/c/a;


# instance fields
.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field private f:Z

.field private g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    const-class v0, Lcom/anythink/core/c/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/anythink/core/c/b;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 40
    sput-object v0, Lcom/anythink/core/c/b;->c:Lcom/anythink/core/c/b;

    .line 41
    sput-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    .line 50
    iput-object p1, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    const/4 p1, 0x0

    .line 51
    iput-boolean p1, p0, Lcom/anythink/core/c/b;->f:Z

    .line 52
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    return-void
.end method

.method public static a()J
    .locals 5

    .line 173
    sget-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->v()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    goto :goto_0

    .line 176
    :cond_0
    sget-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    invoke-virtual {v0}, Lcom/anythink/core/c/a;->v()J

    move-result-wide v0

    return-wide v0

    :cond_1
    :goto_0
    const-wide/32 v0, 0xc800

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcom/anythink/core/c/a;
    .locals 2

    .line 216
    invoke-static {p0}, Lcom/anythink/core/common/c/c;->a(Landroid/content/Context;)Lcom/anythink/core/common/c/c;

    move-result-object p0

    invoke-static {p0}, Lcom/anythink/core/common/c/d;->a(Lcom/anythink/core/common/c/b;)Lcom/anythink/core/common/c/d;

    move-result-object p0

    const-string v0, "AP_SY"

    invoke-virtual {p0, p1, v0}, Lcom/anythink/core/common/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    const/4 p1, 0x0

    if-eqz p0, :cond_0

    .line 218
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 219
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/anythink/core/common/d/y;

    if-eqz p0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/anythink/core/common/d/y;->d()Ljava/lang/String;

    move-result-object p1

    .line 222
    invoke-static {p1}, Lcom/anythink/core/c/a;->e(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/anythink/core/common/d/y;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/anythink/core/c/a;->a(J)V

    :cond_0
    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/anythink/core/c/a;
    .locals 2

    .line 237
    invoke-static {p0}, Lcom/anythink/core/common/c/c;->a(Landroid/content/Context;)Lcom/anythink/core/common/c/c;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/c/d;->a(Lcom/anythink/core/common/c/b;)Lcom/anythink/core/common/c/d;

    move-result-object v0

    const-string v1, "AP_SY"

    invoke-virtual {v0, p1, p2, v1}, Lcom/anythink/core/common/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    .line 238
    invoke-static {p2}, Lcom/anythink/core/c/a;->e(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object p1

    .line 239
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/anythink/core/c/a;->a(J)V

    .line 240
    sget-object p2, Lcom/anythink/core/common/b/e;->n:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/anythink/core/c/a;->H()I

    move-result v0

    const-string v1, "EU_INFO"

    invoke-static {p0, p2, v1, v0}, Lcom/anythink/core/common/g/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1
.end method

.method static synthetic a(Lcom/anythink/core/c/a;)Lcom/anythink/core/c/a;
    .locals 0

    .line 38
    sput-object p0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    return-object p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/anythink/core/c/b;
    .locals 2

    const-class v0, Lcom/anythink/core/c/b;

    monitor-enter v0

    .line 56
    :try_start_0
    sget-object v1, Lcom/anythink/core/c/b;->c:Lcom/anythink/core/c/b;

    if-nez v1, :cond_1

    .line 57
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 58
    :try_start_1
    sget-object v1, Lcom/anythink/core/c/b;->c:Lcom/anythink/core/c/b;

    if-nez v1, :cond_0

    .line 59
    new-instance v1, Lcom/anythink/core/c/b;

    invoke-direct {v1, p0}, Lcom/anythink/core/c/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/anythink/core/c/b;->c:Lcom/anythink/core/c/b;

    .line 61
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p0

    .line 63
    :cond_1
    :goto_0
    sget-object p0, Lcom/anythink/core/c/b;->c:Lcom/anythink/core/c/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    return-object p0

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/core/c/b;Ljava/lang/String;)V
    .locals 3

    .line 4083
    iget-object v0, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 4084
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/b$a;

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 4087
    invoke-interface {v2}, Lcom/anythink/core/c/b$a;->a()V

    goto :goto_0

    .line 4089
    :cond_1
    invoke-interface {v2, p1}, Lcom/anythink/core/c/b$a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 4094
    :cond_2
    iget-object p0, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 4095
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static synthetic a(Lcom/anythink/core/c/b;)Z
    .locals 1

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lcom/anythink/core/c/b;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/anythink/core/c/b;)Landroid/content/Context;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    return-object p0
.end method

.method private b(Landroid/content/Context;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    return-void
.end method

.method static synthetic c()Lcom/anythink/core/c/a;
    .locals 1

    .line 38
    sget-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .line 83
    iget-object v0, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/b$a;

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 87
    invoke-interface {v2}, Lcom/anythink/core/c/b$a;->a()V

    goto :goto_0

    .line 89
    :cond_1
    invoke-interface {v2, p1}, Lcom/anythink/core/c/b$a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    iget-object p1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 95
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private d()Landroid/content/Context;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    return-object v0
.end method

.method private static e()Lcom/anythink/core/c/a;
    .locals 3

    .line 184
    new-instance v0, Lcom/anythink/core/c/a;

    invoke-direct {v0}, Lcom/anythink/core/c/a;-><init>()V

    const/4 v1, 0x1

    .line 185
    iput-boolean v1, v0, Lcom/anythink/core/c/a;->b:Z

    .line 186
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->D()V

    const-string v1, "0"

    .line 187
    invoke-virtual {v0, v1}, Lcom/anythink/core/c/a;->b(Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/c/a;->a(J)V

    .line 189
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->J()V

    .line 191
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->M()V

    .line 192
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->O()V

    const-string v1, ""

    .line 193
    invoke-virtual {v0, v1}, Lcom/anythink/core/c/a;->c(Ljava/lang/String;)V

    .line 195
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->R()V

    .line 196
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->T()V

    .line 197
    invoke-virtual {v0, v1}, Lcom/anythink/core/c/a;->d(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->B()V

    .line 200
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->w()V

    .line 202
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->o()V

    .line 203
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->q()V

    const-string v1, "[\"com.anythink\"]"

    .line 205
    invoke-virtual {v0, v1}, Lcom/anythink/core/c/a;->a(Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0}, Lcom/anythink/core/c/a;->k()V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/anythink/core/c/a;)V
    .locals 1

    if-eqz p2, :cond_1

    .line 301
    invoke-virtual {p2}, Lcom/anythink/core/c/a;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 305
    :cond_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    new-instance v0, Lcom/anythink/core/c/b$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/anythink/core/c/b$2;-><init>(Lcom/anythink/core/c/b;Lcom/anythink/core/c/a;Landroid/content/Context;)V

    invoke-static {v0}, Lcom/anythink/core/common/b/g;->b(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lcom/anythink/core/c/b$a;)V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    .line 252
    :try_start_0
    iget-boolean v0, p0, Lcom/anythink/core/c/b;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 253
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 255
    :try_start_1
    iput-boolean v0, p0, Lcom/anythink/core/c/b;->f:Z

    .line 256
    new-instance v0, Lcom/anythink/core/common/e/d;

    iget-object v1, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/anythink/core/common/e/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 257
    new-instance v1, Lcom/anythink/core/c/b$1;

    invoke-direct {v1, p0, p1}, Lcom/anythink/core/c/b$1;-><init>(Lcom/anythink/core/c/b;Ljava/lang/String;)V

    invoke-virtual {v0, p2, v1}, Lcom/anythink/core/common/e/d;->a(ILcom/anythink/core/common/e/g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 10

    .line 111
    invoke-virtual {p0, p1}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p1, :cond_4

    .line 113
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->a()Lcom/anythink/core/c/c;

    move-result-object v1

    .line 114
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->C()J

    move-result-wide v2

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 116
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->x()J

    move-result-wide v6

    add-long/2addr v6, v2

    const/4 v2, 0x0

    cmp-long v3, v6, v4

    if-gtz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 122
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->x()J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/anythink/core/c/c;->c()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v1, v6, v4

    if-gtz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 1668
    :goto_1
    iget-object p1, p1, Lcom/anythink/core/c/a;->c:Ljava/util/Map;

    .line 127
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->h()Ljava/util/Map;

    move-result-object v4

    if-eqz p1, :cond_2

    .line 129
    invoke-interface {p1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v0

    goto :goto_2

    :cond_2
    if-eqz v4, :cond_3

    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-nez v3, :cond_4

    if-nez v1, :cond_4

    if-nez p1, :cond_4

    return v2

    .line 138
    :cond_4
    sget-object p1, Lcom/anythink/core/c/b;->a:Ljava/lang/String;

    const-string v1, "app Settings timeout or not exists"

    invoke-static {p1, v1}, Lcom/anythink/core/common/g/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Lcom/anythink/core/c/a;
    .locals 3

    monitor-enter p0

    .line 149
    :try_start_0
    sget-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 151
    :try_start_1
    iget-object v0, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 152
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/anythink/core/c/b;->e:Landroid/content/Context;

    .line 2216
    invoke-static {v0}, Lcom/anythink/core/common/c/c;->a(Landroid/content/Context;)Lcom/anythink/core/common/c/c;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/c/d;->a(Lcom/anythink/core/common/c/b;)Lcom/anythink/core/common/c/d;

    move-result-object v0

    const-string v1, "AP_SY"

    invoke-virtual {v0, p1, v1}, Lcom/anythink/core/common/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 2218
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    .line 2219
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/anythink/core/common/d/y;

    if-eqz p1, :cond_1

    .line 2221
    invoke-virtual {p1}, Lcom/anythink/core/common/d/y;->d()Ljava/lang/String;

    move-result-object v0

    .line 2222
    invoke-static {v0}, Lcom/anythink/core/c/a;->e(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2224
    invoke-virtual {p1}, Lcom/anythink/core/common/d/y;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/anythink/core/c/a;->a(J)V

    .line 155
    :cond_1
    sput-object v0, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;

    if-nez v0, :cond_2

    .line 3184
    new-instance p1, Lcom/anythink/core/c/a;

    invoke-direct {p1}, Lcom/anythink/core/c/a;-><init>()V

    const/4 v0, 0x1

    .line 3185
    iput-boolean v0, p1, Lcom/anythink/core/c/a;->b:Z

    .line 3186
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->D()V

    const-string v0, "0"

    .line 3187
    invoke-virtual {p1, v0}, Lcom/anythink/core/c/a;->b(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 3188
    invoke-virtual {p1, v0, v1}, Lcom/anythink/core/c/a;->a(J)V

    .line 3189
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->J()V

    .line 3191
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->M()V

    .line 3192
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->O()V

    const-string v0, ""

    .line 3193
    invoke-virtual {p1, v0}, Lcom/anythink/core/c/a;->c(Ljava/lang/String;)V

    .line 3195
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->R()V

    .line 3196
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->T()V

    const-string v0, ""

    .line 3197
    invoke-virtual {p1, v0}, Lcom/anythink/core/c/a;->d(Ljava/lang/String;)V

    .line 3199
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->B()V

    .line 3200
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->w()V

    .line 3202
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->o()V

    .line 3203
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->q()V

    const-string v0, "[\"com.anythink\"]"

    .line 3205
    invoke-virtual {p1, v0}, Lcom/anythink/core/c/a;->a(Ljava/lang/String;)V

    .line 3206
    invoke-virtual {p1}, Lcom/anythink/core/c/a;->k()V

    .line 156
    sput-object p1, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    :catch_0
    :cond_2
    :try_start_2
    sget-object p1, Lcom/anythink/core/c/b;->d:Lcom/anythink/core/c/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b(Lcom/anythink/core/c/b$a;)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/anythink/core/c/b;->g:Ljava/lang/Object;

    monitor-enter v0

    .line 101
    :try_start_0
    iget-object v1, p0, Lcom/anythink/core/c/b;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 103
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final b()Z
    .locals 1

    .line 245
    iget-boolean v0, p0, Lcom/anythink/core/c/b;->f:Z

    return v0
.end method
