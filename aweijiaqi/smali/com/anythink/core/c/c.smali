.class public final Lcom/anythink/core/c/c;
.super Ljava/lang/Object;


# static fields
.field private static m:Ljava/lang/String; = "ofm_logger"

.field private static n:Ljava/lang/String; = "ofm_tk_sw"

.field private static o:Ljava/lang/String; = "ofm_da_sw"

.field private static p:Ljava/lang/String; = "tk_address"

.field private static q:Ljava/lang/String; = "tk_max_amount"

.field private static r:Ljava/lang/String; = "tk_interval"

.field private static s:Ljava/lang/String; = "da_rt_keys_ft"

.field private static t:Ljava/lang/String; = "tk_no_t_ft"

.field private static u:Ljava/lang/String; = "da_not_keys_ft"

.field private static v:Ljava/lang/String; = "ofm_system"

.field private static w:Ljava/lang/String; = "ofm_tid"

.field private static x:Ljava/lang/String; = "ofm_firm_info"

.field private static y:Ljava/lang/String; = "ofm_st_vt"


# instance fields
.field a:Ljava/lang/String;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I

.field private e:I

.field private f:J

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:I

.field private k:J

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anythink/core/c/c;->a:Ljava/lang/String;

    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/anythink/core/c/c;
    .locals 5

    .line 113
    :try_start_0
    new-instance v0, Lcom/anythink/core/c/c;

    invoke-direct {v0}, Lcom/anythink/core/c/c;-><init>()V

    .line 114
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 116
    sget-object p0, Lcom/anythink/core/c/c;->v:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v0, Lcom/anythink/core/c/c;->d:I

    .line 117
    sget-object p0, Lcom/anythink/core/c/c;->w:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v0, Lcom/anythink/core/c/c;->e:I

    .line 118
    sget-object p0, Lcom/anythink/core/c/c;->y:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/anythink/core/c/c;->f:J

    .line 119
    sget-object p0, Lcom/anythink/core/c/c;->x:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/anythink/core/common/g/h;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p0

    iput-object p0, v0, Lcom/anythink/core/c/c;->b:Ljava/util/Map;

    .line 121
    sget-object p0, Lcom/anythink/core/c/c;->m:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 123
    sget-object v1, Lcom/anythink/core/c/c;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/anythink/core/c/c;->g:I

    .line 124
    sget-object v1, Lcom/anythink/core/c/c;->o:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/anythink/core/c/c;->h:I

    .line 125
    sget-object v1, Lcom/anythink/core/c/c;->p:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/c/c;->i:Ljava/lang/String;

    .line 126
    sget-object v1, Lcom/anythink/core/c/c;->q:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/anythink/core/c/c;->j:I

    .line 127
    sget-object v1, Lcom/anythink/core/c/c;->r:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/anythink/core/c/c;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 130
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    sget-object v2, Lcom/anythink/core/c/c;->t:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object p0

    .line 133
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 135
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 136
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 137
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 140
    :cond_0
    iput-object v2, v0, Lcom/anythink/core/c/c;->l:Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    :cond_1
    return-object v0

    :catchall_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static c(Ljava/lang/String;)Lcom/anythink/core/c/c;
    .locals 3

    .line 157
    :try_start_0
    new-instance v0, Lcom/anythink/core/c/c;

    invoke-direct {v0}, Lcom/anythink/core/c/c;-><init>()V

    .line 158
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 159
    iput-boolean p0, v0, Lcom/anythink/core/c/c;->c:Z

    .line 160
    sget-object v2, Lcom/anythink/core/c/c;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lcom/anythink/core/c/c;->d:I

    .line 161
    sget-object v2, Lcom/anythink/core/c/c;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/anythink/core/c/c;->b:Ljava/util/Map;

    .line 163
    iput p0, v0, Lcom/anythink/core/c/c;->g:I

    .line 164
    iput p0, v0, Lcom/anythink/core/c/c;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/anythink/core/c/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method private i()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/anythink/core/c/c;->j:I

    return v0
.end method

.method private j()J
    .locals 2

    .line 81
    iget-wide v0, p0, Lcom/anythink/core/c/c;->k:J

    return-wide v0
.end method

.method private k()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/anythink/core/c/c;->l:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/anythink/core/c/c;->d:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/anythink/core/c/c;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/anythink/core/c/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/anythink/core/common/g/h;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    .line 42
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final b()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/anythink/core/c/c;->e:I

    return v0
.end method

.method public final c()J
    .locals 2

    .line 61
    iget-wide v0, p0, Lcom/anythink/core/c/c;->f:J

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/anythink/core/c/c;->g:I

    return v0
.end method

.method public final e()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/anythink/core/c/c;->h:I

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/anythink/core/c/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/anythink/core/c/c;->c:Z

    return v0
.end method
