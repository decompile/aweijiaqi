.class public final Lcom/anythink/core/c/d$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/anythink/core/c/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/anythink/core/c/d$b;",
        ">;"
    }
.end annotation


# static fields
.field public static L:I = 0x0

.field public static M:I = 0x1

.field public static N:I = 0x2

.field public static O:I = 0x3

.field public static P:I = 0x4

.field public static Q:I = 0x5


# instance fields
.field public A:I

.field public B:I

.field public C:Ljava/lang/String;

.field public D:J

.field public E:J

.field public F:D

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field private R:J

.field private S:J

.field private T:I

.field private U:Lcom/anythink/core/common/d/l;

.field private V:I

.field private W:I

.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:J

.field public j:Ljava/lang/String;

.field public k:I

.field public l:I

.field public m:D

.field public n:I

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:I

.field public r:J

.field public s:J

.field public t:Ljava/lang/String;

.field public u:J

.field public v:J

.field public w:J

.field public x:J

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private A()J
    .locals 2

    .line 1183
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->D:J

    return-wide v0
.end method

.method private B()J
    .locals 2

    .line 1187
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->E:J

    return-wide v0
.end method

.method private C()D
    .locals 2

    .line 1195
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->F:D

    return-wide v0
.end method

.method private D()I
    .locals 1

    .line 1203
    iget v0, p0, Lcom/anythink/core/c/d$b;->G:I

    return v0
.end method

.method private E()I
    .locals 1

    .line 1211
    iget v0, p0, Lcom/anythink/core/c/d$b;->H:I

    return v0
.end method

.method private F()I
    .locals 1

    .line 1219
    iget v0, p0, Lcom/anythink/core/c/d$b;->I:I

    return v0
.end method

.method private a(Lcom/anythink/core/c/d$b;)I
    .locals 4

    .line 1256
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->m:D

    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private a(D)V
    .locals 0

    .line 1095
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->m:D

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/l;)V
    .locals 0

    .line 1231
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->U:Lcom/anythink/core/common/d/l;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .line 1063
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    return-void
.end method

.method private b(D)V
    .locals 0

    .line 1199
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->F:D

    return-void
.end method

.method private b(I)V
    .locals 0

    .line 1007
    iput p1, p0, Lcom/anythink/core/c/d$b;->J:I

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    .line 1071
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->j:Ljava/lang/String;

    return-void
.end method

.method private c(I)V
    .locals 0

    .line 1023
    iput p1, p0, Lcom/anythink/core/c/d$b;->q:I

    return-void
.end method

.method private c(J)V
    .locals 0

    .line 1015
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->x:J

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 0

    .line 1111
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    return-void
.end method

.method private d(I)V
    .locals 0

    .line 1079
    iput p1, p0, Lcom/anythink/core/c/d$b;->k:I

    return-void
.end method

.method private d(J)V
    .locals 0

    .line 1055
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->i:J

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 0

    .line 1119
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    return-void
.end method

.method private e(I)V
    .locals 0

    .line 1087
    iput p1, p0, Lcom/anythink/core/c/d$b;->l:I

    return-void
.end method

.method private e(J)V
    .locals 0

    .line 1103
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->u:J

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 0

    .line 1175
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->C:Ljava/lang/String;

    return-void
.end method

.method private f(I)V
    .locals 0

    .line 1143
    iput p1, p0, Lcom/anythink/core/c/d$b;->y:I

    return-void
.end method

.method private f(J)V
    .locals 0

    .line 1123
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->v:J

    return-void
.end method

.method private g(I)V
    .locals 0

    .line 1151
    iput p1, p0, Lcom/anythink/core/c/d$b;->z:I

    return-void
.end method

.method private g(J)V
    .locals 0

    .line 1131
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->w:J

    return-void
.end method

.method private h()I
    .locals 1

    .line 1003
    iget v0, p0, Lcom/anythink/core/c/d$b;->J:I

    return v0
.end method

.method private h(I)V
    .locals 0

    .line 1159
    iput p1, p0, Lcom/anythink/core/c/d$b;->A:I

    return-void
.end method

.method private h(J)V
    .locals 0

    .line 1179
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->D:J

    return-void
.end method

.method private i()J
    .locals 2

    .line 1011
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->x:J

    return-wide v0
.end method

.method private i(I)V
    .locals 0

    .line 1167
    iput p1, p0, Lcom/anythink/core/c/d$b;->B:I

    return-void
.end method

.method private i(J)V
    .locals 0

    .line 1191
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->E:J

    return-void
.end method

.method private j()I
    .locals 1

    .line 1019
    iget v0, p0, Lcom/anythink/core/c/d$b;->q:I

    return v0
.end method

.method private j(I)V
    .locals 0

    .line 1207
    iput p1, p0, Lcom/anythink/core/c/d$b;->G:I

    return-void
.end method

.method private k()J
    .locals 2

    .line 1051
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->i:J

    return-wide v0
.end method

.method private k(I)V
    .locals 0

    .line 1215
    iput p1, p0, Lcom/anythink/core/c/d$b;->H:I

    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 1

    .line 1059
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    return-object v0
.end method

.method private l(I)V
    .locals 0

    .line 1223
    iput p1, p0, Lcom/anythink/core/c/d$b;->I:I

    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 1

    .line 1067
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->j:Ljava/lang/String;

    return-object v0
.end method

.method private m(I)V
    .locals 0

    .line 1243
    iput p1, p0, Lcom/anythink/core/c/d$b;->V:I

    return-void
.end method

.method private n()I
    .locals 1

    .line 1075
    iget v0, p0, Lcom/anythink/core/c/d$b;->k:I

    return v0
.end method

.method private n(I)V
    .locals 0

    .line 1251
    iput p1, p0, Lcom/anythink/core/c/d$b;->W:I

    return-void
.end method

.method private o()I
    .locals 1

    .line 1083
    iget v0, p0, Lcom/anythink/core/c/d$b;->l:I

    return v0
.end method

.method private p()D
    .locals 2

    .line 1091
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->m:D

    return-wide v0
.end method

.method private q()J
    .locals 2

    .line 1099
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->u:J

    return-wide v0
.end method

.method private r()Ljava/lang/String;
    .locals 1

    .line 1107
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 1

    .line 1115
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    return-object v0
.end method

.method private t()J
    .locals 2

    .line 1127
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->v:J

    return-wide v0
.end method

.method private u()J
    .locals 2

    .line 1135
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->w:J

    return-wide v0
.end method

.method private v()I
    .locals 1

    .line 1139
    iget v0, p0, Lcom/anythink/core/c/d$b;->y:I

    return v0
.end method

.method private w()I
    .locals 1

    .line 1147
    iget v0, p0, Lcom/anythink/core/c/d$b;->z:I

    return v0
.end method

.method private x()I
    .locals 1

    .line 1155
    iget v0, p0, Lcom/anythink/core/c/d$b;->A:I

    return v0
.end method

.method private y()I
    .locals 1

    .line 1163
    iget v0, p0, Lcom/anythink/core/c/d$b;->B:I

    return v0
.end method

.method private z()Ljava/lang/String;
    .locals 1

    .line 1171
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->C:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .line 1027
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->R:J

    return-wide v0
.end method

.method public final a(I)V
    .locals 0

    .line 1047
    iput p1, p0, Lcom/anythink/core/c/d$b;->T:I

    return-void
.end method

.method public final a(J)V
    .locals 0

    .line 1031
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->R:J

    return-void
.end method

.method public final declared-synchronized a(Lcom/anythink/core/c/d$b;III)V
    .locals 2

    monitor-enter p0

    .line 1271
    :try_start_0
    iget-object v0, p1, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iget-object v1, p0, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1272
    iget-wide v0, p1, Lcom/anythink/core/c/d$b;->m:D

    .line 2095
    iput-wide v0, p0, Lcom/anythink/core/c/d$b;->m:D

    .line 3023
    iput p3, p0, Lcom/anythink/core/c/d$b;->q:I

    .line 1274
    iget-object p3, p1, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    .line 3111
    iput-object p3, p0, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    const/4 p3, 0x0

    .line 1275
    iput p3, p0, Lcom/anythink/core/c/d$b;->a:I

    if-nez p2, :cond_0

    .line 3239
    iget p2, p1, Lcom/anythink/core/c/d$b;->V:I

    .line 3243
    iput p2, p0, Lcom/anythink/core/c/d$b;->V:I

    goto :goto_0

    .line 4243
    :cond_0
    iput p2, p0, Lcom/anythink/core/c/d$b;->V:I

    .line 5115
    :goto_0
    iget-object p2, p1, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    .line 5119
    iput-object p2, p0, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    .line 5227
    iget-object p1, p1, Lcom/anythink/core/c/d$b;->U:Lcom/anythink/core/common/d/l;

    .line 5231
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->U:Lcom/anythink/core/common/d/l;

    .line 5251
    iput p4, p0, Lcom/anythink/core/c/d$b;->W:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1286
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Lcom/anythink/core/common/d/l;III)V
    .locals 2

    .line 6023
    iput p3, p0, Lcom/anythink/core/c/d$b;->q:I

    .line 1294
    iget-wide v0, p1, Lcom/anythink/core/common/d/l;->price:D

    iput-wide v0, p0, Lcom/anythink/core/c/d$b;->m:D

    .line 1295
    iget-object p3, p1, Lcom/anythink/core/common/d/l;->token:Ljava/lang/String;

    iput-object p3, p0, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    const/4 p3, 0x0

    .line 1296
    iput p3, p0, Lcom/anythink/core/c/d$b;->a:I

    .line 6243
    iput p2, p0, Lcom/anythink/core/c/d$b;->V:I

    .line 7231
    iput-object p1, p0, Lcom/anythink/core/c/d$b;->U:Lcom/anythink/core/common/d/l;

    .line 7251
    iput p4, p0, Lcom/anythink/core/c/d$b;->W:I

    return-void
.end method

.method public final b()J
    .locals 2

    .line 1035
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->S:J

    return-wide v0
.end method

.method public final b(J)V
    .locals 0

    .line 1039
    iput-wide p1, p0, Lcom/anythink/core/c/d$b;->S:J

    return-void
.end method

.method public final c()I
    .locals 1

    .line 1043
    iget v0, p0, Lcom/anythink/core/c/d$b;->T:I

    return v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .line 800
    check-cast p1, Lcom/anythink/core/c/d$b;

    .line 7256
    iget-wide v0, p0, Lcom/anythink/core/c/d$b;->m:D

    iget-wide v2, p1, Lcom/anythink/core/c/d$b;->m:D

    cmpl-double p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public final d()Lcom/anythink/core/common/d/l;
    .locals 1

    .line 1227
    iget-object v0, p0, Lcom/anythink/core/c/d$b;->U:Lcom/anythink/core/common/d/l;

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .line 1235
    iget v0, p0, Lcom/anythink/core/c/d$b;->I:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final f()I
    .locals 1

    .line 1239
    iget v0, p0, Lcom/anythink/core/c/d$b;->V:I

    return v0
.end method

.method public final g()I
    .locals 1

    .line 1247
    iget v0, p0, Lcom/anythink/core/c/d$b;->W:I

    return v0
.end method
