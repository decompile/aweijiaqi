.class public final Lcom/anythink/core/c/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/anythink/core/c/d$b;,
        Lcom/anythink/core/c/d$a;
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/anythink/core/common/d/z;

.field private C:I

.field private D:Ljava/lang/String;

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcom/anythink/core/api/ATRewardInfo;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:J

.field private N:J

.field private O:Ljava/lang/String;

.field private P:J

.field private Q:I

.field private R:J

.field private S:I

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/p;",
            ">;"
        }
    .end annotation
.end field

.field private U:Lcom/anythink/core/common/d/r;

.field private V:J

.field private W:D

.field private X:Ljava/lang/String;

.field private Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Ljava/lang/String;

.field private final a:Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:J

.field private b:J

.field private c:J

.field private d:I

.field private e:I

.field private f:I

.field private g:J

.field private h:J

.field private i:J

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:I

.field private w:J

.field private x:J

.field private y:J

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Placement"

    .line 45
    iput-object v0, p0, Lcom/anythink/core/c/d;->a:Ljava/lang/String;

    return-void
.end method

.method private V()Ljava/lang/String;
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/anythink/core/c/d;->ab:Ljava/lang/String;

    return-object v0
.end method

.method private W()Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/anythink/core/c/d;->O:Ljava/lang/String;

    return-object v0
.end method

.method private X()I
    .locals 1

    .line 508
    iget v0, p0, Lcom/anythink/core/c/d;->e:I

    return v0
.end method

.method private Y()I
    .locals 1

    .line 548
    iget v0, p0, Lcom/anythink/core/c/d;->j:I

    return v0
.end method

.method private Z()I
    .locals 1

    .line 557
    iget v0, p0, Lcom/anythink/core/c/d;->k:I

    return v0
.end method

.method private static a(Ljava/lang/String;I)Ljava/util/List;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    move/from16 v0, p1

    const-string v1, "t_c_u"

    const-string v2, "hb_timeout"

    const-string v3, "ecpm"

    const-string v4, "unit_id"

    const-string v5, "pacing"

    const-string v6, "nw_req_num"

    const-string v7, "nw_timeout"

    const-string v8, "nw_cache_time"

    const-string v9, "ug_id"

    const-string v10, "nw_firm_name"

    const-string v11, "nw_firm_id"

    const-string v12, "content"

    const-string v13, "caps_h"

    const-string v14, "caps_d"

    const-string v15, "adapter_class"

    move-object/from16 v16, v1

    .line 1808
    sget v1, Lcom/anythink/core/c/d$b;->L:I

    move-object/from16 v17, v2

    if-eq v0, v1, :cond_0

    sget v1, Lcom/anythink/core/c/d$b;->P:I

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1809
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v18, v2

    .line 1811
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    move-object/from16 v19, v3

    move-object/from16 v3, p0

    invoke-direct {v2, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move-object/from16 v20, v4

    const/4 v3, 0x0

    .line 1815
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_27

    .line 1816
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    if-eqz v4, :cond_26

    move-object/from16 v21, v2

    .line 1820
    new-instance v2, Lcom/anythink/core/c/d$b;

    invoke-direct {v2}, Lcom/anythink/core/c/d$b;-><init>()V

    .line 1822
    iput v0, v2, Lcom/anythink/core/c/d$b;->K:I

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    .line 1823
    :goto_2
    iput v0, v2, Lcom/anythink/core/c/d$b;->n:I

    .line 1824
    invoke-virtual {v4, v15}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 p0, v3

    const-string v3, ""

    if-eqz v0, :cond_2

    .line 1825
    :try_start_1
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->g:Ljava/lang/String;

    goto :goto_3

    .line 1827
    :cond_2
    invoke-virtual {v4, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/anythink/core/c/d$b;->g:Ljava/lang/String;

    .line 1831
    :goto_3
    invoke-virtual {v4, v14}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    move-object/from16 v22, v15

    const/4 v15, -0x1

    if-eqz v0, :cond_3

    .line 1832
    iput v15, v2, Lcom/anythink/core/c/d$b;->d:I

    goto :goto_4

    .line 1834
    :cond_3
    invoke-virtual {v4, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/anythink/core/c/d$b;->d:I

    .line 1838
    :goto_4
    invoke-virtual {v4, v13}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1839
    iput v15, v2, Lcom/anythink/core/c/d$b;->e:I

    goto :goto_5

    .line 1841
    :cond_4
    invoke-virtual {v4, v13}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/anythink/core/c/d$b;->e:I

    .line 1844
    :goto_5
    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1845
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    goto :goto_6

    .line 1847
    :cond_5
    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    .line 1850
    :goto_6
    invoke-virtual {v4, v11}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1851
    iput v15, v2, Lcom/anythink/core/c/d$b;->b:I

    goto :goto_7

    .line 1853
    :cond_6
    invoke-virtual {v4, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/anythink/core/c/d$b;->b:I

    .line 1856
    :goto_7
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1857
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->c:Ljava/lang/String;

    goto :goto_8

    .line 1859
    :cond_7
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/anythink/core/c/d$b;->c:Ljava/lang/String;

    .line 1862
    :goto_8
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "unknown"

    .line 1863
    iput-object v0, v2, Lcom/anythink/core/c/d$b;->h:Ljava/lang/String;

    goto :goto_9

    .line 1865
    :cond_8
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/anythink/core/c/d$b;->h:Ljava/lang/String;

    .line 1868
    :goto_9
    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    move-object/from16 v23, v9

    move-object/from16 v24, v10

    const-wide/16 v9, 0x0

    if-eqz v0, :cond_9

    .line 1869
    invoke-virtual {v2, v9, v10}, Lcom/anythink/core/c/d$b;->a(J)V

    goto :goto_a

    .line 1871
    :cond_9
    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v9, v0

    invoke-virtual {v2, v9, v10}, Lcom/anythink/core/c/d$b;->a(J)V

    .line 1875
    :goto_a
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-wide/16 v9, 0x0

    .line 1876
    invoke-virtual {v2, v9, v10}, Lcom/anythink/core/c/d$b;->b(J)V

    goto :goto_b

    .line 1878
    :cond_a
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v9, v0

    invoke-virtual {v2, v9, v10}, Lcom/anythink/core/c/d$b;->b(J)V

    .line 1881
    :goto_b
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    .line 1882
    invoke-virtual {v2, v0}, Lcom/anythink/core/c/d$b;->a(I)V

    goto :goto_c

    .line 1884
    :cond_b
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/anythink/core/c/d$b;->a(I)V

    .line 1887
    :goto_c
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    const-wide/16 v9, -0x1

    if-eqz v0, :cond_c

    .line 50418
    iput-wide v9, v2, Lcom/anythink/core/c/d$b;->i:J

    goto :goto_d

    .line 1890
    :cond_c
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 50420
    iput-wide v9, v2, Lcom/anythink/core/c/d$b;->i:J

    :goto_d
    move-object/from16 v0, v20

    .line 1893
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 50422
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    goto :goto_e

    .line 1896
    :cond_d
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 50424
    iput-object v9, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    :goto_e
    move-object/from16 v9, v19

    .line 1899
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v10

    move-object/from16 v19, v5

    move-object/from16 v20, v6

    const-wide/16 v5, 0x0

    if-eqz v10, :cond_e

    .line 50426
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->m:D

    goto :goto_f

    :cond_e
    if-nez v1, :cond_f

    .line 1903
    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v5

    .line 50428
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->m:D

    :cond_f
    :goto_f
    move-object/from16 v5, v17

    .line 1907
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    move-object v10, v7

    const-wide/16 v6, 0x7d0

    .line 50430
    iput-wide v6, v2, Lcom/anythink/core/c/d$b;->u:J

    goto :goto_10

    :cond_10
    move-object v10, v7

    .line 1910
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    int-to-long v6, v6

    .line 50432
    iput-wide v6, v2, Lcom/anythink/core/c/d$b;->u:J

    :goto_10
    move-object/from16 v6, v16

    .line 1914
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 50434
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->j:Ljava/lang/String;

    goto :goto_11

    .line 1917
    :cond_11
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 50436
    iput-object v7, v2, Lcom/anythink/core/c/d$b;->j:Ljava/lang/String;

    :goto_11
    const-string v7, "t_c_u_min_t"

    .line 1920
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_12

    const/4 v7, 0x0

    .line 50438
    iput v7, v2, Lcom/anythink/core/c/d$b;->k:I

    goto :goto_12

    :cond_12
    const-string v7, "t_c_u_min_t"

    .line 1923
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    .line 50440
    iput v7, v2, Lcom/anythink/core/c/d$b;->k:I

    :goto_12
    const-string v7, "t_c_u_max_t"

    .line 1926
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    const/16 v7, 0xbb8

    .line 50442
    iput v7, v2, Lcom/anythink/core/c/d$b;->l:I

    goto :goto_13

    :cond_13
    const-string v7, "t_c_u_max_t"

    .line 1929
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    .line 50444
    iput v7, v2, Lcom/anythink/core/c/d$b;->l:I

    :goto_13
    const-string v7, "payload"

    .line 1932
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 50446
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    goto :goto_14

    :cond_14
    const-string v7, "payload"

    .line 1935
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 50448
    iput-object v7, v2, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    :goto_14
    const-string v7, "error"

    .line 1938
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 50450
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    goto :goto_15

    :cond_15
    const-string v3, "error"

    .line 1941
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 50452
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->p:Ljava/lang/String;

    :goto_15
    const-string v3, "l_s_t"

    .line 1947
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    move-object/from16 v17, v5

    move-object/from16 v16, v6

    const-wide/32 v5, 0x1b7740

    .line 50454
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->v:J

    goto :goto_16

    :cond_16
    move-object/from16 v17, v5

    move-object/from16 v16, v6

    const-string v3, "l_s_t"

    .line 1950
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50456
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->v:J

    :goto_16
    const-string v3, "n_d_t"

    .line 1953
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    const-wide/16 v5, -0x1

    .line 50458
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->w:J

    goto :goto_17

    :cond_17
    const-string v3, "n_d_t"

    .line 1956
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50460
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->w:J

    :goto_17
    const-string v3, "hb_t_c_t"

    .line 1959
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    const-wide/32 v5, 0x1b7740

    .line 50462
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->x:J

    goto :goto_18

    :cond_18
    const-string v3, "hb_t_c_t"

    .line 1962
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50464
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->x:J

    :goto_18
    const-string v3, "sort_type"

    .line 1966
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    if-eqz v1, :cond_19

    const/4 v3, 0x0

    goto :goto_19

    :cond_19
    const/4 v3, 0x1

    .line 50466
    :goto_19
    iput v3, v2, Lcom/anythink/core/c/d$b;->q:I

    goto :goto_1a

    :cond_1a
    const-string v3, "sort_type"

    .line 1969
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50468
    iput v3, v2, Lcom/anythink/core/c/d$b;->q:I

    :goto_1a
    const-string v3, "s_sw"

    .line 1973
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/4 v3, 0x1

    .line 50470
    iput v3, v2, Lcom/anythink/core/c/d$b;->y:I

    goto :goto_1b

    :cond_1b
    const-string v3, "s_sw"

    .line 1976
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50472
    iput v3, v2, Lcom/anythink/core/c/d$b;->y:I

    :goto_1b
    const-string v3, "c_sw"

    .line 1978
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    const/4 v3, 0x1

    .line 50474
    iput v3, v2, Lcom/anythink/core/c/d$b;->z:I

    goto :goto_1c

    :cond_1c
    const-string v3, "c_sw"

    .line 1981
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50476
    iput v3, v2, Lcom/anythink/core/c/d$b;->z:I

    :goto_1c
    const-string v3, "ecpm_level"

    .line 1985
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 50478
    iput v15, v2, Lcom/anythink/core/c/d$b;->B:I

    goto :goto_1d

    :cond_1d
    const-string v3, "ecpm_level"

    .line 1988
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50480
    iput v3, v2, Lcom/anythink/core/c/d$b;->B:I

    :goto_1d
    const-string v3, "precision"

    .line 1991
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1e

    const-string v3, "publisher_defined"

    .line 50482
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->C:Ljava/lang/String;

    goto :goto_1e

    :cond_1e
    const-string v3, "precision"

    .line 1994
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 50484
    iput-object v3, v2, Lcom/anythink/core/c/d$b;->C:Ljava/lang/String;

    :goto_1e
    const-string v3, "nx_req_time"

    .line 1997
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    const-wide/16 v5, 0x0

    .line 50486
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->D:J

    goto :goto_1f

    :cond_1f
    const-string v3, "nx_req_time"

    .line 2000
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50488
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->D:J

    :goto_1f
    const-string v3, "bid_fail_interval"

    .line 2003
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    const-wide/16 v5, 0x0

    .line 50490
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->E:J

    goto :goto_20

    :cond_20
    const-string v3, "bid_fail_interval"

    .line 2006
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50492
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->E:J

    :goto_20
    const-string v3, "cy_ecpm"

    .line 2012
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    const-wide/16 v5, 0x0

    .line 50494
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->F:D

    goto :goto_21

    :cond_21
    const-string v3, "cy_ecpm"

    .line 2015
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v5

    .line 50496
    iput-wide v5, v2, Lcom/anythink/core/c/d$b;->F:D

    :goto_21
    const-string v3, "irrf_sw"

    .line 2021
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    const/4 v3, 0x1

    .line 50498
    iput v3, v2, Lcom/anythink/core/c/d$b;->G:I

    goto :goto_22

    :cond_22
    const-string v3, "irrf_sw"

    .line 2024
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50500
    iput v3, v2, Lcom/anythink/core/c/d$b;->G:I

    :goto_22
    const-string v3, "wfe_t_sw"

    .line 2030
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_23

    const/4 v3, 0x1

    .line 50502
    iput v3, v2, Lcom/anythink/core/c/d$b;->H:I

    goto :goto_23

    :cond_23
    const-string v3, "wfe_t_sw"

    .line 2033
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50504
    iput v3, v2, Lcom/anythink/core/c/d$b;->H:I

    :goto_23
    const-string v3, "ubp_sw"

    .line 2039
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_24

    const/4 v3, 0x2

    .line 50506
    iput v3, v2, Lcom/anythink/core/c/d$b;->I:I

    goto :goto_24

    :cond_24
    const-string v3, "ubp_sw"

    .line 2042
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 50508
    iput v3, v2, Lcom/anythink/core/c/d$b;->I:I

    :goto_24
    const-string v3, "bid_pl_sw"

    .line 2049
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_25

    const/4 v3, 0x1

    .line 50510
    iput v3, v2, Lcom/anythink/core/c/d$b;->J:I

    :goto_25
    move-object/from16 v4, v18

    goto :goto_26

    :cond_25
    const/4 v3, 0x1

    const-string v5, "bid_pl_sw"

    .line 2052
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 50512
    iput v4, v2, Lcom/anythink/core/c/d$b;->J:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_25

    .line 2056
    :goto_26
    :try_start_2
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_27

    :cond_26
    move-object/from16 v21, v2

    move/from16 p0, v3

    move-object/from16 v23, v9

    move-object/from16 v24, v10

    move-object/from16 v22, v15

    move-object/from16 v4, v18

    move-object/from16 v9, v19

    move-object/from16 v0, v20

    const/4 v3, 0x1

    move-object/from16 v19, v5

    move-object/from16 v20, v6

    move-object v10, v7

    :goto_27
    add-int/lit8 v2, p0, 0x1

    move v3, v2

    move-object/from16 v18, v4

    move-object v7, v10

    move-object/from16 v5, v19

    move-object/from16 v6, v20

    move-object/from16 v2, v21

    move-object/from16 v15, v22

    move-object/from16 v10, v24

    move-object/from16 v20, v0

    move-object/from16 v19, v9

    move-object/from16 v9, v23

    move/from16 v0, p1

    goto/16 :goto_1

    :catch_0
    :cond_27
    move-object/from16 v4, v18

    :catch_1
    return-object v4
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 1791
    sget v0, Lcom/anythink/core/c/d$b;->L:I

    invoke-static {p0, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p0

    .line 1792
    sget v0, Lcom/anythink/core/c/d$b;->P:I

    invoke-static {p1, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p1

    .line 1794
    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1796
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;J)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/p;",
            ">;"
        }
    .end annotation

    .line 2073
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2076
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 2078
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge p0, v2, :cond_0

    .line 2079
    new-instance v2, Lcom/anythink/core/common/d/p;

    invoke-direct {v2}, Lcom/anythink/core/common/d/p;-><init>()V

    .line 2080
    invoke-virtual {v1, p0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "o_id"

    .line 2081
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->c(Ljava/lang/String;)V

    const-string v4, "c_id"

    .line 2082
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->d(Ljava/lang/String;)V

    const-string v4, "t"

    .line 2083
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->e(Ljava/lang/String;)V

    const-string v4, "p_g"

    .line 2084
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->p(Ljava/lang/String;)V

    const-string v4, "d"

    .line 2085
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->f(Ljava/lang/String;)V

    const-string v4, "ic_u"

    .line 2086
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->g(Ljava/lang/String;)V

    const-string v4, "im_u"

    .line 2087
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->h(Ljava/lang/String;)V

    const-string v4, "f_i_u"

    .line 2088
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->i(Ljava/lang/String;)V

    const-string v4, "a_c_u"

    .line 2089
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->j(Ljava/lang/String;)V

    const-string v4, "c_t"

    .line 2090
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->k(Ljava/lang/String;)V

    const-string v4, "v_u"

    .line 2091
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->l(Ljava/lang/String;)V

    const-string v4, "l_t"

    .line 2092
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->c(I)V

    const-string v4, "p_u"

    .line 2093
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->m(Ljava/lang/String;)V

    const-string v4, "dl"

    .line 2094
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->n(Ljava/lang/String;)V

    const-string v4, "c_u"

    .line 2095
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->o(Ljava/lang/String;)V

    const-string v4, "ip_u"

    .line 2096
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->v(Ljava/lang/String;)V

    const-string v4, "t_u"

    .line 2099
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->w(Ljava/lang/String;)V

    const-string v4, "t_u_25"

    .line 2100
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->x(Ljava/lang/String;)V

    const-string v4, "t_u_50"

    .line 2101
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->y(Ljava/lang/String;)V

    const-string v4, "t_u_75"

    .line 2102
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->z(Ljava/lang/String;)V

    const-string v4, "t_u_100"

    .line 2103
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->A(Ljava/lang/String;)V

    const-string v4, "s_e_c_t_u"

    .line 2104
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->B(Ljava/lang/String;)V

    const-string v4, "c_t_u"

    .line 2105
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->C(Ljava/lang/String;)V

    const-string v4, "ip_n_u"

    .line 2106
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->D(Ljava/lang/String;)V

    const-string v4, "c_n_u"

    .line 2107
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->E(Ljava/lang/String;)V

    const-string v4, "o_a_d_c"

    .line 2110
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 50514
    iput v4, v2, Lcom/anythink/core/common/d/p;->z:I

    const-string v4, "o_a_p"

    .line 2111
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 50516
    iput-wide v4, v2, Lcom/anythink/core/common/d/p;->A:J

    .line 2112
    invoke-virtual {v2, p2, p3}, Lcom/anythink/core/common/d/p;->a(J)V

    const-string v4, "unit_type"

    .line 2114
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->b(I)V

    const-string v4, "c_m"

    .line 2115
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->e(I)V

    const-string v4, "ext_h_pic"

    .line 2118
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->r(Ljava/lang/String;)V

    const-string v4, "ext_big_h_pic"

    .line 2119
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->s(Ljava/lang/String;)V

    const-string v4, "ext_rect_h_pic"

    .line 2120
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/anythink/core/common/d/p;->t(Ljava/lang/String;)V

    const-string v4, "ext_home_h_pic"

    .line 2121
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/anythink/core/common/d/p;->u(Ljava/lang/String;)V

    .line 2123
    invoke-virtual {v2, p1}, Lcom/anythink/core/common/d/p;->a(Ljava/lang/String;)V

    .line 2124
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 p0, p0, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception p0

    .line 2127
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 1778
    sget v0, Lcom/anythink/core/c/d$b;->M:I

    invoke-static {p0, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p0

    .line 1779
    sget v0, Lcom/anythink/core/c/d$b;->O:I

    invoke-static {p1, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p1

    .line 1780
    sget v0, Lcom/anythink/core/c/d$b;->N:I

    invoke-static {p2, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p2

    .line 1781
    sget v0, Lcom/anythink/core/c/d$b;->Q:I

    invoke-static {p3, v0}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p3

    .line 1783
    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1784
    invoke-interface {p0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1785
    invoke-interface {p0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method private a(D)V
    .locals 0

    .line 225
    iput-wide p1, p0, Lcom/anythink/core/c/d;->W:D

    return-void
.end method

.method private a(I)V
    .locals 0

    .line 402
    iput p1, p0, Lcom/anythink/core/c/d;->F:I

    return-void
.end method

.method private a(J)V
    .locals 0

    .line 241
    iput-wide p1, p0, Lcom/anythink/core/c/d;->V:J

    return-void
.end method

.method private a(Lcom/anythink/core/api/ATRewardInfo;)V
    .locals 0

    .line 386
    iput-object p1, p0, Lcom/anythink/core/c/d;->J:Lcom/anythink/core/api/ATRewardInfo;

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/r;)V
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/anythink/core/c/d;->U:Lcom/anythink/core/common/d/r;

    return-void
.end method

.method private a(Lcom/anythink/core/common/d/z;)V
    .locals 0

    .line 602
    iput-object p1, p0, Lcom/anythink/core/c/d;->B:Lcom/anythink/core/common/d/z;

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 217
    iput-object p1, p0, Lcom/anythink/core/c/d;->Y:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;)V"
        }
    .end annotation

    .line 378
    iput-object p1, p0, Lcom/anythink/core/c/d;->I:Ljava/util/Map;

    return-void
.end method

.method private aa()I
    .locals 1

    .line 566
    iget v0, p0, Lcom/anythink/core/c/d;->l:I

    return v0
.end method

.method private ab()J
    .locals 2

    .line 651
    iget-wide v0, p0, Lcom/anythink/core/c/d;->ac:J

    return-wide v0
.end method

.method public static b(Ljava/lang/String;)Lcom/anythink/core/c/d;
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "gro_id"

    const-string v2, "refresh"

    const-string v3, "show_type"

    const-string v4, "wifi_auto_sw"

    const-string v5, "unit_pacing"

    const-string v6, "unit_caps_h"

    const-string v7, "unit_caps_d"

    const-string v8, "req_ug_num"

    const-string v9, "ad_delivery_sw"

    const-string v10, "ps_ct_out"

    const-string v11, "ps_ct"

    const-string v12, "pucs"

    const/4 v13, 0x0

    if-nez v0, :cond_0

    return-object v13

    .line 1314
    :cond_0
    :try_start_0
    new-instance v14, Lcom/anythink/core/c/d;

    invoke-direct {v14}, Lcom/anythink/core/c/d;-><init>()V

    .line 1315
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1317
    invoke-virtual {v15, v11}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    .line 2488
    iput-wide v1, v14, Lcom/anythink/core/c/d;->b:J

    goto :goto_0

    .line 1320
    :cond_1
    invoke-virtual {v15, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 3488
    iput-wide v1, v14, Lcom/anythink/core/c/d;->b:J

    .line 1323
    :goto_0
    invoke-virtual {v15, v10}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    .line 3496
    iput-wide v0, v14, Lcom/anythink/core/c/d;->c:J

    goto :goto_1

    .line 1326
    :cond_2
    invoke-virtual {v15, v10}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 4496
    iput-wide v0, v14, Lcom/anythink/core/c/d;->c:J

    .line 1329
    :goto_1
    invoke-virtual {v15, v12}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    .line 4504
    iput v1, v14, Lcom/anythink/core/c/d;->d:I

    goto :goto_2

    .line 1332
    :cond_3
    invoke-virtual {v15, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 5504
    iput v0, v14, Lcom/anythink/core/c/d;->d:I

    .line 1335
    :goto_2
    invoke-virtual {v15, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5512
    iput v1, v14, Lcom/anythink/core/c/d;->e:I

    goto :goto_3

    .line 1338
    :cond_4
    invoke-virtual {v15, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 6512
    iput v0, v14, Lcom/anythink/core/c/d;->e:I

    .line 1341
    :goto_3
    invoke-virtual {v15, v8}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, -0x1

    if-eqz v0, :cond_5

    .line 6520
    iput v2, v14, Lcom/anythink/core/c/d;->f:I

    goto :goto_4

    .line 1344
    :cond_5
    invoke-virtual {v15, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 7520
    iput v0, v14, Lcom/anythink/core/c/d;->f:I

    .line 1347
    :goto_4
    invoke-virtual {v15, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    const-wide/16 v8, -0x1

    if-eqz v0, :cond_6

    .line 7528
    iput-wide v8, v14, Lcom/anythink/core/c/d;->g:J

    goto :goto_5

    .line 1350
    :cond_6
    invoke-virtual {v15, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 8528
    iput-wide v10, v14, Lcom/anythink/core/c/d;->g:J

    .line 1353
    :goto_5
    invoke-virtual {v15, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8536
    iput-wide v8, v14, Lcom/anythink/core/c/d;->h:J

    goto :goto_6

    .line 1356
    :cond_7
    invoke-virtual {v15, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 9536
    iput-wide v6, v14, Lcom/anythink/core/c/d;->h:J

    .line 1359
    :goto_6
    invoke-virtual {v15, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 9544
    iput-wide v8, v14, Lcom/anythink/core/c/d;->i:J

    goto :goto_7

    .line 1362
    :cond_8
    invoke-virtual {v15, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 10544
    iput-wide v5, v14, Lcom/anythink/core/c/d;->i:J

    .line 1365
    :goto_7
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    const/4 v5, 0x0

    if-eqz v0, :cond_9

    .line 10552
    iput v5, v14, Lcom/anythink/core/c/d;->j:I

    goto :goto_8

    .line 1368
    :cond_9
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 11552
    iput v0, v14, Lcom/anythink/core/c/d;->j:I

    .line 1371
    :goto_8
    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 11561
    iput v5, v14, Lcom/anythink/core/c/d;->k:I

    goto :goto_9

    .line 1374
    :cond_a
    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 12561
    iput v0, v14, Lcom/anythink/core/c/d;->k:I

    :goto_9
    move-object/from16 v0, v17

    .line 1378
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 12570
    iput v5, v14, Lcom/anythink/core/c/d;->l:I

    goto :goto_a

    .line 1381
    :cond_b
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 13570
    iput v0, v14, Lcom/anythink/core/c/d;->l:I

    :goto_a
    move-object/from16 v0, v16

    .line 1384
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 13578
    iput v5, v14, Lcom/anythink/core/c/d;->m:I

    goto :goto_b

    .line 1387
    :cond_c
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 14578
    iput v0, v14, Lcom/anythink/core/c/d;->m:I

    :goto_b
    const-string v0, "format"

    .line 1391
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 15483
    iput v5, v14, Lcom/anythink/core/c/d;->u:I

    goto :goto_c

    :cond_d
    const-string v0, "format"

    .line 1394
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 16483
    iput v0, v14, Lcom/anythink/core/c/d;->u:I

    :goto_c
    const-string v0, "auto_refresh"

    .line 1397
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 17467
    iput v5, v14, Lcom/anythink/core/c/d;->v:I

    goto :goto_d

    :cond_e
    const-string v0, "auto_refresh"

    .line 1400
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 18467
    iput v0, v14, Lcom/anythink/core/c/d;->v:I

    :goto_d
    const-string v0, "auto_refresh_time"

    .line 1404
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 19467
    iput v5, v14, Lcom/anythink/core/c/d;->v:I

    goto :goto_e

    :cond_f
    const-string v0, "auto_refresh_time"

    .line 1407
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 19475
    iput-wide v3, v14, Lcom/anythink/core/c/d;->w:J

    :goto_e
    const-string v0, "s_t"

    .line 1412
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-wide/32 v3, 0xdbba0

    .line 20435
    iput-wide v3, v14, Lcom/anythink/core/c/d;->x:J

    goto :goto_f

    :cond_10
    const-string v0, "s_t"

    .line 1415
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 21435
    iput-wide v3, v14, Lcom/anythink/core/c/d;->x:J

    :goto_f
    const-string v0, "l_s_t"

    .line 1418
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-wide/32 v3, 0x1b7740

    .line 21443
    iput-wide v3, v14, Lcom/anythink/core/c/d;->y:J

    goto :goto_10

    :cond_11
    const-string v0, "l_s_t"

    .line 1421
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 22443
    iput-wide v3, v14, Lcom/anythink/core/c/d;->y:J

    :goto_10
    const-string v0, "ra"

    .line 1424
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 22451
    iput v2, v14, Lcom/anythink/core/c/d;->z:I

    goto :goto_11

    :cond_12
    const-string v0, "ra"

    .line 1427
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 23451
    iput v0, v14, Lcom/anythink/core/c/d;->z:I

    :goto_11
    const-string v0, "asid"

    .line 1430
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const-string v3, ""

    if-eqz v0, :cond_13

    .line 23459
    :try_start_1
    iput-object v3, v14, Lcom/anythink/core/c/d;->A:Ljava/lang/String;

    goto :goto_12

    :cond_13
    const-string v0, "asid"

    .line 1433
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24459
    iput-object v0, v14, Lcom/anythink/core/c/d;->A:Ljava/lang/String;

    :goto_12
    const-string v0, "tp_ps"

    .line 1436
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 24602
    iput-object v13, v14, Lcom/anythink/core/c/d;->B:Lcom/anythink/core/common/d/z;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_17

    .line 1440
    :cond_14
    :try_start_2
    new-instance v0, Lcom/anythink/core/common/d/z;

    invoke-direct {v0}, Lcom/anythink/core/common/d/z;-><init>()V

    const-string v4, "tp_ps"

    .line 1441
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1442
    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v1, :cond_15

    const/4 v6, 0x1

    goto :goto_13

    :cond_15
    const/4 v6, 0x0

    :goto_13
    iput-boolean v6, v0, Lcom/anythink/core/common/d/z;->a:Z

    const-string v6, "apdt"

    .line 1443
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v0, Lcom/anythink/core/common/d/z;->b:J

    const-string v6, "aprn"

    .line 1444
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v0, Lcom/anythink/core/common/d/z;->c:I

    const-string v6, "puas"

    .line 1445
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v1, :cond_16

    const/4 v6, 0x1

    goto :goto_14

    :cond_16
    const/4 v6, 0x0

    :goto_14
    iput-boolean v6, v0, Lcom/anythink/core/common/d/z;->d:Z

    const-string v6, "cdt"

    .line 1446
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v0, Lcom/anythink/core/common/d/z;->e:J

    const-string v6, "ski_swt"

    .line 1447
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v1, :cond_17

    const/4 v6, 0x1

    goto :goto_15

    :cond_17
    const/4 v6, 0x0

    :goto_15
    iput-boolean v6, v0, Lcom/anythink/core/common/d/z;->f:Z

    const-string v6, "aut_swt"

    .line 1448
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v1, :cond_18

    goto :goto_16

    :cond_18
    const/4 v1, 0x0

    :goto_16
    iput-boolean v1, v0, Lcom/anythink/core/common/d/z;->g:Z

    .line 25602
    iput-object v0, v14, Lcom/anythink/core/c/d;->B:Lcom/anythink/core/common/d/z;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :goto_17
    :try_start_3
    const-string v0, "ug_list"

    .line 1456
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    const-string v1, "[]"

    if-eqz v0, :cond_19

    .line 25610
    :try_start_4
    iput-object v1, v14, Lcom/anythink/core/c/d;->n:Ljava/lang/String;

    goto :goto_18

    :cond_19
    const-string v0, "ug_list"

    .line 1459
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26610
    iput-object v0, v14, Lcom/anythink/core/c/d;->n:Ljava/lang/String;

    :goto_18
    const-string v0, "ol_list"

    .line 1462
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 26618
    iput-object v1, v14, Lcom/anythink/core/c/d;->o:Ljava/lang/String;

    goto :goto_19

    :cond_1a
    const-string v0, "ol_list"

    .line 1465
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27618
    iput-object v0, v14, Lcom/anythink/core/c/d;->o:Ljava/lang/String;

    .line 28606
    :goto_19
    iget-object v0, v14, Lcom/anythink/core/c/d;->n:Ljava/lang/String;

    .line 28614
    iget-object v4, v14, Lcom/anythink/core/c/d;->o:Ljava/lang/String;

    .line 1468
    invoke-static {v0, v4}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 29318
    iput-object v0, v14, Lcom/anythink/core/c/d;->s:Ljava/util/List;

    const-string v0, "s2shb_list"

    .line 1471
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 29626
    iput-object v1, v14, Lcom/anythink/core/c/d;->p:Ljava/lang/String;

    goto :goto_1a

    :cond_1b
    const-string v0, "s2shb_list"

    .line 1474
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30626
    iput-object v0, v14, Lcom/anythink/core/c/d;->p:Ljava/lang/String;

    :goto_1a
    const-string v0, "adx_list"

    .line 1477
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 31286
    iput-object v1, v14, Lcom/anythink/core/c/d;->aa:Ljava/lang/String;

    goto :goto_1b

    :cond_1c
    const-string v0, "adx_list"

    .line 1480
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32286
    iput-object v0, v14, Lcom/anythink/core/c/d;->aa:Ljava/lang/String;

    :goto_1b
    const-string v0, "hb_list"

    .line 1483
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 32634
    iput-object v1, v14, Lcom/anythink/core/c/d;->q:Ljava/lang/String;

    goto :goto_1c

    :cond_1d
    const-string v0, "hb_list"

    .line 1486
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33634
    iput-object v0, v14, Lcom/anythink/core/c/d;->q:Ljava/lang/String;

    :goto_1c
    const-string v0, "inh_list"

    .line 1489
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 34249
    iput-object v1, v14, Lcom/anythink/core/c/d;->r:Ljava/lang/String;

    goto :goto_1d

    :cond_1e
    const-string v0, "inh_list"

    .line 1492
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35249
    iput-object v0, v14, Lcom/anythink/core/c/d;->r:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 35622
    :goto_1d
    :try_start_5
    iget-object v0, v14, Lcom/anythink/core/c/d;->p:Ljava/lang/String;

    .line 36282
    iget-object v1, v14, Lcom/anythink/core/c/d;->aa:Ljava/lang/String;

    .line 36630
    iget-object v4, v14, Lcom/anythink/core/c/d;->q:Ljava/lang/String;

    .line 37245
    iget-object v6, v14, Lcom/anythink/core/c/d;->r:Ljava/lang/String;

    .line 1496
    invoke-static {v0, v1, v4, v6}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 37326
    iput-object v0, v14, Lcom/anythink/core/c/d;->t:Ljava/util/List;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    :try_start_6
    const-string v0, "updateTime"

    .line 1506
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const-wide/16 v0, 0x0

    .line 37655
    iput-wide v0, v14, Lcom/anythink/core/c/d;->ac:J

    goto :goto_1e

    :cond_1f
    const-string v0, "updateTime"

    .line 1509
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 38655
    iput-wide v0, v14, Lcom/anythink/core/c/d;->ac:J

    :goto_1e
    const-string v4, "t_g_id"

    .line 1516
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 39410
    iput v2, v14, Lcom/anythink/core/c/d;->C:I

    goto :goto_1f

    :cond_20
    const-string v4, "t_g_id"

    .line 1519
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 40410
    iput v4, v14, Lcom/anythink/core/c/d;->C:I

    :goto_1f
    const-string v4, "s_id"

    .line 1522
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 40418
    iput-object v3, v14, Lcom/anythink/core/c/d;->D:Ljava/lang/String;

    goto :goto_20

    :cond_21
    const-string v4, "s_id"

    .line 1525
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 41418
    iput-object v4, v14, Lcom/anythink/core/c/d;->D:Ljava/lang/String;

    :goto_20
    const-string v4, "u_n_f_sw"

    .line 1528
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 41426
    iput v5, v14, Lcom/anythink/core/c/d;->E:I

    goto :goto_21

    :cond_22
    const-string v4, "u_n_f_sw"

    .line 1531
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 42426
    iput v4, v14, Lcom/anythink/core/c/d;->E:I

    :goto_21
    const-string v4, "m_o"

    .line 1534
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 43302
    iput-object v13, v14, Lcom/anythink/core/c/d;->T:Ljava/util/List;

    goto :goto_22

    :cond_23
    const-string v4, "m_o"

    .line 1537
    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "m_o_ks"

    invoke-virtual {v15, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v0, v1}, Lcom/anythink/core/c/d;->a(Ljava/lang/String;Ljava/lang/String;J)Ljava/util/List;

    move-result-object v0

    .line 44302
    iput-object v0, v14, Lcom/anythink/core/c/d;->T:Ljava/util/List;

    :goto_22
    const-string v0, "m_o_s"

    .line 1540
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    const-string v0, "m_o_s"

    .line 1542
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/d/r;->b(Ljava/lang/String;)Lcom/anythink/core/common/d/r;

    move-result-object v0

    .line 45290
    iput-object v0, v14, Lcom/anythink/core/c/d;->U:Lcom/anythink/core/common/d/r;

    :cond_24
    const-string v0, "p_m_o"

    .line 1545
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 45402
    iput v5, v14, Lcom/anythink/core/c/d;->F:I

    goto :goto_23

    :cond_25
    const-string v0, "p_m_o"

    .line 1548
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 46402
    iput v0, v14, Lcom/anythink/core/c/d;->F:I

    :goto_23
    const-string v0, "sdk_custom"

    .line 1552
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 47394
    iput-object v13, v14, Lcom/anythink/core/c/d;->H:Ljava/util/Map;

    goto :goto_25

    .line 1555
    :cond_26
    new-instance v0, Lorg/json/JSONObject;

    const-string v1, "sdk_custom"

    invoke-virtual {v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1556
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1557
    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 1558
    :goto_24
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_27

    .line 1559
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1560
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_24

    .line 48394
    :cond_27
    iput-object v1, v14, Lcom/anythink/core/c/d;->H:Ljava/util/Map;

    :goto_25
    const-string v0, "callback"

    .line 1567
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 1568
    new-instance v0, Lorg/json/JSONObject;

    const-string v1, "callback"

    invoke-virtual {v15, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "sc_list"

    .line 1570
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    const-string v4, "rw_num"

    const-string v6, "rw_n"

    if-nez v1, :cond_29

    .line 1571
    :try_start_7
    new-instance v1, Lorg/json/JSONObject;

    const-string v7, "sc_list"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1572
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1575
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v8

    .line 1577
    :goto_26
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_28

    .line 1578
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1580
    new-instance v10, Lorg/json/JSONObject;

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1582
    new-instance v11, Lcom/anythink/core/api/ATRewardInfo;

    invoke-direct {v11}, Lcom/anythink/core/api/ATRewardInfo;-><init>()V

    .line 1583
    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/anythink/core/api/ATRewardInfo;->rewardName:Ljava/lang/String;

    .line 1584
    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v11, Lcom/anythink/core/api/ATRewardInfo;->rewardNumber:I

    .line 1585
    invoke-virtual {v7, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_26

    .line 49378
    :cond_28
    iput-object v7, v14, Lcom/anythink/core/c/d;->I:Ljava/util/Map;

    :cond_29
    const-string v1, "reward"

    .line 1590
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2c

    .line 1591
    new-instance v1, Lorg/json/JSONObject;

    const-string v7, "reward"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1592
    new-instance v7, Lcom/anythink/core/api/ATRewardInfo;

    invoke-direct {v7}, Lcom/anythink/core/api/ATRewardInfo;-><init>()V

    .line 1593
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2a

    .line 1594
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/anythink/core/api/ATRewardInfo;->rewardName:Ljava/lang/String;

    .line 1596
    :cond_2a
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2b

    .line 1597
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v7, Lcom/anythink/core/api/ATRewardInfo;->rewardNumber:I

    .line 49386
    :cond_2b
    iput-object v7, v14, Lcom/anythink/core/c/d;->J:Lcom/anythink/core/api/ATRewardInfo;

    :cond_2c
    const-string v1, "currency"

    .line 1602
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2d

    const-string v1, "currency"

    .line 1603
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50366
    iput-object v1, v14, Lcom/anythink/core/c/d;->K:Ljava/lang/String;

    :cond_2d
    const-string v1, "cc"

    .line 1606
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2e

    const-string v1, "cc"

    .line 1607
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50368
    iput-object v1, v14, Lcom/anythink/core/c/d;->L:Ljava/lang/String;

    :cond_2e
    const-string v1, "exch_r"

    .line 1610
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2f

    const-string v1, "exch_r"

    .line 1611
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 50370
    iput-wide v6, v14, Lcom/anythink/core/c/d;->W:D

    :cond_2f
    const-string v1, "acct_cy"

    .line 1613
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_30

    const-string v1, "acct_cy"

    .line 1614
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50372
    iput-object v0, v14, Lcom/anythink/core/c/d;->X:Ljava/lang/String;

    :cond_30
    const-string v0, "hb_start_time"

    .line 1620
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    const-wide/16 v0, 0x7d0

    .line 50374
    iput-wide v0, v14, Lcom/anythink/core/c/d;->M:J

    goto :goto_27

    :cond_31
    const-string v0, "hb_start_time"

    .line 1623
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50376
    iput-wide v0, v14, Lcom/anythink/core/c/d;->M:J

    :goto_27
    const-string v0, "hb_bid_timeout"

    .line 1625
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    const-wide/16 v0, 0x2710

    .line 50378
    iput-wide v0, v14, Lcom/anythink/core/c/d;->N:J

    goto :goto_28

    :cond_32
    const-string v0, "hb_bid_timeout"

    .line 1628
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50380
    iput-wide v0, v14, Lcom/anythink/core/c/d;->N:J

    :goto_28
    const-string v0, "addr_bid"

    .line 1632
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 50382
    iput-object v3, v14, Lcom/anythink/core/c/d;->O:Ljava/lang/String;

    goto :goto_29

    :cond_33
    const-string v0, "addr_bid"

    .line 1635
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50384
    iput-object v0, v14, Lcom/anythink/core/c/d;->O:Ljava/lang/String;

    :goto_29
    const-string v0, "load_fail_wtime"

    .line 1641
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    const-wide/16 v0, 0x2710

    .line 50386
    iput-wide v0, v14, Lcom/anythink/core/c/d;->P:J

    goto :goto_2a

    :cond_34
    const-string v0, "load_fail_wtime"

    .line 1644
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50388
    iput-wide v0, v14, Lcom/anythink/core/c/d;->P:J

    :goto_2a
    const-string v0, "load_cap"

    .line 1647
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 50390
    iput v2, v14, Lcom/anythink/core/c/d;->Q:I

    goto :goto_2b

    :cond_35
    const-string v0, "load_cap"

    .line 1650
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 50392
    iput v0, v14, Lcom/anythink/core/c/d;->Q:I

    :goto_2b
    const-string v0, "load_cap_time"

    .line 1653
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    const-wide/32 v0, 0xdbba0

    .line 50394
    iput-wide v0, v14, Lcom/anythink/core/c/d;->R:J

    goto :goto_2c

    :cond_36
    const-string v0, "load_cap_time"

    .line 1656
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50396
    iput-wide v0, v14, Lcom/anythink/core/c/d;->R:J

    :goto_2c
    const-string v0, "cached_offers_num"

    .line 1659
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    const/4 v0, 0x2

    .line 50398
    iput v0, v14, Lcom/anythink/core/c/d;->S:I

    goto :goto_2d

    :cond_37
    const-string v0, "cached_offers_num"

    .line 1662
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 50400
    iput v0, v14, Lcom/anythink/core/c/d;->S:I

    :goto_2d
    const-string v0, "ilrd"

    .line 1668
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 50402
    iput-object v13, v14, Lcom/anythink/core/c/d;->Z:Ljava/lang/String;

    goto :goto_2e

    :cond_38
    const-string v0, "ilrd"

    .line 1671
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50404
    iput-object v0, v14, Lcom/anythink/core/c/d;->Z:Ljava/lang/String;

    :goto_2e
    const-string v0, "adx_st"

    .line 1674
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 50406
    iput-object v3, v14, Lcom/anythink/core/c/d;->ab:Ljava/lang/String;

    goto :goto_2f

    :cond_39
    const-string v0, "adx_st"

    .line 1677
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50408
    iput-object v0, v14, Lcom/anythink/core/c/d;->ab:Ljava/lang/String;

    :goto_2f
    const-string v0, "fbhb_bid_wtime"

    .line 1683
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const-wide/16 v0, 0xfa0

    .line 50410
    iput-wide v0, v14, Lcom/anythink/core/c/d;->V:J

    goto :goto_30

    :cond_3a
    const-string v0, "fbhb_bid_wtime"

    .line 1686
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 50412
    iput-wide v0, v14, Lcom/anythink/core/c/d;->V:J

    :goto_30
    const-string v0, "burl_nt_firm"

    .line 1692
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 50414
    iput-object v13, v14, Lcom/anythink/core/c/d;->Y:Ljava/util/List;

    goto :goto_32

    :cond_3b
    const-string v0, "burl_nt_firm"

    .line 1695
    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 1696
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1697
    :goto_31
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v5, v2, :cond_3c

    .line 1698
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_31

    .line 50416
    :cond_3c
    iput-object v1, v14, Lcom/anythink/core/c/d;->Y:Ljava/util/List;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    :goto_32
    return-object v14

    :catch_2
    return-object v13
.end method

.method private b(I)V
    .locals 0

    .line 410
    iput p1, p0, Lcom/anythink/core/c/d;->C:I

    return-void
.end method

.method private b(J)V
    .locals 0

    .line 334
    iput-wide p1, p0, Lcom/anythink/core/c/d;->M:J

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/p;",
            ">;)V"
        }
    .end annotation

    .line 302
    iput-object p1, p0, Lcom/anythink/core/c/d;->T:Ljava/util/List;

    return-void
.end method

.method private b(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 394
    iput-object p1, p0, Lcom/anythink/core/c/d;->H:Ljava/util/Map;

    return-void
.end method

.method private c(I)V
    .locals 0

    .line 426
    iput p1, p0, Lcom/anythink/core/c/d;->E:I

    return-void
.end method

.method private c(J)V
    .locals 0

    .line 342
    iput-wide p1, p0, Lcom/anythink/core/c/d;->N:J

    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    .line 318
    iput-object p1, p0, Lcom/anythink/core/c/d;->s:Ljava/util/List;

    return-void
.end method

.method private d(I)V
    .locals 0

    .line 451
    iput p1, p0, Lcom/anythink/core/c/d;->z:I

    return-void
.end method

.method private d(J)V
    .locals 0

    .line 435
    iput-wide p1, p0, Lcom/anythink/core/c/d;->x:J

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/anythink/core/c/d;->X:Ljava/lang/String;

    return-void
.end method

.method private d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;)V"
        }
    .end annotation

    .line 326
    iput-object p1, p0, Lcom/anythink/core/c/d;->t:Ljava/util/List;

    return-void
.end method

.method private e(I)V
    .locals 0

    .line 467
    iput p1, p0, Lcom/anythink/core/c/d;->v:I

    return-void
.end method

.method private e(J)V
    .locals 0

    .line 443
    iput-wide p1, p0, Lcom/anythink/core/c/d;->y:J

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/anythink/core/c/d;->r:Ljava/lang/String;

    return-void
.end method

.method private f(I)V
    .locals 0

    .line 483
    iput p1, p0, Lcom/anythink/core/c/d;->u:I

    return-void
.end method

.method private f(J)V
    .locals 0

    .line 475
    iput-wide p1, p0, Lcom/anythink/core/c/d;->w:J

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/anythink/core/c/d;->Z:Ljava/lang/String;

    return-void
.end method

.method private g(I)V
    .locals 0

    .line 504
    iput p1, p0, Lcom/anythink/core/c/d;->d:I

    return-void
.end method

.method private g(J)V
    .locals 0

    .line 488
    iput-wide p1, p0, Lcom/anythink/core/c/d;->b:J

    return-void
.end method

.method private g(Ljava/lang/String;)V
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/anythink/core/c/d;->ab:Ljava/lang/String;

    return-void
.end method

.method private h(I)V
    .locals 0

    .line 512
    iput p1, p0, Lcom/anythink/core/c/d;->e:I

    return-void
.end method

.method private h(J)V
    .locals 0

    .line 496
    iput-wide p1, p0, Lcom/anythink/core/c/d;->c:J

    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/anythink/core/c/d;->aa:Ljava/lang/String;

    return-void
.end method

.method private i(I)V
    .locals 0

    .line 520
    iput p1, p0, Lcom/anythink/core/c/d;->f:I

    return-void
.end method

.method private i(J)V
    .locals 0

    .line 528
    iput-wide p1, p0, Lcom/anythink/core/c/d;->g:J

    return-void
.end method

.method private i(Ljava/lang/String;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/anythink/core/c/d;->O:Ljava/lang/String;

    return-void
.end method

.method private j(I)V
    .locals 0

    .line 552
    iput p1, p0, Lcom/anythink/core/c/d;->j:I

    return-void
.end method

.method private j(J)V
    .locals 0

    .line 536
    iput-wide p1, p0, Lcom/anythink/core/c/d;->h:J

    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/anythink/core/c/d;->K:Ljava/lang/String;

    return-void
.end method

.method private k(I)V
    .locals 0

    .line 561
    iput p1, p0, Lcom/anythink/core/c/d;->k:I

    return-void
.end method

.method private k(J)V
    .locals 0

    .line 544
    iput-wide p1, p0, Lcom/anythink/core/c/d;->i:J

    return-void
.end method

.method private k(Ljava/lang/String;)V
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/anythink/core/c/d;->L:Ljava/lang/String;

    return-void
.end method

.method private l(I)V
    .locals 0

    .line 570
    iput p1, p0, Lcom/anythink/core/c/d;->l:I

    return-void
.end method

.method private l(J)V
    .locals 0

    .line 655
    iput-wide p1, p0, Lcom/anythink/core/c/d;->ac:J

    return-void
.end method

.method private l(Ljava/lang/String;)V
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/anythink/core/c/d;->D:Ljava/lang/String;

    return-void
.end method

.method private m(I)V
    .locals 0

    .line 578
    iput p1, p0, Lcom/anythink/core/c/d;->m:I

    return-void
.end method

.method private m(J)V
    .locals 0

    .line 663
    iput-wide p1, p0, Lcom/anythink/core/c/d;->P:J

    return-void
.end method

.method private m(Ljava/lang/String;)V
    .locals 0

    .line 459
    iput-object p1, p0, Lcom/anythink/core/c/d;->A:Ljava/lang/String;

    return-void
.end method

.method private n(I)V
    .locals 0

    .line 671
    iput p1, p0, Lcom/anythink/core/c/d;->Q:I

    return-void
.end method

.method private n(J)V
    .locals 0

    .line 679
    iput-wide p1, p0, Lcom/anythink/core/c/d;->R:J

    return-void
.end method

.method private n(Ljava/lang/String;)V
    .locals 0

    .line 610
    iput-object p1, p0, Lcom/anythink/core/c/d;->n:Ljava/lang/String;

    return-void
.end method

.method private o(I)V
    .locals 0

    .line 687
    iput p1, p0, Lcom/anythink/core/c/d;->S:I

    return-void
.end method

.method private o(Ljava/lang/String;)V
    .locals 0

    .line 618
    iput-object p1, p0, Lcom/anythink/core/c/d;->o:Ljava/lang/String;

    return-void
.end method

.method private p(Ljava/lang/String;)V
    .locals 0

    .line 626
    iput-object p1, p0, Lcom/anythink/core/c/d;->p:Ljava/lang/String;

    return-void
.end method

.method private q(Ljava/lang/String;)V
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/anythink/core/c/d;->q:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .line 463
    iget v0, p0, Lcom/anythink/core/c/d;->v:I

    return v0
.end method

.method public final B()J
    .locals 2

    .line 471
    iget-wide v0, p0, Lcom/anythink/core/c/d;->w:J

    return-wide v0
.end method

.method public final C()I
    .locals 1

    .line 479
    iget v0, p0, Lcom/anythink/core/c/d;->u:I

    return v0
.end method

.method public final D()J
    .locals 2

    .line 492
    iget-wide v0, p0, Lcom/anythink/core/c/d;->c:J

    return-wide v0
.end method

.method public final E()I
    .locals 1

    .line 500
    iget v0, p0, Lcom/anythink/core/c/d;->d:I

    return v0
.end method

.method public final F()I
    .locals 1

    .line 516
    iget v0, p0, Lcom/anythink/core/c/d;->f:I

    return v0
.end method

.method public final G()J
    .locals 2

    .line 524
    iget-wide v0, p0, Lcom/anythink/core/c/d;->g:J

    return-wide v0
.end method

.method public final H()J
    .locals 2

    .line 532
    iget-wide v0, p0, Lcom/anythink/core/c/d;->h:J

    return-wide v0
.end method

.method public final I()J
    .locals 2

    .line 540
    iget-wide v0, p0, Lcom/anythink/core/c/d;->i:J

    return-wide v0
.end method

.method public final J()I
    .locals 1

    .line 574
    iget v0, p0, Lcom/anythink/core/c/d;->m:I

    return v0
.end method

.method public final K()Lcom/anythink/core/common/d/z;
    .locals 1

    .line 598
    iget-object v0, p0, Lcom/anythink/core/c/d;->B:Lcom/anythink/core/common/d/z;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .line 606
    iget-object v0, p0, Lcom/anythink/core/c/d;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .line 614
    iget-object v0, p0, Lcom/anythink/core/c/d;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 1

    .line 622
    iget-object v0, p0, Lcom/anythink/core/c/d;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .line 630
    iget-object v0, p0, Lcom/anythink/core/c/d;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final P()J
    .locals 2

    .line 659
    iget-wide v0, p0, Lcom/anythink/core/c/d;->P:J

    return-wide v0
.end method

.method public final Q()I
    .locals 1

    .line 667
    iget v0, p0, Lcom/anythink/core/c/d;->Q:I

    return v0
.end method

.method public final R()J
    .locals 2

    .line 675
    iget-wide v0, p0, Lcom/anythink/core/c/d;->R:J

    return-wide v0
.end method

.method public final S()I
    .locals 1

    .line 683
    iget v0, p0, Lcom/anythink/core/c/d;->S:I

    return v0
.end method

.method public final T()Z
    .locals 2

    .line 1716
    iget v0, p0, Lcom/anythink/core/c/d;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final U()Z
    .locals 5

    .line 2154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Already cache time -- > "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/anythink/core/c/d;->ac:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Placement"

    invoke-static {v1, v0}, Lcom/anythink/core/common/g/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/anythink/core/c/d;->ac:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/anythink/core/c/d;->b:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/anythink/core/c/d;->Y:Ljava/util/List;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/anythink/core/c/d$b;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/anythink/core/c/d$b;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "age"

    .line 2162
    iget-object v1, p3, Lcom/anythink/core/c/d$b;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/anythink/core/common/g/h;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2165
    :try_start_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->h()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2166
    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2167
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0xd

    if-gt v0, v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    :cond_0
    const/4 v0, 0x0

    .line 2176
    :goto_0
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/anythink/core/c/b;->a(Landroid/content/Context;)Lcom/anythink/core/c/b;

    move-result-object v4

    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/anythink/core/common/b/g;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/anythink/core/c/b;->b(Ljava/lang/String;)Lcom/anythink/core/c/a;

    move-result-object v4

    .line 50518
    iget-object v5, p3, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    const-string v6, "payload"

    .line 2177
    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2178
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->b()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "app_ccpa_switch"

    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179
    invoke-virtual {v4}, Lcom/anythink/core/c/a;->c()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v2, "app_coppa_switch"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2180
    invoke-static {}, Lcom/anythink/core/common/b/g;->a()Lcom/anythink/core/common/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/g;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anythink/core/common/b/h;->a(Landroid/content/Context;)Lcom/anythink/core/common/b/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anythink/core/common/b/h;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v2, "gdpr_consent"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50519
    iget v0, p0, Lcom/anythink/core/c/d;->u:I

    .line 2182
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "0"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2183
    invoke-virtual {p3}, Lcom/anythink/core/c/d$b;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, "request_ad_num"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198
    :cond_3
    iget v0, p3, Lcom/anythink/core/c/d$b;->b:I

    const/16 v2, 0x23

    if-eq v0, v2, :cond_4

    iget v0, p3, Lcom/anythink/core/c/d$b;->b:I

    const/16 v3, 0x42

    if-eq v0, v3, :cond_4

    iget v0, p3, Lcom/anythink/core/c/d$b;->K:I

    sget v3, Lcom/anythink/core/c/d$b;->P:I

    if-ne v0, v3, :cond_8

    .line 2200
    :cond_4
    new-instance v0, Lcom/anythink/core/common/d/i;

    invoke-direct {v0}, Lcom/anythink/core/common/d/i;-><init>()V

    .line 50520
    iget-object v3, p3, Lcom/anythink/core/c/d$b;->o:Ljava/lang/String;

    .line 2201
    iput-object v3, v0, Lcom/anythink/core/common/d/i;->a:Ljava/lang/String;

    .line 2202
    iget v3, p3, Lcom/anythink/core/c/d$b;->b:I

    iput v3, v0, Lcom/anythink/core/common/d/i;->f:I

    .line 2203
    iget-object v3, p3, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    iput-object v3, v0, Lcom/anythink/core/common/d/i;->c:Ljava/lang/String;

    .line 2204
    iput-object p2, v0, Lcom/anythink/core/common/d/i;->d:Ljava/lang/String;

    .line 2205
    iput-object p1, v0, Lcom/anythink/core/common/d/i;->b:Ljava/lang/String;

    .line 2206
    iget p1, p0, Lcom/anythink/core/c/d;->C:I

    iput p1, v0, Lcom/anythink/core/common/d/i;->h:I

    .line 2207
    iget p1, p0, Lcom/anythink/core/c/d;->m:I

    iput p1, v0, Lcom/anythink/core/common/d/i;->i:I

    .line 2208
    iget-object p1, p3, Lcom/anythink/core/c/d$b;->c:Ljava/lang/String;

    iput-object p1, v0, Lcom/anythink/core/common/d/i;->g:Ljava/lang/String;

    .line 2209
    invoke-virtual {p3}, Lcom/anythink/core/c/d$b;->d()Lcom/anythink/core/common/d/l;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 2210
    iget-object p1, p1, Lcom/anythink/core/common/d/l;->g:Ljava/lang/String;

    goto :goto_3

    :cond_5
    const-string p1, ""

    :goto_3
    iput-object p1, v0, Lcom/anythink/core/common/d/i;->j:Ljava/lang/String;

    .line 2211
    iget p1, p3, Lcom/anythink/core/c/d$b;->b:I

    if-ne p1, v2, :cond_6

    .line 50521
    iget-object p1, p0, Lcom/anythink/core/c/d;->U:Lcom/anythink/core/common/d/r;

    .line 2212
    iput-object p1, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    goto :goto_4

    .line 50522
    :cond_6
    iget-object p1, p0, Lcom/anythink/core/c/d;->ab:Ljava/lang/String;

    .line 2214
    invoke-static {p1}, Lcom/anythink/core/common/d/v;->b(Ljava/lang/String;)Lcom/anythink/core/common/d/v;

    move-result-object p1

    iput-object p1, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    .line 2216
    :goto_4
    iget-object p1, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    if-eqz p1, :cond_7

    .line 2217
    iget-object p1, v0, Lcom/anythink/core/common/d/i;->k:Lcom/anythink/core/common/d/j;

    iget p2, p0, Lcom/anythink/core/c/d;->u:I

    invoke-virtual {p1, p2}, Lcom/anythink/core/common/d/j;->h(I)V

    :cond_7
    const-string p1, "basead_params"

    .line 2220
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    return-object v1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .line 346
    iget-object v0, p0, Lcom/anythink/core/c/d;->t:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 349
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/anythink/core/c/d$b;

    .line 350
    iget-object v2, v2, Lcom/anythink/core/c/d$b;->t:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v1
.end method

.method public final b()D
    .locals 2

    .line 221
    iget-wide v0, p0, Lcom/anythink/core/c/d;->W:D

    return-wide v0
.end method

.method public final c(Ljava/lang/String;)Lcom/anythink/core/common/d/p;
    .locals 3

    .line 2140
    iget-object v0, p0, Lcom/anythink/core/c/d;->T:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2141
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anythink/core/common/d/p;

    .line 2142
    invoke-virtual {v1}, Lcom/anythink/core/common/d/p;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/anythink/core/c/d;->U:Lcom/anythink/core/common/d/r;

    invoke-virtual {v1, v2}, Lcom/anythink/core/common/d/p;->a(Lcom/anythink/core/common/d/r;)Z

    move-result v2

    if-nez v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/anythink/core/c/d;->X:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .line 237
    iget-wide v0, p0, Lcom/anythink/core/c/d;->V:J

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/anythink/core/c/d;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/anythink/core/c/d;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/anythink/core/c/d;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/anythink/core/common/d/r;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/anythink/core/c/d;->U:Lcom/anythink/core/common/d/r;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/anythink/core/common/d/p;",
            ">;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/anythink/core/c/d;->T:Ljava/util/List;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 314
    iget-object v0, p0, Lcom/anythink/core/c/d;->s:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/anythink/core/c/d$b;",
            ">;"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/anythink/core/c/d;->t:Ljava/util/List;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .line 330
    iget-wide v0, p0, Lcom/anythink/core/c/d;->M:J

    return-wide v0
.end method

.method public final m()J
    .locals 2

    .line 338
    iget-wide v0, p0, Lcom/anythink/core/c/d;->N:J

    return-wide v0
.end method

.method public final n()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/anythink/core/api/ATRewardInfo;",
            ">;"
        }
    .end annotation

    .line 358
    iget-object v0, p0, Lcom/anythink/core/c/d;->I:Ljava/util/Map;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/anythink/core/c/d;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/anythink/core/c/d;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/anythink/core/api/ATRewardInfo;
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/anythink/core/c/d;->J:Lcom/anythink/core/api/ATRewardInfo;

    return-object v0
.end method

.method public final r()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lcom/anythink/core/c/d;->H:Ljava/util/Map;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .line 398
    iget v0, p0, Lcom/anythink/core/c/d;->F:I

    return v0
.end method

.method public final t()I
    .locals 1

    .line 406
    iget v0, p0, Lcom/anythink/core/c/d;->C:I

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .line 414
    iget-object v0, p0, Lcom/anythink/core/c/d;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final v()I
    .locals 1

    .line 422
    iget v0, p0, Lcom/anythink/core/c/d;->E:I

    return v0
.end method

.method public final w()J
    .locals 2

    .line 431
    iget-wide v0, p0, Lcom/anythink/core/c/d;->x:J

    return-wide v0
.end method

.method public final x()J
    .locals 2

    .line 439
    iget-wide v0, p0, Lcom/anythink/core/c/d;->y:J

    return-wide v0
.end method

.method public final y()I
    .locals 1

    .line 447
    iget v0, p0, Lcom/anythink/core/c/d;->z:I

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .line 455
    iget-object v0, p0, Lcom/anythink/core/c/d;->A:Ljava/lang/String;

    return-object v0
.end method
