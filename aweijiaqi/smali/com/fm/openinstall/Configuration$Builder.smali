.class public Lcom/fm/openinstall/Configuration$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fm/openinstall/Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fm/openinstall/Configuration$Builder;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fm/openinstall/Configuration$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/fm/openinstall/Configuration$Builder;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public adEnabled(Z)Lcom/fm/openinstall/Configuration$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/fm/openinstall/Configuration$Builder;->a:Z

    return-object p0
.end method

.method public build()Lcom/fm/openinstall/Configuration;
    .locals 5

    new-instance v0, Lcom/fm/openinstall/Configuration;

    iget-boolean v1, p0, Lcom/fm/openinstall/Configuration$Builder;->a:Z

    iget-object v2, p0, Lcom/fm/openinstall/Configuration$Builder;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/fm/openinstall/Configuration$Builder;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/fm/openinstall/Configuration;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/fm/openinstall/Configuration$1;)V

    return-object v0
.end method

.method public gaid(Ljava/lang/String;)Lcom/fm/openinstall/Configuration$Builder;
    .locals 0

    iput-object p1, p0, Lcom/fm/openinstall/Configuration$Builder;->c:Ljava/lang/String;

    return-object p0
.end method

.method public oaid(Ljava/lang/String;)Lcom/fm/openinstall/Configuration$Builder;
    .locals 0

    iput-object p1, p0, Lcom/fm/openinstall/Configuration$Builder;->b:Ljava/lang/String;

    return-object p0
.end method
