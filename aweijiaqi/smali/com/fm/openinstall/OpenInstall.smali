.class public Lcom/fm/openinstall/OpenInstall;
.super Ljava/lang/Object;


# static fields
.field private static a:Lio/openinstall/d; = null

.field private static volatile b:Z = false

.field private static c:Landroid/content/Context;

.field private static d:Ljava/lang/Runnable;

.field private static e:Lcom/fm/openinstall/Configuration;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/fm/openinstall/Configuration;Ljava/lang/Runnable;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/fm/openinstall/OpenInstall;->init(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    const/4 p0, 0x0

    sput-object p0, Lcom/fm/openinstall/OpenInstall;->d:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private static a()Z
    .locals 3

    sget-boolean v0, Lcom/fm/openinstall/OpenInstall;->b:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "\u8bf7\u5148\u8c03\u7528 init(Context) \u521d\u59cb\u5316"

    invoke-static {v2, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public static checkYYB(Landroid/content/Intent;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const-string v3, "openinstall_intent"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    return v0

    :cond_1
    sget-object v3, Lio/openinstall/k/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lio/openinstall/k/b;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lio/openinstall/k/b;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    return v2

    :cond_3
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object p0

    if-eqz v1, :cond_4

    if-eqz p0, :cond_4

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-interface {p0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    return v0

    :cond_4
    return v2
.end method

.method public static getInstall(Lcom/fm/openinstall/listener/AppInstallListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/fm/openinstall/OpenInstall;->getInstall(Lcom/fm/openinstall/listener/AppInstallListener;I)V

    return-void
.end method

.method public static getInstall(Lcom/fm/openinstall/listener/AppInstallListener;I)V
    .locals 3

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    invoke-interface {p0, p1, p1}, Lcom/fm/openinstall/listener/AppInstallListener;->onInstallFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    return-void

    :cond_0
    sget-object v0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2, p0}, Lio/openinstall/d;->a(JLcom/fm/openinstall/listener/AppInstallListener;)V

    return-void
.end method

.method public static getUpdateApk(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V
    .locals 1

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/fm/openinstall/listener/GetUpdateApkListener;->onGetFinish(Ljava/io/File;)V

    return-void

    :cond_0
    sget-object v0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {v0, p0}, Lio/openinstall/d;->a(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V

    return-void
.end method

.method public static getWakeUp(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)Z
    .locals 2

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-static {p0}, Lcom/fm/openinstall/OpenInstall;->isValidIntent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {v0, p0, p1}, Lio/openinstall/d;->a(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    const/4 p0, 0x1

    return p0

    :cond_1
    return v1
.end method

.method public static getWakeUpYYB(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)Z
    .locals 2

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-static {p0}, Lcom/fm/openinstall/OpenInstall;->checkYYB(Landroid/content/Intent;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {p0, p1}, Lio/openinstall/d;->a(Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    const/4 p0, 0x1

    return p0

    :cond_1
    return v1
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, Lcom/fm/openinstall/Configuration;->getDefault()Lcom/fm/openinstall/Configuration;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/fm/openinstall/OpenInstall;->init(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)V

    return-void
.end method

.method public static init(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)V
    .locals 2

    invoke-static {p0}, Lio/openinstall/k/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0, p1}, Lcom/fm/openinstall/OpenInstall;->init(Landroid/content/Context;Ljava/lang/String;Lcom/fm/openinstall/Configuration;)V

    return-void

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "\u8bf7\u5728AndroidManifest.xml\u4e2d\u914d\u7f6eOpenInstall\u63d0\u4f9b\u7684AppKey"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/fm/openinstall/Configuration;->getDefault()Lcom/fm/openinstall/Configuration;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/fm/openinstall/OpenInstall;->init(Landroid/content/Context;Ljava/lang/String;Lcom/fm/openinstall/Configuration;)V

    return-void
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Lcom/fm/openinstall/Configuration;)V
    .locals 3

    if-eqz p0, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "SDK Version : 2.5.0"

    invoke-static {v2, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-static {p0}, Lcom/fm/openinstall/OpenInstall;->isMainProcess(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "\u4e3a\u4e86\u4e0d\u5f71\u54cd\u6570\u636e\u7684\u83b7\u53d6\uff0c\u8bf7\u53ea\u5728\u4e3b\u8fdb\u7a0b\u4e2d\u521d\u59cb\u5316 OpenInstall"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    if-nez p2, :cond_2

    invoke-static {}, Lcom/fm/openinstall/Configuration;->getDefault()Lcom/fm/openinstall/Configuration;

    move-result-object p2

    :cond_2
    const-class v0, Lcom/fm/openinstall/OpenInstall;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/fm/openinstall/OpenInstall;->b:Z

    if-nez v1, :cond_4

    sget-object v1, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    if-nez v1, :cond_3

    invoke-static {p0, p2}, Lio/openinstall/d;->a(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)Lio/openinstall/d;

    move-result-object p0

    sput-object p0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {p0, p1}, Lio/openinstall/d;->a(Ljava/lang/String;)V

    :cond_3
    const/4 p0, 0x1

    sput-boolean p0, Lcom/fm/openinstall/OpenInstall;->b:Z

    :cond_4
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "\u8bf7\u524d\u5f80OpenInstall\u63a7\u5236\u53f0\u7684 \u201cAndroid\u96c6\u6210\u201d -> \u201cAndroid\u5e94\u7528\u914d\u7f6e\u201d \u4e2d\u83b7\u53d6AppKey"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_6
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "context\u4e0d\u80fd\u4e3a\u7a7a"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static initWithPermission(Landroid/app/Activity;Lcom/fm/openinstall/Configuration;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/fm/openinstall/OpenInstall;->initWithPermission(Landroid/app/Activity;Lcom/fm/openinstall/Configuration;Ljava/lang/Runnable;)V

    return-void
.end method

.method public static initWithPermission(Landroid/app/Activity;Lcom/fm/openinstall/Configuration;Ljava/lang/Runnable;)V
    .locals 2

    invoke-static {p0}, Lio/openinstall/k/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.permission.READ_PHONE_STATE"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e7

    invoke-static {p0, v0, v1}, Lio/openinstall/k/f;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/fm/openinstall/OpenInstall;->c:Landroid/content/Context;

    sput-object p2, Lcom/fm/openinstall/OpenInstall;->d:Ljava/lang/Runnable;

    sput-object p1, Lcom/fm/openinstall/OpenInstall;->e:Lcom/fm/openinstall/Configuration;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/fm/openinstall/OpenInstall;->a(Landroid/content/Context;Lcom/fm/openinstall/Configuration;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public static isMainProcess(Landroid/content/Context;)Z
    .locals 5

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const-string v1, "activity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget-object p0, p0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_3
    return v2
.end method

.method public static isValidIntent(Landroid/content/Intent;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lio/openinstall/h/a;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\|"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 p0, 0x1

    return p0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return v0
.end method

.method public static onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    sget-object p1, Lcom/fm/openinstall/OpenInstall;->c:Landroid/content/Context;

    if-eqz p1, :cond_0

    const/16 p2, 0x3e7

    if-ne p0, p2, :cond_0

    sget-object p0, Lcom/fm/openinstall/OpenInstall;->e:Lcom/fm/openinstall/Configuration;

    sget-object p2, Lcom/fm/openinstall/OpenInstall;->d:Ljava/lang/Runnable;

    invoke-static {p1, p0, p2}, Lcom/fm/openinstall/OpenInstall;->a(Landroid/content/Context;Lcom/fm/openinstall/Configuration;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public static reportEffectPoint(Ljava/lang/String;J)V
    .locals 1

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {v0, p0, p1, p2}, Lio/openinstall/d;->a(Ljava/lang/String;J)V

    return-void
.end method

.method public static reportRegister()V
    .locals 1

    invoke-static {}, Lcom/fm/openinstall/OpenInstall;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/fm/openinstall/OpenInstall;->a:Lio/openinstall/d;

    invoke-virtual {v0}, Lio/openinstall/d;->a()V

    return-void
.end method

.method public static setDebug(Z)V
    .locals 0

    sput-boolean p0, Lio/openinstall/k/c;->a:Z

    return-void
.end method
