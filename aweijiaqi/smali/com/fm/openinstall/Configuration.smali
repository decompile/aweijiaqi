.class public Lcom/fm/openinstall/Configuration;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fm/openinstall/Configuration$Builder;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/fm/openinstall/Configuration;->a:Z

    iput-object p2, p0, Lcom/fm/openinstall/Configuration;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/fm/openinstall/Configuration;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/fm/openinstall/Configuration$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/fm/openinstall/Configuration;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getDefault()Lcom/fm/openinstall/Configuration;
    .locals 3

    new-instance v0, Lcom/fm/openinstall/Configuration;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, v1}, Lcom/fm/openinstall/Configuration;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getGaid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fm/openinstall/Configuration;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getOaid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fm/openinstall/Configuration;->b:Ljava/lang/String;

    return-object v0
.end method

.method public isAdEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/fm/openinstall/Configuration;->a:Z

    return v0
.end method
