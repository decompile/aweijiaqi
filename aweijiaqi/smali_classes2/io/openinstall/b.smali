.class public Lio/openinstall/b;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:Lio/openinstall/c;

.field private b:Ljava/util/concurrent/CountDownLatch;

.field private c:Ljava/util/concurrent/LinkedBlockingQueue;

.field private d:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lio/openinstall/b;->b:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lio/openinstall/b;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lio/openinstall/b;->d:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Lio/openinstall/c;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    return-void
.end method

.method public a()Z
    .locals 2

    const-wide/16 v0, 0xa

    invoke-virtual {p0, v0, v1}, Lio/openinstall/b;->a(J)Z

    move-result v0

    return v0
.end method

.method public a(J)Z
    .locals 3

    iget-object v0, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    sget-object v1, Lio/openinstall/c;->a:Lio/openinstall/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    sget-object v1, Lio/openinstall/c;->b:Lio/openinstall/c;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lio/openinstall/b;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    iget-object v1, p0, Lio/openinstall/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    :try_start_0
    iget-object v0, p0, Lio/openinstall/b;->b:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0x1

    add-long/2addr p1, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :cond_1
    :goto_0
    iget-object p1, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    sget-object p2, Lio/openinstall/c;->c:Lio/openinstall/c;

    if-ne p1, p2, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public b()Lio/openinstall/c;
    .locals 1

    iget-object v0, p0, Lio/openinstall/b;->a:Lio/openinstall/c;

    return-object v0
.end method

.method public b(J)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lio/openinstall/b;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lio/openinstall/b;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
