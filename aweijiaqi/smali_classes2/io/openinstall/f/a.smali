.class public Lio/openinstall/f/a;
.super Lio/openinstall/f/o;


# instance fields
.field private final m:Lio/openinstall/k/d;

.field private n:Lio/openinstall/a/c;

.field private o:Lio/openinstall/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lio/openinstall/f/o;-><init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V

    const-string p1, "CoreHandler"

    invoke-static {p1}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object p1

    iput-object p1, p0, Lio/openinstall/f/a;->m:Lio/openinstall/k/d;

    const/4 p1, 0x0

    iput-object p1, p0, Lio/openinstall/f/a;->n:Lio/openinstall/a/c;

    iput-object p1, p0, Lio/openinstall/f/a;->o:Lio/openinstall/g;

    new-instance p1, Lio/openinstall/g;

    invoke-direct {p1}, Lio/openinstall/g;-><init>()V

    iput-object p1, p0, Lio/openinstall/f/a;->o:Lio/openinstall/g;

    new-instance p1, Lio/openinstall/a/c;

    invoke-direct {p1}, Lio/openinstall/a/c;-><init>()V

    iput-object p1, p0, Lio/openinstall/f/a;->n:Lio/openinstall/a/c;

    return-void
.end method

.method private a(J)J
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    const-wide/16 p1, 0xa

    :cond_0
    return-wide p1
.end method

.method static synthetic a(Lio/openinstall/f/a;Ljava/lang/String;)Lcom/fm/openinstall/model/AppData;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lio/openinstall/f/a;->c(Ljava/lang/String;)Lcom/fm/openinstall/model/AppData;

    move-result-object p0

    return-object p0
.end method

.method private a(Lio/openinstall/b/c;)Lio/openinstall/b/c;
    .locals 1

    iget-object v0, p0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->e()Lio/openinstall/b/c;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0, p1}, Lio/openinstall/f;->a(Lio/openinstall/b/c;)V

    return-object p1
.end method

.method static synthetic a(Lio/openinstall/f/a;Lio/openinstall/b/c;)Lio/openinstall/b/c;
    .locals 0

    invoke-direct {p0, p1}, Lio/openinstall/f/a;->a(Lio/openinstall/b/c;)Lio/openinstall/b/c;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lio/openinstall/f/a;)Lio/openinstall/g;
    .locals 0

    iget-object p0, p0, Lio/openinstall/f/a;->o:Lio/openinstall/g;

    return-object p0
.end method

.method static synthetic a(Lio/openinstall/f/a;ZLjava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lio/openinstall/f/a;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(ZLjava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    invoke-static {}, Lio/openinstall/h/a;->b()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lio/openinstall/h/a;->a()Ljava/lang/String;

    move-result-object p1

    :goto_0
    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    iget-object v1, p0, Lio/openinstall/f/a;->f:Ljava/lang/String;

    aput-object v1, v0, p1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "https://%s/api/v2/android/%s/%s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic b(Lio/openinstall/f/a;)Lio/openinstall/a/c;
    .locals 0

    iget-object p0, p0, Lio/openinstall/f/a;->n:Lio/openinstall/a/c;

    return-object p0
.end method

.method private b(JLcom/fm/openinstall/listener/AppInstallListener;)V
    .locals 3

    new-instance v0, Lio/openinstall/f/n;

    invoke-direct {v0, p0, p1, p2}, Lio/openinstall/f/n;-><init>(Lio/openinstall/f/a;J)V

    new-instance v1, Lio/openinstall/f/c;

    invoke-direct {v1, p0, p3}, Lio/openinstall/f/c;-><init>(Lio/openinstall/f/a;Lcom/fm/openinstall/listener/AppInstallListener;)V

    new-instance p3, Lio/openinstall/j/b;

    iget-object v2, p0, Lio/openinstall/f/a;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-direct {p3, v2, v0, v1}, Lio/openinstall/j/b;-><init>(Ljava/util/concurrent/ThreadPoolExecutor;Ljava/util/concurrent/Callable;Lio/openinstall/j/a;)V

    invoke-virtual {p3, p1, p2}, Lio/openinstall/j/b;->a(J)V

    iget-object p1, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p1, p3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Landroid/net/Uri;)V
    .locals 1

    new-instance v0, Lio/openinstall/f/d;

    invoke-direct {v0, p0, p1}, Lio/openinstall/f/d;-><init>(Lio/openinstall/f/a;Landroid/net/Uri;)V

    iget-object p1, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V
    .locals 2

    new-instance v0, Lio/openinstall/f/l;

    invoke-direct {v0, p0, p1}, Lio/openinstall/f/l;-><init>(Lio/openinstall/f/a;Landroid/net/Uri;)V

    new-instance v1, Lio/openinstall/f/m;

    invoke-direct {v1, p0, p2, p1}, Lio/openinstall/f/m;-><init>(Lio/openinstall/f/a;Lcom/fm/openinstall/listener/AppWakeUpListener;Landroid/net/Uri;)V

    new-instance p1, Lio/openinstall/j/b;

    iget-object p2, p0, Lio/openinstall/f/a;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-direct {p1, p2, v0, v1}, Lio/openinstall/j/b;-><init>(Ljava/util/concurrent/ThreadPoolExecutor;Ljava/util/concurrent/Callable;Lio/openinstall/j/a;)V

    iget-object p2, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V
    .locals 3

    iget-object v0, p0, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".apk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lio/openinstall/f/f;

    invoke-direct {v2, p0, v0, v1, p1}, Lio/openinstall/f/f;-><init>(Lio/openinstall/f/a;Ljava/lang/String;Ljava/lang/String;Lcom/fm/openinstall/listener/GetUpdateApkListener;)V

    iget-object p1, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/fm/openinstall/model/AppData;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Lcom/fm/openinstall/model/AppData;

    invoke-direct {v0}, Lcom/fm/openinstall/model/AppData;-><init>()V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "c"

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/fm/openinstall/model/AppData;->setChannel(Ljava/lang/String;)V

    :cond_1
    const-string p1, "d"

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/fm/openinstall/model/AppData;->setData(Ljava/lang/String;)V

    :cond_2
    return-object v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Lio/openinstall/f/k;

    invoke-direct {v0, p0}, Lio/openinstall/f/k;-><init>(Lio/openinstall/f/a;)V

    iget-object v1, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private g()V
    .locals 2

    new-instance v0, Lio/openinstall/f/e;

    invoke-direct {v0, p0}, Lio/openinstall/f/e;-><init>(Lio/openinstall/f/a;)V

    iget-object v1, p0, Lio/openinstall/f/a;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    new-instance v7, Lio/openinstall/f/b;

    invoke-direct {v7, p0}, Lio/openinstall/f/b;-><init>(Lio/openinstall/f/a;)V

    new-instance v8, Lio/openinstall/f/g;

    invoke-direct {v8, p0}, Lio/openinstall/f/g;-><init>(Lio/openinstall/f/a;)V

    const/4 v1, 0x3

    const/4 v2, 0x3

    const-wide/16 v3, 0x3c

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    return-object v9
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lio/openinstall/f/j;

    invoke-direct {v0, p0}, Lio/openinstall/f/j;-><init>(Lio/openinstall/f/a;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected b()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 10

    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    new-instance v7, Lio/openinstall/f/h;

    invoke-direct {v7, p0}, Lio/openinstall/f/h;-><init>(Lio/openinstall/f/a;)V

    new-instance v8, Lio/openinstall/f/i;

    invoke-direct {v8, p0}, Lio/openinstall/f/i;-><init>(Lio/openinstall/f/a;)V

    const/4 v1, 0x3

    const/4 v2, 0x3

    const-wide/16 v3, 0x3c

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    return-object v9
.end method

.method protected c()V
    .locals 0

    invoke-super {p0}, Lio/openinstall/f/o;->c()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lio/openinstall/f/a;->f()V

    goto/16 :goto_0

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p1}, Lio/openinstall/f/p;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/fm/openinstall/listener/AppWakeUpListener;

    invoke-direct {p0, v0, p1}, Lio/openinstall/f/a;->b(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    goto :goto_0

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fm/openinstall/listener/AppInstallListener;

    invoke-virtual {p1}, Lio/openinstall/f/p;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lio/openinstall/f/a;->a(J)J

    move-result-wide v1

    invoke-direct {p0, v1, v2, v0}, Lio/openinstall/f/a;->b(JLcom/fm/openinstall/listener/AppInstallListener;)V

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, Lio/openinstall/f/a;->b(Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lio/openinstall/f/a;->g()V

    goto :goto_0

    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_5

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/fm/openinstall/listener/GetUpdateApkListener;

    invoke-direct {p0, p1}, Lio/openinstall/f/a;->b(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V

    goto :goto_0

    :cond_5
    iget p1, p1, Landroid/os/Message;->what:I

    if-nez p1, :cond_6

    invoke-virtual {p0}, Lio/openinstall/f/a;->c()V

    :cond_6
    :goto_0
    return-void
.end method
