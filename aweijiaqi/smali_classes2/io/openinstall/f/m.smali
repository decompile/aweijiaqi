.class Lio/openinstall/f/m;
.super Ljava/lang/Object;

# interfaces
.implements Lio/openinstall/j/a;


# instance fields
.field final synthetic a:Lcom/fm/openinstall/listener/AppWakeUpListener;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;Lcom/fm/openinstall/listener/AppWakeUpListener;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/m;->c:Lio/openinstall/f/a;

    iput-object p2, p0, Lio/openinstall/f/m;->a:Lcom/fm/openinstall/listener/AppWakeUpListener;

    iput-object p3, p0, Lio/openinstall/f/m;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/openinstall/h/a/b;)V
    .locals 5

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v0, v1, :cond_5

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeWakeUp success : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeWakeUp warning : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :try_start_0
    new-instance v0, Lcom/fm/openinstall/model/AppData;

    invoke-direct {v0}, Lcom/fm/openinstall/model/AppData;-><init>()V

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->b()I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v0, p0, Lio/openinstall/f/m;->c:Lio/openinstall/f/a;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;Ljava/lang/String;)Lcom/fm/openinstall/model/AppData;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lio/openinstall/h/b;->d(Ljava/lang/String;)Lio/openinstall/h/b;

    move-result-object p1

    invoke-virtual {p1}, Lio/openinstall/h/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fm/openinstall/model/AppData;->setChannel(Ljava/lang/String;)V

    invoke-virtual {p1}, Lio/openinstall/h/b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/fm/openinstall/model/AppData;->setData(Ljava/lang/String;)V

    :goto_0
    iget-object p1, p0, Lio/openinstall/f/m;->a:Lcom/fm/openinstall/listener/AppWakeUpListener;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lio/openinstall/f/m;->a:Lcom/fm/openinstall/listener/AppWakeUpListener;

    invoke-interface {p1, v0, v2}, Lcom/fm/openinstall/listener/AppWakeUpListener;->onWakeUpFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    :cond_3
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/fm/openinstall/model/AppData;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_7

    iget-object p1, p0, Lio/openinstall/f/m;->c:Lio/openinstall/f/a;

    iget-object v0, p0, Lio/openinstall/f/m;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Lio/openinstall/f/a;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_4

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v3

    const-string p1, "decodeWakeUp error : %s"

    invoke-static {p1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    iget-object p1, p0, Lio/openinstall/f/m;->a:Lcom/fm/openinstall/listener/AppWakeUpListener;

    if-eqz p1, :cond_7

    invoke-interface {p1, v2, v2}, Lcom/fm/openinstall/listener/AppWakeUpListener;->onWakeUpFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    goto :goto_1

    :cond_5
    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_6

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeWakeUp fail : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lio/openinstall/f/m;->a:Lcom/fm/openinstall/listener/AppWakeUpListener;

    if-eqz v0, :cond_7

    new-instance v1, Lcom/fm/openinstall/model/Error;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->b()I

    move-result v3

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v3, p1}, Lcom/fm/openinstall/model/Error;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v1}, Lcom/fm/openinstall/listener/AppWakeUpListener;->onWakeUpFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    :cond_7
    :goto_1
    return-void
.end method
