.class Lio/openinstall/f/c;
.super Ljava/lang/Object;

# interfaces
.implements Lio/openinstall/j/a;


# instance fields
.field final synthetic a:Lcom/fm/openinstall/listener/AppInstallListener;

.field final synthetic b:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;Lcom/fm/openinstall/listener/AppInstallListener;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/c;->b:Lio/openinstall/f/a;

    iput-object p2, p0, Lio/openinstall/f/c;->a:Lcom/fm/openinstall/listener/AppInstallListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/openinstall/h/a/b;)V
    .locals 5

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v0, v1, :cond_3

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeInstall success : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeInstall warning : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lio/openinstall/h/b;->d(Ljava/lang/String;)Lio/openinstall/h/b;

    move-result-object p1

    new-instance v0, Lcom/fm/openinstall/model/AppData;

    invoke-direct {v0}, Lcom/fm/openinstall/model/AppData;-><init>()V

    invoke-virtual {p1}, Lio/openinstall/h/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fm/openinstall/model/AppData;->setChannel(Ljava/lang/String;)V

    invoke-virtual {p1}, Lio/openinstall/h/b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/fm/openinstall/model/AppData;->setData(Ljava/lang/String;)V

    iget-object p1, p0, Lio/openinstall/f/c;->a:Lcom/fm/openinstall/listener/AppInstallListener;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lio/openinstall/f/c;->a:Lcom/fm/openinstall/listener/AppInstallListener;

    invoke-interface {p1, v0, v2}, Lcom/fm/openinstall/listener/AppInstallListener;->onInstallFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_2

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v3

    const-string p1, "decodeInstall error : %s"

    invoke-static {p1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    iget-object p1, p0, Lio/openinstall/f/c;->a:Lcom/fm/openinstall/listener/AppInstallListener;

    if-eqz p1, :cond_5

    invoke-interface {p1, v2, v2}, Lcom/fm/openinstall/listener/AppInstallListener;->onInstallFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    goto :goto_0

    :cond_3
    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_4

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "decodeInstall fail : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lio/openinstall/f/c;->a:Lcom/fm/openinstall/listener/AppInstallListener;

    if-eqz v0, :cond_5

    new-instance v1, Lcom/fm/openinstall/model/Error;

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->b()I

    move-result v3

    invoke-virtual {p1}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v3, p1}, Lcom/fm/openinstall/model/Error;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v2, v1}, Lcom/fm/openinstall/listener/AppInstallListener;->onInstallFinish(Lcom/fm/openinstall/model/AppData;Lcom/fm/openinstall/model/Error;)V

    :cond_5
    :goto_0
    return-void
.end method
