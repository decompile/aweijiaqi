.class public abstract Lio/openinstall/f/o;
.super Landroid/os/Handler;


# instance fields
.field protected a:Ljava/util/concurrent/ThreadPoolExecutor;

.field protected b:Ljava/util/concurrent/ThreadPoolExecutor;

.field protected c:Landroid/content/Context;

.field protected d:Lio/openinstall/b;

.field protected e:Lio/openinstall/c/b;

.field protected f:Ljava/lang/String;

.field protected g:Lio/openinstall/f;

.field protected h:Lio/openinstall/i;

.field protected i:Lio/openinstall/b/a;

.field protected j:Lio/openinstall/g/d;

.field protected k:Lcom/fm/openinstall/Configuration;

.field protected l:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V
    .locals 0

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lio/openinstall/f/o;->c:Landroid/content/Context;

    iput-object p3, p0, Lio/openinstall/f/o;->d:Lio/openinstall/b;

    invoke-virtual {p0}, Lio/openinstall/f/o;->a()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p2

    iput-object p2, p0, Lio/openinstall/f/o;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p0}, Lio/openinstall/f/o;->b()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p2

    iput-object p2, p0, Lio/openinstall/f/o;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    iput-object p5, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    iput-object p6, p0, Lio/openinstall/f/o;->k:Lcom/fm/openinstall/Configuration;

    iput-object p4, p0, Lio/openinstall/f/o;->g:Lio/openinstall/f;

    invoke-static {p1}, Lio/openinstall/i;->a(Landroid/content/Context;)Lio/openinstall/i;

    move-result-object p2

    iput-object p2, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-static {p1}, Lio/openinstall/b/a;->a(Landroid/content/Context;)Lio/openinstall/b/a;

    move-result-object p2

    iput-object p2, p0, Lio/openinstall/f/o;->i:Lio/openinstall/b/a;

    invoke-static {p1}, Lio/openinstall/g/d;->a(Landroid/content/Context;)Lio/openinstall/g/d;

    move-result-object p1

    iput-object p1, p0, Lio/openinstall/f/o;->j:Lio/openinstall/g/d;

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/concurrent/ThreadPoolExecutor;
.end method

.method public a(JLcom/fm/openinstall/listener/AppInstallListener;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {v1, p2, p1, p3}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xc

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v2}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, p2}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x1f

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, p1}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lio/openinstall/e/a;)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x15

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v2}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/o;->f:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x16

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Lio/openinstall/f/p;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v2}, Lio/openinstall/f/p;-><init>(Ljava/lang/Object;Ljava/lang/Long;Ljava/lang/Object;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract b()Ljava/util/concurrent/ThreadPoolExecutor;
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lio/openinstall/c/b;->b(Ljava/lang/String;)Lio/openinstall/c/b;

    move-result-object p1

    iget-object v0, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {v0, p1}, Lio/openinstall/c/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {v0, p1}, Lio/openinstall/c/b;->a(Lio/openinstall/c/b;)V

    iget-object p1, p0, Lio/openinstall/f/o;->g:Lio/openinstall/f;

    iget-object v0, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {p1, v0}, Lio/openinstall/f;->a(Lio/openinstall/c/b;)V

    iget-object p1, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {p1}, Lio/openinstall/c/b;->i()V

    :cond_1
    iget-object p1, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {p1}, Lio/openinstall/c/b;->h()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lio/openinstall/f/o;->j:Lio/openinstall/g/d;

    iget-object v0, p0, Lio/openinstall/f/o;->f:Ljava/lang/String;

    iget-object v1, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {v1}, Lio/openinstall/c/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lio/openinstall/g/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method protected c()V
    .locals 1

    iget-object v0, p0, Lio/openinstall/f/o;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/o;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    :cond_1
    invoke-virtual {p0}, Lio/openinstall/f/o;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method

.method public d()V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {p0, v0}, Lio/openinstall/f/o;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected e()Ljava/util/Map;
    .locals 3

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deviceId"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "macAddress"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->m()Ljava/lang/String;

    move-result-object v1

    const-string v2, "serialNumber"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->n()Ljava/lang/String;

    move-result-object v1

    const-string v2, "androidId"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pkg"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "certFinger"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "version"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    iget-object v1, p0, Lio/openinstall/f/o;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->e()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "versionCode"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    const-string v1, "apiVersion"

    const-string v2, "2.5.0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {v0}, Lio/openinstall/c/b;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/openinstall/f/o;->j:Lio/openinstall/g/d;

    iget-object v1, p0, Lio/openinstall/f/o;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/openinstall/g/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lio/openinstall/f/o;->e:Lio/openinstall/c/b;

    invoke-virtual {v0}, Lio/openinstall/c/b;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    const-string v2, "installId"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lio/openinstall/f/o;->l:Ljava/util/Map;

    return-object v0
.end method
