.class Lio/openinstall/f/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)J
    .locals 2

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    const-wide/16 v0, 0x1

    return-wide v0

    :cond_0
    const/16 v0, 0x8

    if-ge p1, v0, :cond_1

    const-wide/16 v0, 0x5

    return-wide v0

    :cond_1
    const-wide/16 v0, 0xa

    return-wide v0
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v0}, Lio/openinstall/b;->b()Lio/openinstall/c;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/openinstall/f;->a(Ljava/lang/String;)Lio/openinstall/c;

    move-result-object v0

    sget-object v1, Lio/openinstall/c;->a:Lio/openinstall/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->a()Lio/openinstall/c;

    move-result-object v0

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    iget-object v2, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v2, v2, Lio/openinstall/f/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lio/openinstall/f;->a(Ljava/lang/String;Lio/openinstall/c;)V

    :cond_0
    sget-object v1, Lio/openinstall/c;->a:Lio/openinstall/c;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v1}, Lio/openinstall/f;->g()V

    :cond_1
    sget-object v1, Lio/openinstall/c;->a:Lio/openinstall/c;

    if-eq v0, v1, :cond_3

    sget-object v1, Lio/openinstall/c;->d:Lio/openinstall/c;

    if-eq v0, v1, :cond_3

    sget-object v1, Lio/openinstall/c;->e:Lio/openinstall/c;

    if-ne v0, v1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v1}, Lio/openinstall/f;->d()Lio/openinstall/c/b;

    move-result-object v1

    iget-object v2, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v2, v2, Lio/openinstall/f/a;->e:Lio/openinstall/c/b;

    invoke-virtual {v2, v1}, Lio/openinstall/c/b;->a(Lio/openinstall/c/b;)V

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v1, v0}, Lio/openinstall/b;->a(Lio/openinstall/c;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->e:Lio/openinstall/c/b;

    invoke-virtual {v0}, Lio/openinstall/c/b;->i()V

    goto/16 :goto_7

    :cond_3
    :goto_0
    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    sget-object v1, Lio/openinstall/c;->b:Lio/openinstall/c;

    invoke-virtual {v0, v1}, Lio/openinstall/b;->a(Lio/openinstall/c;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-static {v0}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;)Lio/openinstall/g;

    move-result-object v0

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lio/openinstall/g;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v0}, Lcom/fm/openinstall/Configuration;->isAdEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v0}, Lcom/fm/openinstall/Configuration;->getOaid()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-static {v0}, Lio/openinstall/f/a;->b(Lio/openinstall/f/a;)Lio/openinstall/a/c;

    move-result-object v0

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lio/openinstall/a/c;->a(Landroid/content/Context;)V

    :cond_4
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "model"

    invoke-virtual {v0, v2, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "buildId"

    invoke-virtual {v0, v2, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "buildDisplay"

    invoke-virtual {v0, v2, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "brand"

    invoke-virtual {v0, v2, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->k()Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v3, "imei"

    invoke-virtual {v0, v3, v2}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    const-string v2, "imei2"

    invoke-virtual {v0, v2, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->o()Ljava/util/IdentityHashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/IdentityHashMap;->putAll(Ljava/util/Map;)V

    invoke-static {}, Lio/openinstall/d/c;->a()Lio/openinstall/d/c;

    move-result-object v1

    iget-object v2, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v2, v2, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lio/openinstall/d/c;->a(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    sget-boolean v3, Lio/openinstall/k/c;->a:Z

    if-eqz v3, :cond_5

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "check device is emulator"

    invoke-static {v4, v3}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v3, "simulator"

    invoke-virtual {v0, v3, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->isAdEnabled()Z

    move-result v1

    const-string v3, "gaid"

    if-eqz v1, :cond_7

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getGaid()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->c:Landroid/content/Context;

    invoke-static {v1}, Lio/openinstall/a/a;->a(Landroid/content/Context;)Lio/openinstall/a/a$a;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v1, ""

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Lio/openinstall/a/a$a;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getGaid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    sget-boolean v1, Lio/openinstall/k/c;->a:Z

    if-eqz v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u4f20\u5165\u7684 gaid \u4e3a "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v4, v4, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v4}, Lcom/fm/openinstall/Configuration;->getGaid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getGaid()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v3, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->isAdEnabled()Z

    move-result v1

    const-string v3, "oaid"

    if-eqz v1, :cond_a

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getOaid()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-static {v1}, Lio/openinstall/f/a;->b(Lio/openinstall/f/a;)Lio/openinstall/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lio/openinstall/a/c;->a()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v3, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getOaid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    sget-boolean v1, Lio/openinstall/k/c;->a:Z

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u4f20\u5165\u7684 oaid \u4e3a "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v4, v4, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v4}, Lcom/fm/openinstall/Configuration;->getOaid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->k:Lcom/fm/openinstall/Configuration;

    invoke-virtual {v1}, Lcom/fm/openinstall/Configuration;->getOaid()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_c
    :goto_3
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-static {v1}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;)Lio/openinstall/g;

    move-result-object v1

    invoke-virtual {v1}, Lio/openinstall/g;->a()Ljava/lang/String;

    move-result-object v1

    const-string v3, "gReferrer"

    invoke-virtual {v0, v3, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->i:Lio/openinstall/b/a;

    invoke-virtual {v1}, Lio/openinstall/b/a;->b()Lio/openinstall/b/c;

    move-result-object v1

    iget-object v3, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v3, v3, Lio/openinstall/f/a;->i:Lio/openinstall/b/a;

    invoke-virtual {v3}, Lio/openinstall/b/a;->c()V

    iget-object v3, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-static {v3, v1}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;Lio/openinstall/b/c;)Lio/openinstall/b/c;

    move-result-object v1

    const/4 v3, 0x1

    if-eqz v1, :cond_e

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lio/openinstall/b/c;->c(I)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v1}, Lio/openinstall/b/c;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "pbHtml"

    invoke-virtual {v0, v5, v4}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    invoke-virtual {v1, v3}, Lio/openinstall/b/c;->c(I)Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v1}, Lio/openinstall/b/c;->a()Ljava/lang/String;

    move-result-object v1

    const-string v4, "pbText"

    invoke-virtual {v0, v4, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v1}, Lio/openinstall/i;->a()Ljava/lang/String;

    move-result-object v1

    const-string v4, "apkInfo"

    invoke-virtual {v0, v4, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    :cond_f
    invoke-static {v2}, Lio/openinstall/h/a/a;->a(Z)Lio/openinstall/h/a/a;

    move-result-object v4

    iget-object v5, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    const-string v6, "init"

    invoke-static {v5, v2, v6}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-virtual {v6}, Lio/openinstall/f/a;->e()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v0}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lio/openinstall/h/a/b;

    move-result-object v4

    :try_start_0
    iget-object v5, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-direct {p0, v1}, Lio/openinstall/f/k;->a(I)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lio/openinstall/b;->b(J)Ljava/lang/Object;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v5, :cond_10

    const/4 v1, 0x0

    goto :goto_4

    :catch_0
    nop

    :cond_10
    :goto_4
    add-int/2addr v1, v3

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v5

    sget-object v6, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    if-eq v5, v6, :cond_f

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/openinstall/f/a;->b(Ljava/lang/String;)V

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    if-ne v0, v1, :cond_11

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/openinstall/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    sget-object v1, Lio/openinstall/c;->c:Lio/openinstall/c;

    :goto_5
    invoke-virtual {v0, v1}, Lio/openinstall/b;->a(Lio/openinstall/c;)V

    goto :goto_6

    :cond_11
    invoke-virtual {v4}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v1, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    if-ne v0, v1, :cond_12

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/openinstall/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    sget-object v1, Lio/openinstall/c;->e:Lio/openinstall/c;

    goto :goto_5

    :cond_12
    invoke-virtual {v4}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v1, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    if-ne v0, v1, :cond_13

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v4}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/openinstall/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    sget-object v1, Lio/openinstall/c;->d:Lio/openinstall/c;

    goto :goto_5

    :cond_13
    :goto_6
    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v0}, Lio/openinstall/b;->c()V

    iget-object v0, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    iget-object v1, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v1, v1, Lio/openinstall/f/a;->f:Ljava/lang/String;

    iget-object v2, p0, Lio/openinstall/f/k;->a:Lio/openinstall/f/a;

    iget-object v2, v2, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v2}, Lio/openinstall/b;->b()Lio/openinstall/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/openinstall/f;->a(Ljava/lang/String;Lio/openinstall/c;)V

    :goto_7
    return-void
.end method
