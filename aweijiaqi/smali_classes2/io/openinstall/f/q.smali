.class public Lio/openinstall/f/q;
.super Lio/openinstall/f/o;


# instance fields
.field private m:Lio/openinstall/k/d;

.field private n:Ljava/io/File;

.field private o:Ljava/lang/Long;

.field private p:J

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lio/openinstall/f/o;-><init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V

    const-string p2, "StatsHandler"

    invoke-static {p2}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object p2

    iput-object p2, p0, Lio/openinstall/f/q;->m:Lio/openinstall/k/d;

    new-instance p2, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p1

    const-string p3, "AliveLog.txt"

    invoke-direct {p2, p1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object p2, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {p4}, Lio/openinstall/f;->f()Z

    move-result p1

    iput-boolean p1, p0, Lio/openinstall/f/q;->q:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lio/openinstall/f/q;->p:J

    new-instance p1, Lio/openinstall/f/r;

    invoke-direct {p1, p0}, Lio/openinstall/f/r;-><init>(Lio/openinstall/f/q;)V

    invoke-virtual {p5, p1}, Lio/openinstall/c/b;->a(Lio/openinstall/c/a;)V

    return-void
.end method

.method static synthetic a(Lio/openinstall/f/q;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/q;->o:Ljava/lang/Long;

    return-object p1
.end method

.method private b(Lio/openinstall/e/a;)V
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/32 v3, 0xa00000

    cmp-long v5, v1, v3

    if-ltz v5, :cond_1

    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    :cond_1
    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v2, Ljava/io/BufferedWriter;

    invoke-direct {v2, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {p1}, Lio/openinstall/e/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V

    invoke-virtual {p1}, Lio/openinstall/e/a;->a()Z

    move-result p1

    invoke-virtual {p0, p1}, Lio/openinstall/f/q;->a(Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V

    :goto_0
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_3

    :catchall_0
    move-exception p1

    move-object v0, v2

    goto :goto_1

    :catch_0
    move-object v0, v2

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_1

    :catchall_2
    move-exception p1

    move-object v1, v0

    :goto_1
    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    :cond_3
    throw p1

    :catch_2
    move-object v1, v0

    :catch_3
    :goto_2
    if-eqz v0, :cond_4

    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_4
    if-eqz v1, :cond_5

    goto :goto_0

    :catch_4
    :cond_5
    :goto_3
    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {}, Lio/openinstall/h/a;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lio/openinstall/f/q;->f:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const-string p1, "https://%s/api/v2/android/%s/%s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lio/openinstall/f/q;->d:Lio/openinstall/b;

    invoke-virtual {v0}, Lio/openinstall/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/openinstall/f/q;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->c()Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/q;->e:Lio/openinstall/c/b;

    invoke-virtual {v0}, Lio/openinstall/c/b;->e()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    iget-object v3, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_5

    :catch_0
    nop

    goto :goto_5

    :catchall_0
    move-exception v0

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v3, v1

    :goto_2
    move-object v1, v2

    goto :goto_3

    :catch_1
    move-object v3, v1

    :catch_2
    move-object v1, v2

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v3, v1

    :goto_3
    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    :cond_5
    throw v0

    :catch_4
    move-object v3, v1

    :goto_4
    if-eqz v1, :cond_6

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_6
    if-eqz v3, :cond_7

    goto :goto_1

    :cond_7
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    return-void

    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Lio/openinstall/h/a/a;->a(Z)Lio/openinstall/h/a/a;

    move-result-object v2

    const-string v3, "stats/events"

    invoke-direct {p0, v3}, Lio/openinstall/f/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lio/openinstall/f/q;->e()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lio/openinstall/h/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lio/openinstall/f/q;->b(Ljava/lang/String;)V

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v2

    sget-object v3, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    const/4 v4, 0x1

    if-ne v2, v3, :cond_a

    sget-boolean v2, Lio/openinstall/k/c;->a:Z

    if-eqz v2, :cond_9

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "statEvents success : %s"

    invoke-static {v3, v2}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_9
    invoke-virtual {v0}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    sget-boolean v2, Lio/openinstall/k/c;->a:Z

    if-eqz v2, :cond_b

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "statEvents warning : %s"

    invoke-static {v3, v2}, Lio/openinstall/k/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :cond_a
    sget-boolean v2, Lio/openinstall/k/c;->a:Z

    if-eqz v2, :cond_b

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "statEvents fail : %s"

    invoke-static {v3, v2}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    :goto_6
    invoke-virtual {v0}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v0

    sget-object v2, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    if-ne v0, v2, :cond_d

    iget-object v0, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lio/openinstall/f/q;->n:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lio/openinstall/f/q;->p:J

    iget-boolean v0, p0, Lio/openinstall/f/q;->q:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lio/openinstall/f/q;->g:Lio/openinstall/f;

    invoke-virtual {v0, v1}, Lio/openinstall/f;->a(Z)V

    iput-boolean v1, p0, Lio/openinstall/f/q;->q:Z

    :cond_d
    return-void
.end method

.method private g()Z
    .locals 9

    iget-object v0, p0, Lio/openinstall/f/q;->o:Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lio/openinstall/f/q;->q:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v0, p0, Lio/openinstall/f/q;->o:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long v5, v5, v7

    iget-wide v7, p0, Lio/openinstall/f/q;->p:J

    sub-long/2addr v3, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method


# virtual methods
.method protected a()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected c()V
    .locals 0

    invoke-super {p0}, Lio/openinstall/f/o;->c()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/openinstall/e/a;

    invoke-direct {p0, p1}, Lio/openinstall/f/q;->b(Lio/openinstall/e/a;)V

    goto :goto_0

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_2

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lio/openinstall/f/p;

    invoke-virtual {p1}, Lio/openinstall/f/p;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-direct {p0}, Lio/openinstall/f/q;->g()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_1
    invoke-direct {p0}, Lio/openinstall/f/q;->f()V

    goto :goto_0

    :cond_2
    iget p1, p1, Landroid/os/Message;->what:I

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lio/openinstall/f/q;->c()V

    :cond_3
    :goto_0
    return-void
.end method
