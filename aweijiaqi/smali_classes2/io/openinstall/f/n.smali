.class Lio/openinstall/f/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;J)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/n;->b:Lio/openinstall/f/a;

    iput-wide p2, p0, Lio/openinstall/f/n;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lio/openinstall/h/a/b;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lio/openinstall/f/n;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    iget-wide v1, p0, Lio/openinstall/f/n;->a:J

    invoke-virtual {v0, v1, v2}, Lio/openinstall/b;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/openinstall/f/n;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    const/16 v3, -0xc

    invoke-direct {v1, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u521d\u59cb\u5316\u65f6\u9519\u8bef\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/n;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    invoke-virtual {v1, v0}, Lio/openinstall/h/a/b;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lio/openinstall/f/n;->b:Lio/openinstall/f/a;

    invoke-virtual {v1}, Lio/openinstall/h/a/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/openinstall/f/a;->b(Ljava/lang/String;)V

    return-object v1
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lio/openinstall/f/n;->a()Lio/openinstall/h/a/b;

    move-result-object v0

    return-object v0
.end method
