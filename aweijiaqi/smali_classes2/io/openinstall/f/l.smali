.class Lio/openinstall/f/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iput-object p2, p0, Lio/openinstall/f/l;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lio/openinstall/h/a/b;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v0}, Lio/openinstall/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    const/16 v3, -0xc

    invoke-direct {v1, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u521d\u59cb\u5316\u65f6\u9519\u8bef\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lio/openinstall/f/l;->a:Landroid/net/Uri;

    const-string v1, "decode-wakeup-url"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->i:Lio/openinstall/b/a;

    invoke-virtual {v0}, Lio/openinstall/b/a;->b()Lio/openinstall/b/c;

    move-result-object v0

    iget-object v4, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v4, v4, Lio/openinstall/f/a;->i:Lio/openinstall/b/a;

    invoke-virtual {v4}, Lio/openinstall/b/a;->c()V

    new-instance v4, Ljava/util/IdentityHashMap;

    invoke-direct {v4}, Ljava/util/IdentityHashMap;-><init>()V

    iget-object v5, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v5}, Lio/openinstall/i;->f()Ljava/lang/String;

    move-result-object v5

    const-string v6, "model"

    invoke-virtual {v4, v6, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v5}, Lio/openinstall/i;->g()Ljava/lang/String;

    move-result-object v5

    const-string v6, "buildId"

    invoke-virtual {v4, v6, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v5}, Lio/openinstall/i;->h()Ljava/lang/String;

    move-result-object v5

    const-string v6, "buildDisplay"

    invoke-virtual {v4, v6, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v5}, Lio/openinstall/i;->i()Ljava/lang/String;

    move-result-object v5

    const-string v6, "brand"

    invoke-virtual {v4, v6, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    iget-object v5, v5, Lio/openinstall/f/a;->h:Lio/openinstall/i;

    invoke-virtual {v5}, Lio/openinstall/i;->o()Ljava/util/IdentityHashMap;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/IdentityHashMap;->putAll(Ljava/util/Map;)V

    if-eqz v0, :cond_2

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lio/openinstall/b/c;->c(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lio/openinstall/b/c;->b()Ljava/lang/String;

    move-result-object v5

    const-string v6, "pbHtml"

    invoke-virtual {v4, v6, v5}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, v3}, Lio/openinstall/b/c;->c(I)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lio/openinstall/b/c;->a()Ljava/lang/String;

    move-result-object v0

    const-string v5, "pbText"

    invoke-virtual {v4, v5, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-static {v3}, Lio/openinstall/h/a/a;->a(Z)Lio/openinstall/h/a/a;

    move-result-object v0

    iget-object v3, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    invoke-static {v3, v2, v1}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    invoke-virtual {v2}, Lio/openinstall/f/a;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lio/openinstall/h/a/b;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    invoke-virtual {v0}, Lio/openinstall/h/a/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/openinstall/f/a;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const-string v4, "The wakeup parameter is invalid"

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_7

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "c"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lio/openinstall/k/a;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    invoke-direct {v1, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    invoke-virtual {v1, v0}, Lio/openinstall/h/a/b;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2

    :cond_4
    new-instance v0, Lio/openinstall/h/a/b;

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    invoke-direct {v0, v1, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Lio/openinstall/h/a/b;->c(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "h"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v4, p0, Lio/openinstall/f/l;->a:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "wakeupUrl"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lio/openinstall/h/a/a;->a(Z)Lio/openinstall/h/a/a;

    move-result-object v3

    iget-object v4, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    invoke-static {v4, v2, v1}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lio/openinstall/f/l;->b:Lio/openinstall/f/a;

    invoke-virtual {v2}, Lio/openinstall/f/a;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v3, v1, v2, v0}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lio/openinstall/h/a/b;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lio/openinstall/h/a/b;

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    invoke-direct {v0, v1, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    goto :goto_1

    :cond_7
    new-instance v0, Lio/openinstall/h/a/b;

    sget-object v1, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    invoke-direct {v0, v1, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    :goto_1
    invoke-virtual {v0, v4}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    :goto_2
    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lio/openinstall/f/l;->a()Lio/openinstall/h/a/b;

    move-result-object v0

    return-object v0
.end method
