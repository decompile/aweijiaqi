.class Lio/openinstall/f/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lio/openinstall/f/a;


# direct methods
.method constructor <init>(Lio/openinstall/f/a;)V
    .locals 0

    iput-object p1, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->d:Lio/openinstall/b;

    invoke-virtual {v0}, Lio/openinstall/b;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->g:Lio/openinstall/f;

    invoke-virtual {v0}, Lio/openinstall/f;->c()Ljava/lang/String;

    move-result-object v0

    sget-boolean v2, Lio/openinstall/k/c;->a:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u521d\u59cb\u5316\u65f6\u9519\u8bef\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    iget-object v0, v0, Lio/openinstall/f/a;->e:Lio/openinstall/c/b;

    invoke-virtual {v0}, Lio/openinstall/c/b;->f()Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_2

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "registerStatsEnabled is disable"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x1

    invoke-static {v0}, Lio/openinstall/h/a/a;->a(Z)Lio/openinstall/h/a/a;

    move-result-object v2

    iget-object v3, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    const-string v4, "stats/register"

    invoke-static {v3, v0, v4}, Lio/openinstall/f/a;->a(Lio/openinstall/f/a;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    invoke-virtual {v4}, Lio/openinstall/f/a;->e()Ljava/util/Map;

    move-result-object v4

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lio/openinstall/h/a/b;

    move-result-object v2

    iget-object v3, p0, Lio/openinstall/f/e;->a:Lio/openinstall/f/a;

    invoke-virtual {v2}, Lio/openinstall/h/a/b;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/openinstall/f/a;->b(Ljava/lang/String;)V

    invoke-virtual {v2}, Lio/openinstall/h/a/b;->a()Lio/openinstall/h/a/b$a;

    move-result-object v3

    sget-object v4, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    if-ne v3, v4, :cond_5

    sget-boolean v3, Lio/openinstall/k/c;->a:Z

    if-eqz v3, :cond_4

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lio/openinstall/h/a/b;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const-string v4, "statRegister success : %s"

    invoke-static {v4, v3}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-virtual {v2}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    sget-boolean v3, Lio/openinstall/k/c;->a:Z

    if-eqz v3, :cond_6

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "statRegister warning : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    sget-boolean v3, Lio/openinstall/k/c;->a:Z

    if-eqz v3, :cond_6

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lio/openinstall/h/a/b;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "statRegister fail : %s"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    :goto_0
    return-void
.end method
