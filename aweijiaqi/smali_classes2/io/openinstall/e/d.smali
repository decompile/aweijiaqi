.class public Lio/openinstall/e/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/concurrent/LinkedBlockingQueue;

.field private b:Lio/openinstall/k/d;

.field private volatile c:Z

.field private d:Ljava/lang/Thread;

.field private e:Landroid/app/Application;

.field private f:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private g:Lio/openinstall/f/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lio/openinstall/e/d;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    const-string v0, "EventsCollector"

    invoke-static {v0}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object v0

    iput-object v0, p0, Lio/openinstall/e/d;->b:Lio/openinstall/k/d;

    iput-boolean v1, p0, Lio/openinstall/e/d;->c:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iput-object v0, p0, Lio/openinstall/e/d;->e:Landroid/app/Application;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lio/openinstall/e/e;

    invoke-direct {v1, p0}, Lio/openinstall/e/e;-><init>(Lio/openinstall/e/d;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lio/openinstall/e/d;->d:Ljava/lang/Thread;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "EventsHandler-Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lio/openinstall/f/q;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    move-object v2, v1

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v2 .. v8}, Lio/openinstall/f/q;-><init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V

    iput-object v1, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    invoke-direct {p0}, Lio/openinstall/e/d;->b()V

    return-void
.end method

.method static synthetic a(Lio/openinstall/e/d;)Z
    .locals 0

    iget-boolean p0, p0, Lio/openinstall/e/d;->c:Z

    return p0
.end method

.method static synthetic b(Lio/openinstall/e/d;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 0

    iget-object p0, p0, Lio/openinstall/e/d;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object p0
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/openinstall/e/d;->c:Z

    iget-object v0, p0, Lio/openinstall/e/d;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lio/openinstall/e/d;->c()V

    return-void
.end method

.method static synthetic c(Lio/openinstall/e/d;)Lio/openinstall/f/q;
    .locals 0

    iget-object p0, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    return-object p0
.end method

.method private c()V
    .locals 2

    new-instance v0, Lio/openinstall/e/f;

    invoke-direct {v0, p0}, Lio/openinstall/e/f;-><init>(Lio/openinstall/e/d;)V

    iput-object v0, p0, Lio/openinstall/e/d;->f:Landroid/app/Application$ActivityLifecycleCallbacks;

    iget-object v1, p0, Lio/openinstall/e/d;->e:Landroid/app/Application;

    invoke-virtual {v1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    new-instance v0, Lio/openinstall/e/a;

    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "$register"

    invoke-direct {v0, v2, v1}, Lio/openinstall/e/a;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lio/openinstall/e/a;->a(Z)V

    iget-object v1, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    invoke-virtual {v1, v0}, Lio/openinstall/f/q;->a(Lio/openinstall/e/a;)V

    return-void
.end method

.method public a(J)V
    .locals 3

    const-wide/16 v0, 0x1

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    new-instance v0, Lio/openinstall/e/a;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lio/openinstall/e/a;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object p1, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    invoke-virtual {p1, v0}, Lio/openinstall/f/q;->a(Lio/openinstall/e/a;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    invoke-virtual {v0, p1}, Lio/openinstall/f/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lio/openinstall/e/a;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lio/openinstall/e/a;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object p1, p0, Lio/openinstall/e/d;->g:Lio/openinstall/f/q;

    invoke-virtual {p1, v0}, Lio/openinstall/f/q;->a(Lio/openinstall/e/a;)V

    :cond_0
    return-void
.end method
