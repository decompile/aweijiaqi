.class public Lio/openinstall/a/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/openinstall/a/a$c;,
        Lio/openinstall/a/a$b;,
        Lio/openinstall/a/a$a;
    }
.end annotation


# static fields
.field private static final a:Lio/openinstall/k/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "AdvertisingIdClient"

    invoke-static {v0}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object v0

    sput-object v0, Lio/openinstall/a/a;->a:Lio/openinstall/k/d;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lio/openinstall/a/a$a;
    .locals 7

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-ne v0, v1, :cond_1

    sget-boolean p0, Lio/openinstall/k/c;->a:Z

    if-eqz p0, :cond_0

    new-array p0, v2, [Ljava/lang/Object;

    const-string v0, "AdvertisingIdClient \u4e0d\u80fd\u5728\u4e3b\u7ebf\u7a0b\u8c03\u7528"

    invoke-static {v0, p0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v3

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    new-instance v0, Lio/openinstall/a/a$b;

    invoke-direct {v0, v3}, Lio/openinstall/a/a$b;-><init>(Lio/openinstall/a/b;)V

    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v0, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_2

    :try_start_1
    new-instance v1, Lio/openinstall/a/a$c;

    invoke-virtual {v0}, Lio/openinstall/a/a$b;->a()Landroid/os/IBinder;

    move-result-object v5

    invoke-direct {v1, v5}, Lio/openinstall/a/a$c;-><init>(Landroid/os/IBinder;)V

    new-instance v5, Lio/openinstall/a/a$a;

    invoke-virtual {v1}, Lio/openinstall/a/a$c;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4}, Lio/openinstall/a/a$c;->a(Z)Z

    move-result v1

    invoke-direct {v5, v6, v1}, Lio/openinstall/a/a$a;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-object v5

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_2

    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v1

    :cond_2
    :goto_2
    sget-boolean p0, Lio/openinstall/k/c;->a:Z

    if-eqz p0, :cond_3

    new-array p0, v2, [Ljava/lang/Object;

    const-string v0, "Google Play connection failed"

    invoke-static {v0, p0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    return-object v3
.end method
