.class public final enum Lio/openinstall/h/a/b$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/openinstall/h/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lio/openinstall/h/a/b$a;

.field public static final enum b:Lio/openinstall/h/a/b$a;

.field public static final enum c:Lio/openinstall/h/a/b$a;

.field private static final synthetic d:[Lio/openinstall/h/a/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lio/openinstall/h/a/b$a;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1}, Lio/openinstall/h/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    new-instance v0, Lio/openinstall/h/a/b$a;

    const/4 v2, 0x1

    const-string v3, "FAIL"

    invoke-direct {v0, v3, v2}, Lio/openinstall/h/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    new-instance v0, Lio/openinstall/h/a/b$a;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3}, Lio/openinstall/h/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    const/4 v4, 0x3

    new-array v4, v4, [Lio/openinstall/h/a/b$a;

    sget-object v5, Lio/openinstall/h/a/b$a;->a:Lio/openinstall/h/a/b$a;

    aput-object v5, v4, v1

    sget-object v1, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    sput-object v4, Lio/openinstall/h/a/b$a;->d:[Lio/openinstall/h/a/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/openinstall/h/a/b$a;
    .locals 1

    const-class v0, Lio/openinstall/h/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/openinstall/h/a/b$a;

    return-object p0
.end method

.method public static values()[Lio/openinstall/h/a/b$a;
    .locals 1

    sget-object v0, Lio/openinstall/h/a/b$a;->d:[Lio/openinstall/h/a/b$a;

    invoke-virtual {v0}, [Lio/openinstall/h/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/openinstall/h/a/b$a;

    return-object v0
.end method
