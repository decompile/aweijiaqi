.class public Lio/openinstall/h/a/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Lio/openinstall/k/d;

.field private b:Ljavax/net/ssl/SSLSocketFactory;

.field private c:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "HttpPoster"

    invoke-static {v0}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object v0

    iput-object v0, p0, Lio/openinstall/h/a/a;->a:Lio/openinstall/k/d;

    iput-boolean p1, p0, Lio/openinstall/h/a/a;->c:Z

    return-void
.end method

.method public static a(Z)Lio/openinstall/h/a/a;
    .locals 1

    new-instance v0, Lio/openinstall/h/a/a;

    invoke-direct {v0, p0}, Lio/openinstall/h/a/a;-><init>(Z)V

    return-object v0
.end method

.method private a(Ljava/net/HttpURLConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    instance-of v0, p1, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0, v0}, Lio/openinstall/h/a/a;->a(Ljavax/net/ssl/HttpsURLConnection;)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/16 v0, 0x1388

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    return-void
.end method

.method private a(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lio/openinstall/h/a/a;->b:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_0

    new-instance v0, Lio/openinstall/h/a/d;

    invoke-direct {v0}, Lio/openinstall/h/a/d;-><init>()V

    const-string v1, "TLS"

    invoke-static {v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljavax/net/ssl/TrustManager;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x0

    invoke-virtual {v1, v0, v2, v0}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lio/openinstall/h/a/a;->b:Ljavax/net/ssl/SSLSocketFactory;

    :cond_0
    iget-object v0, p0, Lio/openinstall/h/a/a;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {p1, v0}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lio/openinstall/h/a/b;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Lio/openinstall/h/a/b;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Lio/openinstall/h/a/b;
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p3

    const-string v2, "UTF-8"

    new-instance v3, Lio/openinstall/h/a/b;

    invoke-direct {v3}, Lio/openinstall/h/a/b;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "timestamp"

    move-object/from16 v6, p2

    invoke-interface {v6, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    move-object v4, v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    :cond_0
    const/4 v8, -0x1

    const/4 v9, 0x0

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Lio/openinstall/h/a/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v10

    check-cast v10, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    invoke-direct {v1, v10}, Lio/openinstall/h/a/a;->a(Ljava/net/HttpURLConnection;)V

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v11

    array-length v11, v11

    invoke-virtual {v10, v11}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    if-eqz p4, :cond_1

    const-string v12, "content-type"

    const-string v13, "text/plain;charset=utf-8"

    invoke-virtual {v10, v12, v13}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "content-length"

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v12, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->connect()V

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v12, Ljava/io/BufferedOutputStream;

    invoke-direct {v12, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v12}, Ljava/io/BufferedOutputStream;->flush()V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v9

    goto/16 :goto_8

    :catch_0
    move-exception v0

    move-object v2, v9

    goto/16 :goto_4

    :catch_1
    move-object v8, v9

    goto/16 :goto_7

    :cond_2
    move-object v11, v9

    move-object v12, v11

    :goto_0
    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v13

    const/16 v14, 0xc8

    if-ne v13, v14, :cond_3

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-static {v9}, Lio/openinstall/h/a/c;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lio/openinstall/h/a/b;->a(Ljava/lang/String;)Lio/openinstall/h/a/b;

    move-result-object v4

    goto :goto_1

    :cond_3
    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v13

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-static {v9}, Lio/openinstall/h/a/c;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    invoke-virtual {v4, v15}, Lio/openinstall/h/a/b;->a(Lio/openinstall/h/a/b$a;)V

    invoke-virtual {v4, v8}, Lio/openinstall/h/a/b;->a(I)V

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, " : "

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    if-eqz v12, :cond_4

    :try_start_4
    invoke-virtual {v12}, Ljava/io/BufferedOutputStream;->close()V

    goto :goto_2

    :catch_2
    nop

    goto :goto_3

    :cond_4
    :goto_2
    if-eqz v11, :cond_5

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    :cond_5
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_6
    :goto_3
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    const/4 v7, 0x1

    goto/16 :goto_e

    :catchall_1
    move-exception v0

    move-object v2, v9

    move-object v9, v12

    goto/16 :goto_8

    :catch_3
    move-exception v0

    move-object v2, v9

    move-object v9, v12

    goto :goto_4

    :catch_4
    move-object v8, v9

    move-object v9, v12

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v2, v9

    move-object v11, v2

    goto :goto_8

    :catch_5
    move-exception v0

    move-object v2, v9

    move-object v11, v2

    goto :goto_4

    :catch_6
    move-object v8, v9

    move-object v11, v8

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v2, v9

    move-object v10, v2

    move-object v11, v10

    goto :goto_8

    :catch_7
    move-exception v0

    move-object v2, v9

    move-object v10, v2

    move-object v11, v10

    :goto_4
    :try_start_5
    sget-object v3, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    invoke-virtual {v4, v3}, Lio/openinstall/h/a/b;->a(Lio/openinstall/h/a/b$a;)V

    invoke-virtual {v4, v8}, Lio/openinstall/h/a/b;->a(I)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    if-eqz v9, :cond_8

    :try_start_6
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V

    goto :goto_5

    :catch_8
    nop

    goto :goto_6

    :cond_8
    :goto_5
    if-eqz v11, :cond_9

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    :cond_9
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8

    :cond_a
    :goto_6
    if-eqz v10, :cond_13

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_f

    :catchall_4
    move-exception v0

    goto :goto_8

    :catch_9
    move-object v8, v9

    move-object v10, v8

    move-object v11, v10

    :goto_7
    add-int/lit8 v5, v5, 0x1

    const-wide/16 v12, 0x12c

    :try_start_7
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    goto :goto_b

    :catchall_5
    move-exception v0

    move-object v2, v8

    :goto_8
    if-eqz v9, :cond_b

    :try_start_8
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V

    goto :goto_9

    :catch_a
    nop

    goto :goto_a

    :cond_b
    :goto_9
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    :cond_c
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_a

    :cond_d
    :goto_a
    if-eqz v10, :cond_e

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_e
    throw v0

    :catch_b
    nop

    :goto_b
    if-eqz v9, :cond_f

    :try_start_9
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V

    goto :goto_c

    :catch_c
    nop

    goto :goto_d

    :cond_f
    :goto_c
    if-eqz v11, :cond_10

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    :cond_10
    if-eqz v8, :cond_11

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_c

    :cond_11
    :goto_d
    if-eqz v10, :cond_12

    invoke-virtual {v10}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_12
    :goto_e
    iget-boolean v8, v1, Lio/openinstall/h/a/a;->c:Z

    if-eqz v8, :cond_13

    const/4 v8, 0x3

    if-ge v5, v8, :cond_13

    if-eqz v7, :cond_0

    :cond_13
    :goto_f
    return-object v4
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lio/openinstall/h/a/b;
    .locals 2

    new-instance v0, Lio/openinstall/h/a/b;

    invoke-direct {v0}, Lio/openinstall/h/a/b;-><init>()V

    :try_start_0
    invoke-static {p3}, Lio/openinstall/h/a/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p3

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, p3, v1}, Lio/openinstall/h/a/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)Lio/openinstall/h/a/b;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object p2, Lio/openinstall/h/a/b$a;->b:Lio/openinstall/h/a/b$a;

    invoke-virtual {v0, p2}, Lio/openinstall/h/a/b;->a(Lio/openinstall/h/a/b$a;)V

    const/4 p2, -0x1

    invoke-virtual {v0, p2}, Lio/openinstall/h/a/b;->a(I)V

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method
