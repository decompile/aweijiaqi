.class public Lio/openinstall/j/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/util/concurrent/ThreadPoolExecutor;

.field private b:Ljava/util/concurrent/Callable;

.field private c:Lio/openinstall/j/a;

.field private d:J

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ThreadPoolExecutor;Ljava/util/concurrent/Callable;Lio/openinstall/j/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0xa

    iput-wide v0, p0, Lio/openinstall/j/b;->d:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lio/openinstall/j/b;->e:Landroid/os/Handler;

    iput-object p1, p0, Lio/openinstall/j/b;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    iput-object p2, p0, Lio/openinstall/j/b;->b:Ljava/util/concurrent/Callable;

    iput-object p3, p0, Lio/openinstall/j/b;->c:Lio/openinstall/j/a;

    return-void
.end method

.method static synthetic a(Lio/openinstall/j/b;)Lio/openinstall/j/a;
    .locals 0

    iget-object p0, p0, Lio/openinstall/j/b;->c:Lio/openinstall/j/a;

    return-object p0
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lio/openinstall/j/b;->d:J

    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lio/openinstall/j/b;->a:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, Lio/openinstall/j/b;->b:Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    :try_start_0
    iget-wide v1, p0, Lio/openinstall/j/b;->d:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/openinstall/h/a/b;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    new-instance v0, Lio/openinstall/h/a/b;

    sget-object v2, Lio/openinstall/h/a/b$a;->c:Lio/openinstall/h/a/b$a;

    const/4 v3, -0x4

    invoke-direct {v0, v2, v3}, Lio/openinstall/h/a/b;-><init>(Lio/openinstall/h/a/b$a;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request timeout : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/openinstall/h/a/b;->b(Ljava/lang/String;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lio/openinstall/j/b;->e:Landroid/os/Handler;

    new-instance v2, Lio/openinstall/j/c;

    invoke-direct {v2, p0, v1}, Lio/openinstall/j/c;-><init>(Lio/openinstall/j/b;Lio/openinstall/h/a/b;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
