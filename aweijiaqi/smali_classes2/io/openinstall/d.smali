.class public Lio/openinstall/d;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lio/openinstall/d;


# instance fields
.field private final b:Lio/openinstall/f/a;

.field private final c:Lio/openinstall/e/d;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)V
    .locals 11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v7, Lio/openinstall/b;

    invoke-direct {v7}, Lio/openinstall/b;-><init>()V

    new-instance v8, Lio/openinstall/c/b;

    invoke-direct {v8}, Lio/openinstall/c/b;-><init>()V

    new-instance v0, Lio/openinstall/h;

    invoke-direct {v0}, Lio/openinstall/h;-><init>()V

    const-string v1, "FM_config"

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lio/openinstall/h;->a(Landroid/content/Context;Ljava/lang/String;Lio/openinstall/h$b;)Ljava/util/concurrent/Future;

    move-result-object v0

    new-instance v9, Lio/openinstall/f;

    invoke-direct {v9, v0}, Lio/openinstall/f;-><init>(Ljava/util/concurrent/Future;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CoreHandler-Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v10, Lio/openinstall/f/a;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    move-object v0, v10

    move-object v1, p1

    move-object v3, v7

    move-object v4, v9

    move-object v5, v8

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lio/openinstall/f/a;-><init>(Landroid/content/Context;Landroid/os/Looper;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V

    iput-object v10, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    new-instance v6, Lio/openinstall/e/d;

    move-object v0, v6

    move-object v2, v7

    move-object v3, v9

    move-object v4, v8

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lio/openinstall/e/d;-><init>(Landroid/content/Context;Lio/openinstall/b;Lio/openinstall/f;Lio/openinstall/c/b;Lcom/fm/openinstall/Configuration;)V

    iput-object v6, p0, Lio/openinstall/d;->c:Lio/openinstall/e/d;

    invoke-direct {p0, p1}, Lio/openinstall/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)Lio/openinstall/d;
    .locals 2

    sget-object v0, Lio/openinstall/d;->a:Lio/openinstall/d;

    if-nez v0, :cond_1

    const-class v0, Lio/openinstall/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lio/openinstall/d;->a:Lio/openinstall/d;

    if-nez v1, :cond_0

    new-instance v1, Lio/openinstall/d;

    invoke-direct {v1, p0, p1}, Lio/openinstall/d;-><init>(Landroid/content/Context;Lcom/fm/openinstall/Configuration;)V

    sput-object v1, Lio/openinstall/d;->a:Lio/openinstall/d;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lio/openinstall/d;->a:Lio/openinstall/d;

    return-object p0
.end method

.method static synthetic a(Lio/openinstall/d;)Lio/openinstall/f/a;
    .locals 0

    iget-object p0, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    new-instance v0, Lio/openinstall/e;

    invoke-direct {v0, p0, p1}, Lio/openinstall/e;-><init>(Lio/openinstall/d;Landroid/app/Application;)V

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method

.method private a(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V
    .locals 2

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "decodeWakeUp"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    invoke-virtual {v0, p1, p2}, Lio/openinstall/f/a;->a(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "reportRegister"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lio/openinstall/d;->c:Lio/openinstall/e/d;

    invoke-virtual {v0}, Lio/openinstall/e/d;->a()V

    return-void
.end method

.method public a(JLcom/fm/openinstall/listener/AppInstallListener;)V
    .locals 2

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "getInstallData"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    invoke-virtual {v0, p1, p2, p3}, Lio/openinstall/f/a;->a(JLcom/fm/openinstall/listener/AppInstallListener;)V

    return-void
.end method

.method public a(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)V
    .locals 0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lio/openinstall/d;->a(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    return-void
.end method

.method public a(Lcom/fm/openinstall/listener/AppWakeUpListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/openinstall/d;->a(Landroid/net/Uri;Lcom/fm/openinstall/listener/AppWakeUpListener;)V

    return-void
.end method

.method public a(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V
    .locals 2

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "getOriginalApk"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    invoke-virtual {v0, p1}, Lio/openinstall/f/a;->a(Lcom/fm/openinstall/listener/GetUpdateApkListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    invoke-virtual {v0, p1}, Lio/openinstall/f/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lio/openinstall/d;->c:Lio/openinstall/e/d;

    invoke-virtual {v0, p1}, Lio/openinstall/e/d;->a(Ljava/lang/String;)V

    iget-object p1, p0, Lio/openinstall/d;->b:Lio/openinstall/f/a;

    invoke-virtual {p1}, Lio/openinstall/f/a;->d()V

    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 2

    sget-boolean v0, Lio/openinstall/k/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "reportEffectPoint"

    invoke-static {v1, v0}, Lio/openinstall/k/c;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lio/openinstall/d;->c:Lio/openinstall/e/d;

    invoke-virtual {v0, p1, p2, p3}, Lio/openinstall/e/d;->a(Ljava/lang/String;J)V

    return-void
.end method
