.class public Lio/openinstall/b/a;
.super Ljava/lang/Object;


# static fields
.field private static volatile e:Lio/openinstall/b/a;


# instance fields
.field private a:Lio/openinstall/k/d;

.field private b:Landroid/content/ClipboardManager;

.field private c:Ljava/util/concurrent/CountDownLatch;

.field private d:Landroid/os/Handler;

.field private f:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "ClipDataUtil"

    invoke-static {v0}, Lio/openinstall/k/d;->a(Ljava/lang/String;)Lio/openinstall/k/d;

    move-result-object v0

    iput-object v0, p0, Lio/openinstall/b/a;->a:Lio/openinstall/k/d;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lio/openinstall/b/a;->c:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lio/openinstall/b/b;

    invoke-direct {v0, p0}, Lio/openinstall/b/b;-><init>(Lio/openinstall/b/a;)V

    iput-object v0, p0, Lio/openinstall/b/a;->f:Ljava/lang/Runnable;

    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/ClipboardManager;

    iput-object p1, p0, Lio/openinstall/b/a;->b:Landroid/content/ClipboardManager;

    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "ClipData-Thread"

    invoke-direct {p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lio/openinstall/b/a;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lio/openinstall/b/a;)Landroid/content/ClipData;
    .locals 0

    invoke-direct {p0}, Lio/openinstall/b/a;->d()Landroid/content/ClipData;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;)Lio/openinstall/b/a;
    .locals 2

    sget-object v0, Lio/openinstall/b/a;->e:Lio/openinstall/b/a;

    if-nez v0, :cond_1

    const-class v0, Lio/openinstall/b/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lio/openinstall/b/a;->e:Lio/openinstall/b/a;

    if-nez v1, :cond_0

    new-instance v1, Lio/openinstall/b/a;

    invoke-direct {v1, p0}, Lio/openinstall/b/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Lio/openinstall/b/a;->e:Lio/openinstall/b/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lio/openinstall/b/a;->e:Lio/openinstall/b/a;

    return-object p0
.end method

.method private a(Landroid/content/ClipData;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lio/openinstall/b/a;->b:Landroid/content/ClipboardManager;

    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic a(Lio/openinstall/b/a;Landroid/content/ClipData;)V
    .locals 0

    invoke-direct {p0, p1}, Lio/openinstall/b/a;->a(Landroid/content/ClipData;)V

    return-void
.end method

.method private d()Landroid/content/ClipData;
    .locals 4

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lio/openinstall/b/a;->c:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0x3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    :cond_0
    iget-object v0, p0, Lio/openinstall/b/a;->b:Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/openinstall/b/c;
    .locals 2

    new-instance v0, Lio/openinstall/b/c;

    invoke-direct {v0}, Lio/openinstall/b/c;-><init>()V

    if-eqz p1, :cond_0

    sget-object v1, Lio/openinstall/k/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Lio/openinstall/b/c;->b(Ljava/lang/String;)V

    const/4 p1, 0x2

    invoke-virtual {v0, p1}, Lio/openinstall/b/c;->b(I)V

    :cond_0
    if-eqz p2, :cond_1

    const/16 p1, 0x8

    invoke-static {p2, p1}, Lio/openinstall/k/a;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lio/openinstall/k/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0, p2}, Lio/openinstall/b/c;->a(Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lio/openinstall/b/c;->b(I)V

    :cond_1
    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lio/openinstall/b/a;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public b()Lio/openinstall/b/c;
    .locals 4

    new-instance v0, Lio/openinstall/b/c;

    invoke-direct {v0}, Lio/openinstall/b/c;-><init>()V

    invoke-direct {p0}, Lio/openinstall/b/a;->d()Landroid/content/ClipData;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    if-eqz v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    const/4 v3, 0x0

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {p0, v0, v3}, Lio/openinstall/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Lio/openinstall/b/c;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public c()V
    .locals 4

    iget-object v0, p0, Lio/openinstall/b/a;->d:Landroid/os/Handler;

    iget-object v1, p0, Lio/openinstall/b/a;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
