.class public Lio/openinstall/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lio/openinstall/c;

.field public static final b:Lio/openinstall/c;

.field public static final c:Lio/openinstall/c;

.field public static final d:Lio/openinstall/c;

.field public static final e:Lio/openinstall/c;


# instance fields
.field private f:I

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lio/openinstall/c;

    const/4 v1, 0x1

    const-string v2, "\u672a\u521d\u59cb\u5316"

    invoke-direct {v0, v1, v2}, Lio/openinstall/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Lio/openinstall/c;->a:Lio/openinstall/c;

    new-instance v0, Lio/openinstall/c;

    const/4 v1, 0x2

    const-string v2, "\u6b63\u5728\u521d\u59cb\u5316"

    invoke-direct {v0, v1, v2}, Lio/openinstall/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Lio/openinstall/c;->b:Lio/openinstall/c;

    new-instance v0, Lio/openinstall/c;

    const/4 v1, 0x0

    const-string v2, "\u521d\u59cb\u5316\u6210\u529f"

    invoke-direct {v0, v1, v2}, Lio/openinstall/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Lio/openinstall/c;->c:Lio/openinstall/c;

    new-instance v0, Lio/openinstall/c;

    const/4 v1, -0x1

    const-string v2, "\u521d\u59cb\u5316\u5931\u8d25"

    invoke-direct {v0, v1, v2}, Lio/openinstall/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Lio/openinstall/c;->d:Lio/openinstall/c;

    new-instance v0, Lio/openinstall/c;

    const/4 v1, -0x2

    const-string v2, "\u521d\u59cb\u5316\u9519\u8bef"

    invoke-direct {v0, v1, v2}, Lio/openinstall/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Lio/openinstall/c;->e:Lio/openinstall/c;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lio/openinstall/c;->f:I

    iput-object p2, p0, Lio/openinstall/c;->g:Ljava/lang/String;

    return-void
.end method

.method public static a(I)Lio/openinstall/c;
    .locals 1

    const/4 v0, -0x2

    if-eq p0, v0, :cond_4

    const/4 v0, -0x1

    if-eq p0, v0, :cond_3

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p0, Lio/openinstall/c;->b:Lio/openinstall/c;

    goto :goto_1

    :cond_1
    :goto_0
    sget-object p0, Lio/openinstall/c;->a:Lio/openinstall/c;

    goto :goto_1

    :cond_2
    sget-object p0, Lio/openinstall/c;->c:Lio/openinstall/c;

    goto :goto_1

    :cond_3
    sget-object p0, Lio/openinstall/c;->d:Lio/openinstall/c;

    goto :goto_1

    :cond_4
    sget-object p0, Lio/openinstall/c;->e:Lio/openinstall/c;

    :goto_1
    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lio/openinstall/c;->f:I

    return v0
.end method
