.class public Lio/openinstall/i/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private final a:Lio/openinstall/i/f;

.field private final b:Lio/openinstall/i/e;


# direct methods
.method public constructor <init>(Lio/openinstall/i/e;Lio/openinstall/i/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lio/openinstall/i/a;->a:Lio/openinstall/i/f;

    iput-object p1, p0, Lio/openinstall/i/a;->b:Lio/openinstall/i/e;

    return-void
.end method

.method public constructor <init>(Lio/openinstall/i/f;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lio/openinstall/i/a;-><init>(Lio/openinstall/i/e;Lio/openinstall/i/f;)V

    return-void
.end method


# virtual methods
.method public a()Lio/openinstall/i/f;
    .locals 1

    iget-object v0, p0, Lio/openinstall/i/a;->a:Lio/openinstall/i/f;

    return-object v0
.end method

.method public a([B)V
    .locals 1

    iget-object v0, p0, Lio/openinstall/i/a;->b:Lio/openinstall/i/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lio/openinstall/i/e;->a([B)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lio/openinstall/i/a;->a:Lio/openinstall/i/f;

    invoke-virtual {v0, p1}, Lio/openinstall/i/f;->a([B)V

    :goto_0
    return-void
.end method

.method public b()Lio/openinstall/i/e;
    .locals 1

    iget-object v0, p0, Lio/openinstall/i/a;->b:Lio/openinstall/i/e;

    return-object v0
.end method

.method public c()[B
    .locals 1

    iget-object v0, p0, Lio/openinstall/i/a;->b:Lio/openinstall/i/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/openinstall/i/e;->d()[B

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lio/openinstall/i/a;->a:Lio/openinstall/i/f;

    iget-object v0, v0, Lio/openinstall/i/f;->g:[B

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lio/openinstall/i/a;->d()Lio/openinstall/i/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/openinstall/i/a;
    .locals 3

    new-instance v0, Lio/openinstall/i/a;

    iget-object v1, p0, Lio/openinstall/i/a;->b:Lio/openinstall/i/e;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lio/openinstall/i/e;->f()Lio/openinstall/i/e;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lio/openinstall/i/a;->a:Lio/openinstall/i/f;

    invoke-virtual {v2}, Lio/openinstall/i/f;->b()Lio/openinstall/i/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/openinstall/i/a;-><init>(Lio/openinstall/i/e;Lio/openinstall/i/f;)V

    return-object v0
.end method
