.class public final Lcom/yxcorp/kuaishou/addfp/a/b/g/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/yxcorp/kuaishou/addfp/a/b/a;
.implements Lcom/yxcorp/kuaishou/addfp/a/b/b;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yxcorp/kuaishou/addfp/a/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yxcorp/kuaishou/addfp/a/a/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->b:Lcom/yxcorp/kuaishou/addfp/a/a/a;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IInterface;)V
    .locals 1

    iget-object p1, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->b:Lcom/yxcorp/kuaishou/addfp/a/a/a;

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->b()Z

    move-result v0

    invoke-interface {p1, v0, p0}, Lcom/yxcorp/kuaishou/addfp/a/a/a;->a(ZLcom/yxcorp/kuaishou/addfp/a/b/a;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/yxcorp/kuaishou/addfp/a/a/a;)V
    .locals 1

    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object p1

    iget-object v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->a:Landroid/content/Context;

    invoke-virtual {p1, v0, p0}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->a(Landroid/content/Context;Lcom/yxcorp/kuaishou/addfp/a/b/b;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->b()Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    return-object v0
.end method

.method public final c()Z
    .locals 1

    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->b()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    invoke-static {}, Lcom/yxcorp/kuaishou/addfp/a/b/g/f;->a()Lcom/yxcorp/kuaishou/addfp/a/b/g/d;

    move-result-object v0

    iget-object v1, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yxcorp/kuaishou/addfp/a/b/g/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->b:Lcom/yxcorp/kuaishou/addfp/a/a/a;

    if-eqz v0, :cond_0

    const-string v0, "failed here"

    invoke-static {v0}, Lcom/yxcorp/kuaishou/addfp/android/b/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/g/g;->b:Lcom/yxcorp/kuaishou/addfp/a/a/a;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, Lcom/yxcorp/kuaishou/addfp/a/a/a;->a(ZLcom/yxcorp/kuaishou/addfp/a/b/a;)V

    :cond_0
    return-void
.end method
