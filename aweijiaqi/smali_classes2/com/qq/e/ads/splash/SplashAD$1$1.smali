.class Lcom/qq/e/ads/splash/SplashAD$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qq/e/ads/splash/SplashAD$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/comm/pi/POFactory;

.field private synthetic b:Lcom/qq/e/ads/splash/SplashAD$1;


# direct methods
.method constructor <init>(Lcom/qq/e/ads/splash/SplashAD$1;Lcom/qq/e/comm/pi/POFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iput-object p2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    const v1, 0x30da7

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    iget-object v3, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v3, v3, Lcom/qq/e/ads/splash/SplashAD$1;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v4, v4, Lcom/qq/e/ads/splash/SplashAD$1;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v5, v5, Lcom/qq/e/ads/splash/SplashAD$1;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/qq/e/comm/pi/POFactory;->getNativeSplashAdView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Lcom/qq/e/comm/pi/NSPVI;)Lcom/qq/e/comm/pi/NSPVI;

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->b(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/constants/LoadAdParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v1}, Lcom/qq/e/ads/splash/SplashAD;->b(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/constants/LoadAdParams;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setLoadAdParams(Lcom/qq/e/comm/constants/LoadAdParams;)V

    :cond_0
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->c(Lcom/qq/e/ads/splash/SplashAD;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v1}, Lcom/qq/e/ads/splash/SplashAD;->c(Lcom/qq/e/ads/splash/SplashAD;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setDeveloperLogo(I)V

    :cond_1
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->d(Lcom/qq/e/ads/splash/SplashAD;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v1}, Lcom/qq/e/ads/splash/SplashAD;->d(Lcom/qq/e/ads/splash/SplashAD;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setDeveloperLogo([B)V

    :cond_2
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->d:Ljava/util/Map;

    iget-object v2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v2, v2, Lcom/qq/e/ads/splash/SplashAD$1;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Ljava/util/Map;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->e:I

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setFetchDelay(I)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    new-instance v1, Lcom/qq/e/ads/splash/SplashAD$ADListenerAdapter;

    iget-object v2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v2, v2, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/qq/e/ads/splash/SplashAD$ADListenerAdapter;-><init>(Lcom/qq/e/ads/splash/SplashAD;B)V

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setAdListener(Lcom/qq/e/comm/adevent/ADListener;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->f:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setSkipView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->g:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setFloatView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->h:Lcom/qq/e/ads/splash/SplashADListener;

    instance-of v0, v0, Lcom/qq/e/ads/splash/SplashADZoomOutListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->h:Lcom/qq/e/ads/splash/SplashADListener;

    check-cast v0, Lcom/qq/e/ads/splash/SplashADZoomOutListener;

    invoke-interface {v0}, Lcom/qq/e/ads/splash/SplashADZoomOutListener;->isSupportZoomOut()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/qq/e/comm/pi/NSPVI;->setSupportZoomOut(Z)V

    :cond_3
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->e(Lcom/qq/e/ads/splash/SplashAD;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->f(Lcom/qq/e/ads/splash/SplashAD;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v1}, Lcom/qq/e/ads/splash/SplashAD;->e(Lcom/qq/e/ads/splash/SplashAD;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/splash/SplashAD;->fetchFullScreenAndShowIn(Landroid/view/ViewGroup;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v1}, Lcom/qq/e/ads/splash/SplashAD;->e(Lcom/qq/e/ads/splash/SplashAD;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/splash/SplashAD;->fetchAndShowIn(Landroid/view/ViewGroup;)V

    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->g(Lcom/qq/e/ads/splash/SplashAD;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/NSPVI;->preload()V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0, v3}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Z)Z

    :cond_6
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->h(Lcom/qq/e/ads/splash/SplashAD;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->f(Lcom/qq/e/ads/splash/SplashAD;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/NSPVI;->fetchFullScreenAdOnly()V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object v0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/NSPVI;->fetchAdOnly()V

    :goto_1
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    invoke-static {v0, v3}, Lcom/qq/e/ads/splash/SplashAD;->b(Lcom/qq/e/ads/splash/SplashAD;Z)Z

    goto :goto_2

    :cond_8
    const-string v0, "SplashAdView created by factory return null"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v2, v2, Lcom/qq/e/ads/splash/SplashAD$1;->h:Lcom/qq/e/ads/splash/SplashADListener;

    invoke-static {v0, v2, v1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Lcom/qq/e/ads/splash/SplashADListener;I)V

    goto :goto_2

    :cond_9
    const-string v0, "factory return null"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v2, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v2, v2, Lcom/qq/e/ads/splash/SplashAD$1;->h:Lcom/qq/e/ads/splash/SplashADListener;

    invoke-static {v0, v2, v1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Lcom/qq/e/ads/splash/SplashADListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_a
    return-void

    :catchall_0
    move-exception v0

    const-string v1, "Unknown Exception"

    invoke-static {v1, v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v0, v0, Lcom/qq/e/ads/splash/SplashAD$1;->i:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v1, p0, Lcom/qq/e/ads/splash/SplashAD$1$1;->b:Lcom/qq/e/ads/splash/SplashAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/splash/SplashAD$1;->h:Lcom/qq/e/ads/splash/SplashADListener;

    const/16 v2, 0x1770

    invoke-static {v0, v1, v2}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashAD;Lcom/qq/e/ads/splash/SplashADListener;I)V

    :goto_2
    return-void
.end method
