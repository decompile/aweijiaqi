.class Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/adevent/ADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdListenerAdapter"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;


# direct methods
.method constructor <init>(Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    return-void
.end method


# virtual methods
.method public onADEvent(Lcom/qq/e/comm/adevent/ADEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onVideoComplete()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/qq/e/comm/util/AdErrorConvertor;->formatErrorCode(I)Lcom/qq/e/comm/util/AdError;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onError(Lcom/qq/e/comm/util/AdError;)V

    return-void

    :pswitch_3
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onClose()V

    return-void

    :pswitch_4
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onClick()V

    return-void

    :pswitch_5
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onExpose()V

    return-void

    :pswitch_6
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onShow()V

    return-void

    :pswitch_7
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onVideoCached()V

    return-void

    :pswitch_8
    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;->a:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-interface {p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onAdLoaded()V

    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
