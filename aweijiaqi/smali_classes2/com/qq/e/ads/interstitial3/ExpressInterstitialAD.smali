.class public Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;
.super Lcom/qq/e/ads/LiteAbstractAD;

# interfaces
.implements Lcom/qq/e/comm/compliance/ApkDownloadComplianceInterface;
.implements Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/qq/e/ads/LiteAbstractAD<",
        "Lcom/qq/e/comm/pi/UIADI2;",
        ">;",
        "Lcom/qq/e/comm/compliance/ApkDownloadComplianceInterface;",
        "Lcom/qq/e/comm/compliance/DownloadConfirmListener;"
    }
.end annotation


# instance fields
.field private a:Lcom/qq/e/comm/pi/UIADI2;

.field private b:Z

.field private c:Z

.field private d:Lcom/qq/e/ads/nativ/express2/VideoOption2;

.field private e:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

.field private f:Lcom/qq/e/comm/compliance/DownloadConfirmListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/qq/e/ads/LiteAbstractAD;-><init>()V

    iput-object p3, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->e:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object p3

    invoke-virtual {p3}, Lcom/qq/e/comm/managers/GDTADManager;->isInitialized()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object p3

    invoke-virtual {p3}, Lcom/qq/e/comm/managers/GDTADManager;->getAppStatus()Lcom/qq/e/comm/managers/status/APPStatus;

    move-result-object p3

    invoke-virtual {p3}, Lcom/qq/e/comm/managers/status/APPStatus;->getAPPID()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p3, p2}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string p1, "SDK \u5c1a\u672a\u521d\u59cb\u5316\uff0c\u8bf7\u5728 Application \u4e2d\u8c03\u7528 GDTADManager.getInstance().initWith() \u521d\u59cb\u5316"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/16 p1, 0x7d3

    invoke-virtual {p0, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/qq/e/ads/LiteAbstractAD;-><init>()V

    iput-object p4, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->e:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-virtual {p0, p1, p2, p3}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;Lcom/qq/e/comm/pi/POFactory;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    new-instance v0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;

    iget-object v1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->e:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    invoke-direct {v0, v1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD$AdListenerAdapter;-><init>(Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;)V

    invoke-interface {p2, p1, p3, p4, v0}, Lcom/qq/e/comm/pi/POFactory;->getExpressInterstitialADDelegate(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/comm/adevent/ADListener;)Lcom/qq/e/comm/pi/UIADI2;

    move-result-object p1

    return-object p1
.end method

.method protected final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->e:Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/qq/e/comm/util/AdErrorConvertor;->formatErrorCode(I)Lcom/qq/e/comm/util/AdError;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAdListener;->onError(Lcom/qq/e/comm/util/AdError;)V

    :cond_0
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/qq/e/comm/pi/UIADI2;

    iput-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->d:Lcom/qq/e/ads/nativ/express2/VideoOption2;

    invoke-interface {p1, v0}, Lcom/qq/e/comm/pi/UIADI2;->setVideoOption(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V

    iget-boolean p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->b:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/UIADI2;->loadHalfScreenAD()V

    iput-boolean v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->b:Z

    :cond_0
    iget-boolean p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->c:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/UIADI2;->loadFullScreenAD()V

    iput-boolean v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->c:Z

    :cond_1
    return-void
.end method

.method public checkValidity()Lcom/qq/e/comm/util/VideoAdValidity;
    .locals 5

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->hasShown()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/qq/e/comm/util/VideoAdValidity;->SHOWED:Lcom/qq/e/comm/util/VideoAdValidity;

    return-object v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->getExpireTimestamp()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    sget-object v0, Lcom/qq/e/comm/util/VideoAdValidity;->OVERDUE:Lcom/qq/e/comm/util/VideoAdValidity;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->isVideoAd()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->isVideoCached()Z

    move-result v0

    goto :goto_0

    :cond_2
    const-string v0, "don\'t call isVideoCached before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    sget-object v0, Lcom/qq/e/comm/util/VideoAdValidity;->NONE_CACHE:Lcom/qq/e/comm/util/VideoAdValidity;

    return-object v0

    :cond_3
    sget-object v0, Lcom/qq/e/comm/util/VideoAdValidity;->VALID:Lcom/qq/e/comm/util/VideoAdValidity;

    return-object v0
.end method

.method public closeHalfScreenAD()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->closeHalfScreenAD()V

    return-void

    :cond_0
    const-string v0, "don\'t call closeHalfScreenAD before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    return-void
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->destroy()V

    :cond_0
    return-void
.end method

.method public getApkInfoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->getApkInfoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getECPMLevel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->getECPMLevel()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "don\'t call getECPMLevel before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpireTimestamp()J
    .locals 2

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->getExpireTimestamp()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-string v0, "don\'t call getExpireTimestamp before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getVideoDuration()I
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->getVideoDuration()I

    move-result v0

    return v0

    :cond_0
    const-string v0, "don\'t call getVideoDuration before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public hasShown()Z
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->hasShown()Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "don\'t call hasShown before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public isVideoAd()Z
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->isVideoAd()Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "don\'t call isVideoAd before loading AD success"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public loadFullScreenAD()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->loadFullScreenAD()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->c:Z

    return-void
.end method

.method public loadHalfScreenAD()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UIADI2;->loadHalfScreenAD()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->b:Z

    return-void
.end method

.method public onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->f:Lcom/qq/e/comm/compliance/DownloadConfirmListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/qq/e/comm/compliance/DownloadConfirmListener;->onDownloadConfirm(Landroid/app/Activity;ILjava/lang/String;Lcom/qq/e/comm/compliance/DownloadConfirmCallBack;)V

    :cond_0
    return-void
.end method

.method public setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->f:Lcom/qq/e/comm/compliance/DownloadConfirmListener;

    iget-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/qq/e/comm/pi/UIADI2;->setDownloadConfirmListener(Lcom/qq/e/comm/compliance/DownloadConfirmListener;)V

    :cond_0
    return-void
.end method

.method public setVideoOption(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UIADI2;->setVideoOption(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V

    return-void

    :cond_0
    iput-object p1, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->d:Lcom/qq/e/ads/nativ/express2/VideoOption2;

    return-void
.end method

.method public showFullScreenAD(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UIADI2;->showFullScreenAD(Landroid/app/Activity;)V

    return-void

    :cond_0
    const-string p1, "don\'t call showAD before loading AD success"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    return-void
.end method

.method public showHalfScreenAD(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/interstitial3/ExpressInterstitialAD;->a:Lcom/qq/e/comm/pi/UIADI2;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UIADI2;->showHalfScreenAD(Landroid/app/Activity;)V

    return-void

    :cond_0
    const-string p1, "don\'t call showHalfScreenAD before loading AD success"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->w(Ljava/lang/String;)V

    return-void
.end method
