.class final Lcom/qq/e/comm/services/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/net/NetworkCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qq/e/comm/services/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/comm/managers/plugin/PM;

.field private synthetic b:J

.field private synthetic c:Lcom/qq/e/comm/services/a;


# direct methods
.method constructor <init>(Lcom/qq/e/comm/services/a;Lcom/qq/e/comm/managers/plugin/PM;J)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/comm/services/a$1;->c:Lcom/qq/e/comm/services/a;

    iput-object p2, p0, Lcom/qq/e/comm/services/a$1;->a:Lcom/qq/e/comm/managers/plugin/PM;

    iput-wide p3, p0, Lcom/qq/e/comm/services/a$1;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onException(Ljava/lang/Exception;)V
    .locals 10

    const-string v0, "ActivateError"

    invoke-static {v0, p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object p1

    new-instance v9, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const-string v1, "sdk.e.qq.com"

    const-string v2, "launch"

    const-string v3, ""

    const/4 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    invoke-virtual {p1, v9}, Lcom/qq/e/comm/services/RetCodeService;->send(Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;)V

    return-void
.end method

.method public final onResponse(Lcom/qq/e/comm/net/rr/Request;Lcom/qq/e/comm/net/rr/Response;)V
    .locals 11

    const-string p1, "url"

    const-string v0, "jar"

    const-string v1, "sig"

    const-string v2, "ret"

    :try_start_0
    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_3

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStringContent()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ACTIVERESPONSE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/qq/e/comm/util/GDTLogger;->d(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string p1, "SDK Server response empty string,maybe zip or tea format error"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->report(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object p1

    new-instance v9, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v1, "sdk.e.qq.com"

    const-string v2, "launch"

    const-string v3, ""

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    :goto_0
    invoke-virtual {p1, v9}, Lcom/qq/e/comm/services/RetCodeService;->send(Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;)V

    return-void

    :cond_0
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v5, -0x1

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    :cond_1
    if-eqz v5, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "Response Error,retCode="

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    iget-object v2, p0, Lcom/qq/e/comm/services/a$1;->a:Lcom/qq/e/comm/managers/plugin/PM;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_4

    :try_start_2
    iget-object v2, p0, Lcom/qq/e/comm/services/a$1;->a:Lcom/qq/e/comm/managers/plugin/PM;

    invoke-virtual {v2}, Lcom/qq/e/comm/managers/plugin/PM;->getPOFactory()Lcom/qq/e/comm/pi/POFactory;

    move-result-object v2

    const/4 v5, 0x1

    invoke-interface {v2, v5, v3}, Lcom/qq/e/comm/pi/POFactory;->config(ILjava/lang/String;)V
    :try_end_2
    .catch Lcom/qq/e/comm/managers/plugin/c; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v2

    :try_start_3
    invoke-virtual {v2}, Lcom/qq/e/comm/managers/plugin/c;->printStackTrace()V

    :goto_2
    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/qq/e/comm/services/a$1;->a:Lcom/qq/e/comm/managers/plugin/PM;

    invoke-virtual {v1, v0, p1}, Lcom/qq/e/comm/managers/plugin/PM;->update(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v0, "SDK server response code error while launch or activate,code:"

    invoke-direct {p1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_4
    :goto_3
    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object p1

    new-instance v9, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v1, "sdk.e.qq.com"

    const-string v2, "launch"

    const-string v3, ""

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    goto/16 :goto_0

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p1

    :try_start_4
    const-string v0, "Parse Active or launch response exception"

    invoke-static {v0, p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object p1

    new-instance v9, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v1, "sdk.e.qq.com"

    const-string v2, "launch"

    const-string v3, ""

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    goto/16 :goto_0

    :catch_2
    move-exception p1

    :try_start_5
    const-string v0, "ActivateError"

    invoke-static {v0, p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object p1

    new-instance v9, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v1, "sdk.e.qq.com"

    const-string v2, "launch"

    const-string v3, ""

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    goto/16 :goto_0

    :goto_4
    invoke-static {}, Lcom/qq/e/comm/services/RetCodeService;->getInstance()Lcom/qq/e/comm/services/RetCodeService;

    move-result-object v0

    new-instance v10, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;

    invoke-interface {p2}, Lcom/qq/e/comm/net/rr/Response;->getStatusCode()I

    move-result v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/qq/e/comm/services/a$1;->b:J

    sub-long/2addr v1, v3

    long-to-int v6, v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v2, "sdk.e.qq.com"

    const-string v3, "launch"

    const-string v4, ""

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    invoke-virtual {v0, v10}, Lcom/qq/e/comm/services/RetCodeService;->send(Lcom/qq/e/comm/services/RetCodeService$RetCodeInfo;)V

    throw p1
.end method
