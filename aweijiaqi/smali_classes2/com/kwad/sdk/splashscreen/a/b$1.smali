.class Lcom/kwad/sdk/splashscreen/a/b$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kwad/sdk/splashscreen/a/b;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/kwad/sdk/splashscreen/a/b;


# direct methods
.method constructor <init>(Lcom/kwad/sdk/splashscreen/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/d;->a:Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-boolean p1, p1, Lcom/kwad/sdk/splashscreen/d;->b:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    invoke-virtual {p1}, Lcom/kwad/sdk/splashscreen/d;->d()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/d;->a:Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;

    invoke-interface {v1}, Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;->onSkippedAd()V

    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iput-boolean v0, v1, Lcom/kwad/sdk/splashscreen/d;->b:Z

    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/d;->d:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iput-object p1, v1, Lcom/kwad/sdk/core/response/model/AdTemplate;->mMiniWindowId:Ljava/lang/String;

    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/d;->f:Lcom/kwad/sdk/splashscreen/b/a;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/d;->f:Lcom/kwad/sdk/splashscreen/b/a;

    invoke-virtual {v1}, Lcom/kwad/sdk/splashscreen/b/a;->d()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    long-to-int v1, v1

    :cond_1
    iget-object v2, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/d;->d:Lcom/kwad/sdk/core/response/model/AdTemplate;

    const/16 v3, 0x72

    invoke-static {}, Lcom/kwad/sdk/core/config/c;->ai()I

    move-result v4

    invoke-static {v2, v3, v4, v1}, Lcom/kwad/sdk/core/report/a;->a(Lcom/kwad/sdk/core/response/model/AdTemplate;III)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    invoke-static {p1}, Lcom/kwad/sdk/splashscreen/a/b;->a(Lcom/kwad/sdk/splashscreen/a/b;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/d;->a:Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object p1, p1, Lcom/kwad/sdk/splashscreen/d;->a:Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;->onSkippedAd()V

    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/d;->f:Lcom/kwad/sdk/splashscreen/b/a;

    if-eqz v1, :cond_3

    :try_start_0
    const-string v1, "duration"

    iget-object v2, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v2, v2, Lcom/kwad/sdk/splashscreen/d;->f:Lcom/kwad/sdk/splashscreen/b/a;

    invoke-virtual {v2}, Lcom/kwad/sdk/splashscreen/b/a;->d()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/kwad/sdk/splashscreen/a/b$1;->a:Lcom/kwad/sdk/splashscreen/a/b;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/a/b;->a:Lcom/kwad/sdk/splashscreen/d;

    iget-object v1, v1, Lcom/kwad/sdk/splashscreen/d;->d:Lcom/kwad/sdk/core/response/model/AdTemplate;

    invoke-static {v1, v0, p1}, Lcom/kwad/sdk/core/report/a;->a(Lcom/kwad/sdk/core/response/model/AdTemplate;ILorg/json/JSONObject;)V

    :cond_4
    return-void
.end method
