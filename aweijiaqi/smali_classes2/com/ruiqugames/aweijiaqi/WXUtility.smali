.class public Lcom/ruiqugames/aweijiaqi/WXUtility;
.super Ljava/lang/Object;
.source "WXUtility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;,
        Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKVComparator;
    }
.end annotation


# static fields
.field private static _context:Landroid/content/Context; = null

.field private static _isUseServerSign:Z = true

.field private static _mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

.field public static api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

.field public static payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static PayInit(Landroid/content/Context;)V
    .locals 2

    const-string v0, "kill_virus"

    const-string v1, "wxPayInit"

    .line 97
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_context:Landroid/content/Context;

    .line 99
    move-object v0, p0

    check-cast v0, Lcom/ruiqugames/aweijiaqi/MainActivity;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    .line 100
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-eqz v0, :cond_0

    .line 102
    invoke-interface {v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->unregisterApp()V

    .line 104
    :cond_0
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v0, v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/tencent/mm/opensdk/openapi/WXAPIFactory;->createWXAPI(Landroid/content/Context;Ljava/lang/String;Z)Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    move-result-object p0

    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    .line 105
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v0, v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    invoke-interface {p0, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->registerApp(Ljava/lang/String;)Z

    .line 106
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->InitHandle()V

    return-void
.end method

.method private static getHashString(Ljava/security/MessageDigest;)Ljava/lang/String;
    .locals 5

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    invoke-virtual {p0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p0

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    .line 322
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 316
    :cond_0
    aget-byte v3, p0, v2

    shr-int/lit8 v4, v3, 0x4

    and-int/lit8 v4, v4, 0xf

    .line 318
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    and-int/lit8 v3, v3, 0xf

    .line 319
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static getMD5(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "MD5"

    .line 301
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 302
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 303
    invoke-static {v0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->getHashString(Ljava/security/MessageDigest;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 307
    invoke-virtual {p0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method private static getRandomStringByLength(I)Ljava/lang/String;
    .locals 5

    .line 246
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 247
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-lt v2, p0, :cond_0

    .line 252
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/16 v3, 0x24

    .line 249
    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    const-string v4, "abcdefghijklmnopqrstuvwxyz0123456789"

    .line 250
    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getSecondTimestamp()Ljava/lang/String;
    .locals 4

    .line 264
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 265
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    const/4 v3, 0x0

    sub-int/2addr v1, v2

    .line 268
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public static getSign(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, ""

    const/4 v1, 0x0

    move-object v2, v0

    .line 283
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 287
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "key=zlcU3oyPx0TsIbWIV1Xxvmy4l8ylZsfs"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 289
    invoke-static {p0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->getMD5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    goto :goto_1

    :cond_0
    move-object v0, p0

    :goto_1
    return-object v0

    .line 285
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    iget-object v2, v2, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;->key:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    iget-object v2, v2, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;->value:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static onLoginWX(ILjava/lang/String;)V
    .locals 2

    .line 202
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "errCode"

    .line 205
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "wxCode"

    .line 206
    invoke-virtual {v0, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :catch_0
    sget-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "onWXLogin"

    invoke-virtual {p0, v0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static onShareWX(ILjava/lang/String;)V
    .locals 3

    const-string v0, "kill_virus"

    .line 221
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "errCode"

    .line 224
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "wxCode"

    .line 225
    invoke-virtual {v1, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 229
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    const-string p0, "resp ___onWXShareError"

    .line 230
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "resp .... = "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    sget-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "onWXShare"

    invoke-virtual {p0, v0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static weChatPay(Ljava/lang/String;)V
    .locals 8

    const-string v0, "kill_virus"

    .line 141
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 142
    invoke-virtual {v1, p0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 147
    sget-boolean v2, Lcom/ruiqugames/aweijiaqi/WXUtility;->_isUseServerSign:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "Sign=WXPay"

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 149
    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    .line 150
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    .line 151
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 155
    :cond_0
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/WXUtility;->getSecondTimestamp()Ljava/lang/String;

    move-result-object v2

    const/16 v1, 0x20

    .line 156
    invoke-static {v1}, Lcom/ruiqugames/aweijiaqi/WXUtility;->getRandomStringByLength(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 159
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 160
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "appid"

    sget-object v7, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v7, v7, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "partnerid"

    sget-object v7, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v7, v7, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxPartner:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "prepayid"

    invoke-direct {v5, v6, p0}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "package"

    invoke-direct {v5, v6, v3}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "noncestr"

    invoke-direct {v5, v6, v4}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;

    const-string v6, "timestamp"

    invoke-direct {v5, v6, v2}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKV;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v5, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKVComparator;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKVComparator;-><init>(Lcom/ruiqugames/aweijiaqi/WXUtility$PayParamKVComparator;)V

    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 167
    invoke-static {v1}, Lcom/ruiqugames/aweijiaqi/WXUtility;->getSign(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 170
    :goto_0
    new-instance v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;

    invoke-direct {v5}, Lcom/tencent/mm/opensdk/modelpay/PayReq;-><init>()V

    .line 171
    sget-object v6, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v6, v6, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    iput-object v6, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->appId:Ljava/lang/String;

    .line 172
    sget-object v6, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v6, v6, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxPartner:Ljava/lang/String;

    iput-object v6, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->partnerId:Ljava/lang/String;

    .line 173
    iput-object p0, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->prepayId:Ljava/lang/String;

    .line 174
    iput-object v3, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->packageValue:Ljava/lang/String;

    .line 175
    iput-object v4, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->nonceStr:Ljava/lang/String;

    .line 176
    iput-object v2, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->timeStamp:Ljava/lang/String;

    .line 177
    iput-object v1, v5, Lcom/tencent/mm/opensdk/modelpay/PayReq;->sign:Ljava/lang/String;

    const-string v3, "******************** appid = wx145a41445a88580e"

    .line 179
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "******************** partnerid = 1487038752"

    .line 180
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "******************** prepayid = "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string p0, "******************** package = Sign=WXPay"

    .line 182
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance p0, Ljava/lang/StringBuilder;

    const-string v3, "******************** noncestr = "

    invoke-direct {p0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    new-instance p0, Ljava/lang/StringBuilder;

    const-string v3, "******************** timestamp = "

    invoke-direct {p0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance p0, Ljava/lang/StringBuilder;

    const-string v2, "******************** sign = "

    invoke-direct {p0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    sget-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p0, v5}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 190
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public static wxInit(Landroid/content/Context;)V
    .locals 3

    const-string v0, "kill_virus"

    const-string v1, "wxInit"

    .line 63
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_context:Landroid/content/Context;

    .line 65
    move-object v0, p0

    check-cast v0, Lcom/ruiqugames/aweijiaqi/MainActivity;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v0, "wx145a41445a88580e"

    const/4 v1, 0x0

    .line 66
    invoke-static {p0, v0, v1}, Lcom/tencent/mm/opensdk/openapi/WXAPIFactory;->createWXAPI(Landroid/content/Context;Ljava/lang/String;Z)Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    move-result-object p0

    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    .line 67
    invoke-interface {p0, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->registerApp(Ljava/lang/String;)Z

    move-result p0

    .line 68
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wxInit registerApp------->rlt = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public static wxLoginImpl()V
    .locals 4

    .line 114
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->isWXAppInstalled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_context:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "\u6ca1\u6709\u68c0\u6d4b\u5230\u5fae\u4fe1\u5e94\u7528\uff0c\u8bf7\u5b89\u88c5\u5fae\u4fe1\u4e4b\u540e\u518d\u6b21\u767b\u5f55"

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    .line 121
    :cond_0
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;

    invoke-direct {v0}, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;-><init>()V

    const-string v1, "snsapi_userinfo"

    .line 122
    iput-object v1, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;->scope:Ljava/lang/String;

    const-string v1, "wechat_ruiqu_thirteenwater"

    .line 123
    iput-object v1, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;->state:Ljava/lang/String;

    .line 124
    sget-object v1, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v1, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    move-result v0

    .line 126
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WX registerApp------->rlt = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public static wxReInit(Landroid/content/Context;)V
    .locals 3

    const-string v0, "kill_virus"

    const-string v1, "WeChat ReInit"

    .line 85
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_context:Landroid/content/Context;

    .line 87
    move-object v0, p0

    check-cast v0, Lcom/ruiqugames/aweijiaqi/MainActivity;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->_mainActivity:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v0, "wx145a41445a88580e"

    const/4 v1, 0x0

    .line 88
    invoke-static {p0, v0, v1}, Lcom/tencent/mm/opensdk/openapi/WXAPIFactory;->createWXAPI(Landroid/content/Context;Ljava/lang/String;Z)Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    move-result-object p0

    sput-object p0, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    .line 89
    invoke-interface {p0, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->registerApp(Ljava/lang/String;)Z

    move-result p0

    .line 90
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WeChat ReInit registerApp------->rlt = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
