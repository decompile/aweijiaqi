.class Lcom/ruiqugames/aweijiaqi/MainActivity$8;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ruiqugames/aweijiaqi/MainActivity;->ShareMutilPic(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

.field private final synthetic val$desc:Ljava/lang/String;

.field private final synthetic val$flag:I


# direct methods
.method constructor <init>(Lcom/ruiqugames/aweijiaqi/MainActivity;ILjava/lang/String;)V
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iput p2, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->val$flag:I

    iput-object p3, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->val$desc:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 602
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v0, v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->files:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 605
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v1, v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->stringItem:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 612
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 615
    iget v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->val$flag:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "com.tencent.mm"

    if-nez v1, :cond_0

    .line 616
    :try_start_1
    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.tencent.mm.ui.tools.ShareImgUI"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 618
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.tencent.mm.ui.tools.ShareToTimeLineUI"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Kdescription"

    .line 619
    iget-object v3, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->val$desc:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    .line 622
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "image/*"

    .line 623
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 625
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 626
    iget-object v2, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v2, v2, Lcom/ruiqugames/aweijiaqi/MainActivity;->files:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v2, "android.intent.extra.STREAM"

    .line 630
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 631
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v1, v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 626
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 627
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 606
    :cond_2
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v1, v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->stringItem:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_3

    .line 609
    :cond_3
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v1, v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->stringItem:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->GetFileFromLocal(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 610
    iget-object v2, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$8;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    iget-object v2, v2, Lcom/ruiqugames/aweijiaqi/MainActivity;->files:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    .line 633
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void
.end method
