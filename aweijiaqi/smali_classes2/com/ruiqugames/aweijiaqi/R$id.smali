.class public final Lcom/ruiqugames/aweijiaqi/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ruiqugames/aweijiaqi/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f080000

.field public static final CTRL:I = 0x7f080001

.field public static final FUNCTION:I = 0x7f080002

.field public static final META:I = 0x7f080003

.field public static final SHIFT:I = 0x7f080004

.field public static final SYM:I = 0x7f080005

.field public static final action_bar:I = 0x7f080006

.field public static final action_bar_activity_content:I = 0x7f080007

.field public static final action_bar_container:I = 0x7f080008

.field public static final action_bar_root:I = 0x7f080009

.field public static final action_bar_spinner:I = 0x7f08000a

.field public static final action_bar_subtitle:I = 0x7f08000b

.field public static final action_bar_title:I = 0x7f08000c

.field public static final action_container:I = 0x7f08000d

.field public static final action_context_bar:I = 0x7f08000e

.field public static final action_divider:I = 0x7f08000f

.field public static final action_image:I = 0x7f080010

.field public static final action_menu_divider:I = 0x7f080011

.field public static final action_menu_presenter:I = 0x7f080012

.field public static final action_mode_bar:I = 0x7f080013

.field public static final action_mode_bar_stub:I = 0x7f080014

.field public static final action_mode_close_button:I = 0x7f080015

.field public static final action_text:I = 0x7f080016

.field public static final actions:I = 0x7f080017

.field public static final activity_chooser_view_content:I = 0x7f080018

.field public static final add:I = 0x7f080019

.field public static final alertTitle:I = 0x7f08001a

.field public static final all:I = 0x7f08001b

.field public static final always:I = 0x7f08001c

.field public static final anythink_gdpr_btn_area:I = 0x7f08001d

.field public static final anythink_myoffer_banner_ad_install_btn:I = 0x7f08001e

.field public static final anythink_myoffer_banner_ad_text:I = 0x7f08001f

.field public static final anythink_myoffer_banner_ad_title:I = 0x7f080020

.field public static final anythink_myoffer_banner_close:I = 0x7f080021

.field public static final anythink_myoffer_banner_desc:I = 0x7f080022

.field public static final anythink_myoffer_banner_icon:I = 0x7f080023

.field public static final anythink_myoffer_banner_main_image:I = 0x7f080024

.field public static final anythink_myoffer_banner_root:I = 0x7f080025

.field public static final anythink_myoffer_banner_self_ad_logo:I = 0x7f080026

.field public static final anythink_myoffer_banner_view_id:I = 0x7f080027

.field public static final anythink_myoffer_bg_blur_id:I = 0x7f080028

.field public static final anythink_myoffer_btn_banner_cta:I = 0x7f080029

.field public static final anythink_myoffer_btn_close_id:I = 0x7f08002a

.field public static final anythink_myoffer_btn_mute_id:I = 0x7f08002b

.field public static final anythink_myoffer_count_down_view_id:I = 0x7f08002c

.field public static final anythink_myoffer_end_card_id:I = 0x7f08002d

.field public static final anythink_myoffer_feedback_et:I = 0x7f08002e

.field public static final anythink_myoffer_feedback_iv_close:I = 0x7f08002f

.field public static final anythink_myoffer_feedback_ll_abnormal:I = 0x7f080030

.field public static final anythink_myoffer_feedback_ll_report_ad_1:I = 0x7f080031

.field public static final anythink_myoffer_feedback_ll_report_ad_2:I = 0x7f080032

.field public static final anythink_myoffer_feedback_tv_1:I = 0x7f080033

.field public static final anythink_myoffer_feedback_tv_2:I = 0x7f080034

.field public static final anythink_myoffer_feedback_tv_3:I = 0x7f080035

.field public static final anythink_myoffer_feedback_tv_4:I = 0x7f080036

.field public static final anythink_myoffer_feedback_tv_5:I = 0x7f080037

.field public static final anythink_myoffer_feedback_tv_6:I = 0x7f080038

.field public static final anythink_myoffer_feedback_tv_7:I = 0x7f080039

.field public static final anythink_myoffer_feedback_tv_8:I = 0x7f08003a

.field public static final anythink_myoffer_feedback_tv_9:I = 0x7f08003b

.field public static final anythink_myoffer_feedback_tv_abnormal:I = 0x7f08003c

.field public static final anythink_myoffer_feedback_tv_commit:I = 0x7f08003d

.field public static final anythink_myoffer_feedback_tv_other_suggestion:I = 0x7f08003e

.field public static final anythink_myoffer_feedback_tv_report_ad:I = 0x7f08003f

.field public static final anythink_myoffer_full_screen_view_id:I = 0x7f080040

.field public static final anythink_myoffer_iv_banner_icon:I = 0x7f080041

.field public static final anythink_myoffer_iv_logo:I = 0x7f080042

.field public static final anythink_myoffer_loading_id:I = 0x7f080043

.field public static final anythink_myoffer_main_image_id:I = 0x7f080044

.field public static final anythink_myoffer_media_ad_bg_blur:I = 0x7f080045

.field public static final anythink_myoffer_media_ad_close:I = 0x7f080046

.field public static final anythink_myoffer_media_ad_cta:I = 0x7f080047

.field public static final anythink_myoffer_media_ad_logo:I = 0x7f080048

.field public static final anythink_myoffer_media_ad_main_image:I = 0x7f080049

.field public static final anythink_myoffer_player_view_id:I = 0x7f08004a

.field public static final anythink_myoffer_rating_view:I = 0x7f08004b

.field public static final anythink_myoffer_rl_root:I = 0x7f08004c

.field public static final anythink_myoffer_splash_ad_content_image_area:I = 0x7f08004d

.field public static final anythink_myoffer_splash_ad_install_btn:I = 0x7f08004e

.field public static final anythink_myoffer_splash_ad_logo:I = 0x7f08004f

.field public static final anythink_myoffer_splash_ad_title:I = 0x7f080050

.field public static final anythink_myoffer_splash_bg:I = 0x7f080051

.field public static final anythink_myoffer_splash_bottom_area:I = 0x7f080052

.field public static final anythink_myoffer_splash_desc:I = 0x7f080053

.field public static final anythink_myoffer_splash_icon:I = 0x7f080054

.field public static final anythink_myoffer_splash_root:I = 0x7f080055

.field public static final anythink_myoffer_splash_self_ad_logo:I = 0x7f080056

.field public static final anythink_myoffer_splash_skip:I = 0x7f080057

.field public static final anythink_myoffer_tv_banner_desc:I = 0x7f080058

.field public static final anythink_myoffer_tv_banner_title:I = 0x7f080059

.field public static final anythink_plugin_320_banner_adchoice_icon:I = 0x7f08005a

.field public static final anythink_plugin_320_banner_adfrom_view:I = 0x7f08005b

.field public static final anythink_plugin_320_banner_cta:I = 0x7f08005c

.field public static final anythink_plugin_320_banner_desc:I = 0x7f08005d

.field public static final anythink_plugin_320_banner_icon:I = 0x7f08005e

.field public static final anythink_plugin_320_banner_title:I = 0x7f08005f

.field public static final anythink_plugin_640_banner_adchoice_icon:I = 0x7f080060

.field public static final anythink_plugin_640_banner_adfrom_view:I = 0x7f080061

.field public static final anythink_plugin_640_banner_cta:I = 0x7f080062

.field public static final anythink_plugin_640_banner_desc:I = 0x7f080063

.field public static final anythink_plugin_640_banner_from:I = 0x7f080064

.field public static final anythink_plugin_640_banner_title:I = 0x7f080065

.field public static final anythink_plugin_640_image_area:I = 0x7f080066

.field public static final anythink_plugin_auto_banner_adchoice_icon:I = 0x7f080067

.field public static final anythink_plugin_auto_banner_adfrom_view:I = 0x7f080068

.field public static final anythink_plugin_auto_banner_cta:I = 0x7f080069

.field public static final anythink_plugin_auto_banner_desc:I = 0x7f08006a

.field public static final anythink_plugin_auto_banner_icon:I = 0x7f08006b

.field public static final anythink_plugin_auto_banner_title:I = 0x7f08006c

.field public static final anythink_plugin_rating_view:I = 0x7f08006d

.field public static final anythink_plugin_splash_ad_content_image_area:I = 0x7f08006e

.field public static final anythink_plugin_splash_ad_express_area:I = 0x7f08006f

.field public static final anythink_plugin_splash_ad_from:I = 0x7f080070

.field public static final anythink_plugin_splash_ad_install_btn:I = 0x7f080071

.field public static final anythink_plugin_splash_ad_logo:I = 0x7f080072

.field public static final anythink_plugin_splash_ad_title:I = 0x7f080073

.field public static final anythink_plugin_splash_bg:I = 0x7f080074

.field public static final anythink_plugin_splash_desc:I = 0x7f080075

.field public static final anythink_plugin_splash_native:I = 0x7f080076

.field public static final anythink_plugin_splash_right_area:I = 0x7f080077

.field public static final anythink_plugin_splash_self_ad_logo:I = 0x7f080078

.field public static final anythink_plugin_splash_skip:I = 0x7f080079

.field public static final anythink_policy_agree_view:I = 0x7f08007a

.field public static final anythink_policy_content_view:I = 0x7f08007b

.field public static final anythink_policy_loading_view:I = 0x7f08007c

.field public static final anythink_policy_reject_view:I = 0x7f08007d

.field public static final anythink_policy_webview_area:I = 0x7f08007e

.field public static final anythink_tips:I = 0x7f08007f

.field public static final anythink_tips_area:I = 0x7f080080

.field public static final app_logo:I = 0x7f080081

.field public static final async:I = 0x7f080082

.field public static final auto:I = 0x7f080083

.field public static final banner_container:I = 0x7f080084

.field public static final beginning:I = 0x7f080085

.field public static final blocking:I = 0x7f080086

.field public static final bottom:I = 0x7f080087

.field public static final btn_action:I = 0x7f080088

.field public static final buttonPanel:I = 0x7f080089

.field public static final cancel_tv:I = 0x7f08008a

.field public static final center:I = 0x7f08008b

.field public static final center_horizontal:I = 0x7f08008c

.field public static final center_vertical:I = 0x7f08008d

.field public static final checkbox:I = 0x7f08008e

.field public static final chronometer:I = 0x7f08008f

.field public static final clip_horizontal:I = 0x7f080090

.field public static final clip_vertical:I = 0x7f080091

.field public static final collapseActionView:I = 0x7f080092

.field public static final confirm_tv:I = 0x7f080093

.field public static final container:I = 0x7f080094

.field public static final content:I = 0x7f080095

.field public static final contentPanel:I = 0x7f080096

.field public static final content_layout:I = 0x7f080097

.field public static final content_status:I = 0x7f080098

.field public static final content_text:I = 0x7f080099

.field public static final coordinator:I = 0x7f08009a

.field public static final custom:I = 0x7f08009b

.field public static final customPanel:I = 0x7f08009c

.field public static final dash_line:I = 0x7f08009d

.field public static final decor_content_parent:I = 0x7f08009e

.field public static final default_activity_button:I = 0x7f08009f

.field public static final design_bottom_sheet:I = 0x7f0800a0

.field public static final design_menu_item_action_area:I = 0x7f0800a1

.field public static final design_menu_item_action_area_stub:I = 0x7f0800a2

.field public static final design_menu_item_text:I = 0x7f0800a3

.field public static final design_navigation_view:I = 0x7f0800a4

.field public static final disableHome:I = 0x7f0800a5

.field public static final edit_query:I = 0x7f0800a6

.field public static final end:I = 0x7f0800a7

.field public static final enterAlways:I = 0x7f0800a8

.field public static final enterAlwaysCollapsed:I = 0x7f0800a9

.field public static final exitUntilCollapsed:I = 0x7f0800aa

.field public static final expand_activities_button:I = 0x7f0800ab

.field public static final expanded_menu:I = 0x7f0800ac

.field public static final feed_large_img_btn:I = 0x7f0800ad

.field public static final feed_large_img_description:I = 0x7f0800ae

.field public static final feed_large_img_title:I = 0x7f0800af

.field public static final feed_large_imgview:I = 0x7f0800b0

.field public static final fill:I = 0x7f0800b1

.field public static final fill_horizontal:I = 0x7f0800b2

.field public static final fill_vertical:I = 0x7f0800b3

.field public static final filled:I = 0x7f0800b4

.field public static final fixed:I = 0x7f0800b5

.field public static final forever:I = 0x7f0800b6

.field public static final gdt_feed_large_img_btn:I = 0x7f0800b7

.field public static final gdt_feed_large_img_description:I = 0x7f0800b8

.field public static final gdt_feed_large_img_title:I = 0x7f0800b9

.field public static final gdt_feed_large_imgview:I = 0x7f0800ba

.field public static final gdt_game_start:I = 0x7f0800bb

.field public static final gdt_native_ad_container:I = 0x7f0800bc

.field public static final gdt_native_ad_linear_container:I = 0x7f0800bd

.field public static final ghost_view:I = 0x7f0800be

.field public static final group_divider:I = 0x7f0800bf

.field public static final home:I = 0x7f0800c0

.field public static final homeAsUp:I = 0x7f0800c1

.field public static final icon:I = 0x7f0800c2

.field public static final icon_group:I = 0x7f0800c3

.field public static final ifRoom:I = 0x7f0800c4

.field public static final image:I = 0x7f0800c5

.field public static final info:I = 0x7f0800c6

.field public static final italic:I = 0x7f0800c7

.field public static final item_touch_helper_previous_elevation:I = 0x7f0800c8

.field public static final iv_app_icon:I = 0x7f0800c9

.field public static final iv_detail_back:I = 0x7f0800ca

.field public static final iv_listitem_express:I = 0x7f0800cb

.field public static final iv_privacy_back:I = 0x7f0800cc

.field public static final ksad_actionbar_black_style_h5:I = 0x7f0800cd

.field public static final ksad_actionbar_landscape_vertical:I = 0x7f0800ce

.field public static final ksad_actionbar_logo:I = 0x7f0800cf

.field public static final ksad_actionbar_portrait_horizontal:I = 0x7f0800d0

.field public static final ksad_ad_desc:I = 0x7f0800d1

.field public static final ksad_ad_dislike:I = 0x7f0800d2

.field public static final ksad_ad_dislike_logo:I = 0x7f0800d3

.field public static final ksad_ad_download_container:I = 0x7f0800d4

.field public static final ksad_ad_h5_container:I = 0x7f0800d5

.field public static final ksad_ad_image:I = 0x7f0800d6

.field public static final ksad_ad_image_left:I = 0x7f0800d7

.field public static final ksad_ad_image_mid:I = 0x7f0800d8

.field public static final ksad_ad_image_right:I = 0x7f0800d9

.field public static final ksad_ad_interstitial_logo:I = 0x7f0800da

.field public static final ksad_ad_label_play_bar:I = 0x7f0800db

.field public static final ksad_ad_light_convert_btn:I = 0x7f0800dc

.field public static final ksad_ad_normal_container:I = 0x7f0800dd

.field public static final ksad_ad_normal_convert_btn:I = 0x7f0800de

.field public static final ksad_ad_normal_des:I = 0x7f0800df

.field public static final ksad_ad_normal_logo:I = 0x7f0800e0

.field public static final ksad_ad_normal_title:I = 0x7f0800e1

.field public static final ksad_aggregate_web_ad_icon:I = 0x7f0800e2

.field public static final ksad_aggregate_web_navi_back:I = 0x7f0800e3

.field public static final ksad_aggregate_web_root:I = 0x7f0800e4

.field public static final ksad_aggregate_web_titlebar:I = 0x7f0800e5

.field public static final ksad_aggregate_webview:I = 0x7f0800e6

.field public static final ksad_app_ad_desc:I = 0x7f0800e7

.field public static final ksad_app_container:I = 0x7f0800e8

.field public static final ksad_app_desc:I = 0x7f0800e9

.field public static final ksad_app_download:I = 0x7f0800ea

.field public static final ksad_app_download_btn:I = 0x7f0800eb

.field public static final ksad_app_download_btn_cover:I = 0x7f0800ec

.field public static final ksad_app_download_count:I = 0x7f0800ed

.field public static final ksad_app_icon:I = 0x7f0800ee

.field public static final ksad_app_introduce:I = 0x7f0800ef

.field public static final ksad_app_name:I = 0x7f0800f0

.field public static final ksad_app_score:I = 0x7f0800f1

.field public static final ksad_app_title:I = 0x7f0800f2

.field public static final ksad_blur_video_cover:I = 0x7f0800f3

.field public static final ksad_bottom_container:I = 0x7f0800f4

.field public static final ksad_card_ad_desc:I = 0x7f0800f5

.field public static final ksad_card_app_close:I = 0x7f0800f6

.field public static final ksad_card_app_container:I = 0x7f0800f7

.field public static final ksad_card_app_desc:I = 0x7f0800f8

.field public static final ksad_card_app_download_btn:I = 0x7f0800f9

.field public static final ksad_card_app_download_count:I = 0x7f0800fa

.field public static final ksad_card_app_icon:I = 0x7f0800fb

.field public static final ksad_card_app_name:I = 0x7f0800fc

.field public static final ksad_card_app_score:I = 0x7f0800fd

.field public static final ksad_card_app_score_container:I = 0x7f0800fe

.field public static final ksad_card_close:I = 0x7f0800ff

.field public static final ksad_card_h5_container:I = 0x7f080100

.field public static final ksad_card_h5_open_btn:I = 0x7f080101

.field public static final ksad_card_logo:I = 0x7f080102

.field public static final ksad_click_mask:I = 0x7f080103

.field public static final ksad_close_btn:I = 0x7f080104

.field public static final ksad_container:I = 0x7f080105

.field public static final ksad_continue_btn:I = 0x7f080106

.field public static final ksad_data_flow_container:I = 0x7f080107

.field public static final ksad_data_flow_play_btn:I = 0x7f080108

.field public static final ksad_data_flow_play_tip:I = 0x7f080109

.field public static final ksad_detail_call_btn:I = 0x7f08010a

.field public static final ksad_detail_close_btn:I = 0x7f08010b

.field public static final ksad_detail_reward_icon:I = 0x7f08010c

.field public static final ksad_detail_reward_icon_new:I = 0x7f08010d

.field public static final ksad_detail_reward_tip_new:I = 0x7f08010e

.field public static final ksad_download_bar:I = 0x7f08010f

.field public static final ksad_download_bar_cover:I = 0x7f080110

.field public static final ksad_download_container:I = 0x7f080111

.field public static final ksad_download_icon:I = 0x7f080112

.field public static final ksad_download_install:I = 0x7f080113

.field public static final ksad_download_name:I = 0x7f080114

.field public static final ksad_download_percent_num:I = 0x7f080115

.field public static final ksad_download_progress:I = 0x7f080116

.field public static final ksad_download_progress_cover:I = 0x7f080117

.field public static final ksad_download_size:I = 0x7f080118

.field public static final ksad_download_status:I = 0x7f080119

.field public static final ksad_download_tips_web_card_webView:I = 0x7f08011a

.field public static final ksad_draw_h5_logo:I = 0x7f08011b

.field public static final ksad_draw_tailframe_logo:I = 0x7f08011c

.field public static final ksad_end_close_btn:I = 0x7f08011d

.field public static final ksad_end_left_call_btn:I = 0x7f08011e

.field public static final ksad_end_reward_icon:I = 0x7f08011f

.field public static final ksad_end_reward_icon_layout:I = 0x7f080120

.field public static final ksad_end_reward_icon_new_left:I = 0x7f080121

.field public static final ksad_end_reward_icon_new_right:I = 0x7f080122

.field public static final ksad_end_right_call_btn:I = 0x7f080123

.field public static final ksad_exit_intercept_content_layout:I = 0x7f080124

.field public static final ksad_exit_intercept_dialog_layout:I = 0x7f080125

.field public static final ksad_feed_logo:I = 0x7f080126

.field public static final ksad_feed_video_container:I = 0x7f080127

.field public static final ksad_foreground_cover:I = 0x7f080128

.field public static final ksad_h5_ad_desc:I = 0x7f080129

.field public static final ksad_h5_container:I = 0x7f08012a

.field public static final ksad_h5_desc:I = 0x7f08012b

.field public static final ksad_h5_open:I = 0x7f08012c

.field public static final ksad_h5_open_btn:I = 0x7f08012d

.field public static final ksad_h5_open_cover:I = 0x7f08012e

.field public static final ksad_image_container:I = 0x7f08012f

.field public static final ksad_install_tips_close:I = 0x7f080130

.field public static final ksad_install_tips_content:I = 0x7f080131

.field public static final ksad_install_tips_icon:I = 0x7f080132

.field public static final ksad_install_tips_install:I = 0x7f080133

.field public static final ksad_interactive_landing_page_container:I = 0x7f080134

.field public static final ksad_interstitial_close:I = 0x7f080135

.field public static final ksad_interstitial_count_down:I = 0x7f080136

.field public static final ksad_interstitial_desc:I = 0x7f080137

.field public static final ksad_interstitial_download_btn:I = 0x7f080138

.field public static final ksad_interstitial_logo:I = 0x7f080139

.field public static final ksad_interstitial_mute:I = 0x7f08013a

.field public static final ksad_interstitial_native:I = 0x7f08013b

.field public static final ksad_interstitial_play:I = 0x7f08013c

.field public static final ksad_interstitial_play_end:I = 0x7f08013d

.field public static final ksad_interstitial_playing:I = 0x7f08013e

.field public static final ksad_item_touch_helper_previous_elevation:I = 0x7f08013f

.field public static final ksad_js_bottom:I = 0x7f080140

.field public static final ksad_js_slide_black:I = 0x7f080141

.field public static final ksad_js_top:I = 0x7f080142

.field public static final ksad_kwad_titlebar_title:I = 0x7f080143

.field public static final ksad_kwad_web_navi_back:I = 0x7f080144

.field public static final ksad_kwad_web_navi_close:I = 0x7f080145

.field public static final ksad_kwad_web_title_bar:I = 0x7f080146

.field public static final ksad_kwad_web_titlebar:I = 0x7f080147

.field public static final ksad_landing_page_container:I = 0x7f080148

.field public static final ksad_landing_page_root:I = 0x7f080149

.field public static final ksad_logo_container:I = 0x7f08014a

.field public static final ksad_logo_icon:I = 0x7f08014b

.field public static final ksad_logo_text:I = 0x7f08014c

.field public static final ksad_message_toast_txt:I = 0x7f08014d

.field public static final ksad_middle_end_card:I = 0x7f08014e

.field public static final ksad_middle_end_card_native:I = 0x7f08014f

.field public static final ksad_middle_end_card_webview_container:I = 0x7f080150

.field public static final ksad_mini_web_card_container:I = 0x7f080151

.field public static final ksad_mini_web_card_webView:I = 0x7f080152

.field public static final ksad_normal_text:I = 0x7f080153

.field public static final ksad_play_detail_top_toolbar:I = 0x7f080154

.field public static final ksad_play_end_top_toolbar:I = 0x7f080155

.field public static final ksad_play_end_web_card_container:I = 0x7f080156

.field public static final ksad_play_web_card_webView:I = 0x7f080157

.field public static final ksad_playable_webview:I = 0x7f080158

.field public static final ksad_preload_container:I = 0x7f080159

.field public static final ksad_product_name:I = 0x7f08015a

.field public static final ksad_progress_bar:I = 0x7f08015b

.field public static final ksad_progress_bg:I = 0x7f08015c

.field public static final ksad_recycler_container:I = 0x7f08015d

.field public static final ksad_recycler_view:I = 0x7f08015e

.field public static final ksad_reward_container_new:I = 0x7f08015f

.field public static final ksad_reward_mini_card_close:I = 0x7f080160

.field public static final ksad_root_container:I = 0x7f080161

.field public static final ksad_score_fifth:I = 0x7f080162

.field public static final ksad_score_fourth:I = 0x7f080163

.field public static final ksad_skip_icon:I = 0x7f080164

.field public static final ksad_splash_background:I = 0x7f080165

.field public static final ksad_splash_close_btn:I = 0x7f080166

.field public static final ksad_splash_foreground:I = 0x7f080167

.field public static final ksad_splash_frame:I = 0x7f080168

.field public static final ksad_splash_logo_container:I = 0x7f080169

.field public static final ksad_splash_preload_tips:I = 0x7f08016a

.field public static final ksad_splash_root_container:I = 0x7f08016b

.field public static final ksad_splash_skip_time:I = 0x7f08016c

.field public static final ksad_splash_sound:I = 0x7f08016d

.field public static final ksad_splash_texture:I = 0x7f08016e

.field public static final ksad_splash_v_plus:I = 0x7f08016f

.field public static final ksad_splash_video_player:I = 0x7f080170

.field public static final ksad_splash_web_card_webView:I = 0x7f080171

.field public static final ksad_status_tv:I = 0x7f080172

.field public static final ksad_tf_h5_ad_desc:I = 0x7f080173

.field public static final ksad_tf_h5_open_btn:I = 0x7f080174

.field public static final ksad_title:I = 0x7f080175

.field public static final ksad_top_container:I = 0x7f080176

.field public static final ksad_top_container_product:I = 0x7f080177

.field public static final ksad_top_outer:I = 0x7f080178

.field public static final ksad_top_toolbar_close_tip:I = 0x7f080179

.field public static final ksad_video_app_tail_frame:I = 0x7f08017a

.field public static final ksad_video_complete_app_container:I = 0x7f08017b

.field public static final ksad_video_complete_app_icon:I = 0x7f08017c

.field public static final ksad_video_complete_h5_container:I = 0x7f08017d

.field public static final ksad_video_container:I = 0x7f08017e

.field public static final ksad_video_control_container:I = 0x7f08017f

.field public static final ksad_video_control_fullscreen:I = 0x7f080180

.field public static final ksad_video_control_fullscreen_container:I = 0x7f080181

.field public static final ksad_video_control_fullscreen_title:I = 0x7f080182

.field public static final ksad_video_control_play_button:I = 0x7f080183

.field public static final ksad_video_control_play_duration:I = 0x7f080184

.field public static final ksad_video_control_play_status:I = 0x7f080185

.field public static final ksad_video_control_play_total:I = 0x7f080186

.field public static final ksad_video_count_down:I = 0x7f080187

.field public static final ksad_video_count_down_new:I = 0x7f080188

.field public static final ksad_video_cover:I = 0x7f080189

.field public static final ksad_video_cover_image:I = 0x7f08018a

.field public static final ksad_video_error_container:I = 0x7f08018b

.field public static final ksad_video_fail_tip:I = 0x7f08018c

.field public static final ksad_video_first_frame:I = 0x7f08018d

.field public static final ksad_video_first_frame_container:I = 0x7f08018e

.field public static final ksad_video_h5_tail_frame:I = 0x7f08018f

.field public static final ksad_video_landscape_horizontal:I = 0x7f080190

.field public static final ksad_video_landscape_vertical:I = 0x7f080191

.field public static final ksad_video_network_unavailable:I = 0x7f080192

.field public static final ksad_video_place_holder:I = 0x7f080193

.field public static final ksad_video_play_bar_app_landscape:I = 0x7f080194

.field public static final ksad_video_play_bar_app_portrait:I = 0x7f080195

.field public static final ksad_video_play_bar_h5:I = 0x7f080196

.field public static final ksad_video_player:I = 0x7f080197

.field public static final ksad_video_portrait_horizontal:I = 0x7f080198

.field public static final ksad_video_portrait_vertical:I = 0x7f080199

.field public static final ksad_video_progress:I = 0x7f08019a

.field public static final ksad_video_sound_switch:I = 0x7f08019b

.field public static final ksad_video_tail_frame:I = 0x7f08019c

.field public static final ksad_video_tail_frame_container:I = 0x7f08019d

.field public static final ksad_video_text_below:I = 0x7f08019e

.field public static final ksad_video_tf_logo:I = 0x7f08019f

.field public static final ksad_video_thumb_container:I = 0x7f0801a0

.field public static final ksad_video_thumb_image:I = 0x7f0801a1

.field public static final ksad_video_thumb_img:I = 0x7f0801a2

.field public static final ksad_video_thumb_left:I = 0x7f0801a3

.field public static final ksad_video_thumb_mid:I = 0x7f0801a4

.field public static final ksad_video_thumb_right:I = 0x7f0801a5

.field public static final ksad_video_webView:I = 0x7f0801a6

.field public static final ksad_video_webview:I = 0x7f0801a7

.field public static final ksad_web_card_container:I = 0x7f0801a8

.field public static final ksad_web_card_frame:I = 0x7f0801a9

.field public static final ksad_web_card_webView:I = 0x7f0801aa

.field public static final ksad_web_download_container:I = 0x7f0801ab

.field public static final ksad_web_download_progress:I = 0x7f0801ac

.field public static final ksad_web_exit_intercept_negative_btn:I = 0x7f0801ad

.field public static final ksad_web_exit_intercept_positive_btn:I = 0x7f0801ae

.field public static final ksad_web_tip_bar:I = 0x7f0801af

.field public static final ksad_web_tip_bar_textview:I = 0x7f0801b0

.field public static final ksad_web_tip_close_btn:I = 0x7f0801b1

.field public static final ksad_web_video_seek_bar:I = 0x7f0801b2

.field public static final labeled:I = 0x7f0801b3

.field public static final largeLabel:I = 0x7f0801b4

.field public static final left:I = 0x7f0801b5

.field public static final left_icon:I = 0x7f0801b6

.field public static final line:I = 0x7f0801b7

.field public static final line1:I = 0x7f0801b8

.field public static final line3:I = 0x7f0801b9

.field public static final listMode:I = 0x7f0801ba

.field public static final list_item:I = 0x7f0801bb

.field public static final ll_download:I = 0x7f0801bc

.field public static final logo_area:I = 0x7f0801bd

.field public static final lv_dislike_custom:I = 0x7f0801be

.field public static final masked:I = 0x7f0801bf

.field public static final message:I = 0x7f0801c0

.field public static final message_tv:I = 0x7f0801c1

.field public static final middle:I = 0x7f0801c2

.field public static final mini:I = 0x7f0801c3

.field public static final mtrl_child_content_container:I = 0x7f0801c4

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0801c5

.field public static final multiply:I = 0x7f0801c6

.field public static final navigation_header_container:I = 0x7f0801c7

.field public static final never:I = 0x7f0801c8

.field public static final none:I = 0x7f0801c9

.field public static final normal:I = 0x7f0801ca

.field public static final notification_background:I = 0x7f0801cb

.field public static final notification_container:I = 0x7f0801cc

.field public static final notification_main_column:I = 0x7f0801cd

.field public static final notification_main_column_container:I = 0x7f0801ce

.field public static final notification_title:I = 0x7f0801cf

.field public static final outline:I = 0x7f0801d0

.field public static final parallax:I = 0x7f0801d1

.field public static final parentPanel:I = 0x7f0801d2

.field public static final parent_matrix:I = 0x7f0801d3

.field public static final permission_list:I = 0x7f0801d4

.field public static final pin:I = 0x7f0801d5

.field public static final privacy_webview:I = 0x7f0801d6

.field public static final progress_bar:I = 0x7f0801d7

.field public static final progress_circular:I = 0x7f0801d8

.field public static final progress_horizontal:I = 0x7f0801d9

.field public static final radio:I = 0x7f0801da

.field public static final right:I = 0x7f0801db

.field public static final right_icon:I = 0x7f0801dc

.field public static final right_side:I = 0x7f0801dd

.field public static final root:I = 0x7f0801de

.field public static final save_image_matrix:I = 0x7f0801df

.field public static final save_non_transition_alpha:I = 0x7f0801e0

.field public static final save_scale_type:I = 0x7f0801e1

.field public static final screen:I = 0x7f0801e2

.field public static final scroll:I = 0x7f0801e3

.field public static final scrollIndicatorDown:I = 0x7f0801e4

.field public static final scrollIndicatorUp:I = 0x7f0801e5

.field public static final scrollView:I = 0x7f0801e6

.field public static final scrollable:I = 0x7f0801e7

.field public static final search_badge:I = 0x7f0801e8

.field public static final search_bar:I = 0x7f0801e9

.field public static final search_button:I = 0x7f0801ea

.field public static final search_close_btn:I = 0x7f0801eb

.field public static final search_edit_frame:I = 0x7f0801ec

.field public static final search_go_btn:I = 0x7f0801ed

.field public static final search_mag_icon:I = 0x7f0801ee

.field public static final search_plate:I = 0x7f0801ef

.field public static final search_src_text:I = 0x7f0801f0

.field public static final search_voice_btn:I = 0x7f0801f1

.field public static final select_dialog_listview:I = 0x7f0801f2

.field public static final selected:I = 0x7f0801f3

.field public static final shortcut:I = 0x7f0801f4

.field public static final showCustom:I = 0x7f0801f5

.field public static final showHome:I = 0x7f0801f6

.field public static final showTitle:I = 0x7f0801f7

.field public static final smallLabel:I = 0x7f0801f8

.field public static final snackbar_action:I = 0x7f0801f9

.field public static final snackbar_text:I = 0x7f0801fa

.field public static final snap:I = 0x7f0801fb

.field public static final snapMargins:I = 0x7f0801fc

.field public static final spacer:I = 0x7f0801fd

.field public static final splash_ad_container:I = 0x7f0801fe

.field public static final splash_ad_skip:I = 0x7f0801ff

.field public static final splash_container:I = 0x7f080200

.field public static final split_action_bar:I = 0x7f080201

.field public static final src_atop:I = 0x7f080202

.field public static final src_in:I = 0x7f080203

.field public static final src_over:I = 0x7f080204

.field public static final start:I = 0x7f080205

.field public static final stretch:I = 0x7f080206

.field public static final submenuarrow:I = 0x7f080207

.field public static final submit_area:I = 0x7f080208

.field public static final tabMode:I = 0x7f080209

.field public static final tag_transition_group:I = 0x7f08020a

.field public static final tag_unhandled_key_event_manager:I = 0x7f08020b

.field public static final tag_unhandled_key_listeners:I = 0x7f08020c

.field public static final text:I = 0x7f08020d

.field public static final text2:I = 0x7f08020e

.field public static final textSpacerNoButtons:I = 0x7f08020f

.field public static final textSpacerNoTitle:I = 0x7f080210

.field public static final textStart:I = 0x7f080211

.field public static final text_input_password_toggle:I = 0x7f080212

.field public static final textinput_counter:I = 0x7f080213

.field public static final textinput_error:I = 0x7f080214

.field public static final textinput_helper_text:I = 0x7f080215

.field public static final time:I = 0x7f080216

.field public static final title:I = 0x7f080217

.field public static final titleDividerNoCustom:I = 0x7f080218

.field public static final title_bar:I = 0x7f080219

.field public static final title_template:I = 0x7f08021a

.field public static final toolBar:I = 0x7f08021b

.field public static final top:I = 0x7f08021c

.field public static final topPanel:I = 0x7f08021d

.field public static final touch_outside:I = 0x7f08021e

.field public static final transition_current_scene:I = 0x7f08021f

.field public static final transition_layout_save:I = 0x7f080220

.field public static final transition_position:I = 0x7f080221

.field public static final transition_scene_layoutid_cache:I = 0x7f080222

.field public static final transition_transform:I = 0x7f080223

.field public static final tt_ad_container:I = 0x7f080224

.field public static final tt_ad_content_layout:I = 0x7f080225

.field public static final tt_ad_logo:I = 0x7f080226

.field public static final tt_app_detail_back_tv:I = 0x7f080227

.field public static final tt_app_developer_tv:I = 0x7f080228

.field public static final tt_app_name_tv:I = 0x7f080229

.field public static final tt_app_privacy_back_tv:I = 0x7f08022a

.field public static final tt_app_privacy_title:I = 0x7f08022b

.field public static final tt_app_privacy_tv:I = 0x7f08022c

.field public static final tt_app_privacy_url_tv:I = 0x7f08022d

.field public static final tt_app_version_tv:I = 0x7f08022e

.field public static final tt_appdownloader_action:I = 0x7f08022f

.field public static final tt_appdownloader_desc:I = 0x7f080230

.field public static final tt_appdownloader_download_progress:I = 0x7f080231

.field public static final tt_appdownloader_download_progress_new:I = 0x7f080232

.field public static final tt_appdownloader_download_size:I = 0x7f080233

.field public static final tt_appdownloader_download_status:I = 0x7f080234

.field public static final tt_appdownloader_download_success:I = 0x7f080235

.field public static final tt_appdownloader_download_success_size:I = 0x7f080236

.field public static final tt_appdownloader_download_success_status:I = 0x7f080237

.field public static final tt_appdownloader_download_text:I = 0x7f080238

.field public static final tt_appdownloader_icon:I = 0x7f080239

.field public static final tt_appdownloader_root:I = 0x7f08023a

.field public static final tt_backup_draw_bg:I = 0x7f08023b

.field public static final tt_backup_logoLayout:I = 0x7f08023c

.field public static final tt_battery_time_layout:I = 0x7f08023d

.field public static final tt_browser_download_btn:I = 0x7f08023e

.field public static final tt_browser_download_btn_stub:I = 0x7f08023f

.field public static final tt_browser_progress:I = 0x7f080240

.field public static final tt_browser_titlebar_dark_view_stub:I = 0x7f080241

.field public static final tt_browser_titlebar_view_stub:I = 0x7f080242

.field public static final tt_browser_webview:I = 0x7f080243

.field public static final tt_browser_webview_loading:I = 0x7f080244

.field public static final tt_bu_close:I = 0x7f080245

.field public static final tt_bu_desc:I = 0x7f080246

.field public static final tt_bu_dislike:I = 0x7f080247

.field public static final tt_bu_download:I = 0x7f080248

.field public static final tt_bu_icon:I = 0x7f080249

.field public static final tt_bu_img:I = 0x7f08024a

.field public static final tt_bu_img_1:I = 0x7f08024b

.field public static final tt_bu_img_2:I = 0x7f08024c

.field public static final tt_bu_img_3:I = 0x7f08024d

.field public static final tt_bu_img_container:I = 0x7f08024e

.field public static final tt_bu_img_content:I = 0x7f08024f

.field public static final tt_bu_name:I = 0x7f080250

.field public static final tt_bu_score:I = 0x7f080251

.field public static final tt_bu_score_bar:I = 0x7f080252

.field public static final tt_bu_title:I = 0x7f080253

.field public static final tt_bu_total_title:I = 0x7f080254

.field public static final tt_bu_video_container:I = 0x7f080255

.field public static final tt_bu_video_container_inner:I = 0x7f080256

.field public static final tt_bu_video_icon:I = 0x7f080257

.field public static final tt_bu_video_name1:I = 0x7f080258

.field public static final tt_bu_video_name2:I = 0x7f080259

.field public static final tt_bu_video_score:I = 0x7f08025a

.field public static final tt_bu_video_score_bar:I = 0x7f08025b

.field public static final tt_button_ok:I = 0x7f08025c

.field public static final tt_click_lower_non_content_layout:I = 0x7f08025d

.field public static final tt_click_upper_non_content_layout:I = 0x7f08025e

.field public static final tt_column_line:I = 0x7f08025f

.field public static final tt_comment_backup:I = 0x7f080260

.field public static final tt_comment_close:I = 0x7f080261

.field public static final tt_comment_commit:I = 0x7f080262

.field public static final tt_comment_content:I = 0x7f080263

.field public static final tt_comment_number:I = 0x7f080264

.field public static final tt_comment_vertical:I = 0x7f080265

.field public static final tt_dialog_content:I = 0x7f080266

.field public static final tt_dislike_comment_layout:I = 0x7f080267

.field public static final tt_dislike_layout:I = 0x7f080268

.field public static final tt_dislike_line1:I = 0x7f080269

.field public static final tt_download_app_btn:I = 0x7f08026a

.field public static final tt_download_app_detail:I = 0x7f08026b

.field public static final tt_download_app_developer:I = 0x7f08026c

.field public static final tt_download_app_privacy:I = 0x7f08026d

.field public static final tt_download_app_version:I = 0x7f08026e

.field public static final tt_download_btn:I = 0x7f08026f

.field public static final tt_download_cancel:I = 0x7f080270

.field public static final tt_download_icon:I = 0x7f080271

.field public static final tt_download_layout:I = 0x7f080272

.field public static final tt_download_title:I = 0x7f080273

.field public static final tt_edit_suggestion:I = 0x7f080274

.field public static final tt_filer_words_lv:I = 0x7f080275

.field public static final tt_full_ad_app_name:I = 0x7f080276

.field public static final tt_full_ad_appname:I = 0x7f080277

.field public static final tt_full_ad_desc:I = 0x7f080278

.field public static final tt_full_ad_download:I = 0x7f080279

.field public static final tt_full_ad_icon:I = 0x7f08027a

.field public static final tt_full_comment:I = 0x7f08027b

.field public static final tt_full_desc:I = 0x7f08027c

.field public static final tt_full_image_full_bar:I = 0x7f08027d

.field public static final tt_full_image_layout:I = 0x7f08027e

.field public static final tt_full_image_reward_bar:I = 0x7f08027f

.field public static final tt_full_img:I = 0x7f080280

.field public static final tt_full_rb_score:I = 0x7f080281

.field public static final tt_full_root:I = 0x7f080282

.field public static final tt_full_splash_bar_layout:I = 0x7f080283

.field public static final tt_game_start:I = 0x7f080284

.field public static final tt_image:I = 0x7f080285

.field public static final tt_image_full_bar:I = 0x7f080286

.field public static final tt_image_group_layout:I = 0x7f080287

.field public static final tt_insert_ad_img:I = 0x7f080288

.field public static final tt_insert_ad_logo:I = 0x7f080289

.field public static final tt_insert_ad_text:I = 0x7f08028a

.field public static final tt_insert_dislike_icon_img:I = 0x7f08028b

.field public static final tt_insert_express_ad_fl:I = 0x7f08028c

.field public static final tt_install_btn_no:I = 0x7f08028d

.field public static final tt_install_btn_yes:I = 0x7f08028e

.field public static final tt_install_content:I = 0x7f08028f

.field public static final tt_install_title:I = 0x7f080290

.field public static final tt_item_desc_tv:I = 0x7f080291

.field public static final tt_item_select_img:I = 0x7f080292

.field public static final tt_item_title_tv:I = 0x7f080293

.field public static final tt_item_tv:I = 0x7f080294

.field public static final tt_item_tv_son:I = 0x7f080295

.field public static final tt_lite_web_back:I = 0x7f080296

.field public static final tt_lite_web_title:I = 0x7f080297

.field public static final tt_lite_web_view:I = 0x7f080298

.field public static final tt_message:I = 0x7f080299

.field public static final tt_middle_page_layout:I = 0x7f08029a

.field public static final tt_native_video_container:I = 0x7f08029b

.field public static final tt_native_video_frame:I = 0x7f08029c

.field public static final tt_native_video_img_cover:I = 0x7f08029d

.field public static final tt_native_video_img_cover_viewStub:I = 0x7f08029e

.field public static final tt_native_video_img_id:I = 0x7f08029f

.field public static final tt_native_video_layout:I = 0x7f0802a0

.field public static final tt_native_video_play:I = 0x7f0802a1

.field public static final tt_native_video_titlebar:I = 0x7f0802a2

.field public static final tt_negtive:I = 0x7f0802a3

.field public static final tt_open_app_detail_layout:I = 0x7f0802a4

.field public static final tt_personalization_layout:I = 0x7f0802a5

.field public static final tt_personalization_name:I = 0x7f0802a6

.field public static final tt_playable_ad_close:I = 0x7f0802a7

.field public static final tt_playable_ad_close_layout:I = 0x7f0802a8

.field public static final tt_playable_ad_dislike:I = 0x7f0802a9

.field public static final tt_playable_ad_mute:I = 0x7f0802aa

.field public static final tt_playable_loading:I = 0x7f0802ab

.field public static final tt_playable_pb_view:I = 0x7f0802ac

.field public static final tt_playable_play:I = 0x7f0802ad

.field public static final tt_playable_progress_tip:I = 0x7f0802ae

.field public static final tt_positive:I = 0x7f0802af

.field public static final tt_privacy_layout:I = 0x7f0802b0

.field public static final tt_privacy_list:I = 0x7f0802b1

.field public static final tt_privacy_webview:I = 0x7f0802b2

.field public static final tt_ratio_image_view:I = 0x7f0802b3

.field public static final tt_rb_score:I = 0x7f0802b4

.field public static final tt_rb_score_backup:I = 0x7f0802b5

.field public static final tt_reward_ad_appname:I = 0x7f0802b6

.field public static final tt_reward_ad_appname_backup:I = 0x7f0802b7

.field public static final tt_reward_ad_download:I = 0x7f0802b8

.field public static final tt_reward_ad_download_backup:I = 0x7f0802b9

.field public static final tt_reward_ad_download_layout:I = 0x7f0802ba

.field public static final tt_reward_ad_icon:I = 0x7f0802bb

.field public static final tt_reward_ad_icon_backup:I = 0x7f0802bc

.field public static final tt_reward_browser_webview:I = 0x7f0802bd

.field public static final tt_reward_browser_webview_playable:I = 0x7f0802be

.field public static final tt_reward_full_endcard_backup:I = 0x7f0802bf

.field public static final tt_reward_playable_loading:I = 0x7f0802c0

.field public static final tt_reward_root:I = 0x7f0802c1

.field public static final tt_rl_download:I = 0x7f0802c2

.field public static final tt_root_view:I = 0x7f0802c3

.field public static final tt_scroll_view:I = 0x7f0802c4

.field public static final tt_splash_ad_gif:I = 0x7f0802c5

.field public static final tt_splash_backup_desc:I = 0x7f0802c6

.field public static final tt_splash_backup_img:I = 0x7f0802c7

.field public static final tt_splash_backup_text:I = 0x7f0802c8

.field public static final tt_splash_backup_video_container:I = 0x7f0802c9

.field public static final tt_splash_bar_text:I = 0x7f0802ca

.field public static final tt_splash_close_btn:I = 0x7f0802cb

.field public static final tt_splash_express_container:I = 0x7f0802cc

.field public static final tt_splash_icon_close:I = 0x7f0802cd

.field public static final tt_splash_icon_image:I = 0x7f0802ce

.field public static final tt_splash_icon_video_container:I = 0x7f0802cf

.field public static final tt_splash_skip_btn:I = 0x7f0802d0

.field public static final tt_splash_video_ad_mute:I = 0x7f0802d1

.field public static final tt_splash_video_container:I = 0x7f0802d2

.field public static final tt_title:I = 0x7f0802d3

.field public static final tt_titlebar_app_detail:I = 0x7f0802d4

.field public static final tt_titlebar_app_name:I = 0x7f0802d5

.field public static final tt_titlebar_app_privacy:I = 0x7f0802d6

.field public static final tt_titlebar_back:I = 0x7f0802d7

.field public static final tt_titlebar_close:I = 0x7f0802d8

.field public static final tt_titlebar_detail_layout:I = 0x7f0802d9

.field public static final tt_titlebar_developer:I = 0x7f0802da

.field public static final tt_titlebar_dislike:I = 0x7f0802db

.field public static final tt_titlebar_title:I = 0x7f0802dc

.field public static final tt_top_dislike:I = 0x7f0802dd

.field public static final tt_top_layout_proxy:I = 0x7f0802de

.field public static final tt_top_mute:I = 0x7f0802df

.field public static final tt_top_skip:I = 0x7f0802e0

.field public static final tt_video_ad_bottom_layout:I = 0x7f0802e1

.field public static final tt_video_ad_button:I = 0x7f0802e2

.field public static final tt_video_ad_button_draw:I = 0x7f0802e3

.field public static final tt_video_ad_close_layout:I = 0x7f0802e4

.field public static final tt_video_ad_cover:I = 0x7f0802e5

.field public static final tt_video_ad_cover_center_layout:I = 0x7f0802e6

.field public static final tt_video_ad_cover_center_layout_draw:I = 0x7f0802e7

.field public static final tt_video_ad_covers:I = 0x7f0802e8

.field public static final tt_video_ad_finish_cover_image:I = 0x7f0802e9

.field public static final tt_video_ad_full_screen:I = 0x7f0802ea

.field public static final tt_video_ad_logo_image:I = 0x7f0802eb

.field public static final tt_video_ad_name:I = 0x7f0802ec

.field public static final tt_video_ad_replay:I = 0x7f0802ed

.field public static final tt_video_app_detail:I = 0x7f0802ee

.field public static final tt_video_app_detail_layout:I = 0x7f0802ef

.field public static final tt_video_app_name:I = 0x7f0802f0

.field public static final tt_video_app_privacy:I = 0x7f0802f1

.field public static final tt_video_back:I = 0x7f0802f2

.field public static final tt_video_btn_ad_image_tv:I = 0x7f0802f3

.field public static final tt_video_close:I = 0x7f0802f4

.field public static final tt_video_current_time:I = 0x7f0802f5

.field public static final tt_video_developer:I = 0x7f0802f6

.field public static final tt_video_draw_layout_viewStub:I = 0x7f0802f7

.field public static final tt_video_fullscreen_back:I = 0x7f0802f8

.field public static final tt_video_loading_cover_image:I = 0x7f0802f9

.field public static final tt_video_loading_progress:I = 0x7f0802fa

.field public static final tt_video_loading_retry:I = 0x7f0802fb

.field public static final tt_video_loading_retry_layout:I = 0x7f0802fc

.field public static final tt_video_play:I = 0x7f0802fd

.field public static final tt_video_progress:I = 0x7f0802fe

.field public static final tt_video_retry:I = 0x7f0802ff

.field public static final tt_video_retry_des:I = 0x7f080300

.field public static final tt_video_reward_bar:I = 0x7f080301

.field public static final tt_video_reward_container:I = 0x7f080302

.field public static final tt_video_seekbar:I = 0x7f080303

.field public static final tt_video_time_left_time:I = 0x7f080304

.field public static final tt_video_time_play:I = 0x7f080305

.field public static final tt_video_title:I = 0x7f080306

.field public static final tt_video_top_layout:I = 0x7f080307

.field public static final tt_video_top_title:I = 0x7f080308

.field public static final tt_video_traffic_continue_play_btn:I = 0x7f080309

.field public static final tt_video_traffic_continue_play_tv:I = 0x7f08030a

.field public static final tt_video_traffic_tip_layout:I = 0x7f08030b

.field public static final tt_video_traffic_tip_layout_viewStub:I = 0x7f08030c

.field public static final tt_video_traffic_tip_tv:I = 0x7f08030d

.field public static final tv_app_detail:I = 0x7f08030e

.field public static final tv_app_developer:I = 0x7f08030f

.field public static final tv_app_name:I = 0x7f080310

.field public static final tv_app_privacy:I = 0x7f080311

.field public static final tv_app_version:I = 0x7f080312

.field public static final tv_empty:I = 0x7f080313

.field public static final tv_give_up:I = 0x7f080314

.field public static final tv_permission_description:I = 0x7f080315

.field public static final tv_permission_title:I = 0x7f080316

.field public static final uniform:I = 0x7f080317

.field public static final unlabeled:I = 0x7f080318

.field public static final up:I = 0x7f080319

.field public static final useLogo:I = 0x7f08031a

.field public static final view_offset_helper:I = 0x7f08031b

.field public static final visible:I = 0x7f08031c

.field public static final wd_surfaceViewForVideo:I = 0x7f08031d

.field public static final wd_videoJump:I = 0x7f08031e

.field public static final wd_videoView:I = 0x7f08031f

.field public static final webView:I = 0x7f080320

.field public static final webViewback:I = 0x7f080321

.field public static final web_frame:I = 0x7f080322

.field public static final withText:I = 0x7f080323

.field public static final wrap_content:I = 0x7f080324


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
