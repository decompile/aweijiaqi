.class Lcom/ruiqugames/aweijiaqi/MainActivity$2;
.super Landroid/os/Handler;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ruiqugames/aweijiaqi/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;


# direct methods
.method constructor <init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
    .locals 0

    .line 810
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .line 813
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "9000"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v2, :cond_2

    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    goto/16 :goto_0

    .line 834
    :cond_0
    new-instance v0, Lcom/ruiqugames/aweijiaqi/AuthResult;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-direct {v0, p1, v2}, Lcom/ruiqugames/aweijiaqi/AuthResult;-><init>(Ljava/util/Map;Z)V

    .line 835
    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/AuthResult;->getResultStatus()Ljava/lang/String;

    move-result-object p1

    .line 839
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    const-string v1, "authCode:%s"

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/AuthResult;->getResultCode()Ljava/lang/String;

    move-result-object p1

    const-string v4, "200"

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 842
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6388\u6743\u6210\u529f\n"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/AuthResult;->getAuthCode()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 843
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 846
    :cond_1
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6388\u6743\u5931\u8d25"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/AuthResult;->getAuthCode()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 847
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 816
    :cond_2
    new-instance v0, Lcom/ruiqugames/aweijiaqi/PayResult;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-direct {v0, p1}, Lcom/ruiqugames/aweijiaqi/PayResult;-><init>(Ljava/util/Map;)V

    .line 820
    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/PayResult;->getResult()Ljava/lang/String;

    .line 821
    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/PayResult;->getResultStatus()Ljava/lang/String;

    move-result-object p1

    .line 823
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 825
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v0, "\u652f\u4ed8\u6210\u529f"

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 828
    :cond_3
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v0, "\u652f\u4ed8\u5931\u8d25"

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void
.end method
