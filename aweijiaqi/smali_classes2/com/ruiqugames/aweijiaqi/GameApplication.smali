.class public Lcom/ruiqugames/aweijiaqi/GameApplication;
.super Landroid/app/Application;
.source "GameApplication.java"


# static fields
.field public static context:Landroid/content/Context;

.field public static json:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-static {p1}, Lcn/dxs/myapplication/hookpms/ServiceManagerWraper;->hookPMS(Landroid/content/Context;)V

    .line 21
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public isMainProcess()Z
    .locals 4

    .line 75
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const-string v1, "activity"

    .line 76
    invoke-virtual {p0, v1}, Lcom/ruiqugames/aweijiaqi/GameApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 77
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 78
    iget v3, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/GameApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 3

    .line 19
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 20
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/GameApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/ruiqugames/aweijiaqi/GameApplication;->context:Landroid/content/Context;

    const/4 v0, 0x0

    .line 24
    invoke-static {v0}, Lcom/anythink/core/api/ATSDK;->setNetworkLogDebug(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/GameApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "a60c87f6a4d387"

    const-string v2, "3dcc97024a82d4f4eef24e0ff425302c"

    invoke-static {v0, v1, v2}, Lcom/anythink/core/api/ATSDK;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/GameApplication;->isMainProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-static {p0}, Lcom/fm/openinstall/OpenInstall;->init(Landroid/content/Context;)V

    .line 47
    new-instance v0, Lcom/ruiqugames/aweijiaqi/GameApplication$1;

    invoke-direct {v0, p0}, Lcom/ruiqugames/aweijiaqi/GameApplication$1;-><init>(Lcom/ruiqugames/aweijiaqi/GameApplication;)V

    invoke-static {v0}, Lcom/fm/openinstall/OpenInstall;->getInstall(Lcom/fm/openinstall/listener/AppInstallListener;)V

    :cond_0
    return-void
.end method
