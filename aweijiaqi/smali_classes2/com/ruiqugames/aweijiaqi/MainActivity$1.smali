.class Lcom/ruiqugames/aweijiaqi/MainActivity$1;
.super Lcom/fm/openinstall/listener/AppWakeUpAdapter;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ruiqugames/aweijiaqi/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;


# direct methods
.method constructor <init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
    .locals 0

    .line 401
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$1;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-direct {p0}, Lcom/fm/openinstall/listener/AppWakeUpAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onWakeUp(Lcom/fm/openinstall/model/AppData;)V
    .locals 5

    .line 405
    invoke-virtual {p1}, Lcom/fm/openinstall/model/AppData;->getChannel()Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-virtual {p1}, Lcom/fm/openinstall/model/AppData;->getData()Ljava/lang/String;

    move-result-object v1

    .line 409
    sget-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    if-nez v2, :cond_0

    .line 411
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 412
    sput-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    .line 415
    :cond_0
    :try_start_0
    sget-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    const-string v3, "errCode"

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 416
    sget-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    const-string v3, "bindData"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    sget-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    const-string v3, "channelCode"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "onWakeUp OpenInstall"

    .line 418
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindData : bindData = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$1;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v1, "OnGetShareParam"

    sget-object v2, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getWakeUp : wakeupData = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/fm/openinstall/model/AppData;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "OpenInstall"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
