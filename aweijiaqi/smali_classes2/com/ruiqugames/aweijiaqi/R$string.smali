.class public final Lcom/ruiqugames/aweijiaqi/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ruiqugames/aweijiaqi/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0e0000

.field public static final abc_action_bar_up_description:I = 0x7f0e0001

.field public static final abc_action_menu_overflow_description:I = 0x7f0e0002

.field public static final abc_action_mode_done:I = 0x7f0e0003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e0004

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e0005

.field public static final abc_capital_off:I = 0x7f0e0006

.field public static final abc_capital_on:I = 0x7f0e0007

.field public static final abc_font_family_body_1_material:I = 0x7f0e0008

.field public static final abc_font_family_body_2_material:I = 0x7f0e0009

.field public static final abc_font_family_button_material:I = 0x7f0e000a

.field public static final abc_font_family_caption_material:I = 0x7f0e000b

.field public static final abc_font_family_display_1_material:I = 0x7f0e000c

.field public static final abc_font_family_display_2_material:I = 0x7f0e000d

.field public static final abc_font_family_display_3_material:I = 0x7f0e000e

.field public static final abc_font_family_display_4_material:I = 0x7f0e000f

.field public static final abc_font_family_headline_material:I = 0x7f0e0010

.field public static final abc_font_family_menu_material:I = 0x7f0e0011

.field public static final abc_font_family_subhead_material:I = 0x7f0e0012

.field public static final abc_font_family_title_material:I = 0x7f0e0013

.field public static final abc_menu_alt_shortcut_label:I = 0x7f0e0014

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f0e0015

.field public static final abc_menu_delete_shortcut_label:I = 0x7f0e0016

.field public static final abc_menu_enter_shortcut_label:I = 0x7f0e0017

.field public static final abc_menu_function_shortcut_label:I = 0x7f0e0018

.field public static final abc_menu_meta_shortcut_label:I = 0x7f0e0019

.field public static final abc_menu_shift_shortcut_label:I = 0x7f0e001a

.field public static final abc_menu_space_shortcut_label:I = 0x7f0e001b

.field public static final abc_menu_sym_shortcut_label:I = 0x7f0e001c

.field public static final abc_prepend_shortcut_label:I = 0x7f0e001d

.field public static final abc_search_hint:I = 0x7f0e001e

.field public static final abc_searchview_description_clear:I = 0x7f0e001f

.field public static final abc_searchview_description_query:I = 0x7f0e0020

.field public static final abc_searchview_description_search:I = 0x7f0e0021

.field public static final abc_searchview_description_submit:I = 0x7f0e0022

.field public static final abc_searchview_description_voice:I = 0x7f0e0023

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e0024

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e0025

.field public static final abc_toolbar_collapse_description:I = 0x7f0e0026

.field public static final anythink_basead_click_empty:I = 0x7f0e0027

.field public static final anythink_basead_click_fail:I = 0x7f0e0028

.field public static final anythink_myoffer_cta_learn_more:I = 0x7f0e0029

.field public static final anythink_myoffer_feedback_abnormal:I = 0x7f0e002a

.field public static final anythink_myoffer_feedback_black_white_screen:I = 0x7f0e002b

.field public static final anythink_myoffer_feedback_can_not_close:I = 0x7f0e002c

.field public static final anythink_myoffer_feedback_fraud_ads:I = 0x7f0e002d

.field public static final anythink_myoffer_feedback_hint:I = 0x7f0e002e

.field public static final anythink_myoffer_feedback_induce_click:I = 0x7f0e002f

.field public static final anythink_myoffer_feedback_not_interesting:I = 0x7f0e0030

.field public static final anythink_myoffer_feedback_plagiarism:I = 0x7f0e0031

.field public static final anythink_myoffer_feedback_report:I = 0x7f0e0032

.field public static final anythink_myoffer_feedback_submit:I = 0x7f0e0033

.field public static final anythink_myoffer_feedback_suggestion:I = 0x7f0e0034

.field public static final anythink_myoffer_feedback_text:I = 0x7f0e0035

.field public static final anythink_myoffer_feedback_video_freeze:I = 0x7f0e0036

.field public static final anythink_myoffer_feedback_violation_of_laws:I = 0x7f0e0037

.field public static final anythink_myoffer_feedback_vulgar_porn:I = 0x7f0e0038

.field public static final anythink_myoffer_splash_skip_text:I = 0x7f0e0039

.field public static final anythink_plugin_splash_skip_text:I = 0x7f0e003a

.field public static final app_name:I = 0x7f0e003b

.field public static final appbar_scrolling_view_behavior:I = 0x7f0e003c

.field public static final bottom_sheet_behavior:I = 0x7f0e003d

.field public static final character_counter_content_description:I = 0x7f0e003e

.field public static final character_counter_pattern:I = 0x7f0e003f

.field public static final fab_transformation_scrim_behavior:I = 0x7f0e0040

.field public static final fab_transformation_sheet_behavior:I = 0x7f0e0041

.field public static final game_view_content_description:I = 0x7f0e0042

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f0e0043

.field public static final known_staff:I = 0x7f0e0044

.field public static final ksad_ad_default_author:I = 0x7f0e0045

.field public static final ksad_ad_default_username:I = 0x7f0e0046

.field public static final ksad_ad_default_username_normal:I = 0x7f0e0047

.field public static final ksad_ad_function_disable:I = 0x7f0e0048

.field public static final ksad_click_to_next_video:I = 0x7f0e0049

.field public static final ksad_data_error_toast:I = 0x7f0e004a

.field public static final ksad_default_no_more_tip_or_toast_txt:I = 0x7f0e004b

.field public static final ksad_entry_tab_like_format:I = 0x7f0e004c

.field public static final ksad_half_page_loading_error_tip:I = 0x7f0e004d

.field public static final ksad_half_page_loading_no_comment_tip:I = 0x7f0e004e

.field public static final ksad_half_page_loading_no_related_tip:I = 0x7f0e004f

.field public static final ksad_install_tips:I = 0x7f0e0050

.field public static final ksad_look_related_button:I = 0x7f0e0051

.field public static final ksad_look_related_title:I = 0x7f0e0052

.field public static final ksad_network_dataFlow_tip:I = 0x7f0e0053

.field public static final ksad_network_error_toast:I = 0x7f0e0054

.field public static final ksad_page_load_more_tip:I = 0x7f0e0055

.field public static final ksad_page_load_no_more_tip:I = 0x7f0e0056

.field public static final ksad_page_loading_data_error_sub_title:I = 0x7f0e0057

.field public static final ksad_page_loading_data_error_title:I = 0x7f0e0058

.field public static final ksad_page_loading_data_limit_error_title:I = 0x7f0e0059

.field public static final ksad_page_loading_error_retry:I = 0x7f0e005a

.field public static final ksad_page_loading_network_error_sub_title:I = 0x7f0e005b

.field public static final ksad_page_loading_network_error_title:I = 0x7f0e005c

.field public static final ksad_photo_hot_enter_label_text:I = 0x7f0e005d

.field public static final ksad_photo_hot_enter_watch_count_format:I = 0x7f0e005e

.field public static final ksad_photo_hot_enter_watch_extra_button_format:I = 0x7f0e005f

.field public static final ksad_photo_hot_enter_watch_extra_button_format_v2:I = 0x7f0e0060

.field public static final ksad_photo_hot_scroll_more_hot_label:I = 0x7f0e0061

.field public static final ksad_reward_default_tip:I = 0x7f0e0062

.field public static final ksad_reward_success_tip:I = 0x7f0e0063

.field public static final ksad_slide_left_tips:I = 0x7f0e0064

.field public static final ksad_slide_up_tips:I = 0x7f0e0065

.field public static final ksad_text_placeholder:I = 0x7f0e0066

.field public static final ksad_trend_is_no_valid:I = 0x7f0e0067

.field public static final ksad_trend_list_item_photo_count_format:I = 0x7f0e0068

.field public static final ksad_trend_list_panel_title:I = 0x7f0e0069

.field public static final ksad_trend_title_info_format:I = 0x7f0e006a

.field public static final ksad_tube_author_name_label_text:I = 0x7f0e006b

.field public static final ksad_tube_enter_paly_count:I = 0x7f0e006c

.field public static final ksad_tube_episode_index:I = 0x7f0e006d

.field public static final ksad_tube_hot_list_label_string:I = 0x7f0e006e

.field public static final ksad_tube_more_episode:I = 0x7f0e006f

.field public static final ksad_tube_update_default:I = 0x7f0e0070

.field public static final ksad_tube_update_finished_format_text:I = 0x7f0e0071

.field public static final ksad_tube_update_unfinished_format_text:I = 0x7f0e0072

.field public static final ksad_video_no_found:I = 0x7f0e0073

.field public static final ksad_watch_next_video:I = 0x7f0e0074

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f0e0075

.field public static final password_toggle_content_description:I = 0x7f0e0076

.field public static final path_password_eye:I = 0x7f0e0077

.field public static final path_password_eye_mask_strike_through:I = 0x7f0e0078

.field public static final path_password_eye_mask_visible:I = 0x7f0e0079

.field public static final path_password_strike_through:I = 0x7f0e007a

.field public static final search_menu_title:I = 0x7f0e007b

.field public static final status_bar_notification_info_overflow:I = 0x7f0e007c

.field public static final tt_00_00:I = 0x7f0e007d

.field public static final tt_ad:I = 0x7f0e007e

.field public static final tt_ad_logo_txt:I = 0x7f0e007f

.field public static final tt_app_name:I = 0x7f0e0080

.field public static final tt_app_privacy_dialog_title:I = 0x7f0e0081

.field public static final tt_appdownloader_button_cancel_download:I = 0x7f0e0082

.field public static final tt_appdownloader_button_queue_for_wifi:I = 0x7f0e0083

.field public static final tt_appdownloader_button_start_now:I = 0x7f0e0084

.field public static final tt_appdownloader_download_percent:I = 0x7f0e0085

.field public static final tt_appdownloader_download_remaining:I = 0x7f0e0086

.field public static final tt_appdownloader_download_unknown_title:I = 0x7f0e0087

.field public static final tt_appdownloader_duration_hours:I = 0x7f0e0088

.field public static final tt_appdownloader_duration_minutes:I = 0x7f0e0089

.field public static final tt_appdownloader_duration_seconds:I = 0x7f0e008a

.field public static final tt_appdownloader_jump_unknown_source:I = 0x7f0e008b

.field public static final tt_appdownloader_label_cancel:I = 0x7f0e008c

.field public static final tt_appdownloader_label_cancel_directly:I = 0x7f0e008d

.field public static final tt_appdownloader_label_ok:I = 0x7f0e008e

.field public static final tt_appdownloader_label_reserve_wifi:I = 0x7f0e008f

.field public static final tt_appdownloader_notification_download:I = 0x7f0e0090

.field public static final tt_appdownloader_notification_download_complete_open:I = 0x7f0e0091

.field public static final tt_appdownloader_notification_download_complete_with_install:I = 0x7f0e0092

.field public static final tt_appdownloader_notification_download_complete_without_install:I = 0x7f0e0093

.field public static final tt_appdownloader_notification_download_continue:I = 0x7f0e0094

.field public static final tt_appdownloader_notification_download_delete:I = 0x7f0e0095

.field public static final tt_appdownloader_notification_download_failed:I = 0x7f0e0096

.field public static final tt_appdownloader_notification_download_install:I = 0x7f0e0097

.field public static final tt_appdownloader_notification_download_open:I = 0x7f0e0098

.field public static final tt_appdownloader_notification_download_pause:I = 0x7f0e0099

.field public static final tt_appdownloader_notification_download_restart:I = 0x7f0e009a

.field public static final tt_appdownloader_notification_download_resume:I = 0x7f0e009b

.field public static final tt_appdownloader_notification_download_space_failed:I = 0x7f0e009c

.field public static final tt_appdownloader_notification_download_waiting_net:I = 0x7f0e009d

.field public static final tt_appdownloader_notification_download_waiting_wifi:I = 0x7f0e009e

.field public static final tt_appdownloader_notification_downloading:I = 0x7f0e009f

.field public static final tt_appdownloader_notification_install_finished_open:I = 0x7f0e00a0

.field public static final tt_appdownloader_notification_insufficient_space_error:I = 0x7f0e00a1

.field public static final tt_appdownloader_notification_need_wifi_for_size:I = 0x7f0e00a2

.field public static final tt_appdownloader_notification_no_internet_error:I = 0x7f0e00a3

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I = 0x7f0e00a4

.field public static final tt_appdownloader_notification_paused_in_background:I = 0x7f0e00a5

.field public static final tt_appdownloader_notification_pausing:I = 0x7f0e00a6

.field public static final tt_appdownloader_notification_prepare:I = 0x7f0e00a7

.field public static final tt_appdownloader_notification_request_btn_no:I = 0x7f0e00a8

.field public static final tt_appdownloader_notification_request_btn_yes:I = 0x7f0e00a9

.field public static final tt_appdownloader_notification_request_message:I = 0x7f0e00aa

.field public static final tt_appdownloader_notification_request_title:I = 0x7f0e00ab

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I = 0x7f0e00ac

.field public static final tt_appdownloader_resume_in_wifi:I = 0x7f0e00ad

.field public static final tt_appdownloader_tip:I = 0x7f0e00ae

.field public static final tt_appdownloader_wifi_recommended_body:I = 0x7f0e00af

.field public static final tt_appdownloader_wifi_recommended_title:I = 0x7f0e00b0

.field public static final tt_appdownloader_wifi_required_body:I = 0x7f0e00b1

.field public static final tt_appdownloader_wifi_required_title:I = 0x7f0e00b2

.field public static final tt_auto_play_cancel_text:I = 0x7f0e00b3

.field public static final tt_cancel:I = 0x7f0e00b4

.field public static final tt_click_replay:I = 0x7f0e00b5

.field public static final tt_comment_num:I = 0x7f0e00b6

.field public static final tt_comment_num_backup:I = 0x7f0e00b7

.field public static final tt_comment_score:I = 0x7f0e00b8

.field public static final tt_common_download_app_detail:I = 0x7f0e00b9

.field public static final tt_common_download_app_privacy:I = 0x7f0e00ba

.field public static final tt_common_download_cancel:I = 0x7f0e00bb

.field public static final tt_confirm_download:I = 0x7f0e00bc

.field public static final tt_confirm_download_have_app_name:I = 0x7f0e00bd

.field public static final tt_dislike_comment_hint:I = 0x7f0e00be

.field public static final tt_dislike_feedback_repeat:I = 0x7f0e00bf

.field public static final tt_dislike_feedback_success:I = 0x7f0e00c0

.field public static final tt_dislike_header_tv_back:I = 0x7f0e00c1

.field public static final tt_dislike_header_tv_title:I = 0x7f0e00c2

.field public static final tt_dislike_other_suggest:I = 0x7f0e00c3

.field public static final tt_dislike_other_suggest_out:I = 0x7f0e00c4

.field public static final tt_dislike_submit:I = 0x7f0e00c5

.field public static final tt_download:I = 0x7f0e00c6

.field public static final tt_download_finish:I = 0x7f0e00c7

.field public static final tt_feedback:I = 0x7f0e00c8

.field public static final tt_full_screen_skip_tx:I = 0x7f0e00c9

.field public static final tt_image_download_apk:I = 0x7f0e00ca

.field public static final tt_install:I = 0x7f0e00cb

.field public static final tt_label_cancel:I = 0x7f0e00cc

.field public static final tt_label_ok:I = 0x7f0e00cd

.field public static final tt_no_network:I = 0x7f0e00ce

.field public static final tt_open_app_detail_developer:I = 0x7f0e00cf

.field public static final tt_open_app_detail_privacy:I = 0x7f0e00d0

.field public static final tt_open_app_detail_privacy_list:I = 0x7f0e00d1

.field public static final tt_open_app_name:I = 0x7f0e00d2

.field public static final tt_open_app_version:I = 0x7f0e00d3

.field public static final tt_open_landing_page_app_name:I = 0x7f0e00d4

.field public static final tt_permission_denied:I = 0x7f0e00d5

.field public static final tt_playable_btn_play:I = 0x7f0e00d6

.field public static final tt_quit:I = 0x7f0e00d7

.field public static final tt_request_permission_descript_external_storage:I = 0x7f0e00d8

.field public static final tt_request_permission_descript_location:I = 0x7f0e00d9

.field public static final tt_request_permission_descript_read_phone_state:I = 0x7f0e00da

.field public static final tt_reward_feedback:I = 0x7f0e00db

.field public static final tt_reward_screen_skip_tx:I = 0x7f0e00dc

.field public static final tt_splash_backup_ad_btn:I = 0x7f0e00dd

.field public static final tt_splash_backup_ad_title:I = 0x7f0e00de

.field public static final tt_splash_click_bar_text:I = 0x7f0e00df

.field public static final tt_splash_skip_tv_text:I = 0x7f0e00e0

.field public static final tt_tip:I = 0x7f0e00e1

.field public static final tt_unlike:I = 0x7f0e00e2

.field public static final tt_video_bytesize:I = 0x7f0e00e3

.field public static final tt_video_bytesize_M:I = 0x7f0e00e4

.field public static final tt_video_bytesize_MB:I = 0x7f0e00e5

.field public static final tt_video_continue_play:I = 0x7f0e00e6

.field public static final tt_video_dial_phone:I = 0x7f0e00e7

.field public static final tt_video_dial_replay:I = 0x7f0e00e8

.field public static final tt_video_download_apk:I = 0x7f0e00e9

.field public static final tt_video_mobile_go_detail:I = 0x7f0e00ea

.field public static final tt_video_retry_des_txt:I = 0x7f0e00eb

.field public static final tt_video_without_wifi_tips:I = 0x7f0e00ec

.field public static final tt_web_title_default:I = 0x7f0e00ed

.field public static final tt_will_play:I = 0x7f0e00ee


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
