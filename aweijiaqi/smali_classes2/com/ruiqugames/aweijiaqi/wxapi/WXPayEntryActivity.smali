.class public Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;
.super Landroid/app/Activity;
.source "WXPayEntryActivity.java"

# interfaces
.implements Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;


# static fields
.field static handle:Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;

.field static saveIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static InitHandle()V
    .locals 3

    .line 43
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-eqz v0, :cond_0

    const-string v0, "kill_virus"

    const-string v1, "InitHandle, errCode = "

    .line 44
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    sget-object v1, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->saveIntent:Landroid/content/Intent;

    sget-object v2, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->handle:Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;

    invoke-interface {v0, v1, v2}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    sput-object p0, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->handle:Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;

    .line 25
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    sput-object p1, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->saveIntent:Landroid/content/Intent;

    .line 26
    sget-object p1, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-eqz p1, :cond_0

    .line 27
    sget-object p1, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 34
    sput-object p1, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->saveIntent:Landroid/content/Intent;

    .line 35
    sput-object p0, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->handle:Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;

    .line 36
    invoke-virtual {p0, p1}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->setIntent(Landroid/content/Intent;)V

    .line 37
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->payApi:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0, p1, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    :cond_0
    return-void
.end method

.method public onReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)V
    .locals 0

    .line 51
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->finish()V

    return-void
.end method

.method public onResp(Lcom/tencent/mm/opensdk/modelbase/BaseResp;)V
    .locals 2

    .line 56
    invoke-virtual {p1}, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 58
    iget p1, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    const/4 v0, -0x2

    if-eq p1, v0, :cond_2

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_0

    const-string p1, "\u672a\u77e5\u9519\u8bef\uff0c\u53d1\u9001\u8fd4\ufffd???????"

    goto :goto_0

    :cond_0
    const-string p1, "\u652f\u4ed8\u6210\u529f"

    goto :goto_0

    :cond_1
    const-string p1, "\u652f\u4ed8\u5931\u8d25"

    goto :goto_0

    :cond_2
    const-string p1, "\u7528\u6237\u53d6\u6d88\u652f\u4ed8"

    :goto_0
    const/4 v0, 0x1

    .line 73
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 76
    :cond_3
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXPayEntryActivity;->finish()V

    return-void
.end method
