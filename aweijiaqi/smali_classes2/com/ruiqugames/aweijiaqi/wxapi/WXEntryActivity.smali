.class public Lcom/ruiqugames/aweijiaqi/wxapi/WXEntryActivity;
.super Landroid/app/Activity;
.source "WXEntryActivity.java"

# interfaces
.implements Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 24
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "WXEntryActivity------->onCreate:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    sget-object p1, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-nez p1, :cond_0

    .line 27
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/MainActivity;->cancelWaiting()V

    goto :goto_0

    .line 30
    :cond_0
    sget-object p1, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    .line 32
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/MainActivity;->cancelWaiting()V

    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 44
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "WXEntryActivity------->onDestroy:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 45
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 50
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "WXEntryActivity------->onNewIntent:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/ruiqugames/aweijiaqi/wxapi/WXEntryActivity;->setIntent(Landroid/content/Intent;)V

    .line 54
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0, p1, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    return-void
.end method

.method public onReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)V
    .locals 1

    .line 62
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v0, "WXEntryActivity------->onReq:"

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXEntryActivity;->finish()V

    return-void
.end method

.method public onResp(Lcom/tencent/mm/opensdk/modelbase/BaseResp;)V
    .locals 8

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onResp type = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "kill_virus"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onResp errCode = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {p1}, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->getType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget v0, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    if-nez v0, :cond_0

    .line 77
    move-object v0, p1

    check-cast v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Resp;

    iget-object v3, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Resp;->code:Ljava/lang/String;

    .line 78
    iget p1, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    invoke-static {p1, v3}, Lcom/ruiqugames/aweijiaqi/WXUtility;->onLoginWX(ILjava/lang/String;)V

    .line 81
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v3, "onResp code = "

    invoke-direct {p1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Resp;->code:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "\u6388\u6743\u6210\u529f"

    .line 83
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->getType()I

    move-result v0

    const-string v1, "\u7528\u6237\u53d6\u6d88"

    const-string v3, "\u672a\u77e5\u9519\u8bef\uff0c\u53d1\u9001\u8fd4\ufffd???????"

    const-string v4, "\u53d1\ufffd?\ufffd\u88ab\u62d2\u7edd"

    const/4 v5, -0x2

    const/4 v6, -0x4

    if-ne v0, v2, :cond_3

    .line 87
    iget v0, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    if-eq v0, v6, :cond_1

    if-eq v0, v5, :cond_2

    move-object v1, v3

    goto :goto_0

    :cond_1
    move-object v1, v4

    .line 98
    :cond_2
    :goto_0
    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 99
    iget p1, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    const-string v0, ""

    invoke-static {p1, v0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->onLoginWX(ILjava/lang/String;)V

    goto :goto_2

    .line 101
    :cond_3
    invoke-virtual {p1}, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->getType()I

    move-result v0

    const/4 v7, 0x2

    if-ne v0, v7, :cond_7

    iget v0, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    if-nez v0, :cond_7

    .line 102
    iget v0, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    const-string v7, "Wechatshare"

    invoke-static {v0, v7}, Lcom/ruiqugames/aweijiaqi/WXUtility;->onShareWX(ILjava/lang/String;)V

    .line 103
    iget p1, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    if-eq p1, v6, :cond_5

    if-eq p1, v5, :cond_6

    if-eqz p1, :cond_4

    move-object v1, v3

    goto :goto_1

    :cond_4
    const-string v1, "\u5206\u4eab\u6210\u529f"

    goto :goto_1

    :cond_5
    move-object v1, v4

    .line 117
    :cond_6
    :goto_1
    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 120
    :cond_7
    :goto_2
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/wxapi/WXEntryActivity;->finish()V

    return-void
.end method

.method public onStop()V
    .locals 2

    .line 38
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "WXEntryActivity------->onStop:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 39
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
