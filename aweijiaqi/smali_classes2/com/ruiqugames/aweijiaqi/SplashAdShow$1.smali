.class Lcom/ruiqugames/aweijiaqi/SplashAdShow$1;
.super Ljava/lang/Object;
.source "SplashAdShow.java"

# interfaces
.implements Lcom/anythink/splashad/api/ATSplashAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ruiqugames/aweijiaqi/SplashAdShow;->showSplash(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClick(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onAdClick:\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/core/api/ATAdInfo;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SplashAdShowActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onAdDismiss(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 2

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onAdDismiss:\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/core/api/ATAdInfo;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SplashAdShowActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->jumpToMainActivity()V

    return-void
.end method

.method public onAdLoaded()V
    .locals 3

    const-string v0, "SplashAdShowActivity"

    const-string v1, "onAdLoaded---------"

    .line 88
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    .line 89
    sput-boolean v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->isShow:Z

    .line 90
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->view:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    .line 93
    :cond_0
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->view:Landroid/view/View;

    sget v1, Lcom/ruiqugames/aweijiaqi/R$id;->splash_ad_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 94
    sget-object v1, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->splashAd:Lcom/anythink/splashad/api/ATSplashAd;

    sget-object v2, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v1, v2, v0}, Lcom/anythink/splashad/api/ATSplashAd;->show(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public onAdShow(Lcom/anythink/core/api/ATAdInfo;)V
    .locals 1

    const/4 p1, 0x1

    .line 106
    sput-boolean p1, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->isShow:Z

    const-string p1, "SplashAdShowActivity"

    const-string v0, "onAdShow:\n"

    .line 107
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onNoAdError(Lcom/anythink/core/api/AdError;)V
    .locals 2

    const/4 v0, 0x1

    .line 99
    sput-boolean v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->isShow:Z

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNoAdError---------:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/anythink/core/api/AdError;->printStackTrace()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SplashAdShowActivity"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->jumpToMainActivity()V

    return-void
.end method
