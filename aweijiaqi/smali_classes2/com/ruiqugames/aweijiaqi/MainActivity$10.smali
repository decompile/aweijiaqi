.class Lcom/ruiqugames/aweijiaqi/MainActivity$10;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lnet/security/device/api/SecurityInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ruiqugames/aweijiaqi/MainActivity;->GetSafeSession()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;


# direct methods
.method constructor <init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
    .locals 0

    .line 1290
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$10;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitFinish(I)V
    .locals 2

    const-string v0, "AliyunDevice"

    const/16 v1, 0x2710

    if-eq v1, p1, :cond_0

    const-string v1, "\u521d\u59cb\u5316\u5931\u8d25\uff0c\u7ee7\u7eed\u8c03\u7528getSession\u83b7\u53d6\u7684\u7ed3\u679c\u662f\u65e0\u6548\ufffd???????."

    .line 1294
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "errorCode"

    .line 1297
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1299
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$10;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v1, "OnAliCallBack"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1303
    :cond_0
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$10;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->access$0(Lcom/ruiqugames/aweijiaqi/MainActivity;Z)V

    const-string p1, "\u521d\u59cb\u5316\u6210\ufffd???????."

    .line 1304
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    iget-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$10;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->GetAliSession()V

    :catch_0
    :goto_0
    return-void
.end method
