.class public Lcom/ruiqugames/aweijiaqi/MainActivity;
.super Lcom/unity3d/player/UnityPlayerActivity;
.source "MainActivity.java"


# static fields
.field private static final APPLY_LAUCH_PERMISSION_RESPONSE:I = 0x2

.field private static final APPLY_PERMISSION_RESPONSE:I = 0x1

.field public static IsShowSplash:Z = true

.field private static TAG:Ljava/lang/String; = "JAQ"

.field private static VMP_SIGN_WITH_GENERAL_WUA2:I = 0x3

.field public static _mainContext:Landroid/content/Context;

.field static _progress:Landroid/app/ProgressDialog;

.field public static aliMsg:Ljava/lang/String;

.field public static app:Lcom/ruiqugames/aweijiaqi/MainActivity;

.field public static container:Landroid/view/ViewGroup;

.field public static goName:Ljava/lang/String;


# instance fields
.field private RoomID:I

.field private _gameObjectName:Ljava/lang/String;

.field files:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private instance:Lcom/alibaba/wireless/security/open/avmp/IAVMPGenericComponent$IAVMPGenericInstance;

.field private isAliInit:Z

.field private jaqVMPComp:Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

.field private mHandler:Landroid/os/Handler;

.field stringItem:[Ljava/lang/String;

.field wakeUpAdapter:Lcom/fm/openinstall/listener/AppWakeUpAdapter;

.field public wxAppId:Ljava/lang/String;

.field public wxPartner:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 80
    invoke-direct {p0}, Lcom/unity3d/player/UnityPlayerActivity;-><init>()V

    const/4 v0, 0x0

    .line 82
    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->instance:Lcom/alibaba/wireless/security/open/avmp/IAVMPGenericComponent$IAVMPGenericInstance;

    .line 83
    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->jaqVMPComp:Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    .line 90
    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_gameObjectName:Ljava/lang/String;

    const/4 v0, 0x0

    .line 92
    iput v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I

    const-string v1, ""

    .line 95
    iput-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    .line 96
    iput-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxPartner:Ljava/lang/String;

    .line 106
    iput-boolean v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->isAliInit:Z

    .line 401
    new-instance v0, Lcom/ruiqugames/aweijiaqi/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$1;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wakeUpAdapter:Lcom/fm/openinstall/listener/AppWakeUpAdapter;

    .line 582
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->files:Ljava/util/ArrayList;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 583
    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->stringItem:[Ljava/lang/String;

    .line 810
    new-instance v0, Lcom/ruiqugames/aweijiaqi/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$2;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public static final GetFileFromLocal(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .line 579
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private GetManifestMetaData(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 290
    :try_start_0
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v1, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 291
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 293
    invoke-virtual {p1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public static MADInitFullVideo(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public static MADIsFullVideoLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static MADIsFullVideoReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static MADLoadFullVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public static MADShowFullVideo(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public static MADVideoCheckSwitch(ZZ)V
    .locals 0

    return-void
.end method

.method static synthetic access$0(Lcom/ruiqugames/aweijiaqi/MainActivity;Z)V
    .locals 0

    .line 106
    iput-boolean p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->isAliInit:Z

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .line 84
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private aliPay(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method private declared-synchronized avmpSign(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    .line 1101
    :try_start_0
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->initAVMP()Z

    move-result v1
    :try_end_0
    .catch Lcom/alibaba/wireless/security/jaq/JAQException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1102
    monitor-exit p0

    return-object v0

    .line 1104
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->jaqVMPComp:Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    sget v2, Lcom/ruiqugames/aweijiaqi/MainActivity;->VMP_SIGN_WITH_GENERAL_WUA2:I

    const-string v3, "UTF-8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;->avmpSign(I[B)[B

    move-result-object p1

    .line 1105
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Lcom/alibaba/wireless/security/jaq/JAQException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1107
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 1111
    :catch_0
    :try_start_2
    sget-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "UnsupportedEncodingException exception error !!!"

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception p1

    .line 1109
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "avmp sign failed with errorCode="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/alibaba/wireless/security/jaq/JAQException;->getErrorCode()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1113
    :goto_0
    monitor-exit p0

    return-object v0

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public static bmpToByteArray(Landroid/graphics/Bitmap;I)[B
    .locals 6

    mul-int/lit16 p1, p1, 0x400

    .line 725
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 726
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    const/16 v3, 0xb4

    if-le v1, v3, :cond_0

    const/high16 v0, 0x43340000    # 180.0f

    mul-float v2, v2, v0

    float-to-int v0, v2

    const/16 v1, 0xb4

    .line 733
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 734
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 735
    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v5, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v0, 0x0

    invoke-virtual {v3, p0, v0, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 736
    new-instance p0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 737
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v2, v0, v1, p0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    const/16 v0, 0x5b

    .line 739
    :goto_0
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    array-length v1, v1

    if-le v1, p1, :cond_2

    if-gtz v0, :cond_1

    goto :goto_1

    .line 740
    :cond_1
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 741
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2, v1, v0, p0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    add-int/lit8 v0, v0, -0xa

    goto :goto_0

    .line 744
    :cond_2
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "bmpToByteArray-----> quality:"

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "kill_virus"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    .line 747
    :try_start_0
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object p1
.end method

.method public static bmpToByteArray(Landroid/graphics/Bitmap;ZI)[B
    .locals 6

    .line 759
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-le p2, v0, :cond_0

    .line 760
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    .line 761
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0

    .line 763
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    .line 764
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 767
    :goto_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 768
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 771
    :goto_1
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v4, p2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v4, v4, p2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 p2, 0x0

    invoke-virtual {v2, p0, v3, v5, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    if-eqz p1, :cond_1

    .line 773
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 774
    :cond_1
    new-instance p2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 775
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v0, v3, p2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 776
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 777
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 779
    :try_start_0
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 784
    :catch_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    .line 785
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_1
.end method

.method private static buildTransaction(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    .line 790
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static cancelWaiting()V
    .locals 1

    .line 522
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_progress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 523
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private checkAndRequestPermission()V
    .locals 7

    .line 301
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getAllPermissionCallbck()V

    return-void

    .line 307
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 309
    :try_start_0
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/unity3d/player/UnityPlayer;->currentActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    const/4 v2, 0x1

    :goto_0
    const/16 v3, 0x14

    if-le v2, v3, :cond_1

    goto :goto_1

    .line 312
    :cond_1
    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "permission"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    const-string v4, "crazyducklog"

    .line 317
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u8bf7\u6c42\u6743\u9650\ufffd??????????? "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-static {p0, v3}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_3

    .line 319
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 323
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 326
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 327
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getAllPermissionCallbck()V

    goto :goto_2

    .line 329
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 330
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const/4 v0, 0x2

    .line 331
    invoke-static {p0, v1, v0}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    :goto_2
    return-void
.end method

.method private getAllPermissionCallbck()V
    .locals 4

    .line 205
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_mainContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ttcsj_isshowsplash"

    const/4 v2, 0x1

    .line 206
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->IsShowSplash:Z

    if-eqz v0, :cond_0

    const-string v0, "crazyducklog"

    const-string v1, "getAllPermissionCallbck"

    .line 209
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Timer2"

    .line 210
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 214
    new-instance v1, Lcom/ruiqugames/aweijiaqi/MainActivity$4;

    invoke-direct {v1, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$4;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    return-void
.end method

.method public static getUnityGameobjectName()Ljava/lang/String;
    .locals 1

    .line 466
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->goName:Ljava/lang/String;

    return-object v0
.end method

.method public static hasAllPermissionsGranted([I)Z
    .locals 5

    .line 279
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    aget v3, p0, v2

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private declared-synchronized initAVMP()Z
    .locals 4

    monitor-enter p0

    .line 1082
    :try_start_0
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->jaqVMPComp:Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    if-eqz v0, :cond_0

    .line 1083
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "AVMP instance has been initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/alibaba/wireless/security/jaq/JAQException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    .line 1084
    monitor-exit p0

    return v0

    .line 1086
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/alibaba/wireless/security/open/SecurityGuardManager;->getInstance(Landroid/content/Context;)Lcom/alibaba/wireless/security/open/SecurityGuardManager;

    move-result-object v0

    const-class v1, Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    invoke-virtual {v0, v1}, Lcom/alibaba/wireless/security/open/SecurityGuardManager;->getInterface(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->jaqVMPComp:Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;

    .line 1087
    invoke-interface {v0}, Lcom/alibaba/wireless/security/jaq/avmp/IJAQAVMPSignComponent;->initialize()Z

    move-result v0
    :try_end_1
    .catch Lcom/alibaba/wireless/security/jaq/JAQException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 1091
    :try_start_2
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "unkown exception has occured"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 1089
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "init failed with errorCode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/alibaba/wireless/security/jaq/JAQException;->getErrorCode()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    const/4 v0, 0x0

    .line 1094
    :goto_1
    monitor-exit p0

    return v0

    :goto_2
    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public AddCSJRewardVideo(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public AddGDTRewardVideo(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public GetAliSession()V
    .locals 1

    .line 1318
    new-instance v0, Lcom/ruiqugames/aweijiaqi/MainActivity$11;

    invoke-direct {v0, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$11;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    .line 1351
    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/MainActivity$11;->start()V

    return-void
.end method

.method public GetSafeSession()Ljava/lang/String;
    .locals 3

    .line 1287
    iget-boolean v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->isAliInit:Z

    if-eqz v0, :cond_0

    .line 1288
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->GetAliSession()V

    goto :goto_0

    .line 1290
    :cond_0
    invoke-static {}, Lnet/security/device/api/SecurityDevice;->getInstance()Lnet/security/device/api/SecurityDevice;

    move-result-object v0

    new-instance v1, Lcom/ruiqugames/aweijiaqi/MainActivity$10;

    invoke-direct {v1, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$10;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    const-string v2, "a169375db4a0a60dd6aaf0e6d2c8da0b"

    invoke-virtual {v0, p0, v2, v1}, Lnet/security/device/api/SecurityDevice;->init(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;)V

    .line 1311
    :goto_0
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->aliMsg:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method public GetShareParam()Ljava/lang/String;
    .locals 1

    .line 1042
    sget-object v0, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 1043
    sget-object v0, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public GetTaiJiDunIPAndPort(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public HideCSJBanner()V
    .locals 0

    return-void
.end method

.method public HideCSJFeed()V
    .locals 0

    return-void
.end method

.method public HideGDTBanner()V
    .locals 0

    return-void
.end method

.method public HideGDTFeed()V
    .locals 0

    return-void
.end method

.method public InitCSJ(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitCSJBannerExpress(Ljava/lang/String;III)V
    .locals 0

    return-void
.end method

.method public InitCSJFeed(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public InitCSJFullVideo(IIILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitCSJInteractionExpress(Ljava/lang/String;FF)V
    .locals 0

    return-void
.end method

.method public InitCSJRewardVideo(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitCSJRewardVideoCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitGDT(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitGDTBannerExpress(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitGDTFeed(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public InitGDTInteractionExpress(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitGDTRewardVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitKSVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public InitPaySdk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxAppId:Ljava/lang/String;

    .line 552
    iput-object p2, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wxPartner:Ljava/lang/String;

    .line 553
    invoke-static {p0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->PayInit(Landroid/content/Context;)V

    return-void
.end method

.method public InitWeChat()V
    .locals 0

    .line 547
    invoke-static {p0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->wxReInit(Landroid/content/Context;)V

    return-void
.end method

.method public LoadCSJFeed()V
    .locals 0

    return-void
.end method

.method public LoadGDTFeed()V
    .locals 0

    return-void
.end method

.method public LoadKSVideo(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public MADBannerIsReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADBannerLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADFeedGetHeight()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADFeedGetWidth()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADHideBanner()V
    .locals 0

    return-void
.end method

.method public MADInitBanner(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public MADInitFeed(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public MADInitInterAd(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public MADInitVideo(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public MADIsFeedLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADIsFeedReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADIsInterLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADIsInterReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADIsVideoLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADIsVideoReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public MADLoadBanner()V
    .locals 0

    return-void
.end method

.method public MADLoadFeed()V
    .locals 0

    return-void
.end method

.method public MADLoadInter()V
    .locals 0

    return-void
.end method

.method public MADLoadVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public MADRemoveBanner()V
    .locals 0

    return-void
.end method

.method public MADRemoveFeed()V
    .locals 0

    return-void
.end method

.method public MADReshowBanner()V
    .locals 0

    return-void
.end method

.method public MADShowBanner(I)V
    .locals 0

    return-void
.end method

.method public MADShowFeed(I)V
    .locals 0

    return-void
.end method

.method public MADShowInter()V
    .locals 0

    return-void
.end method

.method public MADShowVideo(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public OnRegister()V
    .locals 0

    return-void
.end method

.method public ReloadCSJInteractionExpress()V
    .locals 0

    return-void
.end method

.method public ReloadGDTInteractionExpress()V
    .locals 0

    return-void
.end method

.method public ReqNextCSJBanner(FF)V
    .locals 0

    return-void
.end method

.method public ReqNextGDTBanner()V
    .locals 0

    return-void
.end method

.method public SendCheckRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 1117
    invoke-direct {p0, p2}, Lcom/ruiqugames/aweijiaqi/MainActivity;->avmpSign(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1119
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u7b7e\u540d\u6210\u529f :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "send: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/ruiqugames/aweijiaqi/MainActivity$9;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/ruiqugames/aweijiaqi/MainActivity$9;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1160
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    .line 1122
    :cond_0
    sget-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->TAG:Ljava/lang/String;

    const-string p2, "\u7b7e\u540d\u5931\u8d25"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public SetPostUrl(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public SetSafeSwitch(Z)V
    .locals 2

    .line 1280
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_mainContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1281
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ali_isgetsession"

    .line 1282
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1283
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public ShareMutilPic(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 587
    sget-object v0, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->isWXAppInstalled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "\u60a8\u8fd8\u6ca1\u6709\u5b89\u88c5\u5fae\u4fe1"

    .line 588
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->stringItem:[Ljava/lang/String;

    aput-object p3, v0, v1

    const/4 p3, 0x1

    .line 592
    aput-object p4, v0, p3

    const/4 p3, 0x2

    .line 593
    aput-object p5, v0, p3

    const/4 p3, 0x3

    .line 594
    aput-object p6, v0, p3

    const/4 p3, 0x4

    .line 595
    aput-object p7, v0, p3

    const/4 p3, 0x5

    .line 596
    aput-object p8, v0, p3

    .line 598
    new-instance p3, Ljava/lang/Thread;

    new-instance p4, Lcom/ruiqugames/aweijiaqi/MainActivity$8;

    invoke-direct {p4, p0, p1, p2}, Lcom/ruiqugames/aweijiaqi/MainActivity$8;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;ILjava/lang/String;)V

    invoke-direct {p3, p4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 636
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void
.end method

.method public SharePictrue(Ljava/lang/String;ZIII)V
    .locals 2

    .line 673
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 674
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;

    invoke-direct {v0, p1}, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;-><init>(Landroid/graphics/Bitmap;)V

    .line 675
    new-instance v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {v1}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>()V

    .line 676
    iput-object v0, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->mediaObject:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;

    const/4 v0, 0x1

    .line 677
    invoke-static {p1, p3, p4, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p3

    .line 678
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 p1, 0x0

    const/16 p4, 0x64

    :goto_0
    if-gt p1, p5, :cond_0

    .line 686
    new-instance p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {p1}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string p3, "img"

    .line 687
    invoke-static {p3}, Lcom/ruiqugames/aweijiaqi/MainActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 688
    iput-object v1, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    .line 690
    iput p2, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    .line 692
    sget-object p2, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p2, p1}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    return-void

    .line 681
    :cond_0
    invoke-static {p3, v0, p4}, Lcom/ruiqugames/aweijiaqi/MainActivity;->bmpToByteArray(Landroid/graphics/Bitmap;ZI)[B

    move-result-object p1

    iput-object p1, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    .line 682
    iget-object p1, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    array-length p1, p1

    div-int/lit16 p1, p1, 0x400

    add-int/lit8 p4, p4, -0x1

    goto :goto_0
.end method

.method public ShareWebPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 699
    new-instance p4, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;

    invoke-direct {p4}, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;-><init>()V

    .line 700
    iput-object p1, p4, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;->webpageUrl:Ljava/lang/String;

    .line 701
    new-instance p1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {p1, p4}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>(Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;)V

    .line 702
    iput-object p2, p1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->title:Ljava/lang/String;

    .line 703
    iput-object p3, p1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->description:Ljava/lang/String;

    const-string p2, "ic_launcher"

    const-string p3, "drawable"

    .line 704
    invoke-static {p2, p3}, Lcom/iwodong/unityplugin/SystemUtilsOperation;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    .line 705
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-static {p3, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    const/16 p3, 0x1e

    .line 706
    invoke-static {p2, p3}, Lcom/ruiqugames/aweijiaqi/MainActivity;->bmpToByteArray(Landroid/graphics/Bitmap;I)[B

    move-result-object p2

    iput-object p2, p1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    .line 707
    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Unity thumbData.length = "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p3, p1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    array-length p3, p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "kill_virus"

    invoke-static {p3, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    new-instance p2, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {p2}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string p4, "webpage"

    .line 709
    invoke-static {p4}, Lcom/ruiqugames/aweijiaqi/MainActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    iput-object p4, p2, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 710
    iput-object p1, p2, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    .line 712
    iput p5, p2, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    const-string p1, "Unity sendReq "

    .line 713
    invoke-static {p3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    invoke-virtual {p2}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->checkArgs()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 716
    sget-object p1, Lcom/ruiqugames/aweijiaqi/WXUtility;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p1, p2}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    goto :goto_0

    :cond_0
    const-string p1, "Unity sendReq fail"

    .line 718
    invoke-static {p3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public ShowCSJBanner(FF)V
    .locals 0

    return-void
.end method

.method public ShowCSJFeed(FF)V
    .locals 0

    return-void
.end method

.method public ShowCSJFullVideo()V
    .locals 0

    return-void
.end method

.method public ShowCSJInteractionExpress()V
    .locals 0

    return-void
.end method

.method public ShowCSJRewardVideo()V
    .locals 0

    return-void
.end method

.method public ShowGDTBanner(FF)V
    .locals 0

    return-void
.end method

.method public ShowGDTFeed(FF)V
    .locals 0

    return-void
.end method

.method public ShowGDTInteractionExpress()V
    .locals 0

    return-void
.end method

.method public ShowGDTRewardVideo()V
    .locals 0

    return-void
.end method

.method public ShowKSVideo()V
    .locals 0

    return-void
.end method

.method public ShowSplashAd()V
    .locals 0

    .line 901
    invoke-static {p0}, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->showSplash(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    return-void
.end method

.method public callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_gameObjectName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_gameObjectName:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/unity3d/player/UnityPlayer;->UnitySendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p1, "kill_virus"

    const-string p2, "Unity\u5bf9\u8c61\u4e3a\u7a7a  \u9519\u8bef\u4ee3\u7801\ufffd???????????1"

    .line 490
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public clearRoomID()V
    .locals 1

    const/4 v0, 0x0

    .line 798
    iput v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I

    return-void
.end method

.method public decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 644
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 647
    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 648
    iput-boolean v1, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 650
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    .line 651
    invoke-static {v2, v3, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 652
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 655
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v4, 0x258

    if-gt v2, v4, :cond_0

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-le v2, v4, :cond_1

    :cond_0
    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    int-to-double v4, v4

    .line 657
    iget v6, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget p1, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v6, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    int-to-double v6, p1

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int p1, v4

    int-to-double v4, p1

    .line 656
    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-int v1, v1

    .line 661
    :cond_1
    new-instance p1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 662
    iput v1, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 663
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 664
    invoke-static {v1, v3, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 665
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    return-object p1
.end method

.method public getRoomID()I
    .locals 1

    .line 794
    iget v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I

    return v0
.end method

.method public hasPermission(Ljava/lang/String;)Z
    .locals 4

    .line 233
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 236
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    const/4 v3, 0x1

    if-ge v0, v2, :cond_1

    return v3

    .line 239
    :cond_1
    invoke-virtual {p0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_2

    return v3

    :cond_2
    return v1
.end method

.method public moveCSJBannerY(F)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public moveGDTBannerY(F)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 390
    invoke-super {p0, p1, p2, p3}, Lcom/unity3d/player/UnityPlayerActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    return-void

    .line 396
    :cond_1
    :goto_0
    invoke-static {p1, p2, p3}, Lcom/iwodong/unityplugin/PhotoPicker;->onPickedResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 110
    invoke-super {p0, p1}, Lcom/unity3d/player/UnityPlayerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    sget-object p1, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    if-eqz p1, :cond_0

    .line 112
    sget-object p1, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "OnGetShareParam"

    invoke-virtual {p0, v0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string p1, "csj"

    const-string v0, "mainoncreate"

    .line 117
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-static {p0}, Lcom/ruiqugames/aweijiaqi/WXUtility;->wxInit(Landroid/content/Context;)V

    .line 119
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wakeUpAdapter:Lcom/fm/openinstall/listener/AppWakeUpAdapter;

    invoke-static {p1, v0}, Lcom/fm/openinstall/OpenInstall;->getWakeUp(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)Z

    .line 120
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    .line 121
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    .line 123
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "MainActivity------->onCreate:"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 124
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "scheme:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    .line 137
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object p1

    const-string v2, "kill_virus"

    if-eqz p1, :cond_1

    .line 138
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x7

    if-lt v3, v4, :cond_1

    const-string v3, "="

    .line 139
    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 141
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "values.length:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    aget-object v4, v3, v0

    const-string v5, "roomid"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 144
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "roomID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    .line 147
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 151
    :cond_1
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RoomID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "queryString:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_2
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sput-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->_mainContext:Landroid/content/Context;

    .line 158
    sput-object p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->app:Lcom/ruiqugames/aweijiaqi/MainActivity;

    .line 159
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    sput-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->container:Landroid/view/ViewGroup;

    const-string p1, "BuglyAppId"

    .line 163
    invoke-direct {p0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->GetManifestMetaData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 166
    sget-object v2, Lcom/ruiqugames/aweijiaqi/MainActivity;->_mainContext:Landroid/content/Context;

    invoke-static {v2, p1, v0}, Lcom/tencent/bugly/crashreport/CrashReport;->initCrashReport(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 168
    :cond_3
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->checkAndRequestPermission()V

    .line 169
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->initAVMP()Z

    .line 171
    sget-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->_mainContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "ali_isgetsession"

    .line 173
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 177
    invoke-static {}, Lnet/security/device/api/SecurityDevice;->getInstance()Lnet/security/device/api/SecurityDevice;

    move-result-object p1

    new-instance v0, Lcom/ruiqugames/aweijiaqi/MainActivity$3;

    invoke-direct {v0, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$3;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    const-string v1, "a169375db4a0a60dd6aaf0e6d2c8da0b"

    invoke-virtual {p1, p0, v1, v0}, Lnet/security/device/api/SecurityDevice;->init(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;)V

    :cond_4
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 227
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "MainActivity------->onDestroy:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 228
    invoke-super {p0}, Lcom/unity3d/player/UnityPlayerActivity;->onDestroy()V

    const/4 v0, 0x0

    .line 229
    iput-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wakeUpAdapter:Lcom/fm/openinstall/listener/AppWakeUpAdapter;

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5

    .line 432
    iget-object v0, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->wakeUpAdapter:Lcom/fm/openinstall/listener/AppWakeUpAdapter;

    invoke-static {p1, v0}, Lcom/fm/openinstall/OpenInstall;->getWakeUp(Landroid/content/Intent;Lcom/fm/openinstall/listener/AppWakeUpListener;)Z

    .line 433
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 434
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    .line 435
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "MainActivity------->onNewIntent:"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 436
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "scheme:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 438
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object p1

    const-string v0, "kill_virus"

    if-eqz p1, :cond_0

    .line 439
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x7

    if-lt v1, v2, :cond_0

    const-string v1, "="

    .line 440
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 441
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const-string v3, "roomid"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "roomID:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    aget-object v4, v1, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->RoomID:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 446
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 450
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryString:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    const-string p2, "crazyducklog"

    const-string v0, "onRequestPermissionsResult"

    .line 337
    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 351
    :cond_0
    invoke-static {p3}, Lcom/ruiqugames/aweijiaqi/MainActivity;->hasAllPermissionsGranted([I)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 352
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x1d

    if-ge p1, p3, :cond_4

    const-string p1, "onRequestPermissionsResult 2"

    .line 354
    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-direct {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getAllPermissionCallbck()V

    goto :goto_0

    .line 360
    :cond_1
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string p2, "\u5e94\u7528\u7f3a\u5c11\u5fc5\u8981\u7684\u6743\u9650\uff01\u8bf7\u70b9\u51fb\"\u6743\u9650\"\uff0c\u6253\ufffd????????\u5fc5\u8981\u7684\u6743\ufffd????????"

    .line 361
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const p2, 0x1040014

    .line 362
    invoke-virtual {p1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const p2, 0x104000a

    .line 363
    new-instance p3, Lcom/ruiqugames/aweijiaqi/MainActivity$7;

    invoke-direct {p3, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$7;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    invoke-virtual {p1, p2, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 373
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 340
    :cond_2
    array-length p1, p3

    const-string p2, "OnRequestPermissiontResponse"

    if-lez p1, :cond_3

    const/4 p1, 0x0

    aget p1, p3, p1

    if-nez p1, :cond_3

    .line 341
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string p3, "onRequestPermissionsResult------->PERMISSION_GRANTED:"

    invoke-virtual {p1, p3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string p1, "0"

    .line 342
    invoke-virtual {p0, p2, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 344
    :cond_3
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string p3, "onRequestPermissionsResult------->PERMISSION_DENIED:"

    invoke-virtual {p1, p3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string p1, "1"

    .line 345
    invoke-virtual {p0, p2, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public preloadAD(II)V
    .locals 0

    return-void
.end method

.method public requestPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    move-object v0, p7

    const-string v1, "1"

    .line 568
    invoke-virtual {p7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2"

    .line 572
    invoke-virtual {p7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public requestPermission(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 243
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 246
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p2, "\u6ca1\u6709\u8be5\u6743\u9650\uff0c\u65e0\u6cd5\u4f7f\u7528\ufffd???????????\u8981\u6b64\u6743\u9650\u7684\u529f\ufffd???????????"

    .line 249
    :cond_1
    invoke-virtual {p0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->hasPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 250
    invoke-static {p0, p1}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 252
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const p2, 0x1040014

    .line 253
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const p2, 0x104000a

    .line 254
    new-instance v1, Lcom/ruiqugames/aweijiaqi/MainActivity$5;

    invoke-direct {v1, p0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity$5;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;Ljava/lang/String;)V

    invoke-virtual {v0, p2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 p1, 0x1040000

    .line 263
    new-instance p2, Lcom/ruiqugames/aweijiaqi/MainActivity$6;

    invoke-direct {p2, p0}, Lcom/ruiqugames/aweijiaqi/MainActivity$6;-><init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 269
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_2
    const/4 p2, 0x1

    new-array v0, p2, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 271
    invoke-static {p0, v0, p2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    :cond_3
    :goto_0
    return-void
.end method

.method public requestWXLogin()V
    .locals 1

    const-string v0, "\u5fae\u4fe1\u767b\u9646\u4e2d"

    .line 558
    invoke-virtual {p0, v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->startWating(Ljava/lang/String;)V

    .line 559
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/WXUtility;->wxLoginImpl()V

    return-void
.end method

.method public savePref(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setUnityGameObjectName(Ljava/lang/String;)V
    .locals 2

    .line 495
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_gameObjectName:Ljava/lang/String;

    .line 496
    sput-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->goName:Ljava/lang/String;

    .line 497
    sget-object p1, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    if-eqz p1, :cond_0

    .line 498
    sget-object p1, Lcom/ruiqugames/aweijiaqi/GameApplication;->json:Lorg/json/JSONObject;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "OnGetShareParam"

    invoke-virtual {p0, v0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_0
    sget-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->aliMsg:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 501
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "errorCode"

    const/4 v1, 0x0

    .line 503
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "session"

    .line 504
    sget-object v1, Lcom/ruiqugames/aweijiaqi/MainActivity;->aliMsg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "OnAliCallBack"

    .line 505
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 462
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public startWating(Ljava/lang/String;)V
    .locals 1

    .line 513
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_progress:Landroid/app/ProgressDialog;

    .line 514
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->_progress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 517
    :cond_0
    sget-object p1, Lcom/ruiqugames/aweijiaqi/MainActivity;->_progress:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
