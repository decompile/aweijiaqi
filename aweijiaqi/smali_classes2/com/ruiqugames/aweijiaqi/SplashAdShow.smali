.class public Lcom/ruiqugames/aweijiaqi/SplashAdShow;
.super Ljava/lang/Object;
.source "SplashAdShow.java"


# static fields
.field private static _parentView:Landroid/view/View; = null

.field private static container:Landroid/view/ViewGroup; = null

.field static hasHandleJump:Z = false

.field static isShow:Z = false

.field static mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

.field static splashAd:Lcom/anythink/splashad/api/ATSplashAd;

.field static view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static AddAdToView()V
    .locals 6

    .line 161
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    const/16 v3, 0x7d8

    const/4 v4, 0x0

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 168
    sget-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->app:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->_parentView:Landroid/view/View;

    .line 169
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 171
    check-cast v0, Landroid/view/ViewGroup;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->container:Landroid/view/ViewGroup;

    .line 174
    :cond_0
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

    new-instance v1, Lcom/ruiqugames/aweijiaqi/SplashAdShow$3;

    invoke-direct {v1}, Lcom/ruiqugames/aweijiaqi/SplashAdShow$3;-><init>()V

    invoke-virtual {v0, v1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$0()Landroid/view/ViewGroup;
    .locals 1

    .line 34
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public static jumpToMainActivity()V
    .locals 2

    .line 194
    sget-boolean v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->hasHandleJump:Z

    if-nez v0, :cond_0

    .line 195
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 197
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

    new-instance v1, Lcom/ruiqugames/aweijiaqi/SplashAdShow$4;

    invoke-direct {v1}, Lcom/ruiqugames/aweijiaqi/SplashAdShow$4;-><init>()V

    invoke-virtual {v0, v1}, Lcom/ruiqugames/aweijiaqi/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 210
    :cond_0
    sget-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->splashAd:Lcom/anythink/splashad/api/ATSplashAd;

    if-eqz v0, :cond_1

    .line 211
    invoke-virtual {v0}, Lcom/anythink/splashad/api/ATSplashAd;->onDestory()V

    :cond_1
    return-void
.end method

.method public static showSplash(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
    .locals 9

    .line 40
    sput-object p0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

    const-string v0, "layout_inflater"

    .line 41
    invoke-virtual {p0, v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 43
    sget v1, Lcom/ruiqugames/aweijiaqi/R$layout;->splash_ad_show:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->view:Landroid/view/View;

    .line 46
    sget v1, Lcom/ruiqugames/aweijiaqi/R$id;->splash_ad_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 47
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 49
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 50
    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v2, 0x6

    .line 54
    invoke-virtual {p0, v2}, Lcom/ruiqugames/aweijiaqi/MainActivity;->setRequestedOrientation(I)V

    .line 55
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v2, v2

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    mul-double v2, v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 56
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    const-wide v4, 0x3feb333333333333L    # 0.85

    const/4 v6, 0x7

    if-ne v2, v3, :cond_1

    .line 58
    invoke-virtual {p0, v6}, Lcom/ruiqugames/aweijiaqi/MainActivity;->setRequestedOrientation(I)V

    .line 59
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 60
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v2, v2

    mul-double v2, v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p0, v6}, Lcom/ruiqugames/aweijiaqi/MainActivity;->setRequestedOrientation(I)V

    .line 63
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 64
    invoke-virtual {p0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v2, v2

    mul-double v2, v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 74
    :goto_0
    new-instance v6, Lcom/anythink/network/toutiao/TTATRequestInfo;

    const/4 v1, 0x0

    const-string v2, "5180156"

    const-string v3, "887491020"

    invoke-direct {v6, v2, v3, v1}, Lcom/anythink/network/toutiao/TTATRequestInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v1, "551710"

    .line 75
    invoke-virtual {v6, v1}, Lcom/anythink/core/api/ATMediationRequestInfo;->setAdSourceId(Ljava/lang/String;)V

    .line 85
    new-instance v1, Lcom/anythink/splashad/api/ATSplashAd;

    new-instance v7, Lcom/ruiqugames/aweijiaqi/SplashAdShow$1;

    invoke-direct {v7}, Lcom/ruiqugames/aweijiaqi/SplashAdShow$1;-><init>()V

    const/16 v8, 0x1388

    const-string v5, "b60c87fb787f1a"

    move-object v3, v1

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/anythink/splashad/api/ATSplashAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/anythink/core/api/ATMediationRequestInfo;Lcom/anythink/splashad/api/ATSplashAdListener;I)V

    sput-object v1, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->splashAd:Lcom/anythink/splashad/api/ATSplashAd;

    .line 134
    invoke-virtual {v1}, Lcom/anythink/splashad/api/ATSplashAd;->isAdReady()Z

    move-result p0

    const-string v1, "SplashAd"

    if-eqz p0, :cond_2

    const-string p0, "SplashAd is ready to show."

    .line 135
    invoke-static {v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object p0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->splashAd:Lcom/anythink/splashad/api/ATSplashAd;

    sget-object v1, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->mainApp:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {p0, v1, v0}, Lcom/anythink/splashad/api/ATSplashAd;->show(Landroid/app/Activity;Landroid/view/ViewGroup;)V

    goto :goto_1

    :cond_2
    const-string p0, "SplashAd isn\'t ready to show, start to request."

    .line 138
    invoke-static {v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object p0, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->splashAd:Lcom/anythink/splashad/api/ATSplashAd;

    invoke-virtual {p0}, Lcom/anythink/splashad/api/ATSplashAd;->loadAd()V

    .line 141
    :goto_1
    invoke-static {}, Lcom/ruiqugames/aweijiaqi/SplashAdShow;->AddAdToView()V

    .line 143
    new-instance p0, Ljava/util/Timer;

    invoke-direct {p0}, Ljava/util/Timer;-><init>()V

    .line 144
    new-instance v0, Lcom/ruiqugames/aweijiaqi/SplashAdShow$2;

    invoke-direct {v0}, Lcom/ruiqugames/aweijiaqi/SplashAdShow$2;-><init>()V

    const-wide/16 v1, 0x1b58

    invoke-virtual {p0, v0, v1, v2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method
