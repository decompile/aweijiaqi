.class public Lcom/ruiqugames/aweijiaqi/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ALI_KEY:Ljava/lang/String; = "a169375db4a0a60dd6aaf0e6d2c8da0b"

.field public static final CSJ_SPLASH_ID:Ljava/lang/String; = "887491020"

.field public static final ERR_CODE_CANCEL:Ljava/lang/String; = "\u7528\u6237\u53d6\u6d88"

.field public static final ERR_CODE_DENY:Ljava/lang/String; = "\u53d1\ufffd?\ufffd\u88ab\u62d2\u7edd"

.field public static final ERR_CODE_PAY_COMMON:Ljava/lang/String; = "\u652f\u4ed8\u5931\u8d25"

.field public static final ERR_CODE_PAY_SUCCESS:Ljava/lang/String; = "\u652f\u4ed8\u6210\u529f"

.field public static final ERR_CODE_PAY_USER_CANCEL:Ljava/lang/String; = "\u7528\u6237\u53d6\u6d88\u652f\u4ed8"

.field public static final ERR_CODE_SHARE_SUCCESS:Ljava/lang/String; = "\u5206\u4eab\u6210\u529f"

.field public static final ERR_CODE_SUCCESS:Ljava/lang/String; = "\u6388\u6743\u6210\u529f"

.field public static final ERR_CODE_UNKNOWN:Ljava/lang/String; = "\u672a\u77e5\u9519\u8bef\uff0c\u53d1\u9001\u8fd4\ufffd???????"

.field public static final LOG_TAG:Ljava/lang/String; = "kill_virus"

.field public static final MAD_APPID:Ljava/lang/String; = "5180156"

.field public static final MAD_APP_NAME:Ljava/lang/String; = "\u963f\u4f1f\u7684\u5047\u671f3"

.field public static final MAD_SPLASH_ID:Ljava/lang/String; = "887491020"

.field public static final RANDOM_STR_LENGTH:I = 0x20

.field public static final SDK_AUTH_FLAG:I = 0x2

.field public static final SDK_PAY_FLAG:I = 0x1

.field public static final TOP_ON_APPID:Ljava/lang/String; = "a60c87f6a4d387"

.field public static final TOP_ON_APPKEY:Ljava/lang/String; = "3dcc97024a82d4f4eef24e0ff425302c"

.field public static final TOP_ON_CSJ_SPLASH_ID:Ljava/lang/String; = "887491020"

.field public static final TOP_ON_SPLASH_ID:Ljava/lang/String; = "b60c87fb787f1a"

.field public static final TOP_ON_SPLASH_SOURCE:Ljava/lang/String; = "551710"

.field public static final WX_APP_ID:Ljava/lang/String; = "wx145a41445a88580e"

.field public static final WX_CODE_LOGIN:Ljava/lang/String; = "\u5fae\u4fe1\u767b\u9646\u4e2d"

.field public static final WX_PAY_PACKAGE:Ljava/lang/String; = "Sign=WXPay"

.field public static final WX_PAY_PARTNER_ID:Ljava/lang/String; = "1487038752"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
