.class Lcom/ruiqugames/aweijiaqi/MainActivity$11;
.super Ljava/lang/Thread;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ruiqugames/aweijiaqi/MainActivity;->GetAliSession()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;


# direct methods
.method constructor <init>(Lcom/ruiqugames/aweijiaqi/MainActivity;)V
    .locals 0

    .line 1318
    iput-object p1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$11;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1321
    invoke-static {}, Lnet/security/device/api/SecurityDevice;->getInstance()Lnet/security/device/api/SecurityDevice;

    move-result-object v0

    invoke-virtual {v0}, Lnet/security/device/api/SecurityDevice;->getSession()Lnet/security/device/api/SecuritySession;

    move-result-object v0

    const-string v1, "AliyunDevice"

    if-eqz v0, :cond_1

    const/16 v2, 0x2710

    .line 1323
    iget v3, v0, Lnet/security/device/api/SecuritySession;->code:I

    const-string v4, "OnAliCallBack"

    const-string v5, "errorCode"

    if-ne v2, v3, :cond_0

    .line 1324
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "session: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lnet/security/device/api/SecuritySession;->session:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    iget-object v0, v0, Lnet/security/device/api/SecuritySession;->session:Ljava/lang/String;

    sput-object v0, Lcom/ruiqugames/aweijiaqi/MainActivity;->aliMsg:Ljava/lang/String;

    .line 1329
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x0

    .line 1331
    :try_start_0
    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "session"

    .line 1332
    sget-object v2, Lcom/ruiqugames/aweijiaqi/MainActivity;->aliMsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1333
    iget-object v1, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$11;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1338
    :cond_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1340
    :try_start_1
    iget v3, v0, Lnet/security/device/api/SecuritySession;->code:I

    invoke-virtual {v2, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1342
    iget-object v3, p0, Lcom/ruiqugames/aweijiaqi/MainActivity$11;->this$0:Lcom/ruiqugames/aweijiaqi/MainActivity;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/ruiqugames/aweijiaqi/MainActivity;->callUnityFunc(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1345
    :catch_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getSession error, code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lnet/security/device/api/SecuritySession;->code:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v0, "getSession is null."

    .line 1348
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :catch_1
    :goto_0
    return-void
.end method
