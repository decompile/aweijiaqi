.class final Lcom/tramini/plugin/a/g/a$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/tramini/plugin/a/g/a;->a(Lcom/tramini/plugin/b/a;Ljava/lang/String;Ljava/lang/String;Lcom/tramini/plugin/a/g/a$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/tramini/plugin/b/a;

.field final synthetic b:Lcom/tramini/plugin/a/g/a$a;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/tramini/plugin/b/a;Lcom/tramini/plugin/a/g/a$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/tramini/plugin/a/g/a$1;->a:Lcom/tramini/plugin/b/a;

    iput-object p2, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    iput-object p3, p0, Lcom/tramini/plugin/a/g/a$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    const/4 v0, 0x0

    .line 47
    :try_start_0
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->a:Lcom/tramini/plugin/b/a;

    invoke-virtual {v1}, Lcom/tramini/plugin/b/a;->e()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    if-nez v1, :cond_1

    .line 50
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    invoke-interface {v1, v0}, Lcom/tramini/plugin/a/g/a$a;->a(Lcom/tramini/plugin/a/c/a;)V

    :cond_0
    return-void

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/tramini/plugin/a/c/c;

    if-eqz v1, :cond_2

    .line 56
    iget-object v2, v1, Lcom/tramini/plugin/a/c/c;->d:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v2, ""

    .line 57
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 58
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    if-eqz v1, :cond_3

    .line 59
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    invoke-interface {v1, v0}, Lcom/tramini/plugin/a/g/a$a;->a(Lcom/tramini/plugin/a/c/a;)V

    :cond_3
    return-void

    .line 64
    :cond_4
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 72
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->c:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/16 v6, 0x32

    const/4 v7, 0x5

    const/4 v8, 0x4

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v11, 0x1

    if-eq v5, v6, :cond_a

    const/16 v6, 0x36

    if-eq v5, v6, :cond_9

    const/16 v6, 0x38

    if-eq v5, v6, :cond_8

    const/16 v6, 0x624

    if-eq v5, v6, :cond_7

    const/16 v6, 0x646

    if-eq v5, v6, :cond_6

    const/16 v6, 0x647

    if-eq v5, v6, :cond_5

    goto :goto_1

    :cond_5
    const-string v5, "29"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x5

    goto :goto_1

    :cond_6
    const-string v5, "28"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x4

    goto :goto_1

    :cond_7
    const-string v5, "15"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x3

    goto :goto_1

    :cond_8
    const-string v5, "8"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x2

    goto :goto_1

    :cond_9
    const-string v5, "6"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x1

    goto :goto_1

    :cond_a
    const-string v5, "2"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v4, 0x0

    :cond_b
    :goto_1
    if-eqz v4, :cond_11

    if-eq v4, v11, :cond_10

    if-eq v4, v10, :cond_f

    if-eq v4, v9, :cond_e

    if-eq v4, v8, :cond_d

    if-eq v4, v7, :cond_c

    goto :goto_2

    .line 95
    :cond_c
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1, v2}, Lcom/tramini/plugin/a/e/f;->a(Lorg/json/JSONObject;Lcom/tramini/plugin/a/c/c;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0

    goto :goto_2

    .line 92
    :cond_d
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1, v2}, Lcom/tramini/plugin/a/e/c;->a(Lorg/json/JSONObject;Lcom/tramini/plugin/a/c/c;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0

    goto :goto_2

    .line 89
    :cond_e
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1, v2}, Lcom/tramini/plugin/a/e/g;->a(Lorg/json/JSONObject;Lcom/tramini/plugin/a/c/c;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0

    goto :goto_2

    .line 86
    :cond_f
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1, v2}, Lcom/tramini/plugin/a/e/b;->a(Lorg/json/JSONObject;Lcom/tramini/plugin/a/c/c;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0

    goto :goto_2

    .line 83
    :cond_10
    iget-object v2, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1, v2}, Lcom/tramini/plugin/a/e/d;->a(Lorg/json/JSONObject;Lcom/tramini/plugin/a/c/c;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0

    goto :goto_2

    .line 80
    :cond_11
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->d:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/tramini/plugin/a/e/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    nop

    .line 101
    :goto_2
    iget-object v1, p0, Lcom/tramini/plugin/a/g/a$1;->b:Lcom/tramini/plugin/a/g/a$a;

    if-eqz v1, :cond_12

    .line 102
    invoke-interface {v1, v0}, Lcom/tramini/plugin/a/g/a$a;->a(Lcom/tramini/plugin/a/c/a;)V

    :cond_12
    return-void
.end method
