.class public final Lcom/tramini/plugin/a/g/a/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/tramini/plugin/a/g/a/a;


# instance fields
.field private b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/tramini/plugin/a/g/a/a;->b:Ljava/util/concurrent/ExecutorService;

    .line 29
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/tramini/plugin/a/g/a/a;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a()Lcom/tramini/plugin/a/g/a/a;
    .locals 1

    .line 33
    sget-object v0, Lcom/tramini/plugin/a/g/a/a;->a:Lcom/tramini/plugin/a/g/a/a;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/tramini/plugin/a/g/a/a;

    invoke-direct {v0}, Lcom/tramini/plugin/a/g/a/a;-><init>()V

    sput-object v0, Lcom/tramini/plugin/a/g/a/a;->a:Lcom/tramini/plugin/a/g/a/a;

    .line 36
    :cond_0
    sget-object v0, Lcom/tramini/plugin/a/g/a/a;->a:Lcom/tramini/plugin/a/g/a/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/tramini/plugin/a/g/a/b;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/tramini/plugin/a/g/a/a;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    const-wide/16 v0, 0x0

    .line 44
    invoke-virtual {p0, p1, v0, v1}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 3

    if-eqz p1, :cond_0

    .line 49
    new-instance v0, Lcom/tramini/plugin/a/g/a/a$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/tramini/plugin/a/g/a/a$1;-><init>(Lcom/tramini/plugin/a/g/a/a;JLjava/lang/Runnable;)V

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    const-wide/16 v1, 0x3e8

    div-long/2addr p1, v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/tramini/plugin/a/g/a/b;->a(I)V

    .line 62
    invoke-virtual {p0, v0}, Lcom/tramini/plugin/a/g/a/a;->a(Lcom/tramini/plugin/a/g/a/b;)V

    :cond_0
    return-void
.end method
