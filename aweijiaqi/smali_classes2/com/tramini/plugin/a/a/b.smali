.class public Lcom/tramini/plugin/a/a/b;
.super Ljava/lang/Object;


# static fields
.field private static b:Lcom/tramini/plugin/a/a/b;


# instance fields
.field a:Z

.field private c:Landroid/content/Context;

.field private d:Landroid/os/Handler;

.field private e:Landroid/content/BroadcastReceiver;

.field private f:Landroid/content/BroadcastReceiver;

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 51
    iput v0, p0, Lcom/tramini/plugin/a/a/b;->g:I

    const/4 v0, 0x0

    .line 189
    iput-boolean v0, p0, Lcom/tramini/plugin/a/a/b;->a:Z

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/tramini/plugin/a/a/b;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/tramini/plugin/a/a/b;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/tramini/plugin/a/a/b;->e:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic a(Lcom/tramini/plugin/a/a/b;)Landroid/content/Context;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    return-object p0
.end method

.method public static a()Lcom/tramini/plugin/a/a/b;
    .locals 2

    .line 56
    sget-object v0, Lcom/tramini/plugin/a/a/b;->b:Lcom/tramini/plugin/a/a/b;

    if-nez v0, :cond_0

    .line 57
    const-class v0, Lcom/tramini/plugin/a/a/b;

    monitor-enter v0

    .line 58
    :try_start_0
    new-instance v1, Lcom/tramini/plugin/a/a/b;

    invoke-direct {v1}, Lcom/tramini/plugin/a/a/b;-><init>()V

    sput-object v1, Lcom/tramini/plugin/a/a/b;->b:Lcom/tramini/plugin/a/a/b;

    .line 59
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 61
    :cond_0
    :goto_0
    sget-object v0, Lcom/tramini/plugin/a/a/b;->b:Lcom/tramini/plugin/a/a/b;

    return-object v0
.end method

.method static synthetic a(Lcom/tramini/plugin/a/a/b;Landroid/content/Context;)V
    .locals 7

    .line 2145
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    const-string v1, "P_IL_O"

    const-string v2, "tramini"

    const-string v3, ""

    invoke-static {v0, v2, v1, v3}, Lcom/tramini/plugin/a/g/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 2146
    iput-boolean v3, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    .line 2149
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyyMMdd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2150
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v0, :cond_2

    .line 2152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_0
    const-string v6, "-"

    .line 2156
    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2159
    aget-object v6, v0, v3

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2160
    iput-boolean v5, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    goto :goto_0

    .line 2164
    :cond_1
    :try_start_0
    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/tramini/plugin/a/a/b;->g:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 2166
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 2167
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    :cond_2
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    .line 2175
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "-1"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v1, v4}, Lcom/tramini/plugin/a/g/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2176
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    .line 2137
    :cond_3
    invoke-static {p1}, Lcom/tramini/plugin/a/c;->a(Landroid/content/Context;)Lcom/tramini/plugin/a/c;

    move-result-object v0

    iget-boolean v1, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/c;->a(Z)V

    .line 2138
    iput-boolean v3, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    .line 2140
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/a/b;->e(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/tramini/plugin/a/a/b;Lcom/tramini/plugin/b/a;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/a/b;->b(Lcom/tramini/plugin/b/a;)V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 1

    .line 329
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/tramini/plugin/a/a/b;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/tramini/plugin/a/a/b;->e:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic b(Lcom/tramini/plugin/a/a/b;Landroid/content/Context;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/a/b;->e(Landroid/content/Context;)V

    return-void
.end method

.method private b(Lcom/tramini/plugin/b/a;)V
    .locals 1

    .line 240
    new-instance v0, Lcom/tramini/plugin/a/a/b$4;

    invoke-direct {v0, p0, p1}, Lcom/tramini/plugin/a/a/b$4;-><init>(Lcom/tramini/plugin/a/a/b;Lcom/tramini/plugin/b/a;)V

    invoke-direct {p0, v0}, Lcom/tramini/plugin/a/a/b;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 2

    .line 321
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 322
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private static b(Ljava/lang/Runnable;J)V
    .locals 1

    .line 341
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    return-void
.end method

.method private c(Ljava/lang/Runnable;)V
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private d()V
    .locals 7

    .line 145
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    const-string v1, "P_IL_O"

    const-string v2, "tramini"

    const-string v3, ""

    invoke-static {v0, v2, v1, v3}, Lcom/tramini/plugin/a/g/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 146
    iput-boolean v3, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    .line 149
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyyMMdd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 150
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v0, :cond_2

    .line 152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const-string v6, "-"

    .line 156
    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 159
    aget-object v6, v0, v3

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 160
    iput-boolean v5, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    goto :goto_0

    .line 164
    :cond_1
    :try_start_0
    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/tramini/plugin/a/a/b;->g:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 167
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 175
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "-1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v1, v3}, Lcom/tramini/plugin/a/g/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    :cond_3
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 7

    .line 1145
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    const-string v1, "P_IL_O"

    const-string v2, "tramini"

    const-string v3, ""

    invoke-static {v0, v2, v1, v3}, Lcom/tramini/plugin/a/g/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 1146
    iput-boolean v3, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    .line 1149
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyyMMdd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1150
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v0, :cond_2

    .line 1152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_0
    const-string v6, "-"

    .line 1156
    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1159
    aget-object v6, v0, v3

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1160
    iput-boolean v5, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    goto :goto_0

    .line 1164
    :cond_1
    :try_start_0
    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/tramini/plugin/a/a/b;->g:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1166
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 1167
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    :cond_2
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    .line 1175
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "-1"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v1, v4}, Lcom/tramini/plugin/a/g/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    iput v5, p0, Lcom/tramini/plugin/a/a/b;->g:I

    .line 137
    :cond_3
    invoke-static {p1}, Lcom/tramini/plugin/a/c;->a(Landroid/content/Context;)Lcom/tramini/plugin/a/c;

    move-result-object v0

    iget-boolean v1, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/c;->a(Z)V

    .line 138
    iput-boolean v3, p0, Lcom/tramini/plugin/a/a/b;->h:Z

    .line 140
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/a/b;->e(Landroid/content/Context;)V

    return-void
.end method

.method private static e()V
    .locals 0

    return-void
.end method

.method private e(Landroid/content/Context;)V
    .locals 1

    .line 182
    invoke-static {p1}, Lcom/tramini/plugin/b/b;->a(Landroid/content/Context;)Lcom/tramini/plugin/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/tramini/plugin/b/b;->b()Lcom/tramini/plugin/b/a;

    move-result-object v0

    .line 183
    invoke-direct {p0, v0}, Lcom/tramini/plugin/a/a/b;->b(Lcom/tramini/plugin/b/a;)V

    .line 186
    invoke-virtual {p0, p1}, Lcom/tramini/plugin/a/a/b;->b(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .line 317
    iput p1, p0, Lcom/tramini/plugin/a/a/b;->g:I

    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 88
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1069
    iput-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    .line 92
    invoke-static {p1}, Lcom/tramini/plugin/a/g/h;->a(Landroid/content/Context;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v0, :cond_1

    return-void

    .line 98
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 99
    invoke-static {}, Lcom/tramini/plugin/a/a/b;->a()Lcom/tramini/plugin/a/a/b;

    move-result-object v0

    .line 1073
    iget-object v0, v0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    .line 99
    iget-object v1, p0, Lcom/tramini/plugin/a/a/b;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/tramini/plugin/a/a/b;->f:Landroid/content/BroadcastReceiver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    :cond_2
    :try_start_2
    new-instance v0, Lcom/tramini/plugin/a/b;

    invoke-direct {v0}, Lcom/tramini/plugin/a/b;-><init>()V

    iput-object v0, p0, Lcom/tramini/plugin/a/a/b;->f:Landroid/content/BroadcastReceiver;

    .line 107
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/tramini/plugin/a/g/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 110
    iget-object v1, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/tramini/plugin/a/a/b;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 116
    :catchall_1
    :try_start_3
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/tramini/plugin/a/a/b$1;

    invoke-direct {v1, p0, p1}, Lcom/tramini/plugin/a/a/b$1;-><init>(Lcom/tramini/plugin/a/a/b;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :catchall_2
    return-void
.end method

.method public final declared-synchronized a(Lcom/tramini/plugin/b/a;)V
    .locals 4

    monitor-enter p0

    .line 192
    :try_start_0
    iget-boolean v0, p0, Lcom/tramini/plugin/a/a/b;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 193
    monitor-exit p0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 197
    :try_start_1
    iput-boolean v0, p0, Lcom/tramini/plugin/a/a/b;->a:Z

    .line 198
    invoke-static {}, Lcom/tramini/plugin/a/g/b;->a()Lcom/tramini/plugin/a/g/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/tramini/plugin/a/g/b;->a(Lcom/tramini/plugin/b/a;)V

    .line 200
    invoke-static {}, Lcom/tramini/plugin/a/a/b;->a()Lcom/tramini/plugin/a/a/b;

    move-result-object v0

    new-instance v1, Lcom/tramini/plugin/a/a/b$2;

    invoke-direct {v1, p0, p1}, Lcom/tramini/plugin/a/a/b$2;-><init>(Lcom/tramini/plugin/a/a/b;Lcom/tramini/plugin/b/a;)V

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Lcom/tramini/plugin/a/a/b;->a(Ljava/lang/Runnable;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .line 290
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/tramini/plugin/a/a/b$5;

    invoke-direct {v1, p0, p1}, Lcom/tramini/plugin/a/a/b$5;-><init>(Lcom/tramini/plugin/a/a/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/tramini/plugin/a/a/b;->c:Landroid/content/Context;

    return-object v0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 2

    .line 216
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/tramini/plugin/a/a/b$3;

    invoke-direct {v1, p0, p1}, Lcom/tramini/plugin/a/a/b$3;-><init>(Lcom/tramini/plugin/a/a/b;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .line 304
    invoke-static {}, Lcom/tramini/plugin/a/g/a/a;->a()Lcom/tramini/plugin/a/g/a/a;

    move-result-object v0

    new-instance v1, Lcom/tramini/plugin/a/a/b$6;

    invoke-direct {v1, p0, p1}, Lcom/tramini/plugin/a/a/b$6;-><init>(Lcom/tramini/plugin/a/a/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/g/a/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c()I
    .locals 1

    .line 313
    iget v0, p0, Lcom/tramini/plugin/a/a/b;->g:I

    return v0
.end method
