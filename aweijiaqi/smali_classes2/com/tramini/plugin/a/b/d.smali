.class public Lcom/tramini/plugin/a/b/d;
.super Lcom/tramini/plugin/a/b/b;


# static fields
.field private static a:Lcom/tramini/plugin/a/b/d;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/b/b;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/tramini/plugin/a/b/d;
    .locals 2

    .line 24
    sget-object v0, Lcom/tramini/plugin/a/b/d;->a:Lcom/tramini/plugin/a/b/d;

    if-nez v0, :cond_0

    .line 25
    const-class v0, Lcom/tramini/plugin/a/b/d;

    monitor-enter v0

    .line 26
    :try_start_0
    new-instance v1, Lcom/tramini/plugin/a/b/d;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/tramini/plugin/a/b/d;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/tramini/plugin/a/b/d;->a:Lcom/tramini/plugin/a/b/d;

    .line 27
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 30
    :cond_0
    :goto_0
    sget-object p0, Lcom/tramini/plugin/a/b/d;->a:Lcom/tramini/plugin/a/b/d;

    return-object p0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "CREATE TABLE IF NOT EXISTS il(id TEXT ,value TEXT ,time INTEGER)"

    .line 46
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS il_all(id TEXT ,value TEXT ,time INTEGER)"

    .line 47
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 49
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "DROP TABLE IF EXISTS \'il\'"

    .line 56
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS \'il_all\'"

    .line 57
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    .line 59
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .line 67
    invoke-static {p1}, Lcom/tramini/plugin/a/b/d;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method protected final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    :try_start_0
    const-string v0, "DROP TABLE IF EXISTS \'il\'"

    .line 1056
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS \'il_all\'"

    .line 1057
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1059
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 73
    :goto_0
    invoke-static {p1}, Lcom/tramini/plugin/a/b/d;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    const-string v0, "tramini.db"

    return-object v0
.end method

.method protected final d()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final e()V
    .locals 0

    return-void
.end method
