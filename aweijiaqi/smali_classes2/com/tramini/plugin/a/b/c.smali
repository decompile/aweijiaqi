.class public Lcom/tramini/plugin/a/b/c;
.super Lcom/tramini/plugin/a/b/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/tramini/plugin/a/b/c$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/tramini/plugin/a/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    const-class v0, Lcom/tramini/plugin/a/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/tramini/plugin/a/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/tramini/plugin/a/b/b;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/tramini/plugin/a/b/e;-><init>(Lcom/tramini/plugin/a/b/b;)V

    return-void
.end method

.method public static a(Lcom/tramini/plugin/a/b/b;)Lcom/tramini/plugin/a/b/c;
    .locals 1

    .line 30
    sget-object v0, Lcom/tramini/plugin/a/b/c;->b:Lcom/tramini/plugin/a/b/c;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/tramini/plugin/a/b/c;

    invoke-direct {v0, p0}, Lcom/tramini/plugin/a/b/c;-><init>(Lcom/tramini/plugin/a/b/b;)V

    sput-object v0, Lcom/tramini/plugin/a/b/c;->b:Lcom/tramini/plugin/a/b/c;

    .line 33
    :cond_0
    sget-object p0, Lcom/tramini/plugin/a/b/c;->b:Lcom/tramini/plugin/a/b/c;

    return-object p0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/tramini/plugin/a/c/d;)J
    .locals 8

    monitor-enter p0

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 75
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "id"

    .line 76
    iget-object v4, p1, Lcom/tramini/plugin/a/c/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "value"

    .line 77
    iget-object v4, p1, Lcom/tramini/plugin/a/c/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "time"

    .line 78
    iget-wide v4, p1, Lcom/tramini/plugin/a/c/d;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 79
    iget-object v3, p1, Lcom/tramini/plugin/a/c/d;->a:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/tramini/plugin/a/b/c;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "id = ? "

    .line 84
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "il_all"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object p1, p1, Lcom/tramini/plugin/a/c/d;->a:Ljava/lang/String;

    aput-object p1, v6, v7

    invoke-virtual {v4, v5, v0, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v0, p1

    monitor-exit p0

    return-wide v0

    .line 89
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    const-string v3, "il_all"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 96
    :catch_0
    monitor-exit p0

    return-wide v1

    .line 72
    :cond_2
    :goto_0
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)J
    .locals 2

    .line 20
    check-cast p1, Lcom/tramini/plugin/a/c/d;

    invoke-virtual {p0, p1}, Lcom/tramini/plugin/a/b/c;->a(Lcom/tramini/plugin/a/c/d;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final declared-synchronized c()Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/tramini/plugin/a/c/d;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    .line 39
    :try_start_0
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "il_all"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_2

    .line 41
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 42
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 44
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    new-instance v3, Lcom/tramini/plugin/a/c/d;

    invoke-direct {v3}, Lcom/tramini/plugin/a/c/d;-><init>()V

    const-string v4, "id"

    .line 46
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/tramini/plugin/a/c/d;->a:Ljava/lang/String;

    const-string v4, "value"

    .line 47
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/tramini/plugin/a/c/d;->b:Ljava/lang/String;

    const-string v4, "time"

    .line 48
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/tramini/plugin/a/c/d;->c:J

    .line 50
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 61
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 53
    :cond_1
    monitor-exit p0

    return-object v2

    :catchall_0
    nop

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    goto :goto_2

    :catchall_1
    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    .line 61
    :goto_2
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 65
    :cond_3
    :goto_3
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized d()J
    .locals 3

    monitor-enter p0

    .line 100
    :try_start_0
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    .line 101
    monitor-exit p0

    return-wide v0

    .line 104
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/tramini/plugin/a/b/c;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "il_all"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
