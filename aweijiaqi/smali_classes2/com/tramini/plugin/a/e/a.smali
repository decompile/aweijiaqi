.class public final Lcom/tramini/plugin/a/e/a;
.super Lcom/tramini/plugin/a/e/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/tramini/plugin/a/e/e;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    if-eqz v0, :cond_b

    .line 49
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v12, p2

    invoke-virtual {v1, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 54
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 55
    array-length v2, v1

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    :goto_0
    move-object v14, v1

    goto :goto_2

    .line 56
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    goto :goto_0

    :goto_2
    if-eqz v14, :cond_b

    .line 58
    array-length v1, v14

    if-eqz v1, :cond_b

    .line 59
    array-length v15, v14

    const/16 v16, 0x0

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v15, :cond_b

    aget-object v1, v14, v7

    const/4 v2, 0x1

    .line 60
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 61
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 62
    instance-of v2, v1, Lorg/json/JSONObject;

    if-eqz v2, :cond_8

    .line 64
    array-length v2, v8

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v2, :cond_7

    aget-object v4, v8, v3

    .line 65
    move-object v5, v1

    check-cast v5, Lorg/json/JSONObject;

    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 67
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 72
    array-length v5, v9

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v5, :cond_6

    aget-object v0, v9, v6

    .line 73
    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move/from16 p1, v2

    .line 74
    array-length v2, v0

    move-object/from16 v17, v4

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v2, :cond_5

    aget-object v18, v0, v4

    move-object/from16 v19, v0

    .line 75
    array-length v0, v10

    move/from16 v20, v2

    move-object/from16 v2, v18

    move/from16 v18, v5

    const/4 v5, 0x0

    :goto_7
    if-ge v5, v0, :cond_2

    move/from16 v21, v0

    aget-object v0, v10, v5

    move/from16 v22, v7

    const-string v7, ""

    .line 76
    invoke-virtual {v2, v0, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v5, v5, 0x1

    move/from16 v0, v21

    move/from16 v7, v22

    goto :goto_7

    :cond_2
    move/from16 v22, v7

    .line 79
    array-length v0, v11

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v0, :cond_4

    aget-object v7, v11, v5

    .line 80
    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 81
    invoke-virtual {v7, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 83
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 84
    new-instance v0, Lcom/tramini/plugin/a/c/a;

    invoke-direct {v0}, Lcom/tramini/plugin/a/c/a;-><init>()V

    .line 85
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "i_nm"

    .line 86
    iget-object v3, v0, Lcom/tramini/plugin/a/c/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    iput-object v1, v0, Lcom/tramini/plugin/a/c/a;->f:Lorg/json/JSONObject;

    return-object v0

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_4
    add-int/lit8 v4, v4, 0x1

    move/from16 v5, v18

    move-object/from16 v0, v19

    move/from16 v2, v20

    move/from16 v7, v22

    goto :goto_6

    :cond_5
    move/from16 v18, v5

    move/from16 v22, v7

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p0

    move/from16 v2, p1

    move-object/from16 v4, v17

    goto :goto_5

    :cond_6
    move/from16 p1, v2

    move/from16 v22, v7

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    move/from16 v2, p1

    move/from16 v7, v22

    goto/16 :goto_4

    :cond_7
    move v0, v7

    goto :goto_9

    :cond_8
    move/from16 v22, v7

    if-eqz v1, :cond_9

    .line 95
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    move-object v2, v13

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v0, v22

    move-object/from16 v7, p6

    .line 96
    invoke-static/range {v1 .. v7}, Lcom/tramini/plugin/a/e/a;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_a

    return-object v1

    :cond_9
    move/from16 v0, v22

    :cond_a
    :goto_9
    add-int/lit8 v7, v0, 0x1

    move-object/from16 v0, p0

    goto/16 :goto_3

    :catchall_0
    :cond_b
    const/4 v0, 0x0

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;
    .locals 8

    const-string v0, "pre"

    .line 26
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "in_key"

    .line 27
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const-string v1, "rex"

    .line 28
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const-string v2, "re_key"

    .line 29
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    const-string v4, "p_key"

    .line 30
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 33
    invoke-static {v0}, Lcom/tramini/plugin/a/e/a;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {v1}, Lcom/tramini/plugin/a/e/a;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v5

    .line 35
    invoke-static {v2}, Lcom/tramini/plugin/a/e/a;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v6

    .line 36
    invoke-static {v4}, Lcom/tramini/plugin/a/e/a;->a(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v7

    const-string v1, "in_na"

    .line 38
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/tramini/plugin/a/e/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    move-object v4, v0

    invoke-static/range {v1 .. v7}, Lcom/tramini/plugin/a/e/a;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/tramini/plugin/a/c/a;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lorg/json/JSONArray;)[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 113
    :try_start_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 114
    new-array v0, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 116
    invoke-virtual {p0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception p0

    .line 119
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    return-object v0
.end method
