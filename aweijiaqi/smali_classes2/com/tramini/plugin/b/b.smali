.class public Lcom/tramini/plugin/b/b;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/tramini/plugin/b/b;

.field private static c:Lcom/tramini/plugin/b/a;


# instance fields
.field private d:Landroid/content/Context;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    const-class v0, Lcom/tramini/plugin/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/tramini/plugin/b/b;->a:Ljava/lang/String;

    const/4 v0, 0x0

    .line 27
    sput-object v0, Lcom/tramini/plugin/b/b;->b:Lcom/tramini/plugin/b/b;

    .line 28
    sput-object v0, Lcom/tramini/plugin/b/b;->c:Lcom/tramini/plugin/b/a;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    const/4 p1, 0x0

    .line 34
    iput-boolean p1, p0, Lcom/tramini/plugin/b/b;->e:Z

    return-void
.end method

.method static synthetic a(Lcom/tramini/plugin/b/b;)Landroid/content/Context;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    return-object p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/tramini/plugin/b/b;
    .locals 2

    const-class v0, Lcom/tramini/plugin/b/b;

    monitor-enter v0

    .line 38
    :try_start_0
    sget-object v1, Lcom/tramini/plugin/b/b;->b:Lcom/tramini/plugin/b/b;

    if-nez v1, :cond_0

    .line 39
    new-instance v1, Lcom/tramini/plugin/b/b;

    invoke-direct {v1, p0}, Lcom/tramini/plugin/b/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/tramini/plugin/b/b;->b:Lcom/tramini/plugin/b/b;

    .line 41
    :cond_0
    sget-object p0, Lcom/tramini/plugin/b/b;->b:Lcom/tramini/plugin/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private a(Lcom/tramini/plugin/a/d/b;)V
    .locals 2

    .line 104
    iget-boolean v0, p0, Lcom/tramini/plugin/b/b;->e:Z

    if-eqz v0, :cond_0

    return-void

    .line 108
    :cond_0
    sget-object v0, Lcom/tramini/plugin/a/g/c;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 112
    :cond_1
    new-instance v0, Lcom/tramini/plugin/a/d/d;

    invoke-direct {v0}, Lcom/tramini/plugin/a/d/d;-><init>()V

    const/4 v1, 0x0

    .line 113
    invoke-virtual {v0, v1, p1}, Lcom/tramini/plugin/a/d/d;->a(ILcom/tramini/plugin/a/d/b;)V

    return-void
.end method

.method static synthetic a(Lcom/tramini/plugin/b/b;Z)Z
    .locals 0

    .line 23
    iput-boolean p1, p0, Lcom/tramini/plugin/b/b;->e:Z

    return p1
.end method

.method private static b(Landroid/content/Context;)Lcom/tramini/plugin/b/a;
    .locals 3

    const-string v0, "tramini"

    const-string v1, "P_SY"

    const-string v2, ""

    .line 93
    invoke-static {p0, v0, v1, v2}, Lcom/tramini/plugin/a/g/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 94
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-static {p0}, Lcom/tramini/plugin/a/g/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 96
    invoke-static {p0}, Lcom/tramini/plugin/b/a;->a(Ljava/lang/String;)Lcom/tramini/plugin/b/a;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public final a(Lcom/tramini/plugin/a/d/c;)V
    .locals 2

    .line 120
    new-instance v0, Lcom/tramini/plugin/b/b$1;

    invoke-direct {v0, p0, p1}, Lcom/tramini/plugin/b/b$1;-><init>(Lcom/tramini/plugin/b/b;Lcom/tramini/plugin/a/d/c;)V

    .line 1104
    iget-boolean p1, p0, Lcom/tramini/plugin/b/b;->e:Z

    if-nez p1, :cond_0

    .line 1108
    sget-object p1, Lcom/tramini/plugin/a/g/c;->a:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1112
    new-instance p1, Lcom/tramini/plugin/a/d/d;

    invoke-direct {p1}, Lcom/tramini/plugin/a/d/d;-><init>()V

    const/4 v1, 0x0

    .line 1113
    invoke-virtual {p1, v1, v0}, Lcom/tramini/plugin/a/d/d;->a(ILcom/tramini/plugin/a/d/b;)V

    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 5

    .line 50
    iget-object v0, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "tramini"

    const-string v3, "P_UD_TE"

    invoke-static {v0, v2, v3, v1}, Lcom/tramini/plugin/a/g/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 51
    invoke-virtual {p0}, Lcom/tramini/plugin/b/b;->b()Lcom/tramini/plugin/b/a;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {v2}, Lcom/tramini/plugin/b/a;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized b()Lcom/tramini/plugin/b/a;
    .locals 4

    monitor-enter p0

    .line 68
    :try_start_0
    sget-object v0, Lcom/tramini/plugin/b/b;->c:Lcom/tramini/plugin/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 70
    :try_start_1
    iget-object v0, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 71
    invoke-static {}, Lcom/tramini/plugin/a/a/b;->a()Lcom/tramini/plugin/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/tramini/plugin/a/a/b;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/tramini/plugin/b/b;->d:Landroid/content/Context;

    const-string v1, "tramini"

    const-string v2, "P_SY"

    const-string v3, ""

    .line 1093
    invoke-static {v0, v1, v2, v3}, Lcom/tramini/plugin/a/g/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1094
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1095
    invoke-static {v0}, Lcom/tramini/plugin/a/g/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1096
    invoke-static {v0}, Lcom/tramini/plugin/b/a;->a(Ljava/lang/String;)Lcom/tramini/plugin/b/a;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 73
    :goto_0
    sput-object v0, Lcom/tramini/plugin/b/b;->c:Lcom/tramini/plugin/b/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catch_0
    :try_start_2
    invoke-static {}, Lcom/tramini/plugin/a/a/b;->a()Lcom/tramini/plugin/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/tramini/plugin/b/b;->c:Lcom/tramini/plugin/b/a;

    invoke-virtual {v0, v1}, Lcom/tramini/plugin/a/a/b;->a(Lcom/tramini/plugin/b/a;)V

    .line 83
    :cond_2
    sget-object v0, Lcom/tramini/plugin/b/b;->c:Lcom/tramini/plugin/b/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
