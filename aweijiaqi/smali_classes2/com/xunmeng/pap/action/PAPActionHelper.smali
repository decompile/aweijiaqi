.class public Lcom/xunmeng/pap/action/PAPActionHelper;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static onEventActivate()V
    .locals 2

    invoke-static {}, Lcom/xunmeng/pap/f/d;->a()Lcom/xunmeng/pap/f/c;

    move-result-object v0

    new-instance v1, Lcom/xunmeng/pap/action/PAPActionHelper$a;

    invoke-direct {v1}, Lcom/xunmeng/pap/action/PAPActionHelper$a;-><init>()V

    invoke-interface {v0, v1}, Lcom/xunmeng/pap/f/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static onEventOrderWay(IJZ)V
    .locals 3

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, -0x1

    if-eq p0, v1, :cond_0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    const-string v1, "order_way"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-wide/16 v1, -0x1

    cmp-long p0, p1, v1

    if-eqz p0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    const-string p1, "order_price"

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {p3}, Lcom/xunmeng/pap/action/a;->a(Z)Ljava/lang/String;

    move-result-object p0

    const-string p1, "order_status"

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/xunmeng/pap/action/b;->d:Lcom/xunmeng/pap/action/b;

    invoke-static {p0, v0}, Lcom/xunmeng/pap/action/PAPAction;->onActionTrack(Lcom/xunmeng/pap/action/b;Ljava/util/Map;)V

    return-void
.end method

.method public static onEventRegister(IZ)V
    .locals 2

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, -0x1

    if-eq p0, v1, :cond_0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    const-string v1, "register_way"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p1}, Lcom/xunmeng/pap/action/a;->a(Z)Ljava/lang/String;

    move-result-object p0

    const-string p1, "register_status"

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/xunmeng/pap/action/b;->c:Lcom/xunmeng/pap/action/b;

    invoke-static {p0, v0}, Lcom/xunmeng/pap/action/PAPAction;->onActionTrack(Lcom/xunmeng/pap/action/b;Ljava/util/Map;)V

    return-void
.end method
