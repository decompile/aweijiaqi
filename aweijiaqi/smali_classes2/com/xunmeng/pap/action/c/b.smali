.class public Lcom/xunmeng/pap/action/c/b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xunmeng/pap/action/c/b$a;
    }
.end annotation


# static fields
.field private static volatile b:Lcom/xunmeng/pap/action/c/b;


# instance fields
.field private a:Lcom/xunmeng/pap/action/c/b$a;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()Lcom/xunmeng/pap/action/c/b;
    .locals 2

    sget-object v0, Lcom/xunmeng/pap/action/c/b;->b:Lcom/xunmeng/pap/action/c/b;

    if-nez v0, :cond_1

    const-class v0, Lcom/xunmeng/pap/action/c/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xunmeng/pap/action/c/b;->b:Lcom/xunmeng/pap/action/c/b;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xunmeng/pap/action/c/b;

    invoke-direct {v1}, Lcom/xunmeng/pap/action/c/b;-><init>()V

    sput-object v1, Lcom/xunmeng/pap/action/c/b;->b:Lcom/xunmeng/pap/action/c/b;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xunmeng/pap/action/c/b;->b:Lcom/xunmeng/pap/action/c/b;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "activate_result_key"

    invoke-virtual {v0, v2, v1}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v0

    const/4 v2, 0x1

    const-string v3, "activate_retry_key"

    invoke-virtual {v0, v3, v2}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/action/c/b;->a:Lcom/xunmeng/pap/action/c/b$a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/xunmeng/pap/action/c/b$a;

    invoke-direct {v0}, Lcom/xunmeng/pap/action/c/b$a;-><init>()V

    iput-object v0, p0, Lcom/xunmeng/pap/action/c/b;->a:Lcom/xunmeng/pap/action/c/b$a;

    :cond_1
    sget-object v0, Lcom/xunmeng/pap/action/b;->b:Lcom/xunmeng/pap/action/b;

    iget-object v2, p0, Lcom/xunmeng/pap/action/c/b;->a:Lcom/xunmeng/pap/action/c/b$a;

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v1}, Lcom/xunmeng/pap/action/PAPAction;->onActionTrack(Lcom/xunmeng/pap/action/b;Ljava/util/Map;Lcom/xunmeng/pap/a/b;Z)V

    return-void
.end method
