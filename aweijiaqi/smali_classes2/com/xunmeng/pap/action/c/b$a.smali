.class Lcom/xunmeng/pap/action/c/b$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/xunmeng/pap/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xunmeng/pap/action/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xunmeng/pap/action/c/b$a;->a:Z

    return-void
.end method

.method private b(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "oaid"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "imei1"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "imei2"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    const-string v0, "activate_result_key"

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object p1

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/xunmeng/pap/g/g;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Ljava/util/Map;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "sub_op"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return v2

    :cond_0
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v1

    const-string v3, "activate_time_key"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v4

    const-string v5, "activate_result_key"

    invoke-virtual {v4, v5, v2}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Z)Z

    move-result v4

    const-string v6, "time"

    if-eqz v4, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {p1, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v1

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/xunmeng/pap/g/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/xunmeng/pap/action/c/b$a;->b(Ljava/util/Map;)Z

    move-result p1

    const-string v1, "activate_retry_key"

    const/4 v3, 0x1

    if-eqz p1, :cond_2

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object p1

    invoke-virtual {p1, v1, v2}, Lcom/xunmeng/pap/g/g;->b(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/xunmeng/pap/action/c/a;->b()Lcom/xunmeng/pap/action/c/a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/xunmeng/pap/action/c/a;->a(Ljava/lang/String;)V

    return v3

    :cond_2
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object p1

    invoke-virtual {p1, v1, v3}, Lcom/xunmeng/pap/g/g;->b(Ljava/lang/String;Z)V

    iget-boolean p1, p0, Lcom/xunmeng/pap/action/c/b$a;->a:Z

    if-nez p1, :cond_3

    new-instance p1, Lcom/xunmeng/pap/action/c/a$b;

    const-wide/16 v6, 0x2710

    const v1, 0x7fffffff

    invoke-direct {p1, v6, v7, v1, v0}, Lcom/xunmeng/pap/action/c/a$b;-><init>(JILjava/lang/String;)V

    invoke-static {}, Lcom/xunmeng/pap/action/c/a;->b()Lcom/xunmeng/pap/action/c/a;

    move-result-object v0

    new-instance v1, Lcom/xunmeng/pap/action/c/b$a$a;

    invoke-direct {v1, p0}, Lcom/xunmeng/pap/action/c/b$a$a;-><init>(Lcom/xunmeng/pap/action/c/b$a;)V

    invoke-virtual {v0, p1, v1}, Lcom/xunmeng/pap/action/c/a;->a(Lcom/xunmeng/pap/action/c/a$b;Lcom/xunmeng/pap/action/c/c;)V

    iput-boolean v3, p0, Lcom/xunmeng/pap/action/c/b$a;->a:Z

    :cond_3
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object p1

    invoke-virtual {p1, v5, v2}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Z)Z

    move-result p1

    xor-int/2addr p1, v3

    return p1
.end method
