.class Lcom/xunmeng/pap/action/c/a$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xunmeng/pap/action/c/a;->a(Ljava/lang/String;Lcom/xunmeng/pap/action/c/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/xunmeng/pap/action/c/c;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/xunmeng/pap/action/c/a$b;

.field final synthetic d:J

.field final synthetic e:Lcom/xunmeng/pap/action/c/a;


# direct methods
.method constructor <init>(Lcom/xunmeng/pap/action/c/a;Lcom/xunmeng/pap/action/c/c;Ljava/lang/String;Lcom/xunmeng/pap/action/c/a$b;J)V
    .locals 0

    iput-object p1, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    iput-object p2, p0, Lcom/xunmeng/pap/action/c/a$a;->a:Lcom/xunmeng/pap/action/c/c;

    iput-object p3, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/xunmeng/pap/action/c/a$a;->c:Lcom/xunmeng/pap/action/c/a$b;

    iput-wide p5, p0, Lcom/xunmeng/pap/action/c/a$a;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->a:Lcom/xunmeng/pap/action/c/c;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v0}, Lcom/xunmeng/pap/action/c/a;->a(Lcom/xunmeng/pap/action/c/a;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v0}, Lcom/xunmeng/pap/action/c/a;->b(Lcom/xunmeng/pap/action/c/a;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v0}, Lcom/xunmeng/pap/action/c/a;->c(Lcom/xunmeng/pap/action/c/a;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/xunmeng/pap/action/c/a$a;->c:Lcom/xunmeng/pap/action/c/a$b;

    invoke-static {v3}, Lcom/xunmeng/pap/action/c/a$b;->a(Lcom/xunmeng/pap/action/c/a$b;)I

    move-result v3

    if-le v2, v3, :cond_3

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v0}, Lcom/xunmeng/pap/action/c/a;->b(Lcom/xunmeng/pap/action/c/a;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    :cond_3
    iget-object v2, p0, Lcom/xunmeng/pap/action/c/a$a;->a:Lcom/xunmeng/pap/action/c/c;

    iget-object v3, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/xunmeng/pap/action/c/c;->a(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x1

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v3, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v3}, Lcom/xunmeng/pap/action/c/a;->c(Lcom/xunmeng/pap/action/c/a;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/xunmeng/pap/action/c/a;->a()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "%s retry count: %d"

    const/4 v5, 0x2

    :try_start_1
    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/xunmeng/pap/action/c/a$a;->b:Ljava/lang/String;

    aput-object v6, v5, v1

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a$a;->e:Lcom/xunmeng/pap/action/c/a;

    invoke-static {v0}, Lcom/xunmeng/pap/action/c/a;->b(Lcom/xunmeng/pap/action/c/a;)Landroid/os/Handler;

    move-result-object v0

    iget-wide v1, p0, Lcom/xunmeng/pap/action/c/a$a;->d:J

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
