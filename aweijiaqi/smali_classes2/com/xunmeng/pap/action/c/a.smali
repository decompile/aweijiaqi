.class public Lcom/xunmeng/pap/action/c/a;
.super Landroid/os/HandlerThread;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xunmeng/pap/action/c/a$b;
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;

.field private static volatile e:Lcom/xunmeng/pap/action/c/a;


# instance fields
.field private a:Landroid/os/Handler;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xunmeng/pap/action/c/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/xunmeng/pap/action/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xunmeng/pap/action/c/a;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "PAP_ACTION_MESSAGE_HANDLER"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/xunmeng/pap/action/c/a;->c:Ljava/util/Map;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/xunmeng/pap/action/c/a;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xunmeng/pap/action/c/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/xunmeng/pap/action/c/a;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    return-object p0
.end method

.method private a(Ljava/lang/String;Lcom/xunmeng/pap/action/c/c;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/xunmeng/pap/action/c/c<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/xunmeng/pap/action/c/a$b;

    if-nez v5, :cond_1

    return-void

    :cond_1
    invoke-virtual {v5}, Lcom/xunmeng/pap/action/c/a$b;->b()J

    move-result-wide v8

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a;->a:Landroid/os/Handler;

    new-instance v10, Lcom/xunmeng/pap/action/c/a$a;

    move-object v1, v10

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-wide v6, v8

    invoke-direct/range {v1 .. v7}, Lcom/xunmeng/pap/action/c/a$a;-><init>(Lcom/xunmeng/pap/action/c/a;Lcom/xunmeng/pap/action/c/c;Ljava/lang/String;Lcom/xunmeng/pap/action/c/a$b;J)V

    invoke-virtual {v0, v10, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic b(Lcom/xunmeng/pap/action/c/a;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/xunmeng/pap/action/c/a;->a:Landroid/os/Handler;

    return-object p0
.end method

.method public static b()Lcom/xunmeng/pap/action/c/a;
    .locals 2

    sget-object v0, Lcom/xunmeng/pap/action/c/a;->e:Lcom/xunmeng/pap/action/c/a;

    if-nez v0, :cond_1

    const-class v0, Lcom/xunmeng/pap/action/c/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xunmeng/pap/action/c/a;->e:Lcom/xunmeng/pap/action/c/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xunmeng/pap/action/c/a;

    invoke-direct {v1}, Lcom/xunmeng/pap/action/c/a;-><init>()V

    sput-object v1, Lcom/xunmeng/pap/action/c/a;->e:Lcom/xunmeng/pap/action/c/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xunmeng/pap/action/c/a;->e:Lcom/xunmeng/pap/action/c/a;

    return-object v0
.end method

.method static synthetic c(Lcom/xunmeng/pap/action/c/a;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/xunmeng/pap/action/c/a;->c:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/xunmeng/pap/action/c/a$b;Lcom/xunmeng/pap/action/c/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xunmeng/pap/action/c/a$b;",
            "Lcom/xunmeng/pap/action/c/c<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/xunmeng/pap/action/c/a$b;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/xunmeng/pap/action/c/a;->c:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, p2}, Lcom/xunmeng/pap/action/c/a;->a(Ljava/lang/String;Lcom/xunmeng/pap/action/c/c;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/xunmeng/pap/action/c/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
