.class public final enum Lcom/xunmeng/pap/action/b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/xunmeng/pap/action/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lcom/xunmeng/pap/action/b;

.field public static final enum c:Lcom/xunmeng/pap/action/b;

.field public static final enum d:Lcom/xunmeng/pap/action/b;

.field private static final synthetic e:[Lcom/xunmeng/pap/action/b;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/xunmeng/pap/action/b;

    const/4 v1, 0x0

    const-string v2, "ACTIVATE"

    const-string v3, "activate"

    invoke-direct {v0, v2, v1, v3}, Lcom/xunmeng/pap/action/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xunmeng/pap/action/b;->b:Lcom/xunmeng/pap/action/b;

    new-instance v0, Lcom/xunmeng/pap/action/b;

    const/4 v2, 0x1

    const-string v3, "REGISTER"

    const-string v4, "register"

    invoke-direct {v0, v3, v2, v4}, Lcom/xunmeng/pap/action/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xunmeng/pap/action/b;->c:Lcom/xunmeng/pap/action/b;

    new-instance v0, Lcom/xunmeng/pap/action/b;

    const/4 v3, 0x2

    const-string v4, "PAY_ORDER"

    const-string v5, "pay_order"

    invoke-direct {v0, v4, v3, v5}, Lcom/xunmeng/pap/action/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xunmeng/pap/action/b;->d:Lcom/xunmeng/pap/action/b;

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/xunmeng/pap/action/b;

    sget-object v5, Lcom/xunmeng/pap/action/b;->b:Lcom/xunmeng/pap/action/b;

    aput-object v5, v4, v1

    sget-object v1, Lcom/xunmeng/pap/action/b;->c:Lcom/xunmeng/pap/action/b;

    aput-object v1, v4, v2

    aput-object v0, v4, v3

    sput-object v4, Lcom/xunmeng/pap/action/b;->e:[Lcom/xunmeng/pap/action/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/xunmeng/pap/action/b;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xunmeng/pap/action/b;
    .locals 1

    const-class v0, Lcom/xunmeng/pap/action/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/xunmeng/pap/action/b;

    return-object p0
.end method

.method public static values()[Lcom/xunmeng/pap/action/b;
    .locals 1

    sget-object v0, Lcom/xunmeng/pap/action/b;->e:[Lcom/xunmeng/pap/action/b;

    invoke-virtual {v0}, [Lcom/xunmeng/pap/action/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xunmeng/pap/action/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/action/b;->a:Ljava/lang/String;

    return-object v0
.end method
