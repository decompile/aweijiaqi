.class public Lcom/xunmeng/pap/b/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "PAPTransSDK.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/xunmeng/pap/b/a;
    .locals 1

    new-instance v0, Lcom/xunmeng/pap/b/a;

    invoke-direct {v0, p0}, Lcom/xunmeng/pap/b/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE actions_table ( _id INTEGER PRIMARY KEY AUTOINCREMENT, log_id CHAR(32) unique not null, action_type TEXT not null, action_time BIGINT not null, create_time BIGINT not null, action_param TEXT not null, status TINYINT not null )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/xunmeng/pap/b/a;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    const-string p2, "DROP TABLE IF EXISTS actions_table"

    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/xunmeng/pap/b/a;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
