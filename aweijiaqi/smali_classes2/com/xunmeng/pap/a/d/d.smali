.class public Lcom/xunmeng/pap/a/d/d;
.super Landroid/os/HandlerThread;
.source ""


# instance fields
.field private a:Landroid/os/Handler;

.field private final b:Lcom/xunmeng/pap/a/c/a;

.field private final c:Lcom/xunmeng/pap/a/c/b;

.field private volatile d:Z

.field private e:Landroid/content/Context;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "PAP_ACTION_MESSAGE_HANDLER"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xunmeng/pap/a/d/d;->d:Z

    iput-object p1, p0, Lcom/xunmeng/pap/a/d/d;->e:Landroid/content/Context;

    iput-object p2, p0, Lcom/xunmeng/pap/a/d/d;->f:Ljava/lang/String;

    new-instance v0, Lcom/xunmeng/pap/a/c/c/a;

    invoke-direct {v0, p1}, Lcom/xunmeng/pap/a/c/c/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xunmeng/pap/a/d/d;->b:Lcom/xunmeng/pap/a/c/a;

    new-instance p1, Lcom/xunmeng/pap/a/c/c/b;

    invoke-direct {p1, p2, p3}, Lcom/xunmeng/pap/a/c/c/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xunmeng/pap/a/d/d;->c:Lcom/xunmeng/pap/a/c/b;

    invoke-direct {p0}, Lcom/xunmeng/pap/a/d/d;->e()V

    return-void
.end method

.method private e()V
    .locals 6

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->b:Lcom/xunmeng/pap/a/c/a;

    const v1, 0x7fffffff

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x2

    aput v5, v3, v4

    invoke-interface {v0, v1, v3}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;[I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xunmeng/pap/a/d/a;

    iget-object v3, p0, Lcom/xunmeng/pap/a/d/d;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-virtual {v1}, Lcom/xunmeng/pap/a/d/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v2}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;I)Z

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xunmeng/pap/a/d/d;->d:Z

    return-void
.end method

.method a(J)V
    .locals 2

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public a(Lcom/xunmeng/pap/a/d/b;)V
    .locals 2

    iget-boolean v0, p0, Lcom/xunmeng/pap/a/d/d;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method b()V
    .locals 2

    iget-boolean v0, p0, Lcom/xunmeng/pap/a/d/d;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public b(Lcom/xunmeng/pap/a/d/b;)V
    .locals 2

    iget-boolean v0, p0, Lcom/xunmeng/pap/a/d/d;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    return-void
.end method

.method c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/d;->e:Landroid/content/Context;

    return-object v0
.end method

.method public declared-synchronized start()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/xunmeng/pap/a/d/e;

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/d;->b:Lcom/xunmeng/pap/a/c/a;

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/d;->c:Lcom/xunmeng/pap/a/c/b;

    invoke-direct {v0, v1, v2, p0}, Lcom/xunmeng/pap/a/d/e;-><init>(Lcom/xunmeng/pap/a/c/a;Lcom/xunmeng/pap/a/c/b;Lcom/xunmeng/pap/a/d/d;)V

    new-instance v1, Lcom/xunmeng/pap/a/d/c;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/xunmeng/pap/a/d/d;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-direct {v1, v2, p0, v0, v3}, Lcom/xunmeng/pap/a/d/c;-><init>(Landroid/os/Looper;Lcom/xunmeng/pap/a/d/d;Lcom/xunmeng/pap/a/d/e;Lcom/xunmeng/pap/a/c/a;)V

    iput-object v1, p0, Lcom/xunmeng/pap/a/d/d;->a:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/xunmeng/pap/a/d/d;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
