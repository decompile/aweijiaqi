.class Lcom/xunmeng/pap/a/d/e;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final h:Ljava/lang/String; = "e"


# instance fields
.field private final a:Lcom/xunmeng/pap/a/c/a;

.field private final b:Lcom/xunmeng/pap/a/c/b;

.field private c:Lcom/xunmeng/pap/a/d/d;

.field private volatile d:J

.field private volatile e:I

.field private f:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final g:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/xunmeng/pap/a/d/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/xunmeng/pap/a/c/a;Lcom/xunmeng/pap/a/c/b;Lcom/xunmeng/pap/a/d/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xunmeng/pap/a/d/e;->d:J

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/xunmeng/pap/a/d/e;->e:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/xunmeng/pap/a/d/e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    iput-object p1, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    iput-object p3, p0, Lcom/xunmeng/pap/a/d/e;->c:Lcom/xunmeng/pap/a/d/d;

    iput-object p2, p0, Lcom/xunmeng/pap/a/d/e;->b:Lcom/xunmeng/pap/a/c/b;

    return-void
.end method

.method static synthetic a(Lcom/xunmeng/pap/a/d/e;)Lcom/xunmeng/pap/a/d/d;
    .locals 0

    iget-object p0, p0, Lcom/xunmeng/pap/a/d/e;->c:Lcom/xunmeng/pap/a/d/d;

    return-object p0
.end method

.method static synthetic a(Lcom/xunmeng/pap/a/d/e;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/e;->d(Ljava/util/List;)V

    return-void
.end method

.method private b(Lcom/xunmeng/pap/a/d/a;)V
    .locals 3

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xunmeng/pap/a/d/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    invoke-virtual {v0}, Lcom/xunmeng/pap/a/d/a;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;I)Z

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/xunmeng/pap/a/d/a;->a(I)V

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    invoke-virtual {p1}, Lcom/xunmeng/pap/a/d/a;->d()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;I)Z

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xunmeng/pap/a/d/f;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xunmeng/pap/a/d/f;

    invoke-virtual {v0}, Lcom/xunmeng/pap/a/d/f;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    invoke-interface {v1, v0}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xunmeng/pap/a/d/f;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/xunmeng/pap/a/d/e;->h:Ljava/lang/String;

    const-string v1, "report failed"

    invoke-static {v0, v1}, Lcom/xunmeng/pap/g/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    const/4 v1, 0x3

    const/16 v2, 0x1e

    if-lt v0, v2, :cond_0

    const v0, 0xea60

    :goto_0
    iput v0, p0, Lcom/xunmeng/pap/a/d/e;->e:I

    goto :goto_1

    :cond_0
    const/16 v2, 0xa

    if-lt v0, v2, :cond_1

    const/16 v0, 0x2710

    goto :goto_0

    :cond_1
    const/4 v2, 0x6

    if-lt v0, v2, :cond_2

    const/16 v0, 0xfa0

    goto :goto_0

    :cond_2
    if-lt v0, v1, :cond_3

    const/16 v0, 0x7d0

    goto :goto_0

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xunmeng/pap/a/d/f;

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    invoke-virtual {v0}, Lcom/xunmeng/pap/a/d/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;I)Z

    goto :goto_2

    :cond_4
    return-void
.end method

.method private d(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xunmeng/pap/a/d/f;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->b:Lcom/xunmeng/pap/a/c/b;

    invoke-interface {v0, p1}, Lcom/xunmeng/pap/a/c/b;->a(Ljava/util/List;)Lcom/xunmeng/pap/e/e;

    move-result-object v0

    sget-object v1, Lcom/xunmeng/pap/a/d/e;->h:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/xunmeng/pap/e/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/xunmeng/pap/e/e;->b()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/e;->b(Ljava/util/List;)V

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    sget-object v0, Lcom/xunmeng/pap/a/d/e;->h:Ljava/lang/String;

    const-string v1, "report success"

    invoke-static {v0, v1}, Lcom/xunmeng/pap/g/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const/16 v2, 0x1f4

    if-ne v1, v2, :cond_2

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, Lcom/xunmeng/pap/e/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "error_code"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const v1, 0xcb20

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->c:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {v0}, Lcom/xunmeng/pap/a/d/d;->a()V

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_2
    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/e;->c(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/Throwable;)V

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/e;->c(Ljava/util/List;)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 7

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->a:Lcom/xunmeng/pap/a/c/a;

    invoke-interface {v0}, Lcom/xunmeng/pap/a/c/a;->a()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/xunmeng/pap/a/d/e;->d:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    iget v4, p0, Lcom/xunmeng/pap/a/d/e;->e:I

    int-to-long v4, v4

    cmp-long v6, v0, v4

    if-gez v6, :cond_1

    goto :goto_0

    :cond_1
    move-wide v0, v2

    :goto_0
    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/e;->c:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {v2, v0, v1}, Lcom/xunmeng/pap/a/d/d;->a(J)V

    return-void

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xunmeng/pap/a/d/e;->d:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x14

    if-ge v2, v3, :cond_4

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const/high16 v2, 0x100000

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xunmeng/pap/a/d/a;

    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/xunmeng/pap/a/d/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/2addr v1, v4

    new-instance v4, Lcom/xunmeng/pap/a/d/f;

    invoke-virtual {v2}, Lcom/xunmeng/pap/a/d/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v3}, Lcom/xunmeng/pap/a/d/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/xunmeng/pap/f/d;->a()Lcom/xunmeng/pap/f/c;

    move-result-object v1

    new-instance v2, Lcom/xunmeng/pap/a/d/e$a;

    invoke-direct {v2, p0, v0}, Lcom/xunmeng/pap/a/d/e$a;-><init>(Lcom/xunmeng/pap/a/d/e;Ljava/util/List;)V

    invoke-interface {v1, v2}, Lcom/xunmeng/pap/f/c;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/xunmeng/pap/a/d/e;->a()V

    :cond_5
    return-void
.end method

.method a(Lcom/xunmeng/pap/a/d/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/e;->b(Lcom/xunmeng/pap/a/d/a;)V

    invoke-virtual {p0}, Lcom/xunmeng/pap/a/d/e;->a()V

    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xunmeng/pap/a/d/a;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xunmeng/pap/a/d/a;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/xunmeng/pap/a/d/e;->b(Lcom/xunmeng/pap/a/d/a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xunmeng/pap/a/d/e;->a()V

    return-void
.end method

.method b()I
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/e;->g:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    rsub-int v0, v0, 0xc8

    return v0
.end method
