.class Lcom/xunmeng/pap/a/d/c;
.super Landroid/os/Handler;
.source ""


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final a:Lcom/xunmeng/pap/a/d/e;

.field private final b:Lcom/xunmeng/pap/a/c/a;

.field private final c:Lcom/xunmeng/pap/a/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/xunmeng/pap/a/d/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xunmeng/pap/a/d/c;->d:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/os/Looper;Lcom/xunmeng/pap/a/d/d;Lcom/xunmeng/pap/a/d/e;Lcom/xunmeng/pap/a/c/a;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p3, p0, Lcom/xunmeng/pap/a/d/c;->a:Lcom/xunmeng/pap/a/d/e;

    iput-object p4, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    iput-object p2, p0, Lcom/xunmeng/pap/a/d/c;->c:Lcom/xunmeng/pap/a/d/d;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)Lcom/xunmeng/pap/a/d/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/xunmeng/pap/a/d/a;"
        }
    .end annotation

    const-string v0, "time"

    new-instance v1, Lcom/xunmeng/pap/a/d/a;

    invoke-direct {v1}, Lcom/xunmeng/pap/a/d/a;-><init>()V

    invoke-static {p2}, Lcom/xunmeng/pap/g/h;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xunmeng/pap/a/d/a;->a(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/xunmeng/pap/a/d/a;->a(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :try_start_0
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    invoke-virtual {v1, v2, v3}, Lcom/xunmeng/pap/a/d/a;->a(J)V

    invoke-virtual {v1, p1}, Lcom/xunmeng/pap/a/d/a;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/xunmeng/pap/g/h;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/xunmeng/pap/a/d/a;->c(Ljava/lang/String;)V

    return-object v1
.end method

.method private a()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/c;->c:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {v1}, Lcom/xunmeng/pap/a/d/d;->d()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/c;->c:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {v2}, Lcom/xunmeng/pap/a/d/d;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "app_pm_id"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "sdk_version"

    const-string v3, "0.0.4"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/xunmeng/pap/g/h;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "log_id"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/xunmeng/pap/g/b;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xunmeng/pap/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "imei1"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/xunmeng/pap/g/b;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xunmeng/pap/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "imei2"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/xunmeng/pap/g/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xunmeng/pap/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "oaid"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lcom/xunmeng/pap/g/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xunmeng/pap/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "android_id"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lcom/xunmeng/pap/g/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xunmeng/pap/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mac"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lcom/xunmeng/pap/g/b;->c(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "network"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "platform"

    const-string v2, "android"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xunmeng/pap/a/d/c;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private a(Lcom/xunmeng/pap/a/d/b;)V
    .locals 3

    invoke-virtual {p1}, Lcom/xunmeng/pap/a/d/b;->a()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/xunmeng/pap/a/d/c;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-virtual {p1}, Lcom/xunmeng/pap/a/d/b;->c()Lcom/xunmeng/pap/a/b;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/xunmeng/pap/a/b;->a(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/xunmeng/pap/a/d/b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/xunmeng/pap/a/d/c;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/xunmeng/pap/a/d/a;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/c;->a(Lcom/xunmeng/pap/a/d/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/c;->a:Lcom/xunmeng/pap/a/d/e;

    invoke-virtual {v2, p1}, Lcom/xunmeng/pap/a/d/e;->a(Lcom/xunmeng/pap/a/d/a;)V

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1, v0}, Lcom/xunmeng/pap/a/b;->a(Z)V

    :cond_3
    return-void
.end method

.method private a(Lcom/xunmeng/pap/a/d/a;)Z
    .locals 3

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-interface {v0}, Lcom/xunmeng/pap/a/c/a;->a()I

    move-result v0

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    const/16 v1, 0x14d

    invoke-interface {v0, v1}, Lcom/xunmeng/pap/a/c/a;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-interface {v2, v1}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-interface {v0, p1}, Lcom/xunmeng/pap/a/c/a;->a(Lcom/xunmeng/pap/a/d/a;)Z

    move-result p1

    return p1
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, Lcom/xunmeng/pap/a/d/c;->a:Lcom/xunmeng/pap/a/d/e;

    invoke-virtual {v0}, Lcom/xunmeng/pap/a/d/e;->b()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xunmeng/pap/a/d/c;->b:Lcom/xunmeng/pap/a/c/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-interface {v1, v0, v2}, Lcom/xunmeng/pap/a/c/a;->a(Ljava/lang/String;[I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/xunmeng/pap/a/d/c;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xunmeng/pap/a/d/c;->a:Lcom/xunmeng/pap/a/d/e;

    invoke-virtual {v1, v0}, Lcom/xunmeng/pap/a/d/e;->a(Ljava/util/List;)V

    :cond_2
    :goto_0
    return-void

    :array_0
    .array-data 4
        0x1
        0x3
    .end array-data
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xunmeng/pap/a/d/c;->a:Lcom/xunmeng/pap/a/d/e;

    invoke-virtual {p1}, Lcom/xunmeng/pap/a/d/e;->a()V

    goto :goto_0

    :cond_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, p1, Lcom/xunmeng/pap/a/d/b;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/xunmeng/pap/a/d/b;

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/a/d/c;->a(Lcom/xunmeng/pap/a/d/b;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/xunmeng/pap/a/d/c;->b()V

    :cond_3
    :goto_0
    return-void
.end method
