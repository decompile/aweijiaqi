.class public Lcom/xunmeng/pap/a/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/xunmeng/pap/a/d/g;


# static fields
.field private static volatile c:Lcom/xunmeng/pap/a/a;


# instance fields
.field private a:Lcom/xunmeng/pap/a/d/d;

.field public b:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/xunmeng/pap/a/a;
    .locals 2

    sget-object v0, Lcom/xunmeng/pap/a/a;->c:Lcom/xunmeng/pap/a/a;

    if-nez v0, :cond_1

    const-class v0, Lcom/xunmeng/pap/a/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xunmeng/pap/a/a;->c:Lcom/xunmeng/pap/a/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xunmeng/pap/a/a;

    invoke-direct {v1}, Lcom/xunmeng/pap/a/a;-><init>()V

    sput-object v1, Lcom/xunmeng/pap/a/a;->c:Lcom/xunmeng/pap/a/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xunmeng/pap/a/a;->c:Lcom/xunmeng/pap/a/a;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xunmeng/pap/g/g;->a(Landroid/content/Context;)V

    iput-boolean p4, p0, Lcom/xunmeng/pap/a/a;->b:Z

    invoke-static {}, Lcom/xunmeng/pap/d/b;->b()Lcom/xunmeng/pap/d/b;

    move-result-object p4

    invoke-virtual {p4, v0}, Lcom/xunmeng/pap/d/b;->a(Landroid/content/Context;)V

    new-instance p4, Lcom/xunmeng/pap/a/d/d;

    invoke-direct {p4, p1, p2, p3}, Lcom/xunmeng/pap/a/d/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/xunmeng/pap/a/a;->a:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {p4}, Lcom/xunmeng/pap/a/d/d;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/util/Map;Lcom/xunmeng/pap/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/xunmeng/pap/a/b;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/a;->a:Lcom/xunmeng/pap/a/d/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/xunmeng/pap/a/d/b;

    invoke-direct {v0, p1, p2, p3}, Lcom/xunmeng/pap/a/d/b;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/xunmeng/pap/a/b;)V

    iget-object p1, p0, Lcom/xunmeng/pap/a/a;->a:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {p1, v0}, Lcom/xunmeng/pap/a/d/d;->b(Lcom/xunmeng/pap/a/d/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b(Ljava/lang/String;Ljava/util/Map;Lcom/xunmeng/pap/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/xunmeng/pap/a/b;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xunmeng/pap/a/a;->a:Lcom/xunmeng/pap/a/d/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/xunmeng/pap/a/d/b;

    invoke-direct {v0, p1, p2, p3}, Lcom/xunmeng/pap/a/d/b;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/xunmeng/pap/a/b;)V

    iget-object p1, p0, Lcom/xunmeng/pap/a/a;->a:Lcom/xunmeng/pap/a/d/d;

    invoke-virtual {p1, v0}, Lcom/xunmeng/pap/a/d/d;->a(Lcom/xunmeng/pap/a/d/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
