.class public Lcom/xunmeng/pap/a/c/c/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/xunmeng/pap/a/c/b;


# static fields
.field private static final d:Ljava/lang/String; = "b"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/xunmeng/pap/e/b;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xunmeng/pap/a/c/c/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/xunmeng/pap/a/c/c/b;->c:Ljava/lang/String;

    new-instance p1, Lcom/xunmeng/pap/e/b;

    invoke-direct {p1}, Lcom/xunmeng/pap/e/b;-><init>()V

    iput-object p1, p0, Lcom/xunmeng/pap/a/c/c/b;->b:Lcom/xunmeng/pap/e/b;

    return-void
.end method

.method private a()[B
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PDD-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xunmeng/pap/a/c/c/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xunmeng/pap/a/c/c/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xunmeng/pap/g/a;->a([B)Lcom/xunmeng/pap/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/g/a;->b()Lcom/xunmeng/pap/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/g/a;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xunmeng/pap/g/a;->a(Ljava/lang/String;)Lcom/xunmeng/pap/g/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/g/a;->d()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lcom/xunmeng/pap/e/e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/xunmeng/pap/a/d/f;",
            ">;)",
            "Lcom/xunmeng/pap/e/e;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xunmeng/pap/a/d/f;

    invoke-virtual {v1}, Lcom/xunmeng/pap/a/d/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "$"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-ltz p1, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x24

    if-ne v2, v1, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v0

    :cond_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/xunmeng/pap/a/c/c/b;->a:Ljava/lang/String;

    const-string v2, "appPmId"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "sdkVersion"

    const-string v2, "0.0.4"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "sver"

    const-string v2, "0.0.1"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xunmeng/pap/a/c/c/b;->d:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/xunmeng/pap/g/h;->a(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/xunmeng/pap/a/c/c/b;->a()[B

    move-result-object v1

    const-string v2, "6cfe9031b6dc3f499f2a93a57eacd5be"

    invoke-static {v2}, Lcom/xunmeng/pap/g/a;->a(Ljava/lang/String;)Lcom/xunmeng/pap/g/a;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/xunmeng/pap/g/c;->a([B[BLcom/xunmeng/pap/g/a;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/xunmeng/pap/e/d;->f()Lcom/xunmeng/pap/e/d$b;

    move-result-object v1

    const-string v2, "POST"

    invoke-virtual {v1, v2}, Lcom/xunmeng/pap/e/d$b;->b(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;

    move-result-object v1

    const-string v2, "https://paptk.pinduoduo.com/t.gif"

    invoke-virtual {v1, v2}, Lcom/xunmeng/pap/e/d$b;->c(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;

    move-result-object v1

    const-string v2, "application/octet-stream"

    invoke-virtual {v1, v2}, Lcom/xunmeng/pap/e/d$b;->a(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xunmeng/pap/e/d$b;->a([B)Lcom/xunmeng/pap/e/d$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xunmeng/pap/e/d$b;->a(Ljava/util/Map;)Lcom/xunmeng/pap/e/d$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/xunmeng/pap/e/d$b;->a()Lcom/xunmeng/pap/e/d;

    move-result-object p1

    iget-object v0, p0, Lcom/xunmeng/pap/a/c/c/b;->b:Lcom/xunmeng/pap/e/b;

    invoke-virtual {v0, p1}, Lcom/xunmeng/pap/e/b;->a(Lcom/xunmeng/pap/e/d;)Lcom/xunmeng/pap/e/f/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/xunmeng/pap/e/f/a;->a()Lcom/xunmeng/pap/e/e;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "encrypt failed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "gzip failed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
