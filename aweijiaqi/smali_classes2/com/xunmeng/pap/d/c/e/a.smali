.class public Lcom/xunmeng/pap/d/c/e/a;
.super Lcom/xunmeng/pap/d/c/a;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xunmeng/pap/d/c/a;-><init>()V

    return-void
.end method

.method private a(Landroid/os/IBinder;Lcom/xunmeng/pap/d/c/c;)V
    .locals 1

    const-string v0, "Identifier"

    :try_start_0
    invoke-static {p1}, Lcom/xunmeng/pap/d/c/e/b$a;->a(Landroid/os/IBinder;)Lcom/xunmeng/pap/d/c/e/b;

    move-result-object p1

    invoke-interface {p1}, Lcom/xunmeng/pap/d/c/e/b;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xunmeng/pap/d/c/a;->b:Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/xunmeng/pap/d/c/a;->b:Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/xunmeng/pap/d/c/c;->a(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/xunmeng/pap/d/c/a;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/xunmeng/pap/g/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/xunmeng/pap/d/c/a;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/xunmeng/pap/d/c/e/a;Landroid/os/IBinder;Lcom/xunmeng/pap/d/c/c;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xunmeng/pap/d/c/e/a;->a(Landroid/os/IBinder;Lcom/xunmeng/pap/d/c/c;)V

    return-void
.end method

.method static synthetic a(Lcom/xunmeng/pap/d/c/e/a;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/xunmeng/pap/d/c/a;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private b(Landroid/content/Context;Lcom/xunmeng/pap/d/c/c;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.uodis.opendevice.OPENIDS_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.huawei.hwid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    new-instance v1, Lcom/xunmeng/pap/d/c/e/a$a;

    invoke-direct {v1, p0, p2}, Lcom/xunmeng/pap/d/c/e/a$a;-><init>(Lcom/xunmeng/pap/d/c/e/a;Lcom/xunmeng/pap/d/c/c;)V

    const/4 p2, 0x1

    invoke-virtual {p1, v0, v1, p2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Identifier"

    invoke-static {p2, p1}, Lcom/xunmeng/pap/g/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/d/c/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/xunmeng/pap/d/c/c;)V
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/d/c/a;->a:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lcom/xunmeng/pap/d/c/e/a;->b(Landroid/content/Context;Lcom/xunmeng/pap/d/c/c;)V

    return-void
.end method
