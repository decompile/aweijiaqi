.class Lcom/xunmeng/pap/d/c/d/a$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xunmeng/pap/d/c/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/xunmeng/pap/d/c/c;

.field private final b:Lcom/xunmeng/pap/d/c/d/a;


# direct methods
.method private constructor <init>(Lcom/xunmeng/pap/d/c/d/a;Lcom/xunmeng/pap/d/c/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/xunmeng/pap/d/c/d/a$b;->a:Lcom/xunmeng/pap/d/c/c;

    iput-object p1, p0, Lcom/xunmeng/pap/d/c/d/a$b;->b:Lcom/xunmeng/pap/d/c/d/a;

    return-void
.end method

.method synthetic constructor <init>(Lcom/xunmeng/pap/d/c/d/a;Lcom/xunmeng/pap/d/c/c;Lcom/xunmeng/pap/d/c/d/a$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xunmeng/pap/d/c/d/a$b;-><init>(Lcom/xunmeng/pap/d/c/d/a;Lcom/xunmeng/pap/d/c/c;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "OnSupport"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object p1

    const/4 p2, 0x1

    aget-object p1, p1, p2

    aget-object p3, p3, p2

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Class;

    const-string v2, "getOAID"

    invoke-virtual {p1, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p3, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array p2, v0, [Ljava/lang/Object;

    invoke-virtual {p1, p3, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/xunmeng/pap/d/c/d/a$b;->a:Lcom/xunmeng/pap/d/c/c;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/xunmeng/pap/d/c/d/a$b;->b:Lcom/xunmeng/pap/d/c/d/a;

    invoke-virtual {p2, p1}, Lcom/xunmeng/pap/d/c/d/a;->a(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/xunmeng/pap/d/c/d/a$b;->a:Lcom/xunmeng/pap/d/c/c;

    invoke-interface {p2, p1}, Lcom/xunmeng/pap/d/c/c;->a(Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1

    :cond_1
    new-instance p1, Lcom/xunmeng/pap/c/a;

    invoke-direct {p1}, Lcom/xunmeng/pap/c/a;-><init>()V

    throw p1

    :cond_2
    new-instance p1, Lcom/xunmeng/pap/c/a;

    invoke-direct {p1}, Lcom/xunmeng/pap/c/a;-><init>()V

    throw p1
.end method
