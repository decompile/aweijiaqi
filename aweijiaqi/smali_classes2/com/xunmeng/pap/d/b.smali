.class public Lcom/xunmeng/pap/d/b;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile c:Lcom/xunmeng/pap/d/b;


# instance fields
.field private volatile a:Ljava/lang/String;

.field private b:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xunmeng/pap/d/b;->b:Z

    return-void
.end method

.method static synthetic a(Lcom/xunmeng/pap/d/b;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xunmeng/pap/d/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v0

    const-string v1, "identifier_oaid"

    invoke-virtual {v0, v1, p1}, Lcom/xunmeng/pap/g/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b()Lcom/xunmeng/pap/d/b;
    .locals 2

    sget-object v0, Lcom/xunmeng/pap/d/b;->c:Lcom/xunmeng/pap/d/b;

    if-nez v0, :cond_1

    const-class v0, Lcom/xunmeng/pap/d/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xunmeng/pap/d/b;->c:Lcom/xunmeng/pap/d/b;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xunmeng/pap/d/b;

    invoke-direct {v1}, Lcom/xunmeng/pap/d/b;-><init>()V

    sput-object v1, Lcom/xunmeng/pap/d/b;->c:Lcom/xunmeng/pap/d/b;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xunmeng/pap/d/b;->c:Lcom/xunmeng/pap/d/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    return-object v0

    :cond_0
    invoke-static {}, Lcom/xunmeng/pap/g/g;->a()Lcom/xunmeng/pap/g/g;

    move-result-object v0

    const-string v1, ""

    const-string v2, "identifier_oaid"

    invoke-virtual {v0, v2, v1}, Lcom/xunmeng/pap/g/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    return-object v0

    :cond_1
    iget-boolean v0, p0, Lcom/xunmeng/pap/d/b;->b:Z

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v0, v2, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xunmeng/pap/d/b;->b:Z

    invoke-static {}, Lcom/xunmeng/pap/d/a;->b()Lcom/xunmeng/pap/d/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/d/a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/xunmeng/pap/d/b;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/xunmeng/pap/d/b;->a:Ljava/lang/String;

    return-object v0

    :cond_3
    return-object v1
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/xunmeng/pap/d/a;->b()Lcom/xunmeng/pap/d/a;

    move-result-object v0

    new-instance v1, Lcom/xunmeng/pap/d/b$a;

    invoke-direct {v1, p0}, Lcom/xunmeng/pap/d/b$a;-><init>(Lcom/xunmeng/pap/d/b;)V

    invoke-virtual {v0, p1, v1}, Lcom/xunmeng/pap/d/a;->a(Landroid/content/Context;Lcom/xunmeng/pap/d/c/c;)V

    return-void
.end method
