.class Lcom/xunmeng/pap/d/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xunmeng/pap/d/a$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/xunmeng/pap/d/c/a;

.field private c:J


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Identifier"

    iput-object v0, p0, Lcom/xunmeng/pap/d/a;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xunmeng/pap/d/a;->c:J

    invoke-static {}, Lcom/xunmeng/pap/d/c/b;->a()Lcom/xunmeng/pap/d/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    return-void
.end method

.method synthetic constructor <init>(Lcom/xunmeng/pap/d/a$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/xunmeng/pap/d/a;-><init>()V

    return-void
.end method

.method static b()Lcom/xunmeng/pap/d/a;
    .locals 1

    invoke-static {}, Lcom/xunmeng/pap/d/a$b;->a()Lcom/xunmeng/pap/d/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->a:Ljava/lang/String;

    const-string v1, "get oaid sync"

    invoke-static {v0, v1}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/xunmeng/pap/d/c/a;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/xunmeng/pap/d/a;->c:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x1388

    cmp-long v5, v1, v3

    if-lez v5, :cond_1

    goto :goto_2

    :cond_1
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, v3, v0

    if-lez v2, :cond_4

    const-wide/16 v0, 0x1f4

    :try_start_0
    iget-object v2, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    invoke-virtual {v2}, Lcom/xunmeng/pap/d/c/a;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    invoke-virtual {v2}, Lcom/xunmeng/pap/d/c/a;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    return-object v2

    :catchall_0
    :cond_3
    sub-long/2addr v3, v0

    goto :goto_0

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    invoke-virtual {v0}, Lcom/xunmeng/pap/d/c/a;->a()Ljava/lang/String;

    move-result-object v0

    :cond_5
    :goto_2
    return-object v0
.end method

.method a(Landroid/content/Context;Lcom/xunmeng/pap/d/c/c;)V
    .locals 2

    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->a:Ljava/lang/String;

    const-string v1, "init supplier"

    invoke-static {v0, v1}, Lcom/xunmeng/pap/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xunmeng/pap/d/a;->b:Lcom/xunmeng/pap/d/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/xunmeng/pap/d/c/a;->a(Landroid/content/Context;Lcom/xunmeng/pap/d/c/c;)V

    :cond_0
    return-void
.end method
