.class public Lcom/xunmeng/pap/e/d$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xunmeng/pap/e/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:[I

.field private d:Ljava/lang/String;

.field private e:[B

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/xunmeng/pap/e/d$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/xunmeng/pap/e/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;
    .locals 0

    iput-object p1, p0, Lcom/xunmeng/pap/e/d$b;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/xunmeng/pap/e/d$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/xunmeng/pap/e/d$b;"
        }
    .end annotation

    iget-object v0, p0, Lcom/xunmeng/pap/e/d$b;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xunmeng/pap/e/d$b;->a:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/xunmeng/pap/e/d$b;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-object p0
.end method

.method public a([B)Lcom/xunmeng/pap/e/d$b;
    .locals 0

    iput-object p1, p0, Lcom/xunmeng/pap/e/d$b;->e:[B

    iget-object p1, p0, Lcom/xunmeng/pap/e/d$b;->g:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "application/x-www-form-urlencoded"

    invoke-virtual {p0, p1}, Lcom/xunmeng/pap/e/d$b;->a(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;

    :cond_0
    return-object p0
.end method

.method public a()Lcom/xunmeng/pap/e/d;
    .locals 2

    new-instance v0, Lcom/xunmeng/pap/e/d;

    invoke-direct {v0}, Lcom/xunmeng/pap/e/d;-><init>()V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->a(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->e:[B

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->a([B)V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->b(Ljava/lang/String;)V

    iget v1, p0, Lcom/xunmeng/pap/e/d$b;->b:I

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->a(I)V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->c:[I

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->a([I)V

    iget-object v1, p0, Lcom/xunmeng/pap/e/d$b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xunmeng/pap/e/d;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;
    .locals 0

    iput-object p1, p0, Lcom/xunmeng/pap/e/d$b;->f:Ljava/lang/String;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/xunmeng/pap/e/d$b;
    .locals 0

    iput-object p1, p0, Lcom/xunmeng/pap/e/d$b;->d:Ljava/lang/String;

    return-object p0
.end method
