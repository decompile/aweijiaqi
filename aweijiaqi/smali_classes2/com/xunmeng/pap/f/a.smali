.class Lcom/xunmeng/pap/f/a;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/xunmeng/pap/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Lcom/xunmeng/pap/f/a;


# instance fields
.field private a:Lcom/xunmeng/pap/f/a;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static e()Ljava/util/concurrent/Executor;
    .locals 1

    invoke-static {}, Lcom/xunmeng/pap/f/a;->f()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-direct {v0}, Lcom/xunmeng/pap/f/a;->g()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/f/a;->a()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method private static f()Lcom/xunmeng/pap/f/a;
    .locals 2

    sget-object v0, Lcom/xunmeng/pap/f/a;->c:Lcom/xunmeng/pap/f/a;

    if-nez v0, :cond_1

    const-class v0, Lcom/xunmeng/pap/f/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xunmeng/pap/f/a;->c:Lcom/xunmeng/pap/f/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/xunmeng/pap/f/a;

    invoke-direct {v1}, Lcom/xunmeng/pap/f/a;-><init>()V

    sput-object v1, Lcom/xunmeng/pap/f/a;->c:Lcom/xunmeng/pap/f/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/xunmeng/pap/f/a;->c:Lcom/xunmeng/pap/f/a;

    return-object v0
.end method

.method private g()Lcom/xunmeng/pap/f/a;
    .locals 1

    iget-object v0, p0, Lcom/xunmeng/pap/f/a;->a:Lcom/xunmeng/pap/f/a;

    if-nez v0, :cond_0

    sget-object v0, Lcom/xunmeng/pap/f/a;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xunmeng/pap/f/a;

    iput-object v0, p0, Lcom/xunmeng/pap/f/a;->a:Lcom/xunmeng/pap/f/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xunmeng/pap/f/a;->a:Lcom/xunmeng/pap/f/a;

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    return-object p0
.end method

.method static h()Ljava/util/concurrent/Executor;
    .locals 1

    invoke-static {}, Lcom/xunmeng/pap/f/a;->f()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-direct {v0}, Lcom/xunmeng/pap/f/a;->g()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/f/a;->b()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method static i()Ljava/util/concurrent/Executor;
    .locals 1

    invoke-static {}, Lcom/xunmeng/pap/f/a;->f()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-direct {v0}, Lcom/xunmeng/pap/f/a;->g()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/f/a;->d()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method

.method static j()Ljava/util/concurrent/Executor;
    .locals 1

    invoke-static {}, Lcom/xunmeng/pap/f/a;->f()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-direct {v0}, Lcom/xunmeng/pap/f/a;->g()Lcom/xunmeng/pap/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xunmeng/pap/f/a;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Ljava/util/concurrent/Executor;
    .locals 10

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v3

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/xunmeng/pap/f/b;

    const-string v1, "Computation"

    invoke-direct {v8, v1}, Lcom/xunmeng/pap/f/b;-><init>(Ljava/lang/String;)V

    new-instance v9, Lcom/xunmeng/pap/f/b$a;

    invoke-direct {v9}, Lcom/xunmeng/pap/f/b$a;-><init>()V

    const-wide/16 v4, 0x0

    move-object v1, v0

    move v2, v3

    invoke-direct/range {v1 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-object v0
.end method

.method b()Ljava/util/concurrent/Executor;
    .locals 10

    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v6}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    new-instance v7, Lcom/xunmeng/pap/f/b;

    const-string v0, "IO"

    invoke-direct {v7, v0}, Lcom/xunmeng/pap/f/b;-><init>(Ljava/lang/String;)V

    new-instance v8, Lcom/xunmeng/pap/f/b$a;

    invoke-direct {v8}, Lcom/xunmeng/pap/f/b$a;-><init>()V

    const/4 v1, 0x0

    const v2, 0x7fffffff

    const-wide/16 v3, 0x3c

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-object v9
.end method

.method c()Ljava/util/concurrent/Executor;
    .locals 10

    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/xunmeng/pap/f/b;

    const-string v0, "Single"

    invoke-direct {v7, v0}, Lcom/xunmeng/pap/f/b;-><init>(Ljava/lang/String;)V

    new-instance v8, Lcom/xunmeng/pap/f/b$a;

    invoke-direct {v8}, Lcom/xunmeng/pap/f/b$a;-><init>()V

    const/4 v1, 0x1

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-object v9
.end method

.method d()Ljava/util/concurrent/Executor;
    .locals 1

    new-instance v0, Lcom/xunmeng/pap/f/a$a;

    invoke-direct {v0, p0}, Lcom/xunmeng/pap/f/a$a;-><init>(Lcom/xunmeng/pap/f/a;)V

    return-object v0
.end method
