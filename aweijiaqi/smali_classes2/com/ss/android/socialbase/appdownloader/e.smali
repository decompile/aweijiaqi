.class public Lcom/ss/android/socialbase/appdownloader/e;
.super Ljava/lang/Object;
.source "AppResourceUtils.java"


# direct methods
.method public static a()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_layout"

    .line 13
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static b()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_title_color"

    .line 17
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->f(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static c()I
    .locals 2

    const-string v0, "textColor"

    const-string v1, "android"

    .line 21
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static d()I
    .locals 2

    const-string v0, "textSize"

    const-string v1, "android"

    .line 25
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static e()I
    .locals 1

    const-string v0, "tt_appdownloader_style_notification_title"

    .line 29
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->d(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static f()I
    .locals 1

    const-string v0, "tt_appdownloader_root"

    .line 33
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static g()I
    .locals 1

    const-string v0, "tt_appdownloader_download_progress"

    .line 37
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static h()I
    .locals 1

    const-string v0, "tt_appdownloader_download_progress_new"

    .line 41
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static i()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success"

    .line 45
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static j()I
    .locals 1

    const-string v0, "tt_appdownloader_download_text"

    .line 49
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static k()I
    .locals 1

    const-string v0, "tt_appdownloader_action"

    .line 53
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static l()I
    .locals 1

    const-string v0, "tt_appdownloader_icon"

    .line 57
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static m()I
    .locals 1

    const-string v0, "tt_appdownloader_desc"

    .line 61
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static n()I
    .locals 1

    const-string v0, "tt_appdownloader_download_size"

    .line 65
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static o()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success_size"

    .line 69
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static p()I
    .locals 1

    const-string v0, "tt_appdownloader_download_status"

    .line 73
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static q()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success_status"

    .line 77
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static r()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_material_background_color"

    .line 81
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->f(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static s()I
    .locals 1

    const-string v0, "tt_appdownloader_action_new_bg"

    .line 85
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static t()I
    .locals 2

    const-string v0, "stat_sys_download"

    const-string v1, "android"

    .line 89
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static u()I
    .locals 2

    const-string v0, "stat_sys_warning"

    const-string v1, "android"

    .line 93
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static v()I
    .locals 2

    const-string v0, "stat_sys_download_done"

    const-string v1, "android"

    .line 97
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
