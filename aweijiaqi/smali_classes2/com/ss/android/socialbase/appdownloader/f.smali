.class public Lcom/ss/android/socialbase/appdownloader/f;
.super Ljava/lang/Object;
.source "AppTaskBuilder.java"


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Lcom/ss/android/socialbase/downloader/constants/f;

.field private K:I

.field private L:I

.field private M:Z

.field private N:Lcom/ss/android/socialbase/downloader/depend/z;

.field private O:Lcom/ss/android/socialbase/downloader/depend/n;

.field private P:Lcom/ss/android/socialbase/appdownloader/c/e;

.field private Q:Lcom/ss/android/socialbase/downloader/depend/t;

.field private R:Lcom/ss/android/socialbase/downloader/depend/r;

.field private S:Z

.field private T:Lcom/ss/android/socialbase/downloader/depend/ah;

.field private U:Z

.field private V:Lorg/json/JSONObject;

.field private W:Ljava/lang/String;

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/m;",
            ">;"
        }
    .end annotation
.end field

.field private Y:I

.field private Z:J

.field private a:Landroid/app/Activity;

.field private aa:I

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Ljava/lang/String;

.field private af:[I

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;

.field private n:Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Lcom/ss/android/socialbase/downloader/downloader/g;

.field private s:Lcom/ss/android/socialbase/downloader/downloader/h;

.field private t:Lcom/ss/android/socialbase/downloader/downloader/s;

.field private u:Lcom/ss/android/socialbase/downloader/notification/a;

.field private v:Z

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 43
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->i:Z

    const/4 v1, 0x0

    .line 44
    iput-boolean v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->j:Z

    .line 45
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->k:Z

    .line 46
    iput-boolean v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->l:Z

    const-string v1, "application/vnd.android.package-archive"

    .line 50
    iput-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->p:Ljava/lang/String;

    const/4 v1, 0x5

    .line 62
    iput v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->B:I

    .line 66
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->F:Z

    .line 70
    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/f;->a:Lcom/ss/android/socialbase/downloader/constants/f;

    iput-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->J:Lcom/ss/android/socialbase/downloader/constants/f;

    const/16 v1, 0x96

    .line 71
    iput v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->K:I

    .line 73
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->M:Z

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->X:Ljava/util/List;

    .line 89
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ac:Z

    .line 94
    iput-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ad:Z

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->b:Landroid/content/Context;

    .line 108
    iput-object p2, p0, Lcom/ss/android/socialbase/appdownloader/f;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public A()Z
    .locals 1

    .line 220
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->D:Z

    return v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->E:Ljava/lang/String;

    return-object v0
.end method

.method public C()Z
    .locals 1

    .line 228
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->F:Z

    return v0
.end method

.method public D()Z
    .locals 1

    .line 232
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->G:Z

    return v0
.end method

.method public E()Lcom/ss/android/socialbase/downloader/downloader/s;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->t:Lcom/ss/android/socialbase/downloader/downloader/s;

    return-object v0
.end method

.method public F()I
    .locals 1

    .line 240
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->K:I

    return v0
.end method

.method public G()I
    .locals 1

    .line 244
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->L:I

    return v0
.end method

.method public H()Z
    .locals 1

    .line 248
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->H:Z

    return v0
.end method

.method public I()Z
    .locals 1

    .line 252
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->I:Z

    return v0
.end method

.method public J()Z
    .locals 1

    .line 256
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->M:Z

    return v0
.end method

.method public K()Z
    .locals 1

    .line 260
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->S:Z

    return v0
.end method

.method public L()Lcom/ss/android/socialbase/downloader/constants/f;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->J:Lcom/ss/android/socialbase/downloader/constants/f;

    return-object v0
.end method

.method public M()Z
    .locals 1

    .line 268
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->A:Z

    return v0
.end method

.method public N()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method public O()Lcom/ss/android/socialbase/downloader/depend/z;
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->N:Lcom/ss/android/socialbase/downloader/depend/z;

    return-object v0
.end method

.method public P()Lcom/ss/android/socialbase/downloader/depend/n;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->O:Lcom/ss/android/socialbase/downloader/depend/n;

    return-object v0
.end method

.method public Q()Lcom/ss/android/socialbase/appdownloader/c/e;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->P:Lcom/ss/android/socialbase/appdownloader/c/e;

    return-object v0
.end method

.method public R()Lcom/ss/android/socialbase/downloader/depend/t;
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->Q:Lcom/ss/android/socialbase/downloader/depend/t;

    return-object v0
.end method

.method public S()Lcom/ss/android/socialbase/downloader/depend/ah;
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->T:Lcom/ss/android/socialbase/downloader/depend/ah;

    return-object v0
.end method

.method public T()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/m;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->X:Ljava/util/List;

    return-object v0
.end method

.method public U()Z
    .locals 1

    .line 299
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->U:Z

    return v0
.end method

.method public V()I
    .locals 1

    .line 303
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->Y:I

    return v0
.end method

.method public W()J
    .locals 2

    .line 307
    iget-wide v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->Z:J

    return-wide v0
.end method

.method public X()Z
    .locals 1

    .line 311
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ad:Z

    return v0
.end method

.method public Y()Ljava/lang/String;
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public Z()[I
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->af:[I

    return-object v0
.end method

.method public a()Landroid/app/Activity;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->a:Landroid/app/Activity;

    return-object v0
.end method

.method public a(J)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 437
    iput-wide p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->z:J

    return-object p0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/constants/f;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 502
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->J:Lcom/ss/android/socialbase/downloader/constants/f;

    return-object p0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->m:Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;

    return-object p0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/depend/m;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 2

    .line 607
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->X:Ljava/util/List;

    monitor-enter v0

    if-eqz p1, :cond_1

    .line 608
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->X:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 611
    :cond_0
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f;->X:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 612
    monitor-exit v0

    return-object p0

    .line 609
    :cond_1
    :goto_0
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p1

    .line 612
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lcom/ss/android/socialbase/downloader/depend/r;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 593
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->R:Lcom/ss/android/socialbase/downloader/depend/r;

    return-object p0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/depend/t;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->Q:Lcom/ss/android/socialbase/downloader/depend/t;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->e:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;)",
            "Lcom/ss/android/socialbase/appdownloader/f;"
        }
    .end annotation

    .line 346
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->h:Ljava/util/List;

    return-object p0
.end method

.method public a(Lorg/json/JSONObject;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 602
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->V:Lorg/json/JSONObject;

    return-object p0
.end method

.method public a(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 351
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->i:Z

    return-object p0
.end method

.method public a(I)V
    .locals 0

    .line 196
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->aa:I

    return-void
.end method

.method public aa()Z
    .locals 1

    .line 323
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ab:Z

    return v0
.end method

.method public ab()Z
    .locals 1

    .line 327
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->ac:Z

    return v0
.end method

.method public ac()Ljava/lang/String;
    .locals 1

    .line 571
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->W:Ljava/lang/String;

    return-object v0
.end method

.method public ad()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 580
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->d:Ljava/util/List;

    return-object v0
.end method

.method public ae()Lcom/ss/android/socialbase/downloader/depend/r;
    .locals 1

    .line 589
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->R:Lcom/ss/android/socialbase/downloader/depend/r;

    return-object v0
.end method

.method public af()Lorg/json/JSONObject;
    .locals 1

    .line 598
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->V:Lorg/json/JSONObject;

    return-object v0
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->b:Landroid/content/Context;

    return-object v0
.end method

.method public b(I)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 442
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->B:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->f:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/ss/android/socialbase/appdownloader/f;"
        }
    .end annotation

    .line 584
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->d:Ljava/util/List;

    return-object p0
.end method

.method public b(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 362
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->j:Z

    return-object p0
.end method

.method public c(I)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 447
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->C:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->g:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 367
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->l:Z

    return-object p0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d(I)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 487
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->K:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 382
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->o:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 392
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->q:Z

    return-object p0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e(I)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 492
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->L:I

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->p:Ljava/lang/String;

    return-object p0
.end method

.method public e(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 417
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->v:Z

    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 575
    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->Y:I

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 427
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->x:Ljava/lang/String;

    return-object p0
.end method

.method public f(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 422
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->w:Z

    return-object p0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->h:Ljava/util/List;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 432
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->y:Ljava/lang/String;

    return-object p0
.end method

.method public g(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 452
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->D:Z

    return-object p0
.end method

.method public g()Z
    .locals 1

    .line 136
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->i:Z

    return v0
.end method

.method public h(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 457
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->E:Ljava/lang/String;

    return-object p0
.end method

.method public h(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 462
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->S:Z

    return-object p0
.end method

.method public h()Z
    .locals 1

    .line 140
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->j:Z

    return v0
.end method

.method public i(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->ae:Ljava/lang/String;

    return-object p0
.end method

.method public i(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 467
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->F:Z

    return-object p0
.end method

.method public i()Z
    .locals 1

    .line 144
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->k:Z

    return v0
.end method

.method public j(Ljava/lang/String;)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 566
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->W:Ljava/lang/String;

    return-object p0
.end method

.method public j(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 472
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->G:Z

    return-object p0
.end method

.method public j()Z
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->l:Z

    return v0
.end method

.method public k(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 477
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->H:Z

    return-object p0
.end method

.method public k()Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->m:Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;

    return-object v0
.end method

.method public l(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 482
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->I:Z

    return-object p0
.end method

.method public l()Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->n:Lcom/ss/android/socialbase/downloader/depend/IDownloadListener;

    return-object v0
.end method

.method public m(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 497
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->M:Z

    return-object p0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->o:Ljava/lang/String;

    return-object v0
.end method

.method public n(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 507
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->A:Z

    return-object p0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->p:Ljava/lang/String;

    return-object v0
.end method

.method public o(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 622
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->ab:Z

    return-object p0
.end method

.method public o()Z
    .locals 1

    .line 168
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->q:Z

    return v0
.end method

.method public p(Z)Lcom/ss/android/socialbase/appdownloader/f;
    .locals 0

    .line 627
    iput-boolean p1, p0, Lcom/ss/android/socialbase/appdownloader/f;->ac:Z

    return-object p0
.end method

.method public p()Lcom/ss/android/socialbase/downloader/notification/a;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->u:Lcom/ss/android/socialbase/downloader/notification/a;

    return-object v0
.end method

.method public q()Lcom/ss/android/socialbase/downloader/downloader/h;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->s:Lcom/ss/android/socialbase/downloader/downloader/h;

    return-object v0
.end method

.method public r()Lcom/ss/android/socialbase/downloader/downloader/g;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->r:Lcom/ss/android/socialbase/downloader/downloader/g;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .line 184
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->v:Z

    return v0
.end method

.method public t()Z
    .locals 1

    .line 188
    iget-boolean v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->w:Z

    return v0
.end method

.method public u()I
    .locals 1

    .line 192
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->aa:I

    return v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->x:Ljava/lang/String;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->y:Ljava/lang/String;

    return-object v0
.end method

.method public x()J
    .locals 2

    .line 208
    iget-wide v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->z:J

    return-wide v0
.end method

.method public y()I
    .locals 1

    .line 212
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->B:I

    return v0
.end method

.method public z()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f;->C:I

    return v0
.end method
