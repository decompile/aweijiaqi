.class public final Lcom/ss/android/b/a/a/c$a;
.super Ljava/lang/Object;
.source "AdDownloadModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/b/a/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field a:Lcom/ss/android/b/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 730
    new-instance v0, Lcom/ss/android/b/a/a/c;

    invoke-direct {v0}, Lcom/ss/android/b/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 775
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput p1, v0, Lcom/ss/android/b/a/a/c;->d:I

    return-object p0
.end method

.method public a(J)Lcom/ss/android/b/a/a/c$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 750
    invoke-virtual {p0, p1, p2}, Lcom/ss/android/b/a/a/c$a;->b(J)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/ss/android/a/a/e/b;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 795
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->h:Lcom/ss/android/a/a/e/b;

    return-object p0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/depend/t;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 915
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->C:Lcom/ss/android/socialbase/downloader/depend/t;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 780
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->e:Ljava/lang/String;

    return-object p0
.end method

.method public a(Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 805
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->j:Lorg/json/JSONObject;

    return-object p0
.end method

.method public a(Z)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 770
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-boolean p1, v0, Lcom/ss/android/b/a/a/c;->c:Z

    return-object p0
.end method

.method public a()Lcom/ss/android/b/a/a/c;
    .locals 1

    .line 957
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    return-object v0
.end method

.method public b(I)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 895
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput p1, v0, Lcom/ss/android/b/a/a/c;->y:I

    return-object p0
.end method

.method public b(J)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 757
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-wide p1, v0, Lcom/ss/android/b/a/a/c;->a:J

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 785
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->f:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 855
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-boolean p1, v0, Lcom/ss/android/b/a/a/c;->r:Z

    return-object p0
.end method

.method public c(J)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 762
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-wide p1, v0, Lcom/ss/android/b/a/a/c;->b:J

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 790
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->g:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 890
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-boolean p1, v0, Lcom/ss/android/b/a/a/c;->x:Z

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 810
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->k:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 910
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-boolean p1, v0, Lcom/ss/android/b/a/a/c;->B:Z

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 835
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->n:Ljava/lang/String;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 840
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->o:Ljava/lang/String;

    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;
    .locals 1

    .line 900
    iget-object v0, p0, Lcom/ss/android/b/a/a/c$a;->a:Lcom/ss/android/b/a/a/c;

    iput-object p1, v0, Lcom/ss/android/b/a/a/c;->z:Ljava/lang/String;

    return-object p0
.end method
