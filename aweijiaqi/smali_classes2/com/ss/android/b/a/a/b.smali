.class public Lcom/ss/android/b/a/a/b;
.super Ljava/lang/Object;
.source "AdDownloadEventConfig.java"

# interfaces
.implements Lcom/ss/android/a/a/c/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/b/a/a/b$a;
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field protected g:Ljava/lang/String;

.field protected h:Ljava/lang/String;

.field protected i:I

.field protected j:Z

.field protected k:Z

.field protected l:Ljava/lang/String;

.field protected transient m:Ljava/lang/Object;

.field protected n:Lorg/json/JSONObject;

.field protected o:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b;->j:Z

    const/4 v0, 0x0

    .line 56
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b;->k:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .line 199
    iput p1, p0, Lcom/ss/android/b/a/a/b;->i:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/ss/android/b/a/a/b;->l:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/Object;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->m:Ljava/lang/Object;

    return-object v0
.end method

.method public k()I
    .locals 1

    .line 154
    iget v0, p0, Lcom/ss/android/b/a/a/b;->i:I

    return v0
.end method

.method public l()Z
    .locals 1

    .line 159
    iget-boolean v0, p0, Lcom/ss/android/b/a/a/b;->j:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .line 164
    iget-boolean v0, p0, Lcom/ss/android/b/a/a/b;->k:Z

    return v0
.end method

.method public n()Lorg/json/JSONObject;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->n:Lorg/json/JSONObject;

    return-object v0
.end method

.method public o()Lorg/json/JSONObject;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->o:Lorg/json/JSONObject;

    return-object v0
.end method
