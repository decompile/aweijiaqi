.class public Lcom/ss/android/a/a/c/g;
.super Ljava/lang/Object;
.source "SimpleDownloadEventConfig.java"

# interfaces
.implements Lcom/ss/android/a/a/c/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/a/a/c/g$a;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/ss/android/a/a/c/g$a;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->a(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->a:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->b(Lcom/ss/android/a/a/c/g$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/c/g;->b:Z

    .line 35
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->c(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->c:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->d(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->d:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->e(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->e:Ljava/lang/String;

    .line 38
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->f(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->f:Ljava/lang/String;

    .line 39
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->g(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->g:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->h(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->h:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->i(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->i:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->j(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->j:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->k(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->k:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->l(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->l:Ljava/lang/Object;

    .line 45
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->m(Lcom/ss/android/a/a/c/g$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/c/g;->m:Z

    .line 46
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->n(Lcom/ss/android/a/a/c/g$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/c/g;->n:Z

    .line 47
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->o(Lcom/ss/android/a/a/c/g$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/c/g;->o:Z

    .line 48
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->p(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/c/g;->p:Ljava/lang/String;

    .line 49
    invoke-static {p1}, Lcom/ss/android/a/a/c/g$a;->q(Lcom/ss/android/a/a/c/g$a;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/a/a/c/g;->q:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/ss/android/a/a/c/g$a;Lcom/ss/android/a/a/c/g$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/ss/android/a/a/c/g;-><init>(Lcom/ss/android/a/a/c/g$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->q:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->j:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/Object;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/ss/android/a/a/c/g;->l:Ljava/lang/Object;

    return-object v0
.end method

.method public k()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .line 109
    iget-boolean v0, p0, Lcom/ss/android/a/a/c/g;->b:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .line 114
    iget-boolean v0, p0, Lcom/ss/android/a/a/c/g;->m:Z

    return v0
.end method

.method public n()Lorg/json/JSONObject;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Lorg/json/JSONObject;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
