.class public Lcom/ss/android/a/a/e/d;
.super Ljava/lang/Object;
.source "DownloadEventModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/a/a/e/d$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:Lorg/json/JSONObject;

.field private final i:Lorg/json/JSONObject;

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:I

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/lang/String;

.field private final n:Z

.field private final o:Ljava/lang/String;

.field private final p:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lcom/ss/android/a/a/e/d$a;)V
    .locals 2

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->a(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->a:Ljava/lang/String;

    .line 93
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->b(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->b:Ljava/lang/String;

    .line 94
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->c(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->c:Ljava/lang/String;

    .line 95
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->d(Lcom/ss/android/a/a/e/d$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/e/d;->d:Z

    .line 96
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->e(Lcom/ss/android/a/a/e/d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/a/a/e/d;->e:J

    .line 97
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->f(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->f:Ljava/lang/String;

    .line 98
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->g(Lcom/ss/android/a/a/e/d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/a/a/e/d;->g:J

    .line 99
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->h(Lcom/ss/android/a/a/e/d$a;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->h:Lorg/json/JSONObject;

    .line 100
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->i(Lcom/ss/android/a/a/e/d$a;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->i:Lorg/json/JSONObject;

    .line 101
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->j(Lcom/ss/android/a/a/e/d$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->j:Ljava/util/List;

    .line 102
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->k(Lcom/ss/android/a/a/e/d$a;)I

    move-result v0

    iput v0, p0, Lcom/ss/android/a/a/e/d;->k:I

    .line 103
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->l(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->l:Ljava/lang/Object;

    .line 104
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->m(Lcom/ss/android/a/a/e/d$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/a/a/e/d;->n:Z

    .line 105
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->n(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->o:Ljava/lang/String;

    .line 106
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->o(Lcom/ss/android/a/a/e/d$a;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/a/a/e/d;->p:Lorg/json/JSONObject;

    .line 107
    invoke-static {p1}, Lcom/ss/android/a/a/e/d$a;->p(Lcom/ss/android/a/a/e/d$a;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/a/a/e/d;->m:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/ss/android/a/a/e/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/ss/android/a/a/e/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .line 285
    iget-boolean v0, p0, Lcom/ss/android/a/a/e/d;->d:Z

    return v0
.end method

.method public d()Lorg/json/JSONObject;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/ss/android/a/a/e/d;->h:Lorg/json/JSONObject;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .line 321
    iget-boolean v0, p0, Lcom/ss/android/a/a/e/d;->n:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "category: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\ttag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tlabel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nisAd: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/ss/android/a/a/e/d;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\tadId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/ss/android/a/a/e/d;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\tlogExtra: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\textValue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/ss/android/a/a/e/d;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\nextJson: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->h:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nparamsJson: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->i:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nclickTrackUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->j:Ljava/util/List;

    const-string v2, ""

    if-eqz v1, :cond_0

    .line 347
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\teventSource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/ss/android/a/a/e/d;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\textraObject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->l:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 349
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nisV3: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/ss/android/a/a/e/d;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\tV3EventName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\tV3EventParams: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/a/a/e/d;->p:Lorg/json/JSONObject;

    if-eqz v1, :cond_2

    .line 352
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
