.class public Lcom/xdandroid/hellodaemon/WakeUpReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WakeUpReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xdandroid/hellodaemon/WakeUpReceiver$WakeUpAutoStartReceiver;
    }
.end annotation


# static fields
.field protected static final ACTION_CANCEL_JOB_ALARM_SUB:Ljava/lang/String; = "com.xdandroid.hellodaemon.CANCEL_JOB_ALARM_SUB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string p2, "com.xdandroid.hellodaemon.CANCEL_JOB_ALARM_SUB"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 23
    invoke-static {}, Lcom/xdandroid/hellodaemon/WatchDogService;->cancelJobAlarmSub()V

    return-void

    .line 26
    :cond_0
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez p1, :cond_1

    return-void

    .line 27
    :cond_1
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    return-void
.end method
