.class public abstract Lcom/xdandroid/hellodaemon/AbsWorkService;
.super Landroid/app/Service;
.source "AbsWorkService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xdandroid/hellodaemon/AbsWorkService$WorkNotificationService;
    }
.end annotation


# static fields
.field protected static final HASH_CODE:I = 0x1


# instance fields
.field protected mFirstStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/xdandroid/hellodaemon/AbsWorkService;->mFirstStarted:Z

    return-void
.end method

.method public static cancelJobAlarmSub()V
    .locals 3

    .line 19
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    return-void

    .line 20
    :cond_0
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.xdandroid.hellodaemon.CANCEL_JOB_ALARM_SUB"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public abstract isWorkRunning(Landroid/content/Intent;II)Ljava/lang/Boolean;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    .line 107
    invoke-virtual {p0, p1, v0, v0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onStart(Landroid/content/Intent;II)I

    const/4 v0, 0x0

    .line 108
    invoke-virtual {p0, p1, v0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onBind(Landroid/content/Intent;Ljava/lang/Void;)Landroid/os/IBinder;

    move-result-object p1

    return-object p1
.end method

.method public abstract onBind(Landroid/content/Intent;Ljava/lang/Void;)Landroid/os/IBinder;
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    .line 131
    invoke-virtual {p0, v0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onEnd(Landroid/content/Intent;)V

    return-void
.end method

.method protected onEnd(Landroid/content/Intent;)V
    .locals 0

    .line 112
    invoke-virtual {p0, p1}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onServiceKilled(Landroid/content/Intent;)V

    .line 113
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez p1, :cond_0

    return-void

    .line 114
    :cond_0
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    .line 115
    const-class p1, Lcom/xdandroid/hellodaemon/WatchDogService;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    return-void
.end method

.method public abstract onServiceKilled(Landroid/content/Intent;)V
.end method

.method protected onStart(Landroid/content/Intent;II)I
    .locals 2

    .line 48
    const-class v0, Lcom/xdandroid/hellodaemon/WatchDogService;

    invoke-static {v0}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    .line 51
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->shouldStopService(Landroid/content/Intent;II)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->stopService(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->startService(Landroid/content/Intent;II)V

    .line 56
    :cond_1
    :goto_0
    iget-boolean p1, p0, Lcom/xdandroid/hellodaemon/AbsWorkService;->mFirstStarted:Z

    const/4 p2, 0x1

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    .line 57
    iput-boolean p1, p0, Lcom/xdandroid/hellodaemon/AbsWorkService;->mFirstStarted:Z

    .line 59
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x18

    if-gt p1, p3, :cond_2

    .line 61
    new-instance p1, Landroid/app/Notification;

    invoke-direct {p1}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, p2, p1}, Lcom/xdandroid/hellodaemon/AbsWorkService;->startForeground(ILandroid/app/Notification;)V

    .line 63
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x12

    if-lt p1, p3, :cond_2

    .line 64
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->getApplication()Landroid/app/Application;

    move-result-object p3

    const-class v0, Lcom/xdandroid/hellodaemon/AbsWorkService$WorkNotificationService;

    invoke-direct {p1, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceSafely(Landroid/content/Intent;)V

    .line 66
    :cond_2
    invoke-virtual {p0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    new-instance p3, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/xdandroid/hellodaemon/AbsWorkService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/xdandroid/hellodaemon/WatchDogService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p3, p2, p2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_3
    return p2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 0

    .line 101
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onStart(Landroid/content/Intent;II)I

    move-result p1

    return p1
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0

    .line 123
    invoke-virtual {p0, p1}, Lcom/xdandroid/hellodaemon/AbsWorkService;->onEnd(Landroid/content/Intent;)V

    return-void
.end method

.method public abstract shouldStopService(Landroid/content/Intent;II)Ljava/lang/Boolean;
.end method

.method startService(Landroid/content/Intent;II)V
    .locals 1

    .line 75
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->shouldStopService(Landroid/content/Intent;II)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 78
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->isWorkRunning(Landroid/content/Intent;II)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 81
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->startWork(Landroid/content/Intent;II)V

    return-void
.end method

.method public abstract startWork(Landroid/content/Intent;II)V
.end method

.method stopService(Landroid/content/Intent;II)V
    .locals 0

    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/AbsWorkService;->stopWork(Landroid/content/Intent;II)V

    .line 96
    invoke-static {}, Lcom/xdandroid/hellodaemon/AbsWorkService;->cancelJobAlarmSub()V

    return-void
.end method

.method public abstract stopWork(Landroid/content/Intent;II)V
.end method
