.class public Lcom/xdandroid/hellodaemon/JobSchedulerService;
.super Landroid/app/job/JobService;
.source "JobSchedulerService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .line 16
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 17
    :cond_0
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
