.class public final Lcom/xdandroid/hellodaemon/DaemonEnv;
.super Ljava/lang/Object;
.source "DaemonEnv.java"


# static fields
.field static final BIND_STATE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Service;",
            ">;",
            "Landroid/content/ServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_WAKE_UP_INTERVAL:I = 0x57e40

.field private static final MINIMAL_WAKE_UP_INTERVAL:I = 0x2bf20

.field static sApp:Landroid/content/Context; = null

.field static sInitialized:Z = false

.field static sServiceClass:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/xdandroid/hellodaemon/AbsWorkService;",
            ">;"
        }
    .end annotation
.end field

.field private static sWakeUpInterval:I = 0x57e40


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->BIND_STATE_MAP:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getWakeUpInterval()I
    .locals 2

    .line 62
    sget v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sWakeUpInterval:I

    const v1, 0x2bf20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static initialize(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/xdandroid/hellodaemon/AbsWorkService;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 29
    sput-object p0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    .line 30
    sput-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    if-eqz p2, :cond_0

    .line 31
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p0

    sput p0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sWakeUpInterval:I

    :cond_0
    const/4 p0, 0x1

    .line 32
    sput-boolean p0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    return-void
.end method

.method public static startServiceMayBind(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .line 36
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    return-void

    .line 37
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    invoke-static {v0}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceSafely(Landroid/content/Intent;)V

    .line 39
    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->BIND_STATE_MAP:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ServiceConnection;

    if-nez v1, :cond_1

    .line 40
    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    new-instance v2, Lcom/xdandroid/hellodaemon/DaemonEnv$1;

    invoke-direct {v2, p0, v0}, Lcom/xdandroid/hellodaemon/DaemonEnv$1;-><init>(Ljava/lang/Class;Landroid/content/Intent;)V

    const/4 p0, 0x1

    invoke-virtual {v1, v0, v2, p0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_1
    return-void
.end method

.method static startServiceSafely(Landroid/content/Intent;)V
    .locals 1

    .line 57
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    return-void

    .line 58
    :cond_0
    :try_start_0
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
