.class final Lcom/xdandroid/hellodaemon/DaemonEnv$1;
.super Ljava/lang/Object;
.source "DaemonEnv.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$i:Landroid/content/Intent;

.field final synthetic val$serviceClass:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/Class;Landroid/content/Intent;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$serviceClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$i:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 43
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->BIND_STATE_MAP:Ljava/util/Map;

    iget-object p2, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$serviceClass:Ljava/lang/Class;

    invoke-interface {p1, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .line 48
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->BIND_STATE_MAP:Ljava/util/Map;

    iget-object v0, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$serviceClass:Ljava/lang/Class;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object p1, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$i:Landroid/content/Intent;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceSafely(Landroid/content/Intent;)V

    .line 50
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez p1, :cond_0

    return-void

    .line 51
    :cond_0
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    iget-object v0, p0, Lcom/xdandroid/hellodaemon/DaemonEnv$1;->val$i:Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method
