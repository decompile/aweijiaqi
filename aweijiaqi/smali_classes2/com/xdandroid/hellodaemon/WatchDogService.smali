.class public Lcom/xdandroid/hellodaemon/WatchDogService;
.super Landroid/app/Service;
.source "WatchDogService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xdandroid/hellodaemon/WatchDogService$WatchDogNotificationService;
    }
.end annotation


# static fields
.field protected static final HASH_CODE:I = 0x2

.field protected static sDisposable:Lio/reactivex/disposables/Disposable;

.field protected static sPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static cancelJobAlarmSub()V
    .locals 2

    .line 123
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    return-void

    .line 124
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 125
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const-string v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const/4 v1, 0x2

    .line 126
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    goto :goto_0

    .line 128
    :cond_1
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 129
    sget-object v1, Lcom/xdandroid/hellodaemon/WatchDogService;->sPendingIntent:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 131
    :cond_2
    :goto_0
    sget-object v0, Lcom/xdandroid/hellodaemon/WatchDogService;->sDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    :cond_3
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    .line 90
    invoke-virtual {p0, p1, v0, v0}, Lcom/xdandroid/hellodaemon/WatchDogService;->onStart(Landroid/content/Intent;II)I

    const/4 p1, 0x0

    return-object p1
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    .line 113
    invoke-virtual {p0, v0}, Lcom/xdandroid/hellodaemon/WatchDogService;->onEnd(Landroid/content/Intent;)V

    return-void
.end method

.method protected onEnd(Landroid/content/Intent;)V
    .locals 0

    .line 95
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez p1, :cond_0

    return-void

    .line 96
    :cond_0
    sget-object p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    .line 97
    const-class p1, Lcom/xdandroid/hellodaemon/WatchDogService;

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceMayBind(Ljava/lang/Class;)V

    return-void
.end method

.method protected final onStart(Landroid/content/Intent;II)I
    .locals 8

    .line 33
    sget-boolean p1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    const/4 p2, 0x1

    if-nez p1, :cond_0

    return p2

    .line 35
    :cond_0
    sget-object p1, Lcom/xdandroid/hellodaemon/WatchDogService;->sDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_1

    return p2

    .line 37
    :cond_1
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x18

    const/4 v0, 0x2

    if-gt p1, p3, :cond_2

    .line 38
    new-instance p1, Landroid/app/Notification;

    invoke-direct {p1}, Landroid/app/Notification;-><init>()V

    invoke-virtual {p0, v0, p1}, Lcom/xdandroid/hellodaemon/WatchDogService;->startForeground(ILandroid/app/Notification;)V

    .line 39
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt p1, v1, :cond_2

    .line 40
    new-instance p1, Landroid/content/Intent;

    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const-class v2, Lcom/xdandroid/hellodaemon/WatchDogService$WatchDogNotificationService;

    invoke-direct {p1, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/xdandroid/hellodaemon/DaemonEnv;->startServiceSafely(Landroid/content/Intent;)V

    .line 45
    :cond_2
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt p1, v1, :cond_4

    .line 46
    new-instance p1, Landroid/app/job/JobInfo$Builder;

    new-instance v1, Landroid/content/ComponentName;

    sget-object v2, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const-class v3, Lcom/xdandroid/hellodaemon/JobSchedulerService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p1, v0, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 47
    invoke-static {}, Lcom/xdandroid/hellodaemon/DaemonEnv;->getWakeUpInterval()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p3, :cond_3

    invoke-static {}, Landroid/app/job/JobInfo;->getMinPeriodMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/app/job/JobInfo;->getMinFlexMillis()J

    move-result-wide v2

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setPeriodic(JJ)Landroid/app/job/JobInfo$Builder;

    .line 50
    :cond_3
    invoke-virtual {p1, p2}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    const-string p3, "jobscheduler"

    .line 51
    invoke-virtual {p0, p3}, Lcom/xdandroid/hellodaemon/WatchDogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/app/job/JobScheduler;

    .line 52
    invoke-virtual {p1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0

    :cond_4
    const-string p1, "alarm"

    .line 55
    invoke-virtual {p0, p1}, Lcom/xdandroid/hellodaemon/WatchDogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Landroid/app/AlarmManager;

    .line 56
    new-instance p1, Landroid/content/Intent;

    sget-object p3, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    sget-object v2, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-direct {p1, p3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    sget-object p3, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {p3, v0, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    sput-object p1, Lcom/xdandroid/hellodaemon/WatchDogService;->sPendingIntent:Landroid/app/PendingIntent;

    const/4 v2, 0x0

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Lcom/xdandroid/hellodaemon/DaemonEnv;->getWakeUpInterval()I

    move-result p1

    int-to-long v5, p1

    add-long/2addr v3, v5

    invoke-static {}, Lcom/xdandroid/hellodaemon/DaemonEnv;->getWakeUpInterval()I

    move-result p1

    int-to-long v5, p1

    sget-object v7, Lcom/xdandroid/hellodaemon/WatchDogService;->sPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual/range {v1 .. v7}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 63
    :goto_0
    invoke-static {}, Lcom/xdandroid/hellodaemon/DaemonEnv;->getWakeUpInterval()I

    move-result p1

    int-to-long v0, p1

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lio/reactivex/Flowable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Flowable;

    move-result-object p1

    new-instance p3, Lcom/xdandroid/hellodaemon/WatchDogService$1;

    invoke-direct {p3, p0}, Lcom/xdandroid/hellodaemon/WatchDogService$1;-><init>(Lcom/xdandroid/hellodaemon/WatchDogService;)V

    new-instance v0, Lcom/xdandroid/hellodaemon/WatchDogService$2;

    invoke-direct {v0, p0}, Lcom/xdandroid/hellodaemon/WatchDogService$2;-><init>(Lcom/xdandroid/hellodaemon/WatchDogService;)V

    .line 64
    invoke-virtual {p1, p3, v0}, Lio/reactivex/Flowable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    sput-object p1, Lcom/xdandroid/hellodaemon/WatchDogService;->sDisposable:Lio/reactivex/disposables/Disposable;

    .line 77
    invoke-virtual {p0}, Lcom/xdandroid/hellodaemon/WatchDogService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    new-instance p3, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/xdandroid/hellodaemon/WatchDogService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sServiceClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p3, p2, p2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return p2
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 0

    .line 85
    invoke-virtual {p0, p1, p2, p3}, Lcom/xdandroid/hellodaemon/WatchDogService;->onStart(Landroid/content/Intent;II)I

    move-result p1

    return p1
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0

    .line 105
    invoke-virtual {p0, p1}, Lcom/xdandroid/hellodaemon/WatchDogService;->onEnd(Landroid/content/Intent;)V

    return-void
.end method
