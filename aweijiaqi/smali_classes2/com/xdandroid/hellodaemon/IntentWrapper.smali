.class public Lcom/xdandroid/hellodaemon/IntentWrapper;
.super Ljava/lang/Object;
.source "IntentWrapper.java"


# static fields
.field protected static final COOLPAD:I = 0x71

.field protected static final DOZE:I = 0x62

.field protected static final GIONEE:I = 0x6e

.field protected static final HUAWEI:I = 0x63

.field protected static final HUAWEI_GOD:I = 0x64

.field protected static final LENOVO:I = 0x72

.field protected static final LENOVO_GOD:I = 0x73

.field protected static final LETV:I = 0x6f

.field protected static final LETV_GOD:I = 0x70

.field protected static final MEIZU:I = 0x68

.field protected static final MEIZU_GOD:I = 0x69

.field protected static final OPPO:I = 0x6a

.field protected static final OPPO_OLD:I = 0x6c

.field protected static final SAMSUNG_L:I = 0x67

.field protected static final SAMSUNG_M:I = 0x6b

.field protected static final VIVO_GOD:I = 0x6d

.field protected static final XIAOMI:I = 0x65

.field protected static final XIAOMI_GOD:I = 0x66

.field protected static final ZTE:I = 0x74

.field protected static final ZTE_GOD:I = 0x75

.field protected static sApplicationName:Ljava/lang/String;

.field protected static sIntentWrapperList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xdandroid/hellodaemon/IntentWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected intent:Landroid/content/Intent;

.field protected type:I


# direct methods
.method protected constructor <init>(Landroid/content/Intent;I)V
    .locals 0

    .line 415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    iput-object p1, p0, Lcom/xdandroid/hellodaemon/IntentWrapper;->intent:Landroid/content/Intent;

    .line 417
    iput p2, p0, Lcom/xdandroid/hellodaemon/IntentWrapper;->type:I

    return-void
.end method

.method public static getApplicationName()Ljava/lang/String;
    .locals 3

    .line 180
    sget-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sApplicationName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 181
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 185
    :cond_0
    :try_start_0
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 186
    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sApplicationName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 189
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 190
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sApplicationName:Ljava/lang/String;

    .line 193
    :cond_1
    :goto_0
    sget-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sApplicationName:Ljava/lang/String;

    return-object v0
.end method

.method public static getIntentWrapperList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/xdandroid/hellodaemon/IntentWrapper;",
            ">;"
        }
    .end annotation

    .line 59
    sget-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    if-nez v0, :cond_3

    .line 61
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 63
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    .line 66
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    .line 67
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 68
    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->isIgnoringBatteryOptimizations(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 72
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x62

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "huawei.intent.action.HSM_BOOTAPP_MANAGER"

    .line 78
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x63

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 83
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.huawei.systemmanager"

    const-string v3, "com.huawei.systemmanager.optimize.process.ProtectActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 84
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x64

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "miui.intent.action.OP_AUTO_START"

    .line 88
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.DEFAULT"

    .line 89
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    sget-object v2, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x65

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 94
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.miui.powerkeeper"

    const-string v4, "com.miui.powerkeeper.ui.HiddenAppsConfigActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 95
    sget-object v2, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "package_name"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "package_label"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    sget-object v2, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x66

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.samsung.android.sm"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 101
    sget-object v2, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x67

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 105
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.sm_cn"

    const-string v4, "com.samsung.android.sm.ui.battery.BatteryActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 106
    sget-object v2, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x6b

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.meizu.safe.security.SHOW_APPSEC"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    sget-object v1, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x68

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 116
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.meizu.safe"

    const-string v3, "com.meizu.safe.powerui.PowerAppPermissionActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 117
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x69

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.coloros.safecenter"

    const-string v3, "com.coloros.safecenter.permission.startup.StartupAppListActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 122
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x6a

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 126
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.color.safecenter"

    const-string v3, "com.color.safecenter.permission.startup.StartupAppListActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 127
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x6c

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 131
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.vivo.abe"

    const-string v3, "com.vivo.applicationbehaviorengine.ui.ExcessivePowerManagerActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 132
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x6d

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 136
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.gionee.softmanager"

    const-string v3, "com.gionee.softmanager.MainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 137
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x6e

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 141
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.letv.android.letvsafe"

    const-string v3, "com.letv.android.letvsafe.AutobootManageActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 142
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x6f

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 146
    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.letv.android.letvsafe.BackgroundAppManageActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 147
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x70

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 151
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.yulong.android.security"

    const-string v3, "com.yulong.android.seccenter.tabbarmain"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 152
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x71

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 156
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.lenovo.security"

    const-string v3, "com.lenovo.security.purebackground.PureBackgroundActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 157
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x72

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 161
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.lenovo.powersetting"

    const-string v3, "com.lenovo.powersetting.ui.Settings$HighPowerApplicationsActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 162
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x73

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 166
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.zte.heartyservice"

    const-string v3, "com.zte.heartyservice.autorun.AppAutoRunManager"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 167
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v3, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v4, 0x74

    invoke-direct {v3, v0, v4}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 171
    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.zte.heartyservice.setting.ClearAppSettingsActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 172
    sget-object v1, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    new-instance v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    const/16 v3, 0x75

    invoke-direct {v2, v0, v3}, Lcom/xdandroid/hellodaemon/IntentWrapper;-><init>(Landroid/content/Intent;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_3
    sget-object v0, Lcom/xdandroid/hellodaemon/IntentWrapper;->sIntentWrapperList:Ljava/util/List;

    return-object v0
.end method

.method public static onBackPressed(Landroid/app/Activity;)V
    .locals 2

    .line 407
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.HOME"

    .line 408
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static whiteListMatters(Landroid/app/Activity;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/xdandroid/hellodaemon/IntentWrapper;",
            ">;"
        }
    .end annotation

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    const-string p1, "\u6838\u5fc3\u670d\u52a1\u7684\u6301\u7eed\u8fd0\u884c"

    .line 204
    :cond_0
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getIntentWrapperList()Ljava/util/List;

    move-result-object v1

    .line 205
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/xdandroid/hellodaemon/IntentWrapper;

    .line 207
    invoke-virtual {v2}, Lcom/xdandroid/hellodaemon/IntentWrapper;->doesActivityExists()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 208
    :cond_2
    iget v3, v2, Lcom/xdandroid/hellodaemon/IntentWrapper;->type:I

    const-string v4, " \u5bf9\u5e94\u7684\u5f00\u5173\u5173\u95ed\u3002"

    const-string v5, " \u7684\u540e\u53f0\u8fd0\u884c"

    const-string v6, "\u9700\u8981\u5173\u95ed "

    const-string v7, " \u7684\u81ea\u542f\u52a8"

    const-string v8, "\u9700\u8981 "

    const-string v9, " \u5bf9\u5e94\u7684\u5f00\u5173\u6253\u5f00\u3002"

    const-string v10, "\u9700\u8981\u5141\u8bb8 "

    const-string v11, "\u786e\u5b9a"

    const/4 v12, 0x0

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 387
    :pswitch_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 388
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " \u7684\u540e\u53f0\u8017\u7535\u4f18\u5316"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " \u7684\u540e\u53f0\u8017\u7535\u4f18\u5316\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u540e\u53f0\u8017\u7535\u4f18\u5316\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 390
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$15;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$15;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 392
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 395
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 396
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 375
    :pswitch_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 376
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u540e\u53f0\u81ea\u542f\u3001\u540e\u53f0 GPS \u548c\u540e\u53f0\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u540e\u53f0\u7ba1\u7406\u300f\u4e2d\uff0c\u5206\u522b\u627e\u5230\u300e\u540e\u53f0\u81ea\u542f\u300f\u3001\u300e\u540e\u53f0 GPS\u300f\u548c\u300e\u540e\u53f0\u8fd0\u884c\u300f\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 378
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$14;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$14;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 380
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 383
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 384
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 327
    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 328
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u81ea\u542f\u52a8\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u9177\u7ba1\u5bb6\u300f\u4e2d\uff0c\u627e\u5230\u300e\u8f6f\u4ef6\u7ba1\u7406\u300f->\u300e\u81ea\u542f\u52a8\u7ba1\u7406\u300f\uff0c\u53d6\u6d88\u52fe\u9009 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u72b6\u6001\u6539\u4e3a\u300e\u5df2\u5141\u8bb8\u300f\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 330
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$10;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$10;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 332
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 335
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 336
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 363
    :pswitch_3
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 364
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\u9700\u8981\u7981\u6b62 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " \u88ab\u81ea\u52a8\u6e05\u7406"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " \u88ab\u81ea\u52a8\u6e05\u7406\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u5e94\u7528\u4fdd\u62a4\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 366
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$13;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$13;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 368
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 371
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 372
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 351
    :pswitch_4
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 352
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 353
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u9700\u8981\u52a0\u5165\u5e94\u7528\u81ea\u542f\u548c\u7eff\u8272\u540e\u53f0\u767d\u540d\u5355"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u81ea\u542f\u52a8\u548c\u540e\u53f0\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u7cfb\u7edf\u7ba1\u5bb6\u300f\u4e2d\uff0c\u5206\u522b\u627e\u5230\u300e\u5e94\u7528\u7ba1\u7406\u300f->\u300e\u5e94\u7528\u81ea\u542f\u300f\u548c\u300e\u7eff\u8272\u540e\u53f0\u300f->\u300e\u6e05\u7406\u767d\u540d\u5355\u300f\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u6dfb\u52a0\u5230\u767d\u540d\u5355\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 354
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$12;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$12;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 356
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 359
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 360
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 339
    :pswitch_5
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 340
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5728\u540e\u53f0\u9ad8\u8017\u7535\u65f6\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u540e\u53f0\u9ad8\u8017\u7535\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 342
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$11;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$11;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 344
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 347
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 348
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 275
    :pswitch_6
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 276
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5728\u5c4f\u5e55\u5173\u95ed\u65f6\u7ee7\u7eed\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u7535\u6c60\u300f\u9875\u9762\u4e2d\uff0c\u70b9\u51fb\u300e\u672a\u76d1\u89c6\u7684\u5e94\u7528\u7a0b\u5e8f\u300f->\u300e\u6dfb\u52a0\u5e94\u7528\u7a0b\u5e8f\u300f\uff0c\u52fe\u9009 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\uff0c\u7136\u540e\u70b9\u51fb\u300e\u5b8c\u6210\u300f\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 278
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$6;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$6;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 280
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 283
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 284
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 299
    :pswitch_7
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 300
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 301
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u9700\u8981\u5728\u5f85\u673a\u65f6\u4fdd\u6301\u8fd0\u884c"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5728\u5f85\u673a\u65f6\u4fdd\u6301\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u5f85\u673a\u8017\u7535\u7ba1\u7406\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 302
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$8;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$8;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 304
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 307
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 308
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 287
    :pswitch_8
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 288
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u4fdd\u6301\u540e\u53f0\u8fd0\u884c"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u4fdd\u6301\u540e\u53f0\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u5e94\u7528\u4fe1\u606f\u754c\u9762\u4e2d\uff0c\u5c06\u300e\u540e\u53f0\u7ba1\u7406\u300f\u9009\u9879\u66f4\u6539\u4e3a\u300e\u4fdd\u6301\u540e\u53f0\u8fd0\u884c\u300f\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$7;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$7;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 292
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 295
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 296
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 263
    :pswitch_9
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 264
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u5728\u5c4f\u5e55\u5173\u95ed\u65f6\u7ee7\u7eed\u8fd0\u884c\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u667a\u80fd\u7ba1\u7406\u5668\u300f\u4e2d\uff0c\u70b9\u51fb\u300e\u5185\u5b58\u300f\uff0c\u9009\u62e9\u300e\u81ea\u542f\u52a8\u5e94\u7528\u7a0b\u5e8f\u300f\u9009\u9879\u5361\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 266
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$5;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$5;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 268
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 271
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 272
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 251
    :pswitch_a
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 252
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u795e\u9690\u6a21\u5f0f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u795e\u9690\u6a21\u5f0f\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u795e\u9690\u6a21\u5f0f\u8bbe\u7f6e\u4e2d\uff0c\u9009\u62e9\u300e\u65e0\u9650\u5236\u300f\uff0c\u7136\u540e\u9009\u62e9\u300e\u5141\u8bb8\u5b9a\u4f4d\u300f\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 254
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$4;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$4;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 256
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 259
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 260
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 315
    :pswitch_b
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 316
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u52a0\u5165\u5230\u81ea\u542f\u52a8\u767d\u540d\u5355\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u81ea\u542f\u52a8\u7ba1\u7406\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 318
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$9;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$9;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 320
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 323
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 324
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 239
    :pswitch_c
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 240
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u9700\u8981\u52a0\u5165\u9501\u5c4f\u6e05\u7406\u767d\u540d\u5355"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u52a0\u5165\u5230\u9501\u5c4f\u6e05\u7406\u767d\u540d\u5355\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u9501\u5c4f\u6e05\u7406\u300f\u5217\u8868\u4e2d\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 242
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$3;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$3;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 244
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 247
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 248
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 226
    :pswitch_d
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 227
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u81ea\u52a8\u542f\u52a8"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u81ea\u52a8\u542f\u52a8\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u81ea\u542f\u7ba1\u7406\u300f\u4e2d\uff0c\u5c06 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 229
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$2;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$2;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 231
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 234
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 235
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 210
    :pswitch_e
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_1

    const-string v3, "power"

    .line 211
    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 212
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/PowerManager;->isIgnoringBatteryOptimizations(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0

    .line 213
    :cond_3
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 214
    invoke-virtual {v3, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u9700\u8981\u5ffd\u7565 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u7684\u7535\u6c60\u4f18\u5316"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-static {}, Lcom/xdandroid/hellodaemon/IntentWrapper;->getApplicationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " \u52a0\u5165\u5230\u7535\u6c60\u4f18\u5316\u7684\u5ffd\u7565\u540d\u5355\u3002\n\n\u8bf7\u70b9\u51fb\u300e\u786e\u5b9a\u300f\uff0c\u5728\u5f39\u51fa\u7684\u300e\u5ffd\u7565\u7535\u6c60\u4f18\u5316\u300f\u5bf9\u8bdd\u6846\u4e2d\uff0c\u9009\u62e9\u300e\u662f\u300f\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/xdandroid/hellodaemon/IntentWrapper$1;

    invoke-direct {v4, v2, p0}, Lcom/xdandroid/hellodaemon/IntentWrapper$1;-><init>(Lcom/xdandroid/hellodaemon/IntentWrapper;Landroid/app/Activity;)V

    .line 218
    invoke-virtual {v3, v11, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 221
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 222
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    return-object v0

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_b
        :pswitch_6
        :pswitch_b
        :pswitch_5
        :pswitch_4
        :pswitch_b
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method protected doesActivityExists()Z
    .locals 4

    .line 424
    sget-boolean v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sInitialized:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 425
    :cond_0
    sget-object v0, Lcom/xdandroid/hellodaemon/DaemonEnv;->sApp:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 426
    iget-object v2, p0, Lcom/xdandroid/hellodaemon/IntentWrapper;->intent:Landroid/content/Intent;

    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 427
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public startActivitySafely(Landroid/app/Activity;)V
    .locals 1

    .line 434
    :try_start_0
    iget-object v0, p0, Lcom/xdandroid/hellodaemon/IntentWrapper;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method
