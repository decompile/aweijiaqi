.class final Lms/bd/c/d1;
.super Lms/bd/c/b$a;
.source ""


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lms/bd/c/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(IJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-static {}, Lms/bd/c/z1;->a()Lms/bd/c/z1;

    move-result-object p1

    invoke-virtual {p1}, Lms/bd/c/z1;->b()Ljava/lang/Throwable;

    move-result-object p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    return-object p2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p1

    array-length p3, p1

    const/4 p4, 0x4

    if-ge p3, p4, :cond_1

    return-object p2

    :cond_1
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const/4 p3, 0x1

    :goto_0
    if-ge p3, p4, :cond_4

    aget-object p5, p1, p3

    if-eqz p5, :cond_3

    aget-object p5, p1, p3

    invoke-virtual {p5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object p5

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-eqz p5, :cond_2

    goto :goto_1

    :cond_2
    aget-object p5, p1, p3

    invoke-virtual {p5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object p5

    invoke-static {p5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p5

    invoke-static {}, Lms/bd/c/z1;->a()Lms/bd/c/z1;

    move-result-object v0

    aget-object v1, p1, p3

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p5, v1}, Lms/bd/c/z1;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;

    move-result-object p5

    invoke-interface {p2, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_4
    return-object p2
.end method
