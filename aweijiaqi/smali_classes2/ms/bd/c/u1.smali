.class public final Lms/bd/c/u1;
.super Lms/bd/c/t1;
.source ""


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lms/bd/c/t1;-><init>()V

    return-void
.end method

.method private declared-synchronized a(Lcom/bytedance/frameworks/baselib/network/http/NetworkParams$AddSecurityFactorProcessCallback;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/bytedance/frameworks/baselib/network/http/NetworkParams;->setAddSecurityFactorProcessCallback(Lcom/bytedance/frameworks/baselib/network/http/NetworkParams$AddSecurityFactorProcessCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method protected a(J)Ljava/lang/Object;
    .locals 0

    iput-wide p1, p0, Lms/bd/c/u1;->a:J

    new-instance p1, Lms/bd/c/u1$a;

    invoke-direct {p1, p0}, Lms/bd/c/u1$a;-><init>(Lms/bd/c/u1;)V

    invoke-direct {p0, p1}, Lms/bd/c/u1;->a(Lcom/bytedance/frameworks/baselib/network/http/NetworkParams$AddSecurityFactorProcessCallback;)V

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-wide v0, p0, Lms/bd/c/u1;->a:J

    const v2, 0x3000002

    invoke-static {v2, v0, v1}, Lms/bd/c/b;->a(IJ)Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lms/bd/c/u1;->a:J

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method
