.class public final Lms/bd/c/g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a()V
    .locals 2

    .line 1
    new-instance v0, Lms/bd/c/c;

    invoke-direct {v0}, Lms/bd/c/c;-><init>()V

    const v1, 0x10001

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance v0, Lms/bd/c/d;

    invoke-direct {v0}, Lms/bd/c/d;-><init>()V

    const v1, 0x10002

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance v0, Lms/bd/c/e;

    invoke-direct {v0}, Lms/bd/c/e;-><init>()V

    const v1, 0x10003

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance v0, Lms/bd/c/f;

    invoke-direct {v0}, Lms/bd/c/f;-><init>()V

    const v1, 0x10004

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    .line 2
    invoke-static {}, Lms/bd/c/b0;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/bytedance/mobsec/matrix/net/TTNetHttpClient;

    invoke-direct {v0}, Lcom/bytedance/mobsec/matrix/net/TTNetHttpClient;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/bytedance/mobsec/matrix/net/b;

    invoke-direct {v0}, Lcom/bytedance/mobsec/matrix/net/b;-><init>()V

    :goto_0
    const v1, 0x30001

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    const v1, 0x30002

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    const v1, 0x30003

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    .line 3
    invoke-static {}, Lms/bd/c/b0;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lms/bd/c/k;

    invoke-direct {v0}, Lms/bd/c/k;-><init>()V

    goto :goto_1

    :cond_1
    new-instance v0, Lms/bd/c/i;

    invoke-direct {v0}, Lms/bd/c/i;-><init>()V

    :goto_1
    invoke-static {v0}, Lms/bd/c/j;->a(Lms/bd/c/j;)V

    const v1, 0x20001

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    const v1, 0x20002

    invoke-static {v1, v0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    invoke-static {p0, p1, p2}, Lms/bd/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    if-lez v5, :cond_0

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    array-length v5, v4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v4, v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const v9, 0x1000001

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-string v13, "3ed9fb"

    :try_start_1
    new-array v14, v2, [B

    const/16 v15, 0x6d

    aput-byte v15, v14, v3

    invoke-static/range {v9 .. v14}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const v10, 0x1000001

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-string v14, "cd136d"

    :try_start_2
    new-array v15, v2, [B

    const/16 v16, 0x3d

    aput-byte v16, v15, v3

    invoke-static/range {v10 .. v15}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v9, p0

    invoke-static {v9, v8, v7}, Lms/bd/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v9, p0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v1, 0x400

    new-array v1, v1, [B

    :goto_1
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    invoke-virtual {v4, v1, v3, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    const/4 v0, 0x3

    new-array v9, v0, [B

    fill-array-data v9, :array_0

    const v4, 0x1000001

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-string v8, "10d524"

    invoke-static/range {v4 .. v9}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    :cond_2
    :goto_2
    return v2

    :array_0
    .array-data 1
        0x23t
        0x33t
        0x11t
    .end array-data
.end method
