.class public final Lms/bd/c/k;
.super Lms/bd/c/j;
.source ""


# instance fields
.field private a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lms/bd/c/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)V
    .locals 11

    move-object v0, p0

    iget-object v1, v0, Lms/bd/c/k;->a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-virtual/range {v1 .. v10}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;->monitorSLA(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 1

    iget-object v0, p0, Lms/bd/c/k;->a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lms/bd/c/k;->a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;->monitorEvent(Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONObject;Lorg/json/JSONObject;)V

    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {}, Lms/bd/c/a;->a()Lms/bd/c/a;

    move-result-object v2

    invoke-virtual {v2}, Lms/bd/c/a;->b()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-array v11, v4, [B

    fill-array-data v11, :array_0

    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "850140"

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_0
    move-object v5, v3

    :goto_0
    const/4 v1, 0x0

    if-nez v5, :cond_1

    return v1

    :cond_1
    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "5718bb"

    const/16 v12, 0xa

    const/4 v13, 0x7

    const/4 v14, 0x5

    const/4 v15, 0x1

    :try_start_1
    new-array v11, v13, [B

    const/16 v16, 0x37

    aput-byte v16, v11, v1

    const/16 v16, 0x31

    aput-byte v16, v11, v15

    const/16 v17, 0x49

    const/16 v18, 0x2

    aput-byte v17, v11, v18

    const/16 v17, 0x73

    aput-byte v17, v11, v4

    const/16 v17, 0x5c

    const/16 v19, 0x4

    aput-byte v17, v11, v19

    const/16 v17, 0x7c

    aput-byte v17, v11, v14

    const/16 v17, 0x32

    const/16 v20, 0x6

    aput-byte v17, v11, v20

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "fa53bb"

    const/16 v7, 0x8

    :try_start_2
    new-array v8, v7, [B

    const/16 v9, 0x7f

    aput-byte v9, v8, v1

    const/16 v9, 0x6c

    aput-byte v9, v8, v15

    const/16 v10, 0x55

    aput-byte v10, v8, v18

    const/16 v10, 0x53

    aput-byte v10, v8, v4

    const/16 v10, 0x62

    aput-byte v10, v8, v19

    const/16 v10, 0x74

    aput-byte v10, v8, v14

    aput-byte v9, v8, v20

    const/16 v9, 0x44

    aput-byte v9, v8, v13

    move-object/from16 v26, v8

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "3b36fe"

    :try_start_3
    new-array v11, v12, [B

    const/16 v17, 0x21

    aput-byte v17, v11, v1

    const/16 v17, 0x6f

    aput-byte v17, v11, v15

    const/16 v17, 0x4e

    aput-byte v17, v11, v18

    aput-byte v9, v11, v4

    const/16 v9, 0x50

    aput-byte v9, v11, v19

    const/16 v27, 0x75

    aput-byte v27, v11, v14

    aput-byte v14, v11, v20

    const/16 v26, 0x71

    aput-byte v26, v11, v13

    aput-byte v17, v11, v7

    const/16 v28, 0x9

    aput-byte v27, v11, v28

    move-object/from16 v26, v11

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "2f3553"

    :try_start_4
    new-array v7, v12, [B

    aput-byte v16, v7, v1

    const/16 v16, 0x61

    aput-byte v16, v7, v15

    aput-byte v9, v7, v18

    aput-byte v17, v7, v4

    const/16 v4, 0x18

    aput-byte v4, v7, v19

    const/16 v4, 0x30

    aput-byte v4, v7, v14

    aput-byte v19, v7, v20

    aput-byte v27, v7, v13

    const/16 v4, 0x8

    aput-byte v17, v7, v4

    const/16 v4, 0x76

    aput-byte v4, v7, v28

    move-object/from16 v26, v7

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v7, v9, :cond_2

    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v7, v9, :cond_3

    invoke-virtual {v4, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    invoke-static {v6, v8}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitorUtils;->setConfigUrl(Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v6, v10}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitorUtils;->setDefaultReportUrl(Ljava/lang/String;Ljava/util/List;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    :cond_4
    return v1

    :catch_1
    move-object v1, v3

    move-object v3, v6

    goto :goto_3

    :catch_2
    move-object v1, v3

    :goto_3
    new-array v11, v14, [B

    fill-array-data v11, :array_1

    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "262018"

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v6, v3

    move-object v3, v1

    :goto_4
    new-array v1, v12, [B

    fill-array-data v1, :array_2

    const v16, 0x1000001

    const/16 v17, 0x0

    const-wide/16 v18, 0x0

    const-string v20, "69bb53"

    move-object/from16 v21, v1

    invoke-static/range {v16 .. v21}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    new-array v1, v12, [B

    fill-array-data v1, :array_3

    const-string v20, "d639cb"

    move-object/from16 v21, v1

    invoke-static/range {v16 .. v21}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    new-instance v1, Lms/bd/c/k$a;

    invoke-direct {v1, v0, v5, v3}, Lms/bd/c/k$a;-><init>(Lms/bd/c/k;Lorg/json/JSONObject;Ljava/lang/String;)V

    invoke-static {v2, v6, v5, v1}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitorUtils;->initMonitor(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor$IGetExtendParams;)V

    invoke-static {v6}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitorUtils;->getInstance(Ljava/lang/String;)Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    move-result-object v1

    iput-object v1, v0, Lms/bd/c/k;->a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    return v15

    nop

    :array_0
    .array-data 1
        0x3at
        0x32t
        0x4et
    .end array-data

    :array_1
    .array-data 1
        0x30t
        0x31t
        0x4ct
        0x7bt
        0x7t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x24t
        0x34t
        0x1ft
        0x10t
        0x3t
        0x23t
        0x0t
        0x2at
        0x1ft
        0x21t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x67t
        0x31t
        0x50t
        0x42t
        0x4et
        0x61t
        0x52t
        0x25t
        0x4et
        0x7at
    .end array-data
.end method

.method public b(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)V
    .locals 11

    move-object v0, p0

    iget-object v1, v0, Lms/bd/c/k;->a:Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-wide v2, p1

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-virtual/range {v1 .. v10}, Lcom/bytedance/framwork/core/sdkmonitor/SDKMonitor;->monitorApiError(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/json/JSONObject;)V

    return-void
.end method
