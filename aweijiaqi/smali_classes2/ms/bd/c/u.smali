.class public Lms/bd/c/u;
.super Lms/bd/c/t$b;
.source ""


# instance fields
.field private final g:Lms/bd/c/w;


# direct methods
.method public constructor <init>(ZLms/bd/c/w;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lms/bd/c/t$b;-><init>()V

    iput-boolean p1, p0, Lms/bd/c/t$b;->a:Z

    iput-object p2, p0, Lms/bd/c/u;->g:Lms/bd/c/w;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz p1, :cond_0

    sget-object p1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    :goto_0
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const-wide/16 v2, 0x10

    const/4 p1, 0x2

    .line 1
    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    const-wide/16 v2, 0x1c

    .line 2
    invoke-virtual {p2, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    .line 3
    iput-wide v2, p0, Lms/bd/c/t$b;->b:J

    const-wide/16 v2, 0x20

    .line 4
    invoke-virtual {p2, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    int-to-long v2, v0

    and-long/2addr v2, v4

    .line 5
    iput-wide v2, p0, Lms/bd/c/t$b;->c:J

    const-wide/16 v2, 0x2a

    .line 6
    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const v2, 0xffff

    and-int/2addr v0, v2

    .line 7
    iput v0, p0, Lms/bd/c/t$b;->d:I

    const-wide/16 v3, 0x2c

    .line 8
    invoke-virtual {p2, v1, v3, v4, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v2

    .line 9
    iput v0, p0, Lms/bd/c/t$b;->e:I

    const-wide/16 v3, 0x2e

    .line 10
    invoke-virtual {p2, v1, v3, v4, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v2

    .line 11
    iput v0, p0, Lms/bd/c/t$b;->f:I

    const-wide/16 v2, 0x30

    .line 12
    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    const-wide/16 v2, 0x32

    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    return-void
.end method


# virtual methods
.method public a(JI)Lms/bd/c/t$a;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Lms/bd/c/r;

    iget-object v1, p0, Lms/bd/c/u;->g:Lms/bd/c/w;

    move-object v0, v6

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lms/bd/c/r;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;JI)V

    return-object v6
.end method

.method public a(J)Lms/bd/c/t$c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lms/bd/c/x;

    iget-object v1, p0, Lms/bd/c/u;->g:Lms/bd/c/w;

    invoke-direct {v0, v1, p0, p1, p2}, Lms/bd/c/x;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;J)V

    return-object v0
.end method

.method public a(I)Lms/bd/c/t$d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lms/bd/c/z;

    iget-object v1, p0, Lms/bd/c/u;->g:Lms/bd/c/w;

    invoke-direct {v0, v1, p0, p1}, Lms/bd/c/z;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;I)V

    return-object v0
.end method
