.class public Lms/bd/c/z;
.super Lms/bd/c/t$d;
.source ""


# direct methods
.method public constructor <init>(Lms/bd/c/w;Lms/bd/c/t$b;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lms/bd/c/t$d;-><init>()V

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-boolean v2, p2, Lms/bd/c/t$b;->a:Z

    if-eqz v2, :cond_0

    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    :cond_0
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    :goto_0
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-wide v2, p2, Lms/bd/c/t$b;->c:J

    iget p2, p2, Lms/bd/c/t$b;->f:I

    mul-int p3, p3, p2

    int-to-long p2, p3

    add-long/2addr v2, p2

    const-wide/16 p2, 0x1c

    add-long/2addr v2, p2

    .line 1
    invoke-virtual {p1, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p1

    int-to-long p1, p1

    const-wide v0, 0xffffffffL

    and-long/2addr p1, v0

    .line 2
    iput-wide p1, p0, Lms/bd/c/t$d;->a:J

    return-void
.end method
