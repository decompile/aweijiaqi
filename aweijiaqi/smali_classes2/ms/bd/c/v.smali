.class public Lms/bd/c/v;
.super Lms/bd/c/t$b;
.source ""


# instance fields
.field private final g:Lms/bd/c/w;


# direct methods
.method public constructor <init>(ZLms/bd/c/w;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lms/bd/c/t$b;-><init>()V

    iput-boolean p1, p0, Lms/bd/c/t$b;->a:Z

    iput-object p2, p0, Lms/bd/c/v;->g:Lms/bd/c/w;

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz p1, :cond_0

    sget-object p1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    :goto_0
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const-wide/16 v2, 0x10

    const/4 p1, 0x2

    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    const-wide/16 v2, 0x20

    invoke-virtual {p2, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    iput-wide v2, p0, Lms/bd/c/t$b;->b:J

    const-wide/16 v2, 0x28

    invoke-virtual {p2, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    iput-wide v2, p0, Lms/bd/c/t$b;->c:J

    const-wide/16 v2, 0x36

    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const v2, 0xffff

    and-int/2addr v0, v2

    iput v0, p0, Lms/bd/c/t$b;->d:I

    const-wide/16 v3, 0x38

    invoke-virtual {p2, v1, v3, v4, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v2

    iput v0, p0, Lms/bd/c/t$b;->e:I

    const-wide/16 v3, 0x3a

    invoke-virtual {p2, v1, v3, v4, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v2

    iput v0, p0, Lms/bd/c/t$b;->f:I

    const-wide/16 v2, 0x3c

    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    const-wide/16 v2, 0x3e

    invoke-virtual {p2, v1, v2, v3, p1}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    return-void
.end method


# virtual methods
.method public a(JI)Lms/bd/c/t$a;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Lms/bd/c/s;

    iget-object v1, p0, Lms/bd/c/v;->g:Lms/bd/c/w;

    move-object v0, v6

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lms/bd/c/s;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;JI)V

    return-object v6
.end method

.method public a(J)Lms/bd/c/t$c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lms/bd/c/y;

    iget-object v1, p0, Lms/bd/c/v;->g:Lms/bd/c/w;

    invoke-direct {v0, v1, p0, p1, p2}, Lms/bd/c/y;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;J)V

    return-object v0
.end method

.method public a(I)Lms/bd/c/t$d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lms/bd/c/a0;

    iget-object v1, p0, Lms/bd/c/v;->g:Lms/bd/c/w;

    invoke-direct {v0, v1, p0, p1}, Lms/bd/c/a0;-><init>(Lms/bd/c/w;Lms/bd/c/t$b;I)V

    return-object v0
.end method
