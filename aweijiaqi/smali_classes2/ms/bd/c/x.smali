.class public Lms/bd/c/x;
.super Lms/bd/c/t$c;
.source ""


# direct methods
.method public constructor <init>(Lms/bd/c/w;Lms/bd/c/t$b;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lms/bd/c/t$c;-><init>()V

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-boolean v2, p2, Lms/bd/c/t$b;->a:Z

    if-eqz v2, :cond_0

    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    :cond_0
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    :goto_0
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-wide v2, p2, Lms/bd/c/t$b;->b:J

    iget p2, p2, Lms/bd/c/t$b;->d:I

    int-to-long v4, p2

    mul-long p3, p3, v4

    add-long/2addr v2, p3

    invoke-virtual {p1, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p2

    int-to-long p2, p2

    const-wide v4, 0xffffffffL

    and-long/2addr p2, v4

    iput-wide p2, p0, Lms/bd/c/t$c;->a:J

    const-wide/16 p2, 0x4

    add-long/2addr p2, v2

    invoke-virtual {p1, v1, p2, p3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p2

    int-to-long p2, p2

    and-long/2addr p2, v4

    iput-wide p2, p0, Lms/bd/c/t$c;->b:J

    const-wide/16 p2, 0x8

    add-long/2addr p2, v2

    invoke-virtual {p1, v1, p2, p3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p2

    int-to-long p2, p2

    and-long/2addr p2, v4

    iput-wide p2, p0, Lms/bd/c/t$c;->c:J

    const-wide/16 p2, 0x14

    add-long/2addr v2, p2

    invoke-virtual {p1, v1, v2, v3, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p1

    int-to-long p1, p1

    and-long/2addr p1, v4

    iput-wide p1, p0, Lms/bd/c/t$c;->d:J

    return-void
.end method
