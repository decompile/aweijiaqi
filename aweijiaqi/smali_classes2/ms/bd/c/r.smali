.class public Lms/bd/c/r;
.super Lms/bd/c/t$a;
.source ""


# direct methods
.method public constructor <init>(Lms/bd/c/w;Lms/bd/c/t$b;JI)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lms/bd/c/t$a;-><init>()V

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-boolean p2, p2, Lms/bd/c/t$b;->a:Z

    if-eqz p2, :cond_0

    sget-object p2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    :cond_0
    sget-object p2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    :goto_0
    invoke-virtual {v1, p2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    mul-int/lit8 p5, p5, 0x8

    int-to-long v2, p5

    add-long/2addr p3, v2

    .line 1
    invoke-virtual {p1, v1, p3, p4, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p2

    int-to-long v2, p2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    .line 2
    iput-wide v2, p0, Lms/bd/c/t$a;->a:J

    const-wide/16 v2, 0x4

    add-long/2addr p3, v2

    .line 3
    invoke-virtual {p1, v1, p3, p4, v0}, Lms/bd/c/w;->a(Ljava/nio/ByteBuffer;JI)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p1

    int-to-long p1, p1

    and-long/2addr p1, v4

    .line 4
    iput-wide p1, p0, Lms/bd/c/t$a;->b:J

    return-void
.end method
