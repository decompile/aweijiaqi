.class public Lms/bd/c/m1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lms/bd/c/m1$a;
    }
.end annotation


# static fields
.field private static volatile a:Z

.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lms/bd/c/e0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lms/bd/c/m1;->b:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    const v0, 0x4000003

    invoke-static {v0}, Lms/bd/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lms/bd/c/m1$a;
    .locals 6

    const-class v0, Lms/bd/c/m1;

    monitor-enter v0

    if-eqz p0, :cond_3

    :try_start_0
    sget-boolean v1, Lms/bd/c/m1;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-nez v1, :cond_0

    monitor-exit v0

    return-object v2

    :cond_0
    const v1, 0x4000002

    :try_start_1
    invoke-static {v1, p0}, Lms/bd/c/b;->a(ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_1

    monitor-exit v0

    return-object v2

    :cond_1
    :try_start_2
    sget-object v3, Lms/bd/c/m1;->b:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lms/bd/c/e0;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez p0, :cond_2

    monitor-exit v0

    return-object v2

    :cond_2
    :try_start_3
    new-instance v2, Lms/bd/c/l1;

    invoke-static {}, Lms/bd/c/a;->a()Lms/bd/c/a;

    move-result-object v3

    invoke-virtual {v3}, Lms/bd/c/a;->b()Landroid/content/Context;

    move-result-object v3

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, p0, v3, v4, v5}, Lms/bd/c/l1;-><init>(Lms/bd/c/e0;Landroid/content/Context;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v0

    return-object v2

    :cond_3
    :try_start_4
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v1, "appID must be set"

    invoke-direct {p0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13

    const-class v0, Lms/bd/c/m1;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lms/bd/c/a;->a()Lms/bd/c/a;

    move-result-object p0

    invoke-virtual {p0, v6}, Lms/bd/c/a;->a(Landroid/content/Context;)V

    invoke-static {v6, p1}, Lms/bd/c/n;->a(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p0, 0x3

    new-array v12, p0, [B

    const/16 p1, 0x6d

    const/4 v1, 0x0

    aput-byte p1, v12, v1

    const/16 p1, 0x6e

    const/4 v2, 0x1

    aput-byte p1, v12, v2

    const/16 p1, 0x56

    const/4 v3, 0x2

    aput-byte p1, v12, v3

    const v7, 0x1000001

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-string v11, "2a6bf4"

    .line 1
    invoke-static/range {v7 .. v12}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 2
    new-instance p1, Lms/bd/c/p0;

    invoke-direct {p1}, Lms/bd/c/p0;-><init>()V

    const v4, 0x1000002

    invoke-static {v4, p1}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p1, Lms/bd/c/a1;

    invoke-direct {p1}, Lms/bd/c/a1;-><init>()V

    const v4, 0x1000001

    invoke-static {v4, p1}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-array v12, p0, [B

    const/16 p0, 0x6f

    aput-byte p0, v12, v1

    const/16 p0, 0x6a

    aput-byte p0, v12, v2

    const/16 p0, 0x12

    aput-byte p0, v12, v3

    const v7, 0x1000001

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-string v11, "0ee8a6"

    invoke-static/range {v7 .. v12}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    new-instance p0, Lms/bd/c/e1;

    invoke-direct {p0}, Lms/bd/c/e1;-><init>()V

    const p1, 0x1000003

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/f1;

    invoke-direct {p0}, Lms/bd/c/f1;-><init>()V

    const p1, 0x1000005

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/g1;

    invoke-direct {p0}, Lms/bd/c/g1;-><init>()V

    const p1, 0x1000006

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/h1;

    invoke-direct {p0}, Lms/bd/c/h1;-><init>()V

    const p1, 0x1000007

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/i1;

    invoke-direct {p0}, Lms/bd/c/i1;-><init>()V

    const p1, 0x1000008

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/j1;

    invoke-direct {p0}, Lms/bd/c/j1;-><init>()V

    const p1, 0x1000009

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/k1;

    invoke-direct {p0}, Lms/bd/c/k1;-><init>()V

    const p1, 0x100000a

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/f0;

    invoke-direct {p0}, Lms/bd/c/f0;-><init>()V

    const p1, 0x100000b

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/g0;

    invoke-direct {p0}, Lms/bd/c/g0;-><init>()V

    const p1, 0x100000c

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/h0;

    invoke-direct {p0}, Lms/bd/c/h0;-><init>()V

    const p1, 0x1000010

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/i0;

    invoke-direct {p0}, Lms/bd/c/i0;-><init>()V

    const p1, 0x1000011

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/j0;

    invoke-direct {p0}, Lms/bd/c/j0;-><init>()V

    const p1, 0x1000013

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/k0;

    invoke-direct {p0}, Lms/bd/c/k0;-><init>()V

    const p1, 0x1000016

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/l0;

    invoke-direct {p0}, Lms/bd/c/l0;-><init>()V

    const p1, 0x1000017

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/m0;

    invoke-direct {p0}, Lms/bd/c/m0;-><init>()V

    const p1, 0x1000019

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/n0;

    invoke-direct {p0}, Lms/bd/c/n0;-><init>()V

    const p1, 0x100001a

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/o0;

    invoke-direct {p0}, Lms/bd/c/o0;-><init>()V

    const p1, 0x100001b

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/q0;

    invoke-direct {p0}, Lms/bd/c/q0;-><init>()V

    const p1, 0x100001c

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/r0;

    invoke-direct {p0}, Lms/bd/c/r0;-><init>()V

    const p1, 0x100001d

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/s0;

    invoke-direct {p0}, Lms/bd/c/s0;-><init>()V

    const p1, 0x100001e

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/t0;

    invoke-direct {p0}, Lms/bd/c/t0;-><init>()V

    const p1, 0x100001f

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/u0;

    invoke-direct {p0}, Lms/bd/c/u0;-><init>()V

    const p1, 0x1000020

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/v0;

    invoke-direct {p0}, Lms/bd/c/v0;-><init>()V

    const p1, 0x1000021

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/w0;

    invoke-direct {p0}, Lms/bd/c/w0;-><init>()V

    const p1, 0x1000022

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/x0;

    invoke-direct {p0}, Lms/bd/c/x0;-><init>()V

    const p1, 0x1000023

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/y0;

    invoke-direct {p0}, Lms/bd/c/y0;-><init>()V

    const p1, 0x1000018

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/z0;

    invoke-direct {p0}, Lms/bd/c/z0;-><init>()V

    const p1, 0x1000024

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/b1;

    invoke-direct {p0}, Lms/bd/c/b1;-><init>()V

    const p1, 0x1000025

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/c1;

    invoke-direct {p0}, Lms/bd/c/c1;-><init>()V

    const p1, 0x1000026

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    new-instance p0, Lms/bd/c/d1;

    invoke-direct {p0}, Lms/bd/c/d1;-><init>()V

    const p1, 0x1000027

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    .line 3
    invoke-static {}, Lms/bd/c/b0;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Lms/bd/c/u1;

    invoke-direct {p0}, Lms/bd/c/u1;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p0, Lms/bd/c/s1;

    invoke-direct {p0}, Lms/bd/c/s1;-><init>()V

    :goto_0
    const p1, 0x2000001

    invoke-static {p1, p0}, Lms/bd/c/b;->a(ILms/bd/c/b$a;)V

    .line 4
    invoke-static {}, Lms/bd/c/g;->a()V

    invoke-static {}, Lcom/bytedance/mobsec/metasec/ml/a;->a()V

    const v1, 0x1000003

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    .line 5
    invoke-static/range {v1 .. v6}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lms/bd/c/e0;Ljava/lang/String;)Z
    .locals 4

    const-class v0, Lms/bd/c/m1;

    monitor-enter v0

    if-eqz p0, :cond_7

    if-eqz p1, :cond_6

    :try_start_0
    iget-object v1, p1, Lms/bd/c/e0;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p1, Lms/bd/c/e0;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lms/bd/c/e0;->g:Ljava/lang/String;

    :goto_0
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    sget-boolean v2, Lms/bd/c/m1;->a:Z

    const/4 v3, 0x1

    if-nez v2, :cond_2

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-boolean v2, Lms/bd/c/m1;->a:Z

    if-nez v2, :cond_1

    invoke-static {p0, p2}, Lms/bd/c/m1;->a(Landroid/content/Context;Ljava/lang/String;)V

    sput-boolean v3, Lms/bd/c/m1;->a:Z

    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p0

    :cond_2
    :goto_1
    sget-object p0, Lms/bd/c/m1;->b:Ljava/util/Map;

    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 p2, 0x0

    if-eqz p0, :cond_3

    monitor-exit v0

    return p2

    :cond_3
    :try_start_3
    invoke-virtual {p1}, Lms/bd/c/e0;->a()Ljava/lang/String;

    move-result-object p0

    const v2, 0x4000001

    invoke-static {v2, p0}, Lms/bd/c/b;->a(ILjava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_4

    sget-object p0, Lms/bd/c/m1;->b:Ljava/util/Map;

    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v0

    return v3

    :cond_4
    monitor-exit v0

    return p2

    :cond_5
    :try_start_4
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "appID must be set"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_6
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "config could not be null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_7
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "context could not be null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method
