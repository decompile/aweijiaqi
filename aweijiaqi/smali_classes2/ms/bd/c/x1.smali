.class public Lms/bd/c/x1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x5

    new-array v6, v0, [B

    fill-array-data v6, :array_0

    const v1, 0x1000001

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const-string v5, "fbd40a"

    invoke-static/range {v1 .. v6}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v0, 0x2

    new-array v6, v0, [B

    fill-array-data v6, :array_1

    const-string v5, "85f2a9"

    invoke-static/range {v1 .. v6}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lms/bd/c/x1;->a:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 1
        0x4ct
        0x3ct
        0x56t
        0x1et
        0x32t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x27t
        0x33t
    .end array-data
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ljava/lang/String;
    .locals 27

    const-class v1, Lms/bd/c/x1;

    monitor-enter v1

    const/16 v0, 0x25

    const/16 v6, 0xe

    const/16 v7, 0xa

    const/4 v11, 0x6

    const/4 v12, 0x5

    const/4 v14, 0x4

    const/4 v15, 0x2

    const/16 v16, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v18, 0x1000001

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    const-string v22, "a9cb55"

    new-array v13, v2, [B

    const/16 v17, 0x3f

    aput-byte v17, v13, v3

    move-object/from16 v23, v13

    invoke-static/range {v18 .. v23}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v13, Lms/bd/c/x1;->a:Ljava/lang/String;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-static {v4}, Lms/bd/c/x1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v17, :cond_0

    monitor-exit v1

    return-object v13

    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v13

    sget-object v8, Lms/bd/c/x1;->a:Ljava/lang/String;

    invoke-virtual {v13, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v9, 0x1000

    new-array v9, v9, [B

    :goto_0
    const/16 v5, 0x1000

    invoke-virtual {v8, v9, v3, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/4 v10, -0x1

    if-eq v5, v10, :cond_1

    invoke-virtual {v13, v9, v3, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-array v8, v7, [B

    aput-byte v0, v8, v3

    const/16 v9, 0x6c

    aput-byte v9, v8, v2

    const/16 v9, 0x4f

    aput-byte v9, v8, v15

    const/16 v9, 0x19

    aput-byte v9, v8, v16

    aput-byte v6, v8, v14

    const/16 v9, 0x63

    aput-byte v9, v8, v12

    const/16 v9, 0x63

    aput-byte v9, v8, v11

    const/16 v9, 0x10

    const/4 v10, 0x7

    aput-byte v9, v8, v10

    const/16 v9, 0x37

    const/16 v10, 0x8

    aput-byte v9, v8, v10

    const/16 v9, 0x72

    const/16 v10, 0x9

    aput-byte v9, v8, v10

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "7f1b54"

    move-object/from16 v26, v8

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lms/bd/c/x1;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v4}, Lms/bd/c/x1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    new-instance v5, Ljava/io/RandomAccessFile;

    new-array v8, v15, [B

    const/16 v9, 0x35

    aput-byte v9, v8, v3

    const/16 v9, 0x76

    aput-byte v9, v8, v2

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "6c7458"

    move-object/from16 v26, v8

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v5, v4, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v8, v2, [B

    aput-byte v15, v8, v3

    const-wide/16 v9, 0x10

    invoke-virtual {v5, v9, v10}, Ljava/io/RandomAccessFile;->seek(J)V

    invoke-virtual {v5, v8}, Ljava/io/RandomAccessFile;->write([B)V

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V

    invoke-static {v4}, Lms/bd/c/x1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    const/16 v4, 0x14

    :try_start_2
    new-array v4, v4, [B

    const/16 v5, 0x23

    aput-byte v5, v4, v3

    const/16 v5, 0xc

    aput-byte v5, v4, v2

    const/16 v5, 0x1b

    aput-byte v5, v4, v15

    aput-byte v3, v4, v16

    const/16 v5, 0x55

    aput-byte v5, v4, v14

    const/16 v8, 0x49

    aput-byte v8, v4, v12

    const/16 v8, 0x44

    aput-byte v8, v4, v11

    const/16 v8, 0x2c

    const/4 v9, 0x7

    aput-byte v8, v4, v9

    const/16 v8, 0x46

    const/16 v9, 0x8

    aput-byte v8, v4, v9

    const/16 v8, 0x40

    const/16 v9, 0x9

    aput-byte v8, v4, v9

    const/16 v8, 0x43

    aput-byte v8, v4, v7

    const/16 v8, 0xb

    aput-byte v16, v4, v8

    const/16 v8, 0x6e

    const/16 v9, 0xc

    aput-byte v8, v4, v9

    const/16 v9, 0xd

    aput-byte v8, v4, v9

    aput-byte v0, v4, v6

    const/16 v0, 0xf

    const/16 v8, 0x4f

    aput-byte v8, v4, v0

    const/16 v0, 0x3d

    const/16 v8, 0x10

    aput-byte v0, v4, v8

    const/16 v0, 0x11

    aput-byte v5, v4, v0

    const/16 v0, 0x12

    const/16 v5, 0x3b

    aput-byte v5, v4, v0

    const/16 v0, 0x13

    const/16 v5, 0x58

    aput-byte v5, v4, v0

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "b5454c"

    move-object/from16 v26, v4

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    const/16 v0, 0x10

    new-array v0, v0, [B

    const/16 v4, 0x79

    aput-byte v4, v0, v3

    aput-byte v2, v0, v2

    const/16 v3, 0x1e

    aput-byte v3, v0, v15

    aput-byte v14, v0, v16

    aput-byte v16, v0, v14

    const/16 v4, 0x4e

    aput-byte v4, v0, v12

    aput-byte v3, v0, v11

    const/16 v5, 0x2b

    const/4 v8, 0x7

    aput-byte v5, v0, v8

    const/16 v5, 0x52

    const/16 v8, 0x8

    aput-byte v5, v0, v8

    const/16 v5, 0x9

    aput-byte v4, v0, v5

    const/16 v5, 0x1b

    aput-byte v5, v0, v7

    const/16 v5, 0xb

    aput-byte v2, v0, v5

    const/16 v2, 0xc

    aput-byte v3, v0, v2

    const/16 v2, 0xd

    aput-byte v14, v0, v2

    aput-byte v16, v0, v6

    const/16 v2, 0xf

    aput-byte v4, v0, v2

    const v21, 0x1000001

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-string v25, "8811bd"

    move-object/from16 v26, v0

    invoke-static/range {v21 .. v26}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_5
    monitor-exit v1

    return-object v5

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/io/BufferedInputStream;)Ljava/lang/String;
    .locals 9
    .annotation runtime Lcom/bytedance/JProtect;
    .end annotation

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const/16 v0, 0x1000

    new-array v1, v0, [B

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :cond_1
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_2

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    if-ge v3, v0, :cond_1

    goto :goto_0

    :catch_0
    const/4 p0, 0x3

    new-array v8, p0, [B

    fill-array-data v8, :array_0

    const v3, 0x1000001

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-string v7, "afc3cf"

    invoke-static/range {v3 .. v8}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    nop

    :array_0
    .array-data 1
        0x75t
        0x6dt
        0x13t
    .end array-data
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .annotation runtime Lcom/bytedance/JProtect;
    .end annotation

    :goto_0
    const/16 v0, 0x50

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x5

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    new-array v11, v2, [B

    const/16 v6, 0x37

    aput-byte v6, v11, v1

    const/16 v6, 0x32

    aput-byte v6, v11, v0

    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "5898c6"

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    new-instance v6, Ljava/io/BufferedOutputStream;

    invoke-virtual {v5}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-virtual {v6, p0}, Ljava/io/BufferedOutputStream;->write([B)V

    const/16 p0, 0xa

    invoke-virtual {v6, p0}, Ljava/io/BufferedOutputStream;->write(I)V

    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {v5}, Ljava/lang/Process;->waitFor()I

    invoke-static {v7}, Lms/bd/c/x1;->a(Ljava/io/BufferedInputStream;)Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz p0, :cond_0

    :try_start_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catch_0
    move-object v4, p0

    goto :goto_4

    :cond_0
    :goto_2
    :try_start_5
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    :catch_1
    new-array v13, v3, [B

    fill-array-data v13, :array_0

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "891070"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_3
    :try_start_6
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_a

    :catch_2
    new-array v13, v3, [B

    fill-array-data v13, :array_1

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "18b2b3"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_a

    :catchall_0
    move-exception p0

    goto/16 :goto_b

    :catch_3
    :goto_4
    move-object p0, v4

    goto :goto_5

    :catchall_1
    move-exception p0

    goto/16 :goto_c

    :catch_4
    move-object p0, v4

    move-object v7, p0

    :goto_5
    move-object v4, v6

    goto :goto_7

    :catchall_2
    move-exception p0

    goto :goto_6

    :catchall_3
    move-exception p0

    move-object v5, v4

    :goto_6
    move-object v0, v4

    goto :goto_d

    :catch_5
    move-object v5, v4

    :catch_6
    move-object p0, v4

    move-object v7, p0

    :goto_7
    :try_start_7
    new-array v13, v3, [B

    const/16 v6, 0x21

    aput-byte v6, v13, v1

    const/16 v1, 0x3c

    aput-byte v1, v13, v0

    const/16 v0, 0x40

    aput-byte v0, v13, v2

    const/4 v0, 0x3

    const/16 v1, 0x73

    aput-byte v1, v13, v0

    const/4 v0, 0x4

    const/16 v1, 0x5f

    aput-byte v1, v13, v0

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "5708e4"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    if-eqz v4, :cond_1

    :try_start_8
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_8

    :catch_7
    new-array v13, v3, [B

    fill-array-data v13, :array_2

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "454815"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_1
    :goto_8
    if-eqz v7, :cond_2

    :try_start_9
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_9

    :catch_8
    new-array v13, v3, [B

    fill-array-data v13, :array_3

    const v8, 0x1000001

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-string v12, "e866a8"

    invoke-static/range {v8 .. v13}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_2
    :goto_9
    if-eqz v5, :cond_3

    :goto_a
    invoke-virtual {v5}, Ljava/lang/Process;->destroy()V

    :cond_3
    return-object p0

    :catchall_4
    move-exception p0

    move-object v6, v4

    :goto_b
    move-object v4, v7

    :goto_c
    move-object v0, v4

    move-object v4, v6

    :goto_d
    if-eqz v4, :cond_4

    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_e

    :catch_9
    new-array v11, v3, [B

    fill-array-data v11, :array_4

    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "cfdef3"

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_4
    :goto_e
    if-eqz v0, :cond_5

    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_f

    :catch_a
    new-array v11, v3, [B

    fill-array-data v11, :array_5

    const v6, 0x1000001

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-string v10, "9349d6"

    invoke-static/range {v6 .. v11}, Lms/bd/c/h;->a(IIJLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_5
    :goto_f
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/lang/Process;->destroy()V

    :cond_6
    throw p0

    :pswitch_1
    const/16 v0, 0x52

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x50
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 1
        0x2ct
        0x32t
        0x41t
        0x7bt
        0xdt
    .end array-data

    nop

    :array_1
    .array-data 1
        0x25t
        0x33t
        0x12t
        0x79t
        0x58t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x20t
        0x3et
        0x44t
        0x73t
        0xbt
    .end array-data

    nop

    :array_3
    .array-data 1
        0x71t
        0x33t
        0x46t
        0x7dt
        0x5bt
    .end array-data

    nop

    :array_4
    .array-data 1
        0x77t
        0x6dt
        0x14t
        0x2et
        0x5ct
    .end array-data

    nop

    :array_5
    .array-data 1
        0x2dt
        0x38t
        0x44t
        0x72t
        0x5et
    .end array-data
.end method
