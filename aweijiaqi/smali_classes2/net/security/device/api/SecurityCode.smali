.class public Lnet/security/device/api/SecurityCode;
.super Ljava/lang/Object;
.source "SecurityCode.java"


# static fields
.field public static final SC_APPKEY_EMPTY:I = 0x2719

.field private static final SC_CODE_BASE:I = 0x2710

.field public static final SC_NETWORK_ERROR:I = 0x2714

.field public static final SC_NETWORK_ERROR_EMPTY:I = 0x2715

.field public static final SC_NETWORK_ERROR_INVALID:I = 0x2716

.field public static final SC_NETWORK_RET_CODE_ERROR:I = 0x2718

.field public static final SC_NOT_INIT:I = 0x2711

.field public static final SC_NOT_PERMISSION:I = 0x2712

.field public static final SC_PARAMS_ERROR:I = 0x271a

.field public static final SC_PARSE_SRV_CFG_ERROR:I = 0x2717

.field public static final SC_SUCCESS:I = 0x2710

.field public static final SC_UNKNOWN_ERROR:I = 0x2713


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
