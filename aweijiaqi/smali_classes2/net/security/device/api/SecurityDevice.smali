.class public Lnet/security/device/api/SecurityDevice;
.super Ljava/lang/Object;
.source "SecurityDevice.java"


# static fields
.field private static s_instance:Lnet/security/device/api/SecurityDevice;


# instance fields
.field private ctx:Landroid/content/Context;

.field private initThread:Ljava/lang/Thread;

.field private isInited:Z

.field private otherThread:Ljava/lang/Thread;

.field private userAppKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "securitydevice"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 35
    new-instance v0, Lnet/security/device/api/SecurityDevice;

    invoke-direct {v0}, Lnet/security/device/api/SecurityDevice;-><init>()V

    sput-object v0, Lnet/security/device/api/SecurityDevice;->s_instance:Lnet/security/device/api/SecurityDevice;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lnet/security/device/api/SecurityDevice;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    .line 25
    iput-boolean v1, p0, Lnet/security/device/api/SecurityDevice;->isInited:Z

    .line 53
    iput-object v0, p0, Lnet/security/device/api/SecurityDevice;->initThread:Ljava/lang/Thread;

    .line 54
    iput-object v0, p0, Lnet/security/device/api/SecurityDevice;->otherThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$000(Lnet/security/device/api/SecurityDevice;I)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lnet/security/device/api/SecurityDevice;->runInit(I)V

    return-void
.end method

.method static synthetic access$100(Lnet/security/device/api/SecurityDevice;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lnet/security/device/api/SecurityDevice;->otherThreadRun()V

    return-void
.end method

.method private getCtx()Landroid/content/Context;
    .locals 1

    .line 50
    iget-object v0, p0, Lnet/security/device/api/SecurityDevice;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lnet/security/device/api/SecurityDevice;
    .locals 1

    .line 42
    sget-object v0, Lnet/security/device/api/SecurityDevice;->s_instance:Lnet/security/device/api/SecurityDevice;

    return-object v0
.end method

.method private native getSessionRaw()Lnet/security/device/api/SecuritySession;
.end method

.method public static native gs(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private initCommon(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;I)V
    .locals 2

    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    .line 58
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    iget-boolean v0, p0, Lnet/security/device/api/SecurityDevice;->isInited:Z

    const/16 v1, 0x2710

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    .line 67
    invoke-interface {p3, v1}, Lnet/security/device/api/SecurityInitListener;->onInitFinish(I)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    .line 72
    iput-boolean v0, p0, Lnet/security/device/api/SecurityDevice;->isInited:Z

    .line 73
    iput-object p2, p0, Lnet/security/device/api/SecurityDevice;->userAppKey:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lnet/security/device/api/SecurityDevice;->ctx:Landroid/content/Context;

    .line 77
    iget-object p1, p0, Lnet/security/device/api/SecurityDevice;->initThread:Ljava/lang/Thread;

    if-nez p1, :cond_3

    .line 78
    new-instance p1, Ljava/lang/Thread;

    new-instance p2, Lnet/security/device/api/SecurityDevice$1;

    invoke-direct {p2, p0, p4}, Lnet/security/device/api/SecurityDevice$1;-><init>(Lnet/security/device/api/SecurityDevice;I)V

    invoke-direct {p1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object p1, p0, Lnet/security/device/api/SecurityDevice;->initThread:Ljava/lang/Thread;

    .line 84
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 88
    :cond_3
    iget-object p1, p0, Lnet/security/device/api/SecurityDevice;->otherThread:Ljava/lang/Thread;

    if-nez p1, :cond_4

    .line 89
    new-instance p1, Ljava/lang/Thread;

    new-instance p2, Lnet/security/device/api/SecurityDevice$2;

    invoke-direct {p2, p0}, Lnet/security/device/api/SecurityDevice$2;-><init>(Lnet/security/device/api/SecurityDevice;)V

    invoke-direct {p1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object p1, p0, Lnet/security/device/api/SecurityDevice;->otherThread:Ljava/lang/Thread;

    .line 95
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_4
    if-eqz p3, :cond_5

    .line 99
    invoke-interface {p3, v1}, Lnet/security/device/api/SecurityInitListener;->onInitFinish(I)V

    :cond_5
    return-void

    :cond_6
    :goto_0
    if-eqz p3, :cond_7

    const/16 p1, 0x271a

    .line 60
    invoke-interface {p3, p1}, Lnet/security/device/api/SecurityInitListener;->onInitFinish(I)V

    :cond_7
    return-void
.end method

.method private native initRaw(Landroid/content/Context;Ljava/lang/String;I)I
.end method

.method private native lxRaw()V
.end method

.method private native otherThreadInit(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method private otherThreadRun()V
    .locals 2

    .line 128
    invoke-direct {p0}, Lnet/security/device/api/SecurityDevice;->getCtx()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lnet/security/device/api/SecurityDevice;->userAppKey:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lnet/security/device/api/SecurityDevice;->otherThreadInit(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private native reportUserDataRaw(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private runInit(I)V
    .locals 2

    .line 122
    invoke-direct {p0}, Lnet/security/device/api/SecurityDevice;->getCtx()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lnet/security/device/api/SecurityDevice;->userAppKey:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1}, Lnet/security/device/api/SecurityDevice;->initRaw(Landroid/content/Context;Ljava/lang/String;I)I

    const/4 p1, 0x0

    .line 124
    iput-boolean p1, p0, Lnet/security/device/api/SecurityDevice;->isInited:Z

    return-void
.end method


# virtual methods
.method public getSession()Lnet/security/device/api/SecuritySession;
    .locals 1

    .line 143
    invoke-direct {p0}, Lnet/security/device/api/SecurityDevice;->getSessionRaw()Lnet/security/device/api/SecuritySession;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 109
    invoke-direct {p0, p1, p2, p3, v0}, Lnet/security/device/api/SecurityDevice;->initCommon(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;I)V

    return-void
.end method

.method public initV6(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;)V
    .locals 1

    const/4 v0, 0x1

    .line 118
    invoke-direct {p0, p1, p2, p3, v0}, Lnet/security/device/api/SecurityDevice;->initCommon(Landroid/content/Context;Ljava/lang/String;Lnet/security/device/api/SecurityInitListener;I)V

    return-void
.end method

.method public lx()V
    .locals 0

    .line 177
    invoke-direct {p0}, Lnet/security/device/api/SecurityDevice;->lxRaw()V

    return-void
.end method

.method public reportUserData(ILjava/lang/String;)V
    .locals 0

    add-int/lit16 p1, p1, 0x2710

    .line 152
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lnet/security/device/api/SecurityDevice;->reportUserDataRaw(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
